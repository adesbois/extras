#include <math.h>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <stdarg.h>
#include <iostream>
#include "svm.h"
int libsvm_version = LIBSVM_VERSION;


typedef float Qfloat;
typedef signed char schar;
#ifndef min
template <class T>
static T min(T x, T y) { return (x < y) ? x : y; }
#endif
#ifndef max
template <class T>
static T max(T x, T y) { return (x > y) ? x : y; }
#endif
template <class T>
static void swap(T& x, T& y)
{
	T t = x;
	x   = y;
	y   = t;
}

template <class S, class T>
static void clone(T*& dst, S* src, const int n)
{
	dst = new T[n];
	memcpy((void*)dst, (void*)src, sizeof(T) * n);
}

static double powi(const double base, const int times)
{
	double tmp = base, ret = 1.0;

	for (int t = times; t > 0; t /= 2)
	{
		if (t % 2 == 1) { ret *= tmp; }
		tmp = tmp * tmp;
	}
	return ret;
}

#define INF HUGE_VAL
#define TAU 1e-12
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

static void print_string_stdout(const char* s)
{
	fputs(s,stdout);
	fflush(stdout);
}

static void (*svm_print_string)(const char*) = &print_string_stdout;
#if 0
static void info(const char *fmt,...)
{
	char buf[BUFSIZ];
	va_list ap;
	va_start(ap,fmt);
	vsprintf(buf,fmt,ap);
	va_end(ap);
	(*svm_print_string)(buf);
}
#else
static void info(const char* /*fmt*/, ...) {}
#endif

//
// Kernel Cache
//
// l is the number of total data items
// size is the cache size limit in bytes
//
class Cache
{
public:
	Cache(int l_, long int size_);
	~Cache();

	// request data [0,len)
	// return some position p where [p,len) need to be filled
	// (p >= len if nothing needs to be filled)
	int get_data(int index, Qfloat** data, int len);
	void swap_index(int i, int j);
private:
	int l;
	long int size;

	struct head_t
	{
		head_t *prev, *next;	// a circular list
		Qfloat* data;
		int len;		// data[0,len) is cached in this entry
	};

	head_t* head;
	head_t lru_head;
	void lru_delete(head_t* h);
	void lru_insert(head_t* h);
};

Cache::Cache(const int l_, const long int size_): l(l_), size(size_)
{
	head = static_cast<head_t*>(calloc(l, sizeof(head_t)));	// initialized to 0
	size /= sizeof(Qfloat);
	size -= l * long(sizeof(head_t) / sizeof(Qfloat));
	size          = max(size, 2 * static_cast<long int>(l));	// cache must be large enough for two columns
	lru_head.next = lru_head.prev = &lru_head;
}

Cache::~Cache()
{
	for (head_t* h = lru_head.next; h != &lru_head; h = h->next) { free(h->data); }
	free(head);
}

void Cache::lru_delete(head_t* h)
{
	// delete from current location
	h->prev->next = h->next;
	h->next->prev = h->prev;
}

void Cache::lru_insert(head_t* h)
{
	// insert to last position
	h->next       = &lru_head;
	h->prev       = lru_head.prev;
	h->prev->next = h;
	h->next->prev = h;
}

int Cache::get_data(const int index, Qfloat** data, int len)
{
	head_t* h = &head[index];
	if (h->len) { lru_delete(h); }
	const int more = len - h->len;

	if (more > 0)
	{
		// free old space
		while (size < more)
		{
			head_t* old = lru_head.next;
			lru_delete(old);
			free(old->data);
			size += old->len;
			old->data = nullptr;
			old->len  = 0;
		}

		// allocate new space
		h->data = (Qfloat*)realloc(h->data, sizeof(Qfloat) * len);
		size -= more;
		swap(h->len, len);
	}

	lru_insert(h);
	*data = h->data;
	return len;
}

void Cache::swap_index(int i, int j)
{
	if (i == j) { return; }

	if (head[i].len) { lru_delete(&head[i]); }
	if (head[j].len) { lru_delete(&head[j]); }
	swap(head[i].data, head[j].data);
	swap(head[i].len, head[j].len);
	if (head[i].len) { lru_insert(&head[i]); }
	if (head[j].len) { lru_insert(&head[j]); }

	if (i > j) { swap(i, j); }
	for (head_t* h = lru_head.next; h != &lru_head; h = h->next)
	{
		if (h->len > i)
		{
			if (h->len > j) { swap(h->data[i], h->data[j]); }
			else
			{
				// give up
				lru_delete(h);
				free(h->data);
				size += h->len;
				h->data = nullptr;
				h->len  = 0;
			}
		}
	}
}

//
// Kernel evaluation
//
// the static method k_function is for doing single kernel evaluation
// the constructor of Kernel prepares to calculate the l*l kernel matrix
// the member function get_Q is for getting one column from the Q Matrix
//
class QMatrix
{
public:
	virtual Qfloat* get_Q(const int column, const int len) const = 0;
	virtual Qfloat* get_QD() const = 0;
	virtual void swap_index(const int i, const int j) const = 0;
	virtual ~QMatrix() {}
};

class Kernel : public QMatrix
{
public:
	Kernel(int l, svm_node* const * x_, const svm_parameter& param);
	~Kernel() override;

	static double k_function(const svm_node* x, const svm_node* y,
							 const svm_parameter& param);
	Qfloat* get_Q(const int column, const int len) const override = 0;
	Qfloat* get_QD() const override = 0;

	void swap_index(const int i, const int j) const override
	// no so const...
	{
		swap(x[i], x[j]);
		if (x_square) { swap(x_square[i], x_square[j]); }
	}

protected:

	double (Kernel::*kernel_function)(int i, int j) const;

private:
	const svm_node** x;
	double* x_square;

	// svm_parameter
	const int kernel_type;
	const int degree;
	const double gamma;
	const double coef0;

	static double dot(const svm_node* px, const svm_node* py);

	double kernel_linear(const int i, const int j) const { return dot(x[i], x[j]); }
	double kernel_poly(const int i, const int j) const { return powi(gamma * dot(x[i], x[j]) + coef0, degree); }
	double kernel_rbf(const int i, const int j) const { return exp(-gamma * (x_square[i] + x_square[j] - 2 * dot(x[i], x[j]))); }
	double kernel_sigmoid(const int i, const int j) const { return tanh(gamma * dot(x[i], x[j]) + coef0); }
	double kernel_precomputed(const int i, const int j) const { return x[i][int(x[j][0].value)].value; }
};

Kernel::Kernel(const int l, svm_node* const * x_, const svm_parameter& param)
	: kernel_type(param.kernel_type), degree(param.degree), gamma(param.gamma), coef0(param.coef0)
{
	switch (kernel_type)
	{
		case LINEAR:
			kernel_function = &Kernel::kernel_linear;
			break;
		case POLY:
			kernel_function = &Kernel::kernel_poly;
			break;
		case RBF:
			kernel_function = &Kernel::kernel_rbf;
			break;
		case SIGMOID:
			kernel_function = &Kernel::kernel_sigmoid;
			break;
		case PRECOMPUTED:
			kernel_function = &Kernel::kernel_precomputed;
			break;
		default: break;
	}

	clone(x, x_, l);

	if (kernel_type == RBF)
	{
		x_square = new double[l];
		for (int i = 0; i < l; ++i) { x_square[i] = dot(x[i], x[i]); }
	}
	else { x_square = nullptr; }
}

Kernel::~Kernel()
{
	delete[] x;
	delete[] x_square;
}

double Kernel::dot(const svm_node* px, const svm_node* py)
{
	double sum = 0;
	/*std::cout<<", px("<<px->index<<")="<<std::endl;
	std::cout<<px->value<<std::endl;
	std::cout<<", py("<<py->index<<")="<<std::endl;
	std::cout<<py->value<<")"<<std::endl;*/

	while (px->index != -1 && py->index != -1)
	{
		/*std::cout<<", px("<<px->index<<")="<<std::endl;
		std::cout<<px->value<<std::endl;
		std::cout<<", py("<<py->index<<")="<<std::endl;
		std::cout<<py->value<<")"<<std::endl;*/
		if (px->index == py->index)
		{
			sum += px->value * py->value;
			++px;
			++py;
		}
		else
		{
			if (px->index > py->index) { ++py; }
			else { ++px; }
		}
	}
	return sum;
}

double Kernel::k_function(const svm_node* x, const svm_node* y, const svm_parameter& param)
{
	//std::cout<<"("<<param.kernel_type;
	switch (param.kernel_type)
	{
		case LINEAR:

			return dot(x, y);
		case POLY:
			return powi(param.gamma * dot(x, y) + param.coef0, param.degree);
		case RBF:
		{
			double sum = 0;
			while (x->index != -1 && y->index != -1)
			{
				if (x->index == y->index)
				{
					const double d = x->value - y->value;
					sum += d * d;
					++x;
					++y;
				}
				else
				{
					if (x->index > y->index)
					{
						sum += y->value * y->value;
						++y;
					}
					else
					{
						sum += x->value * x->value;
						++x;
					}
				}
			}

			while (x->index != -1)
			{
				sum += x->value * x->value;
				++x;
			}

			while (y->index != -1)
			{
				sum += y->value * y->value;
				++y;
			}

			return exp(-param.gamma * sum);
		}
		case SIGMOID:
			return tanh(param.gamma * dot(x, y) + param.coef0);
		case PRECOMPUTED:  //x: test (validation), y: SV
			return x[int(y->value)].value;
		default:
			return 0;  // Unreachable 
	}
}

// An SMO algorithm in Fan et al., JMLR 6(2005), p. 1889--1918
// Solves:
//
// min 0.5(\alpha^T Q \alpha) + p^T \alpha
//
// y^T \alpha = \delta
// y_i = +1 or -1
// 0 <= alpha_i <= Cp for y_i = 1
// 0 <= alpha_i <= Cn for y_i = -1
//
// Given:
//
// Q, p, y, Cp, Cn, and an initial feasible point \alpha
// l is the size of vectors and matrices
// eps is the stopping tolerance
//
// solution will be put in \alpha, objective value will be put in obj
//
class Solver
{
public:
	Solver() { }
	virtual ~Solver() { }

	struct SSolutionInfo
	{
		double obj;
		double rho;
		double upperBoundP;
		double upperBoundN;
		double r;	// for Solver_NU
	};

	virtual void solve(const int l_, const QMatrix& q_, const double* p_, const schar* y_, double* alpha_, const double cp, const double cn, const double eps,
					   SSolutionInfo* si, const int shrinking);

protected:
	int m_activeSize = 0;
	schar* y         = nullptr;
	double* G        = nullptr;		// gradient of objective function
	enum { LOWER_BOUND, UPPER_BOUND, FREE };

	char* alpha_status = nullptr;	// LOWER_BOUND, UPPER_BOUND, FREE
	double* alpha      = nullptr;
	const QMatrix* Q   = nullptr;
	const Qfloat* QD   = nullptr;
	double eps         = 0;
	double Cp          = 0, Cn = 0;
	double* p          = nullptr;
	int* active_set    = nullptr;
	double* G_bar      = nullptr;		// gradient, if we treat free variables as 0
	int l              = 0;
	bool unshrink      = false;			// XXX

	double getC(const int i) { return (y[i] > 0) ? Cp : Cn; }

	void updateAlphaStatus(const int i)
	{
		if (alpha[i] >= getC(i)) { alpha_status[i] = UPPER_BOUND; }
		else if (alpha[i] <= 0) { alpha_status[i] = LOWER_BOUND; }
		else { alpha_status[i] = FREE; }
	}

	bool isUpperBound(const int i) const { return alpha_status[i] == UPPER_BOUND; }
	bool isLowerBound(const int i) const { return alpha_status[i] == LOWER_BOUND; }
	bool isFree(const int i) const { return alpha_status[i] == FREE; }
	void swapIndex(int i, int j);
	void reconstructGradient();
	virtual int selectWorkingSet(int& outI, int& outJ);
	virtual double calculateRho();
	virtual void doShrinking();
private:
	bool beShrunk(int i, double gmax1, double gmax2);
};

void Solver::swapIndex(const int i, const int j)
{
	Q->swap_index(i, j);
	swap(y[i], y[j]);
	swap(G[i], G[j]);
	swap(alpha_status[i], alpha_status[j]);
	swap(alpha[i], alpha[j]);
	swap(p[i], p[j]);
	swap(active_set[i], active_set[j]);
	swap(G_bar[i], G_bar[j]);
}

void Solver::reconstructGradient()
{
	// reconstruct inactive elements of G from G_bar and free variables

	if (m_activeSize == l) { return; }

	int i, j;
	int nrFree = 0;

	for (j = m_activeSize; j < l; ++j) { G[j] = G_bar[j] + p[j]; }

	for (j = 0; j < m_activeSize; ++j) { if (isFree(j)) { nrFree++; } }

	if (2 * nrFree < m_activeSize) { info("\nWarning: using -h 0 may be faster\n"); }

	if (nrFree * l > 2 * m_activeSize * (l - m_activeSize))
	{
		for (i = m_activeSize; i < l; ++i)
		{
			const Qfloat* Q_i = Q->get_Q(i, m_activeSize);
			for (j = 0; j < m_activeSize; ++j) { if (isFree(j)) { G[i] += alpha[j] * Q_i[j]; } }
		}
	}
	else
	{
		for (i = 0; i < m_activeSize; ++i)
		{
			if (isFree(i))
			{
				const Qfloat* Q_i    = Q->get_Q(i, l);
				const double alpha_i = alpha[i];
				for (j = m_activeSize; j < l; ++j) { G[j] += alpha_i * Q_i[j]; }
			}
		}
	}
}

void Solver::solve(const int l_, const QMatrix& q_, const double* p_, const schar* y_, double* alpha_, const double cp, const double cn, const double eps,
				   SSolutionInfo* si, const int shrinking)
{
	this->l = l_;
	this->Q = &q_;
	QD      = q_.get_QD();
	clone(p, p_, l_);
	clone(y, y_, l_);
	clone(alpha, alpha_, l_);
	this->Cp  = cp;
	this->Cn  = cn;
	this->eps = eps;
	unshrink  = false;

	// initialize alpha_status
	{
		alpha_status = new char[l_];
		for (int i = 0; i < l_; ++i) { updateAlphaStatus(i); }
	}

	// initialize active set (for shrinking)
	{
		active_set = new int[l_];
		for (int i = 0; i < l_; ++i) { active_set[i] = i; }
		m_activeSize = l_;
	}

	// initialize gradient
	{
		G     = new double[l_];
		G_bar = new double[l_];
		int i;
		for (i = 0; i < l_; ++i)
		{
			G[i]     = p[i];
			G_bar[i] = 0;
		}
		for (i = 0; i < l_; ++i)
		{
			if (!isLowerBound(i))
			{
				const Qfloat* qI    = q_.get_Q(i, l_);
				const double alphaI = alpha[i];
				int j;
				for (j = 0; j < l_; ++j) { G[j] += alphaI * qI[j]; }
				if (isUpperBound(i)) { for (j = 0; j < l_; ++j) { G_bar[j] += getC(i) * qI[j]; } }
			}
		}
	}

	// optimization step

	int iter    = 0;
	int counter = min(l_, 1000) + 1;

	while (true)
	{
		// show progress and do shrinking

		if (--counter == 0)
		{
			counter = min(l_, 1000);
			if (shrinking) { doShrinking(); }
			info(".");
		}

		int i, j;
		if (selectWorkingSet(i, j) != 0)
		{
			// reconstruct the whole gradient
			reconstructGradient();
			// reset active set size and check
			m_activeSize = l_;
			info("*");
			if (selectWorkingSet(i, j) != 0) { break; }
			counter = 1;
			// do shrinking next iteration
		}

		++iter;

		// update alpha[i] and alpha[j], handle bounds carefully

		const Qfloat* qI = q_.get_Q(i, m_activeSize);
		const Qfloat* qJ = q_.get_Q(j, m_activeSize);

		const double cI = getC(i);
		const double cJ = getC(j);

		const double oldAlphaI = alpha[i];
		const double oldAlphaJ = alpha[j];

		if (y[i] != y[j])
		{
			double quadCoef = qI[i] + qJ[j] + 2 * qI[j];
			if (quadCoef <= 0) { quadCoef = TAU; }
			const double delta = (-G[i] - G[j]) / quadCoef;
			const double diff  = alpha[i] - alpha[j];
			alpha[i] += delta;
			alpha[j] += delta;

			if (diff > 0)
			{
				if (alpha[j] < 0)
				{
					alpha[j] = 0;
					alpha[i] = diff;
				}
			}
			else
			{
				if (alpha[i] < 0)
				{
					alpha[i] = 0;
					alpha[j] = -diff;
				}
			}
			if (diff > cI - cJ)
			{
				if (alpha[i] > cI)
				{
					alpha[i] = cI;
					alpha[j] = cI - diff;
				}
			}
			else
			{
				if (alpha[j] > cJ)
				{
					alpha[j] = cJ;
					alpha[i] = cJ + diff;
				}
			}
		}
		else
		{
			double quadCoef = qI[i] + qJ[j] - 2 * qI[j];
			if (quadCoef <= 0) { quadCoef = TAU; }
			const double delta = (G[i] - G[j]) / quadCoef;
			const double sum   = alpha[i] + alpha[j];
			alpha[i] -= delta;
			alpha[j] += delta;

			if (sum > cI)
			{
				if (alpha[i] > cI)
				{
					alpha[i] = cI;
					alpha[j] = sum - cI;
				}
			}
			else
			{
				if (alpha[j] < 0)
				{
					alpha[j] = 0;
					alpha[i] = sum;
				}
			}
			if (sum > cJ)
			{
				if (alpha[j] > cJ)
				{
					alpha[j] = cJ;
					alpha[i] = sum - cJ;
				}
			}
			else
			{
				if (alpha[i] < 0)
				{
					alpha[i] = 0;
					alpha[j] = sum;
				}
			}
		}

		// update G

		const double deltaAlphaI = alpha[i] - oldAlphaI;
		const double deltaAlphaJ = alpha[j] - oldAlphaJ;

		for (int k = 0; k < m_activeSize; ++k) { G[k] += qI[k] * deltaAlphaI + qJ[k] * deltaAlphaJ; }

		// update alpha_status and G_bar

		{
			const bool ui = isUpperBound(i);
			const bool uj = isUpperBound(j);
			updateAlphaStatus(i);
			updateAlphaStatus(j);
			int k;
			if (ui != isUpperBound(i))
			{
				qI = q_.get_Q(i, l_);
				if (ui) { for (k = 0; k < l_; ++k) { G_bar[k] -= cI * qI[k]; } }
				else { for (k = 0; k < l_; ++k) { G_bar[k] += cI * qI[k]; } }
			}

			if (uj != isUpperBound(j))
			{
				qJ = q_.get_Q(j, l_);
				if (uj) { for (k = 0; k < l_; ++k) { G_bar[k] -= cJ * qJ[k]; } }
				else { for (k = 0; k < l_; ++k) { G_bar[k] += cJ * qJ[k]; } }
			}
		}
	}

	// calculate rho

	si->rho = calculateRho();

	// calculate objective value
	{
		double v = 0;
		for (int i = 0; i < l_; ++i) { v += alpha[i] * (G[i] + p[i]); }

		si->obj = v / 2;
	}

	// put back the solution
	{
		for (int i = 0; i < l_; ++i) { alpha_[active_set[i]] = alpha[i]; }
	}

	// juggle everything back
	/*{
		for (int i=0;i<l_;i++) while(active_set[i] != i) swapIndex(i,active_set[i]);	// or Q.swapIndex(i,active_set[i]);
	}*/

	si->upperBoundP = cp;
	si->upperBoundN = cn;

	info("\noptimization finished, #iter = %d\n", iter);

	delete[] p;
	delete[] y;
	delete[] alpha;
	delete[] alpha_status;
	delete[] active_set;
	delete[] G;
	delete[] G_bar;
}

// return 1 if already optimal, return 0 otherwise
int Solver::selectWorkingSet(int& outI, int& outJ)
{
	// return i,j such that
	// i: maximizes -y_i * grad(f)_i, i in I_up(\alpha)
	// j: minimizes the decrease of obj value
	//    (if quadratic coefficeint <= 0, replace it with tau)
	//    -y_j*grad(f)_j < -y_i*grad(f)_i, j in I_low(\alpha)

	double gmax       = -INF;
	double gmax2      = -INF;
	int gmaxIdx       = -1;
	int gminIdx       = -1;
	double objDiffMin = INF;

	for (int t = 0; t < m_activeSize; ++t)
	{
		if (y[t] == +1)
		{
			if (!isUpperBound(t))
			{
				if (-G[t] >= gmax)
				{
					gmax    = -G[t];
					gmaxIdx = t;
				}
			}
		}
		else
		{
			if (!isLowerBound(t))
			{
				if (G[t] >= gmax)
				{
					gmax    = G[t];
					gmaxIdx = t;
				}
			}
		}
	}

	const int i      = gmaxIdx;
	const Qfloat* qI = nullptr;
	if (i != -1)
	{ // NULL Q_i not accessed: Gmax=-INF if i=-1
		qI = Q->get_Q(i, m_activeSize);
	}

	for (int j = 0; j < m_activeSize; ++j)
	{
		if (y[j] == +1)
		{
			if (!isLowerBound(j))
			{
				const double gradDiff = gmax + G[j];
				if (G[j] >= gmax2) { gmax2 = G[j]; }
				if (gradDiff > 0)
				{
					double objDiff;
					const double quadCoef = qI[i] + QD[j] - 2.0 * y[i] * qI[j];
					if (quadCoef > 0) { objDiff = -(gradDiff * gradDiff) / quadCoef; }
					else { objDiff = -(gradDiff * gradDiff) / TAU; }

					if (objDiff <= objDiffMin)
					{
						gminIdx    = j;
						objDiffMin = objDiff;
					}
				}
			}
		}
		else
		{
			if (!isUpperBound(j))
			{
				const double gradDiff = gmax - G[j];
				if (-G[j] >= gmax2) { gmax2 = -G[j]; }
				if (gradDiff > 0)
				{
					double objDiff;
					const double quadCoef = qI[i] + QD[j] + 2.0 * y[i] * qI[j];
					if (quadCoef > 0) { objDiff = -(gradDiff * gradDiff) / quadCoef; }
					else { objDiff = -(gradDiff * gradDiff) / TAU; }

					if (objDiff <= objDiffMin)
					{
						gminIdx    = j;
						objDiffMin = objDiff;
					}
				}
			}
		}
	}

	if (gmax + gmax2 < eps) { return 1; }

	outI = gmaxIdx;
	outJ = gminIdx;
	return 0;
}

bool Solver::beShrunk(const int i, const double gmax1, const double gmax2)
{
	if (isUpperBound(i))
	{
		if (y[i] == +1) { return (-G[i] > gmax1); }
		return (-G[i] > gmax2);
	}
	if (isLowerBound(i))
	{
		if (y[i] == +1) { return (G[i] > gmax2); }
		return (G[i] > gmax1);
	}
	return (false);
}

void Solver::doShrinking()
{
	int i;
	double gmax1 = -INF;		// max { -y_i * grad(f)_i | i in I_up(\alpha) }
	double gmax2 = -INF;		// max { y_i * grad(f)_i | i in I_low(\alpha) }

	// find maximal violating pair first
	for (i = 0; i < m_activeSize; ++i)
	{
		if (y[i] == +1)
		{
			if (!isUpperBound(i)) { if (-G[i] >= gmax1) { gmax1 = -G[i]; } }
			if (!isLowerBound(i)) { if (G[i] >= gmax2) { gmax2 = G[i]; } }
		}
		else
		{
			if (!isUpperBound(i)) { if (-G[i] >= gmax2) { gmax2 = -G[i]; } }
			if (!isLowerBound(i)) { if (G[i] >= gmax1) { gmax1 = G[i]; } }
		}
	}

	if (unshrink == false && gmax1 + gmax2 <= eps * 10)
	{
		unshrink = true;
		reconstructGradient();
		m_activeSize = l;
		info("*");
	}

	for (i = 0; i < m_activeSize; ++i)
	{
		if (beShrunk(i, gmax1, gmax2))
		{
			m_activeSize--;
			while (m_activeSize > i)
			{
				if (!beShrunk(m_activeSize, gmax1, gmax2))
				{
					swapIndex(i, m_activeSize);
					break;
				}
				m_activeSize--;
			}
		}
	}
}

double Solver::calculateRho()
{
	double r;
	int nrFree = 0;
	double ub  = INF, lb = -INF, sumFree = 0;
	for (int i = 0; i < m_activeSize; ++i)
	{
		const double yG = y[i] * G[i];

		if (isUpperBound(i))
		{
			if (y[i] == -1) { ub = min(ub, yG); }
			else { lb = max(lb, yG); }
		}
		else if (isLowerBound(i))
		{
			if (y[i] == +1) { ub = min(ub, yG); }
			else { lb = max(lb, yG); }
		}
		else
		{
			++nrFree;
			sumFree += yG;
		}
	}

	if (nrFree > 0) { r = sumFree / nrFree; }
	else { r = (ub + lb) / 2; }

	return r;
}

//
// Solver for nu-svm classification and regression
//
// additional constraint: e^T \alpha = constant
//
class Solver_NU : public Solver
{
public:
	Solver_NU() {}

	void solve(const int l, const QMatrix& q, const double* p, const schar* y, double* alpha, const double cp, const double cn, const double eps,
			   SSolutionInfo* si, const int shrinking) override
	{
		this->si = si;
		Solver::solve(l, q, p, y, alpha, cp, cn, eps, si, shrinking);
	}

private:
	SSolutionInfo* si = nullptr;
	int selectWorkingSet(int& i, int& j) override;
	double calculateRho() override;
	bool be_shrunk(int i, double gmax1, double gmax2, double gmax3, double gmax4);
	void doShrinking() override;
};

// return 1 if already optimal, return 0 otherwise
int Solver_NU::selectWorkingSet(int& i, int& j)
{
	// return i,j such that y_i = y_j and
	// i: maximizes -y_i * grad(f)_i, i in I_up(\alpha)
	// j: minimizes the decrease of obj value
	//    (if quadratic coefficeint <= 0, replace it with tau)
	//    -y_j*grad(f)_j < -y_i*grad(f)_i, j in I_low(\alpha)

	double gmaxp  = -INF;
	double gmaxp2 = -INF;
	int gmaxpIdx  = -1;

	double gmaxn  = -INF;
	double gmaxn2 = -INF;
	int gmaxnIdx  = -1;

	int gminIdx       = -1;
	double objDiffMin = INF;

	for (int t = 0; t < m_activeSize; ++t)
	{
		if (y[t] == +1)
		{
			if (!isUpperBound(t))
			{
				if (-G[t] >= gmaxp)
				{
					gmaxp    = -G[t];
					gmaxpIdx = t;
				}
			}
		}
		else
		{
			if (!isLowerBound(t))
			{
				if (G[t] >= gmaxn)
				{
					gmaxn    = G[t];
					gmaxnIdx = t;
				}
			}
		}
	}
	const int ip      = gmaxpIdx;
	const int in      = gmaxnIdx;
	const Qfloat* qIp = nullptr;
	const Qfloat* qIn = nullptr;
	if (ip != -1) { qIp = Q->get_Q(ip, m_activeSize); }// NULL Q_ip not accessed: Gmaxp=-INF if ip=-1
	if (in != -1) { qIn = Q->get_Q(in, m_activeSize); }

	for (int t = 0; t < m_activeSize; ++t)
	{
		if (y[t] == +1)
		{
			if (!isLowerBound(t))
			{
				const double gradDiff = gmaxp + G[t];
				if (G[t] >= gmaxp2) { gmaxp2 = G[t]; }
				if (gradDiff > 0)
				{
					double objDiff;
					const double quadCoef = qIp[ip] + QD[t] - 2 * qIp[t];
					if (quadCoef > 0) { objDiff = -(gradDiff * gradDiff) / quadCoef; }
					else { objDiff = -(gradDiff * gradDiff) / TAU; }

					if (objDiff <= objDiffMin)
					{
						gminIdx    = t;
						objDiffMin = objDiff;
					}
				}
			}
		}
		else
		{
			if (!isUpperBound(t))
			{
				const double gradDiff = gmaxn - G[t];
				if (-G[t] >= gmaxn2) { gmaxn2 = -G[t]; }
				if (gradDiff > 0)
				{
					double objDiff;
					const double quadCoef = qIn[in] + QD[t] - 2 * qIn[t];
					if (quadCoef > 0) { objDiff = -(gradDiff * gradDiff) / quadCoef; }
					else { objDiff = -(gradDiff * gradDiff) / TAU; }

					if (objDiff <= objDiffMin)
					{
						gminIdx    = t;
						objDiffMin = objDiff;
					}
				}
			}
		}
	}

	if (max(gmaxp + gmaxp2, gmaxn + gmaxn2) < eps) { return 1; }

	if (y[gminIdx] == +1) { i = gmaxpIdx; }
	else { i = gmaxnIdx; }
	j = gminIdx;

	return 0;
}

bool Solver_NU::be_shrunk(const int i, const double gmax1, const double gmax2, const double gmax3, const double gmax4)
{
	if (isUpperBound(i))
	{
		if (y[i] == +1) { return (-G[i] > gmax1); }
		return (-G[i] > gmax4);
	}
	if (isLowerBound(i))
	{
		if (y[i] == +1) { return (G[i] > gmax2); }
		return (G[i] > gmax3);
	}
	return (false);
}

void Solver_NU::doShrinking()
{
	double gmax1 = -INF;	// max { -y_i * grad(f)_i | y_i = +1, i in I_up(\alpha) }
	double gmax2 = -INF;	// max { y_i * grad(f)_i | y_i = +1, i in I_low(\alpha) }
	double gmax3 = -INF;	// max { -y_i * grad(f)_i | y_i = -1, i in I_up(\alpha) }
	double gmax4 = -INF;	// max { y_i * grad(f)_i | y_i = -1, i in I_low(\alpha) }

	// find maximal violating pair first
	int i;
	for (i = 0; i < m_activeSize; ++i)
	{
		if (!isUpperBound(i))
		{
			if (y[i] == +1) { if (-G[i] > gmax1) { gmax1 = -G[i]; } }
			else if (-G[i] > gmax4) { gmax4 = -G[i]; }
		}
		if (!isLowerBound(i))
		{
			if (y[i] == +1) { if (G[i] > gmax2) { gmax2 = G[i]; } }
			else if (G[i] > gmax3) { gmax3 = G[i]; }
		}
	}

	if (unshrink == false && max(gmax1 + gmax2, gmax3 + gmax4) <= eps * 10)
	{
		unshrink = true;
		reconstructGradient();
		m_activeSize = l;
	}

	for (i = 0; i < m_activeSize; ++i)
	{
		if (be_shrunk(i, gmax1, gmax2, gmax3, gmax4))
		{
			m_activeSize--;
			while (m_activeSize > i)
			{
				if (!be_shrunk(m_activeSize, gmax1, gmax2, gmax3, gmax4))
				{
					swapIndex(i, m_activeSize);
					break;
				}
				m_activeSize--;
			}
		}
	}
}

double Solver_NU::calculateRho()
{
	int nrFree1     = 0, nrFree2  = 0;
	double ub1      = INF, ub2    = INF;
	double lb1      = -INF, lb2   = -INF;
	double sumFree1 = 0, sumFree2 = 0;

	for (int i = 0; i < m_activeSize; ++i)
	{
		if (y[i] == +1)
		{
			if (isUpperBound(i)) { lb1 = max(lb1, G[i]); }
			else if (isLowerBound(i)) { ub1 = min(ub1, G[i]); }
			else
			{
				++nrFree1;
				sumFree1 += G[i];
			}
		}
		else
		{
			if (isUpperBound(i)) { lb2 = max(lb2, G[i]); }
			else if (isLowerBound(i)) { ub2 = min(ub2, G[i]); }
			else
			{
				++nrFree2;
				sumFree2 += G[i];
			}
		}
	}

	double r1, r2;
	if (nrFree1 > 0) { r1 = sumFree1 / nrFree1; }
	else { r1 = (ub1 + lb1) / 2; }

	if (nrFree2 > 0) { r2 = sumFree2 / nrFree2; }
	else { r2 = (ub2 + lb2) / 2; }

	si->r = (r1 + r2) / 2;
	return (r1 - r2) / 2;
}

//
// Q matrices for various formulations
//
class SVC_Q : public Kernel
{
public:
	SVC_Q(const svm_problem& prob, const svm_parameter& param, const schar* y_)
		: Kernel(prob.l, prob.x, param)
	{
		clone(y, y_, prob.l);
		cache = new Cache(prob.l, static_cast<long int>(param.cache_size * (1 << 20)));
		QD    = new Qfloat[prob.l];
		for (int i = 0; i < prob.l; ++i) { QD[i] = Qfloat((this->*kernel_function)(i, i)); }
	}

	Qfloat* get_Q(const int i, const int len) const override
	{
		Qfloat* data;
		int start;
		if ((start = cache->get_data(i, &data, len)) < len)
		{
			for (int j = start; j < len; ++j) { data[j] = Qfloat(y[i] * y[j] * (this->*kernel_function)(i, j)); }
		}
		return data;
	}

	Qfloat* get_QD() const override { return QD; }

	void swap_index(const int i, const int j) const override
	{
		cache->swap_index(i, j);
		Kernel::swap_index(i, j);
		swap(y[i], y[j]);
		swap(QD[i], QD[j]);
	}

	~SVC_Q() override
	{
		delete[] y;
		delete cache;
		delete[] QD;
	}

private:
	schar* y;
	Cache* cache;
	Qfloat* QD;
};

class ONE_CLASS_Q : public Kernel
{
public:
	ONE_CLASS_Q(const svm_problem& prob, const svm_parameter& param)
		: Kernel(prob.l, prob.x, param)
	{
		cache = new Cache(prob.l, static_cast<long int>(param.cache_size * (1 << 20)));
		QD    = new Qfloat[prob.l];
		for (int i = 0; i < prob.l; ++i) { QD[i] = Qfloat((this->*kernel_function)(i, i)); }
	}

	Qfloat* get_Q(const int i, const int len) const override
	{
		Qfloat* data;
		int start;
		if ((start = cache->get_data(i, &data, len)) < len) { for (int j = start; j < len; ++j) { data[j] = Qfloat((this->*kernel_function)(i, j)); } }
		return data;
	}

	Qfloat* get_QD() const override { return QD; }

	void swap_index(const int i, const int j) const override
	{
		cache->swap_index(i, j);
		Kernel::swap_index(i, j);
		swap(QD[i], QD[j]);
	}

	~ONE_CLASS_Q() override
	{
		delete cache;
		delete[] QD;
	}

private:
	Cache* cache;
	Qfloat* QD;
};

class SVR_Q : public Kernel
{
public:
	SVR_Q(const svm_problem& prob, const svm_parameter& param)
		: Kernel(prob.l, prob.x, param)
	{
		l     = prob.l;
		cache = new Cache(l, static_cast<long int>(param.cache_size * (1 << 20)));
		QD    = new Qfloat[2 * l];
		sign  = new schar[2 * l];
		index = new int[2 * l];
		for (int k = 0; k < l; ++k)
		{
			sign[k]      = 1;
			sign[k + l]  = -1;
			index[k]     = k;
			index[k + l] = k;
			QD[k]        = Qfloat((this->*kernel_function)(k, k));
			QD[k + l]    = QD[k];
		}
		buffer[0]   = new Qfloat[2 * l];
		buffer[1]   = new Qfloat[2 * l];
		next_buffer = 0;
	}

	void swap_index(const int i, const int j) const override
	{
		swap(sign[i], sign[j]);
		swap(index[i], index[j]);
		swap(QD[i], QD[j]);
	}

	Qfloat* get_Q(const int i, const int len) const override
	{
		Qfloat* data;
		const int realI = index[i];
		if (cache->get_data(realI, &data, l) < l) { for (int j = 0; j < l; ++j) { data[j] = Qfloat((this->*kernel_function)(realI, j)); } }

		// reorder and copy
		Qfloat* buf    = buffer[next_buffer];
		next_buffer    = 1 - next_buffer;
		const schar si = sign[i];
		for (int j = 0; j < len; ++j) { buf[j] = Qfloat(si) * Qfloat(sign[j]) * data[index[j]]; }
		return buf;
	}

	Qfloat* get_QD() const override { return QD; }

	~SVR_Q() override
	{
		delete cache;
		delete[] sign;
		delete[] index;
		delete[] buffer[0];
		delete[] buffer[1];
		delete[] QD;
	}

private:
	int l;
	Cache* cache;
	schar* sign;
	int* index;
	mutable int next_buffer;
	Qfloat* buffer[2];
	Qfloat* QD;
};

//
// construct and solve various formulations
//
static void solve_c_svc(const svm_problem* prob, const svm_parameter* param, double* alpha, Solver::SSolutionInfo* si, const double Cp, const double Cn)
{
	const int l       = prob->l;
	double* minusOnes = new double[l];
	schar* y          = new schar[l];

	int i;

	for (i = 0; i < l; ++i)
	{
		alpha[i]     = 0;
		minusOnes[i] = -1;
		if (prob->y[i] > 0) { y[i] = +1; }
		else { y[i] = -1; }
	}

	Solver s;
	s.solve(l, SVC_Q(*prob, *param, y), minusOnes, y, alpha, Cp, Cn, param->eps, si, param->shrinking);

	double sumAlpha = 0;
	for (i = 0; i < l; ++i) { sumAlpha += alpha[i]; }

	if (Cp == Cn) { info("nu = %f\n", sumAlpha / (Cp * prob->l)); }

	for (i = 0; i < l; ++i) { alpha[i] *= y[i]; }

	delete[] minusOnes;
	delete[] y;
}

static void solve_nu_svc(const svm_problem* prob, const svm_parameter* param, double* alpha, Solver::SSolutionInfo* si)
{
	int i;
	const int l     = prob->l;
	const double nu = param->nu;

	schar* y = new schar[l];

	for (i = 0; i < l; ++i)
	{
		if (prob->y[i] > 0) { y[i] = +1; }
		else { y[i] = -1; }
	}

	double sumPos = nu * l / 2;
	double sumNeg = nu * l / 2;

	for (i = 0; i < l; ++i)
	{
		if (y[i] == +1)
		{
			alpha[i] = min(1.0, sumPos);
			sumPos -= alpha[i];
		}
		else
		{
			alpha[i] = min(1.0, sumNeg);
			sumNeg -= alpha[i];
		}
	}

	double* zeros = new double[l];

	for (i = 0; i < l; ++i) { zeros[i] = 0; }

	Solver_NU s;
	s.solve(l, SVC_Q(*prob, *param, y), zeros, y, alpha, 1.0, 1.0, param->eps, si, param->shrinking);
	const double r = si->r;

	info("C = %f\n", 1 / r);

	for (i = 0; i < l; ++i) { alpha[i] *= y[i] / r; }

	si->rho /= r;
	si->obj /= (r * r);
	si->upperBoundP = 1 / r;
	si->upperBoundN = 1 / r;

	delete[] y;
	delete[] zeros;
}

static void solve_one_class(const svm_problem* prob, const svm_parameter* param, double* alpha, Solver::SSolutionInfo* si)
{
	const int l   = prob->l;
	double* zeros = new double[l];
	schar* ones   = new schar[l];
	int i;

	const int n = int(param->nu * prob->l);	// # of alpha's at upper bound

	for (i = 0; i < n; ++i) { alpha[i] = 1; }
	if (n < prob->l) { alpha[n] = param->nu * prob->l - n; }
	for (i = n + 1; i < l; ++i) { alpha[i] = 0; }

	for (i = 0; i < l; ++i)
	{
		zeros[i] = 0;
		ones[i]  = 1;
	}

	Solver s;
	s.solve(l, ONE_CLASS_Q(*prob, *param), zeros, ones,
			alpha, 1.0, 1.0, param->eps, si, param->shrinking);

	delete[] zeros;
	delete[] ones;
}

static void solve_epsilon_svr(const svm_problem* prob, const svm_parameter* param, double* alpha, Solver::SSolutionInfo* si)
{
	const int l        = prob->l;
	double* alpha2     = new double[2 * l];
	double* linearTerm = new double[2 * l];
	schar* y           = new schar[2 * l];
	int i;

	for (i = 0; i < l; ++i)
	{
		alpha2[i]     = 0;
		linearTerm[i] = param->p - prob->y[i];
		y[i]          = 1;

		alpha2[i + l]     = 0;
		linearTerm[i + l] = param->p + prob->y[i];
		y[i + l]          = -1;
	}

	Solver s;
	s.solve(2 * l, SVR_Q(*prob, *param), linearTerm, y,
			alpha2, param->C, param->C, param->eps, si, param->shrinking);

	double sumAlpha = 0;
	for (i = 0; i < l; ++i)
	{
		alpha[i] = alpha2[i] - alpha2[i + l];
		sumAlpha += fabs(alpha[i]);
	}
	info("nu = %f\n", sumAlpha / (param->C * l));

	delete[] alpha2;
	delete[] linearTerm;
	delete[] y;
}

static void solve_nu_svr(const svm_problem* prob, const svm_parameter* param, double* alpha, Solver::SSolutionInfo* si)
{
	const int l        = prob->l;
	const double C     = param->C;
	double* alpha2     = new double[2 * l];
	double* linearTerm = new double[2 * l];
	schar* y           = new schar[2 * l];
	int i;

	double sum = C * param->nu * l / 2;
	for (i = 0; i < l; ++i)
	{
		alpha2[i] = alpha2[i + l] = min(sum, C);
		sum -= alpha2[i];

		linearTerm[i] = - prob->y[i];
		y[i]          = 1;

		linearTerm[i + l] = prob->y[i];
		y[i + l]          = -1;
	}

	Solver_NU s;
	s.solve(2 * l, SVR_Q(*prob, *param), linearTerm, y,
			alpha2, C, C, param->eps, si, param->shrinking);

	info("epsilon = %f\n", -si->r);

	for (i = 0; i < l; ++i) { alpha[i] = alpha2[i] - alpha2[i + l]; }

	delete[] alpha2;
	delete[] linearTerm;
	delete[] y;
}

//
// decision_function
//
struct decision_function
{
	double* alpha;
	double rho;
};

static decision_function svm_train_one(const svm_problem* prob, const svm_parameter* param, const double Cp, const double Cn)
{
	double* alpha = Malloc(double, prob->l);
	Solver::SSolutionInfo si;
	switch (param->svm_type)
	{
		case C_SVC:
			solve_c_svc(prob, param, alpha, &si, Cp, Cn);
			break;
		case NU_SVC:
			solve_nu_svc(prob, param, alpha, &si);
			break;
		case ONE_CLASS:
			solve_one_class(prob, param, alpha, &si);
			break;
		case EPSILON_SVR:
			solve_epsilon_svr(prob, param, alpha, &si);
			break;
		case NU_SVR:
			solve_nu_svr(prob, param, alpha, &si);
			break;
		default: break;
	}

	info("obj = %f, rho = %f\n", si.obj, si.rho);

	// output SVs

	int nSV  = 0;
	int nBSV = 0;
	for (int i = 0; i < prob->l; ++i)
	{
		if (fabs(alpha[i]) > 0)
		{
			++nSV;
			if (prob->y[i] > 0) { if (fabs(alpha[i]) >= si.upperBoundP) { ++nBSV; } }
			else { if (fabs(alpha[i]) >= si.upperBoundN) { ++nBSV; } }
		}
	}

	info("nSV = %d, nBSV = %d\n", nSV, nBSV);

	decision_function f;
	f.alpha = alpha;
	f.rho   = si.rho;
	return f;
}

// Platt's binary SVM Probablistic Output: an improvement from Lin et al.
static void sigmoid_train(const int l, const double* decValues, const double* labels, double& a, double& b)
{
	double prior1 = 0, prior0 = 0;
	int i;

	for (i = 0; i < l; ++i)
	{
		if (labels[i] > 0) { prior1 += 1; }
		else { prior0 += 1; }
	}

	const int maxIter     = 100;	// Maximal number of iterations
	const double minStep  = 1e-10;	// Minimal step taken in line search
	const double sigma    = 1e-12;	// For numerically strict PD of Hessian
	const double eps      = 1e-5;
	const double hiTarget = (prior1 + 1.0) / (prior1 + 2.0);
	const double loTarget = 1 / (prior0 + 2.0);
	double* t             = Malloc(double, l);
	double fApB, p, q;
	int iter;

	// Initial Point and Initial Fun Value
	a           = 0.0;
	b           = log((prior0 + 1.0) / (prior1 + 1.0));
	double fval = 0.0;

	for (i = 0; i < l; ++i)
	{
		if (labels[i] > 0) { t[i] = hiTarget; }
		else { t[i] = loTarget; }
		fApB = decValues[i] * a + b;
		if (fApB >= 0) { fval += t[i] * fApB + log(1 + exp(-fApB)); }
		else { fval += (t[i] - 1) * fApB + log(1 + exp(fApB)); }
	}
	for (iter = 0; iter < maxIter; ++iter)
	{
		// Update Gradient and Hessian (use H' = H + sigma I)
		double h11 = sigma; // numerically ensures strict PD
		double h22 = sigma;
		double h21 = 0.0;
		double g1  = 0.0;
		double g2  = 0.0;
		for (i = 0; i < l; ++i)
		{
			fApB = decValues[i] * a + b;
			if (fApB >= 0)
			{
				p = exp(-fApB) / (1.0 + exp(-fApB));
				q = 1.0 / (1.0 + exp(-fApB));
			}
			else
			{
				p = 1.0 / (1.0 + exp(fApB));
				q = exp(fApB) / (1.0 + exp(fApB));
			}
			const double d2 = p * q;
			h11 += decValues[i] * decValues[i] * d2;
			h22 += d2;
			h21 += decValues[i] * d2;
			const double d1 = t[i] - p;
			g1 += decValues[i] * d1;
			g2 += d1;
		}

		// Stopping Criteria
		if (fabs(g1) < eps && fabs(g2) < eps) { break; }

		// Finding Newton direction: -inv(H') * g
		const double det = h11 * h22 - h21 * h21;
		const double dA  = -(h22 * g1 - h21 * g2) / det;
		const double dB  = -(-h21 * g1 + h11 * g2) / det;
		const double gd  = g1 * dA + g2 * dB;


		double stepsize = 1;		// Line Search
		while (stepsize >= minStep)
		{
			const double newA = a + stepsize * dA;
			const double newB = b + stepsize * dB;

			// New function value
			double newf = 0.0;
			for (i = 0; i < l; ++i)
			{
				fApB = decValues[i] * newA + newB;
				if (fApB >= 0) { newf += t[i] * fApB + log(1 + exp(-fApB)); }
				else { newf += (t[i] - 1) * fApB + log(1 + exp(fApB)); }
			}
			// Check sufficient decrease
			if (newf < fval + 0.0001 * stepsize * gd)
			{
				a    = newA;
				b    = newB;
				fval = newf;
				break;
			}
			stepsize = stepsize / 2.0;
		}

		if (stepsize < minStep)
		{
			info("Line search fails in two-class probability estimates\n");
			break;
		}
	}

	if (iter >= maxIter) { info("Reaching maximal iterations in two-class probability estimates\n"); }
	free(t);
}

static double sigmoid_predict(const double decisionValue, const double a, const double b)
{
	const double fApB = decisionValue * a + b;
	if (fApB >= 0) { return exp(-fApB) / (1.0 + exp(-fApB)); }
	return 1.0 / (1 + exp(fApB));
}

// Method 2 from the multiclass_prob paper by Wu, Lin, and Weng
static void multiclass_probability(const int k, double** r, double* p)
{
	int t, j;
	double** Q       = Malloc(double *, k);
	double* Qp       = Malloc(double, k);
	const double eps = 0.005 / k;

	for (t = 0; t < k; ++t)
	{
		p[t]    = 1.0 / k;  // Valid if k = 1
		Q[t]    = Malloc(double, k);
		Q[t][t] = 0;
		for (j = 0; j < t; ++j)
		{
			Q[t][t] += r[j][t] * r[j][t];
			Q[t][j] = Q[j][t];
		}
		for (j = t + 1; j < k; ++j)
		{
			Q[t][t] += r[j][t] * r[j][t];
			Q[t][j] = -r[j][t] * r[t][j];
		}
	}

	int iter          = 0;
	const int maxIter = max(100, k);
	for (; iter < maxIter; ++iter)
	{
		// stopping condition, recalculate QP,pQP for numerical accuracy
		double pQp = 0;
		for (t = 0; t < k; ++t)
		{
			Qp[t] = 0;
			for (j = 0; j < k; ++j) { Qp[t] += Q[t][j] * p[j]; }
			pQp += p[t] * Qp[t];
		}
		double maxError = 0;
		for (t = 0; t < k; ++t)
		{
			const double error = fabs(Qp[t] - pQp);
			if (error > maxError) { maxError = error; }
		}
		if (maxError < eps) { break; }

		for (t = 0; t < k; ++t)
		{
			const double diff = (-Qp[t] + pQp) / Q[t][t];
			p[t] += diff;
			pQp = (pQp + diff * (diff * Q[t][t] + 2 * Qp[t])) / (1 + diff) / (1 + diff);
			for (j = 0; j < k; ++j)
			{
				Qp[j] = (Qp[j] + diff * Q[t][j]) / (1 + diff);
				p[j] /= (1 + diff);
			}
		}
	}
	if (iter >= maxIter) { info("Exceeds max_iter in multiclass_prob\n"); }
	for (t = 0; t < k; ++t) { free(Q[t]); }
	free(Q);
	free(Qp);
}

// Cross-validation decision values for probability estimates
static void svm_binary_svc_probability(const svm_problem* prob, const svm_parameter* param, const double cp, const double cn, double& probA, double& probB)
{
	int i;
	const int nrFold  = 5;
	int* perm         = Malloc(int, prob->l);
	double* decValues = Malloc(double, prob->l);

	// random shuffle
	for (i = 0; i < prob->l; ++i) { perm[i] = i; }
	for (i = 0; i < prob->l; ++i)
	{
		const int j = i + rand() % (prob->l - i);
		swap(perm[i], perm[j]);
	}
	for (i = 0; i < nrFold; ++i)
	{
		const int begin = i * prob->l / nrFold;
		const int end   = (i + 1) * prob->l / nrFold;
		int j;
		struct svm_problem subprob;

		subprob.l = prob->l - (end - begin);
		subprob.x = Malloc(struct svm_node*, subprob.l);
		subprob.y = Malloc(double, subprob.l);

		int k = 0;
		for (j = 0; j < begin; ++j)
		{
			subprob.x[k] = prob->x[perm[j]];
			subprob.y[k] = prob->y[perm[j]];
			++k;
		}
		for (j = end; j < prob->l; ++j)
		{
			subprob.x[k] = prob->x[perm[j]];
			subprob.y[k] = prob->y[perm[j]];
			++k;
		}
		int pCount = 0, nCount = 0;
		for (j = 0; j < k; ++j)
		{
			if (subprob.y[j] > 0) { pCount++; }
			else { nCount++; }
		}

		if (pCount == 0 && nCount == 0) { for (j = begin; j < end; ++j) { decValues[perm[j]] = 0; } }
		else if (pCount > 0 && nCount == 0) { for (j = begin; j < end; ++j) { decValues[perm[j]] = 1; } }
		else if (pCount == 0 && nCount > 0) { for (j = begin; j < end; ++j) { decValues[perm[j]] = -1; } }
		else
		{
			svm_parameter subparam     = *param;
			subparam.probability       = 0;
			subparam.C                 = 1.0;
			subparam.nr_weight         = 2;
			subparam.weight_label      = Malloc(int, 2);
			subparam.weight            = Malloc(double, 2);
			subparam.weight_label[0]   = +1;
			subparam.weight_label[1]   = -1;
			subparam.weight[0]         = cp;
			subparam.weight[1]         = cn;
			struct svm_model* submodel = svm_train(&subprob, &subparam);
			for (j = begin; j < end; ++j)
			{
				svm_predict_values(submodel, prob->x[perm[j]], &(decValues[perm[j]]));
				// ensure +1 -1 order; reason not using CV subroutine
				decValues[perm[j]] *= submodel->label[0];
			}
			svm_destroy_model(submodel);
			svm_destroy_param(&subparam);
		}
		free(subprob.x);
		free(subprob.y);
	}
	sigmoid_train(prob->l, decValues, prob->y, probA, probB);
	free(decValues);
	free(perm);
}

// Return parameter of a Laplace distribution 
static double svm_svr_probability(const svm_problem* prob, const svm_parameter* param)
{
	int i;
	const int nrFold = 5;
	double* ymv      = Malloc(double, prob->l);
	double mae       = 0;

	svm_parameter newparam = *param;
	newparam.probability   = 0;
	svm_cross_validation(prob, &newparam, nrFold, ymv);
	for (i = 0; i < prob->l; ++i)
	{
		ymv[i] = prob->y[i] - ymv[i];
		mae += fabs(ymv[i]);
	}
	mae /= prob->l;
	const double std = sqrt(2 * mae * mae);
	int count        = 0;
	mae              = 0;
	for (i = 0; i < prob->l; ++i)
	{
		if (fabs(ymv[i]) > 5 * std) { count = count + 1; }
		else { mae += fabs(ymv[i]); }
	}
	mae /= (prob->l - count);
	info("Prob. model for test data: target value = predicted value + z,\nz: Laplace distribution e^(-|z|/sigma)/(2sigma),sigma= %g\n", mae);
	free(ymv);
	return mae;
}


// label: label name, start: begin of each class, count: #data of classes, perm: indices to the original data
// perm, length l, must be allocated before calling this subroutine
static void svm_group_classes(const svm_problem* prob, int* nrClassRet, int** labelRet, int** startRet, int** countRet, int* perm)
{
	const int l    = prob->l;
	int maxNrClass = 16;
	int nrClass    = 0;
	int* label     = Malloc(int, maxNrClass);
	int* count     = Malloc(int, maxNrClass);
	int* dataLabel = Malloc(int, l);
	int i;

	for (i = 0; i < l; ++i)
	{
		const int thisLabel = int(prob->y[i]);
		int j;
		for (j = 0; j < nrClass; ++j)
		{
			if (thisLabel == label[j])
			{
				++count[j];
				break;
			}
		}
		dataLabel[i] = j;
		if (j == nrClass)
		{
			if (nrClass == maxNrClass)
			{
				maxNrClass *= 2;
				label = (int*)realloc(label, maxNrClass * sizeof(int));
				count = (int*)realloc(count, maxNrClass * sizeof(int));
			}
			label[nrClass] = thisLabel;
			count[nrClass] = 1;
			++nrClass;
		}
	}

	int* start = Malloc(int, nrClass);
	start[0]   = 0;
	for (i = 1; i < nrClass; ++i) { start[i] = start[i - 1] + count[i - 1]; }
	for (i = 0; i < l; ++i)
	{
		perm[start[dataLabel[i]]] = i;
		++start[dataLabel[i]];
	}
	start[0] = 0;
	for (i = 1; i < nrClass; ++i) { start[i] = start[i - 1] + count[i - 1]; }

	*nrClassRet = nrClass;
	*labelRet   = label;
	*startRet   = start;
	*countRet   = count;
	free(dataLabel);
}

//
// Interface functions
//
svm_model* svm_train(const svm_problem* prob, const svm_parameter* param)
{
	svm_model* model = Malloc(svm_model, 1);
	model->param     = *param;
	model->free_sv   = 0;	// XXX

	if (param->svm_type == ONE_CLASS ||
		param->svm_type == EPSILON_SVR ||
		param->svm_type == NU_SVR)
	{
		// regression or one-class-svm
		model->nr_class = 2;
		model->label    = nullptr;
		model->nSV      = nullptr;
		model->probA    = nullptr;
		model->probB    = nullptr;
		model->sv_coef  = Malloc(double *, 1);

		if (param->probability &&
			(param->svm_type == EPSILON_SVR ||
			 param->svm_type == NU_SVR))
		{
			model->probA    = Malloc(double, 1);
			model->probA[0] = svm_svr_probability(prob, param);
		}

		decision_function f = svm_train_one(prob, param, 0, 0);
		model->rho          = Malloc(double, 1);
		model->rho[0]       = f.rho;

		int nSV = 0;
		int i;
		for (i = 0; i < prob->l; ++i) { if (fabs(f.alpha[i]) > 0) { ++nSV; } }
		model->l          = nSV;
		model->SV         = Malloc(svm_node *, nSV);
		model->sv_coef[0] = Malloc(double, nSV);
		int j             = 0;
		for (i = 0; i < prob->l; ++i)
		{
			if (fabs(f.alpha[i]) > 0)
			{
				model->SV[j]         = prob->x[i];
				model->sv_coef[0][j] = f.alpha[i];
				++j;
			}
		}

		free(f.alpha);
	}
	else
	{
		// classification
		int l = prob->l;
		int nrClass;
		int* label = nullptr;
		int* start = nullptr;
		int* count = nullptr;
		int* perm  = Malloc(int, l);

		// group training data of the same class
		svm_group_classes(prob, &nrClass, &label, &start, &count, perm);
		svm_node** x = Malloc(svm_node *, l);
		int i;
		for (i = 0; i < l; ++i) { x[i] = prob->x[perm[i]]; }

		// calculate weighted C

		double* weightedC = Malloc(double, nrClass);
		for (i = 0; i < nrClass; ++i) { weightedC[i] = param->C; }
		for (i = 0; i < param->nr_weight; ++i)
		{
			int j;
			for (j = 0; j < nrClass; ++j) { if (param->weight_label[i] == label[j]) { break; } }
			if (j == nrClass) { fprintf(stderr, "warning: class label %d specified in weight is not found\n", param->weight_label[i]); }
			else { weightedC[j] *= param->weight[i]; }
		}

		// train k*(k-1)/2 models

		bool* nonzero = Malloc(bool, l);
		for (i = 0; i < l; ++i) { nonzero[i] = false; }
		decision_function* f = Malloc(decision_function, nrClass*(nrClass-1)/2);

		double *probA = nullptr, *probB = nullptr;
		if (param->probability)
		{
			probA = Malloc(double, nrClass*(nrClass-1)/2);
			probB = Malloc(double, nrClass*(nrClass-1)/2);
		}

		int p = 0;
		for (i = 0; i < nrClass; ++i)
		{
			for (int j = i + 1; j < nrClass; ++j)
			{
				svm_problem subProb;
				int si    = start[i], sj = start[j];
				int ci    = count[i], cj = count[j];
				subProb.l = ci + cj;
				subProb.x = Malloc(svm_node *, subProb.l);
				subProb.y = Malloc(double, subProb.l);
				int k;
				for (k = 0; k < ci; ++k)
				{
					subProb.x[k] = x[si + k];
					subProb.y[k] = +1;
				}
				for (k = 0; k < cj; ++k)
				{
					subProb.x[ci + k] = x[sj + k];
					subProb.y[ci + k] = -1;
				}

				if (param->probability) { svm_binary_svc_probability(&subProb, param, weightedC[i], weightedC[j], probA[p], probB[p]); }

				f[p] = svm_train_one(&subProb, param, weightedC[i], weightedC[j]);
				for (k = 0; k < ci; ++k) { if (!nonzero[si + k] && fabs(f[p].alpha[k]) > 0) { nonzero[si + k] = true; } }
				for (k = 0; k < cj; ++k) { if (!nonzero[sj + k] && fabs(f[p].alpha[ci + k]) > 0) { nonzero[sj + k] = true; } }
				free(subProb.x);
				free(subProb.y);
				++p;
			}
		}

		// build output

		model->nr_class = nrClass;

		model->label = Malloc(int, nrClass);
		for (i = 0; i < nrClass; ++i) { model->label[i] = label[i]; }

		model->rho = Malloc(double, nrClass*(nrClass-1)/2);
		for (i = 0; i < nrClass * (nrClass - 1) / 2; ++i) { model->rho[i] = f[i].rho; }

		if (param->probability)
		{
			model->probA = Malloc(double, nrClass*(nrClass-1)/2);
			model->probB = Malloc(double, nrClass*(nrClass-1)/2);
			for (i = 0; i < nrClass * (nrClass - 1) / 2; ++i)
			{
				model->probA[i] = probA[i];
				model->probB[i] = probB[i];
			}
		}
		else
		{
			model->probA = nullptr;
			model->probB = nullptr;
		}

		int totalSV  = 0;
		int* nzCount = Malloc(int, nrClass);
		model->nSV   = Malloc(int, nrClass);
		for (i = 0; i < nrClass; ++i)
		{
			int nSV = 0;
			for (int j = 0; j < count[i]; ++j)
			{
				if (nonzero[start[i] + j])
				{
					++nSV;
					++totalSV;
				}
			}
			model->nSV[i] = nSV;
			nzCount[i]    = nSV;
		}

		info("Total nSV = %d\n", totalSV);

		model->l  = totalSV;
		model->SV = Malloc(svm_node *, totalSV);
		p         = 0;
		for (i = 0; i < l; ++i) { if (nonzero[i]) { model->SV[p++] = x[i]; } }

		int* nzStart = Malloc(int, nrClass);
		nzStart[0]   = 0;
		for (i = 1; i < nrClass; ++i) { nzStart[i] = nzStart[i - 1] + nzCount[i - 1]; }

		model->sv_coef = Malloc(double *, nrClass-1);
		for (i = 0; i < nrClass - 1; ++i) { model->sv_coef[i] = Malloc(double, totalSV); }

		p = 0;
		for (i = 0; i < nrClass; ++i)
		{
			for (int j = i + 1; j < nrClass; ++j)
			{
				// classifier (i,j): coefficients with
				// i are in sv_coef[j-1][nz_start[i]...],
				// j are in sv_coef[i][nz_start[j]...]

				int si = start[i];
				int sj = start[j];
				int ci = count[i];
				int cj = count[j];

				int q = nzStart[i];
				int k;
				for (k = 0; k < ci; ++k) { if (nonzero[si + k]) { model->sv_coef[j - 1][q++] = f[p].alpha[k]; } }
				q = nzStart[j];
				for (k = 0; k < cj; ++k) { if (nonzero[sj + k]) { model->sv_coef[i][q++] = f[p].alpha[ci + k]; } }
				++p;
			}
		}

		free(label);
		free(probA);
		free(probB);
		free(count);
		free(perm);
		free(start);
		free(x);
		free(weightedC);
		free(nonzero);
		for (i = 0; i < nrClass * (nrClass - 1) / 2; ++i) { free(f[i].alpha); }
		free(f);
		free(nzCount);
		free(nzStart);
	}
	return model;
}

// Stratified cross validation
void svm_cross_validation(const svm_problem* prob, const svm_parameter* param, const int nrFold, double* target)
{
	int i;
	int* foldStart = Malloc(int, nrFold+1);
	const int l    = prob->l;
	int* perm      = Malloc(int, l);
	int nrClass;

	// stratified cv may not give leave-one-out rate
	// Each class to l folds -> some folds may have zero elements
	if ((param->svm_type == C_SVC ||
		 param->svm_type == NU_SVC) && nrFold < l)
	{
		int* start = nullptr;
		int* label = nullptr;
		int* count = nullptr;
		svm_group_classes(prob, &nrClass, &label, &start, &count, perm);

		// random shuffle and then data grouped by fold using the array perm
		int* foldCount = Malloc(int, nrFold);
		int c;
		int* index = Malloc(int, l);
		for (i = 0; i < l; ++i) { index[i] = perm[i]; }
		for (c = 0; c < nrClass; ++c)
		{
			for (i = 0; i < count[c]; ++i)
			{
				const int j = i + rand() % (count[c] - i);
				swap(index[start[c] + j], index[start[c] + i]);
			}
		}
		for (i = 0; i < nrFold; ++i)
		{
			foldCount[i] = 0;
			for (c = 0; c < nrClass; ++c) { foldCount[i] += (i + 1) * count[c] / nrFold - i * count[c] / nrFold; }
		}
		foldStart[0] = 0;
		for (i = 1; i <= nrFold; ++i) { foldStart[i] = foldStart[i - 1] + foldCount[i - 1]; }
		for (c = 0; c < nrClass; ++c)
		{
			for (i = 0; i < nrFold; ++i)
			{
				const int begin = start[c] + i * count[c] / nrFold;
				const int end   = start[c] + (i + 1) * count[c] / nrFold;
				for (int j = begin; j < end; ++j)
				{
					perm[foldStart[i]] = index[j];
					foldStart[i]++;
				}
			}
		}
		foldStart[0] = 0;
		for (i = 1; i <= nrFold; ++i) { foldStart[i] = foldStart[i - 1] + foldCount[i - 1]; }
		free(start);
		free(label);
		free(count);
		free(index);
		free(foldCount);
	}
	else
	{
		for (i = 0; i < l; ++i) { perm[i] = i; }
		for (i = 0; i < l; ++i)
		{
			const int j = i + rand() % (l - i);
			swap(perm[i], perm[j]);
		}
		for (i = 0; i <= nrFold; ++i) { foldStart[i] = i * l / nrFold; }
	}

	for (i = 0; i < nrFold; ++i)
	{
		const int begin = foldStart[i];
		const int end   = foldStart[i + 1];
		int j;
		struct svm_problem subprob;

		subprob.l = l - (end - begin);
		subprob.x = Malloc(struct svm_node*, subprob.l);
		subprob.y = Malloc(double, subprob.l);

		int k = 0;
		for (j = 0; j < begin; ++j)
		{
			subprob.x[k] = prob->x[perm[j]];
			subprob.y[k] = prob->y[perm[j]];
			++k;
		}
		for (j = end; j < l; ++j)
		{
			subprob.x[k] = prob->x[perm[j]];
			subprob.y[k] = prob->y[perm[j]];
			++k;
		}
		struct svm_model* submodel = svm_train(&subprob, param);
		if (param->probability &&
			(param->svm_type == C_SVC || param->svm_type == NU_SVC))
		{
			double* probEstimates = Malloc(double, svm_get_nr_class(submodel));
			for (j = begin; j < end; ++j) { target[perm[j]] = svm_predict_probability(submodel, prob->x[perm[j]], probEstimates); }
			free(probEstimates);
		}
		else { for (j = begin; j < end; ++j) { target[perm[j]] = svm_predict(submodel, prob->x[perm[j]]); } }
		svm_destroy_model(submodel);
		free(subprob.x);
		free(subprob.y);
	}
	free(foldStart);
	free(perm);
}


int svm_get_svm_type(const svm_model* model) { return model->param.svm_type; }
int svm_get_nr_class(const svm_model* model) { return model->nr_class; }

void svm_get_labels(const svm_model* model, int* label)
{
	if (model->label != nullptr) { for (int i = 0; i < model->nr_class; ++i) { label[i] = model->label[i]; } }
}

double svm_get_svr_probability(const svm_model* model)
{
	if ((model->param.svm_type == EPSILON_SVR || model->param.svm_type == NU_SVR) && model->probA != nullptr) { return model->probA[0]; }

	fprintf(stderr, "Model doesn't contain information for SVR probability inference\n");
	return 0;
}

double svm_predict_values(const svm_model* model, const svm_node* x, double* decValues)
{
	if (model->param.svm_type == ONE_CLASS ||
		model->param.svm_type == EPSILON_SVR ||
		model->param.svm_type == NU_SVR)
	{
		double* svCoef = model->sv_coef[0];
		double sum     = 0;
		for (int i = 0; i < model->l; ++i) { sum += svCoef[i] * Kernel::k_function(x, model->SV[i], model->param); }
		sum -= model->rho[0];
		*decValues = sum;

		if (model->param.svm_type == ONE_CLASS) { return (sum > 0) ? 1 : -1; }
		return sum;
	}
	const int nr_class = model->nr_class;
	const int l        = model->l;

	//std::cout<<"svm_predict_value: kvalues"<<std::endl;
	//std::cout<<l<<" "<<sizeof(model->SV[i])/sizeof(model->SV[i][0])<<"\n";

	/*const svm_node* y=x;
	while(y->index !=-1)
	{
		std::cout<<y->index << ";" << y->value<<" ";
		y++;
	}
	std::cout<<std::endl;*/
	/*for (int i=0;i<l;i++)
	{
		svm_node* j=model->SV[i];
		while(j->index!=-1)
		{
			std::cout<<j->index << ":" << j->value<<" ";
			j++;
		}
	}
	std::cout<<"\n";*/
	double* kvalue = Malloc(double, l);
	//std::cout<<"kvalue:";
	//std::cout<<l<<",";
	//std::cout<<kvalue;
	//std::cout<<std::endl;
	for (int i = 0; i < l; ++i)
	{
		//std::cout<<model->SV[i]->index<<" "<<model->SV[i]->value<<std::endl;
		/*std::cout<<"<"<<i<<std::endl;
		std::cout<<"; x="<<x<<std::endl;
		std::cout<<" SV="<<model->SV[i]<<std::endl;
		std::cout<<" param="<<&model->param<<std::endl;
		std::cout<<">"<<std::endl;*/
		kvalue[i] = Kernel::k_function(x, model->SV[i], model->param);
		/*std::cout<<","<<std::endl;
		std::cout<<kvalue[i]<<" "<<std::endl;*/
	}
	//std::cout<<std::endl;
	//std::cout<<"svm_predict_value: start"<<std::endl;
	int* start = Malloc(int, nr_class);
	start[0]   = 0;
	for (int i = 1; i < nr_class; ++i) { start[i] = start[i - 1] + model->nSV[i - 1]; }

	//std::cout<<"svm_predict_value: vote"<<std::endl;
	int* vote = Malloc(int, nr_class);
	for (int i = 0; i < nr_class; ++i) { vote[i] = 0; }

	int p = 0;
	//std::cout<<"svm_predict_value: predict vote"<<std::endl;
	for (int i = 0; i < nr_class; ++i)
	{
		for (int j = i + 1; j < nr_class; ++j)
		{
			double sum   = 0;
			const int si = start[i];
			const int sj = start[j];
			const int ci = model->nSV[i];
			const int cj = model->nSV[j];

			int k;
			double* coef1 = model->sv_coef[j - 1];
			double* coef2 = model->sv_coef[i];
			for (k = 0; k < ci; ++k) { sum += coef1[si + k] * kvalue[si + k]; }
			for (k = 0; k < cj; ++k) { sum += coef2[sj + k] * kvalue[sj + k]; }
			sum -= model->rho[p];
			decValues[p] = sum;
			//std::cout<<"i="<<i<<" j="<<j<<" dec_value="<<decValues[p]<<" ";
			if (decValues[p] > 0) { ++vote[i]; }
			else { ++vote[j]; }
			p++;
		}
	}
	//std::cout<<std::endl;

	int voteMaxIdx = 0;
	//std::cout<<"svm_predict_value: vote max"<<std::endl;
	for (int i = 1; i < nr_class; ++i) { if (vote[i] > vote[voteMaxIdx]) { voteMaxIdx = i; } }
	//std::cout<<"svm_predict_value: free all"<<std::endl;
	free(kvalue);
	free(start);
	free(vote);
	return model->label[voteMaxIdx];
}

double svm_predict(const svm_model* model, const svm_node* x)
{
	const int nrClass = model->nr_class;
	double* decValues;
	if (model->param.svm_type == ONE_CLASS || model->param.svm_type == EPSILON_SVR || model->param.svm_type == NU_SVR) { decValues = Malloc(double, 1); }
	else { decValues = Malloc(double, nrClass * (nrClass - 1) / 2); }
	const double predResult = svm_predict_values(model, x, decValues);
	free(decValues);
	return predResult;
}

double svm_predict_probability(const svm_model* model, const svm_node* x, double* probEstimates)
{
	if ((model->param.svm_type == C_SVC || model->param.svm_type == NU_SVC) && model->probA != nullptr && model->probB != nullptr)
	{
		int i;
		const int nrClass = model->nr_class;
		double* decValues = Malloc(double, nrClass*(nrClass-1)/2);
		//std::cout<<"svm: predict values"<<std::endl;
		svm_predict_values(model, x, decValues);
		//std::cout<<"svm: malloc proba"<<std::endl;
		const double minProb  = 1e-7;
		double** pairwiseProb = Malloc(double *, nrClass);
		for (i = 0; i < nrClass; ++i) { pairwiseProb[i] = Malloc(double, nrClass); }
		int k = 0;
		for (i = 0; i < nrClass; ++i)
		{
			for (int j = i + 1; j < nrClass; ++j)
			{
				pairwiseProb[i][j] = min(max(sigmoid_predict(decValues[k], model->probA[k], model->probB[k]), minProb), 1 - minProb);
				pairwiseProb[j][i] = 1 - pairwiseProb[i][j];
				k++;
			}
		}
		//std::cout<<"svm: multiclass_proba"<<std::endl;
		multiclass_probability(nrClass, pairwiseProb, probEstimates);

		int probMaxIdx = 0;
		for (i = 1; i < nrClass; ++i) { if (probEstimates[i] > probEstimates[probMaxIdx]) { probMaxIdx = i; } }
		//std::cout<<"svm: free values"<<std::endl;
		for (i = 0; i < nrClass; ++i) { free(pairwiseProb[i]); }
		free(decValues);
		free(pairwiseProb);
		//printf("probability estimate: %f\n",probEstimates[prob_max_idx]);
		//std::cout<<"svm return:"<<prob_max_idx<<" "<<model->label[prob_max_idx]<<"\n";
		return model->label[probMaxIdx];
	}
	return svm_predict(model, x);
}

int svm_save_model(const char* modelFilename, const svm_model* model)
{
	FILE* fp = fopen(modelFilename, "w");
	if (fp == nullptr) { return -1; }

	const svm_parameter& param = model->param;

	fprintf(fp, "svm_type %s\n", get_svm_type(param.svm_type));
	fprintf(fp, "kernel_type %s\n", get_kernel_type(param.kernel_type));

	if (param.kernel_type == POLY) { fprintf(fp, "degree %d\n", param.degree); }
	if (param.kernel_type == POLY || param.kernel_type == RBF || param.kernel_type == SIGMOID) { fprintf(fp, "gamma %g\n", param.gamma); }
	if (param.kernel_type == POLY || param.kernel_type == SIGMOID) { fprintf(fp, "coef0 %g\n", param.coef0); }

	const int nrClass = model->nr_class;
	const int l       = model->l;
	fprintf(fp, "nr_class %d\n", nrClass);
	fprintf(fp, "total_sv %d\n", l);

	{
		fprintf(fp, "rho");
		for (int i = 0; i < nrClass * (nrClass - 1) / 2; ++i) { fprintf(fp, " %g", model->rho[i]); }
		fprintf(fp, "\n");
	}

	if (model->label)
	{
		fprintf(fp, "label");
		for (int i = 0; i < nrClass; ++i) { fprintf(fp, " %d", model->label[i]); }
		fprintf(fp, "\n");
	}

	if (model->probA) // regression has probA only
	{
		fprintf(fp, "probA");
		for (int i = 0; i < nrClass * (nrClass - 1) / 2; ++i) { fprintf(fp, " %g", model->probA[i]); }
		fprintf(fp, "\n");
	}
	if (model->probB)
	{
		fprintf(fp, "probB");
		for (int i = 0; i < nrClass * (nrClass - 1) / 2; ++i) { fprintf(fp, " %g", model->probB[i]); }
		fprintf(fp, "\n");
	}

	if (model->nSV)
	{
		fprintf(fp, "nr_sv");
		for (int i = 0; i < nrClass; ++i) { fprintf(fp, " %d", model->nSV[i]); }
		fprintf(fp, "\n");
	}

	fprintf(fp, "SV\n");
	const double* const * svCoef = model->sv_coef;
	const svm_node* const * sv   = model->SV;

	for (int i = 0; i < l; ++i)
	{
		for (int j = 0; j < nrClass - 1; ++j) { fprintf(fp, "%.16g ", svCoef[j][i]); }

		const svm_node* p = sv[i];

		if (param.kernel_type == PRECOMPUTED) { fprintf(fp, "0:%d ", int(p->value)); }
		else
		{
			while (p->index != -1)
			{
				fprintf(fp, "%d:%.8g ", p->index, p->value);
				p++;
			}
		}
		fprintf(fp, "\n");
	}
	if (ferror(fp) != 0 || fclose(fp) != 0) { return -1; }
	return 0;
}

static char* line = nullptr;
static int maxLineLen;

static char* readline(FILE* input)
{
	if (fgets(line, maxLineLen, input) == nullptr) { return nullptr; }

	while (strrchr(line, '\n') == nullptr)
	{
		maxLineLen *= 2;
		line          = (char*)realloc(line, maxLineLen);
		const int len = int(strlen(line));
		if (fgets(line + len, maxLineLen - len, input) == nullptr) { break; }
	}
	return line;
}

svm_model* svm_load_model(const char* modelFilename)
{
	FILE* fp = fopen(modelFilename, "rb");
	if (fp == nullptr) { return nullptr; }

	// read parameters

	svm_model* model     = Malloc(svm_model, 1);
	svm_parameter& param = model->param;
	model->rho           = nullptr;
	model->probA         = nullptr;
	model->probB         = nullptr;
	model->label         = nullptr;
	model->nSV           = nullptr;

	char cmd[81];
	while (true)
	{
		int res = fscanf(fp, "%80s", cmd);
		if (0 == res)
		{
			// FIXME: handle with no chars readed
		}
		if (strcmp(cmd, "svm_type") == 0)
		{
			res = fscanf(fp, "%80s", cmd);
			if (0 == res)
			{
				// FIXME: handle with no chars readed
			}
			int i;
			for (i = 0; get_svm_type(i); ++i)
			{
				if (strcmp(get_svm_type(i), cmd) == 0)
				{
					param.svm_type = i;
					break;
				}
			}
			if (get_svm_type(i) == nullptr)
			{
				fprintf(stderr, "unknown svm type.\n");
				free(model->rho);
				free(model->label);
				free(model->nSV);
				free(model);
				return nullptr;
			}
		}
		else if (strcmp(cmd, "kernel_type") == 0)
		{
			res = fscanf(fp, "%80s", cmd);
			if (0 == res)
			{
				// FIXME: handle with no chars readed
			}
			int i;
			for (i = 0; get_kernel_type(i); ++i)
			{
				if (strcmp(get_kernel_type(i), cmd) == 0)
				{
					param.kernel_type = i;
					break;
				}
			}
			if (get_kernel_type(i) == nullptr)
			{
				fprintf(stderr, "unknown kernel function.\n");
				free(model->rho);
				free(model->label);
				free(model->nSV);
				free(model);
				return nullptr;
			}
		}
		else if (strcmp(cmd, "degree") == 0)
		{
			res = fscanf(fp, "%d", &param.degree);
			if (0 == res) { }	// FIXME: handle with no interger readed
		}
		else if (strcmp(cmd, "gamma") == 0)
		{
			res = fscanf(fp, "%lf", &param.gamma);
			if (0 == res) { }	// FIXME: handle with no float readed
		}
		else if (strcmp(cmd, "coef0") == 0)
		{
			res = fscanf(fp, "%lf", &param.coef0);
			if (0 == res) { }	// FIXME: handle with no float readed
		}
		else if (strcmp(cmd, "nr_class") == 0)
		{
			res = fscanf(fp, "%d", &model->nr_class);
			if (0 == res) { }	// FIXME: handle with no interger readed
		}
		else if (strcmp(cmd, "total_sv") == 0)
		{
			res = fscanf(fp, "%d", &model->l);
			if (0 == res) { }	// FIXME: handle with no interger readed
		}
		else if (strcmp(cmd, "rho") == 0)
		{
			const int n = model->nr_class * (model->nr_class - 1) / 2;
			model->rho  = Malloc(double, n);
			for (int i = 0; i < n; ++i)
			{
				res = fscanf(fp, "%lf", &model->rho[i]);
				if (0 == res) { }	// FIXME: handle with no double readed
			}
		}
		else if (strcmp(cmd, "label") == 0)
		{
			const int n  = model->nr_class;
			model->label = Malloc(int, n);
			for (int i = 0; i < n; ++i)
			{
				res = fscanf(fp, "%d", &model->label[i]);
				if (0 == res) { }	// FIXME: handle with no integer readed
			}
		}
		else if (strcmp(cmd, "probA") == 0)
		{
			const int n  = model->nr_class * (model->nr_class - 1) / 2;
			model->probA = Malloc(double, n);
			for (int i = 0; i < n; ++i)
			{
				res = fscanf(fp, "%lf", &model->probA[i]);
				if (0 == res) { }	// FIXME: handle with no double readed
			}
		}
		else if (strcmp(cmd, "probB") == 0)
		{
			const int n  = model->nr_class * (model->nr_class - 1) / 2;
			model->probB = Malloc(double, n);
			for (int i = 0; i < n; ++i)
			{
				res = fscanf(fp, "%lf", &model->probB[i]);
				if (0 == res) { }	// FIXME: handle with no double readed
			}
		}
		else if (strcmp(cmd, "nr_sv") == 0)
		{
			const int n = model->nr_class;
			model->nSV  = Malloc(int, n);
			for (int i = 0; i < n; ++i)
			{
				res = fscanf(fp, "%d", &model->nSV[i]);
				if (0 == res) { }	// FIXME: handle with no integer readed
			}
		}
		else if (strcmp(cmd, "SV") == 0)
		{
			while (true)
			{
				const int c = getc(fp);
				if (c == EOF || c == '\n') { break; }
			}
			break;
		}
		else
		{
			fprintf(stderr, "unknown text in model file: [%s]\n", cmd);
			free(model->rho);
			free(model->label);
			free(model->nSV);
			free(model);
			return nullptr;
		}
	}

	// read sv_coef and SV

	int elements   = 0;
	const long pos = ftell(fp);

	maxLineLen = 1024;
	line       = Malloc(char, maxLineLen);
	char *p, *endptr;

	while (readline(fp) != nullptr)
	{
		p = strtok(line, ":");
		while (true)
		{
			p = strtok(nullptr, ":");
			if (p == nullptr) { break; }
			++elements;
		}
	}
	elements += model->l;

	fseek(fp, pos,SEEK_SET);

	const int m    = model->nr_class - 1;
	const int l    = model->l;
	model->sv_coef = Malloc(double *, m);
	int i;
	for (i = 0; i < m; ++i) { model->sv_coef[i] = Malloc(double, l); }
	model->SV        = Malloc(svm_node*, l);
	svm_node* xSpace = nullptr;
	if (l > 0) { xSpace = Malloc(svm_node, elements); }

	int j = 0;
	for (i = 0; i < l; ++i)
	{
		readline(fp);
		model->SV[i] = &xSpace[j];

		p                    = strtok(line, " \t");
		model->sv_coef[0][i] = strtod(p, &endptr);
		for (int k = 1; k < m; ++k)
		{
			p                    = strtok(nullptr, " \t");
			model->sv_coef[k][i] = strtod(p, &endptr);
		}

		while (true)
		{
			char* idx = strtok(nullptr, ":");
			char* val = strtok(nullptr, " \t");

			if (val == nullptr) { break; }
			xSpace[j].index = int(strtol(idx, &endptr, 10));
			xSpace[j].value = strtod(val, &endptr);

			++j;
		}
		xSpace[j++].index = -1;
	}
	free(line);

	if (ferror(fp) != 0 || fclose(fp) != 0) { return nullptr; }

	model->free_sv = 1;	// XXX
	return model;
}

void svm_destroy_model(svm_model* model)
{
	if (model->free_sv && model->l > 0) { free((void*)(model->SV[0])); }
	for (int i = 0; i < model->nr_class - 1; ++i) { free(model->sv_coef[i]); }
	free(model->SV);
	free(model->sv_coef);
	free(model->rho);
	free(model->label);
	free(model->probA);
	free(model->probB);
	free(model->nSV);
	free(model);
}

void svm_destroy_param(svm_parameter* param)
{
	free(param->weight_label);
	free(param->weight);
}

const char* svm_check_parameter(const svm_problem* prob, const svm_parameter* param)
{
	// svm_type

	const int svmType = param->svm_type;
	if (svmType != C_SVC && svmType != NU_SVC && svmType != ONE_CLASS && svmType != EPSILON_SVR && svmType != NU_SVR) { return "unknown svm type"; }

	// kernel_type, degree

	const int kernelType = param->kernel_type;
	if (kernelType != LINEAR && kernelType != POLY && kernelType != RBF && kernelType != SIGMOID && kernelType != PRECOMPUTED) { return "unknown kernel type"; }

	if (param->gamma < 0) { return "gamma < 0"; }
	if (param->degree < 0) { return "degree of polynomial kernel < 0"; }

	// cache_size,eps,C,nu,p,shrinking
	if (param->cache_size <= 0) { return "cache_size <= 0"; }
	if (param->eps <= 0) { return "eps <= 0"; }
	if (svmType == C_SVC || svmType == EPSILON_SVR || svmType == NU_SVR) { if (param->C <= 0) { return "C <= 0"; } }
	if (svmType == NU_SVC || svmType == ONE_CLASS || svmType == NU_SVR) { if (param->nu <= 0 || param->nu > 1) { return "nu <= 0 or nu > 1"; } }
	if (svmType == EPSILON_SVR) { if (param->p < 0) { return "p < 0"; } }
	if (param->shrinking != 0 && param->shrinking != 1) { return "shrinking != 0 and shrinking != 1"; }
	if (param->probability != 0 && param->probability != 1) { return "probability != 0 and probability != 1"; }
	if (param->probability == 1 && svmType == ONE_CLASS) { return "one-class SVM probability output not supported yet"; }


	// check whether nu-svc is feasible

	if (svmType == NU_SVC)
	{
		const int l    = prob->l;
		int maxNrClass = 16;
		int nrClass    = 0;
		int* label     = Malloc(int, maxNrClass);
		int* count     = Malloc(int, maxNrClass);

		int i;
		for (i = 0; i < l; ++i)
		{
			const int thisLabel = int(prob->y[i]);
			int j;
			for (j = 0; j < nrClass; ++j)
			{
				if (thisLabel == label[j])
				{
					++count[j];
					break;
				}
			}
			if (j == nrClass)
			{
				if (nrClass == maxNrClass)
				{
					maxNrClass *= 2;
					label = (int*)realloc(label, maxNrClass * sizeof(int));
					count = (int*)realloc(count, maxNrClass * sizeof(int));
				}
				label[nrClass] = thisLabel;
				count[nrClass] = 1;
				++nrClass;
			}
		}

		for (i = 0; i < nrClass; ++i)
		{
			const int n1 = count[i];
			for (int j = i + 1; j < nrClass; ++j)
			{
				const int n2 = count[j];
				if (param->nu * (n1 + n2) / 2 > min(n1, n2))
				{
					free(label);
					free(count);
					return "specified nu is infeasible";
				}
			}
		}
		free(label);
		free(count);
	}

	return nullptr;
}

int svm_check_probability_model(const svm_model* model)
{
	return ((model->param.svm_type == C_SVC || model->param.svm_type == NU_SVC) && model->probA != nullptr && model->probB != nullptr)
		   || ((model->param.svm_type == EPSILON_SVR || model->param.svm_type == NU_SVR) && model->probA != nullptr);
}

void svm_set_print_string_function(void (*printFunc)(const char*))
{
	if (printFunc == nullptr) { svm_print_string = &print_string_stdout; }
	else { svm_print_string = printFunc; }
}
