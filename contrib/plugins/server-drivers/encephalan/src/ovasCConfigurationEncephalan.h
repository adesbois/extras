///-------------------------------------------------------------------------------------------------
/// 
/// \file ovasCConfigurationEncephalan.h
/// \brief The CConfigurationEncephalan handles the configuration dialog specific to the Encephalan device.
/// \author Alexey Minin (UrFU)
/// \version 1.0.
/// \date 02/01/2019.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
///
///-------------------------------------------------------------------------------------------------

#pragma once

#include "../ovasCConfigurationBuilder.h"
#include "ovasIDriver.h"

namespace OpenViBE {
namespace AcquisitionServer {
/// <summary>	The CConfigurationEncephalan handles the configuration dialog specific to the Encephalan device. </summary>
/// <seealso cref="CDriverEncephalan" />
class CConfigurationEncephalan final : public CConfigurationBuilder
{
public:
	CConfigurationEncephalan(IDriverContext& driverContext, const char* gtkFileName, uint32_t& connectionPort, char* connectionIp);

	bool preConfigure() override;
	bool postConfigure() override;
	char* getConnectionIp() const { return m_connectionIp; }

protected:
	IDriverContext& m_driverContext;
	uint32_t& m_connectionPort;
	char* m_connectionIp = nullptr;
};
}  // namespace AcquisitionServer
}  // namespace OpenViBE
