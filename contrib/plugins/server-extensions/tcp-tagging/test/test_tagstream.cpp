#include "../ovasCTagStream.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/AcquisitionServer;
using namespace /*OpenViBE::*/AcquisitionServer::Plugins;
using namespace std;

#include <iostream>

int main()
{
	bool ok = false;

	CTagStream tagStream1;

	// The construction of the second TagStream must fail because of port already in use.
	try { CTagStream tagStream2; }
	catch (exception&) { ok = true; }	// This exception is expected, don't print

	// The construction must succeed because another port is used.
	try { CTagStream tagStream3(15362); }
	catch (exception& e)
	{
		// Unexpected exception
		cout << "Exception: " << e.what() << "\n";
		ok = false;
	}

	if (!ok) { return 1; }
	return 0;
}
