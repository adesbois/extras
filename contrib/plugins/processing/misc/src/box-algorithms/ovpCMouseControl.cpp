#include "ovpCMouseControl.h"

#include <iostream>

#if defined TARGET_OS_Linux
	#include <unistd.h>
#endif

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Toolkit;
using namespace /*OpenViBE::Plugins::*/Tools;	//Ambiguous without OpenViBEPlugins
using namespace std;

bool CMouseControl::initialize()
{
	m_decoder = new TStreamedMatrixDecoder<CMouseControl>(*this, 0);

#if !defined(TARGET_OS_Linux)
	getLogManager() << LogLevel_Error << "This box algorithm is not implemented for your operating system\n";
	return false;
#else
	return true;
#endif
}

bool CMouseControl::uninitialize()
{
	m_decoder->uninitialize();
	delete m_decoder;

	return true;
}

bool CMouseControl::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CMouseControl::process()
{
	IBoxIO* boxContext       = getBoxAlgorithmContext()->getDynamicBoxContext();
	const size_t nInputChunk = boxContext->getInputChunkCount(0);

	for (size_t i = 0; i < nInputChunk; ++i)
	{
		m_decoder->decode(i);
		if (m_decoder->isBufferReceived())
		{
			IMatrix* iMatrix = m_decoder->getOutputMatrix();
			if (iMatrix->getBufferElementCount() != 1)
			{
				getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Error << "Error, dimension size isn't 1 for Amplitude input !\n";
				return false;
			}
#if defined TARGET_OS_Linux
			const double* iBuffer = iMatrix->getBuffer();
			m_pMainDisplay=::XOpenDisplay(NULL);
			if (!m_pMainDisplay)
			{
				getLogManager() << LogLevel_Error << "Impossible to open Display.\n";
				return false;
			}
			m_oRootWindow=DefaultRootWindow(m_pMainDisplay);  //all X11 screens
			::XSelectInput(m_pMainDisplay, m_oRootWindow, ButtonPressMask|ButtonReleaseMask|ButtonMotionMask|OwnerGrabButtonMask);

			int offsetY = 0;
			int offsetX = int(iBuffer[0]*100.0);

			getLogManager() << LogLevel_Debug << "offsetX = " << offsetX << "\n";

			::XWarpPointer(m_pMainDisplay, m_oRootWindow, 0, 0, 0, 0, 0, offsetX, offsetY);
			::XCloseDisplay(m_pMainDisplay);
#endif
			// TODO
			// For windows use:
			// SetCursorPos(int x, int y)
		}
	}
	return true;
}
