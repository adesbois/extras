#if defined(WIN32) && defined(TARGET_BUILDTYPE_Debug)
// Windows debug build doesn't typically link as most people don't have the python debug library.
#else

#if defined TARGET_HAS_ThirdPartyPython3

#include "ovpCBoxAlgorithmPython3.h"

#if defined(PY_MAJOR_VERSION) && (PY_MAJOR_VERSION == 3)

#include <fstream>
#include <iostream>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::*/Toolkit;
using namespace /*OpenViBE::Plugins::*/Python;

using namespace std;

//****************************
//***** Static Functions *****
//****************************
///-------------------------------------------------------------------------------------------------
static bool appendToPyObject(PyObject* obj, PyObject* buffer)
{
	PyObject* methodToCall = PyUnicode_FromString("append");
	PyObject* result       = PyObject_CallMethodObjArgs(buffer, methodToCall, obj, NULL);
	Py_CLEAR(methodToCall);
	if (result == nullptr) { return false; }
	Py_CLEAR(result);
	return true;
}

static bool getLenFromPyObject(PyObject* obj, size_t& len)
{
	PyObject* pyLen = PyObject_CallMethod(obj, "__len__", nullptr);
	if (pyLen == nullptr) { return false; }
	len = size_t(PyLong_AsUnsignedLongMask(pyLen));
	Py_CLEAR(pyLen);
	return true;
}

static void getTimeFromPyObject(PyObject* obj, const char* attr, uint64_t& time)
{
	PyObject* pyTime = PyObject_GetAttrString(obj, attr);
	time             = CTime(PyFloat_AsDouble(pyTime)).time();
	Py_CLEAR(pyTime);
}

static void getTimesFromPyObject(PyObject* obj, uint64_t& start, uint64_t& end)
{
	getTimeFromPyObject(obj, "startTime", start);
	getTimeFromPyObject(obj, "endTime", end);
}

static bool setMatrixInfosFromPyObject(PyObject* obj, IMatrix* matrix)
{
	PyObject* pyNDim = PyObject_CallMethod(obj, "getDimensionCount", nullptr);
	if (pyNDim == nullptr) { return false; }

	const size_t nDim = PyLong_AsUnsignedLongMask(pyNDim);
	matrix->setDimensionCount(nDim);
	Py_CLEAR(pyNDim);

	PyObject* pySizeDim  = PyObject_GetAttrString(obj, "dimensionSizes");
	PyObject* pyLabelDim = PyObject_GetAttrString(obj, "dimensionLabels");

	size_t offset = 0;
	for (size_t i = 0; i < nDim; ++i)
	{
		const size_t size = PyLong_AsUnsignedLongMask(PyList_GetItem(pySizeDim, Py_ssize_t(i)));
		matrix->setDimensionSize(i, size);
		for (size_t j = 0; j < size; ++j)
		{
		    PyObject* label  = PyList_GetItem(pyLabelDim, offset + j);
		    if (PyUnicode_Check(label))
		    {
		        matrix->setDimensionLabel(i, j, PyUnicode_AsUTF8(label));
		    }
		    else
		    {
	            Py_CLEAR(pySizeDim);
	            Py_CLEAR(pyLabelDim);
		        return false;
		    }
		}
		offset = offset + size;
	}

	Py_CLEAR(pySizeDim);
	Py_CLEAR(pyLabelDim);
	return true;
}
///-------------------------------------------------------------------------------------------------

bool CBoxAlgorithmPython3::m_isPythonInitialized    = false;
PyObject* CBoxAlgorithmPython3::m_mainModule        = nullptr;
PyObject* CBoxAlgorithmPython3::m_mainDictionnary   = nullptr;
PyObject* CBoxAlgorithmPython3::m_matrixHeader      = nullptr;
PyObject* CBoxAlgorithmPython3::m_matrixBuffer      = nullptr;
PyObject* CBoxAlgorithmPython3::m_matrixEnd         = nullptr;
PyObject* CBoxAlgorithmPython3::m_signalHeader      = nullptr;
PyObject* CBoxAlgorithmPython3::m_signalBuffer      = nullptr;
PyObject* CBoxAlgorithmPython3::m_signalEnd         = nullptr;
PyObject* CBoxAlgorithmPython3::m_stimulationHeader = nullptr;
PyObject* CBoxAlgorithmPython3::m_stimulation       = nullptr;
PyObject* CBoxAlgorithmPython3::m_stimulationSet    = nullptr;
PyObject* CBoxAlgorithmPython3::m_stimulationEnd    = nullptr;
PyObject* CBoxAlgorithmPython3::m_buffer            = nullptr;
PyObject* CBoxAlgorithmPython3::m_execFileFunction  = nullptr;
PyObject* CBoxAlgorithmPython3::m_stdout            = nullptr;
PyObject* CBoxAlgorithmPython3::m_stderr            = nullptr;

bool CBoxAlgorithmPython3::logSysStd(const bool out)
{
	PyObject* pyLog = PyObject_CallMethod((out ? m_stdout : m_stderr), "getvalue", nullptr);
	if (pyLog == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to call sys.std" << (out ? "out" : "err") << ".getvalue().\n";
		return false;
	}

	char* log = PyBytes_AS_STRING(PyUnicode_AsEncodedString(pyLog, "utf-8", "strict"));
	if (log == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to convert pyLog to (char *).\n";
		Py_CLEAR(pyLog);
		return false;
	}
	Py_CLEAR(pyLog);

	if (strlen(log) > 0)
	{
		this->getLogManager() << LogLevel_Info << log;
		PyObject* result = PyObject_CallMethod((out ? m_stdout : m_stderr), "flush", nullptr);
		if (result == nullptr)
		{
			this->getLogManager() << LogLevel_Error << "Failed to call sys.std" << (out ? "out" : "err") << ".flush().\n";
			return false;
		}
		Py_CLEAR(result);
	}
	return true;
}

void CBoxAlgorithmPython3::buildPythonSettings()
{
	const IBox* boxCtx = getBoxAlgorithmContext()->getStaticBoxContext();
	for (uint32_t i = 2; i < boxCtx->getSettingCount(); ++i)
	{
		CString name;
		boxCtx->getSettingName(i, name);
		const CString value = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), i);
		PyDict_SetItemString(m_boxSetting, name.toASCIIString(), PyUnicode_FromString(value.toASCIIString()));
	}
}

bool CBoxAlgorithmPython3::initializePythonSafely()
{
	// Only the first Python box does the initialization of the global parts
	if (m_isPythonInitialized) { return true; }

	this->getLogManager() << LogLevel_Info << "Discovered Python is " << Py_GetVersion() << " (" << Py_GetPlatform() << ")\n";
	this->getLogManager() << LogLevel_Debug << "The Python path is [" << Py_GetPath() << "]\n";

	const string cmd = string("import sys\nsys.path.append('") + Directories::getDataDir().toASCIIString() + "/plugins/python3')\nsys.argv = [\"openvibe\"]\n";
	// cmd += "import openvibe\n" + "from StimulationsCodes import *\n";
	this->getLogManager() << LogLevel_Trace << "Running [\n" << cmd << "\n].\n";

	PyRun_SimpleString(cmd.c_str());

	m_mainModule      = PyImport_AddModule("__main__");
	m_mainDictionnary = PyModule_GetDict(m_mainModule);

	//****************************************
	/*
	//Execute the script which contains the different classes to interact with OpenViBE
	const char* path = (Directories::getDataDir() + "/plugins/python3/openvibe.py").toASCIIString();
	FILE* file       = fopen(path, "r");
	if (!file)
	{
		this->getLogManager() << LogLevel_Error << "Failed to open '" << path << "'.\n";
		return false;
	}
	this->getLogManager() << LogLevel_Info << "file open check\n";

	if (PyRun_SimpleFile(file, path) == -1)
	{
		this->getLogManager() << LogLevel_Error << "Failed to run '" << path << "'.\n";
		return false;
	}
	fclose(file);

	this->getLogManager() << LogLevel_Info << "file run check\n";
	*/
	const string path = string(Directories::getDataDir().toASCIIString()) + "/plugins/python3/openvibe.py";
	ifstream file;
	file.open(path);
	if (!file.is_open())
	{
		this->getLogManager() << LogLevel_Error << "Failed to open '" << path << "'.\n";
		return false;
	}
	const std::string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
	if (PyRun_SimpleString(str.c_str()) == -1)	// Yolo but PyRun_File crash
	{
		this->getLogManager() << LogLevel_Error << "Failed to run '" << path << "'.\n";
		return false;
	}
	//this->getLogManager() << LogLevel_Trace << "Running [\n" << str << "\n].\n";
	file.close();
	//****************************************

// Apply References
	//****************************************
	m_stdout = PySys_GetObject("stdout");
	if (m_stdout == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "sys.stdout does not exist.\n";
		return false;
	}
	m_stderr = PySys_GetObject("stderr");
	if (m_stderr == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "sys.stderr does not exist.\n";
		return false;
	}

	m_execFileFunction = PyDict_GetItemString(m_mainDictionnary, "execfileHandlingException");
	if (m_execFileFunction == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "openvibe.py doesn't have a execfileHandlingException function, quitting" << ".\n";
		return false;
	}

	if (!PyCallable_Check(m_execFileFunction))
	{
		this->getLogManager() << LogLevel_Error << "openvibe.py doesn't have a execfileHandlingException function callable, " << ".\n";
		return false;
	}

	//		Streams
	m_matrixHeader = PyDict_GetItemString(m_mainDictionnary, "OVStreamedMatrixHeader");
	if (m_matrixHeader == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVStreamedMatrixHeader\".\n";
		return false;
	}

	m_matrixBuffer = PyDict_GetItemString(m_mainDictionnary, "OVStreamedMatrixBuffer");
	if (m_matrixBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVStreamedMatrixBuffer\".\n";
		return false;
	}

	m_matrixEnd = PyDict_GetItemString(m_mainDictionnary, "OVStreamedMatrixEnd");
	if (m_matrixEnd == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVStreamedMatrixEnd\".\n";
		return false;
	}

	m_signalHeader = PyDict_GetItemString(m_mainDictionnary, "OVSignalHeader");
	if (m_signalHeader == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVSignalHeader\".\n";
		return false;
	}

	m_signalBuffer = PyDict_GetItemString(m_mainDictionnary, "OVSignalBuffer");
	if (m_signalBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVSignalBuffer\".\n";
		return false;
	}

	m_signalEnd = PyDict_GetItemString(m_mainDictionnary, "OVSignalEnd");
	if (m_signalEnd == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVSignalEnd\".\n";
		return false;
	}

	m_stimulationHeader = PyDict_GetItemString(m_mainDictionnary, "OVStimulationHeader");
	if (m_stimulationHeader == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVStimulationHeader\".\n";
		return false;
	}

	m_stimulation = PyDict_GetItemString(m_mainDictionnary, "OVStimulation");
	if (m_stimulation == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVStimulation\".\n";
		return false;
	}

	m_stimulationSet = PyDict_GetItemString(m_mainDictionnary, "OVStimulationSet");
	if (m_stimulationSet == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVStimulationSet\".\n";
		return false;
	}

	m_stimulationEnd = PyDict_GetItemString(m_mainDictionnary, "OVStimulationEnd");
	if (m_stimulationEnd == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVStimulationEnd\".\n";
		return false;
	}

	m_buffer = PyDict_GetItemString(m_mainDictionnary, "OVBuffer");
	if (m_buffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load class \"OVBuffer\".\n";
		return false;
	}

	m_isPythonInitialized = true;
	this->getLogManager() << LogLevel_Info << "Python Interpreter initialized\n";
	return true;
}

bool CBoxAlgorithmPython3::initialize()
{
	m_box                 = nullptr;
	m_boxInput            = nullptr;
	m_boxOutput           = nullptr;
	m_boxCurrentTime      = nullptr;
	m_boxSetting          = nullptr;
	m_boxInitialize       = nullptr;
	m_boxProcess          = nullptr;
	m_boxUninitialize     = nullptr;
	m_initializeSucceeded = false;

	if (!initializePythonSafely()) { return false; }

	//Initialize the clock frequency of the box depending on the first setting of the box
	m_clockFrequency = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_scriptFilename = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	if (strlen(m_scriptFilename.toASCIIString()) == 0)
	{
		this->getLogManager() << LogLevel_Error << "You have to choose a script.\n";
		return false;
	}
	
	//Create the decoders for the inputs
	const IBox& boxCtx = this->getStaticBoxContext();
	CIdentifier typeID;
	for (size_t i = 0; i < boxCtx.getInputCount(); ++i)
	{
		boxCtx.getInputType(i, typeID);
		if (typeID == OV_TypeId_StreamedMatrix) { m_decoders.push_back(new TStreamedMatrixDecoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_Signal) { m_decoders.push_back(new TSignalDecoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_FeatureVector) { m_decoders.push_back(new TFeatureVectorDecoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_Spectrum) { m_decoders.push_back(new TSpectrumDecoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_ChannelLocalisation) { m_decoders.push_back(new TChannelLocalisationDecoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_Stimulations) { m_decoders.push_back(new TStimulationDecoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_ExperimentInfo) { m_decoders.push_back(new TExperimentInfoDecoder<CBoxAlgorithmPython3>(*this, i)); }
		else
		{
			this->getLogManager() << LogLevel_Error << "Codec to decode " << typeID.str() << " is not implemented.\n";
			return false;
		}
	}

	//Create the encoders for the outputs
	for (size_t i = 0; i < boxCtx.getOutputCount(); ++i)
	{
		boxCtx.getOutputType(i, typeID);
		if (typeID == OV_TypeId_StreamedMatrix) { m_encoders.push_back(new TStreamedMatrixEncoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_Signal) { m_encoders.push_back(new TSignalEncoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_FeatureVector) { m_encoders.push_back(new TFeatureVectorEncoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_Spectrum) { m_encoders.push_back(new TSpectrumEncoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_ChannelLocalisation) { m_encoders.push_back(new TChannelLocalisationEncoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_Stimulations) { m_encoders.push_back(new TStimulationEncoder<CBoxAlgorithmPython3>(*this, i)); }
		else if (typeID == OV_TypeId_ExperimentInfo) { m_encoders.push_back(new TExperimentInfoEncoder<CBoxAlgorithmPython3>(*this, i)); }
		else
		{
			this->getLogManager() << LogLevel_Error << "Codec to encode " << typeID.str() << " is not implemented.\n";
			return false;
		}
	}

	PyObject* pyTmp = Py_BuildValue("s,O", m_scriptFilename.toASCIIString(), m_mainDictionnary);
	if (pyTmp == nullptr) { this->getLogManager() << LogLevel_Error << "Failed to load [" << m_scriptFilename << "]"; }

	PyObject* result = PyObject_CallObject(m_execFileFunction, pyTmp);
	if (result == nullptr || PyLong_AsLong(result) != 0)
	{
		this->getLogManager() << LogLevel_Error << "Failed to run [" << m_scriptFilename << "], result = "
				<< (result ? std::to_string(PyLong_AsLong(result)) : "NULL") << "\n";
		logSysStdout();
		logSysStderr();
		Py_CLEAR(pyTmp);
		Py_CLEAR(result);
		return false;
	}

	Py_CLEAR(pyTmp);
	Py_CLEAR(result);

	m_box = PyObject_GetAttrString(m_mainModule, "box"); // la box qui vient juste d'etre creee
	if (m_box == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load \"box\" object.\n";
		return false;
	}

	m_boxInput = PyObject_GetAttrString(m_box, "input");
	if (m_boxInput == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load \"box.input\".\n";
		return false;
	}

	m_boxOutput = PyObject_GetAttrString(m_box, "output");
	if (m_boxOutput == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load \"box.output\".\n";
		return false;
	}

	std::string type;
	for (size_t i = 0; i < boxCtx.getInputCount(); ++i)
	{
		boxCtx.getInputType(i, typeID);
		if (typeID == OV_TypeId_StreamedMatrix) { type = "StreamedMatrix"; }
		else if (typeID == OV_TypeId_Signal) { type = "Signal"; }
		else if (typeID == OV_TypeId_FeatureVector) { type = "FeatureVector"; }
		else if (typeID == OV_TypeId_Spectrum) { type = "Spectrum"; }
		else if (typeID == OV_TypeId_ChannelLocalisation) { type = "ChannelLocalisation"; }
		else if (typeID == OV_TypeId_Stimulations) { type = "Stimulations"; }
		else if (typeID == OV_TypeId_ExperimentInfo) { type = "ExperimentInfo"; }
		PyObject* res = PyObject_CallMethod(m_box, "addInput", "s", type.c_str());
		if (res == nullptr)
		{
			this->getLogManager() << LogLevel_Error << "Failed to call box.addInput().\n";
			return false;
		}
		Py_CLEAR(res);
	}

	for (size_t i = 0; i < boxCtx.getOutputCount(); ++i)
	{
		boxCtx.getOutputType(i, typeID);
		if (typeID == OV_TypeId_StreamedMatrix) { type = "StreamedMatrix"; }
		else if (typeID == OV_TypeId_Signal) { type = "Signal"; }
		else if (typeID == OV_TypeId_FeatureVector) { type = "FeatureVector"; }
		else if (typeID == OV_TypeId_Spectrum) { type = "Spectrum"; }
		else if (typeID == OV_TypeId_ChannelLocalisation) { type = "ChannelLocalisation"; }
		else if (typeID == OV_TypeId_Stimulations) { type = "Stimulations"; }
		else if (typeID == OV_TypeId_ExperimentInfo) { type = "ExperimentInfo"; }
		PyObject* res = PyObject_CallMethod(m_box, "addOutput", "s", type.c_str());
		if (res == nullptr)
		{
			this->getLogManager() << LogLevel_Error << "Failed to call box.addOutput().\n";
			return false;
		}
		Py_CLEAR(res);
	}

	m_boxSetting = PyObject_GetAttrString(m_box, "setting");
	if (m_boxSetting == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to load \"box.setting\".\n";
		return false;
	}
	buildPythonSettings();

	if (!PyObject_HasAttrString(m_box, "_clock"))
	{
		this->getLogManager() << LogLevel_Error << "Failed to initialize \"box._clock\" attribute because it does not exist.\n";
		return false;
	}

	PyObject* pyBoxClock = PyLong_FromLong(long(m_clockFrequency));
	if (pyBoxClock == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to convert m_ClockFrequency into PyInt.\n";
		return false;
	}
	if (PyObject_SetAttrString(m_box, "_clock", pyBoxClock) == -1)
	{
		this->getLogManager() << LogLevel_Error << "Failed to initialize \"box._clock\" attribute.\n";
		return false;
	}
	Py_CLEAR(pyBoxClock);

	if (!PyObject_HasAttrString(m_box, "_currentTime"))
	{
		this->getLogManager() << LogLevel_Error << "Failed to initialize \"box._currentTime\" attribute because it does not exist.\n";
		return false;
	}

	m_boxCurrentTime = PyFloat_FromDouble(CTime(this->getPlayerContext().getCurrentTime()).toSeconds());
	if (m_boxCurrentTime == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to convert the current time into a PyFloat.\n";
		return false;
	}
	if (PyObject_SetAttrString(m_box, "_currentTime", m_boxCurrentTime) == -1)
	{
		this->getLogManager() << LogLevel_Error << "Failed to initialize \"box._currentTime\" attribute.\n";
		return false;
	}

	if (!PyObject_HasAttrString(m_box, "realInitialize"))
	{
		this->getLogManager() << LogLevel_Error << "No realInitialize.\n";
		return false;
	}

	m_boxInitialize = PyObject_GetAttrString(m_box, "realInitialize");
	if (m_boxInitialize == nullptr) { this->getLogManager() << LogLevel_ImportantWarning << "Failed to load \"box.realInitialize\" function.\n"; }
	else if (!PyCallable_Check(m_boxInitialize)) { this->getLogManager() << LogLevel_ImportantWarning << "\"box.realInitialize\" is not callable.\n"; }

	m_boxProcess = PyObject_GetAttrString(m_box, "realProcess");
	if (m_boxProcess == nullptr) { this->getLogManager() << LogLevel_ImportantWarning << "Failed to load \"box.realProcess\" function.\n"; }
	else if (!PyCallable_Check(m_boxProcess)) { this->getLogManager() << LogLevel_ImportantWarning << "\"box.realProcess\" is not callable.\n"; }

	m_boxUninitialize = PyObject_GetAttrString(m_box, "realUninitialize");
	if (m_boxUninitialize == nullptr) { this->getLogManager() << LogLevel_ImportantWarning << "Failed to load \"box.realUninitialize\" function.\n"; }
	else if (!PyCallable_Check(m_boxUninitialize)) { this->getLogManager() << LogLevel_ImportantWarning << "\"box.realUninitialize\" is not callable.\n"; }

	//Execute the initialize function defined in the python user script
	if (m_boxInitialize && PyCallable_Check(m_boxInitialize))
	{
		PyObject* res          = PyObject_CallObject(m_boxInitialize, nullptr);
		const bool stdoutError = logSysStdout(); // souci car la si l'init plante pas de sortie au bon endroit
		const bool stderrError = logSysStderr();
		if ((res == nullptr) || (!stdoutError) || (!stderrError))
		{
			if (res == nullptr) { this->getLogManager() << LogLevel_Error << "Failed to call \"box.__initialize\" function.\n"; }
			if (!stdoutError) { this->getLogManager() << LogLevel_Error << "logSysStdout() failed during box.__initialization.\n"; }
			if (!stderrError) { this->getLogManager() << LogLevel_Error << "logSysStderr() failed during box.__initialization.\n"; }
			Py_CLEAR(res);
			return false;
		}
		Py_CLEAR(res);
	}

	m_initializeSucceeded = true;
	return true;
}

bool CBoxAlgorithmPython3::uninitialize()
{
	for (size_t i = 0; i < m_decoders.size(); ++i)
	{
		m_decoders[i]->uninitialize();
		delete m_decoders[i];
	}
	m_decoders.clear();

	for (size_t i = 0; i < m_encoders.size(); ++i)
	{
		m_encoders[i]->uninitialize();
		delete m_encoders[i];
	}
	m_encoders.clear();

	if (m_initializeSucceeded)	// we call this uninit only if init had succeeded Execute the uninitialize function defined in the python script
	{	// il y a un souci ici si le script n'a pas ete charge ca ne passe pas
		if (m_boxUninitialize && PyCallable_Check(m_boxUninitialize))
		{
			PyObject* result       = PyObject_CallObject(m_boxUninitialize, nullptr);
			const bool stdoutError = logSysStdout();
			const bool stderrError = logSysStderr();
			if ((result == nullptr) || (!stdoutError) || (!stderrError))
			{
				if (result == nullptr) { this->getLogManager() << LogLevel_Error << "Failed to call \"box.__uninitialize\" function.\n"; }
				if (!stdoutError) { this->getLogManager() << LogLevel_Error << "logSysStdout() failed during box.__uninitialization.\n"; }
				if (!stderrError) { this->getLogManager() << LogLevel_Error << "logSysStderr() failed during box.__uninitialization.\n"; }
				Py_CLEAR(result);
				return false;
			}
			Py_CLEAR(result);
		}
	}

	// Note: Py_CLEAR is safe to use on NULL pointers, so we can clean everything here
	Py_CLEAR(m_box);
	Py_CLEAR(m_boxInput);
	Py_CLEAR(m_boxOutput);
	Py_CLEAR(m_boxCurrentTime);
	Py_CLEAR(m_boxSetting);
	Py_CLEAR(m_boxInitialize);
	Py_CLEAR(m_boxProcess);
	Py_CLEAR(m_boxUninitialize);

	// Py_Initialize() and Py_Finalize() are called in ovp_main.cpp, we never uninitialize Python here

	return true;
}

bool CBoxAlgorithmPython3::processClock(Kernel::CMessageClock& /*msg*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmPython3::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmPython3::transferStreamedMatrixInputChunksToPython(const size_t index)
{
	IBoxIO& boxCtx = this->getDynamicBoxContext();

	if (!PyList_Check(m_boxInput))
	{
		this->getLogManager() << LogLevel_Error << "box.input must be a list.\n";
		return false;
	}

	PyObject* pyBuffer = PyList_GetItem(m_boxInput, Py_ssize_t(index));
	if (pyBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.input[" << index << "].\n";
		return false;
	}
	//Expose input streamed matrix chunks to python
	for (size_t idx = 0; idx < boxCtx.getInputChunkCount(index); ++idx)
	{
		m_decoders[index]->decode(idx);

		if (m_decoders[index]->isHeaderReceived())
		{
			IMatrix* matrix = dynamic_cast<TStreamedMatrixDecoder<CBoxAlgorithmPython3>*>(m_decoders[index])->getOutputMatrix();
			size_t nDim     = matrix->getDimensionCount();

			PyObject* pySizeDim = PyList_New(nDim);
			if (pySizeDim == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new list pySizeDim.\n";
				return false;
			}

			PyObject* pyLabelDim = PyList_New(0);
			if (pyLabelDim == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new list pyLabelDim.\n";
				Py_CLEAR(pySizeDim);
				return false;
			}

			for (size_t i = 0; i < nDim; ++i)
			{
				size_t dimSize = matrix->getDimensionSize(i);
				if (PyList_SetItem(pySizeDim, i, PyLong_FromLong(dimSize)) == -1)
				{
					this->getLogManager() << LogLevel_Error << "Failed to set item " << i << " in dimension size list.\n";
					Py_CLEAR(pySizeDim);
					Py_CLEAR(pyLabelDim);
					return false;
				}
				for (size_t j = 0; j < dimSize; ++j)
				{
					if (PyList_Append(pyLabelDim, PyUnicode_FromString(matrix->getDimensionLabel(i, j))) == -1)
					{
						this->getLogManager() << LogLevel_Error << "Failed to append \"" << matrix->getDimensionLabel(i, j) << "\" in dimension label list.\n";
						Py_CLEAR(pySizeDim);
						Py_CLEAR(pyLabelDim);
						return false;
					}
				}
			}

			PyObject* pyArg = PyTuple_New(4);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 2, pySizeDim) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 2 (dimension size) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 3, pyLabelDim) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 3 (dimension label) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pyMatrixHeader = PyObject_Call(m_matrixHeader, pyArg, nullptr);
			if (pyMatrixHeader == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVStreamedMatrixHeader pyMatrixHeader.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pySizeDim);
			Py_CLEAR(pyLabelDim);
			Py_CLEAR(pyArg);

			if (!appendToPyObject(pyMatrixHeader, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVStreamedMatrixHeader to box.input[" << index << "].\n";
				Py_CLEAR(pyMatrixHeader);
				return false;
			}
			Py_CLEAR(pyMatrixHeader);
		}

		if (m_decoders[index]->isBufferReceived())
		{
			PyObject* pyArg = PyTuple_New(3);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 2, PyList_New(0)) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 2 (bufferElements) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pyMatrixBuffer = PyObject_Call(m_matrixBuffer, pyArg, nullptr);
			if (pyMatrixBuffer == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVStreamedMatrixBuffer pyMatrixBuffer.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pyArg);

			IMatrix* matrix    = dynamic_cast<TStreamedMatrixDecoder<CBoxAlgorithmPython3>*>(m_decoders[index])->getOutputMatrix();
			double* bufferBase = matrix->getBuffer();
			for (size_t i = 0; i < matrix->getBufferElementCount(); ++i)
			{
				if (PyList_Append(pyMatrixBuffer, PyFloat_FromDouble(bufferBase[i])) == -1)
				{
					this->getLogManager() << LogLevel_Error << "Failed to append element " << i << " to pyMatrixBuffer.\n";
					Py_CLEAR(pyMatrixBuffer);
					return false;
				}
			}

			if (!appendToPyObject(pyMatrixBuffer, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVStreamedMatrixBuffer to box.input[" << index << "].\n";
				Py_CLEAR(pyMatrixBuffer);
				return false;
			}
			Py_CLEAR(pyMatrixBuffer);
		}

		if (m_decoders[index]->isEndReceived())
		{
			PyObject* pyArg = PyTuple_New(2);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pyMatrixEnd = PyObject_Call(m_matrixEnd, pyArg, nullptr);
			if (pyMatrixEnd == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVStreamedMatrixEnd pyMatrixEnd.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pyArg);

			if (!appendToPyObject(pyMatrixEnd, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVStreamedMatrixEnd to box.input[" << index << "].\n";
				Py_CLEAR(pyMatrixEnd);
				return false;
			}
			Py_CLEAR(pyMatrixEnd);
		}
	}

	return true;
}

bool CBoxAlgorithmPython3::transferStreamedMatrixOutputChunksFromPython(const size_t index)
{
	IBoxIO& boxCtx  = this->getDynamicBoxContext();
	IMatrix* matrix = dynamic_cast<TStreamedMatrixEncoder<CBoxAlgorithmPython3>*>(m_encoders[index])->getInputMatrix();

	if (!PyList_Check(m_boxOutput))
	{
		this->getLogManager() << LogLevel_Error << "box.output must be a list.\n";
		return false;
	}

	PyObject* pyBuffer = PyList_GetItem(m_boxOutput, Py_ssize_t(index));
	if (pyBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.output[" << index << "].\n";
		return false;
	}

	size_t len;
	if (!getLenFromPyObject(pyBuffer, len))
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.output[" << index << "].__len__().\n";
		return false;
	}

	for (size_t idx = 0; idx < len; ++idx)
	{
		PyObject* pyChunk = PyObject_CallMethod(pyBuffer, "pop", nullptr);
		if (pyChunk == nullptr)
		{
			this->getLogManager() << LogLevel_Error << "Failed to get item " << idx << " of box.output[" << index << "].\n";
			return false;
		}

		if (PyObject_IsInstance(pyChunk, m_matrixHeader) == 1)
		{
			if (!setMatrixInfosFromPyObject(pyChunk, matrix))
			{
				this->getLogManager() << LogLevel_Error << "Failed setting matrix info.\n";
				Py_CLEAR(pyChunk);
				return false;
			}

			m_encoders[index]->encodeHeader();

			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else if (PyObject_IsInstance(pyChunk, m_matrixBuffer) == 1)
		{
			double* bufferBase = matrix->getBuffer();
			for (size_t i = 0; i < matrix->getBufferElementCount(); ++i) { bufferBase[i] = PyFloat_AsDouble(PyList_GetItem(pyChunk, i)); }

			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			m_encoders[index]->encodeBuffer();
			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else if (PyObject_IsInstance(pyChunk, m_matrixEnd) == 1)
		{
			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			m_encoders[index]->encodeEnd();
			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else
		{
			this->getLogManager() << LogLevel_Error << "Unexpected object type for item " << idx << " in box.output[" << index << "].\n";
			Py_CLEAR(pyChunk);
			return false;
		}

		Py_CLEAR(pyChunk);
	}
	return true;
}

bool CBoxAlgorithmPython3::transferSignalInputChunksToPython(const size_t index)
{
	IBoxIO& boxCtx = this->getDynamicBoxContext();

	if (!PyList_Check(m_boxInput))
	{
		this->getLogManager() << LogLevel_Error << "box.input must be a list.\n";
		return false;
	}

	PyObject* pyBuffer = PyList_GetItem(m_boxInput, Py_ssize_t(index));
	if (pyBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.input[" << index << "].\n";
		return false;
	}
	//Expose input signal chunks to python
	for (size_t idx = 0; idx < boxCtx.getInputChunkCount(index); ++idx)
	{
		m_decoders[index]->decode(idx);

		if (m_decoders[index]->isHeaderReceived())
		{
			IMatrix* matrix = dynamic_cast<TSignalDecoder<CBoxAlgorithmPython3> *>(m_decoders[index])->getOutputMatrix();
			size_t nDim     = matrix->getDimensionCount();

			PyObject* pySizeDim = PyList_New(nDim);
			if (pySizeDim == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new list pySizeDim.\n";
				return false;
			}

			PyObject* pyLabelDim = PyList_New(0);
			if (pyLabelDim == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new list pyLabelDim.\n";
				Py_CLEAR(pySizeDim);
				return false;
			}

			for (size_t i = 0; i < nDim; ++i)
			{
				size_t dimSize = matrix->getDimensionSize(i);
				if (PyList_SetItem(pySizeDim, i, PyLong_FromLong(dimSize)) == -1)
				{
					this->getLogManager() << LogLevel_Error << "Failed to set item " << i << " in dimension size list.\n";
					Py_CLEAR(pySizeDim);
					Py_CLEAR(pyLabelDim);
					return false;
				}
				for (size_t j = 0; j < dimSize; ++j)
				{
					if (PyList_Append(pyLabelDim, PyUnicode_FromString(matrix->getDimensionLabel(i, j))) == -1)
					{
						this->getLogManager() << LogLevel_Error << "Failed to append \"" << matrix->getDimensionLabel(i, j) << "\" in dimension label list.\n";
						Py_CLEAR(pySizeDim);
						Py_CLEAR(pyLabelDim);
						return false;
					}
				}
			}

			PyObject* pyArg = PyTuple_New(5);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 2, pySizeDim) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 2 (dimension size) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 3, pyLabelDim) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 3 (dimension label) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(
					pyArg, 4, PyLong_FromLong(long(dynamic_cast<TSignalDecoder<CBoxAlgorithmPython3>*>(m_decoders[index])->getOutputSamplingRate())))
				!= 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 4 (samplingRate) in tuple pyArg.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pySignalHeader = PyObject_Call(m_signalHeader, pyArg, nullptr);
			if (pySignalHeader == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVSignalHeader pySignalHeader.\n";
				Py_CLEAR(pySizeDim);
				Py_CLEAR(pyLabelDim);
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pySizeDim);
			Py_CLEAR(pyLabelDim);
			Py_CLEAR(pyArg);

			if (!appendToPyObject(pySignalHeader, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVSignalHeader to box.input[" << index << "].\n";
				Py_CLEAR(pySignalHeader);
				return false;
			}
			Py_CLEAR(pySignalHeader);
		}

		if (m_decoders[index]->isBufferReceived())
		{
			PyObject* pyArg = PyTuple_New(3);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (startTime) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (endTime) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 2, PyList_New(0)) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 2 (bufferElements) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pySignalBuffer = PyObject_Call(m_signalBuffer, pyArg, nullptr);
			if (pySignalBuffer == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVSignalBuffer pySignalBuffer.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pyArg);

			IMatrix* matrix    = dynamic_cast<TSignalDecoder<CBoxAlgorithmPython3> *>(m_decoders[index])->getOutputMatrix();
			double* bufferBase = matrix->getBuffer();
			for (size_t i = 0; i < matrix->getBufferElementCount(); ++i)
			{
				if (PyList_Append(pySignalBuffer, PyFloat_FromDouble(bufferBase[i])) == -1)
				{
					this->getLogManager() << LogLevel_Error << "Failed to append element " << i << " to pySignalBuffer.\n";
					Py_CLEAR(pySignalBuffer);
					return false;
				}
			}

			if (!appendToPyObject(pySignalBuffer, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVSignalBuffer to box.input[" << index << "].\n";
				Py_CLEAR(pySignalBuffer);
				return false;
			}
			Py_CLEAR(pySignalBuffer);
		}

		if (m_decoders[index]->isEndReceived())
		{
			PyObject* pyArg = PyTuple_New(2);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pySignalEnd = PyObject_Call(m_signalEnd, pyArg, nullptr);
			if (pySignalEnd == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new m_signalEnd pySignalEnd.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pyArg);

			if (!appendToPyObject(pySignalEnd, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVSignalEnd to box.input[" << index << "].\n";
				Py_CLEAR(pySignalEnd);
				return false;
			}
			Py_CLEAR(pySignalEnd);
		}
	}
	return true;
}

bool CBoxAlgorithmPython3::transferSignalOutputChunksFromPython(const size_t index)
{
	IBoxIO& boxCtx  = this->getDynamicBoxContext();
	IMatrix* matrix = dynamic_cast<TSignalEncoder<CBoxAlgorithmPython3>*>(m_encoders[index])->getInputMatrix();

	if (!PyList_Check(m_boxOutput))
	{
		this->getLogManager() << LogLevel_Error << "box.output must be a list.\n";
		return false;
	}

	PyObject* pyBuffer = PyList_GetItem(m_boxOutput, Py_ssize_t(index));
	if (pyBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.output[" << index << "].\n";
		return false;
	}

	size_t len;
	if (!getLenFromPyObject(pyBuffer, len))
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.output[" << index << "].__len__().\n";
		return false;
	}

	for (size_t idx = 0; idx < len; ++idx)
	{
		PyObject* pyChunk = PyObject_CallMethod(pyBuffer, "pop", nullptr);
		if (pyChunk == nullptr)
		{
			this->getLogManager() << LogLevel_Error << "Failed to get item " << idx << " of box.output[" << index << "].\n";
			return false;
		}

		if (PyObject_IsInstance(pyChunk, m_signalHeader) == 1)
		{
			if (!setMatrixInfosFromPyObject(pyChunk, matrix))
			{
				this->getLogManager() << LogLevel_Error << "Failed setting matrix info.\n";
				Py_CLEAR(pyChunk);
				return false;
			}

			PyObject* pySampling = PyObject_GetAttrString(pyChunk, "samplingRate");
			if (pySampling == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to load signal header sampling rate.\n";
				return false;
			}
			TParameterHandler<uint64_t>& sampling = dynamic_cast<TSignalEncoder<CBoxAlgorithmPython3>*>(m_encoders[index])->getInputSamplingRate();
			sampling                              = uint64_t(PyLong_AsLong(pySampling));
			m_encoders[index]->encodeHeader();
			Py_CLEAR(pySampling);

			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else if (PyObject_IsInstance(pyChunk, m_signalBuffer) == 1)
		{
			double* bufferBase = matrix->getBuffer();
			for (size_t i = 0; i < matrix->getBufferElementCount(); ++i) { bufferBase[i] = PyFloat_AsDouble(PyList_GetItem(pyChunk, i)); }

			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			m_encoders[index]->encodeBuffer();
			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else if (PyObject_IsInstance(pyChunk, m_signalEnd) == 1)
		{
			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			m_encoders[index]->encodeEnd();
			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else
		{
			this->getLogManager() << LogLevel_Error << "Unexpected object type for item " << idx << " in box.output[" << index << "].\n";
			Py_CLEAR(pyChunk);
			return false;
		}

		Py_CLEAR(pyChunk);
	}
	return true;
}

bool CBoxAlgorithmPython3::transferStimulationInputChunksToPython(const size_t index)
{
	IBoxIO& boxCtx     = this->getDynamicBoxContext();
	PyObject* pyBuffer = PyList_GetItem(m_boxInput, Py_ssize_t(index));
	if (pyBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.input[" << index << "].\n";
		return false;
	}
	for (size_t idx = 0; idx < boxCtx.getInputChunkCount(index); ++idx)
	{
		m_decoders[index]->decode(idx);

		if (m_decoders[index]->isHeaderReceived())
		{
			PyObject* pyArg = PyTuple_New(2);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pyStimHeader = PyObject_Call(m_stimulationHeader, pyArg, nullptr);
			if (pyStimHeader == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVStimulationHeader pyStimHeader.\n";
				PyErr_Print();
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pyArg);

			if (!appendToPyObject(pyStimHeader, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVStimulationHeader to box.input[" << index << "].\n";
				Py_CLEAR(pyStimHeader);
				return false;
			}
			Py_CLEAR(pyStimHeader);
		}

		if (m_decoders[index]->isBufferReceived())
		{
			IStimulationSet* stimSet = dynamic_cast<TStimulationDecoder<CBoxAlgorithmPython3>*>(m_decoders[index])->getOutputStimulationSet();

			PyObject* pyArg = PyTuple_New(2);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pyStimSet = PyObject_Call(m_stimulationSet, pyArg, nullptr);
			if (pyStimSet == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVStimulationSet pyStimSet.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pyArg);

			for (size_t i = 0; i < stimSet->getStimulationCount(); ++i)
			{
				const uint64_t id       = stimSet->getStimulationIdentifier(i);
				const uint64_t date     = stimSet->getStimulationDate(i);
				const uint64_t duration = stimSet->getStimulationDuration(i);

				pyArg = PyTuple_New(3);
				if (pyArg == nullptr)
				{
					this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
					return false;
				}
				if (PyTuple_SetItem(pyArg, 0, PyLong_FromLong(long(id))) != 0)
				{
					this->getLogManager() << LogLevel_Error << "Failed to set item 0 (identifier) in tuple pyArg.\n";
					Py_CLEAR(pyArg);
					return false;
				}
				if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(date).toSeconds())) != 0)
				{
					this->getLogManager() << LogLevel_Error << "Failed to set item 1 (date) in tuple pyArg.\n";
					Py_CLEAR(pyArg);
					return false;
				}
				if (PyTuple_SetItem(pyArg, 2, PyFloat_FromDouble(CTime(duration).toSeconds())) != 0)
				{
					this->getLogManager() << LogLevel_Error << "Failed to set item 2 (duration) in tuple pyArg.\n";
					Py_CLEAR(pyArg);
					return false;
				}

				PyObject* pyStim = PyObject_Call(m_stimulation, pyArg, nullptr);
				if (pyStim == nullptr)
				{
					this->getLogManager() << LogLevel_Error << "Failed to create a new OVStimulation pyStim.\n";
					Py_CLEAR(pyArg);
					return false;
				}
				Py_CLEAR(pyArg);

				if (!appendToPyObject(pyStim, pyStimSet))
				{
					this->getLogManager() << LogLevel_Error << "Failed to append stimulation to box.input[" << index << "].\n";
					Py_CLEAR(pyStim);
					return false;
				}
				Py_CLEAR(pyStim);
			}

			if (!appendToPyObject(pyStimSet, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append stimulation to box.input[" << index << "].\n";
				Py_CLEAR(pyStimSet);
				return false;
			}
			Py_CLEAR(pyStimSet);
		}

		if (m_decoders[index]->isEndReceived())
		{
			PyObject* pyArg = PyTuple_New(2);
			if (pyArg == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new tuple pyArg.\n";
				return false;
			}
			if (PyTuple_SetItem(pyArg, 0, PyFloat_FromDouble(CTime(boxCtx.getInputChunkStartTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 0 (start time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			if (PyTuple_SetItem(pyArg, 1, PyFloat_FromDouble(CTime(boxCtx.getInputChunkEndTime(index, idx)).toSeconds())) != 0)
			{
				this->getLogManager() << LogLevel_Error << "Failed to set item 1 (end time) in tuple pyArg.\n";
				Py_CLEAR(pyArg);
				return false;
			}

			PyObject* pyStimEnd = PyObject_Call(m_stimulationEnd, pyArg, nullptr);
			if (pyStimEnd == nullptr)
			{
				this->getLogManager() << LogLevel_Error << "Failed to create a new OVStimulationEnd pyStimEnd.\n";
				Py_CLEAR(pyArg);
				return false;
			}
			Py_CLEAR(pyArg);

			if (!appendToPyObject(pyStimEnd, pyBuffer))
			{
				this->getLogManager() << LogLevel_Error << "Failed to append an OVStimulationEnd to box.input[" << index << "].\n";
				Py_CLEAR(pyStimEnd);
				return false;
			}
			Py_CLEAR(pyStimEnd);
		}
	}
	return true;
}

bool CBoxAlgorithmPython3::transferStimulationOutputChunksFromPython(const size_t index)
{
	IBoxIO& boxCtx           = this->getDynamicBoxContext();
	IStimulationSet* stimSet = dynamic_cast<TStimulationEncoder<CBoxAlgorithmPython3>*>(m_encoders[index])->getInputStimulationSet();

	if (!PyList_Check(m_boxOutput))
	{
		this->getLogManager() << LogLevel_Error << "box.output must be a list.\n";
		return false;
	}

	PyObject* pyBuffer = PyList_GetItem(m_boxOutput, Py_ssize_t(index));
	if (pyBuffer == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.output[" << index << "].\n";
		return false;
	}

	size_t len;
	if (!getLenFromPyObject(pyBuffer, len))
	{
		this->getLogManager() << LogLevel_Error << "Failed to get box.output[" << index << "].__len__().\n";
		return false;
	}

	for (size_t idx = 0; idx < len; ++idx)
	{
		PyObject* pyChunk = PyObject_CallMethod(pyBuffer, "pop", nullptr);
		if (pyChunk == nullptr)
		{
			this->getLogManager() << LogLevel_Error << "Failed to get item " << idx << " of box.output[" << index << "].\n";
			return false;
		}

		if (PyObject_IsInstance(pyChunk, m_stimulationHeader) == 1)
		{
			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			stimSet->setStimulationCount(0);
			m_encoders[index]->encodeHeader();
			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else if (PyObject_IsInstance(pyChunk, m_stimulationSet) == 1)
		{
			size_t chunkLen;
			if (!getLenFromPyObject(pyChunk, chunkLen))
			{
				this->getLogManager() << LogLevel_Error << "Failed to get stimulations set length.\n";
				return false;
			}

			stimSet->setStimulationCount(0);
			for (size_t i = 0; i < chunkLen; ++i)
			{
				PyObject* pyStim = PyList_GetItem(pyChunk, Py_ssize_t(i));
				if (pyStim == nullptr)
				{
					this->getLogManager() << LogLevel_Error << "Failed to get item " << i << " of chunk " << idx << ".\n";
					return false;
				}
				if (PyObject_IsInstance(pyStim, m_stimulation) < 1)
				{
					this->getLogManager() << LogLevel_Error << "Item " << i << " is not an OVStimulation.\n";
					return false;
				}

				PyObject* pyID    = PyObject_GetAttrString(pyStim, "identifier");
				const uint64_t id = uint64_t(PyFloat_AsDouble(pyID));
				Py_CLEAR(pyID);

				uint64_t date, duration;
				getTimeFromPyObject(pyStim, "date", date);
				getTimeFromPyObject(pyStim, "duration", duration);

				stimSet->appendStimulation(id, date, duration);
			}

			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			m_encoders[index]->encodeBuffer();
			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else if (PyObject_IsInstance(pyChunk, m_stimulationEnd) == 1)
		{
			uint64_t startTime, endTime;
			getTimesFromPyObject(pyChunk, startTime, endTime);

			m_encoders[index]->encodeEnd();
			boxCtx.markOutputAsReadyToSend(index, startTime, endTime);
		}

		else
		{
			this->getLogManager() << LogLevel_Error << "Unexpected object type for item " << idx << " in box.output[" << index << "].\n";
			Py_CLEAR(pyChunk);
			return false;
		}

		Py_CLEAR(pyChunk);
	}
	return true;
}

bool CBoxAlgorithmPython3::process()
{
	const IBox& boxCtx = this->getStaticBoxContext();
	CIdentifier typeID;

	for (size_t i = 0; i < boxCtx.getInputCount(); ++i)
	{
		boxCtx.getInputType(i, typeID);
		if (typeID == OV_TypeId_StreamedMatrix) { if (!transferStreamedMatrixInputChunksToPython(i)) { return false; } }
		else if (typeID == OV_TypeId_Signal) { if (!transferSignalInputChunksToPython(i)) { return false; } }
		else if (typeID == OV_TypeId_Stimulations) { if (!transferStimulationInputChunksToPython(i)) { return false; } }
			// else if (typeID == OV_TypeId_FeatureVector) { }
// else if (typeID == OV_TypeId_Spectrum) { }
// else if (typeID == OV_TypeId_ChannelLocalisation) { }
			// else if (typeID == OV_TypeId_ExperimentInfo) { }
		else
		{
			this->getLogManager() << LogLevel_Error << "Codec to decode " << typeID.str() << " is not implemented.\n";
			return false;
		}
	}

	//update the python current time
	m_boxCurrentTime = PyFloat_FromDouble(CTime(this->getPlayerContext().getCurrentTime()).toSeconds());
	if (m_boxCurrentTime == nullptr)
	{
		this->getLogManager() << LogLevel_Error << "Failed to convert the current time into a PyFloat during update.\n";
		return false;
	}
	if (PyObject_SetAttrString(m_box, "_currentTime", m_boxCurrentTime) == -1)
	{
		this->getLogManager() << LogLevel_Error << "Failed to update \"box._currentTime\" attribute.\n";
		return false;
	}

	//call the python process function
	if (m_boxProcess && PyCallable_Check(m_boxProcess))
	{
		PyObject* result       = PyObject_CallObject(m_boxProcess, nullptr);
		const bool stdoutError = logSysStdout();
		const bool stderrError = logSysStderr();
		if ((result == nullptr) || (!stdoutError) || (!stderrError))
		{
			if (result == nullptr) { this->getLogManager() << LogLevel_Error << "Failed to call \"box.__process\" function.\n"; }
			if (!stdoutError) { this->getLogManager() << LogLevel_Error << "logSysStdout() failed during box.__process.\n"; }
			if (!stderrError) { this->getLogManager() << LogLevel_Error << "logSysStderr() failed during box.__process.\n"; }
			Py_CLEAR(result);
			return false;
		}
		Py_CLEAR(result);
	}

	for (size_t i = 0; i < boxCtx.getOutputCount(); ++i)
	{
		boxCtx.getOutputType(i, typeID);
		if (typeID == OV_TypeId_StreamedMatrix) { if (!transferStreamedMatrixOutputChunksFromPython(i)) { return false; } }
		else if (typeID == OV_TypeId_Signal) { if (!transferSignalOutputChunksFromPython(i)) { return false; } }
		else if (typeID == OV_TypeId_Stimulations) { if (!transferStimulationOutputChunksFromPython(i)) { return false; } }
			// else if (typeID == OV_TypeId_FeatureVector) { }
// else if (typeID == OV_TypeId_Spectrum) { }
// else if (typeID == OV_TypeId_ChannelLocalisation) { }
			// else if (typeID == OV_TypeId_ExperimentInfo) { }
		else
		{
			this->getLogManager() << LogLevel_Error << "Codec to encode " << typeID.str() << " is not implemented.\n";
			return false;
		}
	}
	return true;
}

#endif // #if defined(PY_MAJOR_VERSION) && (PY_MAJOR_VERSION == 3)
#endif // TARGET_HAS_ThirdPartyPython3
#endif
