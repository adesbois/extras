#if defined TARGET_HAS_ThirdPartyPython3 && !(defined(WIN32) && defined(TARGET_BUILDTYPE_Debug))
// Windows debug build doesn't typically link as most people don't have the python debug library.

#include "box-algorithms/ovpCBoxAlgorithmPython3.h"

#if defined(PY_MAJOR_VERSION) && (PY_MAJOR_VERSION == 3)

#ifdef TARGET_OS_Windows
#include "windows.h"
#endif

#include <string>
#include <iostream>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;
using namespace std;

//-------------------------------------------------------------------------------------------------
class CPython3Initializer
{
public:
	CPython3Initializer();
	~CPython3Initializer();
	bool isPython3Available() const { return m_pythonAvailable; }
private:
#ifdef TARGET_OS_Windows
	static bool checkPython3Path();
#endif
	// PyThreadState *m_pMainPyThreadState;
	bool m_pythonAvailable;
};

#ifdef TARGET_OS_Windows
bool CPython3Initializer::checkPython3Path()
{
	wstring ws(Py_GetPath());
	string path(ws.begin(), ws.end());

	size_t found = path.find_first_of(';');
	while (found != string::npos)
	{
		if (found > 0)
		{
			string filename   = path.substr(0, found);
			const bool exists = (_access(filename.c_str(), 0) == 0);
			if (exists) { return true; }
		}
		path  = path.substr(found + 1);
		found = path.find_first_of(';');
	}

	cout << "Python directory not found. You probably have a corrupted python installation!" << endl;
	cout << "The tried path from Py_GetPath() was [" << Py_GetPath() << "]\n";

	return false;
}
#endif

CPython3Initializer::CPython3Initializer() : m_pythonAvailable(false)
{
#ifdef TARGET_OS_Windows
	__try
	{
		// We do not care about the last file, since it is the OpenViBE runtime path
		if (!Py_IsInitialized() && checkPython3Path())
		{
			Py_Initialize();
			m_pythonAvailable = true;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER) { }
#else
		if (!Py_IsInitialized())
		{
			Py_Initialize();
			m_pythonAvailable = true;
		}
#endif
}

CPython3Initializer::~CPython3Initializer()
{
	if (m_pythonAvailable)
	{
		m_pythonAvailable = false;
		Py_Finalize();
	}
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
OVP_Declare_Begin()
	static CPython3Initializer python3Init;
	if (python3Init.isPython3Available()) { OVP_Declare_New(Python::CBoxAlgorithmPython3Desc); }
OVP_Declare_End()
//-------------------------------------------------------------------------------------------------

#else
#pragma message ("WARNING: Python 3.x headers are required to build the Python plugin, different includes found, skipped")
#endif // #if defined(PY_MAJOR_VERSION) && (PY_MAJOR_VERSION == 2)
#endif // TARGET_HAS_ThirdPartyPython3
