# ---------------------------------
# Finds Python
# Adds library to target
# Adds include path
# ---------------------------------

GET_PROPERTY(OV_PRINTED GLOBAL PROPERTY OV_TRIED_ThirdPartyPython3)

SET(Python_ADDITIONAL_VERSIONS 3.7)

# Stops lookup as soon as a version satisfying the version constraints is found.
# Important when building both 32bits andd 64bits versions on the same machine.
# CMake might go for the 64bits version of Python when building 32bits, making the compilation fail.
# To work, you need to ensure that 32bits versions of Python appearing first in the Path.
# This is only needed for windows.
SET(Python3_FIND_STRATEGY LOCATION)

FIND_PACKAGE(Python3 COMPONENTS Development)

IF(Python3_FOUND)
	OV_PRINT(OV_PRINTED "  Found Python 3 at ${Python3_LIBRARIES}")
	INCLUDE_DIRECTORIES(${Python3_INCLUDE_DIRS})
	TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${Python3_LIBRARIES})
	
	IF(WIN32)
		# These are needed not to cause a popup on machines missing the dll 
		TARGET_LINK_LIBRARIES(${PROJECT_NAME} optimized Delayimp )
		SET_TARGET_PROPERTIES(${PROJECT_NAME} PROPERTIES LINK_FLAGS "/DELAYLOAD:python37.dll")
	ENDIF()
	
	ADD_DEFINITIONS(-DTARGET_HAS_ThirdPartyPython3)
ELSE()
	OV_PRINT(OV_PRINTED "  FAILED to find Python 3 (needs v3.7 with bitness matching build target ${PLATFORM_TARGET})")
ENDIF()

SET_PROPERTY(GLOBAL PROPERTY OV_TRIED_ThirdPartyPython3 "Yes")

