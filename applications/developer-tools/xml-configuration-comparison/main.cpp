///-------------------------------------------------------------------------------------------------
/// 
/// \brief Classes of the Box Features Selection Trainer.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 14/02/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#include <fstream>
#include <iostream>
#include <string>
#include "tinyxml2.h"
#include <algorithm>

using namespace std;
using namespace tinyxml2;


// validation Test take in the algorithm output file, the expected output file (reference) and a tolerance threshold
bool comparison(const char* file1, const char* file2, bool removeSpace)
{
	// Load Files
	XMLDocument xmlDoc1, xmlDoc2;
	if (xmlDoc1.LoadFile(file1) != 0)	// Check File 1 Exist and Loading
	{
		cerr << "Error during loading File." << endl;
		return false;
	}
	if (xmlDoc2.LoadFile(file2) != 0)	// Check File 2 Exist and Loading
	{
		cerr << "Error during loading Reference." << endl;
		return false;
	}

	// Load Roots
	XMLNode *root1 = xmlDoc1.FirstChild(), *root2 = xmlDoc2.FirstChild();	// Get Root Node
	if (root1 == nullptr)				// Check Root Node Exist
	{
		cerr << "File format not compatible." << endl;
		return false;
	}
	if (root2 == nullptr)				// Check Root Node Exist
	{
		cerr << "Reference format not compatible." << endl;
		return false;
	}

	// Count Settings
	size_t n1 = 0, n2 = 0;
	for (XMLElement* e = root1->FirstChildElement(); e != nullptr; e = e->NextSiblingElement()) { n1++; }
	for (XMLElement* e = root2->FirstChildElement(); e != nullptr; e = e->NextSiblingElement()) { n2++; }
	if (n1 != n2)
	{
		cerr << "File and Reference are not same number of features." << endl;
		return false;
	}

	// Load settings
	XMLElement *e1 = root1->FirstChildElement(), *e2 = root2->FirstChildElement();
	for (size_t i = 0; i < n1; ++i, e1 = e1->NextSiblingElement(), e2 = e2->NextSiblingElement())
	{
		string s1(e1->GetText()), s2(e2->GetText());
		cout << s1 << endl << s2 << endl;
		if (removeSpace)
		{
			s1.erase(std::remove_if(s1.begin(), s1.end(), [](const unsigned char x) { return std::isspace(x); }), s1.end());
			s2.erase(std::remove_if(s2.begin(), s2.end(), [](const unsigned char x) { return std::isspace(x); }), s2.end());
		}
		cout << s1 << endl << s2 << endl;

		if (s1 != s2)
		{
			cerr << "Setting number " << i << " is different, For File : \n" << s1 << "\nfor Reference : \n" << s2 << endl;
			return false;
		}
	}

	return true;
}

int main(const int argc, char* argv[])
{
	if (argc != 4)
	{
		cout << "Usage: " << argv[0] << " <file> <reference> <remove space (true or false)>\n";
		return -1;
	}

	bool remove;
	if (strcmp(argv[3], "true") == 0) { remove = true; }
	else if (strcmp(argv[3], "false") == 0) { remove = false; }
	else
	{
		cout << "Usage:  <remove space> must be true or false not " << argv[3] << endl;
		return -1;
	}

	if (!comparison(argv[1], argv[2], remove))
	{
		cout << "Algorithm failed validation test \n" << endl;
		return 1;
	}

	cout << "Test passed\n" << endl;
	return 0;
}
