#pragma once

#include "CPluginObjectDescEnum.hpp"

#include <fstream>
#include <map>

class CAlgorithmGlobalDefinesGen final : public CPluginObjectDescEnum
{
public:

	CAlgorithmGlobalDefinesGen(const OpenViBE::Kernel::IKernelContext& ctx, const std::string& filename);
	~CAlgorithmGlobalDefinesGen() override;
	bool callback(const OpenViBE::Plugins::IPluginObjectDesc& pluginObjectDesc) override;

protected:

	std::ofstream m_file;

	std::map<OpenViBE::CIdentifier, std::string> m_usedIDs;
	// Adds the define to m_usedIdentifiers and m_File
	void addIdentifier(const std::string& objectName, const OpenViBE::CIdentifier& candidate, const std::string& spaces);
};
