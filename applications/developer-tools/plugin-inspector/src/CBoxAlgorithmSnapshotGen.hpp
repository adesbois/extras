#pragma once

#include "CPluginObjectDescEnum.hpp"

#include <map>
#include <vector>
#include <string>

#include <gtk/gtk.h>


// ------------------------------------------------------------------------------------------------------------------------------------
class CBoxAlgorithmSnapshotGen final : public CPluginObjectDescEnum
{
public:

	CBoxAlgorithmSnapshotGen(const OpenViBE::Kernel::IKernelContext& ctx, const std::string& snapshotDir, const std::string& docTemplateDir);
	~CBoxAlgorithmSnapshotGen() override;
	bool callback(const OpenViBE::Plugins::IPluginObjectDesc& pod) override;

protected:

	std::string m_snapshotDir;
	std::string m_docTemplateDir;
	std::vector<std::pair<std::string, std::string>> m_categories;
	std::map<EColors, GdkColor> m_colors;
	GtkWidget* m_window                     = nullptr;
	GtkWidget* m_widget                     = nullptr;
	OpenViBE::CIdentifier m_scenarioID      = OV_UndefinedIdentifier;
	OpenViBE::Kernel::IScenario* m_scenario = nullptr;
};
