#pragma once

#include "CPluginObjectDescEnum.hpp"

#include <map>
#include <vector>
#include <string>

#include <gtk/gtk.h>

// ------------------------------------------------------------------------------------------------------------------------------------
class CAlgorithmSnapshotGen final : public CPluginObjectDescEnum
{
public:

	CAlgorithmSnapshotGen(const OpenViBE::Kernel::IKernelContext& ctx, const std::string& snapshotDir, const std::string& docTemplateDir);
	~CAlgorithmSnapshotGen() override;
	bool callback(const OpenViBE::Plugins::IPluginObjectDesc& pod) override;

protected:

	std::string m_snapshotDir;
	std::string m_docTemplateDir;
	std::vector<std::pair<std::string, std::string>> m_categories;
	std::map<EColors, GdkColor> m_colors;
	GtkWidget* m_window = nullptr;
	GtkWidget* m_widget = nullptr;
};
