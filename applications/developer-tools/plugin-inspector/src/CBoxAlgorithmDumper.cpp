#include "CBoxAlgorithmDumper.hpp"

#include <iostream>
#include <fstream>

using namespace std;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;


// ------------------------------------------------------------------------------------------------------------------------------------
CBoxAlgorithmDumper::CBoxAlgorithmDumper(const IKernelContext& ctx, const string& outFile)
	: CPluginObjectDescEnum(ctx)
{
	if (outFile.empty()) { m_writeToFile = false; }
	if (m_writeToFile) { m_file.open(outFile.c_str()); }
}
// ------------------------------------------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------------------------------------------
bool CBoxAlgorithmDumper::callback(const IPluginObjectDesc& pluginObjectDesc)
{
	const CIdentifier boxID = pluginObjectDesc.getCreatedClassIdentifier();
	(m_writeToFile ? m_file : std::cout) << "BoxAlgorithm " << transform(pluginObjectDesc.getName().toASCIIString()) << " " << boxID.str() << "\n";
	return true;
}
