#include "CPluginObjectDescEnum.hpp"

#include <cctype>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;

bool CPluginObjectDescEnum::enumeratePluginObjectDesc()
{
	CIdentifier id;
	while ((id = m_kernelCtx.getPluginManager().getNextPluginObjectDescIdentifier(id)) != OV_UndefinedIdentifier)
	{
		this->callback(*m_kernelCtx.getPluginManager().getPluginObjectDesc(id));
	}
	return true;
}

bool CPluginObjectDescEnum::enumeratePluginObjectDesc(const CIdentifier& parentClassID)
{
	CIdentifier id;
	while ((id = m_kernelCtx.getPluginManager().getNextPluginObjectDescIdentifier(id, parentClassID)) !=
		   OV_UndefinedIdentifier) { this->callback(*m_kernelCtx.getPluginManager().getPluginObjectDesc(id)); }
	return true;
}

std::string CPluginObjectDescEnum::transform(const std::string& in, const bool removeSlash)
{
	std::string tmp(in);
	std::string out;
	bool lastWasSeparator = true;

	for (std::string::size_type i = 0; i < tmp.length(); ++i)
	{
		if (std::isalpha(tmp[i]) || std::isdigit(tmp[i]) || (!removeSlash && tmp[i] == '/'))
		{
			if (tmp[i] == '/') { out += "_"; }
			else { out += lastWasSeparator ? std::toupper(tmp[i]) : tmp[i]; }
			lastWasSeparator = false;
		}
		else
		{
			//if(!lastWasSeparator) { dst+="_"; }
			lastWasSeparator = true;
		}
	}
	return out;
}
