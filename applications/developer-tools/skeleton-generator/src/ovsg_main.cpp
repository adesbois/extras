#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include <iostream>
#include <clocale> // std::setlocale

#include "ovsgCDriverSkeletonGenerator.h"
#include "ovsgCBoxAlgorithmSkeletonGenerator.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/SkeletonGenerator;

using namespace std;

int main(int argc, char** argv)
{
	CKernelLoader loader;

	cout << "[  INF  ] Created kernel loader, trying to load kernel module" << endl;
	CString err;
#if defined TARGET_OS_Windows
	const CString file = Directories::getLibDir() + "/openvibe-kernel.dll";
#else
	const CString file = Directories::getLibDir() + "/libopenvibe-kernel.so";
#endif
	if (!loader.load(file, &err)) { cout << "[ FAILED ] Error loading kernel (" << err << ")" << " from [" << file << "]\n"; }
	else
	{
		cout << "[  INF  ] Kernel module loaded, trying to get kernel descriptor" << endl;
		IKernelDesc* desc = nullptr;
		loader.initialize();
		loader.getKernelDesc(desc);
		if (!desc) { cout << "[ FAILED ] No kernel descriptor" << endl; }
		else
		{
			cout << "[  INF  ] Got kernel descriptor, trying to create kernel" << endl;
			IKernelContext* ctx = desc->createKernel("skeleton-generator", Directories::getDataDir() + "/kernel/openvibe.conf");
			if (!ctx || !ctx->initialize()) { cout << "[ FAILED ] No kernel created by kernel descriptor" << endl; }
			else
			{
				ctx->getConfigurationManager().addConfigurationFromFile(Directories::getDataDir() + "/kernel/openvibe.conf");
				ctx->getConfigurationManager().addConfigurationFromFile(Directories::getDataDir() + "/applications/skeleton-generator/skeleton-generator.conf");
				Toolkit::initialize(*ctx);
				IConfigurationManager& configMgr = ctx->getConfigurationManager();
				ctx->getPluginManager().addPluginsFromFiles(configMgr.expand("${Kernel_Plugins}"));
				gtk_init(&argc, &argv);

				// We rely on this with 64bit/gtk 2.24, to roll back gtk_init() sometimes switching
				// the locale to one where ',' is needed instead of '.' for separators of floats, 
				// causing issues, for example getConfigurationManager.expandAsFloat("0.05") -> 0; 
				// due to implementation by std::stod().
				std::setlocale(LC_ALL, "C");

				GtkBuilder* builder    = gtk_builder_new();
				const CString filename = Directories::getDataDir() + "/applications/skeleton-generator/generator-interface.ui";

				OV_ERROR_UNLESS(gtk_builder_add_from_file(builder, filename.toASCIIString(), nullptr), "Problem loading [" << filename << "]",
								ErrorType::BadFileRead, -1, ctx->getErrorManager(), ctx->getLogManager());

				//gtk_builder_connect_signals(builder, nullptr);

				GtkWidget* dialog = GTK_WIDGET(gtk_builder_get_object(builder, "sg-selection-dialog"));

				gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_OK, GTK_RESPONSE_OK);
				gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);

				GtkWidget* radioDriver = GTK_WIDGET(gtk_builder_get_object(builder, "sg-driver-selection-radio-button"));
				GtkWidget* radioAlgo   = GTK_WIDGET(gtk_builder_get_object(builder, "sg-algo-selection-radio-button"));
				GtkWidget* radioBox    = GTK_WIDGET(gtk_builder_get_object(builder, "sg-box-selection-radio-button"));


				CBoxAlgorithmSkeletonGenerator boxGenerator(*ctx, builder);
				CDriverSkeletonGenerator driverGenerator(*ctx, builder);

				const gint resp = gtk_dialog_run(GTK_DIALOG(dialog));

				if (resp == GTK_RESPONSE_OK)
				{
					gtk_widget_hide(GTK_WIDGET(dialog));

					if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioDriver))) { driverGenerator.initialize(); }
					OV_ERROR_UNLESS(!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioAlgo)), "NOT YET AVAILABLE.",
									ErrorType::Internal, 0, ctx->getErrorManager(), ctx->getLogManager());

					if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radioBox))) { boxGenerator.initialize(); }
					gtk_main();
				}
				else
				{
					std::cout << "User cancelled. Exit." << std::endl;
					return 0;
				}
			}
		}
	}

	loader.uninitialize();
	loader.unload();
	return 0;
}
