#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsICommandVRPNButton.h"
#include "ovamsCApplication.h"

#include <vrpn_Connection.h>
#include <vrpn_Button.h>

using namespace SSVEPMindShooter;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;


namespace {
void VRPN_CALLBACK ssvep_vrpn_callback_button(void* command, vrpn_BUTTONCB button)
{
	static_cast<ICommandVRPNButton*>(command)->execute(button.button, button.state);
}
}


ICommandVRPNButton::ICommandVRPNButton(CApplication* application, const CString& name)
	: ICommand(application)
{
	IConfigurationManager* configManager = application->getConfigurationManager();
	const std::string buttonName         = std::string(name.toASCIIString()) + std::string("@") + std::string(
											   (configManager->expand("${SSVEP_VRPNHost}")).toASCIIString());

	m_application->getLogManager() << LogLevel_Debug << "+ m_vrpnButton = new vrpn_Button_Remote(" << buttonName << ")\n";


	m_vrpnButton = new vrpn_Button_Remote(buttonName.c_str());
	m_vrpnButton->register_change_handler(static_cast<void*>(this), ssvep_vrpn_callback_button);
}

ICommandVRPNButton::~ICommandVRPNButton()
{
	m_application->getLogManager() << LogLevel_Debug << "- delete m_vrpnButton\n";
	delete m_vrpnButton;
}

void ICommandVRPNButton::processFrame() { m_vrpnButton->mainloop(); }

#endif
