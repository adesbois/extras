#pragma once

#include "ovamsICommandVRPNButton.h"

namespace SSVEPMindShooter {
class CCommandStimulatorControl final : public ICommandVRPNButton
{
public:
	explicit CCommandStimulatorControl(CApplication* application);
	~CCommandStimulatorControl() override { }

	void execute(const int button, const int state) override;
};
}  // namespace SSVEPMindShooter
