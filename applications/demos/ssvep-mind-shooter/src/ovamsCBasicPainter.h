#pragma once

/// this class is a wrapper around some ogre methods to display basic shapes

#include <Ogre.h>

#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
#include "Overlay/OgreOverlayManager.h"
#endif

#define SSVEP_DEFAULT_COLOUR Ogre::ColourValue(1.0f, 1.0f, 1.0f)

namespace SSVEPMindShooter {
class CApplication;

class CBasicPainter
{
public:
	explicit CBasicPainter(CApplication* application);
	~CBasicPainter() {}

	Ogre::SceneManager* getSceneManager() const { return m_sceneManager; }

	Ogre::ManualObject* paintRectangle(const Ogre::RealRect& oRectangle, const Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR, const int plane = 1) const;
	Ogre::ManualObject* paintTexturedRectangle(const Ogre::RealRect& oRectangle, const Ogre::String& sSurface, const int plane = 1) const;
	Ogre::ManualObject* paintTriangle(Ogre::Vector2 p1, Ogre::Vector2 p2, Ogre::Vector2 p3, Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR,
									  int plane                                                                     = 1) const;
	Ogre::ManualObject* paintTriangle(Ogre::Vector3 p1, Ogre::Vector3 p2, Ogre::Vector3 p3, Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR,
									  int plane                                                                     = 1) const;
	//Ogre::ManualObject* paintTexturedTriangle(Ogre::Vector2 p1, Ogre::Vector2 p2, Ogre::Vector2 p3, Ogre::String sSurface, int plane = 1);
	Ogre::ManualObject* paintCircle(Ogre::Real rX, Ogre::Real rY, Ogre::Real rR, Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR, bool bFilled = true,
									int plane                                                            = 1) const;

	Ogre::ManualObject* beginPainingPolygon(bool filled = true, const Ogre::String& material = "BasicSurface/Diffuse") const;
	static void addPointToPolygon(Ogre::ManualObject* pPolygon, Ogre::Real rX, Ogre::Real rY, Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR);
	void finishPaintingPolygon(Ogre::ManualObject* pPolygon, int plane = 1) const;

	void paintText(const std::string& sID, const std::string& sText, Ogre::Real rX, Ogre::Real rY, Ogre::Real rWidth, Ogre::Real rHeight,
				   const Ogre::ColourValue& color) const;

protected:
	CApplication* m_application                = nullptr;
	Ogre::OverlayManager* m_overlayManager     = nullptr;
	Ogre::OverlayContainer* m_overlayContainer = nullptr;
	Ogre::SceneManager* m_sceneManager         = nullptr;
	Ogre::AxisAlignedBox m_aabInf;
};
}  // namespace SSVEPMindShooter
