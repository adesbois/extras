#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandReceiveTarget.h"
#include "ovamsCApplication.h"

using namespace SSVEPMindShooter;

CCommandReceiveTarget::CCommandReceiveTarget(CApplication* application)
	: ICommandVRPNButton(application, "SSVEP_VRPN_TargetControl") {}

void CCommandReceiveTarget::execute(const int button, const int /*state*/) { dynamic_cast<CApplication*>(m_application)->setTarget(button); }

#endif
