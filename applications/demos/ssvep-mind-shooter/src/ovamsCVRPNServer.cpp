#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCVRPNServer.h"
#include "ovamsCApplication.h"

#include <vrpn_Connection.h>
#include <vrpn_Button.h>

using namespace SSVEPMindShooter;
using namespace OpenViBE::Kernel;

CVRPNServer* CVRPNServer::m_vrpnServerInstance = nullptr;
CApplication* CVRPNServer::m_application       = nullptr;

CVRPNServer::CVRPNServer(CApplication* application)
{
	m_application = application;

	const int port = int(m_application->getConfigurationManager()->expandAsInteger("${VRPN_ExternalServerPort}"));

	m_application->getLogManager() << LogLevel_Debug << "VRPN SERVER PORT :" << port << "\n";
	m_connection = vrpn_create_server_connection(port);
}

CVRPNServer* CVRPNServer::getInstance(CApplication* application)
{
	if (m_vrpnServerInstance == nullptr) { m_vrpnServerInstance = new CVRPNServer(application); }
	return m_vrpnServerInstance;
}

void CVRPNServer::addButton(const std::string& name, const int buttonCount)
{
	m_oButtonServer.insert(std::pair<std::string, vrpn_Button_Server*>(name, new vrpn_Button_Server(name.c_str(), m_connection, buttonCount)));
	m_oButtonCache[name].clear();
	m_oButtonCache[name].resize(buttonCount);
}

void CVRPNServer::processFrame()
{
	for (auto it = m_oButtonServer.begin(); it != m_oButtonServer.end(); ++it) { it->second->mainloop(); }
	m_connection->mainloop();
}

void CVRPNServer::changeButtonState(const std::string& name, const int iIndex, const int state)
{
	m_oButtonServer[name]->set_button(iIndex, state);
	m_oButtonCache[name][iIndex] = state;
}

int CVRPNServer::getButtonState(const std::string& name, const int iIndex) { return m_oButtonCache[name][iIndex]; }

#endif
