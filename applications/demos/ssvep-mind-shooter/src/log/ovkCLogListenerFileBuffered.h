#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>
#include <cstdio>

#define OVK_ClassId_Kernel_Log_LogListenerFileBuffered			OpenViBE::CIdentifier(0x0FD252FB, 0x315C1A97)

namespace OpenViBE {
namespace Kernel {
class CLogListenerFileBuffered final : public ILogListener
{
public:

	CLogListenerFileBuffered(const IKernelContext& context, const CString& applicationName, const CString& logFilename);
	~CLogListenerFileBuffered() override;

	bool isActive(const ELogLevel level) override;
	bool activate(const ELogLevel level, const bool active) override;
	bool activate(const ELogLevel startLevel, const ELogLevel endLevel, const bool active) override;
	bool activate(const bool active) override;

	void log(const CTime value) override;

	void log(const uint64_t value) override;
	void log(const uint32_t value) override;

	void log(const int64_t value) override;
	void log(const int value) override;

	void log(const double value) override;

	void log(const bool value) override;

	void log(const CIdentifier& value) override;
	void log(const CString& value) override;
	void log(const std::string& value) override;
	void log(const char* value) override;

	void log(const ELogLevel level) override;
	void log(const ELogColor /*color*/) override {}

	_IsDerivedFromClass_Final_(ILogListener, OVK_ClassId_Kernel_Log_LogListenerFileBuffered)

protected:

	std::map<ELogLevel, bool> m_activeLevels;
	CString m_applicationName;
	CString m_logFilename;

	FILE* m_f = nullptr;
};
}  // namespace Kernel
}  // namespace OpenViBE
