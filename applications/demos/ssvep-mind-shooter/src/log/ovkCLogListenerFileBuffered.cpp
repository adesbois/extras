#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovkCLogListenerFileBuffered.h"

#include <cstdio>
#include <iostream>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace std;


CLogListenerFileBuffered::CLogListenerFileBuffered(const IKernelContext& /*context*/, const CString& applicationName, const CString& logFilename)
	: m_applicationName(applicationName), m_logFilename(logFilename)
{
	m_f = fopen(m_logFilename.toASCIIString(), "wt");
	if (m_f == nullptr)
	{
		cout << "[  ERROR  ] Unable to open [" << m_logFilename << "] for writing. This log listener can not be initialized, it will do nothing!" << endl;
	}
}

CLogListenerFileBuffered::~CLogListenerFileBuffered() { if (m_f != nullptr) { fclose(m_f); } }

bool CLogListenerFileBuffered::isActive(const ELogLevel level)
{
	const auto it = m_activeLevels.find(level);
	if (it == m_activeLevels.end()) { return true; }
	return it->second;
}

bool CLogListenerFileBuffered::activate(const ELogLevel level, const bool active)
{
	m_activeLevels[level] = active;
	return true;
}

bool CLogListenerFileBuffered::activate(const ELogLevel startLevel, const ELogLevel endLevel, const bool active)
{
	for (int i = startLevel; i <= endLevel; ++i) { m_activeLevels[ELogLevel(i)] = active; }
	return true;
}

bool CLogListenerFileBuffered::activate(const bool active) { return activate(LogLevel_First, LogLevel_Last, active); }

void CLogListenerFileBuffered::log(const CTime value) { if (m_f != nullptr) { fprintf(m_f, "%llu", value.time()); } }
void CLogListenerFileBuffered::log(const uint64_t value) { if (m_f != nullptr) { fprintf(m_f, "%llu", value); } }
void CLogListenerFileBuffered::log(const uint32_t value) { if (m_f != nullptr) { fprintf(m_f, "%u", value); } }
void CLogListenerFileBuffered::log(const int64_t value) { if (m_f != nullptr) { fprintf(m_f, "%lli", value); } }
void CLogListenerFileBuffered::log(const int value) { if (m_f != nullptr) { fprintf(m_f, "%i", value); } }
void CLogListenerFileBuffered::log(const double value) { if (m_f != nullptr) { fprintf(m_f, "%lf", value); } }
void CLogListenerFileBuffered::log(const bool value) { if (m_f != nullptr) { fprintf(m_f, "%s", (value ? "true" : "false")); } }
void CLogListenerFileBuffered::log(const CIdentifier& value) { if (m_f != nullptr) { fprintf(m_f, "%s", value.str().c_str()); } }
void CLogListenerFileBuffered::log(const CString& value) { if (m_f != nullptr) { fprintf(m_f, "%s", value.toASCIIString()); } }
void CLogListenerFileBuffered::log(const std::string& value) { if (m_f != nullptr) { fprintf(m_f, "%s", value.c_str()); } }
void CLogListenerFileBuffered::log(const char* value) { if (m_f != nullptr) { fprintf(m_f, "%s", value); } }

void CLogListenerFileBuffered::log(const ELogLevel level)
{
	if (m_f != nullptr)
	{
		switch (level)
		{
			case LogLevel_Debug:
				fprintf(m_f, "[ DEBUG ] ");
				break;

			case LogLevel_Benchmark:
				fprintf(m_f, "[ BENCH ] ");
				break;

			case LogLevel_Trace:
				fprintf(m_f, "[ TRACE ] ");
				break;

			case LogLevel_Info:
				fprintf(m_f, "[  INF  ] ");
				break;

			case LogLevel_Warning:
				fprintf(m_f, "[WARNING] ");
				break;

			case LogLevel_ImportantWarning:
				fprintf(m_f, "[WARNING] ");
				break;

			case LogLevel_Error:
				fprintf(m_f, "[ ERROR ] ");
				break;

			case LogLevel_Fatal:
				fprintf(m_f, "[ FATAL ] ");
				break;

			default:
				fprintf(m_f, "[UNKNOWN] ");
				break;
		}
	}
}

#endif
