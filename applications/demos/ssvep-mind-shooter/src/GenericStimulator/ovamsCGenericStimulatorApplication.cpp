#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCGenericStimulatorApplication.h"

#include "../log/ovkCLogListenerFileBuffered.h"

#include "../ovamsCCommandStartStop.h"
#include "../ovamsCCommandStimulatorControl.h"
#include "../ovamsCCommandReceiveTarget.h"

using namespace Ogre;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace SSVEPMindShooter;

#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
	typedef CEGUI::UVector2 USize;
};
#endif

bool CGenericStimulatorApplication::setup(IKernelContext* poKernelContext)
{
	CApplication::setup(poKernelContext);

	(*m_logManager) << LogLevel_Debug << "  * CGenericStimulatorApplication::setup()\n";
	IConfigurationManager* configManager = &(m_kernelCtx->getConfigurationManager());
	configManager->addConfigurationFromFile(m_ScenarioDir + "/appconf/generic-configuration.conf");
	std::cout << m_ScenarioDir << "/appconf/generic-configuration.conf" << "\n";


	poKernelContext->getLogManager().activate(LogLevel_First, LogLevel_Last, true);

	ILogListener* logListenerFileBuffered = new CLogListenerFileBuffered(*poKernelContext, "ssvep-stimulator",
																		 configManager->expand("${SSVEP_UserDataFolder}/log-[$core{date}-$core{time}].log"));
	poKernelContext->getLogManager().addListener(logListenerFileBuffered);


	CGenericStimulatorFlickeringObject::initialize(this);

	// paint targets

	const size_t nTarget = size_t(configManager->expandAsUInteger("${SSVEP_TargetCount}"));

	for (size_t i = 0; i < nTarget; ++i) { this->addObject(CGenericStimulatorFlickeringObject::createGenericFlickeringObject(i)); }

	// draw the initial text
	m_instructionsReady = m_guiWindowManager->createWindow("TaharezLook/StaticImage", "InstructionsReady");
	m_instructionsReady->setPosition(CEGUI::UVector2(cegui_reldim(0.0F), cegui_reldim(0.0F)));
	m_instructionsReady->setSize(CEGUI::USize(CEGUI::UDim(0.0F, 640.F), CEGUI::UDim(0.0F, 32.F)));

#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(m_instructionsReady);
	CEGUI::ImageManager::getSingleton().addFromImageFile("InstructionsReady", "InstructionText-Start.png");
	m_instructionsReady->setProperty("Image", "InstructionsReady");
#else
	m_sheet->addChildWindow(m_instructionsReady);
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("InstructionsReady", "InstructionText-Start.png");
	m_instructionsReady->setProperty("Image", "set:InstructionsReady image:full_image");
#endif

	m_instructionsReady->setProperty("FrameEnabled", "False");
	m_instructionsReady->setProperty("BackgroundEnabled", "False");
	m_instructionsReady->setVisible(true);


	// initialize commands
	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStartStop(...)\n";
	this->addCommand(new CCommandStartStop(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStimulatorControl(...))\n";
	this->addCommand(new CCommandStimulatorControl(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CC(...))\n";
	this->addCommand(new CCommandReceiveTarget(this));

	(*m_logManager) << LogLevel_Debug << "  * CTrainerApplication::setup() completed successfully\n";

	return true;
}


void CGenericStimulatorApplication::processFrame(const size_t frame)
{
	CApplication::processFrame(frame);
	if (!m_active) { return; }
	for (size_t i = 0; i < m_oObjects.size(); ++i) { m_oObjects[i]->processFrame(); }
}

void CGenericStimulatorApplication::addObject(CGenericStimulatorFlickeringObject* obj)
{
	m_oObjects.push_back(obj);
	obj->setVisible(true);
}

void CGenericStimulatorApplication::setTarget(const int target)
{
	const size_t currentTime = int(time(nullptr) - m_ttStartTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Target set to " << target << "\n";

	for (int i = 0; i < int(m_oObjects.size()); ++i) { m_oObjects[i]->setTarget(target == i); }
}

void CGenericStimulatorApplication::startExperiment()
{
	CApplication::startExperiment();

	m_ttStartTime = time(nullptr);

	this->stopFlickering();
	m_instructionsReady->setVisible(false);
}

void CGenericStimulatorApplication::startFlickering()
{
	const size_t currentTime = size_t(time(nullptr) - m_ttStartTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Starting Visual Stimulation\n";
	m_active = true;
}

void CGenericStimulatorApplication::stopFlickering()
{
	const size_t currentTime = size_t(time(nullptr) - m_ttStartTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Stopping Visual Stimulation\n";
	m_active = false;

	for (size_t i = 0; i < m_oObjects.size(); ++i) { m_oObjects[i]->setVisible(true); }

	this->setTarget(-1);
}

#endif
