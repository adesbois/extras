#pragma once

#include <CEGUI.h>

#include "../ovamsCApplication.h"
#include "ovamsCGenericStimulatorFlickeringObject.h"

namespace SSVEPMindShooter {
class CGenericStimulatorApplication final : public CApplication
{
public:
	explicit CGenericStimulatorApplication(const OpenViBE::CString& sScenarioDir) : CApplication(sScenarioDir) {}
	~CGenericStimulatorApplication() override {}

	bool setup(OpenViBE::Kernel::IKernelContext* poKernelContext) override;
	void setTarget(int target) override;

	void startExperiment() override;
	void startFlickering() override;
	void stopFlickering() override;


private:
	bool m_active = false;
	void processFrame(const size_t frame) override;
	void addObject(CGenericStimulatorFlickeringObject* obj);

	std::vector<CGenericStimulatorFlickeringObject*> m_oObjects;

	time_t m_ttStartTime = 0;

	CEGUI::Window* m_instructionsReady = nullptr;
};
}  // namespace SSVEPMindShooter
