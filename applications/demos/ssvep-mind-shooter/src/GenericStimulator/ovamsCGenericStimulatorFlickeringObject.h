#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

#include "../ovamsCSSVEPFlickeringObject.h"
#include "../ovamsCBasicPainter.h"

/**
 */
namespace SSVEPMindShooter {
class CGenericStimulatorApplication;

class CGenericStimulatorFlickeringObject final : public CSSVEPFlickeringObject
{
public:
	static CGenericStimulatorFlickeringObject* createGenericFlickeringObject(size_t targetId);
	static void initialize(CGenericStimulatorApplication* application);

	static void connectToNode(Ogre::SceneNode* /*sceneNode*/) { }
	void setTarget(const bool isTarget) const { m_pointer->setVisible(isTarget); }
	bool isTarget() const { return m_pointer->getVisible(); }

private:
	static CGenericStimulatorApplication* m_application;
	static Ogre::SceneNode* m_parentNode;
	static CBasicPainter* m_painter;
	static float m_targetWidth;
	static float m_targetHeight;
	static Ogre::ColourValue m_lightColor;
	static Ogre::ColourValue m_darkColor;

	CGenericStimulatorFlickeringObject(float posX, float posY, Ogre::ColourValue color, const OpenViBE::CString& material, size_t stimulationPattern);


	Ogre::SceneNode* m_elementNode = nullptr;
	Ogre::MovableObject* m_pointer = nullptr;
};
}  // namespace SSVEPMindShooter
