#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCGenericStimulatorFlickeringObject.h"
#include "ovamsCGenericStimulatorApplication.h"

using namespace Ogre;
using namespace SSVEPMindShooter;

SceneNode* CGenericStimulatorFlickeringObject::m_parentNode                      = nullptr;
CBasicPainter* CGenericStimulatorFlickeringObject::m_painter                     = nullptr;
ColourValue CGenericStimulatorFlickeringObject::m_lightColor                     = ColourValue(1.0F, 1.0F, 1.0F);
ColourValue CGenericStimulatorFlickeringObject::m_darkColor                      = ColourValue(0.0F, 0.0F, 0.0F);
float CGenericStimulatorFlickeringObject::m_targetWidth                          = 0.2F;
float CGenericStimulatorFlickeringObject::m_targetHeight                         = 0.2F;
CGenericStimulatorApplication* CGenericStimulatorFlickeringObject::m_application = nullptr;

void CGenericStimulatorFlickeringObject::initialize(CGenericStimulatorApplication* application)
{
	m_application                                          = application;
	OpenViBE::Kernel::IConfigurationManager* configManager = application->getConfigurationManager();

	m_painter    = application->getPainter();
	m_parentNode = application->getSceneNode();

	m_targetWidth  = float(configManager->expandAsFloat("${SSVEP_TargetWidth}"));
	m_targetHeight = float(configManager->expandAsFloat("${SSVEP_TargetHeight}"));

	m_lightColor = ColourValue(float(configManager->expandAsFloat("${SSVEP_TargetLightColourRed}")),
							   float(configManager->expandAsFloat("${SSVEP_TargetLightColourGreen}")),
							   float(configManager->expandAsFloat("${SSVEP_TargetLightColourBlue}")));

	m_darkColor = ColourValue(float(configManager->expandAsFloat("${SSVEP_TargetDarkColourRed}")),
							  float(configManager->expandAsFloat("${SSVEP_TargetDarkColourGreen}")),
							  float(configManager->expandAsFloat("${SSVEP_TargetDarkColourBlue}")));

	m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "Target Size : ("
			<< m_targetWidth << ", " << m_targetHeight << ")\n";
	/*
	m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "Target Default Colour : Light = "
									 << Ogre::String(m_lightColor) << ", Dark = " << Ogre::String(m_darkColor) << "\n";
									 */
}

CGenericStimulatorFlickeringObject* CGenericStimulatorFlickeringObject::createGenericFlickeringObject(const size_t targetId)
{
	OpenViBE::Kernel::IConfigurationManager* configManager = m_application->getConfigurationManager();

	if (m_painter != nullptr)
	{
		const ColourValue currentTargetColour = (targetId == 0) ? m_darkColor : m_lightColor;

		const OpenViBE::CIdentifier id = configManager->createConfigurationToken("SSVEPTarget_Id", std::to_string(targetId).c_str());

		const float targetX              = float(configManager->expandAsFloat("${SSVEP_Target_X_${SSVEPTarget_Id}}"));
		const float targetY              = float(configManager->expandAsFloat("${SSVEP_Target_Y_${SSVEPTarget_Id}}"));
		const OpenViBE::CString material = OpenViBE::CString(configManager->expand("${SSVEP_Target_Material_${SSVEPTarget_Id}}"));
		const size_t stimulationPattern  = size_t((*(m_application->getFrequencies()))[targetId]);

		m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "Target (" << targetId << ") : Position = (" << targetX << ", " << targetY <<
				"), Texture = " << material << "\n";

		configManager->releaseConfigurationToken(id);

		return new CGenericStimulatorFlickeringObject(targetX, targetY, currentTargetColour, material, stimulationPattern);
	}
	m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Fatal << "TrainerTarget object was not properly initialized\n";
	return nullptr;
}

CGenericStimulatorFlickeringObject::CGenericStimulatorFlickeringObject(const float posX, const float posY, const ColourValue color,
																	   const OpenViBE::CString& material, const size_t stimulationPattern)
	: CSSVEPFlickeringObject(nullptr, stimulationPattern)
{
	MovableObject* litObject;
	MovableObject* darkObject;

	m_elementNode          = m_parentNode->createChildSceneNode();
	m_objectNode           = m_elementNode->createChildSceneNode();
	SceneNode* pointerNode = m_elementNode->createChildSceneNode();

	const RealRect rectangle(posX - m_targetWidth / 2, posY + m_targetHeight / 2, posX + m_targetWidth / 2, posY - m_targetHeight / 2);

	if (strcmp(material.toASCIIString(), "default") == 0)
	{
		litObject  = m_painter->paintRectangle(rectangle, color);
		darkObject = m_painter->paintRectangle(rectangle, m_darkColor);
	}
	else
	{
		litObject  = m_painter->paintTexturedRectangle(rectangle, std::string("GenericSurface/").append(material));
		darkObject = m_painter->paintTexturedRectangle(rectangle, std::string("GenericSurface/").append(material).append("-dark"));
	}

	m_objectNode->attachObject(litObject);
	litObject->setVisible(true);

	m_objectNode->attachObject(darkObject);
	darkObject->setVisible(false);

	m_pointer = m_painter->paintTriangle(Vector2(posX - 0.05F, posY + m_targetHeight), Vector2(posX, posY + m_targetHeight - 0.05F),
										 Vector2(posX + 0.05F, posY + m_targetHeight), ColourValue(1, 1, 0));

	pointerNode->attachObject(m_pointer);
	m_pointer->setVisible(false);
}

#endif
