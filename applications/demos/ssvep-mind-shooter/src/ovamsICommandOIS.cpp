#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsICommandOIS.h"
#include "ovamsCApplication.h"

#define SSVEP_NO_MOUSE_CAPTURE

using namespace SSVEPMindShooter;
using namespace OpenViBE::Kernel;
using namespace OIS;

InputManager* ICommandOIS::m_inputManager = nullptr;
Keyboard* ICommandOIS::m_keyboard         = nullptr;
Mouse* ICommandOIS::m_mouse               = nullptr;

int ICommandOIS::m_nInstance = 0;
std::vector<ICommandOIS*> ICommandOIS::m_instances;


ICommandOIS::ICommandOIS(CApplication* application)
	: ICommand(application)
{
	if (m_keyboard == nullptr)
	{
		ParamList paramList;
		std::ostringstream windowHandleString;
		size_t windowHandle;


		m_application->getWindow()->getCustomAttribute("WINDOW", &windowHandle);

		windowHandleString << windowHandle;

		paramList.insert(std::make_pair(std::string("WINDOW"), windowHandleString.str()));
		paramList.insert(std::make_pair(std::string("x11_keyboard_grab"), "false"));

		m_inputManager = InputManager::createInputSystem(paramList);

		m_keyboard = dynamic_cast<Keyboard*>(m_inputManager->createInputObject(OISKeyboard, true));
		m_keyboard->setEventCallback(this);

#ifndef SSVEP_NO_MOUSE_CAPTURE
		m_mouse = static_cast<OIS::Mouse*>(m_inputManager->createInputObject(OIS::OISMouse, true));
		m_mouse->setEventCallback( this );
#endif
	}

	m_nInstance++;
	m_instances.push_back(this);
}

ICommandOIS::~ICommandOIS()
{
	--m_nInstance;

	if (m_nInstance == 0)
	{
		if (m_inputManager != nullptr)
		{
			if (m_keyboard != nullptr)
			{
				m_application->getLogManager() << LogLevel_Debug << "- destroy m_keyboard\n";
				m_inputManager->destroyInputObject(m_keyboard);
				m_keyboard = nullptr;
#ifndef SSVEP_NO_MOUSE_CAPTURE
				m_application->getLogManager() << LogLevel_Debug << "- destroy m_mouse\n";
				m_inputManager->destroyInputObject( m_mouse );
				m_mouse = nullptr;
#endif
			}

			InputManager::destroyInputSystem(m_inputManager);
		}
	}
}

void ICommandOIS::processFrame()
{
	m_keyboard->capture();
#ifndef SSVEP_NO_MOUSE_CAPTURE
	m_mouse->capture();
#endif
}


bool ICommandOIS::keyPressed(const KeyEvent& oEvent)
{
	for (size_t i = 0; i < m_instances.size(); ++i) { m_instances[i]->receiveKeyPressedEvent(oEvent.key); }
	return true;
}

bool ICommandOIS::keyReleased(const KeyEvent& oEvent)
{
	for (size_t i = 0; i < m_instances.size(); ++i) { m_instances[i]->receiveKeyReleasedEvent(oEvent.key); }
	return true;
}

#ifndef SSVEP_NO_MOUSE_CAPTURE
bool ICommandOIS::mouseMoved(const MouseEvent & event)
{
	for (size_t i = 0; i < m_instances.size(); ++i) { m_instances[i]->receiveMouseEvent(event); }
	return true;
}
#else
bool ICommandOIS::mouseMoved(const MouseEvent& /*event*/) { return true; }
#endif

#endif
