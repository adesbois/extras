#pragma once

#include <map>

#include "ovamsICommandOIS.h"

namespace SSVEPMindShooter {
class CVRPNServer;

class CCommandCamera final : public ICommandOIS
{
public:
	explicit CCommandCamera(CApplication* application): ICommandOIS(application) { }
	~CCommandCamera() override { }
	void processFrame() override;

	void receiveKeyPressedEvent(OIS::KeyCode key) override;
	void receiveKeyReleasedEvent(OIS::KeyCode key) override;
	void receiveMouseEvent(const OIS::MouseEvent& event) override;

private:
	void updateCamera();
	std::map<OIS::KeyCode, bool> m_keysPressed;

	bool m_bCameraMode = false;
};
}  // namespace SSVEPMindShooter
