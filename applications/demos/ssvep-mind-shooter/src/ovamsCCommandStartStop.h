#pragma once


#include "ovamsICommandOIS.h"

namespace SSVEPMindShooter {
class CVRPNServer;

class CCommandStartStop final : public ICommandOIS
{
public:
	explicit CCommandStartStop(CApplication* application);
	~CCommandStartStop() override;

	void processFrame() override;

	void receiveKeyPressedEvent(OIS::KeyCode key) override;
	void receiveKeyReleasedEvent(OIS::KeyCode key) override;

private:
	CVRPNServer* m_vrpnServer = nullptr;
};
}  // namespace SSVEPMindShooter
