#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandCamera.h"
#include "ovamsCApplication.h"
#include "ovamsCVRPNServer.h"

using namespace SSVEPMindShooter;
using namespace OpenViBE::Kernel;

void CCommandCamera::updateCamera()
{
	Ogre::Vector3 translation(0, 0, 0);
	const Ogre::Real translationSpeed = 10.0;

	if (m_keysPressed[OIS::KC_W]) { translation.z -= translationSpeed; }
	if (m_keysPressed[OIS::KC_D]) { translation.x += translationSpeed; }
	if (m_keysPressed[OIS::KC_A]) { translation.x -= translationSpeed; }
	if (m_keysPressed[OIS::KC_S]) { translation.z += translationSpeed; }


	const Ogre::Vector3 translateVectorFinal = m_application->getCamera()->getDerivedOrientation() * translation;
	m_application->getCameraNode()->translate(translateVectorFinal, Ogre::Node::TS_WORLD);
}

void CCommandCamera::processFrame()
{
	ICommandOIS::processFrame();

	this->updateCamera();

	if (m_keysPressed[OIS::KC_NUMPAD1])
	{
		m_application->debugAction1();
		m_keysPressed[OIS::KC_NUMPAD1] = false;
	}
	if (m_keysPressed[OIS::KC_NUMPAD2])
	{
		m_application->debugAction2();
		m_keysPressed[OIS::KC_NUMPAD2] = false;
	}
	if (m_keysPressed[OIS::KC_NUMPAD3])
	{
		m_application->debugAction3();
		m_keysPressed[OIS::KC_NUMPAD3] = false;
	}
	if (m_keysPressed[OIS::KC_NUMPAD4])
	{
		m_application->debugAction4();
		m_keysPressed[OIS::KC_NUMPAD4] = false;
	}
}

void CCommandCamera::receiveKeyPressedEvent(const OIS::KeyCode key)
{
	if (key == OIS::KC_LCONTROL) { m_bCameraMode = true; }

	if (key == OIS::KC_W) { m_keysPressed[OIS::KC_W] = true; }
	if (key == OIS::KC_A) { m_keysPressed[OIS::KC_A] = true; }
	if (key == OIS::KC_S) { m_keysPressed[OIS::KC_S] = true; }
	if (key == OIS::KC_D) { m_keysPressed[OIS::KC_D] = true; }
	if (key == OIS::KC_NUMPAD1) { m_keysPressed[OIS::KC_NUMPAD1] = true; }
	if (key == OIS::KC_NUMPAD2) { m_keysPressed[OIS::KC_NUMPAD2] = true; }
	if (key == OIS::KC_NUMPAD3) { m_keysPressed[OIS::KC_NUMPAD3] = true; }
	if (key == OIS::KC_NUMPAD4) { m_keysPressed[OIS::KC_NUMPAD4] = true; }
	if (key == OIS::KC_ESCAPE) { m_application->exit(); }
}

void CCommandCamera::receiveKeyReleasedEvent(const OIS::KeyCode key)
{
	if (key == OIS::KC_LCONTROL) { m_bCameraMode = false; }

	if (key == OIS::KC_W) { m_keysPressed[OIS::KC_W] = false; }
	if (key == OIS::KC_A) { m_keysPressed[OIS::KC_A] = false; }
	if (key == OIS::KC_S) { m_keysPressed[OIS::KC_S] = false; }
	if (key == OIS::KC_D) { m_keysPressed[OIS::KC_D] = false; }
	if (key == OIS::KC_NUMPAD1) { m_keysPressed[OIS::KC_NUMPAD1] = false; }
	if (key == OIS::KC_NUMPAD2) { m_keysPressed[OIS::KC_NUMPAD2] = false; }
	if (key == OIS::KC_NUMPAD3) { m_keysPressed[OIS::KC_NUMPAD3] = false; }
	if (key == OIS::KC_NUMPAD4) { m_keysPressed[OIS::KC_NUMPAD4] = false; }
}

void CCommandCamera::receiveMouseEvent(const OIS::MouseEvent& event)
{
	const Ogre::Real rotationSpeedMouse = 1.0F;

	std::cout << m_application->getCamera()->getOrientation() << "\n";
	if (m_bCameraMode)
	{
		m_application->getCamera()->yaw(Ogre::Degree(-float(event.state.X.rel) * rotationSpeedMouse));
		m_application->getCamera()->pitch(Ogre::Degree(-float(event.state.Y.rel) * rotationSpeedMouse));
	}
}

#endif
