#pragma once

#include "ovamsICommandVRPNButton.h"

namespace SSVEPMindShooter {
class CApplication;

class CCommandReceiveTarget final : public ICommandVRPNButton
{
public:
	explicit CCommandReceiveTarget(CApplication* application);
	~CCommandReceiveTarget() override {}

	void execute(const int button, const int state) override;
};
}  // namespace SSVEPMindShooter
