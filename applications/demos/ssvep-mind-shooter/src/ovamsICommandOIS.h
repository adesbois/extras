#pragma once

#include <vector>

#include <OIS.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include "ovamsICommand.h"

namespace SSVEPMindShooter {
class ICommandOIS : public ICommand, OIS::KeyListener, OIS::MouseListener
{
	static std::vector<ICommandOIS*> m_instances;
	static int m_nInstance;

protected:
	static OIS::InputManager* m_inputManager;
	static OIS::Keyboard* m_keyboard;
	static OIS::Mouse* m_mouse;

public:
	explicit ICommandOIS(CApplication* application);
	~ICommandOIS() override;

	void processFrame() override;

protected:
	virtual void receiveKeyPressedEvent(OIS::KeyCode key) = 0;
	virtual void receiveKeyReleasedEvent(OIS::KeyCode key) = 0;
	virtual void receiveMouseEvent(const OIS::MouseEvent& /*oEvent*/) {}

private:
	bool keyPressed(const OIS::KeyEvent& oEvent) override;
	bool keyReleased(const OIS::KeyEvent& oEvent) override;
	bool mouseMoved(const OIS::MouseEvent& event) override;
	bool mousePressed(const OIS::MouseEvent& /*arg*/, OIS::MouseButtonID /*id*/) override { return true; }
	bool mouseReleased(const OIS::MouseEvent& /*arg*/, OIS::MouseButtonID /*id*/) override { return true; }
};
}  // namespace SSVEPMindShooter
