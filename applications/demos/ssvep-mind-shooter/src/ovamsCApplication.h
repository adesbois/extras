#pragma once

#include "ovams_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <Ogre.h>
#include <vector>
#include <CEGUI.h>
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#else
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#endif

#include "ovamsICommand.h"
#include "ovamsCBasicPainter.h"

#include <tcptagging/IStimulusSender.h>

namespace SSVEPMindShooter {
class CApplication : public Ogre::FrameListener, public Ogre::WindowEventListener
{
public:
	explicit CApplication(const OpenViBE::CString& scenarioDir);
	~CApplication() override;

	void addCommand(ICommand* pCommand);
	virtual bool setup(OpenViBE::Kernel::IKernelContext* poKernelContext);
	void go();

	virtual void startExperiment();
	virtual void stopExperiment();
	virtual void startFlickering() { }
	virtual void stopFlickering() { }

	virtual void debugAction1() { }
	virtual void debugAction2() { }
	virtual void debugAction3() { }
	virtual void debugAction4() { }

	virtual void setTarget(int /*i*/) { }

	Ogre::RenderWindow* getWindow() const { return m_window; }
	Ogre::SceneManager* getSceneManager() const { return m_sceneManager; }
	Ogre::SceneNode* getSceneNode() const { return m_sceneNode; }
	Ogre::Camera* getCamera() const { return m_camera; }
	Ogre::SceneNode* getCameraNode() const { return m_cameraNode; }
	CBasicPainter* getPainter() const { return m_painter; }
	OpenViBE::Kernel::ILogManager& getLogManager() const { return (*m_logManager); }
	OpenViBE::Kernel::IConfigurationManager* getConfigurationManager() const { return &(m_kernelCtx->getConfigurationManager()); }
	std::vector<size_t>* getFrequencies() { return &m_frequencies; }

	void exit() { m_continueRendering = false; }

	void resizeViewport() const;

	OpenViBE::Kernel::ILogManager& logPrefix() const
	{
		(*m_logManager) << OpenViBE::Kernel::LogLevel_Trace << "% [" << m_currentTime << "] ";
		return (*m_logManager);
	}

	OpenViBE::CString m_ScenarioDir;

	TCPTagging::IStimulusSender* m_StimulusSender = nullptr;

protected:
	OpenViBE::Kernel::IKernelContext* m_kernelCtx = nullptr;
	OpenViBE::Kernel::ILogManager* m_logManager   = nullptr;

	double m_screenRefreshRate = 0;
	CBasicPainter* m_painter   = nullptr;

	bool m_continueRendering = true;
	size_t m_currentFrame    = 0;
	uint64_t m_currentTime   = 0;

	Ogre::Root* m_root                 = nullptr;
	Ogre::SceneManager* m_sceneManager = nullptr;
	Ogre::Camera* m_camera             = nullptr;
	Ogre::SceneNode* m_cameraNode      = nullptr;
	Ogre::RenderWindow* m_window       = nullptr;
	Ogre::Viewport* m_viewport         = nullptr;
	Ogre::SceneNode* m_sceneNode       = nullptr;

	CEGUI::OgreRenderer* m_guiRenderer       = nullptr;
	CEGUI::WindowManager* m_guiWindowManager = nullptr;
	CEGUI::Window* m_sheet                   = nullptr;

	std::vector<size_t> m_frequencies;

	std::vector<ICommand*> m_commands;

	virtual void processFrame(const size_t /*currentFrame*/) { m_currentTime++; }

	bool frameRenderingQueued(const Ogre::FrameEvent& evt) override;
	bool frameStarted(const Ogre::FrameEvent& evt) override;

	bool configure() const;
	void setupResources() const;

private:
	void setOgreParameters() const;	// Set some render parameters

	void initCEGUI(const char* logFilename);

	Ogre::uint32 m_windowWidth  = 0;
	Ogre::uint32 m_windowHeight = 0;
};
}  // namespace SSVEPMindShooter
