#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace SSVEPMindShooter {
class CApplication;

class ICommand
{
public:
	explicit ICommand(CApplication* application) : m_application(application) {}
	virtual ~ICommand() {}
	virtual void processFrame() = 0;

protected:
	CApplication* m_application = nullptr;
};
}  // namespace SSVEPMindShooter
