#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandControlDiscrete.h"
#include "ovamsCImpactApplication.h"

#include <CEGUI.h>
#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
    typedef CEGUI::UVector2 USize;
};
#endif

using namespace SSVEPMindShooter;

CCommandControlDiscrete::CCommandControlDiscrete(CImpactApplication* application, const bool takeControl)
	: ICommandVRPNButton(application, "SSVEP_VRPN_DiscreteControl"), m_inControl(takeControl) {}

void CCommandControlDiscrete::execute(const int button, const int /*state*/)
{
	if (!m_inControl) { return; }

	CImpactApplication* application = dynamic_cast<CImpactApplication*>(m_application);

	switch (button)
	{
		case 0:
			// Nop
			break;
		case 1:
			application->getShip()->shoot();
			break;
		case 2:
			application->getShip()->move(-6);
			break;
		case 3:
			application->getShip()->move(6);
			break;
		default:
			m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Warning << "Unhandled button " << button << " received\n";
			break;
	}
}

#endif
