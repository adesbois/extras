/// Controls the ship movement by the continuous values from the classifier probabilities
// If bTakeControl is false, feedback levels will still be provided by the analog control.

#pragma once

#include "../ovamsICommandVRPNAnalog.h"
#include <array>

namespace SSVEPMindShooter {
class CImpactApplication;

class CCommandControlAnalog final : public ICommandVRPNAnalog
{
	enum EShipCommand { NOTHING = 0, SHOOT, MOVE_LEFT, MOVE_RIGHT };

public:
	CCommandControlAnalog(CImpactApplication* application, bool takeControl);
	~CCommandControlAnalog() override {}

	void execute(int channelCount, double* channel) override;

private:
	CImpactApplication* m_application = nullptr;
	std::array<int, 3> m_cmdStates;
	std::array<double, 3> m_maxFeedbackLevel;

	void commandeerShip(const std::array<double, 4>& probs);

	bool m_inControl = false;
};
}  // namespace SSVEPMindShooter
