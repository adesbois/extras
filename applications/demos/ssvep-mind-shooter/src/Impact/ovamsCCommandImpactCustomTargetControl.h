#pragma once

/// Used to insert enemy ships into the scene upon receiving a trigger
#include "../ovamsICommandVRPNButton.h"
#include "../ovamsCVRPNServer.h"

namespace SSVEPMindShooter {
class CImpactApplication;

class CCommandImpactCustomTargetControl final : public ICommandVRPNButton
{
public:
	explicit CCommandImpactCustomTargetControl(CImpactApplication* application);
	~CCommandImpactCustomTargetControl() override {}

	void execute(const int button, const int state) override;
	void processFrame() override;

private:
	CVRPNServer* m_vrpnServer = nullptr;
};
}  // namespace SSVEPMindShooter
