#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandControlDiscreteOIS.h"
#include "ovamsCImpactApplication.h"

using namespace SSVEPMindShooter;

CCommandControlDiscreteOIS::CCommandControlDiscreteOIS(CImpactApplication* application)
	: ICommandOIS(application) {}

void CCommandControlDiscreteOIS::processFrame()
{
	ICommandOIS::processFrame();

	CImpactApplication* application = dynamic_cast<CImpactApplication*>(m_application);

	if (m_keysPressed[OIS::KC_UP] || m_keysPressed[OIS::KC_DOWN]) { application->getShip()->shoot(); }
	if (m_keysPressed[OIS::KC_LEFT]) { application->getShip()->move(-6); }
	if (m_keysPressed[OIS::KC_RIGHT]) { application->getShip()->move(6); }
}

void CCommandControlDiscreteOIS::receiveKeyPressedEvent(const OIS::KeyCode key)
{
	if (key == OIS::KC_UP) { m_keysPressed[OIS::KC_UP] = true; }
	if (key == OIS::KC_DOWN) { m_keysPressed[OIS::KC_DOWN] = true; }
	if (key == OIS::KC_LEFT) { m_keysPressed[OIS::KC_LEFT] = true; }
	if (key == OIS::KC_RIGHT) { m_keysPressed[OIS::KC_RIGHT] = true; }
}

void CCommandControlDiscreteOIS::receiveKeyReleasedEvent(const OIS::KeyCode key)
{
	if (key == OIS::KC_UP) { m_keysPressed[OIS::KC_UP] = false; }
	if (key == OIS::KC_DOWN) { m_keysPressed[OIS::KC_DOWN] = false; }
	if (key == OIS::KC_LEFT) { m_keysPressed[OIS::KC_LEFT] = false; }
	if (key == OIS::KC_RIGHT) { m_keysPressed[OIS::KC_RIGHT] = false; }
}

#endif
