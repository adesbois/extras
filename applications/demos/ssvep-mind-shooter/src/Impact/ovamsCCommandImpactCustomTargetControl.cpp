#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandImpactCustomTargetControl.h"
#include "ovamsCImpactApplication.h"

using namespace SSVEPMindShooter;
using namespace OpenViBE::Kernel;

CCommandImpactCustomTargetControl::CCommandImpactCustomTargetControl(CImpactApplication* application)
	: ICommandVRPNButton(application, "SSVEP_VRPN_TargetControl")
{
	m_vrpnServer = CVRPNServer::getInstance(application);
	m_vrpnServer->addButton("SSVEP_VRPN_TargetRequest", 1);
}

void CCommandImpactCustomTargetControl::processFrame()
{
	CImpactApplication* application = dynamic_cast<CImpactApplication*>(m_application);

	ICommandVRPNButton::processFrame();

	if (application->m_TargetRequest)
	{
		m_application->getLogManager() << LogLevel_Info << "Requesting target\n";
		m_vrpnServer->changeButtonState("SSVEP_VRPN_TargetRequest", 0, 1);
		application->m_TargetRequest = false;
	}
	else { m_vrpnServer->changeButtonState("SSVEP_VRPN_TargetRequest", 0, 0); }

	m_vrpnServer->processFrame();
}

/// Add enemies on specified positions depending on the vrpn button triggered
void CCommandImpactCustomTargetControl::execute(const int button, const int /*state*/)
{
	m_application->getLogManager() << LogLevel_Info << "Adding target, position : " << button << "\n";

	CImpactApplication* application = dynamic_cast<CImpactApplication*>(m_application);

	application->m_NextOrigin = 0.0;
	if (button == 0)
	{
		application->m_NextOrigin = -0.21F;
		application->addTarget(1);
		application->addTarget(6);
		application->addTarget(4);
	}
	else if (button == 1)
	{
		application->m_NextOrigin = 0.21F;
		application->addTarget(6);
		application->addTarget(1);
		application->addTarget(3);
	}
}

#endif
