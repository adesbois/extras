#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCImpactShip.h"

#include "ovamsCImpactEnemyShip.h"
#include "ovamsCImpactApplication.h"
#include "../ovamsCSSVEPFlickeringObject.h"

#define SSVEP_SHIP_HULL_COLOUR Ogre::ColourValue(0.0f, 0.5f, 0.5f)
#define SIGN(x) ( (x) < 0 ? -1 : 1 )

#define SCREEN_RATIO ( 16.0f / 9.0f )
#define IMPACTSHIP_SIZE 0.2f
#define IMPACTSHIP_SPEEDFACTOR 0.7f

using namespace SSVEPMindShooter;
using namespace Ogre;

//SP CImpactShip::CImpactShip(CImpactApplication* application, Ogre::SceneNode* parentNode, std::vector<std::pair<size_t, size_t> >* pFrequencies) :
CImpactShip::CImpactShip(CImpactApplication* application, SceneNode* parentNode, std::vector<size_t>* pFrequencies)
	: m_application(application), m_currentAngle(0)
{
	m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "Loading ship\n";

	m_application->getSceneLoader()->parseDotScene("v5.scene", "SSVEPImpact", m_application->getSceneManager());

	m_targetStates[0] = TS_NORMAL;
	m_targetStates[1] = TS_NORMAL;
	m_targetStates[2] = TS_NORMAL;

	SceneManager* sceneManager = m_application->getSceneManager();
	m_projectilesNode          = parentNode->createChildSceneNode();

	m_shipNode = sceneManager->getSceneNode("battleship");
	m_shipNode->scale(0.6F, 0.6F, 0.6F);
	m_shipNode->yaw(Radian(-Math::PI / 2.0F));
	m_shipNode->translate(-25.0F, 0.0F, 0.0F, Node::TS_LOCAL);

	if (!pFrequencies || pFrequencies->size() < 4)
	{
		std::cout << "Error: Expected the frequencies vector to contain indexes 0...3 but its size is " << (pFrequencies ? pFrequencies->size() : -1) <<
				". Crash is likely imminent.\n";
	}

	sceneManager->getSceneNode("v5_ecran_avant2")->setVisible(false);
	m_shipCannon = new CSSVEPFlickeringObject(sceneManager->getSceneNode("shipCannon"), (*pFrequencies)[1]);

	sceneManager->getSceneNode("v5_aile_gauche2")->setVisible(false);
	m_shipLeftWing = new CSSVEPFlickeringObject(sceneManager->getSceneNode("shipLeftWing"), (*pFrequencies)[2]);

	sceneManager->getSceneNode("v5_aile_droite2")->setVisible(false);
	m_shipRightWing = new CSSVEPFlickeringObject(sceneManager->getSceneNode("shipRightWing"), (*pFrequencies)[3]);


	sceneManager->getSceneNode("feedback")->setVisible(true);

	m_targetingNode = sceneManager->getSceneNode("v5_viseur");
}

void CImpactShip::activatePilotAssist(const bool activate)
{
	m_pilotAssistActive = activate;
	m_application->logPrefix() << "Pilot Assist : " << (activate ? "ON" : "OFF") << "\n";
}

void CImpactShip::activateCannonInhibitor(const bool activate)
{
	m_cannonInhibited = activate;

	m_application->logPrefix() << "Cannon Inhibitor : " << (activate ? "ON" : "OFF") << "\n";
}

void CImpactShip::activateTargetLockdown(const bool activate)
{
	m_targetLockdownActive = activate;
	m_application->logPrefix() << "Target Lockdown : " << (activate ? "ON" : "OFF") << "\n";
}

void CImpactShip::activateFeedback(const bool activate)
{
	m_feedbackActive = activate;
	m_application->getSceneManager()->getSceneNode("feedback")->setVisible(activate);
	m_application->logPrefix() << "Feedback : " << (activate ? "ON" : "OFF") << "\n";
}

void CImpactShip::activateFocusPoint(const bool activate)
{
	m_focusPointActive = activate;
	m_application->getSceneManager()->getSceneNode("feedback")->setVisible(!m_focusPointActive && m_feedbackActive);

	if (activate)
	{
		m_application->getSceneManager()->getSceneNode("v5_a1a")->setVisible(false);
		m_application->getSceneManager()->getSceneNode("v5_a2a")->setVisible(true);
		m_application->getSceneManager()->getSceneNode("v5_a3a")->setVisible(false);
		m_application->getSceneManager()->getSceneNode("v5_g1a")->setVisible(true);
		m_application->getSceneManager()->getSceneNode("v5_g2a")->setVisible(false);
		m_application->getSceneManager()->getSceneNode("v5_g3a")->setVisible(false);
		m_application->getSceneManager()->getSceneNode("v5_d1a")->setVisible(true);
		m_application->getSceneManager()->getSceneNode("v5_d2a")->setVisible(false);
		m_application->getSceneManager()->getSceneNode("v5_d3a")->setVisible(false);
	}

	m_application->logPrefix() << "FocusPoint : " << (activate ? "ON" : "OFF") << "\n";
}


bool CImpactShip::projectileExhausted(struct SBlasterProjectile* bp) { return bp->iTTL <= 0; }

void CImpactShip::setAllTargetsVisibility(const bool visible) const
{
	m_shipCannon->setVisible(visible);
	m_shipLeftWing->setVisible(visible);
	m_shipRightWing->setVisible(visible);
}

void CImpactShip::processFrame(const size_t /*currentFrame*/)
{
	if (m_shootingCooldown > 0) { m_shootingCooldown--; }
	if (m_application->isActive())
	{
		// deactivate the wing targets when the ship is on the border
		// std::cout << this->getNormalizedPosition() << "\n";

		if (getNormalizedPosition().x >= -0.75 || !m_targetLockdownActive)
		{
			m_shipLeftWing->processFrame();
			m_targetStates[1] = TS_NORMAL;
		}
		else
		{
			m_shipLeftWing->setVisible(false);
			m_targetStates[1] = TS_OFF;
		}


		if (getNormalizedPosition().x <= 0.75 || !m_targetLockdownActive)
		{
			m_shipRightWing->processFrame();
			m_targetStates[2] = TS_NORMAL;
		}
		else
		{
			m_shipRightWing->setVisible(false);
			m_targetStates[2] = TS_OFF;
		}

		// deactivate the cannon when there is no target locked
		if (m_targetLocked || (!m_targetLockdownActive))
		{
			m_shipCannon->processFrame();
			m_targetStates[0] = (m_cannonInhibited ? TS_INHIBITED : TS_NORMAL);
		}
		else
		{
			m_shipCannon->setVisible(false);
			m_targetStates[0] = TS_OFF;
		}
	}

	m_targetingNode->pitch(Degree(2), Node::TS_PARENT);

	if (m_targetLocked) { m_targetingNode->setVisible(true); }
	else { m_targetingNode->setVisible(false); }

	/*
	if (m_prepareAnimationFrame > 0)
	{
		m_shipNode->translate(0.0f, -0.01f, 0.0f);
		m_prepareAnimationFrame--;
	}
	*/
	if (m_returning)
	{
		if (getNormalizedPosition().x < m_origin - 5e-2 || getNormalizedPosition().x > m_origin + 5e-2)
		{
			m_nCurrentDisplacement = (getNormalizedPosition().x < m_origin) ? 1 : -1;
		}
		else
		{
			m_application->logPrefix() << "Ship Returned to Initial Position\n";
			m_returning = false;
			m_application->startFlickering();
			while (!m_nextEnemyPosition.empty())
			{
				const size_t nextEnemyPosition = m_nextEnemyPosition.front();
				m_nextEnemyPosition.pop_front();
				m_application->insertEnemy(nextEnemyPosition);
			}
		}
	}

	if (m_nCurrentDisplacement != 0)
	{
		// m_application->logPrefix() << "Displacement Vector : value = " << m_nCurrentDisplacement << "\n";

		if (m_returning || !m_targetLocked || !m_pilotAssistActive)
		{
			m_application->logPrefix() << "Ship Moving : direction " << (m_nCurrentDisplacement > 0 ? "left" : "right") << ", full speed\n";
			m_shipNode->translate(0.5F * -float(SIGN(m_nCurrentDisplacement)) * IMPACTSHIP_SPEEDFACTOR, 0.0F, 0.0F);
		}
		else
		{
			Real slowdown = 0.0;

			if (m_application->getCurrentEnemy() != nullptr && m_application->getCurrentEnemy()->getPointValue() > 0)
			{
				const Real enemyPos = m_application->getCurrentEnemy()->getEnemyPosition().x;
				const Real shipPos  = getNormalizedPosition().x;


				if (fabs(enemyPos - shipPos) < m_application->getCurrentEnemy()->getWidth() / 2)
				{
					const Real distance = fabs(shipPos - enemyPos) / (m_application->getCurrentEnemy()->getWidth() / 2);

					if (SIGN(m_nCurrentDisplacement) == SIGN(shipPos - enemyPos)) { slowdown = 1.0F - distance * distance * distance; }

					if (SIGN(m_nCurrentDisplacement) > 0) { m_targetStates[2] = TS_INHIBITED; }
					else { m_targetStates[1] = TS_INHIBITED; }
				}
			}
			m_application->logPrefix() << "Ship Moving : direction " << (m_nCurrentDisplacement > 0 ? "left" : "right") << ", slowdown = " << slowdown << "\n";
			m_shipNode->translate((0.5F - slowdown * 0.3F) * -float(SIGN(m_nCurrentDisplacement)) * IMPACTSHIP_SPEEDFACTOR, 0.0F, 0.0F);
		}

		// m_targetingNode->translate( 0.007f * SIGN(m_nCurrentDisplacement), 0.0f, 0.0f );
		m_nCurrentDisplacement -= SIGN(m_nCurrentDisplacement);
	}


	for (auto it = m_projectiles.begin(); it != m_projectiles.end(); ++it)
	{
		(*it)->iTTL--;

		(*it)->pNode->translate(0.0, -2.0, 0.0, Node::TS_LOCAL);
		(*it)->rRelY -= 1.0F / 65.0F;


		if (projectileExhausted(*it))
		{
			m_application->getSceneManager()->destroyParticleSystem((*it)->pParticleSystem);
			(*it)->pNode->getCreator()->destroySceneNode((*it)->pNode);
			delete *it;
		}
	}
	m_projectiles.erase(std::remove_if(m_projectiles.begin(), m_projectiles.end(), projectileExhausted), m_projectiles.end());


	if (m_oldPosition != m_shipNode->getPosition().x)
	{
		m_oldPosition = m_shipNode->getPosition().x;
		m_application->logPrefix() << "New Ship Position : " << m_oldPosition << " / Normalized X : " << getNormalizedPosition().x << "\n";
	}
}

void CImpactShip::move(const int displacement)
{
	if (displacement < 0 && getNormalizedPosition().x <= -0.75) { return; }
	if (displacement > 0 && getNormalizedPosition().x >= 0.75) { return; }

	//m_application->logPrefix() << "Move : diff vector = " << displacement << ", current vector = " << m_nCurrentDisplacement;

	if (abs(m_nCurrentDisplacement) < 6) { m_nCurrentDisplacement += displacement; }

	//m_application->getLogManager() << ", resulting vector = " << m_nCurrentDisplacement << "\n";
}

void CImpactShip::shoot()
{
	m_application->logPrefix() << "Shoot : ";

	if (m_targetLockdownActive && !m_targetLocked)
	{
		m_application->logPrefix() << "CANCELED : pilot assist blocked the shot\n";
		return;
	}


	if (m_shootingCooldown > 0)
	{
		m_application->logPrefix() << "CANCELED : shooting cooldown blocked the shot\n";
		return;
	}

	m_shootingCooldown = (m_targetStates[0] == TS_INHIBITED) ? 24 : 12;

	struct SBlasterProjectile* projectile = new struct SBlasterProjectile;

	const std::string projectileName = "Projectile_" + std::to_string(++m_nShot);
	projectile->pParticleSystem      = m_application->getSceneManager()->createParticleSystem(projectileName, "Particle/PurpleFountain");


	projectile->iTTL  = 70;
	projectile->pNode = m_projectilesNode->createChildSceneNode();
	projectile->pNode->pitch(Radian(-Math::PI / 2.0F));
	projectile->pNode->translate(this->getPosition().x, 20.0, 0.0, Node::TS_LOCAL);

	projectile->pNode->attachObject(projectile->pParticleSystem);

	projectile->rRelX = -m_shipNode->getPosition().x / 70.0F;
	projectile->rRelY = 0.6F;


	m_application->getLogManager() << "OK : creating particle system\n";
	m_projectiles.push_back(projectile);
}

void CImpactShip::setFeedbackLevels(const int f1, int f2, int f3)
{
	if (m_focusPointActive) { return; }

	SceneManager* sceneManager = m_application->getSceneManager();

	if (m_targetStates[1] == TS_OFF) { f2 = 0; }
	if (m_targetStates[2] == TS_OFF) { f3 = 0; }

	sceneManager->getSceneNode("v5_a1b")->setVisible(false);
	sceneManager->getSceneNode("v5_a2b")->setVisible(false);
	sceneManager->getSceneNode("v5_a3b")->setVisible(false);
	sceneManager->getSceneNode("v5_g1b")->setVisible(false);
	sceneManager->getSceneNode("v5_g2b")->setVisible(false);
	sceneManager->getSceneNode("v5_g3b")->setVisible(false);
	sceneManager->getSceneNode("v5_d1b")->setVisible(false);
	sceneManager->getSceneNode("v5_d2b")->setVisible(false);
	sceneManager->getSceneNode("v5_d3b")->setVisible(false);

	if (m_feedbackActive)
	{
		int nActives = 0;

		if (f1 > 0) { sceneManager->getSceneNode("v5_a1b")->setVisible(true); }
		if (f1 > 1) { sceneManager->getSceneNode("v5_a2b")->setVisible(true); }
		if (f1 > 2)
		{
			sceneManager->getSceneNode("v5_a3b")->setVisible(true);
			nActives++;
		}
		if (f2 > 0) { sceneManager->getSceneNode("v5_g1b")->setVisible(true); }
		if (f2 > 1) { sceneManager->getSceneNode("v5_g2b")->setVisible(true); }
		if (f2 > 2)
		{
			sceneManager->getSceneNode("v5_g3b")->setVisible(true);
			nActives++;
		}
		if (f3 > 0) { sceneManager->getSceneNode("v5_d1b")->setVisible(true); }
		if (f3 > 1) { sceneManager->getSceneNode("v5_d2b")->setVisible(true); }
		if (f3 > 2)
		{
			sceneManager->getSceneNode("v5_d3b")->setVisible(true);
			nActives++;
		}

		if (nActives > 1)
		{
			sceneManager->getSceneNode("v5_a3b")->setVisible(false);
			sceneManager->getSceneNode("v5_g3b")->setVisible(false);
			sceneManager->getSceneNode("v5_d3b")->setVisible(false);
		}

		if (m_targetLockdownActive && !m_targetLocked)
		{
			sceneManager->getSceneNode("v5_a1b")->setVisible(false);
			sceneManager->getSceneNode("v5_a2b")->setVisible(false);
			sceneManager->getSceneNode("v5_a3b")->setVisible(false);
		}
	}
}

Vector2 CImpactShip::getPosition() const { return Vector2(m_shipNode->getPosition().x, m_shipNode->getPosition().y); }

Vector2 CImpactShip::getNormalizedPosition() const { return Vector2(-m_shipNode->getPosition().x / 70.0F, m_shipNode->getPosition().y); }


void CImpactShip::returnToMiddleAndWaitForFoe(const size_t targetPosition, const Real position)
{
	m_application->logPrefix() << "Ship Returning to Middle Position\n";
	m_nextEnemyPosition.push_back(targetPosition);
	m_application->stopFlickering();
	m_origin    = position;
	m_returning = true;
}

bool CImpactShip::evaluateHit(CImpactEnemyShip* enemy)
{
	const Vector2 point = enemy->getEnemyPosition();
	// std::cout << "enemy:" << enemy->getEnemyPosition() << "\n";
	for (auto it = m_projectiles.begin(); it != m_projectiles.end(); ++it)
	{
		// std::cout << "proj: " << (*it)->rRelX << " " << (*it)->rRelY << "\n";

		if (Math::pointInTri2D(Vector2((*it)->rRelX, (*it)->rRelY),
							   Vector2(point.x + enemy->getWidth() / 2.0F, point.y + enemy->getWidth() / 2.0F),
							   Vector2(point.x - enemy->getWidth() / 2.0F, point.y + enemy->getWidth() / 2.0F),
							   Vector2(point.x, point.y - 0.1F)))
		{
			m_application->logPrefix() << "Enemy Hit!\n";
			(*it)->iTTL = 1;
			enemy->processHit(Vector2((*it)->rRelX, (*it)->rRelY));
			return true;
		}
	}

	return false;
}

#endif
