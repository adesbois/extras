#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandImpactTargetControl.h"
#include "ovamsCImpactApplication.h"

using namespace SSVEPMindShooter;
using namespace OpenViBE::Kernel;

CCommandImpactTargetControl::CCommandImpactTargetControl(CImpactApplication* application)
	: ICommandVRPNButton(application, "SSVEP_VRPN_TargetControl")
{
	m_vrpnServer = CVRPNServer::getInstance(application);
	m_vrpnServer->addButton("SSVEP_VRPN_TargetRequest", 1);
}

void CCommandImpactTargetControl::processFrame()
{
	CImpactApplication* application = dynamic_cast<CImpactApplication*>(m_application);

	ICommandVRPNButton::processFrame();

	if (application->m_TargetRequest)
	{
		m_application->getLogManager() << LogLevel_Info << "Requesting target\n";
		m_vrpnServer->changeButtonState("SSVEP_VRPN_TargetRequest", 0, 1);
		application->m_TargetRequest = false;
	}
	else { m_vrpnServer->changeButtonState("SSVEP_VRPN_TargetRequest", 0, 0); }

	m_vrpnServer->processFrame();
}

void CCommandImpactTargetControl::execute(const int button, const int /*state*/)
{
	m_application->getLogManager() << LogLevel_Info << "Adding target, position : " << button << "\n";
	dynamic_cast<CImpactApplication*>(m_application)->addTarget(button);
}

#endif
