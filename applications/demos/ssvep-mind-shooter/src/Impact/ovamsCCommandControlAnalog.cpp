#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandControlAnalog.h"
#include "ovamsCImpactApplication.h"

using namespace OpenViBE;
using namespace SSVEPMindShooter;

#include <CEGUI.h>
#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
    typedef CEGUI::UVector2 USize;
};
#endif


CCommandControlAnalog::CCommandControlAnalog(CImpactApplication* application, const bool takeControl)
	: ICommandVRPNAnalog(application, "SSVEP_VRPN_AnalogControl"), m_application(application), m_inControl(takeControl)
{
	m_cmdStates[0] = 0;
	m_cmdStates[1] = 0;
	m_cmdStates[2] = 0;

	m_maxFeedbackLevel[0] = 0;
	m_maxFeedbackLevel[1] = 0;
	m_maxFeedbackLevel[2] = 0;
}

void CCommandControlAnalog::execute(const int channelCount, double* channel)
{
	CImpactApplication* application = dynamic_cast<CImpactApplication*>(m_application);

	if (m_inControl)
	{
		if (channelCount == 4)
		{
			// 'Modern'  multiclass Mind Shooter assumed
			// The input class coding is assumed to contain the following probabilities, 
			//  0 == 'focus on the middle of the ship')
			//  1 == focus tip
			//  2 == focus left
			//  3 == focus right
			const std::array<double, 4> probs = { channel[0], channel[1], channel[2], channel[3] };
			commandeerShip(probs);
		}
		else if (channelCount == 6)
		{
			// 'Classic' Mind Shooter. The input class probabilities are assumed as follows [class1in,class1Out,class2In,class2Out,class3In,class3Out]
			// Note that with this control scheme, class 0 will never be selected.
			std::array<double, 4> probs = { 0, channel[0], channel[2], channel[4] };
			const double sum            = probs[1] + probs[2] + probs[3];

			if (sum > 0)
			{
				probs[1] = probs[1] / sum;
				probs[2] = probs[2] / sum;
				probs[3] = probs[3] / sum;
			}
			commandeerShip(probs);
		}
		else
		{
			application->getLogManager() << Kernel::LogLevel_Error << "Incorrect analog VRPN input with " << channelCount <<
					" channels. Mind Shooter needs 4 or 6 channels.\n";
			return;
		}
	}

	const int nChannels                          = 3;
	static std::array<int, nChannels> prevLevels = { 0, 0, 0 };

	// Assume input is layered like [prob1,prob2,prob3,prob4]. First ignored for feedback (focus middle of ship).
	std::array<double, nChannels> feedback = { channel[1], channel[2], channel[3] };

	std::array<int, nChannels> level = { 0, 0, 0 };

	//std::cout << "feedback: ";
	for (int i = 0; i < nChannels; ++i)
	{
		if (feedback[i] < 0)
		{
			// Note: shouldn't happen, we're expecting positive numbers
			feedback[i] = 0;
		}

		if (feedback[i] > m_maxFeedbackLevel[i]) { m_maxFeedbackLevel[i] += 0.1; }
		else
		{
			// Slowly decrease max over time in case it was due to artifact
			m_maxFeedbackLevel[i] *= 0.995;
		}

		const double cLevel = (m_maxFeedbackLevel[i] > 0 ? (feedback[i] / m_maxFeedbackLevel[i]) : 0);

		if (cLevel > 0.7) { level[i] = 3; }
		else if (cLevel > 0.3) { level[i] = 2; }
		else if (cLevel > 0.0) { level[i] = 1; }
	}


	if (level[0] != prevLevels[0] || level[1] != prevLevels[1] || level[2] != prevLevels[2])
	{
		application->getLogManager() << Kernel::LogLevel_Trace << "Feedback levels : " << level[0] << " " << level[1] << " " << level[2] << "\n";

		prevLevels[0] = level[0];
		prevLevels[1] = level[1];
		prevLevels[2] = level[2];

		application->getShip()->setFeedbackLevels(level[0], level[1], level[2]);
	}
}

std::string stateToString(const CImpactShip::ETargetState ts)
{
	if (ts == CImpactShip::TS_NORMAL) { return "N"; }
	if (ts == CImpactShip::TS_INHIBITED) { return "I"; }
	if (ts == CImpactShip::TS_OFF) { return "X"; }

	return "-";
}

// Expects 4 non-negative inputs
void CCommandControlAnalog::commandeerShip(const std::array<double, 4>& probs)
{
	m_cmdStates[0] = 0;
	m_cmdStates[1] = 0;
	m_cmdStates[2] = 0;

	// If true, a NOP classification (class 0) will never be selected
	// const bool ignoreIdle = false;

	// Find the strongest activation. 
	size_t maxIdx = 0;
	double maxVal = -std::numeric_limits<double>::max();
	//double sum       = 0;
	for (size_t i = 0; i < 4; ++i)
	{
		if (probs[i] >= maxVal)
			// && !(i==0 && ignoreIdle))
		{
			maxVal = probs[i];
			maxIdx = i;
		}
		//sum += probs[i];
	}
	//if (sum <= 0) { sum = 1; }

	/*
	const bool paralyzed = false;
	if (l_bParalyzed)
	{
		// In this mode, the ship does not move but the prediction is displayed instead. May be useful for debugging.
		char text[512];
		sprintf(text, "Button %d", maxIdx);

		double pos = (maxIdx / 3.0) * 0.85;

		m_application->getInstructionWindow()->setSize(CEGUI::USize(CEGUI::UDim(0.10f, 0), CEGUI::UDim(0.10f, 0)));
		m_application->getInstructionWindow()->setPosition(CEGUI::UVector2(CEGUI::UDim(pos, 0), CEGUI::UDim(0.5f, 0)));
		m_application->getInstructionWindow()->setText(text);
		m_application->getInstructionWindow()->show();

		return;
	}
	*/

	// In principle here we could do different things with the probabilities. If we predict just the
	// maximum, this can be expected to be identical to the Classifier Processor box label output, 
	// hence no difference is expected between the two control schemes (discrete vs analog).
	// To make the two controls a bit different, the probabilities can be used to
	// modulate the strength of the ship movement.

	// Modulating with the probabilities makes the movements a bit jittery
	// const double strength = maxVal / sum;

	// This choice is smoother, but will make the behavior identical to discrete control
	const double strength = 1.0;

	CImpactShip* ship = m_application->getShip();

	if (maxIdx == NOTHING)
	{
		// NOP
	}
	else if (maxIdx == SHOOT && m_application->getShip()->getTargetState(0) != CImpactShip::TS_OFF)
	{
		m_cmdStates[0] = 1;
		ship->shoot();
	}
	else if (maxIdx == MOVE_LEFT && m_application->getShip()->getTargetState(1) != CImpactShip::TS_OFF)
	{
		m_cmdStates[1] = 1;
		ship->move(int(-6 * strength));
	}
	else if (maxIdx == MOVE_RIGHT && m_application->getShip()->getTargetState(2) != CImpactShip::TS_OFF)
	{
		m_cmdStates[2] = 1;
		ship->move(int(6 * strength));
	}
}

#endif
