#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

/*
 * @author Ogre
 * @note This code likely originates from the Ogre project
 * @note Due to the external origin, local variables do not follow OpenViBE coding standards
 *
 */

#include "DotSceneLoader.h"
#include <Ogre.h>

/*
#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>
#include <Terrain/OgreTerrainMaterialGeneratorA.h>


#include "PagedGeometry/PagedGeometry.h"
#include "PagedGeometry/GrassLoader.h"
#include "PagedGeometry/BatchPage.h"
#include "PagedGeometry/ImpostorPage.h"
#include "PagedGeometry/TreeLoader3D.h"


#pragma warning(disable:4390)
#pragma warning(disable:4305)
*/

//using namespace Forests;

Ogre::TerrainGroup* staticGroupPtr = nullptr;

/*
Ogre::Real OgitorTerrainGroupHeightFunction(Ogre::Real x, Ogre::Real z, void *userData)
{
	return staticGroupPtr->getHeightAtWorldPosition(x,0,z);
}
*/
DotSceneLoader::DotSceneLoader() : m_resLocationsBaseDir("")
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Initializing");
	// m_TerrainGlobalOptions = OGRE_NEW Ogre::TerrainGlobalOptions();
}


DotSceneLoader::~DotSceneLoader()
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Destroying everything");
	/*
	if(mGrassLoaderHandle)
		delete mGrassLoaderHandle;

	auto it = mPGHandles.begin();
	while(it != mPGHandles.end())
	{
		delete it[0];
		it++;
	}
	mPGHandles.clear();

	if(mTerrainGroup) { OGRE_DELETE mTerrainGroup; }

	OGRE_DELETE m_TerrainGlobalOptions;
	*/
}

void ParseStringVector(Ogre::String& str, Ogre::StringVector& list)
{
	list.clear();
	Ogre::StringUtil::trim(str, true, true);
	if (str.empty()) { return; }

	size_t pos = str.find(';');
	while (pos != size_t(-1))
	{
		list.push_back(str.substr(0, pos));
		str.erase(0, pos + 1);
		pos = str.find(';');
	}

	if (!str.empty()) { list.push_back(str); }
}

void DotSceneLoader::parseDotScene(const Ogre::String& sceneName, const Ogre::String& groupName, Ogre::SceneManager* yourSceneMgr, Ogre::SceneNode* pAttachNode,
								   const Ogre::String& prependNode)
{
	// set up shared object values
	m_groupName   = groupName;
	m_sceneMgr    = yourSceneMgr;
	m_prependNode = prependNode;
	m_StaticObjects.clear();
	m_DynamicObjects.clear();

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Scene parsing started, ResGroup: " + m_groupName);

	rapidxml::xml_document<> xmlDoc;    // character type defaults to char

	// if the resource group doesn't exists create it
	if (!Ogre::ResourceGroupManager::getSingleton().resourceGroupExists(m_groupName))
	{
		Ogre::ResourceGroupManager::getSingleton().createResourceGroup(m_groupName);
	}

	Ogre::DataStreamPtr stream = Ogre::ResourceGroupManager::getSingleton().openResource(sceneName, groupName);
	if (!stream->isReadable())
	{
		Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] stream is not readable");
		return;
	}

	char* scene = const_cast<char*>(stream->getAsString().c_str());
	xmlDoc.parse<0>(scene);

	// Grab the scene node
	rapidxml::xml_node<>* xmlRoot = xmlDoc.first_node("scene");

	// Validate the File
	if (getAttrib(xmlRoot, "formatVersion", "").empty())
	{
		Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Error: Invalid .scene File. Missing <scene>");
		free(scene);
		return;
	}

	// figure out where to attach any nodes we create
	m_attachNode = pAttachNode;
	if (!m_attachNode) { m_attachNode = m_sceneMgr->getRootSceneNode(); }

	// Process the scene
	processScene(xmlRoot);

	free(scene);

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Scene parsing finished");
}

void DotSceneLoader::processScene(rapidxml::xml_node<>* xmlRoot)
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Scene");
	// Process the scene parameters
	const Ogre::String version = getAttrib(xmlRoot, "formatVersion", "unknown");

	Ogre::String message = "[DotSceneLoader] Parsing dotScene file with version " + version;
	if (xmlRoot->first_attribute("ID")) { message += ", id " + Ogre::String(xmlRoot->first_attribute("ID")->value()); }
	if (xmlRoot->first_attribute("sceneManager")) { message += ", scene manager " + Ogre::String(xmlRoot->first_attribute("sceneManager")->value()); }
	if (xmlRoot->first_attribute("minOgreVersion")) { message += ", min. Ogre version " + Ogre::String(xmlRoot->first_attribute("minOgreVersion")->value()); }
	if (xmlRoot->first_attribute("author")) { message += ", author " + Ogre::String(xmlRoot->first_attribute("author")->value()); }

	Ogre::LogManager::getSingleton().logMessage(message);

	// Process resources (?)
	rapidxml::xml_node<>* pElement = xmlRoot->first_node("resourceLocations");
	if (pElement) { processResourceLocations(pElement); }

	// Process environment (?)
	pElement = xmlRoot->first_node("environment");
	if (pElement) { processEnvironment(pElement); }

	// Process light (?)
	pElement = xmlRoot->first_node("light");
	while (pElement)
	{
		processLight(pElement);
		pElement = pElement->next_sibling("light");
	}

	// Process camera (?)
	pElement = xmlRoot->first_node("camera");
	while (pElement)
	{
		processCamera(pElement);
		pElement = pElement->next_sibling("camera");
	}

	// Process terrain (?)
	/*
	pElement = xmlRoot->first_node("terrain");
	if(pElement)
		processTerrain(pElement);
		*/

	// Process nodes (?)
	pElement = xmlRoot->first_node("nodes");
	if (pElement) { processNodes(pElement); }

	// Process externals (?)
	pElement = xmlRoot->first_node("externals");
	if (pElement) { processExternals(pElement); }

	// Process userDataReference (?)
	pElement = xmlRoot->first_node("userDataReference");
	if (pElement) { processUserDataReference(pElement); }

	// Process octree (?)
	pElement = xmlRoot->first_node("octree");
	if (pElement) { processOctree(pElement); }
}

void DotSceneLoader::processResourceLocations(rapidxml::xml_node<>* xmlNode) const
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Resource Locations");

	// Process resources (?)
	rapidxml::xml_node<>* pElement = xmlNode->first_node("resourceLocation");

	if (pElement)
	{
		// remove the particle templates what are in this resource group
		// (error happens if an already loaded particle is loaded again)
		Ogre::ParticleSystemManager::getSingletonPtr()->removeTemplatesByResourceGroup(m_groupName);
		// and empty the resource group. The previously declared resource locations are not deleted!
		Ogre::ResourceGroupManager::getSingleton().clearResourceGroup(m_groupName);

		// add the resource locations what were in the .scene file
		while (pElement)
		{
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(m_resLocationsBaseDir + getAttrib(pElement, "name"), getAttrib(pElement, "type"),
																		   m_groupName);
			pElement = pElement->next_sibling("resourceLocation");
		}


		Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup(m_groupName);
	}
}

void DotSceneLoader::processNodes(rapidxml::xml_node<>* xmlNode)
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Nodes");

	// Process node (*)
	rapidxml::xml_node<>* pElement = xmlNode->first_node("node");
	while (pElement)
	{
		processNode(pElement);
		pElement = pElement->next_sibling("node");
	}

	// Process position (?)
	pElement = xmlNode->first_node("position");
	if (pElement)
	{
		m_attachNode->setPosition(parseVector3(pElement));
		m_attachNode->setInitialState();
	}

	// Process rotation (?)
	pElement = xmlNode->first_node("rotation");
	if (pElement)
	{
		m_attachNode->setOrientation(parseQuaternion(pElement));
		m_attachNode->setInitialState();
	}

	// Process scale (?)
	pElement = xmlNode->first_node("scale");
	if (pElement)
	{
		m_attachNode->setScale(parseVector3(pElement));
		m_attachNode->setInitialState();
	}
}

void DotSceneLoader::processEnvironment(rapidxml::xml_node<>* xmlNode) const
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Environment");

	// Process camera (?)
	rapidxml::xml_node<>* node = xmlNode->first_node("camera");
	if (node) { processCamera(node); }
	// Process fog (?)
	node = xmlNode->first_node("fog");
	if (node) { processFog(node); }
	// Process skyBox (?)
	node = xmlNode->first_node("skyBox");
	if (node) { processSkyBox(node); }
	// Process skyDome (?)
	node = xmlNode->first_node("skyDome");
	if (node) { processSkyDome(node); }
	// Process skyPlane (?)
	node = xmlNode->first_node("skyPlane");
	if (node) { processSkyPlane(node); }
	// Process clipping (?)
	node = xmlNode->first_node("clipping");
	if (node) { processClipping(node); }
	// Process colourAmbient (?)
	node = xmlNode->first_node("colourAmbient");
	if (node) { m_sceneMgr->setAmbientLight(parseColour(node)); }

	// Process colourBackground (?)
	//! @todo Set the background colour of all viewports (RenderWindow has to be provided then)
	//node = xmlNode->first_node("colourBackground");
	//if (node) {m_sceneMgr->set(parseColour(node));}	

	// Process userDataReference (?)
	node = xmlNode->first_node("userDataReference");
	if (node) { processUserDataReference(node); }
}
/*
void DotSceneLoader::processTerrain(rapidxml::xml_node<>* xmlNode)
{
	Ogre::LogManager::getSingleton().logMessage( "[DotSceneLoader] Process Terrain" );

	Ogre::Real worldSize = getAttribReal(xmlNode, "worldSize");
	int mapSize = Ogre::StringConverter::parseInt(xmlNode->first_attribute("mapSize")->value());
	bool colourmapEnabled = getAttribBool(xmlNode, "colourmapEnabled");
	int colourMapTextureSize = Ogre::StringConverter::parseInt(xmlNode->first_attribute("colourMapTextureSize")->value());
	int compositeMapDistance = Ogre::StringConverter::parseInt(xmlNode->first_attribute("tuningCompositeMapDistance")->value());
	int maxPixelError = Ogre::StringConverter::parseInt(xmlNode->first_attribute("tuningMaxPixelError")->value());

	Ogre::Vector3 lightdir(0, -0.3, 0.75);
	lightdir.normalise();
	Ogre::Light* l = m_sceneMgr->createLight("tstLight");
	l->setType(Ogre::Light::LT_DIRECTIONAL);
	l->setDirection(lightdir);
	l->setDiffuseColour(Ogre::ColourValue(1.0, 1.0, 1.0));
	l->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.4));
//    m_sceneMgr->setAmbientLight(Ogre::ColourValue(0.6, 0.6, 0.6));

	m_TerrainGlobalOptions->setMaxPixelError((Ogre::Real)maxPixelError);
	m_TerrainGlobalOptions->setCompositeMapDistance((Ogre::Real)compositeMapDistance);
	m_TerrainGlobalOptions->setLightMapDirection(lightdir);
	m_TerrainGlobalOptions->setCompositeMapAmbient(m_sceneMgr->getAmbientLight());
	m_TerrainGlobalOptions->setCompositeMapDiffuse(l->getDiffuseColour());

	m_sceneMgr->destroyLight("tstLight");

	mTerrainGroup = OGRE_NEW Ogre::TerrainGroup(m_sceneMgr, Ogre::Terrain::ALIGN_X_Z, mapSize, worldSize);
	mTerrainGroup->setOrigin(Ogre::Vector3::ZERO);

	mTerrainGroup->setResourceGroup(m_groupName);

	rapidxml::xml_node<>* pElement;
	rapidxml::xml_node<>* pPageElement;

	// Process terrain pages (*)
	pElement = xmlNode->first_node("terrainPages");
	if(pElement)
	{
		pPageElement = pElement->first_node("terrainPage");
		while(pPageElement)
		{
			processTerrainPage(pPageElement);
			pPageElement = pPageElement->next_sibling("terrainPage");
		}
	}
	mTerrainGroup->loadAllTerrains(true);

	mTerrainGroup->freeTemporaryResources();
	//mTerrain->setPosition(mTerrainPosition);
}

void DotSceneLoader::processTerrainPage(rapidxml::xml_node<>* xmlNode)
{
	Ogre::String name = getAttrib(xmlNode, "name");

	Ogre::LogManager::getSingleton().logMessage( "[DotSceneLoader] Process Terrain Page: " + name );

	int pageX = Ogre::StringConverter::parseInt(xmlNode->first_attribute("pageX")->value());
	int pageY = Ogre::StringConverter::parseInt(xmlNode->first_attribute("pageY")->value());
	mPGPageSize       = Ogre::StringConverter::parseInt(xmlNode->first_attribute("pagedGeometryPageSize")->value());
	mPGDetailDistance = Ogre::StringConverter::parseInt(xmlNode->first_attribute("pagedGeometryDetailDistance")->value());
	// error checking
	if(mPGPageSize < 10){
		Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] pagedGeometryPageSize value error!", Ogre::LML_CRITICAL);
		mPGPageSize = 10;
	}
	if(mPGDetailDistance < 100){
		Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] pagedGeometryDetailDistance value error!", Ogre::LML_CRITICAL);
		mPGDetailDistance = 100;
	}

	if (Ogre::ResourceGroupManager::getSingleton().resourceExists(mTerrainGroup->getResourceGroup(), name))
	{
		mTerrainGroup->defineTerrain(pageX, pageY, name);
	}
	// grass layers
	rapidxml::xml_node<>* pElement = xmlNode->first_node("grassLayers");

	if(pElement) { processGrassLayers(pElement); }
}

void DotSceneLoader::processGrassLayers(rapidxml::xml_node<>* xmlNode)
{
	Ogre::LogManager::getSingleton().logMessage( "[DotSceneLoader] Process Grass Layers" );

	Ogre::String dMapName = getAttrib(xmlNode, "densityMap");
	m_TerrainGlobalOptions->setVisibilityFlags(Ogre::StringConverter::parseUnsignedInt(xmlNode->first_attribute("visibilityFlags")->value()));

	// create a temporary camera
	Ogre::Camera* tempCam = m_sceneMgr->createCamera("ThIsNamEShoUlDnOtExisT");

	// create paged geometry what the grass will use
	Forests::PagedGeometry * mPGHandle = new PagedGeometry(tempCam, mPGPageSize);
	mPGHandle->addDetailLevel<GrassPage>(mPGDetailDistance);

	//Create a GrassLoader object
	mGrassLoaderHandle = new GrassLoader(mPGHandle);
	mGrassLoaderHandle->setVisibilityFlags(m_TerrainGlobalOptions->getVisibilityFlags());

	//Assign the "grassLoader" to be used to load geometry for the PagedGrass instance
	mPGHandle->setPageLoader(mGrassLoaderHandle);

	// set the terrain group pointer
	staticGroupPtr = mTerrainGroup;

	//Supply a height function to GrassLoader so it can calculate grass Y values
	mGrassLoaderHandle->setHeightFunction(OgitorTerrainGroupHeightFunction);

	// push the page geometry handle into the PGHandles array
	mPGHandles.push_back(mPGHandle);

	// create the layers and load the options for them
	rapidxml::xml_node<>* pElement = xmlNode->first_node("grassLayer");
	rapidxml::xml_node<>* pSubElement;
	Forests::GrassLayer* gLayer;
	Ogre::String tempStr;
	while(pElement)
	{
		// grassLayer
		gLayer = mGrassLoaderHandle->addLayer(pElement->first_attribute("material")->value());
		gLayer->setId(Ogre::StringConverter::parseInt(pElement->first_attribute("id")->value()));
		gLayer->setEnabled(Ogre::StringConverter::parseBool(pElement->first_attribute("enabled")->value()));
		gLayer->setMaxSlope(Ogre::StringConverter::parseReal(pElement->first_attribute("maxSlope")->value()));
		gLayer->setLightingEnabled(Ogre::StringConverter::parseBool(pElement->first_attribute("lighting")->value()));

		Ogre::LogManager::getSingleton().logMessage( "[DotSceneLoader] processing grassLayer "+Ogre::StringConverter::toString(gLayer->getId()) );

		// densityMapProps
		pSubElement = pElement->first_node("densityMapProps");
		tempStr = pSubElement->first_attribute("channel")->value();
		MapChannel mapCh;
		if(!tempStr.compare("ALPHA")) mapCh = CHANNEL_ALPHA; else
		if(!tempStr.compare("BLUE"))  mapCh = CHANNEL_BLUE;  else
		if(!tempStr.compare("COLOR")) mapCh = CHANNEL_COLOR; else
		if(!tempStr.compare("GREEN")) mapCh = CHANNEL_GREEN; else
		if(!tempStr.compare("RED"))   mapCh = CHANNEL_RED;

		gLayer->setDensityMap(dMapName, mapCh);
		gLayer->setDensity(Ogre::StringConverter::parseReal(pSubElement->first_attribute("density")->value()));

		// mapBounds
		pSubElement = pElement->first_node("mapBounds");
		gLayer->setMapBounds( TBounds(Ogre::StringConverter::parseReal(pSubElement->first_attribute("left")->value()),		// left
									  Ogre::StringConverter::parseReal(pSubElement->first_attribute("top")->value()),		// top
									  Ogre::StringConverter::parseReal(pSubElement->first_attribute("right")->value()),		// right
									  Ogre::StringConverter::parseReal(pSubElement->first_attribute("bottom")->value())));	// bottom

		// grassSizes
		pSubElement = pElement->first_node("grassSizes");
		gLayer->setMinimumSize( Ogre::StringConverter::parseReal(pSubElement->first_attribute("minWidth")->value()),   // width
								Ogre::StringConverter::parseReal(pSubElement->first_attribute("minHeight")->value()) );// height
		gLayer->setMaximumSize( Ogre::StringConverter::parseReal(pSubElement->first_attribute("maxWidth")->value()),   // width
								Ogre::StringConverter::parseReal(pSubElement->first_attribute("maxHeight")->value()) );// height

		// techniques
		pSubElement = pElement->first_node("techniques");
		tempStr = pSubElement->first_attribute("renderTechnique")->value();
		GrassTechnique rendTech;

		if(!tempStr.compare("QUAD"))       rendTech = GRASSTECH_QUAD;       else
		if(!tempStr.compare("CROSSQUADS")) rendTech = GRASSTECH_CROSSQUADS; else
		if(!tempStr.compare("SPRITE"))     rendTech = GRASSTECH_SPRITE;
		gLayer->setRenderTechnique( rendTech,
									Ogre::StringConverter::parseBool(pSubElement->first_attribute("blend")->value()) );

		tempStr = pSubElement->first_attribute("fadeTechnique")->value();
		FadeTechnique fadeTech;
		if(!tempStr.compare("ALPHA")) fadeTech = FADETECH_ALPHA; else
		if(!tempStr.compare("GROW"))  fadeTech = FADETECH_GROW; else
		if(!tempStr.compare("ALPHAGROW")) fadeTech = FADETECH_ALPHAGROW;
		gLayer->setFadeTechnique(fadeTech);

		// animation
		pSubElement = pElement->first_node("animation");
		gLayer->setAnimationEnabled(Ogre::StringConverter::parseBool(pSubElement->first_attribute("animate")->value()));
		gLayer->setSwayLength(Ogre::StringConverter::parseReal(pSubElement->first_attribute("swayLength")->value()));
		gLayer->setSwaySpeed(Ogre::StringConverter::parseReal(pSubElement->first_attribute("swaySpeed")->value()));
		gLayer->setSwayDistribution(Ogre::StringConverter::parseReal(pSubElement->first_attribute("swayDistribution")->value()));

		// next layer
		pElement = pElement->next_sibling("grassLayer");
	}

	m_sceneMgr->destroyCamera(tempCam);
}
*/
void DotSceneLoader::processUserDataReference(rapidxml::xml_node<>* /*xmlNode*/, Ogre::SceneNode* /*parent*/)
{
	//! @todo Implement this
}

void DotSceneLoader::processOctree(rapidxml::xml_node<>* /*xmlNode*/)
{
	//! @todo Implement this
}

void DotSceneLoader::processLight(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* parent)
{
	// Process attributes
	const Ogre::String name = getAttrib(xmlNode, "name");
	//Ogre::String id   = getAttrib(xmlNode, "id");

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Light: " + name);

	// Create the light
	Ogre::Light* pLight = m_sceneMgr->createLight(name);
	if (parent) { parent->attachObject(pLight); }

	const Ogre::String value = getAttrib(xmlNode, "type");
	if (value == "point") { pLight->setType(Ogre::Light::LT_POINT); }
	else if (value == "directional") { pLight->setType(Ogre::Light::LT_DIRECTIONAL); }
	else if (value == "spot") { pLight->setType(Ogre::Light::LT_SPOTLIGHT); }
	else if (value == "radPoint") { pLight->setType(Ogre::Light::LT_POINT); }

	pLight->setVisible(getAttribBool(xmlNode, "visible", true));
	pLight->setCastShadows(getAttribBool(xmlNode, "castShadows", true));

	// Process position (?)
	rapidxml::xml_node<>* pElement = xmlNode->first_node("position");
	if (pElement) { pLight->setPosition(parseVector3(pElement)); }

	// Process normal (?)
	pElement = xmlNode->first_node("normal");
	if (pElement) { pLight->setDirection(parseVector3(pElement)); }

	pElement = xmlNode->first_node("directionVector");
	if (pElement)
	{
		pLight->setDirection(parseVector3(pElement));
		m_lightDirection = parseVector3(pElement);
	}

	// Process colourDiffuse (?)
	pElement = xmlNode->first_node("colourDiffuse");
	if (pElement) { pLight->setDiffuseColour(parseColour(pElement)); }

	// Process colourSpecular (?)
	pElement = xmlNode->first_node("colourSpecular");
	if (pElement) { pLight->setSpecularColour(parseColour(pElement)); }

	if (value != "directional")
	{
		// Process lightRange (?)
		pElement = xmlNode->first_node("lightRange");
		if (pElement) { processLightRange(pElement, pLight); }

		// Process lightAttenuation (?)
		pElement = xmlNode->first_node("lightAttenuation");
		if (pElement) { processLightAttenuation(pElement, pLight); }
	}
	// Process userDataReference (?)
	//pElement = xmlNode->first_node("userDataReference");
	//if (pElement) { processUserDataReference(pElement, pLight); }
}

void DotSceneLoader::processCamera(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent) const
{
	// Process attributes
	const Ogre::String name = getAttrib(xmlNode, "name");
	//Ogre::String id = getAttrib(xmlNode, "id");
	//Ogre::Real fov = getAttribReal(xmlNode, "fov", 45);
	//Ogre::Real aspectRatio = getAttribReal(xmlNode, "aspectRatio", 1.3333);
	const Ogre::String projectionType = getAttrib(xmlNode, "projectionType", "perspective");

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Camera: " + name);

	// Create the camera
	Ogre::Camera* pCamera = m_sceneMgr->createCamera(name);

	// set auto Aspest ratio true. If later aspect ratio comes this can be deleted
	pCamera->setAutoAspectRatio(true);

	//TODO: make a flag or attribute indicating whether or not the camera should be attached to any parent node.
	if (pParent) { pParent->attachObject(pCamera); }

	// Set the field-of-view
	//! @todo Is this always in degrees?
	//pCamera->setFOVy(Ogre::Degree(fov));

	// Set the aspect ratio
	//pCamera->setAutoAspectRatio(false);
	//pCamera->setAspectRatio(aspectRatio);

	// Set the projection type
	if (projectionType == "perspective") { pCamera->setProjectionType(Ogre::PT_PERSPECTIVE); }
	else if (projectionType == "orthographic") { pCamera->setProjectionType(Ogre::PT_ORTHOGRAPHIC); }

	// Process clipping (?)
	rapidxml::xml_node<>* element = xmlNode->first_node("clipping");
	if (element)
	{
		const Ogre::Real nearDist = getAttribReal(element, "near");
		pCamera->setNearClipDistance(nearDist);

		const Ogre::Real farDist = getAttribReal(element, "far");
		pCamera->setFarClipDistance(farDist);
	}

	// Process position (?)
	element = xmlNode->first_node("position");
	if (element) { pCamera->setPosition(parseVector3(element)); }

	// Process rotation (?)
	element = xmlNode->first_node("rotation");
	if (element) { pCamera->setOrientation(parseQuaternion(element)); }

	// Process normal (?)
	//element = xmlNode->first_node("normal");
	///< @todo What to do with this element?
	//if (pElement)	{	}

	// Process lookTarget (?)
	//element = xmlNode->first_node("lookTarget");
	///< @todo Implement the camera look target	
	//if (pElement)	{	}

	// Process trackTarget (?)
	//element = xmlNode->first_node("trackTarget");
	///< @todo Implement the camera track target
	//if (pElement) {}
	// Process userDataReference (?)
	//element = xmlNode->first_node("userDataReference");
	///< @todo Implement the camera user data reference
	//if (pElement)	{	}
	/*
		// construct a scenenode is no parent
		if(!pParent)
		{
			Ogre::SceneNode* node = m_attachNode->createChildSceneNode(name);
			node->setPosition(pCamera->getPosition());
			node->setOrientation(pCamera->getOrientation());
			node->scale(1,1,1);
		}
	*/
}

void DotSceneLoader::processNode(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent)
{
	// Construct the node's name
	const Ogre::String name = m_prependNode + getAttrib(xmlNode, "name");

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Node: " + name);

	// Create the scene node
	Ogre::SceneNode* pNode;

	if (name.empty())
	{
		// Let Ogre choose the name
		if (pParent) { pNode = pParent->createChildSceneNode(); }
		else { pNode = m_attachNode->createChildSceneNode(); }
	}
	else
	{
		// Provide the name
		if (pParent) { pNode = pParent->createChildSceneNode(name); }
		else { pNode = m_attachNode->createChildSceneNode(name); }
	}

	// Process other attributes
	// Ogre::String id = getAttrib(xmlNode, "id");
	// bool isTarget = getAttribBool(xmlNode, "isTarget");

	// Process position (?)
	rapidxml::xml_node<>* pElement = xmlNode->first_node("position");
	if (pElement)
	{
		pNode->setPosition(parseVector3(pElement));
		pNode->setInitialState();
	}

	// Process rotation (?)
	pElement = xmlNode->first_node("rotation");
	if (pElement)
	{
		pNode->setOrientation(parseQuaternion(pElement));
		pNode->setInitialState();
	}

	// Process scale (?)
	pElement = xmlNode->first_node("scale");
	if (pElement)
	{
		pNode->setScale(parseVector3(pElement));
		pNode->setInitialState();
	}

	// Process lookTarget (?)
	pElement = xmlNode->first_node("lookTarget");
	if (pElement) { processLookTarget(pElement, pNode); }

	// Process trackTarget (?)
	pElement = xmlNode->first_node("trackTarget");
	if (pElement) { processTrackTarget(pElement, pNode); }

	// Process node (*)
	pElement = xmlNode->first_node("node");
	while (pElement)
	{
		processNode(pElement, pNode);
		pElement = pElement->next_sibling("node");
	}

	// Process entity (*)
	pElement = xmlNode->first_node("entity");
	while (pElement)
	{
		processEntity(pElement, pNode);
		pElement = pElement->next_sibling("entity");
	}

	// Process light (*)
	pElement = xmlNode->first_node("light");
	while (pElement)
	{
		processLight(pElement, pNode);
		pElement = pElement->next_sibling("light");
	}

	// Process camera (*)
	pElement = xmlNode->first_node("camera");
	while (pElement)
	{
		processCamera(pElement, pNode);
		pElement = pElement->next_sibling("camera");
	}

	// Process particleSystem (*)
	pElement = xmlNode->first_node("particleSystem");
	while (pElement)
	{
		processParticleSystem(pElement, pNode);
		pElement = pElement->next_sibling("particleSystem");
	}

	// Process billboardSet (*)
	pElement = xmlNode->first_node("billboardSet");
	while (pElement)
	{
		processBillboardSet(pElement, pNode);
		pElement = pElement->next_sibling("billboardSet");
	}

	// Process plane (*)
	pElement = xmlNode->first_node("plane");
	while (pElement)
	{
		processPlane(pElement, pNode);
		pElement = pElement->next_sibling("plane");
	}

	// Process userDataReference (?)
	pElement = xmlNode->first_node("userDataReference");
	if (pElement) { processUserDataReference(pElement, pNode); }
	/*
		// Process entity (*)
		pElement = xmlNode->first_node("pagedgeometry");
		while(pElement)
		{
			processPagedGeometry(pElement, node);
			pElement = pElement->next_sibling("pagedgeometry");
		}
		*/
}

void DotSceneLoader::processLookTarget(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* parent) const
{
	//! @todo Is this correct? Cause I don't have a clue actually

	// Process attributes
	const Ogre::String nodeName = getAttrib(xmlNode, "nodeName");

	Ogre::Node::TransformSpace relativeTo = Ogre::Node::TS_PARENT;
	const Ogre::String value              = getAttrib(xmlNode, "relativeTo");
	if (value == "local") { relativeTo = Ogre::Node::TS_LOCAL; }
	else if (value == "parent") { relativeTo = Ogre::Node::TS_PARENT; }
	else if (value == "world") { relativeTo = Ogre::Node::TS_WORLD; }

	// Process position (?)
	Ogre::Vector3 position;
	rapidxml::xml_node<>* element = xmlNode->first_node("position");
	if (element) { position = parseVector3(element); }

	// Process localDirection (?)
	Ogre::Vector3 localDirection = Ogre::Vector3::NEGATIVE_UNIT_Z;
	element                      = xmlNode->first_node("localDirection");
	if (element) { localDirection = parseVector3(element); }

	// Setup the look target
	try
	{
		if (!nodeName.empty())
		{
			Ogre::SceneNode* pLookNode = m_sceneMgr->getSceneNode(nodeName);
			position                   = pLookNode->_getDerivedPosition();
		}

		parent->lookAt(position, relativeTo, localDirection);
	}
	catch (Ogre::Exception&/*e*/) { Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Error processing a look target!"); }
}

void DotSceneLoader::processTrackTarget(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent) const
{
	// Process attributes
	const Ogre::String nodeName = getAttrib(xmlNode, "nodeName");

	// Process localDirection (?)
	Ogre::Vector3 localDirection   = Ogre::Vector3::NEGATIVE_UNIT_Z;
	rapidxml::xml_node<>* pElement = xmlNode->first_node("localDirection");
	if (pElement) { localDirection = parseVector3(pElement); }

	// Process offset (?)
	Ogre::Vector3 offset = Ogre::Vector3::ZERO;
	pElement             = xmlNode->first_node("offset");
	if (pElement) { offset = parseVector3(pElement); }

	// Setup the track target
	try
	{
		Ogre::SceneNode* pTrackNode = m_sceneMgr->getSceneNode(nodeName);
		pParent->setAutoTracking(true, pTrackNode, localDirection, offset);
	}
	catch (Ogre::Exception&/*e*/) { Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Error processing a track target!"); }
}

void DotSceneLoader::processEntity(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent)
{
	// Process attributes
	const Ogre::String name = getAttrib(xmlNode, "name");
	//const Ogre::String id           = getAttrib(xmlNode, "id");
	const Ogre::String meshFile     = getAttrib(xmlNode, "meshFile");
	const Ogre::String materialFile = getAttrib(xmlNode, "materialFile");
	const bool isStatic             = getAttribBool(xmlNode, "static", false);
	const bool castShadows          = getAttribBool(xmlNode, "castShadows", true);

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Entity: " + name);

	// TEMP: Maintain a list of static and dynamic objects
	if (isStatic) { m_StaticObjects.push_back(name); }
	else { m_DynamicObjects.push_back(name); }

	// Process vertexBuffer (?)
	//rapidxml::xml_node<>* pElement = xmlNode->first_node("vertexBuffer");
	//if (pElement) { processVertexBuffer(pElement);

	// Process indexBuffer (?)
	//pElement = xmlNode->first_node("indexBuffer");
	//if (pElement) { processIndexBuffer(pElement); }

	// Create the entity
	Ogre::Entity* pEntity = nullptr;
	try
	{
		Ogre::MeshManager::getSingleton().load(meshFile, m_groupName);
		pEntity = m_sceneMgr->createEntity(name, meshFile);
		pEntity->setCastShadows(castShadows);
		pParent->attachObject(pEntity);

		if (!materialFile.empty()) { pEntity->setMaterialName(materialFile); }

		// Process subentity (*)
		/* if materials defined within subentities, those
		   materials will be used instead of the materialFile */
		rapidxml::xml_node<>* pElement = xmlNode->first_node("subentities");

		if (pElement) { processSubEntity(pElement, pEntity); }
		else
		{
			// if the .scene file contains the subentites without
			// the <subentities> </subentities>
			processSubEntity(xmlNode, pEntity);
		}
	}
	catch (Ogre::Exception&/*e*/) { Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Error loading an entity!"); }

	// Process userDataReference (?)
	rapidxml::xml_node<>* pElement = xmlNode->first_node("userDataReference");
	if (pElement) { processUserDataReference(pElement, pEntity); }
}

void DotSceneLoader::processSubEntity(rapidxml::xml_node<>* xmlNode, Ogre::Entity* pEntity)
{
	Ogre::String materialName;
	Ogre::String sIndex;

	// Process subentity
	rapidxml::xml_node<>* pElement = xmlNode->first_node("subentity");

	while (pElement)
	{
		sIndex.clear();
		materialName.clear();

		sIndex       = getAttrib(pElement, "index");				// submesh index
		materialName = getAttrib(pElement, "materialName");	// new material for submesh

		if (!sIndex.empty() && !materialName.empty())
		{
			const int index = Ogre::StringConverter::parseInt(sIndex);
			try { pEntity->getSubEntity(index)->setMaterialName(materialName); }
			catch (...) { Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Subentity material index invalid!"); }
		}
		pElement = pElement->next_sibling("subentity");
	}
}

void DotSceneLoader::processParticleSystem(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent)
{
	// Process attributes
	const Ogre::String name = getAttrib(xmlNode, "name");
	//Ogre::String id         = getAttrib(xmlNode, "id");
	const Ogre::String scriptName = getAttrib(xmlNode, "script");

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process ParticleSystem: " + name);


	// Create the particle system
	try
	{
		Ogre::ParticleSystem* pParticles = m_sceneMgr->createParticleSystem(name, scriptName);
		pParent->attachObject(pParticles);
		m_ParticleSystemList.push_back(pParticles);
		// there is a bug with particles and paged geometry if particle's
		// renderQueue is value is smaller than the grass's renderQueue
		/*
		if(mGrassLoaderHandle)
			pParticles->setRenderQueueGroup(mGrassLoaderHandle->getRenderQueueGroup());
			*/
	}
	catch (Ogre::Exception&/*e*/) { Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Error creating a particle system!"); }
}

void DotSceneLoader::processBillboardSet(rapidxml::xml_node<>* /*xmlNode*/, Ogre::SceneNode* /*parent*/)
{
	//! @todo Implement this
}

void DotSceneLoader::processPlane(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent) const
{
	const Ogre::String name     = getAttrib(xmlNode, "name");
	const Ogre::Real distance   = getAttribReal(xmlNode, "distance");
	const Ogre::Real width      = getAttribReal(xmlNode, "width");
	const Ogre::Real height     = getAttribReal(xmlNode, "height");
	const int xSegments         = Ogre::StringConverter::parseInt(getAttrib(xmlNode, "xSegments"));
	const int ySegments         = Ogre::StringConverter::parseInt(getAttrib(xmlNode, "ySegments"));
	const int numTexCoordSets   = Ogre::StringConverter::parseInt(getAttrib(xmlNode, "numTexCoordSets"));
	const Ogre::Real uTile      = getAttribReal(xmlNode, "uTile");
	const Ogre::Real vTile      = getAttribReal(xmlNode, "vTile");
	const Ogre::String material = getAttrib(xmlNode, "material");
	const bool hasNormals       = getAttribBool(xmlNode, "hasNormals");
	const Ogre::Vector3 normal  = parseVector3(xmlNode->first_node("normal"));
	const Ogre::Vector3 up      = parseVector3(xmlNode->first_node("upVector"));

	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Plane: " + name);

	const Ogre::Plane plane(normal, distance);
	Ogre::MeshPtr res = Ogre::MeshManager::getSingletonPtr()->createPlane(name + "mesh", "General", plane, width, height, xSegments, ySegments, hasNormals,
																		  numTexCoordSets, uTile, vTile, up);
	Ogre::Entity* ent = m_sceneMgr->createEntity(name, name + "mesh");

	ent->setMaterialName(material);

	pParent->attachObject(ent);
}
/*
struct PGInstanceInfo
{
	Ogre::Vector3 pos;
	Ogre::Real    scale;
	Ogre::Real    yaw;
};

typedef std::vector<PGInstanceInfo> PGInstanceList;

void DotSceneLoader::processPagedGeometry(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode *pParent)
{
	Ogre::LogManager::getSingleton().logMessage( "[DotSceneLoader] Process Paged Geometry" );

	Ogre::String filename = "../Projects/SampleScene3/" + getAttrib(xmlNode, "fileName");
	Ogre::String model = getAttrib(xmlNode, "model");
	Ogre::Real pagesize = getAttribReal(xmlNode, "pageSize");
	Ogre::Real batchdistance = getAttribReal(xmlNode, "batchDistance");
	Ogre::Real impostordistance = getAttribReal(xmlNode, "impostorDistance");
	Ogre::Vector4 bounds = Ogre::StringConverter::parseVector4(getAttrib(xmlNode, "bounds"));

	PagedGeometry *mPGHandle = new PagedGeometry();
	mPGHandle->setCamera(m_sceneMgr->getCameraIterator().begin()->second);
	mPGHandle->setPageSize(pagesize);
	mPGHandle->setInfinite();

	mPGHandle->addDetailLevel<BatchPage>(batchdistance,0);
	mPGHandle->addDetailLevel<ImpostorPage>(impostordistance,0);

	TreeLoader3D *mHandle = new TreeLoader3D(mPGHandle, Forests::TBounds(bounds.x, bounds.y, bounds.z, bounds.w));
	mPGHandle->setPageLoader(mHandle);

	mPGHandles.push_back(mPGHandle);
	mTreeHandles.push_back(mHandle);

	std::ifstream stream(filename.c_str());

	if(!stream.is_open())
		return;

	Ogre::StringVector list;

	char res[128];

	PGInstanceList mInstanceList;

	while(!stream.eof())
	{
		stream.getline(res, 128);
		Ogre::String resStr(res);

		ParseStringVector(resStr, list);

		if(list.size() == 3)
		{
			PGInstanceInfo info;

			info.pos = Ogre::StringConverter::parseVector3(list[0]);
			info.scale = Ogre::StringConverter::parseReal(list[1]);
			info.yaw = Ogre::StringConverter::parseReal(list[2]);

			mInstanceList.push_back(info);
		}
		else if(list.size() == 4)
		{
			PGInstanceInfo info;

			info.pos = Ogre::StringConverter::parseVector3(list[1]);
			info.scale = Ogre::StringConverter::parseReal(list[2]);
			info.yaw = Ogre::StringConverter::parseReal(list[3]);

			mInstanceList.push_back(info);
		}
	}

	stream.close();

	if(model != "")
	{
		Ogre::Entity *mEntityHandle = m_sceneMgr->createEntity(model + ".mesh");

		auto it = mInstanceList.begin();

		while(it != mInstanceList.end())
		{
			mHandle->addTree(mEntityHandle, it->pos, Ogre::Degree(it->yaw), it->scale);

			it++;
		}
	}
}
*/
void DotSceneLoader::processFog(rapidxml::xml_node<>* xmlNode) const
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process Fog");

	// Process attributes
	const Ogre::Real expDensity  = getAttribReal(xmlNode, "density", 0.001F);
	const Ogre::Real linearStart = getAttribReal(xmlNode, "start", 0.0);
	const Ogre::Real linearEnd   = getAttribReal(xmlNode, "end", 1.0);

	Ogre::FogMode mode;
	const Ogre::String sMode = getAttrib(xmlNode, "mode");
	if (sMode == "none") { mode = Ogre::FOG_NONE; }
	else if (sMode == "exp") { mode = Ogre::FOG_EXP; }
	else if (sMode == "exp2") { mode = Ogre::FOG_EXP2; }
	else if (sMode == "linear") { mode = Ogre::FOG_LINEAR; }
	else { mode = Ogre::FogMode(Ogre::StringConverter::parseInt(sMode)); }

	// Process colourDiffuse (?)
	Ogre::ColourValue colourDiffuse = Ogre::ColourValue::White;
	rapidxml::xml_node<>* pElement  = xmlNode->first_node("colour");
	if (pElement) { colourDiffuse = parseColour(pElement); }

	// Setup the fog
	m_sceneMgr->setFog(mode, colourDiffuse, expDensity, linearStart, linearEnd);
}

void DotSceneLoader::processSkyBox(rapidxml::xml_node<>* xmlNode) const
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process SkyBox");

	// Process attributes
	const Ogre::String material = getAttrib(xmlNode, "material", "BaseWhite");
	const Ogre::Real distance   = getAttribReal(xmlNode, "distance", 5000);
	const bool drawFirst        = getAttribBool(xmlNode, "drawFirst", true);
	const bool active           = getAttribBool(xmlNode, "active", false);
	if (!active) { return; }

	// Process rotation (?)
	Ogre::Quaternion rotation      = Ogre::Quaternion::IDENTITY;
	rapidxml::xml_node<>* pElement = xmlNode->first_node("rotation");
	if (pElement) { rotation = parseQuaternion(pElement); }

	// Setup the sky box
	m_sceneMgr->setSkyBox(true, material, distance, drawFirst, rotation, m_groupName);
}

void DotSceneLoader::processSkyDome(rapidxml::xml_node<>* xmlNode) const
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process SkyDome");

	// Process attributes
	const Ogre::String material = xmlNode->first_attribute("material")->value();
	const Ogre::Real curvature  = getAttribReal(xmlNode, "curvature", 10);
	const Ogre::Real tiling     = getAttribReal(xmlNode, "tiling", 8);
	const Ogre::Real distance   = getAttribReal(xmlNode, "distance", 4000);
	const bool drawFirst        = getAttribBool(xmlNode, "drawFirst", true);
	const bool active           = getAttribBool(xmlNode, "active", false);
	if (!active) { return; }

	// Process rotation (?)
	Ogre::Quaternion rotation      = Ogre::Quaternion::IDENTITY;
	rapidxml::xml_node<>* pElement = xmlNode->first_node("rotation");
	if (pElement) { rotation = parseQuaternion(pElement); }

	// Setup the sky dome
	m_sceneMgr->setSkyDome(true, material, curvature, tiling, distance, drawFirst, rotation, 16, 16, -1, m_groupName);
}

void DotSceneLoader::processSkyPlane(rapidxml::xml_node<>* xmlNode) const
{
	Ogre::LogManager::getSingleton().logMessage("[DotSceneLoader] Process SkyPlane");

	// Process attributes
	const Ogre::String material = getAttrib(xmlNode, "material");
	const Ogre::Real planeX     = getAttribReal(xmlNode, "planeX", 0);
	const Ogre::Real planeY     = getAttribReal(xmlNode, "planeY", -1);
	const Ogre::Real planeZ     = getAttribReal(xmlNode, "planeX", 0);
	const Ogre::Real planeD     = getAttribReal(xmlNode, "planeD", 5000);
	const Ogre::Real scale      = getAttribReal(xmlNode, "scale", 1000);
	const Ogre::Real bow        = getAttribReal(xmlNode, "bow", 0);
	const Ogre::Real tiling     = getAttribReal(xmlNode, "tiling", 10);
	const bool drawFirst        = getAttribBool(xmlNode, "drawFirst", true);

	// Setup the sky plane
	Ogre::Plane plane;
	plane.normal = Ogre::Vector3(planeX, planeY, planeZ);
	plane.d      = planeD;
	m_sceneMgr->setSkyPlane(true, plane, material, scale, tiling, drawFirst, bow, 1, 1, m_groupName);
}

void DotSceneLoader::processClipping(rapidxml::xml_node<>* /*xmlNode*/)
{
	//! @todo Implement this

	// Process attributes
	// Ogre::Real fNear = getAttribReal(xmlNode, "near", 0);
	// Ogre::Real fFar = getAttribReal(xmlNode, "far", 1);
}

void DotSceneLoader::processLightRange(rapidxml::xml_node<>* xmlNode, Ogre::Light* pLight)
{
	// Process attributes
	const Ogre::Real inner   = getAttribReal(xmlNode, "inner");
	const Ogre::Real outer   = getAttribReal(xmlNode, "outer");
	const Ogre::Real falloff = getAttribReal(xmlNode, "falloff", 1.0);

	// Setup the light range
	pLight->setSpotlightRange(Ogre::Angle(inner), Ogre::Angle(outer), falloff);
}

void DotSceneLoader::processLightAttenuation(rapidxml::xml_node<>* xmlNode, Ogre::Light* pLight)
{
	// Process attributes
	const Ogre::Real range     = getAttribReal(xmlNode, "range");
	const Ogre::Real constant  = getAttribReal(xmlNode, "constant");
	const Ogre::Real linear    = getAttribReal(xmlNode, "linear");
	const Ogre::Real quadratic = getAttribReal(xmlNode, "quadratic");

	// Setup the light attenuation
	pLight->setAttenuation(range, constant, linear, quadratic);
}

Ogre::String DotSceneLoader::getAttrib(rapidxml::xml_node<>* xmlNode, const Ogre::String& attrib, const Ogre::String& defaultValue)
{
	if (xmlNode->first_attribute(attrib.c_str())) { return xmlNode->first_attribute(attrib.c_str())->value(); }
	return defaultValue;
}

Ogre::Real DotSceneLoader::getAttribReal(rapidxml::xml_node<>* xmlNode, const Ogre::String& attrib, const Ogre::Real defaultValue)
{
	if (xmlNode->first_attribute(attrib.c_str())) { return Ogre::StringConverter::parseReal(xmlNode->first_attribute(attrib.c_str())->value()); }
	return defaultValue;
}

bool DotSceneLoader::getAttribBool(rapidxml::xml_node<>* xmlNode, const Ogre::String& attrib, const bool defaultValue)
{
	if (!xmlNode->first_attribute(attrib.c_str())) { return defaultValue; }

	if (Ogre::String(xmlNode->first_attribute(attrib.c_str())->value()) == "true") { return true; }

	return false;
}

Ogre::Vector3 DotSceneLoader::parseVector3(rapidxml::xml_node<>* xmlNode)
{
	return Ogre::Vector3(Ogre::StringConverter::parseReal(xmlNode->first_attribute("x")->value()),
						 Ogre::StringConverter::parseReal(xmlNode->first_attribute("y")->value()),
						 Ogre::StringConverter::parseReal(xmlNode->first_attribute("z")->value())
	);
}

Ogre::Quaternion DotSceneLoader::parseQuaternion(rapidxml::xml_node<>* xmlNode)
{
	//! @todo Fix this crap!

	Ogre::Quaternion orientation;

	if (xmlNode->first_attribute("qx"))
	{
		orientation.x = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qx")->value());
		orientation.y = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qy")->value());
		orientation.z = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qz")->value());
		orientation.w = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qw")->value());
	}
	if (xmlNode->first_attribute("qw"))
	{
		orientation.w = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qw")->value());
		orientation.x = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qx")->value());
		orientation.y = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qy")->value());
		orientation.z = Ogre::StringConverter::parseReal(xmlNode->first_attribute("qz")->value());
	}
	else if (xmlNode->first_attribute("axisX"))
	{
		Ogre::Vector3 axis;
		axis.x                 = Ogre::StringConverter::parseReal(xmlNode->first_attribute("axisX")->value());
		axis.y                 = Ogre::StringConverter::parseReal(xmlNode->first_attribute("axisY")->value());
		axis.z                 = Ogre::StringConverter::parseReal(xmlNode->first_attribute("axisZ")->value());
		const Ogre::Real angle = Ogre::StringConverter::parseReal(xmlNode->first_attribute("angle")->value());
		orientation.FromAngleAxis(Ogre::Angle(angle), axis);
	}
	else if (xmlNode->first_attribute("angleX"))
	{
		//Ogre::Vector3 axis;
		//axis.x = Ogre::StringConverter::parseReal(xmlNode->first_attribute("angleX")->value());
		//axis.y = Ogre::StringConverter::parseReal(xmlNode->first_attribute("angleY")->value());
		//axis.z = Ogre::StringConverter::parseReal(xmlNode->first_attribute("angleZ")->value());
		//orientation.FromAxes(&axis);
		//orientation.F
	}
	else if (xmlNode->first_attribute("x"))
	{
		orientation.x = Ogre::StringConverter::parseReal(xmlNode->first_attribute("x")->value());
		orientation.y = Ogre::StringConverter::parseReal(xmlNode->first_attribute("y")->value());
		orientation.z = Ogre::StringConverter::parseReal(xmlNode->first_attribute("z")->value());
		orientation.w = Ogre::StringConverter::parseReal(xmlNode->first_attribute("w")->value());
	}
	else if (xmlNode->first_attribute("w"))
	{
		orientation.w = Ogre::StringConverter::parseReal(xmlNode->first_attribute("w")->value());
		orientation.x = Ogre::StringConverter::parseReal(xmlNode->first_attribute("x")->value());
		orientation.y = Ogre::StringConverter::parseReal(xmlNode->first_attribute("y")->value());
		orientation.z = Ogre::StringConverter::parseReal(xmlNode->first_attribute("z")->value());
	}

	return orientation;
}

Ogre::ColourValue DotSceneLoader::parseColour(rapidxml::xml_node<>* xmlNode)
{
	return Ogre::ColourValue(Ogre::StringConverter::parseReal(xmlNode->first_attribute("r")->value()),
							 Ogre::StringConverter::parseReal(xmlNode->first_attribute("g")->value()),
							 Ogre::StringConverter::parseReal(xmlNode->first_attribute("b")->value()),
							 xmlNode->first_attribute("a") != nullptr ? Ogre::StringConverter::parseReal(xmlNode->first_attribute("a")->value()) : 1
	);
}

Ogre::String DotSceneLoader::getProperty(const Ogre::String& ndNm, const Ogre::String& prop)
{
	for (size_t i = 0; i < m_NodeProperties.size(); ++i)
	{
		if (m_NodeProperties[i].m_NodeName == ndNm && m_NodeProperties[i].m_PropertyName == prop) { return m_NodeProperties[i].m_ValueName; }
	}

	return "";
}

void DotSceneLoader::processUserDataReference(rapidxml::xml_node<>* xmlNode, Ogre::Entity* pEntity)
{
	const Ogre::String str = xmlNode->first_attribute("id")->value();
	pEntity->getUserObjectBindings().setUserAny(Ogre::Any(str));
}

#endif
