/*
 * @author Ogre
 * @note This code likely originates from the Ogre project
 *
 */
#pragma once

// Includes
#include <OgreVector3.h>
#include <OgreQuaternion.h>
#include <vector>

#include "rapidxml.hpp"

// Forward declarations
namespace Ogre {
class SceneManager;
class SceneNode;
class TerrainGroup;
class TerrainGlobalOptions;
}

/*
namespace Forests
{
	class PagedGeometry;
	class TreeLoader3D;
	class GrassLoader;
	class GrassLayer;
}
*/

class nodeProperty
{
public:
	Ogre::String m_NodeName;
	Ogre::String m_PropertyName;
	Ogre::String m_ValueName;
	Ogre::String m_TypeName;

	nodeProperty(const Ogre::String& node, const Ogre::String& propertyName, const Ogre::String& value, const Ogre::String& type)
		: m_NodeName(node), m_PropertyName(propertyName), m_ValueName(value), m_TypeName(type) {}
};

class DotSceneLoader
{
public:
	Ogre::TerrainGlobalOptions* m_TerrainGlobalOptions = nullptr;

	DotSceneLoader();
	virtual ~DotSceneLoader();

	void parseDotScene(const Ogre::String& sceneName, const Ogre::String& groupName, Ogre::SceneManager* yourSceneMgr,
					   Ogre::SceneNode* pAttachNode = nullptr, const Ogre::String& prependNode = "");
	Ogre::String getProperty(const Ogre::String& ndNm, const Ogre::String& prop);

	Ogre::String getResourceLocationBaseDir() const { return m_resLocationsBaseDir; }
	void setResourceLocationBaseDir(const Ogre::String& newBaseDir) { m_resLocationsBaseDir = newBaseDir; }

	// Ogre::TerrainGroup* getTerrainGroup() { return mTerrainGroup; }


	std::vector<nodeProperty> m_NodeProperties;
	std::vector<Ogre::String> m_StaticObjects;
	std::vector<Ogre::String> m_DynamicObjects;
	// std::vector<Forests::PagedGeometry *>   mPGHandles;
	// std::vector<Forests::TreeLoader3D *>    mTreeHandles;
	// Forests::GrassLoader*                   mGrassLoaderHandle;	// Handle to Forests::GrassLoader object
	std::vector<Ogre::ParticleSystem*> m_ParticleSystemList;	// contains all the Particle Systems created

protected:
	void processScene(rapidxml::xml_node<>* xmlRoot);

	void processNodes(rapidxml::xml_node<>* xmlNode);
	//! @todo Implement this
	static void processExternals(rapidxml::xml_node<>* /*xmlNode*/) { }
	void processEnvironment(rapidxml::xml_node<>* xmlNode) const;
	void processResourceLocations(rapidxml::xml_node<>* xmlNode) const;
	// void processTerrain(rapidxml::xml_node<>* xmlNode);
	// void processTerrainPage(rapidxml::xml_node<>* xmlNode);
	// void processGrassLayers(rapidxml::xml_node<>* xmlNode);
	// void processBlendmaps(rapidxml::xml_node<>* xmlNode);
	static void processUserDataReference(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent = nullptr);
	static void processUserDataReference(rapidxml::xml_node<>* xmlNode, Ogre::Entity* pEntity);
	static void processOctree(rapidxml::xml_node<>* xmlNode);
	void processLight(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* parent = nullptr);
	void processCamera(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent = nullptr) const;

	void processNode(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent = nullptr);
	void processLookTarget(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* parent) const;
	void processTrackTarget(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent) const;
	void processEntity(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent);
	static void processSubEntity(rapidxml::xml_node<>* xmlNode, Ogre::Entity* pEntity);
	void processParticleSystem(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent);
	static void processBillboardSet(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent);
	void processPlane(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode* pParent) const;
	// void processPagedGeometry(rapidxml::xml_node<>* xmlNode, Ogre::SceneNode *pParent);

	void processFog(rapidxml::xml_node<>* xmlNode) const;
	void processSkyBox(rapidxml::xml_node<>* xmlNode) const;
	void processSkyDome(rapidxml::xml_node<>* xmlNode) const;
	void processSkyPlane(rapidxml::xml_node<>* xmlNode) const;
	static void processClipping(rapidxml::xml_node<>* xmlNode);

	static void processLightRange(rapidxml::xml_node<>* xmlNode, Ogre::Light* pLight);
	static void processLightAttenuation(rapidxml::xml_node<>* xmlNode, Ogre::Light* pLight);

	static Ogre::String getAttrib(rapidxml::xml_node<>* xmlNode, const Ogre::String& attrib, const Ogre::String& defaultValue = "");
	static Ogre::Real getAttribReal(rapidxml::xml_node<>* xmlNode, const Ogre::String& attrib, Ogre::Real defaultValue = 0);
	static bool getAttribBool(rapidxml::xml_node<>* xmlNode, const Ogre::String& attrib, bool defaultValue = false);

	static Ogre::Vector3 parseVector3(rapidxml::xml_node<>* xmlNode);
	static Ogre::Quaternion parseQuaternion(rapidxml::xml_node<>* xmlNode);
	static Ogre::ColourValue parseColour(rapidxml::xml_node<>* xmlNode);


	Ogre::SceneManager* m_sceneMgr = nullptr;
	Ogre::SceneNode* m_attachNode  = nullptr;
	Ogre::String m_groupName;
	Ogre::String m_prependNode;
	// Ogre::TerrainGroup* mTerrainGroup = nullptr;
	Ogre::Vector3 m_terrainPosition;
	Ogre::Vector3 m_lightDirection;

	// paged geometry related values
	// int mPGPageSize = 0;
	// int mPGDetailDistance = 0;
	// resource locations basedir
	Ogre::String m_resLocationsBaseDir;
};
