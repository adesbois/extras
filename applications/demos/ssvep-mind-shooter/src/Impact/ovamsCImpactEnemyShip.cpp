#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCImpactEnemyShip.h"
#include "ovamsCImpactApplication.h"

#define IMPACTSHIP_SIZE 0.2f
#define SCREEN_RATIO ( 16.0f / 9.0f )

using namespace SSVEPMindShooter;
using namespace Ogre;

CImpactApplication* CImpactEnemyShip::m_application = nullptr;
SceneNode* CImpactEnemyShip::m_parentNode           = nullptr;
CBasicPainter* CImpactEnemyShip::m_painter          = nullptr;
int CImpactEnemyShip::m_nCurrentExplosion           = 0;

void CImpactEnemyShip::initialize(CImpactApplication* application)
{
	m_application = application;
	m_painter     = m_application->getPainter();
	m_parentNode  = m_application->getSceneNode();
}

CImpactEnemyShip* CImpactEnemyShip::createTarget(const Real position)
{
	if (m_application == nullptr)
	{
		std::cerr << "The CImpactEnemyShip was not initialized" << std::endl;
		return nullptr;
	}

	return new CImpactEnemyShip(position);
}

CImpactEnemyShip::CImpactEnemyShip(const Real position)
{
	m_EnemyLeaving = false;
	const int type = m_application->getNextTargetType();
	m_application->logPrefix() << "Creating enemy " << type << "\n";

	if (type > 0 && type <= 6)
	{
		switch (type)
		{
			case 1:
				m_application->getSceneLoader()->parseDotScene("v2.scene", "SSVEPImpact", m_application->getSceneManager());

				m_enemyNode = m_application->getSceneManager()->getSceneNode("v2_vaisseau");
				m_enemyNode->scale(1.0F, 1.0F, 1.0F);
				m_shipWidth      = 0.5F;
				m_LeaveCountdown = int(2.0F * 3600);
				m_pointValue     = 100;
				break;
			case 2:
				m_application->getSceneLoader()->parseDotScene("v1.scene", "SSVEPImpact", m_application->getSceneManager());

				m_enemyNode = m_application->getSceneManager()->getSceneNode("v1_vaisseau");
				m_enemyNode->scale(1.9F, 1.9F, 1.9F);
				m_shipWidth      = 0.45F;
				m_LeaveCountdown = int(1.5F * 3600);
				m_pointValue     = 200;
				break;
			case 3:
				m_application->getSceneLoader()->parseDotScene("v3.scene", "SSVEPImpact", m_application->getSceneManager());

				m_enemyNode = m_application->getSceneManager()->getSceneNode("v3_vaisseau");
				m_enemyNode->scale(2.2F, 2.0F, 2.0F);
				m_shipWidth      = 0.4F;
				m_LeaveCountdown = int(1.0F * 3600);
				m_pointValue     = 400;
				break;
			case 4:
				m_application->getSceneLoader()->parseDotScene("ship-cheap.scene", "SSVEPImpact", m_application->getSceneManager());
				m_enemyNode  = m_application->getSceneManager()->getSceneNode("ship-cheap_node");
				m_pointValue = 500;
				break;
			case 5:
				m_application->getSceneLoader()->parseDotScene("ship-costly.scene", "SSVEPImpact", m_application->getSceneManager());
				m_enemyNode  = m_application->getSceneManager()->getSceneNode("ship-costly_node");
				m_pointValue = 1500;
				break;
			case 6:
				m_application->getSceneLoader()->parseDotScene("ship-friend.scene", "SSVEPImpact", m_application->getSceneManager());
				m_enemyNode  = m_application->getSceneManager()->getSceneNode("ship-friend_node");
				m_pointValue = -500;
				break;
			default:
				break;
		}

		if (type >= 4 && type <= 6)
		{
			m_enemyNode->scale(1.2F, 1.2F, 1.2F);
			m_shipWidth      = 0.3F;
			m_LeaveCountdown = int(1.0F * 3600);
		}


		m_enemyNode->yaw(Radian(Math::PI));

		m_hitBox = m_application->getSceneNode()->createChildSceneNode();


		//m_hitBox->attachObject(m_painter->paintTriangle(Vector2(+ m_shipWidth / 2, +0.9), Vector2(- m_shipWidth / 2, +0.9), Vector2( 0.0, +0.5)));


		m_hitBox->translate(position / 70.0F, 1.2F, 0.0F, Node::TS_LOCAL);
		m_enemyNode->translate(position, 0.0F, -60.0F, Node::TS_LOCAL);
		m_incomingStatus = 30.0F;
	}
	else { m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Fatal << "Unknown enemy type " << type << " ! The application will now crash!\n"; }
}

void CImpactEnemyShip::destroyAllAttachedMovableObjects(SceneNode* sceneNode)
{
	if (!sceneNode) { return; }

	// Destroy all the attached objects
	SceneNode::ObjectIterator itObject = sceneNode->getAttachedObjectIterator();

	while (itObject.hasMoreElements())
	{
		MovableObject* pObject = static_cast<MovableObject*>(itObject.getNext());
		sceneNode->getCreator()->destroyMovableObject(pObject);
	}

	SceneNode::ChildNodeIterator itChild = sceneNode->getChildIterator();

	while (itChild.hasMoreElements())
	{
		SceneNode* pChildNode = dynamic_cast<SceneNode*>(itChild.getNext());
		destroyAllAttachedMovableObjects(pChildNode);
	}
}

CImpactEnemyShip::~CImpactEnemyShip()
{
	m_application->logPrefix() << "Enemy Destroyed\n";

	destroyAllAttachedMovableObjects(m_enemyNode);
	m_enemyNode->removeAndDestroyAllChildren();
	m_enemyNode->getCreator()->destroySceneNode(m_enemyNode);

	destroyAllAttachedMovableObjects(m_hitBox);
	m_hitBox->removeAndDestroyAllChildren();
	m_hitBox->getCreator()->destroySceneNode(m_hitBox);
}

void CImpactEnemyShip::processFrame()
{
	// oscillation animation
	static int oscillator = 90;

	if (m_LeaveCountdown > 0 && !m_EnemyLeaving) { m_LeaveCountdown--; }
	if (m_LeaveCountdown == 0 && !m_EnemyLeaving)
	{
		m_application->logPrefix() << "Enemy Leaving\n";

		m_EnemyLeaving = true;
		m_pointValue   = 0;
	}

	if (!m_EnemyLeaving)
	{
		oscillator++;
		oscillator = oscillator % 360;
		// m_enemyNode->roll(Radian(Math::Sin(Degree(sin_oscillator)) / 80));
		m_enemyNode->translate(Math::Sin(Degree(Real(oscillator))) / 20.0F * 0, Math::Sin(Degree(Real(oscillator * 2 + 90))) / 20, 0.0F,
							   Node::TS_LOCAL);
		m_hitBox->translate(Math::Sin(Degree(Real(oscillator))) / 20.0F / 50.0F * 0, 0.0F, 0.0F, Node::TS_LOCAL);


		// incoming enemy animation
		if (m_incomingStatus > 0)
		{
			m_hitBox->translate(0.0F, -0.04F, 0.0F, Node::TS_LOCAL);
			m_enemyNode->translate(0.0F, 0.0F, 1.0F, Node::TS_LOCAL);
			m_incomingStatus -= 1.0F;
		}

		// exploding ship animation
		if (m_destructionStatus > 3)
		{
			m_destructionStatus++;

			m_enemyNode->translate(0.1F, 2, 1, Node::TS_PARENT);
			m_enemyNode->rotate(Vector3(0.3F, 0.25F, 1.0F), Radian(3.14F / 20.0F), Node::TS_LOCAL);
			m_enemyNode->scale(0.95F, 0.95F, 0.95F);
		}
	}
	else
	{
		m_enemyNode->translate(0.0F, -1.0F, float(m_LeaveCountdown) * 0.5F, Node::TS_LOCAL);
		m_LeaveCountdown++;

		if (m_enemyNode->getPosition().z < -100)
		{
			m_shipDestroyed = true;
			m_application->logPrefix() << "Enemy Left : marked as destroyed\n";
		}
	}

	if (m_destructionStatus > 50)
	{
		m_shipDestroyed = true;
		if (this->getPointValue() > 0) { m_application->logPrefix() << "Enemy Shot Down : marked as destroyed\n"; }
		else { m_application->logPrefix() << "Friendly Target Shot Down : marked as destroyed\n"; }
	}


	// handle the explosions
	for (auto it = m_explosions.begin(); it != m_explosions.end(); ++it)
	{
		(*it)->ttl--;

		if ((*it)->ttl < 4 && (*it)->ttl >= 0)
		{
			ParticleEmitter* emitter = (*it)->particleSystem->getEmitter(0);
			emitter->setEmissionRate(emitter->getEmissionRate() - 50);
		}

		if (explostionExhausted(*it))
		{
			m_painter->getSceneManager()->destroyParticleSystem((*it)->particleSystem);
			(*it)->node->getCreator()->destroySceneNode((*it)->node);
			delete *it;
		}
	}

	m_explosions.erase(std::remove_if(m_explosions.begin(), m_explosions.end(), explostionExhausted), m_explosions.end());
}

void CImpactEnemyShip::processHit(const Vector2 point)
{
	// set status of the ship as exploding
	if (m_destructionStatus <= 3)
	{
		m_destructionStatus++;
		m_application->logPrefix() << "Enemy Lost a Hitpoint : current hits = " << m_destructionStatus << "\n";
	}

	// create the explosion upon hit
	struct SExplosionAnimation* explosion = new struct SExplosionAnimation;
	const char* projectileName            = ("Explosion " + std::to_string(++m_nCurrentExplosion)).c_str();
	explosion->particleSystem             = m_painter->getSceneManager()->createParticleSystem(projectileName, "Particle/Explosion");


	explosion->ttl  = 25;
	explosion->node = m_parentNode->createChildSceneNode();
	explosion->node->attachObject(explosion->particleSystem);
	explosion->node->translate(point.x * -65.0F, 0.0F, 30.0F, Node::TS_WORLD);

	m_explosions.push_back(explosion);
}

#endif
