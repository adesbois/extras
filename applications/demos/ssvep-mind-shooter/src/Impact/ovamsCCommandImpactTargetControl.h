#pragma once

#include "../ovamsICommandVRPNButton.h"
#include "../ovamsCVRPNServer.h"

namespace SSVEPMindShooter {
class CImpactApplication;

class CCommandImpactTargetControl final : public ICommandVRPNButton
{
public:
	explicit CCommandImpactTargetControl(CImpactApplication* application);
	~CCommandImpactTargetControl() override {}

	void execute(const int button, const int state) override;
	void processFrame() override;

private:
	CVRPNServer* m_vrpnServer = nullptr;
};
}  // namespace SSVEPMindShooter
