#pragma once

#include <vector>

#include <Ogre.h>

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace SSVEPMindShooter {
class CImpactApplication;
class CImpactEnemyShip;
class CSSVEPFlickeringObject;

class CImpactShip
{
public:
	enum ETargetState { TS_NORMAL, TS_INHIBITED, TS_OFF };

	//SP			CImpactShip( CImpactApplication* application, Ogre::SceneNode* parentNode, std::vector<std::pair<size_t, size_t> >* pFrequencies);
	CImpactShip(CImpactApplication* application, Ogre::SceneNode* parentNode, std::vector<size_t>* pFrequencies);
	void processFrame(const size_t currentFrame);

	void moveToPosition() { m_prepareAnimationFrame = 70; }
	void move(int displacement);
	void shoot();
	bool evaluateHit(CImpactEnemyShip* enemy);
	void setAllTargetsVisibility(bool visible) const;
	Ogre::Vector2 getPosition() const;
	Ogre::Vector2 getNormalizedPosition() const;
	void setTargetLocked(const bool targetLocked) { m_targetLocked = targetLocked; }

	void setFeedbackLevels(int f1, int f2, int f3);
	void activatePilotAssist(const bool activate);
	void activateCannonInhibitor(const bool activate);
	void activateTargetLockdown(const bool activate);
	void activateFeedback(const bool activate);
	void activateFocusPoint(const bool activate);
	void returnToMiddleAndWaitForFoe(size_t targetPosition, Ogre::Real position = 0.0);

	ETargetState getTargetState(const int target) { return m_targetStates[target]; }

private:

	struct SBlasterProjectile
	{
		Ogre::ParticleSystem* pParticleSystem;
		Ogre::SceneNode* pNode;
		Ogre::SceneNode* pOutlineNode;
		int iTTL;
		Ogre::Real rRelX, rRelY;
	};

	static bool projectileExhausted(struct SBlasterProjectile* /*bp*/);

	CImpactApplication* m_application = nullptr;

	Ogre::SceneNode* m_shipNode        = nullptr;
	Ogre::SceneNode* m_targetingNode   = nullptr;
	Ogre::SceneNode* m_projectilesNode = nullptr;

	CSSVEPFlickeringObject* m_shipCannon    = nullptr;
	CSSVEPFlickeringObject* m_shipLeftWing  = nullptr;
	CSSVEPFlickeringObject* m_shipRightWing = nullptr;

	ETargetState m_targetStates[3];

	int m_prepareAnimationFrame = 0;
	bool m_targetLocked         = true;
	bool m_returning            = false;
	std::deque<size_t> m_nextEnemyPosition;
	bool m_pilotAssistActive    = false;
	bool m_targetLockdownActive = false;
	bool m_feedbackActive       = false;
	bool m_focusPointActive     = false;
	bool m_cannonInhibited      = false;

	Ogre::Radian m_currentAngle;
	int m_nCurrentDisplacement = 0;

	int m_shootingCooldown = 0;
	std::vector<struct SBlasterProjectile*> m_projectiles;
	int m_nShot              = 0;
	Ogre::Real m_origin      = 0.0;
	Ogre::Real m_oldPosition = 0;
};
}  // namespace SSVEPMindShooter
