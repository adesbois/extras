#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCImpactApplication.h"

#include "openvibe/ov_all.h"

#include "../ovamsCCommandStartStop.h"
#include "../ovamsCCommandStimulatorControl.h"
#include "../ovamsCCommandCamera.h"
#include "ovamsCCommandImpactTargetControl.h"
#include "ovamsCCommandImpactCustomTargetControl.h"

#include "ovamsCCommandControlAnalog.h"
#include "ovamsCCommandControlDiscrete.h"
#include "ovamsCCommandControlDiscreteOIS.h"

#include "../log/ovkCLogListenerFileBuffered.h"


#include <ctime>

using namespace Ogre;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace SSVEPMindShooter;

#define DIALOG_CYCLES_TO_REMOVE 180
//#define NO_KEYBOARD
//#define NO_VRPN

#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
	typedef CEGUI::UVector2 USize;
};
#endif

CImpactApplication::CImpactApplication(const CString& scenarioDir, const CString& applicationSubtype)
	: CApplication(scenarioDir), m_applicationSubtype(applicationSubtype) {}

CImpactApplication::~CImpactApplication()
{
	(*m_logManager) << LogLevel_Debug << "- m_ship\n";
	delete m_ship;
}

bool CImpactApplication::setup(IKernelContext* kernelCtx)
{
	if (!CApplication::setup(kernelCtx)) { return false; }

	IConfigurationManager* configurationManager = &(m_kernelCtx->getConfigurationManager());

	if (m_applicationSubtype == CString("shooter"))
	{
		// A config file appended for the online session
		const CString configFile = m_ScenarioDir + "/appconf/shooter-configuration.conf";
		if (!configurationManager->addConfigurationFromFile(configFile))
		{
			this->getLogManager() << LogLevel_Error << "Unable to open [" << configFile << "]. Has the shooter-controller.lua been run?\n";
			return false;
		}
	}

	const uint64_t logLevel = kernelCtx->getTypeManager().getEnumerationEntryValueFromName(
		OV_TypeId_LogLevel, configurationManager->expand("${SSVEP_LogLevel}"));
	this->getLogManager() << LogLevel_Info << "Parsed loglevel is " << logLevel << "\n";
	if (logLevel != 0xffffffffffffffffULL) { kernelCtx->getLogManager().activate(ELogLevel(logLevel), LogLevel_Last, true); }

	ILogListener* logListenerFileBuffered = new CLogListenerFileBuffered(*kernelCtx, "ssvep-mind-shooter-stimulator",
																		 configurationManager->expand(
																			 "${SSVEP_UserDataFolder}/mind-shooter-[$core{date}-$core{time}]-app.log"));
	kernelCtx->getLogManager().addListener(logListenerFileBuffered);


	m_sceneLoader = new DotSceneLoader();

	std::stringstream enemyOrder;
	enemyOrder << configurationManager->expand("${SSVEP_EnemyOrder}");

	(*m_logManager) << LogLevel_Info << "Enemy order: " << configurationManager->expand("${SSVEP_EnemyOrder}") << "\n";

	while (enemyOrder.peek() != EOF)
	{
		int nextType;
		enemyOrder >> nextType;
		m_enemyOrder.push(nextType);
	}

	(*m_logManager) << LogLevel_Debug << "Adding Impact game resources\n";


	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact")).toASCIIString(), "FileSystem",
		"SSVEPImpact", true);
	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact/battleship")).toASCIIString(),
		"FileSystem", "SSVEPImpact");
	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact/battleship-destroyed")).toASCIIString(),
		"FileSystem", "SSVEPImpact");
	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact/enemy1")).toASCIIString(), "FileSystem",
		"SSVEPImpact");
	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact/enemy2")).toASCIIString(), "FileSystem",
		"SSVEPImpact");
	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact/enemy3")).toASCIIString(), "FileSystem",
		"SSVEPImpact");
	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact/ships")).toASCIIString(), "FileSystem",
		"SSVEPImpact");
	ResourceGroupManager::getSingleton().addResourceLocation(
		CString(configurationManager->expand("${Path_Data}/applications/${SSVEP_MindShooterFolderName}/resources/impact/starsky")).toASCIIString(),
		"FileSystem", "SSVEPImpact");
	ResourceGroupManager::getSingleton().initialiseResourceGroup("SSVEPImpact");

	setupScene();

	// Create the StarShip object
	(*m_logManager) << LogLevel_Debug << "+ m_ship = new CStarShip(...)\n";
	m_ship = new CImpactShip(this, m_sceneNode, &m_frequencies);
	m_ship->activatePilotAssist(configurationManager->expandAsBoolean("${SSVEP_PilotAssist}"));
	m_ship->activateTargetLockdown(configurationManager->expandAsBoolean("${SSVEP_TargetLockdown}") && (m_applicationSubtype == CString("shooter")));
	m_ship->setFeedbackLevels(0, 0, 0);
	m_ship->activateFeedback(configurationManager->expandAsBoolean("${SSVEP_Feedback}") && (m_applicationSubtype == CString("shooter")));
	m_ship->activateFocusPoint(configurationManager->expandAsBoolean("${SSVEP_FocusPoint}"));

	// Initialize the Target class

	CImpactEnemyShip::initialize(this);

	// create CEGUI windows
	m_instructionWindow = static_cast<CEGUI::Window*>(m_guiWindowManager->createWindow("TaharezLook/StaticText", "InstructionsWindow"));
	m_instructionWindow->setFont("BlueHighway-impact");
	m_statusWindow = static_cast<CEGUI::Window*>(m_guiWindowManager->createWindow("TaharezLook/StaticText", "StatusWindow"));
	m_statusWindow->setFont("BlueHighway-impact");
	m_statusWindow->hide();

#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(m_statusWindow);
	m_sheet->addChild(m_instructionWindow);
#else
	m_sheet->addChildWindow(m_statusWindow);
	m_sheet->addChildWindow(m_instructionWindow);
#endif

	// Create commands


#ifndef NO_KEYBOARD
	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandCamera(...)\n";
	this->addCommand(new CCommandCamera(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandShipControlOIS(...))\n";
	this->addCommand(new CCommandControlDiscreteOIS(this));
#endif

#ifndef NO_VRPN

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStartStop(...)\n";
	this->addCommand(new CCommandStartStop(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStimulatorControl(...))\n";
	this->addCommand(new CCommandStimulatorControl(this));

	if (m_applicationSubtype == CString("trainer"))
	{
		(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandTargetControl(...)\n";
		this->addCommand(new CCommandImpactTargetControl(this));
	}
	else
	{
		if (configurationManager->expandAsBoolean("${SSVEP_OneByOne}", false))
		{
			(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandTargetControl(...)\n";
			this->addCommand(new CCommandImpactTargetControl(this));
		}
		else
		{
			(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandImpactCustomTargetControl(...)\n";
			this->addCommand(new CCommandImpactCustomTargetControl(this));
		}

		const bool analogControls = configurationManager->expandAsBoolean("${SSVEP_AnalogControl}", false);

		(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandControlAnalog(...))\n";
		this->addCommand(new CCommandControlAnalog(this, analogControls));

		(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandControlDiscrete(...))\n";
		this->addCommand(new CCommandControlDiscrete(this, !analogControls));

		(*m_logManager) << LogLevel_Info << "Analog control mode: " << (analogControls ? "true" : "false") << "\n";
	}
#endif

	if (m_applicationSubtype == CString("shooter"))
	{
		m_gameState = IDLE_STARTED;
		m_ship->moveToPosition();

		// draw the initial text
		m_instructionWindow->show();
		m_instructionWindow->setSize(CEGUI::USize(CEGUI::UDim(0.50F, 0), CEGUI::UDim(0.10F, 0)));
		m_instructionWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25F, 0), CEGUI::UDim(0.45F, 0)));
		// m_instructionWindow->setText("Appuyez sur la touche ESPACE pour commencer le jeu ...");
		m_instructionWindow->setText("Press SPACE to start the game ...");
	}
	else
	{
		m_gameState = IDLE_TRAINING;

		// draw the initial text
		m_instructionWindow->show();
		m_instructionWindow->setSize(CEGUI::USize(CEGUI::UDim(0.50F, 0), CEGUI::UDim(0.15F, 0)));
		m_instructionWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.25F, 0), CEGUI::UDim(0.425F, 0)));
		// m_instructionWindow->setText("Le systeme a besoin de faire quelques reglages.\nVeuillez vous concentrer sur les cibles indiques par les instructions.\n\nAppuyez sur la touche ESPACE pour commencer l'entrainement ...");
		m_instructionWindow->setText(
			"The system needs to calibrate.\nPlease concentrate on the targets as instructed.\n\nPlease press SPACE to start the process ...");
	}
	return true;
}

void CImpactApplication::setupScene() const
{
	m_sceneManager->setAmbientLight(ColourValue(1.0F, 1.0F, 1.0F));
	m_camera->setNearClipDistance(5);

	m_camera->setPosition(Vector3(0, 100, 0));
	m_camera->lookAt(0, 0, 1);
	m_camera->move(Vector3(0, 0, 0));

	Light* pointLight = m_sceneManager->createLight();
	pointLight->setType(Light::LT_POINT);
	pointLight->setPosition(Vector3(0, 50, 0));

	pointLight->setDiffuseColour(1.0F, 1.0F, 0.8F);
	pointLight->setSpecularColour(1.0F, 1.0F, 1.0F);

	pointLight = m_sceneManager->createLight();
	pointLight->setType(Light::LT_POINT);
	pointLight->setPosition(Vector3(10, -10, 0));

	pointLight->setDiffuseColour(0.5F, 0.0F, 0.0F);

	m_sceneLoader->parseDotScene("v6.scene", "SSVEPImpact", m_sceneManager);
	m_sceneManager->getSceneNode("v6_sky")->showBoundingBox(true);
	m_sceneManager->getSceneNode("v6_sky")->scale(200.0, 300.0, 300.0);
	m_sceneManager->getSceneNode("v6_sky")->translate(0.0, 100.0, 0.0);
}

bool CImpactApplication::enemyDestroyed(CImpactEnemyShip* es) { return es->isDestroyed(); }


void CImpactApplication::processFrame(const size_t frame)
{
	CApplication::processFrame(frame);
	//std::cout << m_window->getAverageFPS() << "\n";

	m_ship->processFrame(frame);
	m_ship->setTargetLocked(false);
	m_sceneManager->getSceneNode("v6_sky")->pitch(Radian(0.0001F));

	if (m_gameState == STARTED)
	{
		size_t nEnemy           = 0;
		bool targetWasDestroyed = false;

		int currentEnemyPointValue = 0;

		if (m_CurrEnemy != nullptr) { currentEnemyPointValue = m_CurrEnemy->getPointValue(); }

		m_CurrEnemy = nullptr;
		for (auto it = m_targets.begin(); it != m_targets.end(); ++it)
		{
			(*it)->processFrame();

			if ((*it)->isTouchable())
			{
				m_ship->evaluateHit(*it);

				const Vector2 enemyPosition = (*it)->getEnemyPosition();
				if (enemyPosition.x > m_ship->getNormalizedPosition().x - (*it)->getWidth() / 2.0
					&& enemyPosition.x < m_ship->getNormalizedPosition().x + (*it)->getWidth() / 2.0)
				{
					m_CurrEnemy = (*it);
					m_ship->setTargetLocked(true);

					if (currentEnemyPointValue != m_CurrEnemy->getPointValue())
					{
						m_ship->activateCannonInhibitor(m_CurrEnemy->getPointValue() < 0);

						logPrefix() << "Targeting enemy : point value = " << m_CurrEnemy->getPointValue() << "\n";
					}
				}

				if ((*it)->getPointValue() > 0) { nEnemy++; }
			}


			if ((*it)->isDestroyed())
			{
				m_score += (*it)->getPointValue();
				delete *it;
				m_statusWindow->setText(("Score: " + std::to_string(m_score)).c_str());
				targetWasDestroyed = true;
			}
		}

		if (currentEnemyPointValue != 0 && m_CurrEnemy == nullptr) { logPrefix() << "Focus on enemy lost\n"; }

		if (targetWasDestroyed)
		{
			const size_t targetCount = m_targets.size();

			m_targets.erase(std::remove_if(m_targets.begin(), m_targets.end(), enemyDestroyed), m_targets.end());

			if (m_targets.empty() && m_targets.size() < targetCount) { m_TargetRequest = true; }

			// if there are only friendly targets remaining make them leave
			if (targetCount > 0 && nEnemy == 0)
			{
				for (auto it = m_targets.begin(); it != m_targets.end(); ++it) { if (!(*it)->m_EnemyLeaving) { (*it)->m_LeaveCountdown = 0; } }
			}
		}
	}

	if (m_dialogHideDelay > 0) { if (--m_dialogHideDelay == 0) { m_instructionWindow->hide(); } }
}

int CImpactApplication::getNextTargetType()
{
	if (!m_enemyOrder.empty())
	{
		const int iNtt = m_enemyOrder.front();
		m_enemyOrder.pop();

		return iNtt;
	}

	return 0;
}

void CImpactApplication::addTarget(const size_t targetPosition)
{
	float textSize = 0.25F;

	logPrefix() << "Add Target : position = " << targetPosition << "\n";

	getLogManager() << LogLevel_Info << "Adding target on position " << targetPosition << "\n";

	if (m_gameState == TRAINING)
	{
		switch (targetPosition)
		{
			case 0:
				textSize = 0.35F;
				// m_instructionWindow->setText("Regardez au milieu (MIDDLE) du vaisseau!");
				m_instructionWindow->setText("Focus on the MIDDLE of the ship");
				this->m_StimulusSender->sendStimulation(OVTK_StimulationId_Label_00);
				break;

			case 1:
				textSize = 0.35F;
				// m_instructionWindow->setText("Regardez le CANON (CANNON) du vaisseau pour tirer!");
				m_instructionWindow->setText("Focus on the CANNON to fire!");
				this->m_StimulusSender->sendStimulation(OVTK_StimulationId_Label_01);
				break;

			case 2:
				textSize = 0.50F;
				// m_instructionWindow->setText("Regardez l'aile GAUCHE (LEFT WING) du vaisseau pour de`placer le vaisseau vers la gauche");
				m_instructionWindow->setText("Focus on the LEFT WING to turn the ship to the left");
				this->m_StimulusSender->sendStimulation(OVTK_StimulationId_Label_02);
				break;

			case 3:
				textSize = 0.50F;
				// m_instructionWindow->setText("Regardez l'aile DROITE (RIGHT WING) du vaisseau pour de`placer le vaisseau vers la droite");
				m_instructionWindow->setText("Focus on the RIGHT WING to turn the ship to the right");
				this->m_StimulusSender->sendStimulation(OVTK_StimulationId_Label_03);
				break;
			default: break;
		}

		m_instructionWindow->setSize(CEGUI::USize(CEGUI::UDim(textSize, 0), CEGUI::UDim(0.10F, 0)));
		m_instructionWindow->setPosition(CEGUI::UVector2(CEGUI::UDim((1.0F - textSize) / 2.0F, 0), CEGUI::UDim(0.45F, 0)));
		m_instructionWindow->show();
		m_dialogHideDelay = DIALOG_CYCLES_TO_REMOVE;
	}
	else if (m_gameState == STARTED)
	{
		// std::cout << (Ogre::Real( targetPosition ) - 3.5) / 7.0 * 100.0 << "\n";
		if (!m_enemyOrder.empty()) { m_ship->returnToMiddleAndWaitForFoe(targetPosition, m_NextOrigin); }
		else { getLogManager() << LogLevel_Error << "No more enemy types\n"; }
	}
}

void CImpactApplication::insertEnemy(const size_t targetPosition)
{
	logPrefix() << "Creating Enemy : position = " << targetPosition << "\n";
	m_targets.push_back(CImpactEnemyShip::createTarget((Real(targetPosition) - 3.5F) / 7.0F * 100.0F));
}

void CImpactApplication::startFlickering()
{
	m_instructionWindow->hide();
	const size_t currentTime = size_t(time(nullptr) - m_startTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Starting Visual Stimulation\n";
	m_active = true;
}

void CImpactApplication::stopFlickering()
{
	const size_t currentTime = size_t(time(nullptr) - m_startTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Stopping Visual Stimulation\n";
	m_active = false;
	m_ship->setAllTargetsVisibility(true);
}

void CImpactApplication::startExperiment()
{
	CApplication::startExperiment();

	if (m_gameState == IDLE_TRAINING)
	{
		m_startTime = time(nullptr);
		m_instructionWindow->hide();
		m_dialogHideDelay = 0;
		m_gameState       = TRAINING;

		m_ship->activatePilotAssist(false);

		logPrefix() << "Start Experiment : IDLE_TRAINING -> TRAINING\n";
	}
	else if (m_gameState == IDLE_STARTED)
	{
		m_TargetRequest = true;
		m_active        = true;
		m_instructionWindow->hide();
		m_dialogHideDelay = 0;
		m_statusWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0F, 0), CEGUI::UDim(0.0F, 0)));
		m_statusWindow->setSize(CEGUI::USize(CEGUI::UDim(0.15F, 0), CEGUI::UDim(0.05F, 0)));
		m_statusWindow->setText("Score: 0");
		m_statusWindow->show();
		m_gameState = STARTED;

		logPrefix() << "Start Experiment : IDLE_STARTED -> STARTED\n";
	}
}

void CImpactApplication::stopExperiment()
{
	if (m_gameState == TRAINING)
	{
		if (m_applicationSubtype == CString("trainer"))
		{
			logPrefix() << "Stop Experiment : exiting\n";
			this->exit();
		}
		else
		{
			const float textSize = 0.35F;
			// m_instructionWindow->setText("L'application va prendre quelque temps pour le calcul, veuillez patienter.\nLe jeu va commencer automatiquement une fois l'application est prete.");
			m_instructionWindow->setText(
				"The application needs some time to compute, please be patient.\nThe game starts automatically when the application is ready.");
			m_instructionWindow->setSize(CEGUI::USize(CEGUI::UDim(textSize, 0), CEGUI::UDim(0.10F, 0)));
			m_instructionWindow->setPosition(CEGUI::UVector2(CEGUI::UDim((1.0F - textSize) / 2.0F, 0), CEGUI::UDim(0.45F, 0)));
			m_instructionWindow->show();

			logPrefix() << "Stop Experiment : Training Finished\n";
		}
	}

	if (m_gameState == STARTED)
	{
		const float textSize = 0.45F;
		m_instructionWindow->setText(("Bravo!\nYou have obtained " + std::to_string(m_score) + " points!\nPress ESC to close the program").c_str());
		m_instructionWindow->setSize(CEGUI::USize(CEGUI::UDim(textSize, 0), CEGUI::UDim(0.10F, 0)));
		m_instructionWindow->setPosition(CEGUI::UVector2(CEGUI::UDim((1.0F - textSize) / 2.0F, 0), CEGUI::UDim(0.45F, 0)));
		m_instructionWindow->show();
		m_active = false;
		m_ship->setAllTargetsVisibility(true);

		logPrefix() << "Stop Experiment : Game Ended\n";
	}
}

void CImpactApplication::debugAction1()
{
	(*m_logManager) << LogLevel_Debug << "Debug Action 1 triggered\n";
	this->startExperiment();
	m_ship->activatePilotAssist(true);
}

void CImpactApplication::debugAction2()
{
	(*m_logManager) << LogLevel_Info << "Debug Action 2 triggered\n";

	//if (m_TargetRequest && m_enemyOrder.size() > 2)
	{
		this->addTarget(1);
		this->addTarget(6);
		this->addTarget(4);
		m_TargetRequest = false;
	}
}

void CImpactApplication::debugAction3()
{
	(*m_logManager) << LogLevel_Info << "Debug Action 3 triggered\n";

	m_TargetRequest = true;
}

void CImpactApplication::debugAction4()
{
	(*m_logManager) << LogLevel_Info << "Debug Action 4 triggered\n";
	(*m_logManager) << LogLevel_Info << "You (X: " << m_ship->getPosition().x << ", Y: " << m_ship->getPosition().y << ")\n";

	if (!m_targets.empty())
	{
		const Vector2 enemyPosition = m_targets[0]->getEnemyPosition();
		(*m_logManager) << LogLevel_Info << "Him (X: " << enemyPosition.x << ", Y: " << enemyPosition.y << ")\n";
	}
}

#endif
