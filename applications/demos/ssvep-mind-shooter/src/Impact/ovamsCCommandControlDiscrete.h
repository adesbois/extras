/// Control ship movements with VRPN using label predictions from the classifier
// If bTakeControl is false, the class will do nothing.

#pragma once

#include "../ovamsICommandVRPNButton.h"

namespace SSVEPMindShooter {
class CImpactApplication;

class CCommandControlDiscrete final : public ICommandVRPNButton
{
public:
	CCommandControlDiscrete(CImpactApplication* application, bool takeControl);
	~CCommandControlDiscrete() override {}

	void execute(const int button, const int state) override;
protected:
	bool m_inControl = false;
};
}  // namespace SSVEPMindShooter
