#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "dotscene/DotSceneLoader.h"

#include "../ovamsCApplication.h"
#include "ovamsCImpactEnemyShip.h"
#include "ovamsCImpactShip.h"
#include <array>


namespace SSVEPMindShooter {
class CImpactApplication final : public CApplication
{
public:
	enum EGameState { BOOTING, IDLE_TRAINING, TRAINING, IDLE_STARTED, STARTED };

	struct SStimulatorState
	{
		float ship_position;
		std::array<double, 3> feedback_level;
		int ship_direction;
		bool shooting;
	};

	struct SGameState
	{
		int movements_right;
		int movements_left;
		int shoots_fired;
	};

	CImpactApplication(const OpenViBE::CString& scenarioDir, const OpenViBE::CString& applicationSubtype);
	~CImpactApplication() override;

	bool setup(OpenViBE::Kernel::IKernelContext* kernelCtx) override;

	CImpactShip* getShip() const { return m_ship; }
	bool isActive() const { return m_active; }

	void startExperiment() override;
	void stopExperiment() override;
	void startFlickering() override;
	void stopFlickering() override;

	void debugAction1() override;
	void debugAction2() override;
	void debugAction3() override;
	void debugAction4() override;

	int getNextTargetType();
	void addTarget(size_t targetPosition);

	void insertEnemy(size_t targetPosition);
	CImpactEnemyShip* getCurrentEnemy() const { return m_CurrEnemy; }
	OpenViBE::CString getSubtype() const { return m_applicationSubtype; }
	EGameState getState() const { return m_gameState; }
	DotSceneLoader* getSceneLoader() const { return m_sceneLoader; }
	CEGUI::Window* getInstructionWindow() const { return m_instructionWindow; }

	CImpactEnemyShip* m_CurrEnemy = nullptr;
	bool m_TargetRequest          = false;
	Ogre::Real m_NextOrigin       = 0.0;

private:
	void setupScene() const;
	void processFrame(const size_t frame) override;
	static bool enemyDestroyed(CImpactEnemyShip* /*es*/);


	OpenViBE::CString m_applicationSubtype;
	EGameState m_gameState = BOOTING;

	std::queue<int> m_enemyOrder;

	bool m_active = false;

	time_t m_startTime;

	DotSceneLoader* m_sceneLoader      = nullptr;
	CEGUI::Window* m_statusWindow      = nullptr;
	CEGUI::Window* m_instructionWindow = nullptr;
	size_t m_dialogHideDelay           = 0;

	CImpactShip* m_ship = nullptr;
	std::vector<CImpactEnemyShip*> m_targets;
	int m_score = 0;
};
}  // namespace SSVEPMindShooter
