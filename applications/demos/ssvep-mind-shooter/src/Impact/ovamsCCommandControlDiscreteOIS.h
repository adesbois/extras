/// Control ship movements with the keyboard

#pragma once

#include <map>

#include "../ovamsICommandOIS.h"

namespace SSVEPMindShooter {
class CImpactApplication;

class CCommandControlDiscreteOIS final : public ICommandOIS
{
public:
	explicit CCommandControlDiscreteOIS(CImpactApplication* application);
	~CCommandControlDiscreteOIS() override {}

	void processFrame() override;

	void receiveKeyPressedEvent(OIS::KeyCode key) override;
	void receiveKeyReleasedEvent(OIS::KeyCode key) override;
private:
	std::map<OIS::KeyCode, bool> m_keysPressed;
};
}  // namespace SSVEPMindShooter
