#pragma once

#include <Ogre.h>

#include "../ovamsCBasicPainter.h"

#define SSVEP_SHOOTER_TARGET_SIZE 0.15f

namespace SSVEPMindShooter {
class CImpactApplication;

class CImpactEnemyShip
{
public:
	static CImpactEnemyShip* createTarget(Ogre::Real position);
	static void initialize(CImpactApplication* application);
	~CImpactEnemyShip();

	void processFrame();
	void processHit(Ogre::Vector2 point);

	Ogre::Vector2 getEnemyPosition() const { return Ogre::Vector2(m_hitBox->getPosition().x, m_hitBox->getPosition().y); }
	bool isDestroyed() const { return m_shipDestroyed; }
	bool isTouchable() const { return m_destructionStatus < 10 && !m_EnemyLeaving; }
	int getPointValue() const { return m_pointValue; }
	Ogre::Real getWidth() const { return m_shipWidth; }
	int m_LeaveCountdown = 0;
	bool m_EnemyLeaving  = false;

private:

	struct SExplosionAnimation
	{
		Ogre::ParticleSystem* particleSystem;
		Ogre::SceneNode* node;
		int ttl;
	};

	static bool explostionExhausted(struct SExplosionAnimation* ea) { return ea->ttl < -25; }
	static void destroyAllAttachedMovableObjects(Ogre::SceneNode* sceneNode);

	static CImpactApplication* m_application;
	static Ogre::SceneNode* m_parentNode;
	static CBasicPainter* m_painter;

	Ogre::SceneNode* m_enemyNode     = nullptr;
	Ogre::SceneNode* m_hitBox        = nullptr;
	Ogre::Real m_incomingStatus      = 0;
	Ogre::Real m_shipWidth           = 0.1F;
	Ogre::uint32 m_destructionStatus = 0;
	bool m_shipDestroyed             = false;
	int m_pointValue                 = 100;

	explicit CImpactEnemyShip(Ogre::Real position);

	static int m_nCurrentExplosion;
	std::vector<struct SExplosionAnimation*> m_explosions;
};
}  // namespace SSVEPMindShooter
