/*
 * Notes:
 *
 * - This demo used to be called 'Impact Shooter' before being renamed to 'Mind Shooter'. The various 
 * mentions of 'Impact' are propagating from that time and should be cleaned up one day.
 *
 *
 */
#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovams_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <openvibe/ov_directories.h>

#include "ovamsCApplication.h"
#include "GenericStimulator/ovamsCGenericStimulatorApplication.h"
#include "Impact/ovamsCImpactApplication.h"

using namespace OpenViBE;

int main(const int argc, char** argv)
{
	if (argc != 3)
	{
		std::cout << "Usage : " << argv[0] << " <application-type> <scenario path>\n";
		exit(1);
	}

	// initialize the OpenViBE kernel

	CKernelLoader kernelLoader;
	CString error;
	Kernel::IKernelDesc* kernelDesc       = nullptr;
	Kernel::IKernelContext* kernelContext = nullptr;
	Kernel::ILogManager* logManager       = nullptr;

	const CString applicationString = CString(argv[1]);
	const CString scenarioFolder    = CString(argv[2]);

	CString applicationType, applicationSubtype;

	if (applicationString == CString("generic"))
	{
		applicationType    = "generic";
		applicationSubtype = "";
	}
	else if (applicationString == CString("impact-trainer"))
	{
		applicationType    = "impact";
		applicationSubtype = "trainer";
	}
	else if (applicationString == CString("impact-shooter"))
	{
		applicationType    = "impact";
		applicationSubtype = "shooter";
	}
	else
	{
		std::cout << "[ERROR] unknown command line parameter '" << applicationString << "'\n";
		exit(3);
	}

#ifdef TARGET_OS_Windows
	std::cout << "[  INF  ] Loading Windows kernel\n";
	if (!kernelLoader.load(Directories::getBinDir() + "/openvibe-kernel.dll", &error))
#else
	std::cout << "[  INF  ] Loading Linux kernel\n";
	if(!kernelLoader.load(Directories::getLibDir() + "/libopenvibe-kernel.so", &error))
#endif
	{
		std::cout << "[ FAILED ] Error loading kernel (" << error << ")" << "\n";
		exit(1);
	}
	std::cout << "[  INF  ] Kernel module loaded, trying to get kernel descriptor\n";

	kernelLoader.initialize();
	kernelLoader.getKernelDesc(kernelDesc);

	if (!kernelDesc) { std::cout << "[ FAILED ] No kernel descriptor\n"; }
	else
	{
		std::cout << "[  INF  ] Got kernel descriptor, trying to create kernel\n";

		kernelContext = kernelDesc->createKernel("ssvep-stimulator", Directories::getDataDir() + "/kernel/openvibe.conf");

		if (!kernelContext) { std::cout << "[ FAILED ] No kernel created by kernel descriptor\n"; }
		else
		{
			kernelContext->initialize();

			Toolkit::initialize(*kernelContext);

			Kernel::IConfigurationManager* configManager = &(kernelContext->getConfigurationManager());
			configManager->addConfigurationFromFile(configManager->expand("${Path_Data}/kernel/openvibe.conf"));
			logManager = &(kernelContext->getLogManager());

			// (*l_poLogManager) << Kernel::LogLevel_Info << configManager->expand("${UserHome}") << "\n";
			const CString configFile = scenarioFolder + "/appconf/application-configuration.conf";
			if (!configManager->addConfigurationFromFile(configFile))
			{
				(*logManager) << Kernel::LogLevel_Error << "Unable to read [" << configFile << "]\n";
				exit(3);
			}

			// This folder contains resources such as textures etc
			configManager->createConfigurationToken("SSVEP_MindShooterFolderName", "ssvep-mind-shooter");

			// This folder is the runtime scenario path
			configManager->createConfigurationToken("SSVEP_MindShooterScenarioPath", scenarioFolder);
		}
	}


	SSVEPMindShooter::CApplication* app;


	if (applicationType == CString("generic"))
	{
		(*logManager) << Kernel::LogLevel_Debug << "+ app = new SSVEPMindShooter::CGenericStimulatorApplication(...)\n";
		app = new SSVEPMindShooter::CGenericStimulatorApplication(scenarioFolder);
	}
	else if (applicationType == CString("impact"))
	{
		(*logManager) << Kernel::LogLevel_Debug << "+ app = new SSVEPMindShooter::CImpactApplication(...)\n";
		(*logManager) << Kernel::LogLevel_Info << "application subtype " << applicationSubtype << "\n";
		app = new SSVEPMindShooter::CImpactApplication(scenarioFolder, applicationSubtype);
	}
	else
	{
		(*logManager) << Kernel::LogLevel_Error << "Wrong application identifier specified\n";

		exit(1);
	}

	if (!app->setup(kernelContext))
	{
		(*logManager) << Kernel::LogLevel_Error << "Cannot proceed, exiting\n";

		exit(2);
	}

	app->go();


	(*logManager) << Kernel::LogLevel_Debug << "- app\n";
	delete app;

	return 0;
}


#else
#include <stdio.h>

int main(int argc, char** argv)
{
	std::cout << "SSVEP Mind Shooter has not been compiled as it depends on Ogre (missing/disabled)\n";

	return -1;
}
#endif
