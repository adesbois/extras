#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCCommandStimulatorControl.h"
#include "ovamsCApplication.h"

#include <toolkit/ovtk_stimulations.h>

using namespace SSVEPMindShooter;

CCommandStimulatorControl::CCommandStimulatorControl(CApplication* application)
	: ICommandVRPNButton(application, "SSVEP_VRPN_StimulatorControl") {}

void CCommandStimulatorControl::execute(const int button, const int /*state*/)
{
	// only run the commands once, skip

	switch (button)
	{
		case 0:
			m_application->startExperiment();
			m_application->m_StimulusSender->sendStimulation(OVTK_StimulationId_ExperimentStart);
			break;

		case 1:
			m_application->stopExperiment();
			m_application->m_StimulusSender->sendStimulation(OVTK_StimulationId_ExperimentStop);
			break;

		case 2:
			m_application->startFlickering();
			m_application->m_StimulusSender->sendStimulation(OVTK_StimulationId_VisualStimulationStart);
			break;

		case 3:
			m_application->stopFlickering();
			m_application->m_StimulusSender->sendStimulation(OVTK_StimulationId_VisualStimulationStop);
			break;

		default:
			m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "[ERROR] Unknown command\n";
			break;
	}
}

#endif
