#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

namespace SSVEPMindShooter {
class CSSVEPFlickeringObject
{
public:
	//SP			CSSVEPFlickeringObject( Ogre::SceneNode* objectNode, size_t LitFrames, size_t DarkFrames );
	CSSVEPFlickeringObject(Ogre::SceneNode* objectNode, uint64_t stimulationPattern);
	virtual ~CSSVEPFlickeringObject() { }

	virtual void setVisible(bool visibility);
	virtual void processFrame();

protected:
	Ogre::SceneNode* m_objectNode = nullptr;
	size_t m_currentFrame         = 0;
	//SP			size_t m_litFrames = 0;
	//SP			size_t m_darkFrames = 0;
	uint64_t m_stimulationPattern = 0;

private:

	bool m_visible = true;
	int m_iId      = 0;
};
}  // namespace SSVEPMindShooter
