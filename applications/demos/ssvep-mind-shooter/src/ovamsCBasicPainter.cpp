#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCBasicPainter.h"
#include "ovamsCApplication.h"

#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
#include "Overlay/OgreOverlaySystem.h"
#include "Overlay/OgreOverlayContainer.h"
#include "Overlay/OgreOverlayElement.h"
#endif

using namespace Ogre;
using namespace SSVEPMindShooter;
using namespace OpenViBE::Kernel;

CBasicPainter::CBasicPainter(CApplication* application)
	: m_application(application), m_sceneManager(application->getSceneManager())
{
	m_aabInf.setInfinite();

#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
	// on Ogre 1.9, overlay system needs to be manually created
	OverlaySystem* pOverlaySystem = OGRE_NEW OverlaySystem();
	m_sceneManager->addRenderQueueListener(pOverlaySystem);
#endif

	m_overlayManager = OverlayManager::getSingletonPtr();

	(m_application->getLogManager()) << LogLevel_Debug << "  + Creating OverlayManager\n";
	Overlay* overlay = m_overlayManager->create("TextOverlay");


	m_overlayContainer = dynamic_cast<OverlayContainer*>(m_overlayManager->createOverlayElement("Panel", "TextContainer"));
	m_overlayContainer->setDimensions(1, 1);
	m_overlayContainer->setPosition(0, 0);

	overlay->add2D(m_overlayContainer);
	overlay->show();
}


ManualObject* CBasicPainter::paintRectangle(const RealRect& oRectangle, const ColourValue color, const int plane) const
{
	ManualObject* object = m_sceneManager->createManualObject();
	object->begin("BasicSurface/Diffuse", RenderOperation::OT_TRIANGLE_FAN);
	object->setUseIdentityProjection(true);
	object->setUseIdentityView(true);

	object->position(oRectangle.right, oRectangle.top, 0.0);
	object->colour(color);
	object->index(0);

	object->position(oRectangle.left, oRectangle.top, 0.0);
	object->colour(color);
	object->index(1);

	object->position(oRectangle.left, oRectangle.bottom, 0.0);
	object->colour(color);
	object->index(2);

	object->position(oRectangle.right, oRectangle.bottom, 0.0);
	object->colour(color);
	object->index(3);

	object->index(0);

	object->end();

	object->setBoundingBox(m_aabInf);
	object->setRenderQueueGroup(RENDER_QUEUE_OVERLAY - plane);

	object->setVisible(true);

	return object;
}

ManualObject* CBasicPainter::paintTexturedRectangle(const RealRect& oRectangle, const String& sSurface, const int plane) const
{
	ManualObject* object = m_sceneManager->createManualObject();
	object->begin(sSurface, RenderOperation::OT_TRIANGLE_FAN);
	object->setUseIdentityProjection(true);
	object->setUseIdentityView(true);

	object->position(oRectangle.right, oRectangle.top, 0.0);
	object->textureCoord(0, 0);
	object->index(0);

	object->position(oRectangle.left, oRectangle.top, 0.0);
	object->textureCoord(1, 0);
	object->index(1);

	object->position(oRectangle.left, oRectangle.bottom, 0.0);
	object->textureCoord(1, 1);
	object->index(2);

	object->position(oRectangle.right, oRectangle.bottom, 0.0);
	object->textureCoord(0, 1);
	object->index(3);

	object->index(0);

	object->end();

	object->setBoundingBox(m_aabInf);
	object->setRenderQueueGroup(RENDER_QUEUE_OVERLAY - plane);

	object->setVisible(true);

	return object;
}


ManualObject* CBasicPainter::paintTriangle(const Vector2 p1, const Vector2 p2, const Vector2 p3, const ColourValue color, const int plane) const
{
	ManualObject* object = m_sceneManager->createManualObject();
	object->begin("BasicSurface/Diffuse", RenderOperation::OT_TRIANGLE_FAN);
	object->setUseIdentityProjection(true);
	object->setUseIdentityView(true);

	object->position(p1.x, p1.y, 0.0);
	object->colour(color);
	object->index(0);

	object->position(p2.x, p2.y, 0.0);
	object->colour(color);
	object->index(1);

	object->position(p3.x, p3.y, 0.0);
	object->colour(color);
	object->index(2);

	object->index(0);

	object->end();

	object->setBoundingBox(m_aabInf);
	object->setRenderQueueGroup(RENDER_QUEUE_OVERLAY - plane);

	object->setVisible(true);

	return object;
}

ManualObject* CBasicPainter::paintTriangle(const Vector3 p1, const Vector3 p2, const Vector3 p3, const ColourValue color, const int plane) const
{
	ManualObject* object = m_sceneManager->createManualObject();
	object->begin("BasicSurface/Diffuse", RenderOperation::OT_TRIANGLE_FAN);
	object->setUseIdentityProjection(true);
	object->setUseIdentityView(true);

	object->position(p1.x, p1.y, p1.z);
	object->colour(color);
	object->index(0);

	object->position(p2.x, p2.y, p2.z);
	object->colour(color);
	object->index(1);

	object->position(p3.x, p3.y, p3.z);
	object->colour(color);
	object->index(2);

	object->index(0);

	object->end();

	object->setBoundingBox(m_aabInf);
	object->setRenderQueueGroup(RENDER_QUEUE_OVERLAY - plane);

	object->setVisible(true);

	return object;
}

ManualObject* CBasicPainter::paintCircle(const Real rX, const Real rY, const Real rR, const ColourValue color, const bool bFilled, const int plane) const
{
	ManualObject* object = m_sceneManager->createManualObject();

	if (bFilled) { object->begin("BasicSurface/Diffuse", RenderOperation::OT_TRIANGLE_FAN); }
	else { object->begin("BasicSurface/Diffuse", RenderOperation::OT_LINE_STRIP); }

	object->setUseIdentityProjection(true);
	object->setUseIdentityView(true);

	float const fAccuracy = 16;
	unsigned uiIndex      = 0;

	for (float theta = 0; theta <= 2 * Math::PI; theta += Math::PI / fAccuracy)
	{
		object->position(rX + rR * cos(theta), rY + rR * sin(theta), 0);
		object->colour(color);
		object->index(uiIndex++);
	}

	object->index(0);
	object->end();
	object->setBoundingBox(m_aabInf);
	object->setRenderQueueGroup(RENDER_QUEUE_OVERLAY - plane);
	object->setVisible(true);

	return object;
}

ManualObject* CBasicPainter::beginPainingPolygon(bool /*filled*/, const String& material) const
{
	ManualObject* object = m_sceneManager->createManualObject();
	object->begin(material, RenderOperation::OT_TRIANGLE_FAN);
	object->setUseIdentityProjection(true);
	object->setUseIdentityView(true);

	return object;
}

void CBasicPainter::addPointToPolygon(ManualObject* pPolygon, const Real rX, const Real rY, const ColourValue color)
{
	pPolygon->position(rX, rY, 0.0);
	pPolygon->colour(color);
}

void CBasicPainter::finishPaintingPolygon(ManualObject* pPolygon, const int plane) const
{
	pPolygon->end();
	pPolygon->setBoundingBox(m_aabInf);
	pPolygon->setRenderQueueGroup(RENDER_QUEUE_OVERLAY - plane);
	pPolygon->setVisible(true);
}


void CBasicPainter::paintText(const std::string& sID, const std::string& sText, const Real rX, const Real rY, const Real rWidth, const Real rHeight,
							  const ColourValue& color) const
{
	OverlayElement* text = m_overlayManager->createOverlayElement("TextArea", sID);

	text->setDimensions(rWidth, rHeight);
	text->setMetricsMode(GMM_PIXELS);
	text->setPosition(rX, rY);
	text->setParameter("font_name", "DejaVu");
	text->setColour(color);
	text->setCaption(sText);

	m_overlayContainer->addChild(text);
}


#endif
