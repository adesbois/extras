#pragma once

#include "ovamsICommand.h"
#include <array>

class vrpn_Analog_Remote;

namespace SSVEPMindShooter {
class ICommandVRPNAnalog : public ICommand
{
public:
	ICommandVRPNAnalog(CApplication* application, const OpenViBE::CString& name);
	~ICommandVRPNAnalog() override;

	void processFrame() override;
	virtual void execute(int channelCount, double* channel) = 0;

protected:
	vrpn_Analog_Remote* m_vrpnAnalog = nullptr;
};
}  // namespace SSVEPMindShooter
