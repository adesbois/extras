#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsICommandVRPNAnalog.h"
#include "ovamsCApplication.h"

#include <vrpn_Connection.h>
#include <vrpn_Analog.h>

using namespace SSVEPMindShooter;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;


namespace {
void VRPN_CALLBACK ssvep_vrpn_callback_analog(void* command, vrpn_ANALOGCB analog)
{
	static_cast<ICommandVRPNAnalog*>(command)->execute(analog.num_channel, analog.channel);
}
}


ICommandVRPNAnalog::ICommandVRPNAnalog(CApplication* application, const CString& name) : ICommand(application)
{
	IConfigurationManager* configManager = application->getConfigurationManager();

	const std::string analogName = std::string(name.toASCIIString()) + "@" + std::string((configManager->expand("${SSVEP_VRPNHost}")).toASCIIString());
	m_application->getLogManager() << LogLevel_Debug << "+ m_vrpnAnalog = new vrpn_Analog_Remote(" << analogName << ")\n";


	m_vrpnAnalog = new vrpn_Analog_Remote(analogName.c_str());
	m_vrpnAnalog->register_change_handler(static_cast<void*>(this), ssvep_vrpn_callback_analog);
}

ICommandVRPNAnalog::~ICommandVRPNAnalog()
{
	m_application->getLogManager() << LogLevel_Debug << "- delete m_vrpnAnalog\n";
	delete m_vrpnAnalog;
}

void ICommandVRPNAnalog::processFrame() { m_vrpnAnalog->mainloop(); }

#endif
