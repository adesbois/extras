#if defined(TARGET_HAS_ThirdPartyOgre3DTerrain)

#include "ovamsCSSVEPFlickeringObject.h"

using namespace SSVEPMindShooter;

// SPCSSVEPFlickeringObject::CSSVEPFlickeringObject(Ogre::SceneNode* objectNode, size_t LitFrames, size_t DarkFrames) :
CSSVEPFlickeringObject::CSSVEPFlickeringObject(Ogre::SceneNode* objectNode, const uint64_t stimulationPattern)
	: m_objectNode(objectNode), m_stimulationPattern(stimulationPattern)
{
	static int nextid = 0;

	m_iId = ++nextid;
}

void CSSVEPFlickeringObject::setVisible(const bool visibility)
{
	if ((!m_visible && visibility) || (m_visible && !visibility)) { m_objectNode->flipVisibility(); }
	m_visible = visibility;
}

void CSSVEPFlickeringObject::processFrame()
{
	// If the bit for the frame is '1', set to visible
	if (((m_stimulationPattern >> m_currentFrame) % 2) == 1) { this->setVisible(true); }
	else { this->setVisible(false); }

	m_currentFrame++;

	// Start over when only '1' is left
	if (m_stimulationPattern >> m_currentFrame == 1) { m_currentFrame = 0; }
}

#endif
