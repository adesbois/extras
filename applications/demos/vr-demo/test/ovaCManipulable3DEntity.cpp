#include "ovaCManipulable3DEntity.h"

REGISTER_OBJECT_FACTORY(CManipulable3DEntity, "ovaCManipulable3DEntity");

CManipulable3DEntity::CManipulable3DEntity(OMK::Controller& controller, const OMK::ObjectDescriptor& objectDesc) : OMK::SimulatedObject(controller, objectDesc) { }

CManipulable3DEntity::~CManipulable3DEntity() { }

/*
	,m_rPositionOutput(addOutput < OMK::Translation > ("position", new OMK::Polator < OMK::Translation > ()))
	,m_rOrientationOutput(addOutput < OMK::HPRRotation > ("orientation", new OMK::Polator < OMK::HPRRotation > ()))
	,m_rScaleOutput(addOutput < OMK::Scale > ("scale", new OMK::Polator < OMK::Scale > ()))
	,m_isGoal(false)
{
	visualiseOutput < OMK::vTranslationInputHandler >(m_rPositionOutput, "DCS_position") ;
	visualiseOutput < OMK::vHPRRotationInputHandler >(m_rOrientationOutput, "DCS_orientation") ;
	visualiseOutput < OMK::vScaleInputHandler >(m_rScaleOutput, "DCS_scale") ;
}
*/

void CManipulable3DEntity::init() { }
//void CManipulable3DEntity::init() { m_rTransformOutput.set(m_oTransform); }

#define duration 1250
#define fadein 500
#define fadeout 500

void CManipulable3DEntity::compute()
{
	if (m_isGoal)
	{
		int delta = getSimulatedDate() - m_iGoalDate;
		if (delta < duration)
		{
			if (delta < fadein)
			{
				double coef = (fadein - delta) / (double)fadein; // 1-0
				double rotation = 180 * pow(coef, 3);
				m_orientation[0] = 0;
				m_orientation[1] = 3 * rotation;
				m_orientation[2] = rotation * sin(M_PI * pow(coef, 3) * 1);

				m_position[0] = 0;
				m_position[1] = 5 + 7 * sin(coef * M_PI / 2);
				m_position[2] = 0;
			}

			if (delta > duration - fadeout)
			{
				double coef = (delta - duration + fadeout) / double(fadeout); // 0-1
				double rotation = 3 * 180 * pow(coef, 4);
				m_orientation[0] = rotation;
				m_orientation[1] = 3 * rotation * sin(M_PI * pow(coef, 3) * 2);
				m_orientation[2] = 0;

				m_position[0] = 0;
				m_position[1] = 5 + 7 * coef * sin(coef * M_PI / 2);
				m_position[2] = 0;
			}
			if (delta >= fadein && delta <= duration - fadeout)
			{
				m_orientation[0] = 0;
				m_orientation[1] = 0;
				m_orientation[2] = 0;

				m_position[0] = 0;
				m_position[1] = 5;
				m_position[2] = 0;
			}
		}
	}
	/*
		m_rTransformOutput.set(m_oTransform);
	*/
}

bool CManipulable3DEntity::processEvent(OMK::Event* pEvent)
{
	if (pEvent->eventId == g_sManipulate3DEntity_Goal)
	{
		m_isGoal = true;
		m_iGoalDate = getSimulatedDate();
	}

	if (pEvent->eventId == g_sManipulate3DEntity_Reset)
	{
		m_position[0] = 0;
		m_position[1] = 0.2;
		m_position[2] = 0;

		m_orientation[0] = 0;
		m_orientation[1] = 0;
		m_orientation[2] = 0;

#if defined _DEBUG_
		std::cout << "### reseted\n";
#endif
		return true;
	}

	if (pEvent->eventId == g_sManipulate3DEntity_Translate)
	{
		PositionEvent* event = dynamic_cast <PositionEvent*>(pEvent);
		if (event)
		{
			const Position& value = event->value;
			m_position[0] += value[0];
			m_position[1] += value[1];
			m_position[2] += value[2];

#if defined _DEBUG_
			std::cout << "### translated\n";
#endif
			return true;
		}
	}

	if (pEvent->eventId == g_sManipulate3DEntity_SetPosition)
	{
		PositionEvent* event = dynamic_cast <PositionEvent*>(pEvent);
		if (event)
		{
			const Position& value = event->value;
			m_position[0] = value[0];
			m_position[1] = value[1];
			m_position[2] = value[2];

#if defined _DEBUG_
			std::cout << "### moved\n";
#endif
			return true;
		}
	}

	if (pEvent->eventId == g_sManipulate3DEntity_Scale)
	{
		ScaleEvent* event = dynamic_cast <ScaleEvent*>(pEvent);
		if (event)
		{
			const Scale& value = event->value;
			m_oScale[0] *= value[0];
			m_oScale[1] *= value[1];
			m_oScale[2] *= value[2];

#if defined _DEBUG_
			std::cout << "### scaled\n";
#endif
			return true;
		}
	}

	if (pEvent->eventId == g_sManipulate3DEntity_SetScale)
	{
		ScaleEvent* event = dynamic_cast <ScaleEvent*>(pEvent);
		if (event)
		{
			const Scale& value = event->value;
			m_oScale[0] = value[0];
			m_oScale[1] = value[1];
			m_oScale[2] = value[2];

#if defined _DEBUG_
			std::cout << "### scaled fixed\n";
#endif
			return true;
		}
	}

	if (pEvent->eventId == g_sManipulate3DEntity_Rotate)
	{
		OrientationEvent* event = dynamic_cast <OrientationEvent*>(pEvent);
		if (event)
		{
			const Orientation& value = event->value;
			m_orientation[0] += value[0];
			m_orientation[1] += value[1];
			m_orientation[2] += value[2];

#if defined _DEBUG_
			std::cout << "### rotated\n";
#endif
			return true;
		}
	}

	if (pEvent->eventId == g_sManipulate3DEntity_SetOrientation)
	{
		OrientationEvent* event = dynamic_cast <OrientationEvent*>(pEvent);
		if (event)
		{
			const Orientation& value = event->value;
			m_orientation[0] = value[0];
			m_orientation[1] = value[1];
			m_orientation[2] = value[2];

#if defined _DEBUG_
			std::cout << "### orirentation set\n";
#endif
			return true;
		}
	}

	return false;
}
