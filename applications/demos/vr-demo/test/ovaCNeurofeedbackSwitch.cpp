#include "ovaCNeurofeedbackSwitch.h"

REGISTER_OBJECT_FACTORY(CNeurofeedbackSwitch, "ovaCNeurofeedbackSwitch");

CNeurofeedbackSwitch::CNeurofeedbackSwitch(OMK::Controller& controller, const OMK::ObjectDescriptor& objectDesc)
	:OMK::SimulatedObject(controller, objectDesc)
{
}

CNeurofeedbackSwitch::~CNeurofeedbackSwitch()
{
}

void CNeurofeedbackSwitch::init()
{
	_FarAway_[0] = 1000;
	_FarAway_[1] = 1000;
	_FarAway_[2] = 1000;

	_Position_[0] = 0;
	_Position_[1] = 0;
	_Position_[2] = 0;

	sendValuedEvent("OpenViBE_ball", g_sManipulate3DEntity_SetPosition, PositionType(_FarAway_));
	sendValuedEvent("OpenViBE_passive_ball", g_sManipulate3DEntity_SetPosition, PositionType(_FarAway_));

	registerForSignal(g_sVrpnButtonStateUpdate);
}

void CNeurofeedbackSwitch::compute()
{
}

bool CNeurofeedbackSwitch::processEvent(OMK::Event* pEvent)
{
	if (pEvent->eventId == g_sVrpnButtonStateUpdate)
	{
		VrpnButtonStateEvent* event = dynamic_cast <VrpnButtonStateEvent*>(pEvent);
		if (event)
		{
			const VrpnButtonState& vrpnButtonState = event->value;
			if (vrpnButtonState.first<int(sizeof(m_button) / sizeof(m_button[0])))
			{
				m_button[vrpnButtonState.first] = (vrpnButtonState.second ? true : false);

#if _DEBUG_
				std::cout << "got event " << *event << std::endl;
#endif
				sendValuedEvent("OpenViBE_ball", g_sManipulate3DEntity_SetPosition, PositionType(m_button[2] ? _Position_ : _FarAway_));
				sendValuedEvent("OpenViBE_passive_ball", g_sManipulate3DEntity_SetPosition, PositionType(m_button[3] ? _Position_ : _FarAway_));
			}
		}
		return true;
	}
	return false;
}

