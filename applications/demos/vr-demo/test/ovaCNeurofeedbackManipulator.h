#pragma once

#include "ova_defines.h"

class CNeurofeedbackManipulator : public OMK::SimulatedObject
{
public:

	DECLARE_OBJECT_FACTORY(CNeurofeedbackManipulator);

	virtual void init();
	virtual void compute();
	virtual bool processEvent(OMK::Event* pEvent);

	Position m_position;
	Orientation m_orientation;
	VrpnAnalogState m_Analogs;
	std::list < OMK::Name > m_vAnalogName;
	bool m_bFirstTime = false;
	double m_dLowestAnalogEver = 0;
	double m_dHighestAnalogEver = 0;
	double m_dTotal = 0;
	double m_dCount = 0;
	int m_iLastPrint = 0;

	double _AnalogRotation_ = 0;
	double _AnalogScale_ = 0;
	double _AnalogSoften_ = 0;
	double _AnalogFall_ = 0;
};


