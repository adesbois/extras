#include "ovaCNeurofeedbackScoreCounter.h"

REGISTER_OBJECT_FACTORY(CNeurofeedbackScoreCounter, "ovaCNeurofeedbackScoreCounter");

CNeurofeedbackScoreCounter::CNeurofeedbackScoreCounter(OMK::Controller& controller, const OMK::ObjectDescriptor& objectDesc)
	:OMK::SimulatedObject(controller, objectDesc)
{
}

CNeurofeedbackScoreCounter::~CNeurofeedbackScoreCounter()
{
}

void CNeurofeedbackScoreCounter::init()
{
	registerForSignal(g_sVrpnButtonStateUpdate);
	registerForSignal(g_sVrpnAnalogStateUpdate);

	m_shouldCare = false;
	m_wasZero = false;
	m_score = 0;

	m_oScale[0] = 1;
	m_oScale[1] = 1;
	m_oScale[2] = 1;

	m_oScaleTarget[0] = 1;
	m_oScaleTarget[1] = 1;
	m_oScaleTarget[2] = 1;

	sendValuedEvent("OpenViBE_score_counter", g_sManipulate3DEntity_SetScale, ScaleType(m_oScale));
}

void CNeurofeedbackScoreCounter::compute()
{
	double speed = .01;
	m_oScale = (float)speed * m_oScaleTarget + float(1 - speed) * m_oScale;

	sendValuedEvent("OpenViBE_score_counter", g_sManipulate3DEntity_SetScale, ScaleType(m_oScale));
}

bool CNeurofeedbackScoreCounter::processEvent(OMK::Event* pEvent)
{
	if (pEvent->eventId == g_sVrpnButtonStateUpdate)
	{
		VrpnButtonStateEvent* event = dynamic_cast <VrpnButtonStateEvent*>(pEvent);
		if (event)
		{
			const VrpnButtonState& vrpnButtonState = event->value;
			if (vrpnButtonState.first<int(sizeof(m_button) / sizeof(m_button[0])))
			{
				m_button[vrpnButtonState.first] = (vrpnButtonState.second ? true : false);
				if (vrpnButtonState.first == 3)
				{
					if (m_button[vrpnButtonState.first])
					{
						m_shouldCare = true;
#ifdef _DEBUG_
						std::cout << "Should care\n";
#endif
					}
					else
					{
						m_shouldCare = false;
#ifdef _DEBUG_
						std::cout << "Should not care\n";
#endif
					}
				}
			}
		}
		return true;
	}

	if (pEvent->eventId == g_sVrpnAnalogStateUpdate)
	{
		VrpnAnalogStateEvent* event = dynamic_cast <VrpnAnalogStateEvent*>(pEvent);
		if (event)
		{
			const VrpnAnalogState& vrpnAnalogState = event->value;
			auto it = vrpnAnalogState.begin();
			if (m_wasZero)
			{
				if (*it >= 1E-3)
				{
					if (m_shouldCare)
					{
						m_oScaleTarget[1] += .2;
						m_score += 1;
						m_shouldCare = false;
						std::cout << "NeW SCoRe : " << m_score << "\n";
					}

					m_wasZero = false;

				}
			}
			else
			{
				if (*it < 1E-3)
				{
					m_wasZero = true;
				}
			}
		}
	}

	return false;
}
