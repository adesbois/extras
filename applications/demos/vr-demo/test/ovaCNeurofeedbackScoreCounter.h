#pragma once

#include "ova_defines.h"

#include <OMKTransform.h>

class CNeurofeedbackScoreCounter : public OMK::SimulatedObject
{
public:

	DECLARE_OBJECT_FACTORY(CNeurofeedbackScoreCounter);

	virtual void init();
	virtual void compute();
	virtual bool processEvent(OMK::Event* pEvent);

	Scale m_oScale;
	Scale m_oScaleTarget;
	bool m_button[1024];
	bool m_shouldCare = false;
	bool m_wasZero = false;
	size_t m_score = 0;
};


