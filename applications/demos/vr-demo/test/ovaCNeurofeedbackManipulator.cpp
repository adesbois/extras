#include "ovaCNeurofeedbackManipulator.h"

#include <OMKMultipleConfigurationParameter.h>
#include <OMKUniqueConfigurationParameter.h>

REGISTER_OBJECT_FACTORY(CNeurofeedbackManipulator, "ovaCNeurofeedbackManipulator");

CNeurofeedbackManipulator::CNeurofeedbackManipulator(OMK::Controller& controller, const OMK::ObjectDescriptor& objectDesc) :OMK::SimulatedObject(controller, objectDesc) { }

CNeurofeedbackManipulator::~CNeurofeedbackManipulator() { }

void CNeurofeedbackManipulator::init()
{
	static OMK::Name g_sVrpnAnalogStateUpdate("vrpn_analog state update");

	_AnalogRotation_ = 0.25;
	_AnalogScale_ = 0.004;
	_AnalogSoften_ = 1;
	_AnalogFall_ = .99;

	m_dLowestAnalogEver = 1E10;
	m_dHighestAnalogEver = -1E10;
	m_iLastPrint = -1;

	registerForSignal(g_sVrpnAnalogStateUpdate);

	OMK::ParametersAccessor::get < double >(getConfigurationParameters(), "_AnalogRotation_", _AnalogRotation_);
	OMK::ParametersAccessor::get < double >(getConfigurationParameters(), "_AnalogScale_", _AnalogScale_);
	OMK::ParametersAccessor::get < double >(getConfigurationParameters(), "_AnalogSoften_", _AnalogSoften_);
	OMK::ParametersAccessor::get < double >(getConfigurationParameters(), "_AnalogFall_", _AnalogFall_);

	m_bFirstTime = true;

	m_position[0] = 4;
	m_position[1] = 0;
	m_position[2] = -3;

	m_orientation[0] = 0;
	m_orientation[1] = 0;
	m_orientation[2] = 0;

	m_dTotal = 0;
	m_dCount = 0;
}

void CNeurofeedbackManipulator::compute()
{
	auto itAnalog = m_Analogs.begin();
	auto itAnalogName = m_vAnalogName.begin();
	for (; itAnalog != m_Analogs.end(); ++itAnalog, ++itAnalogName)
	{
		double analog = (*itAnalog);
		m_dTotal += analog;
		m_dCount += 1.;

		if (analog > m_dHighestAnalogEver) m_dHighestAnalogEver = analog;
		if (analog < m_dLowestAnalogEver)  m_dLowestAnalogEver = analog;
		if (m_iLastPrint != getSimulatedDate() / 1000)
		{
			m_iLastPrint = getSimulatedDate() / 1000;

#if _DEBUG_
			std::cout << "Current session : mean/min/max = "
				<< (m_dTotal / m_dCount) << "/"
				<< m_dLowestAnalogEver << "/"
				<< m_dHighestAnalogEver << "/"
				<< "\n";

			std::cout << "\n";
#endif

			m_dTotal = 0;
			m_dCount = 0;
			m_dLowestAnalogEver = 1E10;
			m_dHighestAnalogEver = -1E10;
		}

		if (analog <= 0)
		{
			m_orientation[0] *= _AnalogFall_;
			m_orientation[1] *= _AnalogFall_;
			m_orientation[2] *= _AnalogFall_;

			m_position[1] *= _AnalogFall_;
		}
		else
		{
			m_orientation[0] += _AnalogRotation_ * ((rand() & 1) == 0 ? -1 : 1);
			m_orientation[1] += _AnalogRotation_ * ((rand() & 1) == 0 ? -1 : 1);
			m_orientation[2] += _AnalogRotation_ * ((rand() & 1) == 0 ? -1 : 1);

			m_position[1] += analog * _AnalogScale_;

			if (m_position[1] > 6) { m_position[1] = 6; }
		}

		if (m_orientation[0] > 5) m_orientation[0] = 5;
		if (m_orientation[1] > 5) m_orientation[1] = 5;
		if (m_orientation[2] > 5) m_orientation[2] = 5;

		if (m_orientation[0] < -5) m_orientation[0] = -5;
		if (m_orientation[1] < -5) m_orientation[1] = -5;
		if (m_orientation[2] < -5) m_orientation[2] = -5;

		sendValuedEvent(*itAnalogName, g_sManipulate3DEntity_SetOrientation, OrientationType(m_orientation));
		sendValuedEvent(*itAnalogName, g_sManipulate3DEntity_SetPosition, PositionType(m_position));
	}
}

bool CNeurofeedbackManipulator::processEvent(OMK::Event* pEvent)
{
	if (pEvent->eventId == g_sVrpnAnalogStateUpdate)
	{
		VrpnAnalogStateEvent* event = dynamic_cast <VrpnAnalogStateEvent*>(pEvent);
		if (event)
		{
			VrpnAnalogState analogs = event->value;

			if (m_bFirstTime)
			{
				// create objects
				size_t i = 0;
				for (auto it1 = analogs.begin(); it1 != analogs.end(); ++it1)
				{
					const char* name = ("nf object " + std::to_string(i++)).c_str();
					m_vAnalogName.push_back(name);

					// OpenMASK won't let me delete those configuration parameters,
					// However, way to go :o)
					OMK::MultipleConfigurationParameter* userParams = new OMK::MultipleConfigurationParameter();
					userParams->appendSubDescriptorNamed("geometryFileName", new OMK::UniqueConfigurationParameter("./Data/neurofeedback.wrl"));

					getController().createObject(OMK::ObjectDescriptor(name, "ovManipulable3DEntity", getController().computeAdequateFrequency(75), userParams), getFathersDescriptor().getName());

					m_Analogs.push_back(0);
				}
				m_bFirstTime = false;
			}

			std::cout << "got data\n";

			for (auto it1 = analogs.begin(), it2 = m_Analogs.begin(); it1 != analogs.end() && it2 != m_Analogs.end(); ++it1, ++it2)
			{
				*it2 = *it1 * _AnalogSoften_ + *it2 * (1 - _AnalogSoften_);
			}

			return true;
		}
	}

	return false;
}
