#include "ovaC3DEntityManipulator.h"

REGISTER_OBJECT_FACTORY(C3DEntityManipulator, "ovaC3DEntityManipulator");

C3DEntityManipulator::C3DEntityManipulator(OMK::Controller& controller, const OMK::ObjectDescriptor& objectDesc) : OMK::SimulatedObject(controller, objectDesc) { }

C3DEntityManipulator::~C3DEntityManipulator() { }

void C3DEntityManipulator::init()
{
	registerForSignal(g_sVrpnButtonStateUpdate);
	registerForSignal(g_sVrpnAnalogStateUpdate);

	m_ballPosition[0] = 1000000;
	m_ballPosition[1] = 1000000;
	m_ballPosition[2] = 1000000;

	m_button[0] = false;
	m_button[1] = false;
	m_button[2] = false;
}

void C3DEntityManipulator::compute()
{
	if (m_button[0] && m_Analogs.size())
	{
		while (m_Analogs.size())
		{
			double value = m_Analogs.front() * 0.002;

			m_ballSpeed[0] = 0;
			m_ballSpeed[1] = 0;
			m_ballSpeed[2] = value;

			m_ballPosition[0] += m_ballSpeed[0];
			m_ballPosition[1] += m_ballSpeed[1];
			m_ballPosition[2] += m_ballSpeed[2];

			m_Analogs.pop_front();
		}
	}
	else
	{
		m_ballPosition[0] += m_ballSpeed[0];
		m_ballPosition[1] += m_ballSpeed[1];
		m_ballPosition[2] += m_ballSpeed[2];
	}

	const double viscosity = 0.010;
	m_ballSpeed[0] *= (1 - viscosity);
	m_ballSpeed[1] *= (1 - viscosity);
	m_ballSpeed[2] *= (1 - viscosity);

	Position far;
	far[0] = 1000000;
	far[1] = 1000000;
	far[2] = 1000000;
	sendValuedEvent((m_button[1] || m_button[2]) ? "OpenViBE_ball" : "OpenViBE_passive_ball", g_sManipulate3DEntity_SetPosition, PositionType(m_ballPosition));
	sendValuedEvent((m_button[1] || m_button[2]) ? "OpenViBE_passive_ball" : "OpenViBE_ball", g_sManipulate3DEntity_SetPosition, PositionType(far));

	Orientation orientation;

	orientation[0] = 0;
	orientation[1] = (m_ballPosition[2] * 180.0 / M_PI) / 0.5;
	orientation[2] = 0;

	sendValuedEvent("OpenViBE_passive_ball", g_sManipulate3DEntity_SetOrientation, OrientationType(orientation));
	sendValuedEvent("OpenViBE_ball", g_sManipulate3DEntity_SetOrientation, OrientationType(orientation));

	if (m_ballPosition[2] < -7 || m_ballPosition[2]>7)
	{
		sendEvent(getName(), g_s3DEntityManipulator_Reset);
		sendEvent("OpenViBE_goal_display", g_sManipulate3DEntity_Goal);
	}
}

bool C3DEntityManipulator::processEvent(OMK::Event* pEvent)
{
	if (pEvent->eventId == g_s3DEntityManipulator_Reset)
	{
		m_ballPosition[0] = 0;
		m_ballPosition[1] = 0.5;
		m_ballPosition[2] = 0;

		m_ballSpeed[0] = 0;
		m_ballSpeed[1] = 0;
		m_ballSpeed[2] = 0;

		return true;
	}

	if (pEvent->eventId == g_sVrpnButtonStateUpdate)
	{
		VrpnButtonStateEvent* event = dynamic_cast <VrpnButtonStateEvent*>(pEvent);
		if (event)
		{
			const VrpnButtonState& vrpnButtonState = event->value;
			if (vrpnButtonState.first<int(sizeof(m_button) / sizeof(m_button[0])))
			{
				m_button[vrpnButtonState.first] = (vrpnButtonState.second ? true : false);
			}
		}
		return true;
	}

	if (pEvent->eventId == g_sVrpnAnalogStateUpdate)
	{
		VrpnAnalogStateEvent* event = dynamic_cast <VrpnAnalogStateEvent*>(pEvent);
		if (event)
		{
			const VrpnAnalogState& vrpnAnalogState = event->value;
			m_AnalogsCache.push_back(vrpnAnalogState.front());
			if (m_AnalogsCache.size() > 10) { m_AnalogsCache.pop_front(); }

			if (m_button[0])
			{
				double analog = 0;
				for (auto it = m_AnalogsCache.begin(); it != m_AnalogsCache.end(); ++it) { analog += *it; }
				m_Analogs.push_back(analog / m_AnalogsCache.size());
			}
		}
		return true;
	}

	return false;
}
