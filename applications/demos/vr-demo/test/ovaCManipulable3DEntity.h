#pragma once

#include "ova_defines.h"

class CManipulable3DEntity : public OMK::SimulatedObject
{
public:

	DECLARE_OBJECT_FACTORY(CManipulable3DEntity);

	virtual void init();
	virtual void compute();
	virtual bool processEvent(OMK::Event* pEvent);

	Position m_position;
	Orientation m_orientation;
	Scale m_oScale;

	/*
		OMK::Output < OMK::Type::Transform >& m_rTransformOutput;
	*/

	bool m_isGoal = false;
	int m_iGoalDat = 0;
};


