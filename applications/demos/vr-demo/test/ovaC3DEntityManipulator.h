#pragma once

#include "ova_defines.h"

class C3DEntityManipulator : public OMK::SimulatedObject
{
public:

	DECLARE_OBJECT_FACTORY(C3DEntityManipulator);

	virtual void init();
	virtual void compute();
	virtual bool processEvent(OMK::Event* pEvent);

	bool m_button[3];
	std::list<double> m_AnalogsCache;
	std::list<double> m_Analogs;
	Wm4::Vector3f m_ballPosition;
	Wm4::Vector3f m_ballSpeed;
};


