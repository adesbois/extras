#pragma once

#include "ova_defines.h"

#include <OMKOutput.h>
#include <OMKTransform.h>

class CNeurofeedbackSwitch : public OMK::SimulatedObject
{
public:

	DECLARE_OBJECT_FACTORY(CNeurofeedbackSwitch);

	virtual void init();
	virtual void compute();
	virtual bool processEvent(OMK::Event* pEvent);

	bool m_button[1024];
	Position _Position_;
	Position _FarAway_;
};


