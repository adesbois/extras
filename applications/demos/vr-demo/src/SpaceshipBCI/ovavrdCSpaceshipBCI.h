#pragma once

#include "../ovavrdCOgreVRApplication.h"

namespace VRDemos {

/**
 * \class CSpaceshipBCI
 * \author Laurent Bonnet (INRIA/IRISA)
 * \date 2010-02-16
 * \brief Ogre application for the application "Use-The-Force".
 *
 * \details The SpaceshipBCI application is based on motor imagery of the feet. 
 * The 3D scene is a hangar, with few barrels stored. 
 * A spaceship is parked on the center of the screen.
 * The subject has to move his feet to lift the spaceship.
 * The feedback is computed according to the bounce of beta detected in the EEG, after the movement.
 * 
 * Each trial has 3 steps:
 * - Rest: the subject has to stand still.
 * - Move: the subject moves his feet.
 * - NoMove: the subject stops.
 * The subject scores if the spaceship is lifted in the NoMove phase.
 */
class CSpaceshipBCI final : public COgreVRApplication
{
public:

	/**
	* \brief constructor.
	*/
	explicit CSpaceshipBCI(const std::string& localization = "eng");

private:

	/**
	* \brief Phases enumeration.
	*/
	enum
	{
		Phase_Rest,   ///< The subject stands still.
		Phase_Move,   ///< The subject moves his feet.
		Phase_NoMove, ///< The subject stops the movement.
	};

	/**
	* \brief Stage enumeration.
	*/
	enum
	{
		Stage_Baseline,          ///< The subject stands still.
		Stage_FreetimeReal,      ///< The subject iterates 10 times with real movements
		Stage_FreetimeImaginary, ///< The subject iterates 10 times with imaginary movements
		Stage_Statistics,        ///< Experiment is over, uninitialize() is called.
	};

	/**
	* \brief Initializes the scene, camera, lights and GUI.
	* \return \em true if the scene is successfully set up.
	*/
	bool initialise() override;

	//----- SCENE COMPONENTS -----//
	/**
	* \brief Loads the GUI.
	*/
	void loadGUI();
	/**
	* \brief Loads the hangar.
	*/
	void loadHangar() const;
	/**
	* \brief Populates the hangar with fixed barrels.
	*/
	void loadHangarBarrels() const;

	/**
	* \brief Loads the movable spaceship.
	*/
	void loadShip();
	/**
	* \brief Loads the movable barrels in front of the ship.
	*/
	void loadBarrels();

	/**
	* \brief Lifts the barrels and spaceship according to the feedback received from the analog server.
	*/
	bool process(double timeSinceLastProcess) override;

	void processStageFreetime(double timeSinceLastProcess);

	int m_score    = 0;										///<Current score.
	int m_nAttempt = 0;										///<Current attempt count.

	int m_phase     = Phase_Rest;							///<Current phase (Rest, Move, NoMove).
	int m_lastPhase = Phase_Rest;							///<Previous phase.

	int m_stage = Stage_Baseline;							///<Current stage (Baseline, FreetimeReal, FreetimeImaginary, Statistics).

	double m_feedback     = 0;								///<The current feedback value received from the VRPN server.
	double m_lastFeedback = 0;								///<Previous feedback value.
	bool m_shouldScore    = false;							///<Tells if the subject is in condition of scoring.

	float m_shipHeight = 0;									///<Current spaceship height in the scene.
	Ogre::Vector3 m_shipOrientations;						///<Current Orientation of the spaceship.

	double m_minimumFeedback = 0;							///<Minimum feedback value ever received.

	std::vector<float> m_smallObjHeight;					///<Current mini-barrels height in the scene.
	std::vector<Ogre::Vector3> m_smallObjOrientation;		///<Current Orientation of the mini-barrels.

	double m_statSpaceshipLiftTime = 0;
	bool m_shouldIncrementStat     = false;


	/** 
	* \brief Key pressed callback, launched when a key is pressed.
	* \return \em true if the rendering engine should continue.
	*/
	bool keyPressed(const OIS::KeyEvent& evt) override;

	double m_betaOffset        = 0;
	int m_betaOffsetPercentage = 0;

	/****************
	* Localization. *
	*****************/
	std::map<std::string, std::string> m_localizedFilenames;
	std::string m_localization;
};
}  // namespace VRDemos
