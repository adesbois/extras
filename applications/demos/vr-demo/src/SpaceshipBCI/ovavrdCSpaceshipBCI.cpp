#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovavrdCSpaceshipBCI.h"

#include <openvibe/ov_directories.h>

#include <iostream>
#include <fstream>
#include <list>
#include <map>

using namespace VRDemos;
using namespace Ogre;
using namespace std;

static const float SMALL_OBJECT_MIN_HEIGHT     = -2.0F;
static const float SMALL_OBJECT_MAX_HEIGHT     = 0.0F;
static const float SMALL_OBJECT_MOVE_SPEED     = .02F;
static const float SMALL_OBJECT_ATTENUATION    = .99F;
static const float SMALL_OBJECT_ROTATION_SPEED = 0.05F;

static const float MIN_HEIGHT     = -0.7F;
static const float MAX_HEIGHT     = 6.0F;
static const float ATTENUATION    = .99F;
static const float ROTATION_SPEED = 0.50F;
static const float MOVE_SPEED     = 0.01F; // 0.004;

static const float OFFSET = 2.0F;

#if defined TARGET_OS_Linux
 #define _strcmpi strcasecmp
#endif

#if !( (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8) )
namespace CEGUI
{ 
	typedef CEGUI::UVector2 USize;
};
#endif

CSpaceshipBCI::CSpaceshipBCI(const string& localization) : COgreVRApplication(OpenViBE::Directories::getDataDir() + "/applications/vr-demo/spaceship")
{
	m_localization = localization;
	if (_strcmpi(localization.c_str(), "fr") == 0)
	{
		m_localizedFilenames.insert(make_pair("phase-init", "pret.png"));
		m_localizedFilenames.insert(make_pair("phase-move", "bouge-les-pieds.png"));
		m_localizedFilenames.insert(make_pair("phase-stop", "stop.png"));
		m_localizedFilenames.insert(make_pair("phase-end", "fin-de-partie.png"));
	}
	else
	{
		m_localizedFilenames.insert(make_pair("phase-init", "ready.png"));
		m_localizedFilenames.insert(make_pair("phase-move", "move-your-feet.png"));
		m_localizedFilenames.insert(make_pair("phase-stop", "stop.png"));
		m_localizedFilenames.insert(make_pair("phase-end", "game-over.png"));
	}
}

bool CSpaceshipBCI::initialise()
{
	//----------- LIGHTS -------------//
	m_sceneManager->setAmbientLight(ColourValue(0.6F, 0.6F, 0.6F));
	m_sceneManager->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE);

	Light* light1 = m_sceneManager->createLight("Light1");
	light1->setPosition(-2.F, 6.F, 2.F);
	light1->setSpecularColour(1.F, 1.F, 1.F);
	light1->setDiffuseColour(1.F, 1.F, 1.F);

	//----------- CAMERA -------------//
	m_camera->setNearClipDistance(0.1F);
	m_camera->setFarClipDistance(50000.0F);
	m_camera->setFOVy(Radian(Degree(70.F)));
	m_camera->setProjectionType(PT_PERSPECTIVE);

	m_camera->setPosition(-10.F, 0.9F, 0.F);
	m_camera->setOrientation(Quaternion(0.707107F, 0.F, -0.707107F, 0.F));

	//----------- HANGAR -------------//
	loadHangar();

	//----------- PARTICLES -------------//
	/*ParticleSystem* particleSystem = m_sceneManager->createParticleSystem("spark-particles","spaceship/spark");
	SceneNode* particleNode          = m_sceneManager->getRootSceneNode()->createChildSceneNode("ParticleNode");
	particleNode->attachObject(particleSystem);
	particleNode->setPosition(9.f,5.f,-13.f);*/

	// populate the hangar with barrels near the walls
	loadHangarBarrels();

	//----------- Ship -------------//
	loadShip();

	//----------- SMALL OBJECTS -------------//
	loadBarrels();

	//----------- GUI -------------//
	loadGUI();

	return true;
}

void CSpaceshipBCI::loadGUI()
{
	const string moveImage = m_localizedFilenames["phase-move"];
	const string stopImage = m_localizedFilenames["phase-stop"];
	const string initImage = m_localizedFilenames["phase-init"];
	const string endImage  = m_localizedFilenames["phase-end"];

	//ENG
	//const string moveImage = "move.png";
	//const string stopImage = "stop-move.png";
	//const string initImage = "calibration.png";

	/*CEGUI::Window * widget  = m_guiWindowManager->createWindow("TaharezLook/StaticText", "score");
	widget->setPosition(CEGUI::UVector2(cegui_reldim(0.01f), cegui_reldim(0.01f)) );
	widget->setSize(CEGUI::USize(CEGUI::UDim(0.2f, 0.f), CEGUI::UDim(0.08f, 0.f)));
	m_sheet->addChildWindow(widget);
	widget->setFont("BlueHighway-24");
	widget->setText("Score: 0\n");
	widget->setProperty("HorzFormatting","WordWrapCentred");
	widget->setProperty("VertFormatting","WordWrapCentred");*/

	CEGUI::Window* move = m_guiManager->createWindow("TaharezLook/StaticImage", "Move");
	move->setPosition(CEGUI::UVector2(cegui_reldim(0.35F), cegui_reldim(0.8F)));
	move->setSize(CEGUI::USize(CEGUI::UDim(0.3F, 0.F), CEGUI::UDim(0.2F, 0.F)));
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(move);
	CEGUI::ImageManager::getSingleton().addFromImageFile("ImageMove", moveImage);
	move->setProperty("Image", "ImageMove");
#else
	m_sheet->addChildWindow(move);
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageMove", moveImage);
	move->setProperty("Image", "set:ImageMove image:full_image");
#endif
	move->setProperty("FrameEnabled", "False");
	move->setProperty("BackgroundEnabled", "False");

	CEGUI::Window* noMove = m_guiManager->createWindow("TaharezLook/StaticImage", "NoMove");
	noMove->setPosition(CEGUI::UVector2(cegui_reldim(0.35F), cegui_reldim(0.8F)));
	noMove->setSize(CEGUI::USize(CEGUI::UDim(0.3F, 0.F), CEGUI::UDim(0.2F, 0.F)));
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(noMove);
	CEGUI::ImageManager::getSingleton().addFromImageFile("ImageNoMove", stopImage);
	noMove->setProperty("Image", "ImageNoMove");
#else
	m_sheet->addChildWindow(noMove);
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageNoMove", stopImage);
	noMove->setProperty("Image", "set:ImageNoMove image:full_image");
#endif

	noMove->setProperty("FrameEnabled", "False");
	noMove->setProperty("BackgroundEnabled", "False");

	CEGUI::Window* calibration = m_guiManager->createWindow("TaharezLook/StaticImage", "Calibration");
	calibration->setPosition(CEGUI::UVector2(cegui_reldim(0.35F), cegui_reldim(0.8F)));
	calibration->setSize(CEGUI::USize(CEGUI::UDim(0.3F, 0.F), CEGUI::UDim(0.2F, 0.F)));
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(calibration);
	CEGUI::ImageManager::getSingleton().addFromImageFile("ImageCalibration", initImage);
	calibration->setProperty("Image", "ImageCalibration");
#else
	m_sheet->addChildWindow(calibration);
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageCalibration", initImage);
	calibration->setProperty("Image", "set:ImageCalibration image:full_image");
#endif
	calibration->setProperty("FrameEnabled", "False");
	calibration->setProperty("BackgroundEnabled", "False");

	CEGUI::Window* statsImage = m_guiManager->createWindow("TaharezLook/StaticImage", "StatsImage");
	statsImage->setPosition(CEGUI::UVector2(cegui_reldim(0.25F), cegui_reldim(0.2F)));
	statsImage->setSize(CEGUI::USize(CEGUI::UDim(0.5F, 0.F), CEGUI::UDim(0.2F, 0.F)));
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(statsImage);
	CEGUI::ImageManager::getSingleton().addFromImageFile("ImageStatistics", endImage);
	statsImage->setProperty("Image", "ImageStatistics");
#else
	m_sheet->addChildWindow(statsImage);
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("ImageStatistics", endImage);
	statsImage->setProperty("Image", "set:ImageStatistics image:full_image");
#endif
	statsImage->setProperty("FrameEnabled", "False");
	statsImage->setProperty("BackgroundEnabled", "False");
	statsImage->setVisible(false);

	CEGUI::Window* statistics = m_guiManager->createWindow("TaharezLook/StaticText", "Statistics");
	statistics->setPosition(CEGUI::UVector2(cegui_reldim(0.25F), cegui_reldim(0.35F)));
	statistics->setSize(CEGUI::USize(CEGUI::UDim(0.5F, 0.F), CEGUI::UDim(0.5F, 0.F)));
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(statistics);
	statistics->setFont("BlueHighway-24");
	statistics->setProperty("HorzFormatting", "WordWrapCentreAligned");
	statistics->setProperty("VertFormatting", "WordWrapCentreAligned");
	statistics->setVisible(false);
#else
	m_sheet->addChildWindow(statistics);
	statistics->setFont("BlueHighway-24");
	statistics->setProperty("HorzFormatting", "WordWrapCentred");
	statistics->setProperty("VertFormatting", "WordWrapCentred");
	statistics->setVisible(false);
#endif

	CEGUI::Window* threshold = m_guiManager->createWindow("TaharezLook/StaticText", "Threshold");
	threshold->setPosition(CEGUI::UVector2(cegui_reldim(0.01F), cegui_reldim(0.01F)));
	threshold->setSize(CEGUI::USize(CEGUI::UDim(0.15F, 0.F), CEGUI::UDim(0.08F, 0.F)));
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(threshold);
#else
	m_sheet->addChildWindow(threshold);
#endif
	threshold->setFont("BlueHighway-12");
	threshold->setProperty("HorzFormatting", "WordWrapCentred");
	threshold->setProperty("VertFormatting", "WordWrapCentred");
	threshold->setVisible(false);
	std::stringstream ss;
	ss << "Seuil : 0%";
	threshold->setText(ss.str());
}

void CSpaceshipBCI::loadHangar() const
{
	Entity* hangarEntity = m_sceneManager->createEntity("Hangar", "hangar.mesh");
	hangarEntity->setCastShadows(false);
	hangarEntity->getSubEntity(0)->setMaterialName("hangar-01_-_Default");
	hangarEntity->getSubEntity(1)->setMaterialName("hangar-03_-_Default");
	hangarEntity->getSubEntity(2)->setMaterialName("hangar-orig_08_-_Default");
	hangarEntity->getSubEntity(3)->setMaterialName("hangar-07_-_Default");

	SceneNode* hangarNode = m_sceneManager->getRootSceneNode()->createChildSceneNode("HangarNode");
	hangarNode->attachObject(hangarEntity);

	hangarNode->setScale(1.F, 1.F, 1.F);
	hangarNode->setPosition(159.534F, 3.22895F, 0.0517212F);
	hangarNode->setOrientation(Quaternion(0.5F, 0.5F, -0.5F, 0.5F));
}

void CSpaceshipBCI::loadShip()
{
	Entity* spaceshipEntity = m_sceneManager->createEntity("Ship", "vaisseau.mesh");
	spaceshipEntity->setCastShadows(true);

	SceneNode* spaceshipNode = m_sceneManager->getRootSceneNode()->createChildSceneNode("ShipNode");
	spaceshipNode->attachObject(spaceshipEntity);

	spaceshipNode->setScale(0.008F, 0.008F, 0.008F);
	const float offset = OFFSET;
	spaceshipNode->setPosition(4.5F, MIN_HEIGHT, -2.F + offset);
	spaceshipNode->rotate(Vector3::UNIT_Y, -Radian(Math::PI / 2.F));

	m_shipOrientations[0] = spaceshipNode->getOrientation().x;
	m_shipOrientations[1] = spaceshipNode->getOrientation().y;
	m_shipOrientations[2] = spaceshipNode->getOrientation().z;
}

void CSpaceshipBCI::loadBarrels()
{
	for (size_t i = 0; i < 3; ++i)
	{
		m_smallObjHeight.push_back(SMALL_OBJECT_MIN_HEIGHT);
		m_smallObjOrientation.push_back(Vector3(0, 0, 0));
	}

	const float offset = OFFSET;

	//-- 1st
	Entity* barrel1Entity = m_sceneManager->createEntity("Mini1", "barrel.mesh");
	barrel1Entity->setCastShadows(true);
	barrel1Entity->setMaterialName("RustyBarrel");

	SceneNode* barrel1Node = m_sceneManager->getRootSceneNode()->createChildSceneNode("Mini1Node");
	barrel1Node->attachObject(barrel1Entity);

	barrel1Node->setScale(0.15F, 0.15F, 0.15F);
	barrel1Node->setPosition(4.F, SMALL_OBJECT_MIN_HEIGHT, -6 + offset);
	barrel1Node->rotate(Vector3::UNIT_X, Radian(Math::PI / 2.F));

	//-- 2nd
	Entity* barrel2Entity = m_sceneManager->createEntity("Mini2", "barrel.mesh");
	barrel2Entity->setCastShadows(true);
	barrel2Entity->setMaterialName("RustyBarrel");

	SceneNode* barrel2Node = m_sceneManager->getRootSceneNode()->createChildSceneNode("Mini2Node");
	barrel2Node->attachObject(barrel2Entity);

	barrel2Node->setScale(0.15F, 0.15F, 0.15F);
	barrel2Node->setPosition(3.F, SMALL_OBJECT_MIN_HEIGHT, 1 + offset);
	barrel2Node->rotate(Vector3::UNIT_X, Radian(Math::PI / 2.F));

	//-- 3rd
	Entity* barrel3Entity = m_sceneManager->createEntity("Mini3", "barrel.mesh");
	barrel3Entity->setCastShadows(true);
	barrel3Entity->setMaterialName("RustyBarrel");

	SceneNode* barrel3Node = m_sceneManager->getRootSceneNode()->createChildSceneNode("Mini3Node");
	barrel3Node->attachObject(barrel3Entity);

	barrel3Node->setScale(0.15F, 0.15F, 0.15F);
	barrel3Node->setPosition(2.5F, SMALL_OBJECT_MIN_HEIGHT, -2 + offset);
	barrel3Node->rotate(Vector3::UNIT_X, Radian(Math::PI / 2.F));
}

void CSpaceshipBCI::loadHangarBarrels() const
{
	//-- 1st
	Entity* barrel1Entity = m_sceneManager->createEntity("Barrel1", "barrel.mesh");
	barrel1Entity->setCastShadows(true);
	barrel1Entity->setMaterialName("RustyBarrel");

	SceneNode* barrel1Node = m_sceneManager->getRootSceneNode()->createChildSceneNode("Barrel1Node");
	barrel1Node->attachObject(barrel1Entity);

	barrel1Node->setScale(0.15F, 0.15F, 0.15F);
	barrel1Node->setPosition(7, -2, -10);
	barrel1Node->rotate(Vector3::UNIT_X, Radian(Math::PI / 2.F));

	//-- 2nd
	Entity* barrel2Entity = m_sceneManager->createEntity("Barrel2", "barrel.mesh");
	barrel2Entity->setCastShadows(true);
	barrel2Entity->setMaterialName("RustyBarrel");

	SceneNode* barrel2Node = m_sceneManager->getRootSceneNode()->createChildSceneNode("Barrel2Node");
	barrel2Node->attachObject(barrel2Entity);

	barrel2Node->setScale(0.15F, 0.25F, 0.15F);
	barrel2Node->setPosition(7, -2, -12);
	barrel2Node->rotate(Vector3::UNIT_X, Radian(Math::PI));

	//-- 3rd
	Entity* barrel3Entity = m_sceneManager->createEntity("Barrel3", "barrel.mesh");
	barrel3Entity->setCastShadows(true);
	barrel3Entity->setMaterialName("RustyBarrel");

	SceneNode* barrel3Node = m_sceneManager->getRootSceneNode()->createChildSceneNode("Barrel3Node");
	barrel3Node->attachObject(barrel3Entity);

	barrel3Node->setScale(0.15F, 0.2F, 0.15F);
	barrel3Node->setPosition(8, -2, -12);
	barrel3Node->rotate(Vector3::UNIT_X, Radian(Math::PI));

	//-- 4th
	Entity* barrel4Entity = m_sceneManager->createEntity("Barrel4", "barrel.mesh");
	barrel4Entity->setCastShadows(true);
	barrel4Entity->setMaterialName("RustyBarrel");

	SceneNode* barrel4Node = m_sceneManager->getRootSceneNode()->createChildSceneNode("Barrel4Node");
	barrel4Node->attachObject(barrel4Entity);

	barrel4Node->setScale(0.15F, 0.2F, 0.15F);
	barrel4Node->setPosition(8, -1, -12);
	barrel4Node->rotate(Vector3::UNIT_X, Radian(Math::PI));
}

bool CSpaceshipBCI::process(double timeSinceLastProcess)
{
	while (!m_vrpnPeripheral->m_Buttons.empty())
	{
		pair<int, int>& vrpnButtonState = m_vrpnPeripheral->m_Buttons.front();

		if (vrpnButtonState.second) // if the button is ON
		{
			// std::cout << "Received button " << vrpnButtonState.first << "\n";

			switch (vrpnButtonState.first)
			{
				case 0: m_phase = Phase_Rest;
					break;
				case 1: m_phase = Phase_Move;
					break;
				case 2: m_phase = Phase_NoMove;
					break;
				case 3: m_stage = Stage_Baseline;
					break;
				case 4: m_stage = Stage_FreetimeReal;
					break;
				case 5: m_stage = Stage_FreetimeImaginary;
					break;
				case 6: m_stage = Stage_Statistics;
					break;
				default: std::cout << "Received unsupported button " << vrpnButtonState.first << "\n";
			}
		}

		m_vrpnPeripheral->m_Buttons.pop_front();
	}

	if (!m_vrpnPeripheral->m_Analogs.empty())
	{
		std::list<double>& vrpnAnalogState = m_vrpnPeripheral->m_Analogs.front();

		m_feedback        = *(vrpnAnalogState.begin());
		m_minimumFeedback = (m_minimumFeedback > m_feedback ? m_feedback : m_minimumFeedback);

		// std::cout << "Received analog " << m_feedback << "\n";

		m_vrpnPeripheral->m_Analogs.pop_front();
	}

	if (m_lastPhase != m_phase)
	{
		switch (m_phase)
		{
			case Phase_NoMove:
				m_shouldScore = true;
				m_nAttempt++;
				break;
			default:
				m_shouldScore = false;
				break;
		}
	}

	if (m_shouldScore)
	{
		const double threshold = 1E-3;
		if (m_lastFeedback < threshold && m_feedback >= threshold)
		{
			m_score++;
			m_shouldScore = false;
		}
	}

	// -------------------------------------------------------------------------------
	// GUI

	switch (m_phase)
	{
		case Phase_Rest:
			m_sheet->getChild("Move")->setVisible(false);
			m_sheet->getChild("NoMove")->setVisible(false);
			break;

		case Phase_Move:
			m_sheet->getChild("Move")->setVisible(true);
			m_sheet->getChild("NoMove")->setVisible(false);
			break;

		case Phase_NoMove:
			m_sheet->getChild("Move")->setVisible(false);
			m_sheet->getChild("NoMove")->setVisible(true);
			break;

		default:
			m_sheet->getChild("Move")->setVisible(false);
			m_sheet->getChild("NoMove")->setVisible(false);
			break;
	}
	std::stringstream ss;
	int count;
	string rang;
	switch (m_stage)
	{
		case Stage_Baseline:
			m_sheet->getChild("Move")->setVisible(false);
			m_sheet->getChild("NoMove")->setVisible(false);
			m_sheet->getChild("Calibration")->setVisible(true);
			m_sheet->getChild("Statistics")->setVisible(false);
			m_sheet->getChild("StatsImage")->setVisible(false);
			break;
		case Stage_FreetimeReal:
			m_sheet->getChild("Calibration")->setVisible(false);
			m_sheet->getChild("Statistics")->setVisible(false);
			m_sheet->getChild("StatsImage")->setVisible(false);
			processStageFreetime(timeSinceLastProcess);
			break;
		case Stage_FreetimeImaginary:
			m_sheet->getChild("Calibration")->setVisible(false);
			m_sheet->getChild("Statistics")->setVisible(false);
			m_sheet->getChild("StatsImage")->setVisible(false);
			processStageFreetime(timeSinceLastProcess);
			break;
		case Stage_Statistics:
			count = 10;
			m_sheet->getChild("Move")->setVisible(false);
			m_sheet->getChild("NoMove")->setVisible(false);
			m_sheet->getChild("Calibration")->setVisible(false);
			m_sheet->getChild("Statistics")->setVisible(true);
			m_sheet->getChild("StatsImage")->setVisible(true);

			if (_strcmpi(m_localization.c_str(), "fr") == 0)
			{
				ss << " Merci d'avoir participe !" << "\n------------\n";

				ss << "Le vaisseau s'est souleve pendant :\n" << m_statSpaceshipLiftTime << " secondes.\n";
				ss << "Temps moyen par essai :\n" << m_statSpaceshipLiftTime / count << " secondes.\n";
				ss << "------------\n";
				ss << "Votre rang : \n";
				rang = "- Apprenti -";
				if (m_statSpaceshipLiftTime / count > 1.0) { rang = "{ Chevalier }"; }
				if (m_statSpaceshipLiftTime / count > 2.0) { rang = "-oO Maitre Oo-"; }
				ss << rang << "\n";
			}
			else
			{
				ss << " Thanks for your participation" << "\n------------\n";

				ss << "You lifted the spaceship during :\n" << m_statSpaceshipLiftTime << " seconds.\n";
				ss << "Mean time per attempt :\n" << m_statSpaceshipLiftTime / count << " seconds.\n";
				ss << "------------\n";
				ss << "Rank : \n";
				rang = "- Apprentice -";
				if (m_statSpaceshipLiftTime / count > 1.0) { rang = "{ Knight }"; }
				if (m_statSpaceshipLiftTime / count > 2.0) { rang = "-oO Master Oo-"; }
				ss << rang << "\n";
			}

			m_sheet->getChild("Statistics")->setText(ss.str());
			break;

		default:
			break;
	}

	// -------------------------------------------------------------------------------
	// End of computation
	std::stringstream ss2;
	ss2 << "Offset : " << m_betaOffsetPercentage << "%";
	m_sheet->getChild("Threshold")->setText(ss2.str());

	m_lastFeedback = m_feedback;
	m_lastPhase    = m_phase;

	return m_bContinue;
}
// -------------------------------------------------------------------------------
bool CSpaceshipBCI::keyPressed(const OIS::KeyEvent& evt)
{
	if (evt.key == OIS::KC_ESCAPE)
	{
		cout << "[ESC] pressed, user termination." << endl;
		cout << "      Saving statistics..." << endl;
		std::stringstream path(OpenViBE::Directories::getLogDir().toASCIIString());
		path << "/openvibe-vr-demo-spaceship-stats.txt";
		remove(path.str().c_str());
		ofstream subjectConf(path.str().c_str());
		subjectConf << "Temps total = " << m_statSpaceshipLiftTime << "\n";
		m_bContinue = false;
	}
	if (evt.key == OIS::KC_END)
	{
		const bool visibility = m_sheet->getChild("Threshold")->isVisible();
		m_sheet->getChild("Threshold")->setVisible(!visibility);
	}
	if (evt.key == OIS::KC_UP)
	{
		m_betaOffset += (-m_minimumFeedback) / 100;
		m_betaOffsetPercentage++;
	}
	if (evt.key == OIS::KC_DOWN)
	{
		m_betaOffset -= (-m_minimumFeedback) / 100;
		m_betaOffsetPercentage--;
	}

	return true;
}

// -------------------------------------------------------------------------------
void CSpaceshipBCI::processStageFreetime(const double timeSinceLastProcess)
{
	// -------------------------------------------------------------------------------
	// Ship 

	if (m_feedback <= m_betaOffset)
	{
		m_shipOrientations[0] *= ATTENUATION;
		m_shipOrientations[1] *= ATTENUATION;
		m_shipOrientations[2] *= ATTENUATION;
		m_shipHeight *= ATTENUATION;
		m_shouldIncrementStat = false;
	}
	else
	{
		m_shipOrientations[0] += ROTATION_SPEED * ((rand() & 1) == 0 ? -1.0 : 1.0);
		m_shipOrientations[1] += ROTATION_SPEED * ((rand() & 1) == 0 ? -1.0 : 1.0);
		m_shipOrientations[2] += ROTATION_SPEED * ((rand() & 1) == 0 ? -1.0 : 1.0);
		m_shipHeight += float((m_feedback - m_betaOffset) * MOVE_SPEED);
		if (m_shouldIncrementStat) { m_statSpaceshipLiftTime += timeSinceLastProcess; }

		if (m_shipOrientations[0] > 5) { m_shipOrientations[0] = 5; }
		if (m_shipOrientations[1] > 5) { m_shipOrientations[1] = 5; }
		if (m_shipOrientations[2] > 5) { m_shipOrientations[2] = 5; }
		if (m_shipOrientations[0] < -5) { m_shipOrientations[0] = -5; }
		if (m_shipOrientations[1] < -5) { m_shipOrientations[1] = -5; }
		if (m_shipOrientations[2] < -5) { m_shipOrientations[2] = -5; }
		if (m_shipHeight > MAX_HEIGHT) { m_shipHeight = MAX_HEIGHT; }
		m_shouldIncrementStat = true;
	}

	// -------------------------------------------------------------------------------
	// Mini Objects
	// For n mini-objects, each one has its own threshold, in a regular n+1 partition.
	// First mini-object starts lifting on the second part (nothing happens in the first part).
	// The threshold for Ship is 0+m_betaOffset.
	//
	// MIN=T0         T1          T2        Tn BetaOffset 
	// ---|--nothing--|--1st mini--|--/~~/---|-----|----

	const size_t nSmallObjects = m_smallObjHeight.size();
	for (size_t i = 0; i < nSmallObjects; ++i)
	{
		if (m_feedback - m_betaOffset <= (m_minimumFeedback - ((i + 1) * m_minimumFeedback / (nSmallObjects + 1))))
		{
			m_smallObjHeight[i] = ((m_smallObjHeight[i] - SMALL_OBJECT_MIN_HEIGHT) * SMALL_OBJECT_ATTENUATION) + SMALL_OBJECT_MIN_HEIGHT;
			m_smallObjOrientation[i][0] *= ATTENUATION;
			m_smallObjOrientation[i][1] *= ATTENUATION;
			m_smallObjOrientation[i][2] *= ATTENUATION;
		}
		else
		{
			m_smallObjOrientation[i][0] += float((rand() & 1) == 0 ? -1 : 1) * SMALL_OBJECT_ROTATION_SPEED;
			m_smallObjOrientation[i][1] += float((rand() & 1) == 0 ? -1 : 1) * SMALL_OBJECT_ROTATION_SPEED;
			m_smallObjOrientation[i][2] += float((rand() & 1) == 0 ? -1 : 1) * SMALL_OBJECT_ROTATION_SPEED;

			m_smallObjHeight[i] += float(m_feedback < m_lastFeedback ? -1 : 1) * float(rand() % 100 + 50) / 100.0F * SMALL_OBJECT_MOVE_SPEED * float(i + 1);
		}

		if (m_smallObjHeight[i] > SMALL_OBJECT_MAX_HEIGHT)
		{
			m_smallObjHeight[i] = SMALL_OBJECT_MAX_HEIGHT + float((rand() & 1) == 0 ? -1 : 1) * SMALL_OBJECT_MOVE_SPEED;
		}
		if (m_smallObjHeight[i] < SMALL_OBJECT_MIN_HEIGHT) { m_smallObjHeight[i] = SMALL_OBJECT_MIN_HEIGHT; }
	}

	// -------------------------------------------------------------------------------
	// Object translations / rotations

	//height
	const Vector3 shipPosition = m_sceneManager->getSceneNode("ShipNode")->getPosition();
	m_sceneManager->getSceneNode("ShipNode")->setPosition(shipPosition.x, MIN_HEIGHT + m_shipHeight, shipPosition.z);

	//orientation
	m_sceneManager->getSceneNode("ShipNode")->setOrientation(Quaternion(1, m_shipOrientations[0] * Math::PI / 180, m_shipOrientations[1] * Math::PI / 180,
																		m_shipOrientations[2] * Math::PI / 180));
	m_sceneManager->getSceneNode("ShipNode")->rotate(Vector3::UNIT_Y, Radian(Math::PI / 2.F));

	//score
	// CEGUI::Window * widget  = m_sheet->getChild("score");
	// stringstream ss;
	// ss << "Score: "<< m_score << " / "<<m_nAttempt<<"\n";
	// widget->setText(ss.str());

	m_sceneManager->getSceneNode("Mini1Node")->setOrientation(Quaternion(1, m_smallObjOrientation[0][0], m_smallObjOrientation[0][1],
																		 m_smallObjOrientation[0][2]));
	const Vector3 miniShip1Position = m_sceneManager->getSceneNode("Mini1Node")->getPosition();
	m_sceneManager->getSceneNode("Mini1Node")->setPosition(miniShip1Position.x, m_smallObjHeight[0], miniShip1Position.z);

	m_sceneManager->getSceneNode("Mini2Node")->setOrientation(Quaternion(1, m_smallObjOrientation[1][0], m_smallObjOrientation[1][1],
																		 m_smallObjOrientation[1][2]));
	const Vector3 miniShip2Position = m_sceneManager->getSceneNode("Mini2Node")->getPosition();
	m_sceneManager->getSceneNode("Mini2Node")->setPosition(miniShip2Position.x, m_smallObjHeight[1], miniShip2Position.z);

	m_sceneManager->getSceneNode("Mini3Node")->setOrientation(Quaternion(1, m_smallObjOrientation[2][0], m_smallObjOrientation[2][1],
																		 m_smallObjOrientation[2][2]));
	const Vector3 miniShip3Position = m_sceneManager->getSceneNode("Mini3Node")->getPosition();
	m_sceneManager->getSceneNode("Mini3Node")->setPosition(miniShip3Position.x, m_smallObjHeight[2], miniShip3Position.z);
}

#endif
