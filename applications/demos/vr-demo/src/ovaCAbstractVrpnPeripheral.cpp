#include "ovaCAbstractVrpnPeripheral.h"

#include <vrpn_Tracker.h>
#include <vrpn_Button.h>
#include <vrpn_Analog.h>

using namespace VRDemos;

class CDeviceInfo
{
public:
	std::string m_Address;
	vrpn_Button_Remote* m_Button = nullptr;
	vrpn_Analog_Remote* m_Analog = nullptr;
};

namespace {
void VRPN_CALLBACK handle_button(void* data, const vrpn_BUTTONCB b)
{
	CAbstractVrpnPeripheral* abstractVrpnPeripheral = static_cast<CAbstractVrpnPeripheral*>(data);

	std::pair<int, int> vrpnButtonState;
	vrpnButtonState.first  = b.button;
	vrpnButtonState.second = b.state;

	abstractVrpnPeripheral->m_Buttons.push_back(vrpnButtonState);
}

void VRPN_CALLBACK handle_analog(void* data, const vrpn_ANALOGCB a)
{
	CAbstractVrpnPeripheral* abstractVrpnPeripheral = static_cast<CAbstractVrpnPeripheral*>(data);
	std::list<double> vrpnAnalogState;

	for (int i = 0; i < a.num_channel; ++i)
	{
		vrpnAnalogState.push_back(a.channel[i] * abstractVrpnPeripheral->m_AnalogScale + abstractVrpnPeripheral->m_AnalogOffset);
	}

	abstractVrpnPeripheral->m_Analogs.push_back(vrpnAnalogState);
}
}  // namespace

CAbstractVrpnPeripheral::~CAbstractVrpnPeripheral()
{
	if (m_Device)
	{
		delete m_Device->m_Analog;
		delete m_Device->m_Button;
		delete m_Device;
		m_Device = nullptr;
	}
}

void CAbstractVrpnPeripheral::init()
{
	m_Device            = new CDeviceInfo;
	m_Device->m_Address = m_Address;
	m_Device->m_Analog  = new vrpn_Analog_Remote(m_Address.c_str());
	m_Device->m_Button  = new vrpn_Button_Remote(m_Address.c_str());

	m_Device->m_Button->register_change_handler(this, &handle_button);
	m_Device->m_Analog->register_change_handler(this, &handle_analog);
}

void CAbstractVrpnPeripheral::loop() const
{
	m_Device->m_Button->mainloop();
	m_Device->m_Analog->mainloop();
}
