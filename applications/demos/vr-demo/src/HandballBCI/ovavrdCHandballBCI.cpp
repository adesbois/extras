#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include <openvibe/ov_directories.h>
#include "ovavrdCHandballBCI.h"
#include <iostream>

template <typename T>
T Abs(const T& t) { return t < 0 ? -t : t; }

template <typename T>
T Min(const T& t1, const T& t2) { return t1 < t2 ? t1 : t2; }

template <typename T>
T Max(const T& t1, const T& t2) { return t1 < t2 ? t2 : t1; }

template <typename T>
T Crop(const T& t1, const T& t, const T& t2) { return Min(Max(t1, t), t2); }

using namespace VRDemos;
using namespace Ogre;

//static const float g_fGrowScoreingSpeed = 0.1f;
static const float VISCOSITY     = 0.003F;
static const double F_EPSILON    = 1E-5;
static const size_t HISTORY_SIZE = 5;
static const float GOAL_DISTANCE = 6.0F;

CHandballBCI::CHandballBCI() : COgreVRApplication(OpenViBE::Directories::getDataDir() + "/applications/vr-demo/handball")
{
	// scores
	m_goalScore   = 0;
	m_leftScore   = 0;
	m_rightScore  = 0;
	m_nTrialRight = 0;
	m_nTrialLeft  = 0;
	// the classification score
	m_classificationScore = 0;

	// The current and previous phases
	m_phase     = Phase_Rest;
	m_lastPhase = Phase_Rest;

	// Feedback from analog VRPN server
	m_feedback     = 0;
	m_lastFeedback = 0;
	m_maxFeedback  = 0;
	m_minFeedback  = 0;

	m_mark                  = Mark_None;
	m_lastMark              = Mark_None;
	m_goalMarkedAtThisPhase = false;
	m_showCross             = false;
	m_showClue              = false;
	m_showFeedback          = false;

	m_ballSpeed        = 0;
	m_ballPosition     = 0;
	m_lastBallPosition = 0;
	m_ballOrientation  = 0;
}

bool CHandballBCI::initialise()
{
	// taken from gymnasium.scene + transformations from demo.omk

	//----------- LIGHTS -------------//
	m_sceneManager->setAmbientLight(ColourValue(0.4F, 0.4F, 0.4F));
	m_sceneManager->setShadowTechnique(SHADOWTYPE_TEXTURE_MODULATIVE);

	Light* light1 = m_sceneManager->createLight("Light1");
	//SceneNode *light1Node = m_sceneManager->getRootSceneNode()->createChildSceneNode( "Light1Node" );
	light1->setPosition(8, 6, -5);
	light1->setSpecularColour(1, 1, 1);
	light1->setDiffuseColour(1, 1, 1);

	//----------- CAMERA -------------//
	m_camera->setNearClipDistance(0.01F);
	m_camera->setFarClipDistance(2000.0F);
	m_camera->setFOVy(Radian(Degree(100)));
	m_camera->setProjectionType(PT_PERSPECTIVE);

	m_cameraNode->setPosition(5.5F, 0.9F, 0.F);
	m_cameraNode->yaw(Radian(Math::PI / 2.F));

	this->initSceneGymnasium();
	this->initSceneAds();
	this->initSceneGoals();
	this->initSceneCrossArrowAndFeedback();
	this->initSceneBalls();

	return true;
}


bool CHandballBCI::process(double /*timeSinceLastProcess*/)
{
	//------------ VRPN --------------//
	// The button VRPN server must use 6 switches, inputs from the stim scenario (lua):
	/*

	[ GET READY | | ACTIVE | REST |~| MARK LEFT | MARK RIGHT | NO INSTRUCTION ]
		
	*/
	while (!m_vrpnPeripheral->m_Buttons.empty())
	{
		std::pair<int, int>& vrpnButtonState = m_vrpnPeripheral->m_Buttons.front();

		if (vrpnButtonState.second)
		{
			switch (vrpnButtonState.first)
			{
				case 0:
					m_phase = Phase_GetReady;
					m_showCross    = true;
					m_showClue     = false;
					m_showFeedback = false;
					break;

				case 1:
					m_phase = Phase_Active;
					m_showCross    = true;
					m_showClue     = false;
					m_showFeedback = true;
					break;

				case 2:
					m_phase = Phase_Rest;
					m_showCross    = false;
					m_showClue     = false;
					m_showFeedback = false;
					break;
					//------------Instructions ---------------
				case 3:
					m_mark = Mark_Left;
					m_showClue = true;
					m_nTrialLeft++;
					break;

				case 4:
					m_mark = Mark_Right;
					m_showClue = true;
					m_nTrialRight++;
					break;

				case 5:
					m_mark = Mark_None;
					break;

				default:
					break;
			}
		}

		m_vrpnPeripheral->m_Buttons.pop_front();
	}

	if (!m_vrpnPeripheral->m_Analogs.empty())
	{
		std::list<double>& vrpnAnalogState = m_vrpnPeripheral->m_Analogs.front();

		//we take the last value from the server. 
		// The input is a probability [0,1] for the left class, map it to [-1,1] to indicate [left,right] range
		double analog = (*vrpnAnalogState.begin());
		analog        = 2.0 * (-analog) + 1.0;

		// std::cout << "list size" << vrpnAnalogState.size() << " head " << analog << "\n";

		//we updtae the max and min values
		// A. VLG : we should compute the min/max only in the ACTIVE phase
		// but if so, the first feedback value is infinite, resulting in a strange ball placement in the first active phase.
		if (m_phase == Phase_Active || m_phase == Phase_GetReady)
		{
			if (analog > m_maxFeedback) { m_maxFeedback = analog; }
			if (analog < m_minFeedback) { m_minFeedback = analog; }
		}

		//maximum of the absolute values

		const double absoluteMinMax = Max(Abs(m_maxFeedback), Abs(m_minFeedback));

		//we update the history, keeping only the last HISTORY_SIZE values
		m_analogHistories.push_back(analog);
		if (m_analogHistories.size() > HISTORY_SIZE) { m_analogHistories.pop_front(); }

		// we compute the mean value of the analog history
		double analogMean = 0;
		for (auto i = m_analogHistories.begin(); i != m_analogHistories.end(); ++i) { analogMean += *i; }
		analogMean /= m_analogHistories.size();

		// and we divide it by the absolute max value, to get a value between 0 and 1
		analogMean /= double(absoluteMinMax != 0.0 ? absoluteMinMax : F_EPSILON);

		// we update the feedback value that will be used to move the ball
		m_lastFeedback = m_feedback;
		m_feedback     = analogMean;

		m_vrpnPeripheral->m_Analogs.pop_front();
	}


	//--------------------------//
	SceneNode* crossNode         = m_sceneManager->getSceneNode("CrossNode");
	SceneNode* leftArrowNode     = m_sceneManager->getSceneNode("LeftArrowNode");
	SceneNode* rightArrowNode    = m_sceneManager->getSceneNode("RightArrowNode");
	SceneNode* feedbackNode      = m_sceneManager->getSceneNode("FeedbackNode");
	SceneNode* activeBallPivot   = m_sceneManager->getSceneNode("ActiveBallPivot");
	SceneNode* passiveBallPivot  = m_sceneManager->getSceneNode("PassiveBallPivot");
	SceneNode* getReadyBallPivot = m_sceneManager->getSceneNode("GetReadyBallPivot");
	SceneNode* ballCommonPivot   = m_sceneManager->getSceneNode("BallCommonNode");

	// update for each phase change
	if (m_phase != m_lastPhase)
	{
		switch (m_phase)
		{
			case Phase_Rest:
				std::cout << "### PHASE REST ###" << std::endl;
				if (m_lastMark == Mark_Left && m_ballPosition > 0) { m_leftScore++; }
				if (m_lastMark == Mark_Right && m_ballPosition < 0) { m_rightScore++; }

				std::cout << "- Current Score: " << std::endl;
				std::cout << "--- GOAL:  " << m_goalScore << "/" << (m_nTrialRight + m_nTrialLeft) << std::endl;
				std::cout << "--- LEFT:  " << m_leftScore << "/" << m_nTrialLeft << std::endl;
				std::cout << "--- RIGHT: " << m_rightScore << "/" << m_nTrialRight << std::endl;
				std::cout << "--- CLASSIFICATION: " << m_classificationScore << std::endl;
				//std::cout << "### ScOrE StAtUs = GoAl:" << m_goalScore << " SiDe:" << m_iSideScore << " ClAsSiFiCaTiOn:" << m_classificationScore << " LoOpCoUnT: " <<   << "\n";

				crossNode->setVisible(false);
				leftArrowNode->setVisible(false);
				rightArrowNode->setVisible(false);
				feedbackNode->setVisible(false);

				getReadyBallPivot->setVisible(false);
				passiveBallPivot->setVisible(true);
				activeBallPivot->setVisible(false);

				m_ballPosition    = 0;
				m_ballOrientation = 0;
				m_ballSpeed       = 0;
				ballCommonPivot->setPosition(0, ballCommonPivot->getPosition().y, m_ballPosition);

				break;
			case Phase_GetReady:
				std::cout << "### PHASE GET READY ###" << std::endl;
				crossNode->setVisible(true, false);
				leftArrowNode->setVisible(false);
				rightArrowNode->setVisible(false);
				feedbackNode->setVisible(false);

				getReadyBallPivot->setVisible(true);
				passiveBallPivot->setVisible(false);
				activeBallPivot->setVisible(false);

				m_goalMarkedAtThisPhase = false;

				break;


			case Phase_Active:
				std::cout << "### PHASE ACTIVE ###" << std::endl;
				crossNode->setVisible(true);
				leftArrowNode->setVisible(false);
				rightArrowNode->setVisible(false);
				feedbackNode->setVisible(true);

				getReadyBallPivot->setVisible(false);
				passiveBallPivot->setVisible(false);
				activeBallPivot->setVisible(true);

				/*std::cout<<"Current analog history : ";
				for (auto ii=m_analogHistories.begin(); ii!=m_analogHistories.end(); ++ii)
				{
					std::cout << (*ii) << " ";
				}
				std::cout << std::endl;*/

				break;
			default: break;
		}
	}


	// update for each phase (loop)
	switch (m_phase)
	{
		case Phase_Rest: break;

		case Phase_GetReady:
			leftArrowNode->setVisible(m_showClue ? (m_mark == Mark_Left) : false);
			rightArrowNode->setVisible(m_showClue ? (m_mark == Mark_Right) : false);
			break;

		case Phase_Active:
			m_ballSpeed = -float(m_feedback * 0.1);
			if (m_mark == Mark_Left && m_ballPosition >= GOAL_DISTANCE && !m_goalMarkedAtThisPhase)
			{
				m_goalScore++;
				m_goalMarkedAtThisPhase = true;
			}
			if (m_mark == Mark_Right && m_ballPosition <= -GOAL_DISTANCE && !m_goalMarkedAtThisPhase)
			{
				m_goalScore++;
				m_goalMarkedAtThisPhase = true;
			}
			if (m_mark == Mark_Left && m_feedback < 0) { m_classificationScore++; }
			if (m_mark == Mark_Right && m_feedback > 0) { m_classificationScore++; }

			break;
		default: break;
	}

	m_ballOrientation += m_ballSpeed;
	m_ballPosition += m_ballSpeed;
	m_ballPosition = Crop(-GOAL_DISTANCE, m_ballPosition, GOAL_DISTANCE);
	m_ballSpeed *= (1.F - VISCOSITY);

	double feedbackScale = m_feedback;
	if (feedbackScale <= 0 && feedbackScale > -F_EPSILON) { feedbackScale = -F_EPSILON; }
	if (feedbackScale >= 0 && feedbackScale < F_EPSILON) { feedbackScale = F_EPSILON; }

	// -------------------------------------------------------------------------------
	// Object displacement stuffs
	//BALL FEEDBACK
	if (m_phase == Phase_Active)
	{
		ballCommonPivot->setPosition(0, ballCommonPivot->getPosition().y, m_ballPosition);
		//ballCommonPivot->setOrientation(1,m_ballOrientation*100.f, 0,0 ); // *4
		//activeBallPivot->pitch(Radian(m_ballOrientation/20.),Node::TS_PARENT);

		Entity* passiveBallEntity = m_sceneManager->getEntity("passive-ball");
		const float alpha         = (m_ballPosition - m_lastBallPosition) / passiveBallEntity->getBoundingRadius();

		/*
		float div =  Math::Floor((m_ballOrientation * 4) / (2 * Math::PI));
		printf("m_ballOrientation=%f\n",(m_ballOrientation*4));
		printf("div=%i\n",div);
		float final = (m_ballOrientation*4)-(div*2*Math::PI);
		printf("final=%f\n",final);*/

		activeBallPivot->pitch(Radian(alpha), Node::TS_PARENT);
		//activeBallPivot->setOrientation(1,(m_ballOrientation*4)/*-(div*2*Math::PI)*/,0,0 ); // *4
		//activeBallPivot->setOrientation(1,m_ballOrientation*4,0,0);
	}

	// GRAZ FEEDBACK
	if (m_phase == Phase_Active)
	{
		feedbackNode->setVisible(true);
		feedbackNode->setScale(0.25F, 0.25F, Real(Abs(feedbackScale)));
		feedbackNode->setPosition(3.499F, 0.20F, -Real(feedbackScale / 2.));
	}
	else { feedbackNode->setVisible(false); }


	// -------------------------------------------------------------------------------
	// End of computation

	m_lastFeedback     = m_feedback;
	m_lastBallPosition = m_ballPosition;
	m_lastPhase        = m_phase;
	m_lastMark         = m_mark;

	return m_bContinue;
}


void CHandballBCI::initSceneGymnasium() const
{
	//----------- GYMNASIUM -------------//
	SceneNode* gymnasiumPivot = m_sceneManager->getRootSceneNode()->createChildSceneNode("GymnasiumPivot");
	SceneNode* gymnasium      = gymnasiumPivot->createChildSceneNode("Gymnasium");
	SceneNode* gymnasiumNode  = gymnasium->createChildSceneNode("GymnasiumNode");

	// GYMNASIUM
	Entity* gymnasiumNodeEntity = m_sceneManager->createEntity("GymnasiumNodeEntity", "gymnasiumNode.mesh");
	gymnasiumNodeEntity->setCastShadows(false);
	gymnasiumNodeEntity->getSubEntity(0)->setMaterialName("gymnasium-surface06");
	gymnasiumNodeEntity->getSubEntity(1)->setMaterialName("gymnasium-surface10");
	gymnasiumNodeEntity->getSubEntity(2)->setMaterialName("gymnasium-surface09");
	gymnasiumNode->attachObject(gymnasiumNodeEntity);

	// GYM PUB
	Entity* gymnasiumPublicityEntity = m_sceneManager->createEntity("GymnasiumPublicity", "gymnasium-publicity.mesh");
	gymnasiumPublicityEntity->setCastShadows(false);
	gymnasiumPublicityEntity->getSubEntity(0)->setMaterialName("gymnasium-surface11");
	gymnasiumPublicityEntity->getSubEntity(1)->setMaterialName("gymnasium-surface12");
	gymnasiumNode->attachObject(gymnasiumPublicityEntity);

	//DOORS
	SceneNode* door01Node = gymnasiumNode->createChildSceneNode("door-01-node");
	door01Node->setPosition(26, 1, -24);
	door01Node->setOrientation(0.923879F, 0, -0.382684F, 0);
	Entity* door01Entity = m_sceneManager->createEntity("door-01-entity", "door-01.mesh");
	door01Entity->setCastShadows(false);
	door01Entity->getSubEntity(0)->setMaterialName("gymnasium-surface07");
	door01Entity->getSubEntity(1)->setMaterialName("gymnasium-surface08");
	door01Node->attachObject(door01Entity);

	SceneNode* door02Node = gymnasiumNode->createChildSceneNode("door-02-node");
	door02Node->setPosition(26, 1, 4);
	door02Node->setOrientation(0.923879F, 0, 0.382684F, 0);
	Entity* door02Entity = m_sceneManager->createEntity("door-02-entity", "door-02.mesh");
	door02Entity->setCastShadows(false);
	door02Entity->getSubEntity(0)->setMaterialName("gymnasium-surface07");
	door02Entity->getSubEntity(1)->setMaterialName("gymnasium-surface08");
	door02Node->attachObject(door02Entity);

	SceneNode* door03Node = gymnasiumNode->createChildSceneNode("door-03-node");
	door03Node->setPosition(-26, 2.5, 4);
	door03Node->setOrientation(0.923879F, 0, -0.382684F, 0);
	Entity* door03Entity = m_sceneManager->createEntity("door-03-entity", "door-03.mesh");
	door03Entity->setCastShadows(false);
	door03Entity->getSubEntity(0)->setMaterialName("gymnasium-surface08");
	door03Entity->getSubEntity(1)->setMaterialName("gymnasium-surface07");
	door03Node->attachObject(door03Entity);

	SceneNode* door04Node = gymnasiumNode->createChildSceneNode("door-04-node");
	door04Node->setPosition(-26, 1, -24);
	door04Node->setOrientation(0.923879F, 0, 0.382684F, 0);
	Entity* door04Entity = m_sceneManager->createEntity("door-04-entity", "door-04.mesh");
	door04Entity->setCastShadows(false);
	door04Entity->getSubEntity(0)->setMaterialName("gymnasium-surface07");
	door04Entity->getSubEntity(1)->setMaterialName("gymnasium-surface08");
	door04Node->attachObject(door04Entity);

	//TABLE
	SceneNode* tableNode = gymnasiumNode->createChildSceneNode("Table");
	tableNode->setPosition(-2.475F, 0.36F, -21.5F);
	tableNode->setOrientation(0.707105F, 0.F, 0.707108F, 0.F);
	Entity* tableEntity = m_sceneManager->createEntity("Table", "table.mesh");
	tableEntity->setCastShadows(false);
	tableEntity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsor01");
	tableEntity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsorBunrakuM2S01");
	tableEntity->getSubEntity(2)->setMaterialName("gymnasium-surface13");
	tableNode->attachObject(tableEntity);


	//TRANSFORM FROM demo.omk
	gymnasiumPivot->translate(-10, 0, 0);
	gymnasiumPivot->rotate(Vector3::UNIT_Y, Radian(-1.570796327F));
}

void CHandballBCI::initSceneAds() const
{
	SceneNode* gymnasiumNode = m_sceneManager->getSceneNode("GymnasiumNode");
	// WALL Ads
	Entity* wallPublicity01Entity = m_sceneManager->createEntity("WallPublicity01", "wall-publicity-01.mesh");
	wallPublicity01Entity->setCastShadows(false);
	wallPublicity01Entity->getSubEntity(0)->setMaterialName("gymnasium-surface14");
	gymnasiumNode->attachObject(wallPublicity01Entity);

	Entity* wallPublicity02Entity = m_sceneManager->createEntity("WallPublicity02", "wall-publicity-02.mesh");
	wallPublicity02Entity->setCastShadows(false);
	wallPublicity02Entity->getSubEntity(0)->setMaterialName("gymnasium-surface15");
	gymnasiumNode->attachObject(wallPublicity02Entity);

	Entity* wallPublicity03Entity = m_sceneManager->createEntity("WallPublicity03", "wall-publicity-03.mesh");
	wallPublicity03Entity->setCastShadows(false);
	wallPublicity03Entity->getSubEntity(0)->setMaterialName("gymnasium-surface02");
	gymnasiumNode->attachObject(wallPublicity03Entity);

	Entity* wallPublicity04Entity = m_sceneManager->createEntity("WallPublicity04", "wall-publicity-04.mesh");
	wallPublicity04Entity->setCastShadows(false);
	wallPublicity04Entity->getSubEntity(0)->setMaterialName("gymnasium-surface03");
	gymnasiumNode->attachObject(wallPublicity04Entity);

	Entity* wallPublicity06Entity = m_sceneManager->createEntity("WallPublicity06", "wall-publicity-06.mesh");
	wallPublicity06Entity->setCastShadows(false);
	wallPublicity06Entity->getSubEntity(0)->setMaterialName("gymnasium-surface04");
	gymnasiumNode->attachObject(wallPublicity06Entity);

	Entity* wallPublicity07Entity = m_sceneManager->createEntity("WallPublicity07", "wall-publicity-07.mesh");
	wallPublicity07Entity->setCastShadows(false);
	wallPublicity07Entity->getSubEntity(0)->setMaterialName("gymnasium-surface05");
	gymnasiumNode->attachObject(wallPublicity07Entity);

	Entity* wallPublicity08Entity = m_sceneManager->createEntity("WallPublicity08", "wall-publicity-08.mesh");
	wallPublicity08Entity->setCastShadows(false);
	wallPublicity08Entity->getSubEntity(0)->setMaterialName("gymnasium-surface14");
	gymnasiumNode->attachObject(wallPublicity08Entity);

	Entity* wallPublicity09Entity = m_sceneManager->createEntity("WallPublicity09", "wall-publicity-09.mesh");
	wallPublicity09Entity->setCastShadows(false);
	wallPublicity09Entity->getSubEntity(0)->setMaterialName("gymnasium-surface14");
	gymnasiumNode->attachObject(wallPublicity09Entity);

	Entity* wallPublicity10Entity = m_sceneManager->createEntity("WallPublicity10", "wall-publicity-10.mesh");
	wallPublicity10Entity->setCastShadows(false);
	wallPublicity10Entity->getSubEntity(0)->setMaterialName("gymnasium-surface14");
	gymnasiumNode->attachObject(wallPublicity10Entity);

	Entity* wallPublicity11Entity = m_sceneManager->createEntity("WallPublicity11", "wall-publicity-11.mesh");
	wallPublicity11Entity->setCastShadows(false);
	wallPublicity11Entity->getSubEntity(0)->setMaterialName("gymnasium-surface14");
	gymnasiumNode->attachObject(wallPublicity11Entity);

	Entity* wallPublicity12Entity = m_sceneManager->createEntity("WallPublicity12", "wall-publicity-12.mesh");
	wallPublicity12Entity->setCastShadows(false);
	wallPublicity12Entity->getSubEntity(0)->setMaterialName("gymnasium-surface14");
	gymnasiumNode->attachObject(wallPublicity12Entity);

	Entity* wallPublicity13Entity = m_sceneManager->createEntity("WallPublicity13", "wall-publicity-13.mesh");
	wallPublicity13Entity->setCastShadows(false);
	wallPublicity13Entity->getSubEntity(0)->setMaterialName("gymnasium-surface14");
	gymnasiumNode->attachObject(wallPublicity13Entity);

	// more ads
	SceneNode* publicity01Node = gymnasiumNode->createChildSceneNode("publicity-01-node");
	publicity01Node->setPosition(-22.5F, 0.01F, -10.F);
	publicity01Node->setOrientation(-3.65178e-006F, 0.F, 1.F, 0.F);
	Entity* publicity01Entity = m_sceneManager->createEntity("publicity-01-entity", "publicity-01.mesh");
	publicity01Entity->setCastShadows(false);
	publicity01Entity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsorBunrakuM2S01");
	publicity01Entity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsor01");
	publicity01Node->attachObject(publicity01Entity);

	SceneNode* publicity02Node = gymnasiumNode->createChildSceneNode("publicity-02-node");
	publicity02Node->setPosition(-22.5F, 0.01F, -10.F);
	publicity02Node->setOrientation(0.707105F, 0.707108F, 0.F, 0.F);
	Entity* publicity02Entity = m_sceneManager->createEntity("publicity-02-entity", "publicity-02.mesh");
	publicity02Entity->setCastShadows(false);
	publicity02Entity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsor01");
	publicity02Entity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsorBunrakuM2S01");
	publicity02Node->attachObject(publicity02Entity);

	SceneNode* publicity03Node = gymnasiumNode->createChildSceneNode("publicity-03-node");
	publicity03Node->setPosition(13.75F, 0.36F, 1.5F);
	publicity03Node->setOrientation(0.707105F, 0.F, -0.707108F, 0.F);
	Entity* publicity03Entity = m_sceneManager->createEntity("publicity-03-entity", "publicity-03.mesh");
	publicity03Entity->setCastShadows(false);
	publicity03Entity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsor01");
	publicity03Entity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsorIrisa01");
	publicity03Node->attachObject(publicity03Entity);

	SceneNode* publicity04Node = gymnasiumNode->createChildSceneNode("publicity-04-node");
	publicity04Node->setPosition(13.75F, 0.36F, 1.5F);
	publicity04Node->setOrientation(0.707105F, 0.F, -0.707108F, 0.F);
	Entity* publicity04Entity = m_sceneManager->createEntity("publicity-04-entity", "publicity-04.mesh");
	publicity04Entity->setCastShadows(false);
	publicity04Entity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsor01");
	publicity04Entity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsorIrisa01");
	publicity04Node->attachObject(publicity04Entity);

	SceneNode* publicity05Node = gymnasiumNode->createChildSceneNode("publicity-05-node");
	publicity05Node->setPosition(13.75F, 0.36F, -21.5F);
	publicity05Node->setOrientation(0.707105F, 0.F, 0.707108F, 0.F);
	Entity* publicity05Entity = m_sceneManager->createEntity("publicity-05-entity", "publicity-05.mesh");
	publicity05Entity->setCastShadows(false);
	publicity05Entity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsor01");
	publicity05Entity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsorIrisa01");
	publicity05Node->attachObject(publicity05Entity);

	SceneNode* publicity06Node = gymnasiumNode->createChildSceneNode("publicity-06-node");
	publicity06Node->setPosition(-13.75F, 0.01F, -21.F);
	publicity06Node->setOrientation(0.707105F, 0.F, 0.707108F, 0.F);
	Entity* publicity06Entity = m_sceneManager->createEntity("publicity-06-entity", "publicity-06.mesh");
	publicity06Entity->setCastShadows(false);
	publicity06Entity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsor01");
	publicity06Entity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsorIrisa01");
	publicity06Node->attachObject(publicity06Entity);

	SceneNode* publicity07Node = gymnasiumNode->createChildSceneNode("publicity-07-node");
	publicity07Node->setPosition(-1.96695e-006F, 0.71F, 1.5F);
	publicity07Node->setOrientation(0.5F, 0.5F, -0.5F, 0.5F);
	Entity* publicity07Entity = m_sceneManager->createEntity("publicity-07-entity", "publicity-07.mesh");
	publicity07Entity->setCastShadows(false);
	publicity07Entity->getSubEntity(0)->setMaterialName("gymnasium-MaterialSponsorIrisa01");
	publicity07Entity->getSubEntity(1)->setMaterialName("gymnasium-MaterialSponsor01");
	publicity07Node->attachObject(publicity07Entity);
}

void CHandballBCI::initSceneGoals() const
{
	//----------- LEFT GOAL -------------//
	SceneNode* leftGoalPivot = m_sceneManager->getRootSceneNode()->createChildSceneNode("LeftGoalPivot");
	SceneNode* leftGoal      = leftGoalPivot->createChildSceneNode("LeftGoal");
	SceneNode* leftGoalNode  = leftGoal->createChildSceneNode("LeftGoalNode");

	Entity* leftGoalEntity = m_sceneManager->createEntity("LgoalNode", "goalNode.mesh");
	leftGoalEntity->setCastShadows(true);
	leftGoalEntity->getSubEntity(0)->setMaterialName("goal-surface02");
	leftGoalEntity->getSubEntity(1)->setMaterialName("goal-surface01");
	leftGoalNode->attachObject(leftGoalEntity);

	Entity* leftGoalBorderEntity = m_sceneManager->createEntity("Lgoal-border", "goal-border.mesh");
	leftGoalBorderEntity->setCastShadows(true);
	leftGoalBorderEntity->getSubEntity(0)->setMaterialName("goal-frontcol01");
	leftGoalNode->attachObject(leftGoalBorderEntity);

	Entity* leftGoalNetEntity = m_sceneManager->createEntity("Lgoal-net", "goal-net.mesh");
	leftGoalNetEntity->setCastShadows(true);
	leftGoalNetEntity->getSubEntity(0)->setMaterialName("goal-foregrou01");
	leftGoalNode->attachObject(leftGoalNetEntity);

	//TRANSFORM FROM demo.omk
	leftGoalPivot->translate(0, 0, -5.92F);
	leftGoalPivot->rotate(Vector3::UNIT_Y, Radian(-1.570796327F));

	//----------- RIGHT GOAL -------------//
	SceneNode* rightGoalPivot = m_sceneManager->getRootSceneNode()->createChildSceneNode("RightGoalPivot");
	SceneNode* rightGoal      = rightGoalPivot->createChildSceneNode("RightGoal");
	SceneNode* rightGoalNode  = rightGoal->createChildSceneNode("RightGoalNode");

	Entity* rightGoalEntity = m_sceneManager->createEntity("RgoalNode", "goalNode.mesh");
	rightGoalEntity->setCastShadows(true);
	rightGoalEntity->getSubEntity(0)->setMaterialName("goal-surface02");
	rightGoalEntity->getSubEntity(1)->setMaterialName("goal-surface01");
	rightGoalNode->attachObject(rightGoalEntity);

	Entity* rightGoalBorderEntity = m_sceneManager->createEntity("Rgoal-border", "goal-border.mesh");
	rightGoalBorderEntity->setCastShadows(true);
	rightGoalBorderEntity->getSubEntity(0)->setMaterialName("goal-frontcol01");
	rightGoalNode->attachObject(rightGoalBorderEntity);

	Entity* rightGoalNetEntity = m_sceneManager->createEntity("Rgoal-net", "goal-net.mesh");
	rightGoalNetEntity->setCastShadows(true);
	rightGoalNetEntity->getSubEntity(0)->setMaterialName("goal-foregrou01");
	rightGoalNode->attachObject(rightGoalNetEntity);

	//TRANSFORM FROM demo.omk
	rightGoalPivot->translate(0, 0, 5.92F);
	rightGoalPivot->rotate(Vector3::UNIT_Y, Radian(1.570796327F));
}

void CHandballBCI::initSceneBalls() const
{
	//----------- ACTIVE BALL -------------//
	SceneNode* ballCommonNode = m_sceneManager->getRootSceneNode()->createChildSceneNode("BallCommonNode");

	SceneNode* activeBallPivot = ballCommonNode->createChildSceneNode("ActiveBallPivot");
	SceneNode* activeBall      = activeBallPivot->createChildSceneNode("ActiveBall");
	SceneNode* activeBallNode  = activeBall->createChildSceneNode("ActiveBallNode");

	Entity* activeBallEntity = m_sceneManager->createEntity("active-ball", "active-ballNode.mesh");
	activeBallEntity->setCastShadows(true);
	activeBallEntity->getSubEntity(0)->setMaterialName("active-ball-active-ball-material");
	activeBallNode->attachObject(activeBallEntity);

	//----------- GetReady BALL -------------//
	SceneNode* getReadyBallPivot = ballCommonNode->createChildSceneNode("GetReadyBallPivot");
	SceneNode* getReadyBall      = getReadyBallPivot->createChildSceneNode("GetReadyBall");
	SceneNode* getReadyBallNode  = getReadyBall->createChildSceneNode("GetReadyBallNode");

	Entity* getReadyBallEntity = m_sceneManager->createEntity("get-ready-ball", "get-ready-ballNode.mesh");
	getReadyBallEntity->setCastShadows(true);
	getReadyBallEntity->getSubEntity(0)->setMaterialName("get-ready-ball-get-ready-ball-material");
	getReadyBallNode->attachObject(getReadyBallEntity);

	//----------- Passive BALL -------------//
	SceneNode* passiveBallPivot = ballCommonNode->createChildSceneNode("PassiveBallPivot");
	SceneNode* passiveBall      = passiveBallPivot->createChildSceneNode("PassiveBall");
	SceneNode* passiveBallNode  = passiveBall->createChildSceneNode("PassiveBallNode");

	Entity* passiveBallEntity = m_sceneManager->createEntity("passive-ball", "passive-ballNode.mesh");
	passiveBallEntity->setCastShadows(true);
	passiveBallEntity->getSubEntity(0)->setMaterialName("passive-ball-surface01");
	passiveBallNode->attachObject(passiveBallEntity);

	ballCommonNode->translate(0, passiveBallEntity->getBoundingRadius(), 0);

	getReadyBallPivot->setVisible(false);
	passiveBallPivot->setVisible(true);
	activeBallPivot->setVisible(false);
}

void CHandballBCI::initSceneCrossArrowAndFeedback()
{
	//----------- CROSS -------------//
	SceneNode* crossCommonNode = m_sceneManager->getRootSceneNode()->createChildSceneNode("CrossCommonNode");

	SceneNode* crossNode = crossCommonNode->createChildSceneNode("CrossNode");
	Entity* crossEntity  = m_sceneManager->createEntity("cross", "crossNode.mesh");
	crossEntity->setCastShadows(false);
	crossEntity->getSubEntity(0)->setMaterialName("cross-cross-material");
	crossNode->attachObject(crossEntity);
	m_crossSize = crossEntity->getBoundingRadius() * 2;
	crossNode->setVisible(false);

	//----------- RIGHT ARROW -------------//
	SceneNode* rArrowNode = crossCommonNode->createChildSceneNode("RightArrowNode");
	Entity* rArrowEntity  = m_sceneManager->createEntity("rightArrow", "basic-arrowNode.mesh");
	rArrowEntity->setCastShadows(false);
	rArrowEntity->getSubEntity(0)->setMaterialName("basic-arrow-basic-arrow-material");
	rArrowNode->attachObject(rArrowEntity);
	rArrowNode->roll(Radian(Math::PI));
	rArrowNode->translate(0, 0, 0.001F);
	rArrowNode->setVisible(false);

	//----------- LEFT ARROW -------------//
	SceneNode* lArrowNode = crossCommonNode->createChildSceneNode("LeftArrowNode");
	Entity* lArrowEntity  = m_sceneManager->createEntity("leftArrow", "basic-arrowNode.mesh");
	lArrowEntity->setCastShadows(false);
	lArrowEntity->getSubEntity(0)->setMaterialName("basic-arrow-basic-arrow-material");
	lArrowNode->attachObject(lArrowEntity);
	lArrowNode->translate(0, 0, 0.001F);
	lArrowNode->setVisible(false);

	crossCommonNode->yaw(Radian(Math::PI / 2.F));
	crossCommonNode->roll(Radian(Math::PI / 2.F));
	crossCommonNode->translate(3.5F, 0.20F, 0.F);
	crossCommonNode->setScale(0.35F, 0.35F, 0.35F);

	//----------- FEEDBACK -------------//
	SceneNode* feedbackNode = m_sceneManager->getRootSceneNode()->createChildSceneNode("FeedbackNode");
	Entity* feedbackEntity  = m_sceneManager->createEntity("feedback", "plane-cyanNode.mesh");
	feedbackEntity->setCastShadows(false);
	feedbackEntity->getSubEntity(0)->setMaterialName("plane-cyan-plane-material");
	feedbackNode->attachObject(feedbackEntity);
	feedbackNode->roll(Radian(-Math::PI / 2.F));
	feedbackNode->setScale(0.25F, 0.25F, 2.59F);
	feedbackNode->setPosition(3.499F, 0.20F, -m_crossSize * feedbackNode->getScale().z / 2);

	feedbackNode->setVisible(false);
}

#endif
