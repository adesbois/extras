#pragma once

#include "../ovavrdCOgreVRApplication.h"

namespace VRDemos {

/**
 * \class CHandballBCI
 * \author Laurent Bonnet (INRIA/IRISA)
 * \date 2010-02-16
 * \brief Ogre application for the Handball application.
 *
 * \details The HandballBCI application is based on motor imagery of the hands. 
 * The 3d scene is a gymnasium, with 2 handball goals.
 * The subject has to move his right/left hand to move a ball in the right/left goal.
 * TODO: full documentation.
 */
class CHandballBCI final : public COgreVRApplication
{
public:

	CHandballBCI();

private:

	enum EPhase { Phase_GetReady, Phase_Active, Phase_Rest, };

	enum EMark { Mark_Right, Mark_Left, Mark_None, };

	bool initialise() override;
	void initSceneGymnasium() const;
	void initSceneAds() const;
	void initSceneGoals() const;
	void initSceneBalls() const;
	void initSceneCrossArrowAndFeedback();

	//virtual bool initGUI();

	bool process(double timeSinceLastProcess) override;

	int m_goalScore  = 0;      // goal !
	int m_leftScore  = 0;      // ball successfully sent on the left side (but maybe no goal)
	int m_rightScore = 0;     // ball successfully sent on the left side (but maybe no goal)

	//for stats
	int m_nTrialRight = 0;
	int m_nTrialLeft  = 0;

	int m_classificationScore = 0;

	int m_phase     = 0;
	int m_lastPhase = 0;

	double m_feedback     = 0;
	double m_lastFeedback = 0;

	double m_maxFeedback = 0;
	double m_minFeedback = 0;
	std::list<double> m_analogHistories;

	int m_mark                   = 0;
	int m_lastMark               = 0;
	bool m_goalMarkedAtThisPhase = false;

	float m_ballSpeed        = 0;
	float m_ballPosition     = 0;		// n.b. The coordinate system of this seems to be inverted wrt the [-left,+right] convention
	float m_lastBallPosition = 0;
	float m_ballOrientation  = 0;

	float m_crossSize = 0;

	bool m_showCross    = false;
	bool m_showClue     = false;
	bool m_showFeedback = false;
};
}  // namespace VRDemos
