#pragma once

class CDeviceInfo;

#include <list>
#include <string>

namespace VRDemos {
/**
 * \class CAbstractVrpnPeripheral
 * \author Laurent Bonnet (INRIA/IRISA)
 * \date 2010-02-16
 * \brief A VRPN peripheral abstraction, client side.
 *
 * \details The CAbstractVrpnPeripheral handles the connection to at most one Analog Server and one Button Server.
 * 
 */
class CAbstractVrpnPeripheral final
{
public:

	std::list<std::pair<int, int>> m_Buttons;					///< list (chronological) of pairs (button_id, button_state).
	std::list<std::list<double>> m_Analogs;						///< list (chronological) of list (channels) of values.
	double m_AnalogScale  = 1;									///< Scalar applied to any value read on the Analog server.
	double m_AnalogOffset = 0;									///< Offset applied to any value read on the Analog server.

	std::string m_Address = "openvibe-vrpn@localhost";			///< The device address ([peripheral-name]@[hostname]).
	CDeviceInfo* m_Device = nullptr;							///< The VRPN Device

	/**
	* \brief Default constructor.
	*/
	CAbstractVrpnPeripheral() {}
	/**
	* \brief Constructor.
	* \param adress The device address.
	*/
	explicit CAbstractVrpnPeripheral(const std::string& adress) { m_Address = adress; }
	/**
	* \brief Destructor.
	*/
	~CAbstractVrpnPeripheral();

	/**
	* \brief Initialize the VRPN Remote object and handlers.
	*/
	void init();

	/**
	* \brief Main loop.
	*/
	void loop() const;
};
}	// namespace VRDemos
