#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovavrdCOgreVRApplication.h"

#include <system/ovCTime.h>
#include <fs/Files.h>

#include <iostream>

#include <vrpn_Tracker.h>

#include <openvibe/ov_directories.h>

#if defined sleep
#undef sleep
#endif

using namespace VRDemos;
using namespace Ogre;

static const float ROTATION_SPEED_MOUSE = 0.5F;
static const float TRANSLATION_SPEED    = 0.2F;

COgreVRApplication::~COgreVRApplication()
{
	if (m_sceneManager) { m_sceneManager->clearScene(); }// does not destroy cameras 
	delete m_vrpnPeripheral;
	delete m_camera;
	delete m_window;

	if (m_inputManager)
	{
		if (m_mouse)
		{
			m_inputManager->destroyInputObject(m_mouse);
			m_mouse = nullptr;
		}

		if (m_keyboard)
		{
			m_inputManager->destroyInputObject(m_keyboard);
			m_keyboard = nullptr;
		}

		m_inputManager->destroyInputSystem(m_inputManager);
	}

	if (m_guiRenderer) { CEGUI::OgreRenderer::destroySystem(); }
}

void COgreVRApplication::go()
{
	if (!this->setup())
	{
		std::cerr << "[FAILED] Setup failed, end of program." << std::endl;
		return;
	}

	this->initialise();

	std::cout << std::endl << "START RENDERING..." << std::endl;

	m_root->startRendering();
}

bool COgreVRApplication::setup()
{
	// Plugin config path setup
	String pluginsPath;
#if defined TARGET_OS_Windows
#if defined TARGET_BUILDTYPE_Debug
	pluginsPath = std::string(getenv("OGRE_HOME")) + std::string("/bin/debug/plugins_d.cfg");
#else
	pluginsPath = std::string(getenv("OGRE_HOME")) + std::string("/bin/release/plugins.cfg");
#endif
#elif defined TARGET_OS_Linux
	pluginsPath = std::string(OpenViBE::Directories::getDataDir()) + std::string("/openvibe-ogre-plugins.cfg");
#else
	#error "failing text"
#endif

	// Create LogManager to stop Ogre flooding the console and creating random files
	const OpenViBE::CString ogreLog = OpenViBE::Directories::getLogDir() + "/openvibe-vr-demo-ogre.log";
	std::cout << "+ Ogre log will be in " << ogreLog << "\n";
	LogManager* logManagerSingleton = new LogManager();
	FS::Files::createParentPath(ogreLog);
	logManagerSingleton->createLog(ogreLog.toASCIIString(), true, false, false);

	// Root creation
	const OpenViBE::CString ogreCfg = OpenViBE::Directories::getUserDataDir() + "/openvibe-vr-demo-ogre.cfg";
	std::cout << "+ Ogre cfg will be in " << ogreCfg << "\n";
	m_root = new Root(pluginsPath, ogreCfg.toASCIIString(), ogreLog.toASCIIString());
	// Resource handling
	this->setupResources();
	//Configuration from file or dialog window if needed
	if (!this->configure())
	{
		std::cerr << "[FAILED] The configuration process ended unexpectedly." << std::endl;
		return false;
	}

	// load ressources
	ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	// Scene graph and rendering initialisation
	// m_sceneManager = m_root->createSceneManager("TerrainSceneManager", "DefaultSceneManager");
	m_sceneManager = m_root->createSceneManager(ST_GENERIC);

	//Camera
	m_camera     = m_sceneManager->createCamera("DefaultCamera");
	m_cameraNode = m_sceneManager->getRootSceneNode()->createChildSceneNode();
	m_cameraNode->attachObject(m_camera);
	//m_cameraNodeYawAndPos = m_sceneManager->getRootSceneNode()->createChildSceneNode();
	//m_cameraNodeYawAndPos->setPosition(Ogre::Vector3::ZERO);
	//m_cameraNodeYawAndPos->setOrientation(Ogre::Quaternion::IDENTITY);

	//m_cameraNodePitch = m_cameraNodeYawAndPos->createChildSceneNode();
	//m_cameraNodePitch->setPosition(Ogre::Vector3::ZERO);
	//m_cameraNodePitch->setOrientation(Ogre::Quaternion::IDENTITY);
	//
	//m_cameraNodePitch->attachObject(m_camera);

	m_camera->setNearClipDistance(0.05F);
	m_camera->setFarClipDistance(300.0F);
	m_camera->setRenderingDistance(0.01F);

	// Create one viewport, entire window
	Viewport* viewPort = m_window->addViewport(m_camera);
	viewPort->setBackgroundColour(ColourValue(0, 0, 0));
	// Alter the camera aspect ratio to match the viewport
	m_camera->setAspectRatio(Real(viewPort->getActualWidth()) / Real(viewPort->getActualHeight()));

	// Set default mipmap level (NB some APIs ignore this)
	TextureManager::getSingleton().setDefaultNumMipmaps(5);

	//Listening the frame rendering
	m_root->addFrameListener(this);

	//Listening the window events
	WindowEventUtilities::addWindowEventListener(m_window, this);

	//OIS
	this->initOIS();

	//CEGUI
	this->initCEGUI(OpenViBE::Directories::getLogDir() + "/openvibe-vr-demo-cegui.log");

	//VRPN
	const std::string vrpnAddress("openvibe-vrpn@localhost");
	m_vrpnPeripheral = new CAbstractVrpnPeripheral(vrpnAddress);
	m_vrpnPeripheral->init();

	std::cout << "Listening to VRPN address [" << vrpnAddress << "]\n";

	return true;
}

void COgreVRApplication::setupResources()
{
	// Load resource paths from config file
	ConfigFile configFile;
	configFile.load(m_resourcePath + "/resources.cfg");
	// Go through all sections & settings in the file
	ConfigFile::SectionIterator it = configFile.getSectionIterator();

	while (it.hasMoreElements())
	{
		String secName                         = it.peekNextKey();
		ConfigFile::SettingsMultiMap* settings = it.getNext();
		for (auto i = settings->begin(); i != settings->end(); ++i)
		{
			String typeName = i->first;
			String archName = i->second;
			ResourceGroupManager::getSingleton().addResourceLocation(m_resourcePath + "/" + archName, typeName, secName);
		}
	}
}

bool COgreVRApplication::configure()
{
	if (! m_root->restoreConfig())
	{
		if (! m_root->showConfigDialog())
		{
			std::cerr << "[FAILED] No configuration created from the dialog window." << std::endl;
			return false;
		}
	}

	m_window = m_root->initialise(true, "VR Application - powered by OpenViBE");

	return true;
}

bool COgreVRApplication::initOIS()
{
	OIS::ParamList paramList;
	size_t windowHnd = 0;
	std::ostringstream windowHndStr;

	// Retrieve the rendering window
	RenderWindow* window = Root::getSingleton().getAutoCreatedWindow();
	window->getCustomAttribute("WINDOW", &windowHnd);
	windowHndStr << windowHnd;
	paramList.insert(make_pair(std::string("WINDOW"), windowHndStr.str()));
	paramList.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
	paramList.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));

	// Create the input manager
	m_inputManager = OIS::InputManager::createInputSystem(paramList);

	//Create all devices
	m_keyboard = dynamic_cast<OIS::Keyboard*>(m_inputManager->createInputObject(OIS::OISKeyboard, true));
	m_mouse    = dynamic_cast<OIS::Mouse*>(m_inputManager->createInputObject(OIS::OISMouse, true));

	m_keyboard->setEventCallback(this);
	m_mouse->setEventCallback(this);

	std::cout << "OIS initialised" << std::endl;

	return true;
}

bool COgreVRApplication::initCEGUI(const char* logFilename)
{
	// Instantiate logger before bootstrapping the system, this way we will be able to get the log redirected
	if (!CEGUI::Logger::getSingletonPtr())
	{
		new CEGUI::DefaultLogger();		// Singleton, instantiate only, no delete
	}
	std::cout << "+ CEGUI log will be in " << logFilename << "\n";
	FS::Files::createParentPath(logFilename);
	try { CEGUI::Logger::getSingleton().setLogFilename(logFilename, false); }
	catch (char* error) { std::cout << "  CEGUI::getSingleton() exception: " << error << "\n"; }
	//catch (const char* error) { std::cout << "  CEGUI::getSingleTon() exception: " << error << "\n"; }

	m_guiRenderer = &(CEGUI::OgreRenderer::bootstrapSystem(*m_window));

#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	CEGUI::SchemeManager::getSingleton().createFromFile(const_cast<CEGUI::utf8*>(reinterpret_cast<const CEGUI::utf8*>("TaharezLook-ov-0.8.scheme")));
	m_guiManager = CEGUI::WindowManager::getSingletonPtr();
	m_sheet      = m_guiManager->createWindow("DefaultWindow", "root");
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(m_sheet);
#else
	CEGUI::SchemeManager::getSingleton().create((CEGUI::utf8*)"TaharezLook-ov.scheme");
	m_guiManager = CEGUI::WindowManager::getSingletonPtr();
	m_sheet      = m_guiManager->createWindow("DefaultGUISheet", "Sheet");
	CEGUI::System::getSingleton().setGUISheet(m_sheet);
#endif

	return true;
}


//--------------------------------------------------------------

bool COgreVRApplication::keyPressed(const OIS::KeyEvent& evt)
{
	if (evt.key == OIS::KC_ESCAPE)
	{
		std::cout << "[ESC] pressed, user termination." << std::endl;
		m_bContinue = false;
	}
	if (evt.key == OIS::KC_RCONTROL)
	{
		std::cout << "Camera mode ON" << std::endl;
		m_bCameraMode = true;
	}


	if (evt.key == OIS::KC_UP) { m_keysPressed[OIS::KC_UP] = true; }
	if (evt.key == OIS::KC_RIGHT) { m_keysPressed[OIS::KC_RIGHT] = true; }
	if (evt.key == OIS::KC_LEFT) { m_keysPressed[OIS::KC_LEFT] = true; }
	if (evt.key == OIS::KC_DOWN) { m_keysPressed[OIS::KC_DOWN] = true; }

	return true;
}


bool COgreVRApplication::keyReleased(const OIS::KeyEvent& evt)
{
	if (evt.key == OIS::KC_RCONTROL)
	{
		std::cout << "Camera mode OFF" << std::endl;
		m_bCameraMode = false;
	}

	if (evt.key == OIS::KC_UP) { m_keysPressed[OIS::KC_UP] = false; }
	if (evt.key == OIS::KC_RIGHT) { m_keysPressed[OIS::KC_RIGHT] = false; }
	if (evt.key == OIS::KC_LEFT) { m_keysPressed[OIS::KC_LEFT] = false; }
	if (evt.key == OIS::KC_DOWN) { m_keysPressed[OIS::KC_DOWN] = false; }


	return true;
}

bool COgreVRApplication::mouseMoved(const OIS::MouseEvent& arg)
{
	if (m_bCameraMode)
	{
		m_camera->yaw(Degree(-float(arg.state.X.rel) * ROTATION_SPEED_MOUSE));
		m_camera->pitch(Degree(-float(arg.state.Y.rel) * ROTATION_SPEED_MOUSE));
	}

	return true;
}

void COgreVRApplication::updateCamera()
{
	Vector3 translation(0, 0, 0);
	if (m_keysPressed[OIS::KC_UP]) { translation.z -= TRANSLATION_SPEED; }
	if (m_keysPressed[OIS::KC_RIGHT]) { translation.x += TRANSLATION_SPEED; }
	if (m_keysPressed[OIS::KC_LEFT]) { translation.x -= TRANSLATION_SPEED; }
	if (m_keysPressed[OIS::KC_DOWN]) { translation.z += TRANSLATION_SPEED; }
	const Vector3 vectorFinal = m_camera->getDerivedOrientation() * translation;
	m_cameraNode->translate(vectorFinal, Node::TS_WORLD);
}

bool COgreVRApplication::frameStarted(const FrameEvent& evt)
{
	m_clock += evt.timeSinceLastFrame;
	if (m_clock >= 1 / MAX_FREQUENCY)
	{
		m_keyboard->capture();
		m_mouse->capture();

		m_vrpnPeripheral->loop();
		//the button states are added in the peripheric, but they have to be popped.
		//the basic class does not pop the states.

		if (m_bCameraMode) { this->updateCamera(); }

		m_bContinue = this->process(m_clock);
		m_clock -= 1 / MAX_FREQUENCY;
	}
	else { System::Time::sleep(1); }

	return m_bContinue;
}

void COgreVRApplication::windowResized(RenderWindow* rw)
{
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	CEGUI::System::getSingleton().notifyDisplaySizeChanged(CEGUI::Sizef(float(rw->getWidth()), float(rw->getHeight())));
#else
	CEGUI::System::getSingleton().notifyDisplaySizeChanged(CEGUI::Size(float(rw->getWidth()), float(rw->getHeight())));
#endif
}

#endif
