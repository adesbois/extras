#pragma once

#include "ova_defines.h"
#include <Ogre.h>
#include <OIS.h>
#include <CEGUI.h>
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#else
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#endif

#include "ovaCAbstractVrpnPeripheral.h"

#include <map>

#define MAX_FREQUENCY 60.f

/**
 * \namespace VRDemos
 * \author Laurent Bonnet (INRIA/IRISA)
 * \date 2010-02-16
 * \brief Contains all VR-demos related code (Ogre application and VRPN peripheral).
 */
namespace VRDemos {

/**
 * \class COgreVRApplication
 * \author Laurent Bonnet (INRIA/IRISA)
 * \date 2010-02-16
 * \brief Basic framework to design VR applications.
 *
 * \details This class is based on Ogre3D, and should be derived to design a specific VR application.
 * It handles the rendering engine setup and configuration. Developer has to initialise the right 3D scene,
 * and give the loop code that will be called each time a frame is rendered.
 */
class COgreVRApplication : public Ogre::FrameListener, OIS::KeyListener, OIS::MouseListener, Ogre::WindowEventListener
{
public:

	/**
	* \brief Default constructor.
	*/
	explicit COgreVRApplication(const char* root) : m_resourcePath(root) { }

	/**
	* \brief Destructor.
	*/
	~COgreVRApplication() override;

	/**
	* \brief Launches the application (engine setup, initialization, rendering start).
	*/
	virtual void go();

protected:
	/**
	* \brief Main Ogre and custom setup steps
	* \return \em true if the setup is a success.
	*/
	virtual bool setup();

	/**
	* \brief Configuration management
	* \return \em true if the configuration step is a success.
	*/
	virtual bool configure();

	/**
	* \brief Parsing of the resource configuration file
	*/
	virtual void setupResources();

	bool m_bContinue = true; ///< Tells if the rendering process should continue after the current frame.

	//----- Main Ogre objects ------//
	Ogre::Root* m_root                 = nullptr;	///< Ogre root.
	Ogre::SceneManager* m_sceneManager = nullptr;	///< The scene manager used.
	Ogre::RenderWindow* m_window       = nullptr;	///< The render window used.
	Ogre::Camera* m_camera             = nullptr;	///< The camera used.
	Ogre::SceneNode* m_cameraNode      = nullptr;	///< The camera node.

	Ogre::String m_resourcePath;					///< Path to the file resource.cfg for Ogre.

	bool m_bCameraMode = false;
	std::map<OIS::KeyCode, bool> m_keysPressed;
	virtual void updateCamera();

	/**
	* \brief Initialize the scene. This function has to be implemented
	* with the specific Ogre code.
	* \return \em true if the initialization process is a success.
	*/
	virtual bool initialise() = 0;
	/**
	* \brief Main loop. Developer has to implement this function
	* with the specific behaviour of its a application.
	* \return \em true if the configuration step is a success.
	*/
	virtual bool process(double timeSinceLastProcess) = 0;

	/**
	* \brief Frame started callback.
	*/
	bool frameStarted(const Ogre::FrameEvent& evt) override;

	/**
	* \brief Frame ended callback.
	* \return \em true if the rendering engine should continue.
	*/
	bool frameEnded(const Ogre::FrameEvent& /*evt*/) override { return true; }

	//-------OIS-------//
	OIS::InputManager* m_inputManager = nullptr;		///< The OIS input manager.
	OIS::Mouse* m_mouse               = nullptr;		///< The mouse.
	OIS::Keyboard* m_keyboard         = nullptr;		///< The keyboard.

	/**
	* \brief Initialize the OIS plugin.
	* \return \em true if the setup is a success.
	*/
	virtual bool initOIS();

	/**
	* \brief Mouse moved callback, launched when the mouse is moved.
	* \return \em true if the rendering engine should continue.
	*/
	bool mouseMoved(const OIS::MouseEvent& arg) override;

	/**
	* \brief Mouse pressed callback, launched when a mouse button is pressed.
	* \return \em true if the rendering engine should continue.
	*/
	bool mousePressed(const OIS::MouseEvent& /*arg*/, OIS::MouseButtonID /*id*/) override { return true; }

	/**
	* \brief Mouse released callback, launched when a mouse button is released.
	* \return \em true if the rendering engine should continue.
	*/
	bool mouseReleased(const OIS::MouseEvent& /*arg*/, OIS::MouseButtonID /*id*/) override { return true; }

	/**
	* \brief Key pressed callback, launched when a key is pressed.
	* \return \em true if the rendering engine should continue.
	*/
	bool keyPressed(const OIS::KeyEvent& evt) override;
	/**
	* \brief Key released callback, launched when a key is released.
	* \return \em true if the rendering engine should continue.
	*/
	bool keyReleased(const OIS::KeyEvent& evt) override;

	void windowResized(Ogre::RenderWindow* rw) override;

	//-------CEGUI-------//
	CEGUI::OgreRenderer* m_guiRenderer = nullptr;	///< The CEGUI renderer.
	//CEGUI::System& m_rGUISystem;					///< The CEGUI system.
	CEGUI::WindowManager* m_guiManager = nullptr;	///< The CEGUI window manager.
	CEGUI::Window* m_sheet             = nullptr;	///< The default sheet.

	/**
	* \brief Initialize the CEGUI plugin.
	* \return \em true if the setup is a success.
	*/
	virtual bool initCEGUI(const char* logFilename);

	//-------VRPN-------//
	CAbstractVrpnPeripheral* m_vrpnPeripheral = nullptr;///< A VRPN peripheric, handles at most one Analog + one Button server.

	//------CLOCK------//
	double m_clock = 0; ///< Clock to impose a maximum frequency.
};
}  // namespace VRDemos
