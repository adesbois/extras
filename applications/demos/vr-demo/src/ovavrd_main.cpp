#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovavrdCOgreVRApplication.h"

#include "HandballBCI/ovavrdCHandballBCI.h"
#include "SpaceshipBCI/ovavrdCSpaceshipBCI.h"

int main(const int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << "Syntax: " << argv[0] << " demo-name\n\n";
		std::cout << "where demo-name could be one of the following :\n";
		std::cout << "  - spaceship\n";
		std::cout << "  - handball\n";
		return 1;
	}

	VRDemos::COgreVRApplication* app;
	if (strcmp(argv[1], "spaceship") == 0)
	{
		std::string localization = "eng";
		if (argc != 3) { std::cout << "No language selected. Default is: english (eng).\n\n"; }
		else
		{
			localization = argv[2];
			std::cout << "User defined language: " << localization << "\n";
			std::cout << "WARNING: if the language keyword is not found, default language will be loaded (eng).\n\n";
		}
		std::cout << std::endl;


		std::cout << " ___     _.'\\   /'._     ___ \n";
		std::cout << "/  b__--- | .'\"'. | ---__d  \\\n";
		std::cout << "\\  p\"\"---_| '._.' |_---\"\"q  /\n";
		std::cout << "     / /   ./   \\.   \\ \\    \n";
		std::cout << "    / /     '---'     \\ \\   \n";
		std::cout << "   /_/                 \\_\\  \n";
		std::cout << "                             \n\n";
		std::cout << "Application started! Lift 'em!\n\n";

		app = new VRDemos::CSpaceshipBCI(localization);
	}
	else if (strcmp(argv[1], "handball") == 0)
	{
		std::cout << "Handball application started !\n";
		app = new VRDemos::CHandballBCI();
	}
	else
	{
		std::cout << "ERROR: the application specified does not exist (" << argv[1] << ").\n";
		std::cout << "Please use one of the following applications:\n";
		std::cout << "  - spaceship\n";
		std::cout << "  - handball\n";
		return 2;
	}

	app->go();
	delete app;

	return 0;
}

#else
#include <stdio.h>

int main(int argc, char** argv)
{
	std::cout << "VR demo has not been compiled as it depends on Ogre (missing/disabled)\n";
	return -1;
}
#endif
