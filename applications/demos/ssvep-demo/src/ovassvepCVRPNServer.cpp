#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCVRPNServer.h"
#include "ovassvepCApplication.h"

#include <vrpn_Connection.h>
#include <vrpn_Button.h>

using namespace SSVEP;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;

CVRPNServer* CVRPNServer::m_vrpnServerInstance = nullptr;
CApplication* CVRPNServer::m_application       = nullptr;

CVRPNServer::CVRPNServer(CApplication* application)
{
	m_application = application;

	const int port = int(m_application->getConfigurationManager()->expandAsInteger("${VRPN_ExternalServerPort}"));

	m_application->getLogManager() << LogLevel_Debug << "VRPN SERVER PORT :" << port << "\n";
	m_connection = vrpn_create_server_connection(port);
}

CVRPNServer* CVRPNServer::getInstance(CApplication* application)
{
	if (m_vrpnServerInstance == nullptr) { m_vrpnServerInstance = new CVRPNServer(application); }
	return m_vrpnServerInstance;
}

void CVRPNServer::addButton(const CString& name, const int buttonCount)
{
	m_buttonServer.insert(
		std::pair<std::string, vrpn_Button_Server*>(name.toASCIIString(), new vrpn_Button_Server(name.toASCIIString(), m_connection, buttonCount)));
	m_buttonCache[name.toASCIIString()].clear();
	m_buttonCache[name.toASCIIString()].resize(buttonCount);
}

void CVRPNServer::processFrame()
{
	for (auto& it : m_buttonServer) { it.second->mainloop(); }

	m_connection->mainloop();
}

void CVRPNServer::changeButtonState(const std::string& name, const int index, const int state)
{
	m_buttonServer[name]->set_button(index, state);
	m_buttonCache[name][index] = state;
}

int CVRPNServer::getButtonState(const std::string& name, const int index) { return m_buttonCache[name][index]; }


#endif
