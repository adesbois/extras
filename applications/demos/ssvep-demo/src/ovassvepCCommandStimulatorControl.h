#pragma once

#include "ovassvepICommandVRPNButton.h"

namespace SSVEP {
class CCommandStimulatorControl final : public ICommandVRPNButton
{
public:
	explicit CCommandStimulatorControl(CApplication* application);
	~CCommandStimulatorControl() override { }

	void execute(const int button, const int state) override;
};
}  // namespace SSVEP
