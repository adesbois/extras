#pragma once

#include "ovassvepICommand.h"

class vrpn_Button_Remote;

namespace SSVEP {
class ICommandVRPNButton : public ICommand
{
public:
	ICommandVRPNButton(CApplication* application, const OpenViBE::CString& name);
	~ICommandVRPNButton() override;

	void processFrame() override;
	virtual void execute(const int button, const int state) = 0;

protected:
	vrpn_Button_Remote* m_vrpnButton = nullptr;
};
}  // namespace SSVEP
