#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvep_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "ovassvepCApplication.h"
#include "Trainer/ovassvepCTrainerApplication.h"
#include "Shooter/ovassvepCShooterApplication.h"

using namespace OpenViBE;

int main(const int argc, char** argv)
{
	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " [trainer|shooter]\n";
		exit(1);
	}

	// initialize the OpenViBE kernel

	CKernelLoader kernelLoader;
	Kernel::IKernelDesc* kernelDesc = nullptr;

	CString error;
#if defined TARGET_OS_Windows
	const CString kernelFile = Directories::getLibDir() + "/openvibe-kernel.dll";
#else
	const CString kernelFile = Directories::getLibDir() + "/libopenvibe-kernel.so";
#endif
	if (!kernelLoader.load(kernelFile, &error))
	{
		std::cout << "[ FAILED ] Error loading kernel (" << error << ")" << " from [" << kernelFile << "]\n";
		return (1);
	}
	std::cout << "[  INF  ] Kernel module loaded, trying to get kernel descriptor\n";

	kernelLoader.initialize();
	kernelLoader.getKernelDesc(kernelDesc);

	if (!kernelDesc)
	{
		std::cout << "[ FAILED ] No kernel descriptor\n";
		return (1);
	}
	std::cout << "[  INF  ] Got kernel descriptor, trying to create kernel\n";

	Kernel::IKernelContext* kernelContext = kernelDesc->createKernel("ssvep-demo", Directories::getDataDir() + "/kernel/openvibe.conf");

	if (!kernelContext)
	{
		std::cout << "[ FAILED ] No kernel created by kernel descriptor\n";
		return (1);
	}
	kernelContext->initialize();

	Toolkit::initialize(*kernelContext);

	Kernel::IConfigurationManager* configManager = &(kernelContext->getConfigurationManager());
	configManager->createConfigurationToken("SSVEP_ApplicationDescriptor", CString(argv[1]));

	const CString configFile = configManager->expand("${Path_Data}/applications/ssvep-demo/openvibe-ssvep-demo.conf");
	configManager->addConfigurationFromFile(configFile);

	Kernel::ILogManager* logManager = &(kernelContext->getLogManager());

	if (configManager->expand("$Env{OGRE_HOME}").length() == 0)
	{
		std::cout << "Error: OGRE_HOME environment variable is not set, this likely will not work.\n";
		// Don't exit as we don't have any way to get the return value seen in designer
	}

	// We need this to address BuildType no longer in SDK 2.0
#if defined(DEBUG)
	configManager->addOrReplaceConfigurationToken("BuildType", "Debug");
#else
	configManager->addOrReplaceConfigurationToken("BuildType", "Release");
#endif

	SSVEP::CApplication* app;

	const CString applicationType = configManager->expand("${SSVEP_ApplicationType}");

	(*logManager) << Kernel::LogLevel_Info << "Selected Application : '" << applicationType.toASCIIString() << "'\n";


	if (applicationType == CString("trainer"))
	{
		(*logManager) << Kernel::LogLevel_Debug << "+ app = new SSVEP::CTrainerApplication(...)\n";
		app = new SSVEP::CTrainerApplication();
	}
	else if (applicationType == CString("shooter"))
	{
		(*logManager) << Kernel::LogLevel_Debug << "+ app = new SSVEP::CShooterApplication(...)\n";
		app = new SSVEP::CShooterApplication();
	}
	else
	{
		(*logManager) << Kernel::LogLevel_Error << "Wrong application identifier specified: '" << applicationType << "'\n";
		return 1;
	}

	if (!app->setup(kernelContext))
	{
		std::cout << "Aborting\n";
		delete app;
		return -1;
	}

	app->go();

	(*logManager) << Kernel::LogLevel_Debug << "- app\n";
	delete app;

	return 0;
}


#else
#include <stdio.h>

int main(int argc, char** argv)
{
	std::cout << "SSVEP demo has not been compiled as it depends on Ogre (missing/disabled)\n";

	return -1;
}
#endif
