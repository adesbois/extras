#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCApplication.h"
#include <cmath>

#include "fs/Files.h"

using namespace OpenViBE;
using namespace SSVEP;
using namespace /*OpenViBE::*/Kernel;

#define MIN(a,b) ( (a) < (b) ? (a) : (b) )

CApplication::~CApplication()
{
	if (m_painter != nullptr)
	{
		(*m_logManager) << LogLevel_Debug << "- m_painter\n";
		delete m_painter;
		m_painter = nullptr;
	}

	for (auto it = m_oCommands.begin();
		 it != m_oCommands.end(); ++it)
	{
		(*m_logManager) << LogLevel_Debug << "- ICommand\n";
		if (*it != nullptr)
		{
			delete *it;
			*it = nullptr;
		}
	}


	(*m_logManager) << LogLevel_Debug << "- m_root\n";
	if (m_root != nullptr)
	{
		delete m_root;
		m_root = nullptr;
	}
}

bool CApplication::setup(IKernelContext* poKernelContext)
{
	m_kernelCtx  = poKernelContext;
	m_logManager = &(m_kernelCtx->getLogManager());

	IConfigurationManager* configurationManager = &(m_kernelCtx->getConfigurationManager());

	(*m_logManager) << LogLevel_Debug << "  * CApplication::setup()\n";

	// Plugin config path setup
	const CString pluginsPath = configurationManager->expand("${Kernel_3DVisualizationOgrePlugins}");

	// Create LogManager to stop Ogre flooding the console and creating random files
	(*m_logManager) << LogLevel_Info << "Ogre plugins path is " << pluginsPath << "\n";

	(*m_logManager) << LogLevel_Debug << "+ Creating Ogre logmanager\n";
	Ogre::LogManager* logManager = new Ogre::LogManager();
	(*m_logManager) << LogLevel_Info << "Ogre log to console : " << configurationManager->expandAsBoolean("${SSVEP_Ogre_LogToConsole}", false) << "\n";
	const CString ogreLog = configurationManager->expand("${Path_Log}") + "/openvibe-ssvep-demo-ogre.log";
	(*m_logManager) << LogLevel_Info << "Ogre log file : " << ogreLog << "\n";
	FS::Files::createParentPath(ogreLog);
	logManager->createLog(ogreLog.toASCIIString(), true, configurationManager->expandAsBoolean("${SSVEP_Ogre_LogToConsole}", false), false);

	// Root creation
	const CString ogreCfg = configurationManager->expand("${Path_UserData}") + "/openvibe-ssvep-demo-ogre.cfg";
	(*m_logManager) << LogLevel_Debug << "+ m_root = new Ogre::Root(...)\n";
	(*m_logManager) << LogLevel_Info << "Ogre cfg file : " << ogreCfg << "\n";
	m_root = new Ogre::Root(pluginsPath.toASCIIString(), ogreCfg.toASCIIString(), ogreLog.toASCIIString());

	// Resource handling
	setupResources();

	// Configuration from file or dialog window if needed
	if (!configure())
	{
		(*m_logManager) << LogLevel_Fatal << "The configuration process ended unexpectedly.\n";
		return false;
	}

	// m_window = m_root->initialise(true);


	Ogre::NameValuePairList optionList;
	m_root->initialise(false);

	optionList["vsync"] = "1";

	m_window = m_root->createRenderWindow("SSVEP Stimulator", 640, 480, configurationManager->expandAsBoolean("${SSVEP_Ogre_FullScreen}", false),
										  &optionList);
	m_windowWidth  = m_window->getWidth();
	m_windowHeight = m_window->getHeight();

	m_sceneManager = m_root->createSceneManager(Ogre::ST_GENERIC);
	m_camera       = m_sceneManager->createCamera("SSVEPApplicationCamera");

	Ogre::SceneManager* fillSceneManager = m_root->createSceneManager(Ogre::ST_GENERIC);
	Ogre::Camera* fillCamera             = fillSceneManager->createCamera("SSVEPFillCamera");
	m_window->addViewport(fillCamera, 0);

	m_viewport = m_window->addViewport(m_camera, 1);
	this->resizeViewport();
	// m_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.5, 0.5));

	m_camera->setAspectRatio(Ogre::Real(m_viewport->getActualWidth()) / Ogre::Real(m_viewport->getActualHeight()));

	m_sceneNode = m_sceneManager->getRootSceneNode()->createChildSceneNode("SSVEPApplicationNode");

	// initialize the painter object
	(*m_logManager) << LogLevel_Debug << "+ m_painter = new CBasicPainter(...)\n";
	m_painter = new CBasicPainter(this);

	(*m_logManager) << LogLevel_Debug << "  * initializing CEGUI\n";
	this->initCEGUI(configurationManager->expand("${Path_Log}") + "/openvibe-ssvep-demo-cegui.log");
	(*m_logManager) << LogLevel_Debug << "  * CEGUI initialized\n";

	// create the vector of stimulation frequencies

	m_ScreenRefreshRate = double(configurationManager->expandAsUInteger("${SSVEP_ScreenRefreshRate}"));

	(*m_logManager) << LogLevel_Info << "Specified screen refresh rate :" << m_ScreenRefreshRate << "Hz\n";

	size_t i = 1;


	CIdentifier frequencyId = configurationManager->createConfigurationToken("SSVEP_FrequencyId", "1");

	m_oFrequencies.push_back(std::pair<size_t, size_t>(30, 30));

	while (configurationManager->lookUpConfigurationTokenIdentifier(configurationManager->expand("SSVEP_Frequency_${SSVEP_FrequencyId}")) !=
		   OV_UndefinedIdentifier)
	{
		std::pair<size_t, size_t> frequency;

		double currentFrequency;

		currentFrequency = double(configurationManager->expandAsFloat("${SSVEP_Frequency_${SSVEP_FrequencyId}}"));

		const double approximatedFrameCount = m_ScreenRefreshRate / currentFrequency;

		if (fabs(approximatedFrameCount - floor(approximatedFrameCount + 0.5)) < 0.003)
		{
			frequency.first  = int(floor(approximatedFrameCount + 0.5)) / 2 + int(floor(approximatedFrameCount + 0.5)) % 2;
			frequency.second = int(floor(approximatedFrameCount + 0.5)) / 2;

			(*m_logManager) << LogLevel_Info << "Frequency number " << i << ": " << currentFrequency << "Hz / " <<
					floor(approximatedFrameCount + 0.5) << " ( " << frequency.first << ", " << frequency.second << ") frames @ " <<
					m_ScreenRefreshRate << "fps\n";

			m_oFrequencies.push_back(frequency);
		}
		else { (*m_logManager) << LogLevel_Error << "The selected frequency (" << currentFrequency << "Hz) is not supported by your screen.\n"; }

		configurationManager->releaseConfigurationToken(frequencyId);

		frequencyId = configurationManager->createConfigurationToken("SSVEP_FrequencyId", std::to_string(++i).c_str());
	}


	return true;
}

bool CApplication::configure() const
{
	if (! m_root->restoreConfig())
	{
		if (! m_root->showConfigDialog())
		{
			(*m_logManager) << LogLevel_Error << "No configuration created from the dialog window.\n";
			return false;
		}
	}

	// Set hard-coded parameters, VSync in particular
	m_root->getRenderSystem()->setConfigOption("VSync", "True");
	// m_root->getRenderSystem()->setConfigOption("Full Screen", "Yes");
	m_root->getRenderSystem()->setConfigOption("Video Mode", "640 x 480 @ 16-bit colour");


	return true;
}

void CApplication::initCEGUI(const char* logFilename)
{
	// Instantiate logger before bootstrapping the system, this way we will be able to get the log redirected
	if (!CEGUI::Logger::getSingletonPtr()) { new CEGUI::DefaultLogger(); }		// singleton; instantiate only, no delete
	(*m_logManager) << LogLevel_Info << "+ CEGUI log will be in '" << logFilename << "'\n";
	FS::Files::createParentPath(logFilename);
	CEGUI::Logger::getSingleton().setLogFilename(logFilename, false);

	(*m_logManager) << LogLevel_Debug << "+ Creating CEGUI Ogre bootstrap\n";
	m_roGUIRenderer = &(CEGUI::OgreRenderer::bootstrapSystem(*m_window));
	(*m_logManager) << LogLevel_Debug << "+ Creating CEGUI Scheme Manager\n";

#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	CEGUI::SchemeManager::getSingleton().createFromFile(const_cast<CEGUI::utf8*>(reinterpret_cast<const CEGUI::utf8*>("TaharezLook-ov-0.8.scheme")));
#else
	CEGUI::SchemeManager::getSingleton().create(reinterpret_cast<CEGUI::utf8*>("TaharezLook-ov.scheme"));
#endif

	(*m_logManager) << LogLevel_Debug << "+ Creating CEGUI WindowManager\n";
	m_guiManager = CEGUI::WindowManager::getSingletonPtr();
	m_sheet      = m_guiManager->createWindow("DefaultWindow", "Sheet");

	(*m_logManager) << LogLevel_Debug << "+ Setting CEGUI StyleSheet\n";

#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(m_sheet);
#else
	CEGUI::System::getSingleton().setGUISheet(m_sheet);
#endif
}

void CApplication::resizeViewport()
{
	(*m_logManager) << LogLevel_Trace << "Creating a new viewport\n";

	const Ogre::uint32 size = MIN(m_windowWidth, m_windowHeight);
	(*m_logManager) << LogLevel_Info << "New viewport size : " << size << "\n";

	m_viewport->setDimensions(Ogre::Real(m_windowWidth - size) / Ogre::Real(m_windowWidth) / 2,
							  Ogre::Real(m_windowHeight - size) / Ogre::Real(m_windowHeight) / 2,
							  Ogre::Real(size) / Ogre::Real(m_windowWidth), Ogre::Real(size) / Ogre::Real(m_windowHeight));
}

void CApplication::processFrame(const size_t /*currentFrame*/)
{
	if (m_windowWidth != m_window->getWidth() || m_windowHeight != m_window->getHeight())
	{
		m_windowWidth  = m_window->getWidth();
		m_windowHeight = m_window->getHeight();
		this->resizeViewport();
	}
}

void CApplication::setupResources()
{
	Ogre::ResourceGroupManager::getSingleton().createResourceGroup("SSVEP");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation((Directories::getDataDir() + "/applications/ssvep-demo/resources").toASCIIString(),
																   "FileSystem", "SSVEP");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation((Directories::getDataDir() + "/applications/ssvep-demo/resources/trainer").toASCIIString(),
																   "FileSystem", "SSVEPTrainer");
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation((Directories::getDataDir() + "/applications/ssvep-demo/resources/gui").toASCIIString(),
																   "FileSystem", "CEGUI");
	Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("SSVEP");
	Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("SSVEPTrainer");
	Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("CEGUI");
}

bool CApplication::frameRenderingQueued(const Ogre::FrameEvent& /*evt*/) { return (m_bContinueRendering && !m_window->isClosed()); }

bool CApplication::frameStarted(const Ogre::FrameEvent& /*evt*/)
{
	++m_currFrame;
	m_currFrame = m_currFrame % int(m_ScreenRefreshRate);
	this->processFrame(m_currFrame);
	for (size_t i = 0; i < m_oCommands.size(); ++i) { m_oCommands[i]->processFrame(); }
	return true;
}

void CApplication::go()
{
	(*m_logManager) << LogLevel_Debug << "Associating application as Ogre frame listener\n";

	m_root->addFrameListener(this);

	(*m_logManager) << LogLevel_Debug << "Entering Ogre rendering loop\n";
	m_root->startRendering();
	(*m_logManager) << LogLevel_Debug << "Ogre rendering loop finished ... exiting\n";
}

void CApplication::addCommand(ICommand* command) { m_oCommands.push_back(command); }

void CApplication::startExperiment() { (*m_logManager) << LogLevel_Info << "[!] Experiment starting\n"; }

void CApplication::stopExperiment()
{
	(*m_logManager) << LogLevel_Info << "[!] Experiment halting\n";
	this->exit();
}

#endif
