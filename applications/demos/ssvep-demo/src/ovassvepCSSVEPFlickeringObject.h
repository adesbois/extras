#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

namespace SSVEP {
class CSSVEPFlickeringObject
{
public:
	CSSVEPFlickeringObject(Ogre::SceneNode* objectNode, size_t litFrames, size_t darkFrames);
	virtual ~CSSVEPFlickeringObject() {}

	virtual void setVisible(bool visibility);
	virtual void processFrame();

protected:
	Ogre::SceneNode* m_objectNode = nullptr;
	size_t m_currentFrame         = 0;
	size_t m_litFrames            = 0;
	size_t m_darkFrames           = 0;

private:

	bool m_visible = true;
};
}  // namespace SSVEP
