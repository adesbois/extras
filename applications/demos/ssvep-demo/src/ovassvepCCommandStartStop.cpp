#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCCommandStartStop.h"
#include "ovassvepCApplication.h"
#include "ovassvepCVRPNServer.h"

using namespace SSVEP;
using namespace OpenViBE::Kernel;

CCommandStartStop::CCommandStartStop(CApplication* application)
	: ICommandOIS(application)
{
	m_vrpnServer = CVRPNServer::getInstance(application);
	m_vrpnServer->addButton("SSVEP_VRPN_StartStop", 2);
}

CCommandStartStop::~CCommandStartStop()
{
	m_application->getLogManager() << LogLevel_Info << "End message sent\n";
	m_vrpnServer->changeButtonState("SSVEP_VRPN_StartStop", 1, 1);
	m_vrpnServer->processFrame();
}

void CCommandStartStop::processFrame()
{
	ICommandOIS::processFrame();
	m_vrpnServer->processFrame();
}

void CCommandStartStop::receiveKeyPressedEvent(const OIS::KeyCode key)
{
	if (key == OIS::KC_SPACE)
	{
		m_application->getLogManager() << LogLevel_Info << "Start message sent\n";

		m_vrpnServer->changeButtonState("SSVEP_VRPN_StartStop", 0, 1);
	}

	if (key == OIS::KC_ESCAPE) { m_application->exit(); }
}

void CCommandStartStop::receiveKeyReleasedEvent(const OIS::KeyCode key)
{
	if (key == OIS::KC_SPACE) { m_vrpnServer->changeButtonState("SSVEP_VRPN_StartStop", 0, 0); }
}

#endif
