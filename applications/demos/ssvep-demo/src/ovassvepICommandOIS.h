#pragma once

#include <vector>

#include <OIS.h>
#include <OISKeyboard.h>
#include "ovassvepICommand.h"

namespace SSVEP {
class ICommandOIS : public ICommand, OIS::KeyListener
{
	static std::vector<ICommandOIS*> m_instances;
	static int m_nInstance;

protected:
	static OIS::InputManager* m_inputManager;
	static OIS::Keyboard* m_keyboard;

public:
	explicit ICommandOIS(CApplication* application);
	~ICommandOIS() override;

	void processFrame() override;

protected:
	virtual void receiveKeyPressedEvent(OIS::KeyCode key) = 0;
	virtual void receiveKeyReleasedEvent(OIS::KeyCode key) = 0;

private:
	bool keyPressed(const OIS::KeyEvent& event) override;
	bool keyReleased(const OIS::KeyEvent& event) override;
};
}  // namespace SSVEP
