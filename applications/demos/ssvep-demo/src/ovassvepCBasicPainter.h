#pragma once

#include <Ogre.h>

#if (OGRE_VERSION_MAJOR > 1) || ((OGRE_VERSION_MAJOR == 1) && (OGRE_VERSION_MINOR >= 9))
#include "Overlay/OgreOverlayContainer.h"
#endif

#define SSVEP_DEFAULT_COLOUR Ogre::ColourValue(1.0f, 1.0f, 1.0f)

namespace SSVEP {
class CApplication;

class Point
{
public:
	Point(const float x, const float y) : m_X(x), m_Y(y) {}

	float m_X = 0.0F, m_Y = 0.0F;
};

class CBasicPainter
{
public:
	explicit CBasicPainter(CApplication* application);
	~CBasicPainter() {}

	Ogre::ManualObject* paintRectangle(const Ogre::RealRect& oRectangle, const Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR, const int plane = 1) const;
	Ogre::ManualObject* paintTriangle(const Point p1, const Point p2, const Point p3, const Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR,
									  const int plane                                                               = 1) const;
	Ogre::ManualObject* paintCircle(const Ogre::Real rX, const Ogre::Real rY, const Ogre::Real rR, const Ogre::ColourValue color = SSVEP_DEFAULT_COLOUR,
									const bool bFilled                                                                           = true,
									const int plane                                                                              = 1) const;

	void paintText(const std::string& sID, const std::string& sText, const Ogre::Real rX, const Ogre::Real rY,
				   const Ogre::Real rWidth, const Ogre::Real rHeight, const Ogre::ColourValue& color) const;

protected:
	CApplication* m_application                = nullptr;
	Ogre::OverlayManager* m_overlayManager     = nullptr;
	Ogre::OverlayContainer* m_overlayContainer = nullptr;
	Ogre::SceneManager* m_sceneManager         = nullptr;
	Ogre::AxisAlignedBox m_aabInf;
};
}  // namespace SSVEP
