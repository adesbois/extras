#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepICommandOIS.h"
#include "ovassvepCApplication.h"

using namespace SSVEP;
using namespace OpenViBE::Kernel;
using namespace OIS;

InputManager* ICommandOIS::m_inputManager = nullptr;
Keyboard* ICommandOIS::m_keyboard         = nullptr;

int ICommandOIS::m_nInstance = 0;
std::vector<ICommandOIS*> ICommandOIS::m_instances;


ICommandOIS::ICommandOIS(CApplication* application)
	: ICommand(application)
{
	if (m_keyboard == nullptr)
	{
		ParamList paramList;
		std::ostringstream windowHandleString;
		size_t windowHandle;


		m_application->getWindow()->getCustomAttribute("WINDOW", &windowHandle);

		windowHandleString << windowHandle;

		paramList.insert(std::make_pair(std::string("WINDOW"), windowHandleString.str()));
		paramList.insert(std::make_pair(std::string("x11_keyboard_grab"), "false"));

		m_inputManager = InputManager::createInputSystem(paramList);

		m_keyboard = dynamic_cast<Keyboard*>(m_inputManager->createInputObject(OISKeyboard, true));

		m_keyboard->setEventCallback(this);
	}

	m_nInstance++;
	m_instances.push_back(this);
}

ICommandOIS::~ICommandOIS()
{
	--m_nInstance;

	if (m_nInstance == 0)
	{
		if (m_inputManager != nullptr)
		{
			if (m_keyboard != nullptr)
			{
				m_application->getLogManager() << LogLevel_Debug << "- destroy m_keyboard\n";
				m_inputManager->destroyInputObject(m_keyboard);
				m_keyboard = nullptr;
			}

			m_inputManager->destroyInputSystem(m_inputManager);
		}
	}
}

void ICommandOIS::processFrame() { m_keyboard->capture(); }


bool ICommandOIS::keyPressed(const KeyEvent& event)
{
	for (size_t i = 0; i < m_instances.size(); ++i) { m_instances[i]->receiveKeyPressedEvent(event.key); }

	return true;
}

bool ICommandOIS::keyReleased(const KeyEvent& event)
{
	for (size_t i = 0; i < m_instances.size(); ++i) { m_instances[i]->receiveKeyReleasedEvent(event.key); }

	return true;
}


#endif
