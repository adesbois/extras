#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCShooterTarget.h"

using namespace SSVEP;

Ogre::SceneNode* CShooterTarget::m_parentNode = nullptr;
CBasicPainter* CShooterTarget::m_painter      = nullptr;

void CShooterTarget::initialize(CBasicPainter* painter, Ogre::SceneNode* parentNode)
{
	m_painter    = painter;
	m_parentNode = parentNode;
}

CShooterTarget* CShooterTarget::createTarget(const Ogre::Radian angle)
{
	if (m_painter == nullptr)
	{
		std::cerr << "The CShooterTarget was not initialized" << std::endl;
		return nullptr;
	}

	return new CShooterTarget(angle);
}

CShooterTarget::CShooterTarget(const Ogre::Radian angle)
{
	m_targetNode = m_parentNode->createChildSceneNode();


	Ogre::MovableObject* targetObject = m_painter->paintCircle(0.0F, 1.0F - 1.0F * SSVEP_SHOOTER_TARGET_SIZE, SSVEP_SHOOTER_TARGET_SIZE,
															   Ogre::ColourValue(1.0F, 1.0F, 1.0F));
	targetObject->setVisible(true);
	m_targetNode->attachObject(targetObject);

	m_targetNode->roll(angle);
}

CShooterTarget::~CShooterTarget()
{
	m_targetNode->removeAndDestroyAllChildren();
	m_targetNode->getCreator()->destroySceneNode(m_targetNode);
}

bool CShooterTarget::collidesWith(const std::pair<Ogre::Real, Ogre::Real> point) const
{
	const Ogre::Radian theta    = (m_targetNode->getOrientation()).getRoll();
	const Ogre::Real targetDist = 1.0F - 2 * SSVEP_SHOOTER_TARGET_SIZE;
	const Ogre::Real dist       = sqrt(pow(point.first + Ogre::Math::Sin(theta) * targetDist, 2) + pow(point.second - Ogre::Math::Cos(theta) * targetDist, 2));
	return dist <= SSVEP_SHOOTER_TARGET_SIZE;
}

#endif
