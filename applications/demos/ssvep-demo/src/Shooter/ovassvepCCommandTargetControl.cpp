#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCCommandTargetControl.h"
#include "ovassvepCShooterApplication.h"

using namespace SSVEP;
using namespace OpenViBE::Kernel;

CCommandTargetControl::CCommandTargetControl(CShooterApplication* application)
	: ICommandVRPNButton(application, "SSVEP_VRPN_ShooterTarget")
{
	m_vrpnServer = CVRPNServer::getInstance(application);
	m_vrpnServer->addButton("SSVEP_VRPN_TargetRequest", 1);
}

void CCommandTargetControl::processFrame()
{
	CShooterApplication* shooterApplication = dynamic_cast<CShooterApplication*>(m_application);

	ICommandVRPNButton::processFrame();

	if (shooterApplication->m_TargetRequest)
	{
		m_application->getLogManager() << LogLevel_Info << "Requesting target\n";
		m_vrpnServer->changeButtonState("SSVEP_VRPN_TargetRequest", 0, 1);
		shooterApplication->m_TargetRequest = false;
	}
	else { m_vrpnServer->changeButtonState("SSVEP_VRPN_TargetRequest", 0, 0); }

	m_vrpnServer->processFrame();
}

void CCommandTargetControl::execute(const int button, const int state)
{
	if (state == 1)
	{
		m_application->getLogManager() << LogLevel_Info << "Target created at position " << button << "\n";
		dynamic_cast<CShooterApplication*>(m_application)->addTarget(button);
	}
}

#endif
