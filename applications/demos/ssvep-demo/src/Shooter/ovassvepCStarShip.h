#pragma once

#include <Ogre.h>

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace SSVEP {
class CApplication;
class CSSVEPFlickeringObject;

class CStarShip
{
public:
	CStarShip(CApplication* application, Ogre::SceneNode* parentNode, Ogre::Real radius, std::vector<std::pair<size_t, size_t>>* frequencies);
	void processFrame(const size_t currentFrame);
	void rotate(const int nRotation) { m_nCurrentRotation += nRotation; }
	bool isShooting() const { return m_isShooting; }
	void shoot();
	std::pair<Ogre::Real, Ogre::Real> getShotCoordinates() const;

private:
	Ogre::SceneNode* m_shipNode = nullptr;

	CSSVEPFlickeringObject* m_shipCannon    = nullptr;
	CSSVEPFlickeringObject* m_shipLeftWing  = nullptr;
	CSSVEPFlickeringObject* m_shipRightWing = nullptr;

	Ogre::Real m_angularSpeed;
	Ogre::Radian m_currentAngle;
	int m_nCurrentRotation = 0;

	Ogre::SceneNode* m_shotNode = nullptr;
	bool m_isShooting           = false;
	Ogre::Real m_shotDistance;
};
}  // namespace SSVEP
