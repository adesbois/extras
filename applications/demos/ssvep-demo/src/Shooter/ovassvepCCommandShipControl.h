#pragma once

#include "../ovassvepICommandVRPNButton.h"

namespace SSVEP {
class CShooterApplication;

class CCommandShipControl final : public ICommandVRPNButton
{
public:
	explicit CCommandShipControl(CShooterApplication* application);
	~CCommandShipControl() override {}

	void execute(const int button, const int state) override;
};
}  // namespace SSVEP
