#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "../ovassvepCApplication.h"
#include "ovassvepCShooterTarget.h"
#include "ovassvepCStarShip.h"


namespace SSVEP {
class CShooterApplication final : public CApplication
{
public:
	CShooterApplication();
	~CShooterApplication() override;

	bool setup(OpenViBE::Kernel::IKernelContext* kernelCtx) override;
	CStarShip* getShip() const { return m_ship; }
	void startExperiment() override;
	void addTarget(size_t targetPosition);

	bool m_TargetRequest = false;

private:
	void processFrame(const size_t frame) override;

	CEGUI::Window* m_instructionsReady = nullptr;
	CStarShip* m_ship                  = nullptr;
	std::vector<CShooterTarget*> m_targets;
};
}  // namespace SSVEP
