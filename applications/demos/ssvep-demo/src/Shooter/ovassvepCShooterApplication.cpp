#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCShooterApplication.h"

#include "../ovassvepCCommandStartStop.h"
#include "../ovassvepCCommandStimulatorControl.h"
#include "ovassvepCCommandTargetControl.h"
#include "ovassvepCCommandShipControl.h"

using namespace Ogre;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace SSVEP;

#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
	typedef CEGUI::UVector2 USize;
};
#endif
CShooterApplication::CShooterApplication() : CApplication() {}

CShooterApplication::~CShooterApplication()
{
	(*m_logManager) << LogLevel_Debug << "- m_ship\n";
	delete m_ship;
}

bool CShooterApplication::setup(IKernelContext* kernelCtx)
{
	if (!CApplication::setup(kernelCtx))
	{
		// If setup failed, logmanager may be in an unusable state, use cout
		std::cout << "Error: Base application setup failed\n";
		return false;
	}

	// Create the StarShip object
	(*m_logManager) << LogLevel_Debug << "+ m_ship = new CStarShip(...)\n";
	m_ship = new CStarShip(this, m_sceneNode, 0.3F, &m_oFrequencies);

	// Initialize the Target class

	CShooterTarget::initialize(m_painter, m_sceneNode);

	// draw the initial text

	m_instructionsReady = m_guiManager->createWindow("TaharezLook/StaticImage", "InstructionsReady");
	m_instructionsReady->setPosition(CEGUI::UVector2(cegui_reldim(0.0F), cegui_reldim(0.0F)));
	m_instructionsReady->setSize(CEGUI::USize(CEGUI::UDim(0.0F, 640.F), CEGUI::UDim(0.0F, 32.F)));

#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(m_instructionsReady);
	CEGUI::ImageManager::getSingleton().addFromImageFile("InstructionsReady", "InstructionText-Start.png");
	m_instructionsReady->setProperty("Image", "InstructionsReady");
#else
	m_sheet->addChildWindow(m_instructionsReady);
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("InstructionsReady", "InstructionText-Start.png");
	m_instructionsReady->setProperty("Image", "set:InstructionsReady image:full_image");
#endif

	m_instructionsReady->setProperty("FrameEnabled", "False");
	m_instructionsReady->setProperty("BackgroundEnabled", "False");
	m_instructionsReady->setVisible(true);

	// Create commands
	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStartStop(...)\n";
	this->addCommand(new CCommandStartStop(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStimulatorControl(...))\n";
	this->addCommand(new CCommandStimulatorControl(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandTargetControl(...)\n";
	this->addCommand(new CCommandTargetControl(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandShipControl(...))\n";
	this->addCommand(new CCommandShipControl(this));

	return true;
}

void CShooterApplication::processFrame(const size_t frame)
{
	CApplication::processFrame(frame);
	m_ship->processFrame(frame);

	if (m_ship->isShooting())
	{
		for (auto it = m_targets.begin(); it != m_targets.end(); ++it)
		{
			if ((*it)->collidesWith(m_ship->getShotCoordinates()))
			{
				delete *it;
				m_targets.erase(it);
				m_TargetRequest = true;
				break;
			}
		}
	}
}

void CShooterApplication::addTarget(const size_t targetPosition)
{
	m_targets.push_back(CShooterTarget::createTarget(Radian(Math::PI * 2 / 360 * 45 * targetPosition)));
}

void CShooterApplication::startExperiment()
{
	m_TargetRequest = true;
	CApplication::startExperiment();

	m_instructionsReady->setVisible(false);
}

#endif
