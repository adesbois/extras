#pragma once

#include "../ovassvepICommandVRPNButton.h"
#include "../ovassvepCVRPNServer.h"

namespace SSVEP {
class CShooterApplication;

class CCommandTargetControl final : public ICommandVRPNButton
{
public:
	explicit CCommandTargetControl(CShooterApplication* application);
	~CCommandTargetControl() override {}

	void execute(const int button, const int state) override;
	void processFrame() override;

private:
	CVRPNServer* m_vrpnServer = nullptr;
};
}  // namespace SSVEP
