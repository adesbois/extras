#pragma once

#include <Ogre.h>

#include "../ovassvepCBasicPainter.h"

#define SSVEP_SHOOTER_TARGET_SIZE 0.15f

namespace SSVEP {
class CShooterTarget
{
public:
	static CShooterTarget* createTarget(Ogre::Radian angle);
	static void initialize(CBasicPainter* painter, Ogre::SceneNode* parentNode);
	~CShooterTarget();

	bool collidesWith(std::pair<Ogre::Real, Ogre::Real> point) const;

private:
	static Ogre::SceneNode* m_parentNode;
	static CBasicPainter* m_painter;

	Ogre::SceneNode* m_targetNode = nullptr;

	explicit CShooterTarget(Ogre::Radian angle);
};
}  // namespace SSVEP
