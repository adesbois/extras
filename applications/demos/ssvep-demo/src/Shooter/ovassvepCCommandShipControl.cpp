#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCCommandShipControl.h"
#include "ovassvepCShooterApplication.h"

using namespace SSVEP;

CCommandShipControl::CCommandShipControl(CShooterApplication* application) : ICommandVRPNButton(application, "SSVEP_VRPN_ShipControl") {}

void CCommandShipControl::execute(const int button, const int /*state*/)
{
	CShooterApplication* shooterApplication = dynamic_cast<CShooterApplication*>(m_application);

	switch (button)
	{
		case 0:
			shooterApplication->getShip()->shoot();
			break;
		case 1:
			shooterApplication->getShip()->rotate(-6);
			break;
		case 2:
			shooterApplication->getShip()->rotate(6);
			break;
		default: break;
	}
}

#endif
