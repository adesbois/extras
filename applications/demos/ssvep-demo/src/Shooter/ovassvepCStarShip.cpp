#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCStarShip.h"

#include "../ovassvepCApplication.h"
#include "../ovassvepCSSVEPFlickeringObject.h"

#define SSVEP_SHIP_HULL_COLOUR Ogre::ColourValue(0.0F, 0.5F, 0.5F)
#define SIGN(x) ( (x) < 0 ? -1 : 1 )

using namespace SSVEP;
using namespace Ogre;

CStarShip::CStarShip(CApplication* application, SceneNode* parentNode, const Real radius, std::vector<std::pair<size_t, size_t>>* frequencies)
{
	// get configuration and controlling objects
	OpenViBE::Kernel::IConfigurationManager* configManager = application->getConfigurationManager();
	CBasicPainter* painter                                 = application->getPainter();
	m_angularSpeed                                         = Real(configManager->expandAsFloat(
		"${SSVEP_ShipAngularSpeed}", Math::PI / 60.0F));
	application->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "Ship Angluar Speed : " << m_angularSpeed << "\n";

	const ColourValue lightColor = ColourValue(float(configManager->expandAsFloat("${SSVEP_TargetLightColourRed}", 1.0F)),
											   float(configManager->expandAsFloat("${SSVEP_TargetLightColourGreen}", 1.0F)),
											   float(configManager->expandAsFloat("${SSVEP_TargetLightColourBlue}", 1.0F)));

	const ColourValue darkColor = ColourValue(float(configManager->expandAsFloat("${SSVEP_TargetDarkColourRed}", 0.0F)),
											  float(configManager->expandAsFloat("${SSVEP_TargetDarkColourGreen}", 0.0F)),
											  float(configManager->expandAsFloat("${SSVEP_TargetDarkColourBlue}", 0.0F)));


	// draw the ship scene objects
	m_shipNode = parentNode->createChildSceneNode();
	m_shotNode = m_shipNode->createChildSceneNode();

	MovableObject* shipHull = painter->paintCircle(0.0F, 0.0F, radius, SSVEP_SHIP_HULL_COLOUR, false, 2);
	m_shipNode->attachObject(shipHull);
	shipHull->setVisible(true);


	// Ogre::RealRect wingRectangle = { - radius * 0.4f, radius * 0.4f, radius * 0.4f, -radius * 0.4f };
	const RealRect wingRectangle(-radius * 0.4F, radius * 0.4F, radius * 0.4F, -radius * 0.4F);

	// paint the cannon

	SceneNode* drawnObjectNode = m_shipNode->createChildSceneNode();

	MovableObject* drawnObject = painter->paintTriangle(Point(0.0F, radius * 0.4F), Point(- radius * 0.4F, -radius * 0.4F),
														Point(radius * 0.4F, -radius * 0.4F), lightColor);
	drawnObject->setVisible(true);
	drawnObjectNode->attachObject(drawnObject);

	drawnObject = painter->paintTriangle(Point(0.0F, radius * 0.4F), Point(- radius * 0.4F, -radius * 0.4F), Point(radius * 0.4F, -radius * 0.4F), darkColor);
	drawnObject->setVisible(false);
	drawnObjectNode->attachObject(drawnObject);

	drawnObjectNode->setPosition(0.0, radius, 0.0);

	m_shipCannon = new CSSVEPFlickeringObject(drawnObjectNode, (*frequencies)[1].first, (*frequencies)[1].second);

	// paint the left wing

	drawnObjectNode = m_shipNode->createChildSceneNode();

	drawnObject = painter->paintRectangle(wingRectangle, lightColor);
	drawnObject->setVisible(true);
	drawnObjectNode->attachObject(drawnObject);

	drawnObject = painter->paintRectangle(wingRectangle, darkColor);
	drawnObject->setVisible(false);
	drawnObjectNode->attachObject(drawnObject);

	drawnObjectNode->setPosition(-radius * 0.875F, -radius * 0.875F, 0.0);

	m_shipLeftWing = new CSSVEPFlickeringObject(drawnObjectNode, (*frequencies)[2].first, (*frequencies)[2].second);

	// paint the right wing

	drawnObjectNode = m_shipNode->createChildSceneNode();

	drawnObject = painter->paintRectangle(wingRectangle, lightColor);
	drawnObject->setVisible(true);
	drawnObjectNode->attachObject(drawnObject);

	drawnObject = painter->paintRectangle(wingRectangle, darkColor);
	drawnObject->setVisible(false);
	drawnObjectNode->attachObject(drawnObject);

	drawnObjectNode->setPosition(radius * 0.875F, -radius * 0.875F, 0.0F);

	m_shipRightWing = new CSSVEPFlickeringObject(drawnObjectNode, (*frequencies)[3].first, (*frequencies)[3].second);

	// create the shot

	drawnObject = painter->paintTriangle(Point(0.0F, radius * 0.25F), Point(-radius * 0.125F, 0.0F), Point(radius * 0.125F, 0.0F),
										 ColourValue(1.0F, 1.0F, 0.0F));
	drawnObject->setVisible(false);

	m_shotNode->attachObject(drawnObject);

	drawnObject = painter->paintTriangle(Point(-radius * 0.125F, 0.0F), Point(0.0F, -radius * 0.75F), Point(radius * 0.125F, 0.0F),
										 ColourValue(1.0F, 1.0F, 0.0F));
	drawnObject->setVisible(false);

	m_shotNode->attachObject(drawnObject);
}

void CStarShip::processFrame(const size_t /*currentFrame*/)
{
	m_shipCannon->processFrame();
	m_shipLeftWing->processFrame();
	m_shipRightWing->processFrame();

	if (m_nCurrentRotation != 0)
	{
		m_shipNode->roll(Radian(m_angularSpeed * SIGN(m_nCurrentRotation)));
		m_currentAngle += Radian(m_angularSpeed * SIGN(m_nCurrentRotation));
		m_nCurrentRotation -= SIGN(m_nCurrentRotation);
	}

	if (m_isShooting)
	{
		m_shotDistance += 0.07F;
		m_shotNode->setPosition(0.0, m_shotDistance, 0.0);

		if (m_shotDistance > 1.5F)
		{
			m_isShooting = false;
			m_shotNode->setVisible(false);
		}
	}
}

void CStarShip::shoot()
{
	if (m_isShooting) { return; }

	m_isShooting   = true;
	m_shotDistance = 0.0F;

	m_shotNode->setPosition(0.0F, 0.0F, 0.0F);
	m_shotNode->setVisible(true);
}

std::pair<Real, Real> CStarShip::getShotCoordinates() const
{
	return std::make_pair(-Math::Sin(m_currentAngle) * m_shotDistance, Math::Cos(m_currentAngle) * m_shotDistance);
}

#endif
