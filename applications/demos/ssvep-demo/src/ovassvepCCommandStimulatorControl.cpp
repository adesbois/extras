#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCCommandStimulatorControl.h"
#include "ovassvepCApplication.h"

using namespace SSVEP;

CCommandStimulatorControl::CCommandStimulatorControl(CApplication* application)
	: ICommandVRPNButton(application, "SSVEP_VRPN_StimulatorControl") {}

void CCommandStimulatorControl::execute(const int button, const int /*state*/)
{
	// only run the commands once, skip

	switch (button)
	{
		case 0:
			m_application->startExperiment();
			break;

		case 1:
			m_application->stopExperiment();
			break;

		case 2:
			m_application->startFlickering();
			break;

		case 3:
			m_application->stopFlickering();
			break;

		default:
			m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Error << "[ERROR] Unknown command\n";
			break;
	}
}

#endif
