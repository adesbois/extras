#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>
#include <vector>
#include <string>


class vrpn_Connection;
class vrpn_Button_Server;

namespace SSVEP {
class CApplication;

class CVRPNServer
{
public:
	static CVRPNServer* getInstance(CApplication* application);
	~CVRPNServer() = default;
	void processFrame();

	void addButton(const OpenViBE::CString& name, int buttonCount);
	void changeButtonState(const std::string& name, int index, int state);
	int getButtonState(const std::string& name, int index);

private:
	static CVRPNServer* m_vrpnServerInstance;
	static CApplication* m_application;

	explicit CVRPNServer(CApplication* application);

	vrpn_Connection* m_connection = nullptr;
	std::map<std::string, vrpn_Button_Server*> m_buttonServer;
	std::map<std::string, std::vector<int>> m_buttonCache;
};
}  // namespace SSVEP
