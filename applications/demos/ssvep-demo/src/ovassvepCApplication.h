#pragma once

#include "ovassvep_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <Ogre.h>
#include <vector>
#include <CEGUI.h>
#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#else
#include <RendererModules/Ogre/CEGUIOgreRenderer.h>
#endif

#include "ovassvepICommand.h"
#include "ovassvepCBasicPainter.h"

namespace SSVEP {
class CApplication : public Ogre::FrameListener, public Ogre::WindowEventListener
{
public:
	CApplication() {}
	~CApplication() override;

	void addCommand(ICommand* command);
	virtual bool setup(OpenViBE::Kernel::IKernelContext* poKernelContext);
	void go();

	virtual void startExperiment();
	virtual void stopExperiment();
	virtual void startFlickering() {}
	virtual void stopFlickering() {}

	Ogre::RenderWindow* getWindow() const { return m_window; }
	Ogre::SceneManager* getSceneManager() const { return m_sceneManager; }
	Ogre::SceneNode* getSceneNode() const { return m_sceneNode; }
	CBasicPainter* getPainter() const { return m_painter; }
	void exit() { m_bContinueRendering = false; }
	OpenViBE::Kernel::ILogManager& getLogManager() const { return (*m_logManager); }
	OpenViBE::Kernel::IConfigurationManager* getConfigurationManager() const { return &(m_kernelCtx->getConfigurationManager()); }
	std::vector<std::pair<size_t, size_t>>* getFrequencies() { return &m_oFrequencies; }
	void resizeViewport();

protected:
	OpenViBE::Kernel::IKernelContext* m_kernelCtx = nullptr;
	OpenViBE::Kernel::ILogManager* m_logManager   = nullptr;

	double m_ScreenRefreshRate = 0;
	CBasicPainter* m_painter   = nullptr;

	bool m_bContinueRendering = true;
	size_t m_currFrame        = 0;

	Ogre::Root* m_root                 = nullptr;
	Ogre::SceneManager* m_sceneManager = nullptr;
	Ogre::Camera* m_camera             = nullptr;
	Ogre::RenderWindow* m_window       = nullptr;
	Ogre::Viewport* m_viewport         = nullptr;
	Ogre::SceneNode* m_sceneNode       = nullptr;

	CEGUI::OgreRenderer* m_roGUIRenderer = nullptr;
	CEGUI::WindowManager* m_guiManager   = nullptr;
	CEGUI::Window* m_sheet               = nullptr;

	std::vector<std::pair<size_t, size_t>> m_oFrequencies;

	std::vector<ICommand*> m_oCommands;

	virtual void processFrame(const size_t currentFrame);

	bool frameRenderingQueued(const Ogre::FrameEvent& evt) override;
	bool frameStarted(const Ogre::FrameEvent& evt) override;

	bool configure() const;
	static void setupResources();


private:
	void initCEGUI(const char* logFilename);
	Ogre::uint32 m_windowWidth  = 0;
	Ogre::uint32 m_windowHeight = 0;
};
}  // namespace SSVEP
