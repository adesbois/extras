#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCSSVEPFlickeringObject.h"

using namespace SSVEP;

CSSVEPFlickeringObject::CSSVEPFlickeringObject(Ogre::SceneNode* objectNode, const size_t litFrames, const size_t darkFrames)
	: m_objectNode(objectNode), m_litFrames(litFrames), m_darkFrames(darkFrames) {}

void CSSVEPFlickeringObject::setVisible(const bool visibility)
{
	if ((!m_visible && visibility) || (m_visible && !visibility)) { m_objectNode->flipVisibility(); }
	m_visible = visibility;
}

void CSSVEPFlickeringObject::processFrame()
{
	if (m_currentFrame < m_litFrames) { this->setVisible(true); }
	else { this->setVisible(false); }

	m_currentFrame++;

	if (m_currentFrame == m_litFrames + m_darkFrames) { m_currentFrame = 0; }
}

#endif
