#pragma once

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <Ogre.h>

#include "../ovassvepCSSVEPFlickeringObject.h"
#include "../ovassvepCBasicPainter.h"

/**
 */
namespace SSVEP {
class CTrainerApplication;

class CTrainerFlickeringObject final : public CSSVEPFlickeringObject
{
public:
	static CTrainerFlickeringObject* createTrainerFlickeringObject(size_t targetId);
	static void initialize(CTrainerApplication* application);

	static void connectToNode(Ogre::SceneNode* /*sceneNode*/) { }
	void setTarget(const bool isTarget) const { m_pointer->setVisible(isTarget); }
	bool isTarget() const { return m_pointer->getVisible(); }

private:
	static CTrainerApplication* m_application;
	static Ogre::SceneNode* m_parentNode;
	static CBasicPainter* m_painter;
	static float m_targetWidth;
	static float m_targetHeight;
	static Ogre::ColourValue m_lightColor;
	static Ogre::ColourValue m_darkColor;

	CTrainerFlickeringObject(float posX, float posY, Ogre::ColourValue colour, uint8_t litFrames, uint8_t darkFrames);


	Ogre::SceneNode* m_elementNode = nullptr;
	Ogre::MovableObject* m_pointer = nullptr;
};
}  // namespace SSVEP
