#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCTrainerApplication.h"

#include "../ovassvepCCommandStartStop.h"
#include "../ovassvepCCommandStimulatorControl.h"
#include "ovassvepCCommandReceiveTarget.h"

using namespace Ogre;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace SSVEP;

#if !((CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8))
namespace CEGUI
{
	typedef CEGUI::UVector2 USize;
};
#endif

CTrainerApplication::CTrainerApplication() : CApplication() {}

bool CTrainerApplication::setup(IKernelContext* kernelCtx)
{
	if (!CApplication::setup(kernelCtx)) { return false; }

	(*m_logManager) << LogLevel_Debug << "  * CTrainerApplication::setup()\n";

	IConfigurationManager* configManager = &(m_kernelCtx->getConfigurationManager());

	CTrainerFlickeringObject::initialize(this);

	// paint targets

	const size_t nTarget = size_t(configManager->expandAsUInteger("${SSVEP_TargetCount}"));

	for (size_t i = 0; i < nTarget; ++i) { this->addObject(CTrainerFlickeringObject::createTrainerFlickeringObject(i)); }

	// draw the initial text
	m_instructionsReady = m_guiManager->createWindow("TaharezLook/StaticImage", "InstructionsReady");
	m_instructionsReady->setPosition(CEGUI::UVector2(cegui_reldim(0.0F), cegui_reldim(0.0F)));
	m_instructionsReady->setSize(CEGUI::USize(CEGUI::UDim(0.0F, 640.F), CEGUI::UDim(0.0F, 32.F)));

#if (CEGUI_VERSION_MAJOR > 0) || (CEGUI_VERSION_MINOR >= 8)
	m_sheet->addChild(m_instructionsReady);
	CEGUI::ImageManager::getSingleton().addFromImageFile("InstructionsReady", "InstructionText-Start.png");
	m_instructionsReady->setProperty("Image", "InstructionsReady");
#else
	m_sheet->addChildWindow(m_instructionsReady);
	CEGUI::ImagesetManager::getSingleton().createFromImageFile("InstructionsReady", "InstructionText-Start.png");
	m_instructionsReady->setProperty("Image", "set:InstructionsReady image:full_image");
#endif


	m_instructionsReady->setProperty("FrameEnabled", "False");
	m_instructionsReady->setProperty("BackgroundEnabled", "False");
	m_instructionsReady->setVisible(true);


	// initialize commands
	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStartStop(...)\n";
	this->addCommand(new CCommandStartStop(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CCommandStimulatorControl(...))\n";
	this->addCommand(new CCommandStimulatorControl(this));

	(*m_logManager) << LogLevel_Debug << "+ addCommand(new CC(...))\n";
	this->addCommand(new CCommandReceiveTarget(this));

	(*m_logManager) << LogLevel_Debug << "  * CTrainerApplication::setup() completed successfully\n";

	return true;
}


void CTrainerApplication::processFrame(const size_t frame)
{
	CApplication::processFrame(frame);

	if (!m_active) { return; }

	for (size_t i = 0; i < m_oObjects.size(); ++i) { m_oObjects[i]->processFrame(); }
}

void CTrainerApplication::addObject(CTrainerFlickeringObject* obj)
{
	m_oObjects.push_back(obj);
	obj->setVisible(true);
}

void CTrainerApplication::setTarget(const int target)
{
	const size_t currentTime = int(time(nullptr) - m_ttStartTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Target set to " << target << "\n";

	for (int i = 0; i < int(m_oObjects.size()); ++i) { m_oObjects[i]->setTarget(target == i); }
}

void CTrainerApplication::startExperiment()
{
	CApplication::startExperiment();

	m_ttStartTime = time(nullptr);

	this->stopFlickering();
	m_instructionsReady->setVisible(false);
}

void CTrainerApplication::startFlickering()
{
	const size_t currentTime = size_t(time(nullptr) - m_ttStartTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Starting Visual Stimulation\n";
	m_active = true;
}

void CTrainerApplication::stopFlickering()
{
	const size_t currentTime = size_t(time(nullptr) - m_ttStartTime);
	(*m_logManager) << LogLevel_Info << currentTime << "    > Stopping Visual Stimulation\n";
	m_active = false;

	for (size_t i = 0; i < m_oObjects.size(); ++i) { m_oObjects[i]->setVisible(true); }

	this->setTarget(-1);
}

#endif
