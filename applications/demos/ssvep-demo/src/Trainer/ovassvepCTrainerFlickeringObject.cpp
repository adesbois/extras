#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCTrainerFlickeringObject.h"
#include "ovassvepCTrainerApplication.h"

using namespace Ogre;
using namespace SSVEP;

SceneNode* CTrainerFlickeringObject::m_parentNode            = nullptr;
CBasicPainter* CTrainerFlickeringObject::m_painter           = nullptr;
ColourValue CTrainerFlickeringObject::m_lightColor           = ColourValue(1.0F, 1.0F, 1.0F);
ColourValue CTrainerFlickeringObject::m_darkColor            = ColourValue(0.0F, 0.0F, 0.0F);
float CTrainerFlickeringObject::m_targetWidth                = 0.2F;
float CTrainerFlickeringObject::m_targetHeight               = 0.2F;
CTrainerApplication* CTrainerFlickeringObject::m_application = nullptr;

void CTrainerFlickeringObject::initialize(CTrainerApplication* application)
{
	m_application                                          = application;
	OpenViBE::Kernel::IConfigurationManager* configManager = application->getConfigurationManager();

	m_painter    = application->getPainter();
	m_parentNode = application->getSceneNode();

	m_targetWidth  = float(configManager->expandAsFloat("${SSVEP_TargetWidth}"));
	m_targetHeight = float(configManager->expandAsFloat("${SSVEP_TargetHeight}"));

	m_lightColor = ColourValue(float(configManager->expandAsFloat("${SSVEP_TargetLightColourRed}")),
							   float(configManager->expandAsFloat("${SSVEP_TargetLightColourGreen}")),
							   float(configManager->expandAsFloat("${SSVEP_TargetLightColourBlue}")));

	m_darkColor = ColourValue(float(configManager->expandAsFloat("${SSVEP_TargetDarkColourRed}")),
							  float(configManager->expandAsFloat("${SSVEP_TargetDarkColourGreen}")),
							  float(configManager->expandAsFloat("${SSVEP_TargetDarkColourBlue}")));
}

CTrainerFlickeringObject* CTrainerFlickeringObject::createTrainerFlickeringObject(const size_t targetId)
{
	OpenViBE::Kernel::IConfigurationManager* configManager = m_application->getConfigurationManager();

	if (m_painter != nullptr)
	{
		const ColourValue currentTargetColour = (targetId == 0) ? m_darkColor : m_lightColor;

		const OpenViBE::CIdentifier id = configManager->createConfigurationToken("SSVEPTarget_Id", std::to_string(targetId).c_str());

		const float targetX  = float(configManager->expandAsFloat("${SSVEP_Target_X_${SSVEPTarget_Id}}"));
		const float targetY  = float(configManager->expandAsFloat("${SSVEP_Target_Y_${SSVEPTarget_Id}}"));
		const size_t framesL = (*(m_application->getFrequencies()))[targetId].first;
		const size_t framesD = (*(m_application->getFrequencies()))[targetId].second;

		m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Info << "New trainer object : id=" << targetId << " litFrames=" << framesL <<
				" darkFrames=" << framesD << "\n";

		configManager->releaseConfigurationToken(id);

		return new CTrainerFlickeringObject(targetX, targetY, currentTargetColour, uint8_t(framesL), uint8_t(framesD));
	}
	m_application->getLogManager() << OpenViBE::Kernel::LogLevel_Fatal << "TrainerTarget object was not properly initialized\n";
	return nullptr;
}

CTrainerFlickeringObject::CTrainerFlickeringObject(const float posX, const float posY, const ColourValue colour, const uint8_t litFrames,
												   const uint8_t darkFrames)
	: CSSVEPFlickeringObject(nullptr, litFrames, darkFrames)
{
	m_elementNode          = m_parentNode->createChildSceneNode();
	m_objectNode           = m_elementNode->createChildSceneNode();
	SceneNode* pointerNode = m_elementNode->createChildSceneNode();

	const RealRect rectangle(posX - m_targetWidth / 2, posY + m_targetHeight / 2, posX + m_targetWidth / 2, posY - m_targetHeight / 2);

	MovableObject* litObject = m_painter->paintRectangle(rectangle, colour);

	m_objectNode->attachObject(litObject);
	litObject->setVisible(true);

	MovableObject* darkObject = m_painter->paintRectangle(rectangle, m_darkColor);
	m_objectNode->attachObject(darkObject);
	darkObject->setVisible(false);

	m_pointer = m_painter->paintTriangle(Point(posX - 0.05F, posY + m_targetHeight), Point(posX, posY + m_targetHeight - 0.05F),
										 Point(posX + 0.05F, posY + m_targetHeight), ColourValue(1, 1, 0));

	pointerNode->attachObject(m_pointer);
	m_pointer->setVisible(false);
}

#endif
