#pragma once

#include "../ovassvepICommandVRPNButton.h"

namespace SSVEP {
class CTrainerApplication;

class CCommandReceiveTarget final : public ICommandVRPNButton
{
public:
	explicit CCommandReceiveTarget(CApplication* application);
	~CCommandReceiveTarget() override { }

	void execute(const int button, const int state) override;
};
}  // namespace SSVEP
