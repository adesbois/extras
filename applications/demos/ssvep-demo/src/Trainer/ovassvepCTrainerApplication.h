#pragma once

#include <CEGUI.h>

#include "../ovassvepCApplication.h"
#include "ovassvepCTrainerFlickeringObject.h"

namespace SSVEP {
class CTrainerApplication final : public CApplication
{
public:
	CTrainerApplication();
	~CTrainerApplication() override {}

	bool setup(OpenViBE::Kernel::IKernelContext* kernelCtx) override;
	void setTarget(int target);

	void startExperiment() override;
	void startFlickering() override;
	void stopFlickering() override;


private:
	bool m_active = false;
	void processFrame(const size_t frame) override;
	void addObject(CTrainerFlickeringObject* obj);

	std::vector<CTrainerFlickeringObject*> m_oObjects;

	time_t m_ttStartTime = 0;

	CEGUI::Window* m_instructionsReady = nullptr;
};
}  // namespace SSVEP
