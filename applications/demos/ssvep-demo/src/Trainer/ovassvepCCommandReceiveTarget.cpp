#if defined(TARGET_HAS_ThirdPartyOgre3D)

#include "ovassvepCCommandReceiveTarget.h"
#include "ovassvepCTrainerApplication.h"

using namespace SSVEP;

CCommandReceiveTarget::CCommandReceiveTarget(CApplication* application)
	: ICommandVRPNButton(application, "SSVEP_VRPN_Target") {}

void CCommandReceiveTarget::execute(const int button, const int /*state*/) { dynamic_cast<CTrainerApplication*>(m_application)->setTarget(button); }

#endif
