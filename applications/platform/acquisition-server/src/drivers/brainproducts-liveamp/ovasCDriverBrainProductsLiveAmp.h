#pragma once

#if defined TARGET_HAS_ThirdPartyLiveAmpAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"
#include <openvibe/ov_all.h>

#include "../ovasCSettingsHelper.h"
#include "../ovasCSettingsHelperOperators.h"

#include <deque>

#ifndef BYTE
typedef unsigned char BYTE;
#endif

namespace OpenViBE {
namespace AcquisitionServer {
/**
 * \class CDriverBrainProductsLiveAmp
 * \author Ratko Petrovic (Brain Products GmbH)
 * \date Mon Nov 21 14:57:37 2016
 * \brief The CDriverBrainProductsLiveAmp allows the acquisition server to acquire data from a Brain Products LiveAmp device.
 *
 * TODO: details
 *
 * \sa CConfigurationBrainProductsLiveAmp
 */
class CDriverBrainProductsLiveAmp final : public IDriver
{
public:

	explicit CDriverBrainProductsLiveAmp(IDriverContext& ctx);
	~CDriverBrainProductsLiveAmp() override { uninitialize(); }

	const char* getName() override { return "Brain Products LiveAmp"; }

	bool initialize(const uint32_t nSamplePerSentBlock, IDriverCallback& callback) override;
	bool uninitialize() override;

	bool start() override;
	bool stop() override;
	bool loop() override;

	bool isConfigurable() override;
	bool configure() override;
	const IHeader* getHeader() override { return &m_header; }

	bool isFlagSet(const EDriverFlag flag) const override
	{
		if (flag == EDriverFlag::IsUnstable) { return false; }	// switch to "Stable" on 3.5.2017 - RP
		return false; // No flags are set
	}

private:

	SettingsHelper m_settings;
	IDriverCallback* m_callback = nullptr;
	void* m_handle              = nullptr;

	CHeader m_header;
	CStimulationSet m_stimSet;
	uint32_t m_nSamplePerSentBlock = 0;
	uint8_t* m_sampleBuffer        = nullptr;

	uint32_t m_bufferSize = 0;
	uint32_t m_sampleSize = 0;

	uint32_t m_physicalSampling  = 250;
	bool m_useAccChannels        = false;
	bool m_useBipolarChannels    = false;
	uint32_t m_impedanceChannels = 0;
	uint32_t m_nUsedChannels     = 0;
	uint32_t m_nEnabledChannels  = 0;

	uint32_t m_nChannel           = 0;
	uint32_t m_nEEG               = 32;
	uint32_t m_nAux               = 0;
	uint32_t m_nACC               = 0;
	uint32_t m_nBipolar           = 0;
	uint32_t m_nTriggersIn        = 0;
	uint32_t m_goodImpedanceLimit = 5000;
	uint32_t m_badImpedanceLimit  = 10000;

	std::string m_sSerialNumber = "05203-0077";
	int m_iRecordingMode        = -1;

	std::vector<float> m_samples;
	std::deque<std::vector<float>> m_sampleCaches;
	std::vector<std::vector<float>> m_sendBuffers;
	std::vector<size_t> m_samplings;
	std::vector<uint16_t> m_triggerIndices;
	std::vector<uint16_t> m_lastTriggerStates;
	std::vector<uint16_t> m_dataTypes;
	std::vector<float> m_resolutions;

	bool m_bSimulationMode = false;
	/**
	* \function initializeLiveAmp
	* \return True if LiveAmp is successful initialized. 
	* \brief Establish BT connection with the LiveAmp amplifier, identified with the serial number.
	*/
	bool initializeLiveAmp();

	/**
	* \function configureLiveAmp
	* \return True if LiveAmp is successful configured. 
	* \brief Configure LiveAmp, sampling rate and mode.
	*/
	bool configureLiveAmp();

	/**
	* \function checkAvailableChannels
	* \return True if checking is successful. 
	* \brief Check how many available channels has LiveAmp. Get count of EEG,AUX and ACC channels.
		Compares the count with the amplifier/driver settings.
	*/
	bool checkAvailableChannels();

	/**
	* \function disableAllAvailableChannels
	* \return True if function is successful. 
	* \brief 
	*/
	bool disableAllAvailableChannels();

	/**
	* \function getChannelIndices
	* \return True if function is successful.  
	* \brief Get indexes of the channel that will be used for acquisition.
	*/
	bool getChannelIndices();

	/**
	* \function impedanceMessureInit
	* \return True if function is successful.  
	* \brief Configures impedance measurement. 
	*/
	bool configureImpedanceMessure();

	/**
	* \function getLiveAmpSampleSize
	* \return Size of 'one' acquired sample. 
	* \brief Calcualtes the size of 'one' sample that LiveAmp delivers. It is actually a sum of the one sample per each channel that will be acquired.
	   This information is needed to read data from buffer, in order to access to each sample of each channel.     
	*/
	uint32_t getLiveAmpSampleSize();

	/**
	* \function liveAmpExtractData
	* \param samplesRead[in] : count of samples read (obtained) from the amplifier 
	* \param extractData[out] : extract data from the buffer
	* \return 
	* \brief Extracts acquired data from the buffer, and puts them into the double vector 'extractData' , in the appropriate format.
	*/
	void liveAmpExtractData(int samplesRead, std::vector<std::vector<float>>& extractData);

	/**
	* \function liveAmpExtractImpedanceData
	* \param samplesRead[in] : count of acquired samples from the amplifier 
	* \param extractData[out] : extract data from the buffer
	* \return 
	* \brief Extracts acquired impedance measurements from the buffer, and puts them into the double vector 'extractData', in the appropriate format. 
	*/
	void liveAmpExtractImpedanceData(int samplesRead, std::vector<std::vector<float>>& extractData);
};
}  //namespace AcquisitionServer
}  //namespace OpenViBE

#endif // TARGET_HAS_ThirdPartyLiveAmpAPI
