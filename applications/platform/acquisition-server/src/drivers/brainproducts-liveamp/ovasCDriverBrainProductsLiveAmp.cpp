/*
 * Brain Products LiveAmp driver for OpenViBE
 * Copyright (C) 2017 Brain Products - Author : Ratko Petrovic
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */
/*********************************************************************
* History
* [2017-03-29] ver 1.0											          - RP
* [2017-04-04] ver 1.1 Cosmetic changes: int/uint32_t, static_cast<>...   - RP
*              Function loop: optimized buffer copying. 
* [2017-04-28] ver 1.2 LiveAmp8 and LiveAmp16 channles support added.     - RP
*			   Introduced checking of Bipolar channels.
*
*********************************************************************/
#if defined TARGET_HAS_ThirdPartyLiveAmpAPI

#include "ovasCDriverBrainProductsLiveAmp.h"
#include "ovasCConfigurationBrainProductsLiveAmp.h"

#include <toolkit/ovtk_all.h>
#include <Amplifier_LIB.h>

using namespace OpenViBE;
using namespace /*OpenViBE::*/AcquisitionServer;
using namespace /*OpenViBE::*/Kernel;
using namespace std;


//___________________________________________________________________//
//                                                                   //
CDriverBrainProductsLiveAmp::CDriverBrainProductsLiveAmp(IDriverContext& ctx)
	: IDriver(ctx), m_settings("AcquisitionServer_Driver_BrainProductsLiveAmp", m_driverCtx.getConfigurationManager())
{
	// set default sampling rates:
	m_samplings.push_back(250);
	m_samplings.push_back(500);
	m_samplings.push_back(1000);

	m_header.setSamplingFrequency(250); // init for the first time

	// The following class allows saving and loading driver settings from the acquisition server .conf file	
	m_settings.add("EEGchannels", &m_nEEG);
	m_settings.add("AUXchannels", &m_nAux);
	m_settings.add("ACCchannels", &m_nACC);
	m_settings.add("BipolarChannels", &m_nBipolar);
	m_settings.add("IncludeACC", &m_useAccChannels);

	m_settings.add("SerialNr", &m_sSerialNumber);
	m_settings.add("GoodImpedanceLimit", &m_goodImpedanceLimit);
	m_settings.add("BadImpedanceLimit", &m_badImpedanceLimit);
	m_settings.add("Header", &m_header);
	m_settings.add("UseBipolarChannels", &m_useBipolarChannels);	//m_settings.add("SettingName", &variable);

	// To save your custom driver settings, register each variable to the SettingsHelper	
	m_settings.load();

	m_physicalSampling = m_header.getSamplingFrequency();
	m_header.setChannelCount(m_nEEG + m_nBipolar + m_nAux + m_nACC);
}

//___________________________________________________________________//
//                                                                   //
bool CDriverBrainProductsLiveAmp::initialize(const uint32_t nSamplePerSentBlock, IDriverCallback& callback)
{
	m_nSamplePerSentBlock = nSamplePerSentBlock;
	m_driverCtx.getLogManager() << LogLevel_Info << "[INIT] LiveAmp initialization has been started. " << "\n";

	if (m_driverCtx.isConnected() || !m_header.isChannelCountSet() || !m_header.isSamplingFrequencySet()) { return false; }

	m_driverCtx.getLogManager() << LogLevel_Info << "[INIT] LiveAmp initialization process is running, please wait..." << "\n";

	// Builds up a buffer to store
	// acquired samples. This buffer
	// will be sent to the acquisition
	// server later...
	if (m_sampleBuffer != nullptr)
	{
		delete [] m_sampleBuffer;
		m_sampleBuffer = nullptr;
	}

	// ...
	// initialize hardware and get
	if (!initializeLiveAmp()) { return false; }

	m_driverCtx.getLogManager() << LogLevel_Info << "[INIT] LiveAmp successfully initialized! \n";

	if (!configureLiveAmp() || !checkAvailableChannels() || !disableAllAvailableChannels()
	) { return false; }	// must disable all channels, than enable only one that will be used.
	if (!getChannelIndices() || !configureImpedanceMessure()) { return false; }

	// check if the impedance checking is possible:
	if (m_driverCtx.isImpedanceCheckRequested())
	{
		if (m_nEEG > 0)
		{
			int type      = -1;
			const int res = ampGetProperty(m_handle, PG_CHANNEL, 0, CPROP_I32_Electrode, &type, sizeof(type));
			if (res != AMP_OK)
			{
				m_driverCtx.getLogManager() << LogLevel_Error << "[configure] GetProperty type error: " << res << "\n";
				return false;
			}

			/*if(type == EL_ACTIVE)
			{
				m_header.setImpedanceCheckRequested (false);
				m_driverCtx.getLogManager() << LogLevel_Warning << "[configure] Cannot provide impedance checking with attached electrodes! \n";
			}*/
		}
	}

	if (!m_driverCtx.isImpedanceCheckRequested())
	{
		// calculate what size has each "sample"
		m_sampleSize = getLiveAmpSampleSize();
		if (m_nEnabledChannels != m_nUsedChannels)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "m_nUsedChannels: " << m_nUsedChannels << " !=  m_nEnabledChannels= "
					<< m_nEnabledChannels << "\n";
		}

		m_bufferSize   = 1000 * m_sampleSize;
		m_sampleBuffer = new uint8_t[m_bufferSize];
	}
	else
	{
		m_sampleSize = m_impedanceChannels * sizeof(float);

		m_bufferSize   = 1000 * m_sampleSize;
		m_sampleBuffer = new uint8_t[m_bufferSize];

		const int res = ampStartAcquisition(m_handle);
		if (res != AMP_OK) { m_driverCtx.getLogManager() << LogLevel_Trace << " [Impedance] Device Handle = NULL.\n"; }

		m_driverCtx.getLogManager() << LogLevel_Trace << "Impedance Acquisition started...\n";
	}

	m_sendBuffers.clear();

	m_samples.clear();
	m_samples.resize(m_header.getChannelCount());// allocate for all analog signals / channels

	// Saves parameters
	m_callback            = &callback;
	m_nSamplePerSentBlock = nSamplePerSentBlock;

	return true;
}


bool CDriverBrainProductsLiveAmp::start()
{
	m_driverCtx.getLogManager() << LogLevel_Trace << "START called.\n";
	if (!m_driverCtx.isConnected() || m_driverCtx.isStarted()) { return false; }

	if (m_handle == nullptr) { m_driverCtx.getLogManager() << LogLevel_Error << "Device Handle = NULL.\n"; }

	int res = ampGetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_RecordingMode, &m_iRecordingMode, sizeof(m_iRecordingMode));
	if (m_iRecordingMode != RM_NORMAL)
	{
		res = ampStopAcquisition(m_handle);
		if (res != AMP_OK) { m_driverCtx.getLogManager() << LogLevel_Error << " [start] ampStopAcquisition error code = " << res << "\n"; }

		m_iRecordingMode = RM_NORMAL;
		res              = ampSetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_RecordingMode, &m_iRecordingMode, sizeof(m_iRecordingMode));
		if (res != AMP_OK) { m_driverCtx.getLogManager() << LogLevel_Error << " [start] ampSetProperty mode error code = " << res << "\n"; }

		// calculate what size has each "sample"
		m_sampleSize = getLiveAmpSampleSize();
		if (m_nEnabledChannels != m_nUsedChannels)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "Used Channels: " << m_nUsedChannels << " !=  Enabled Channels= " << m_nEnabledChannels << "\n";
		}
		delete [] m_sampleBuffer;
		m_sampleBuffer = nullptr;

		m_bufferSize   = 1000 * m_sampleSize;
		m_sampleBuffer = new BYTE[m_bufferSize];
		m_sendBuffers.clear();
		m_samples.clear();
		m_samples.resize(m_header.getChannelCount());// allocate for all analog signals / channels
	}

	res = ampStartAcquisition(m_handle);
	if (res != AMP_OK) { m_driverCtx.getLogManager() << LogLevel_Error << " [start] ampStartAcquisition error code = " << res << "\n"; }

	m_driverCtx.getLogManager() << LogLevel_Trace << "Acquisition started...\n";

	return true;
}

bool CDriverBrainProductsLiveAmp::loop()
{
	if (!m_driverCtx.isConnected()) { return false; }

	if (m_driverCtx.isStarted())
	{
		//CStimulationSet stimSet;
		std::vector<std::vector<float>> buffers(1, std::vector<float>(1));

		while (true)
		{
			// receive samples from hardware
			// put them the correct way in the sample array
			// whether the buffer is full, send it to the acquisition server
			const int samplesRead = ampGetData(m_handle, m_sampleBuffer, m_bufferSize, 0);
			if (samplesRead < 1) { return true; }

			const size_t nLocChannel = m_header.getChannelCount();

			liveAmpExtractData(samplesRead, buffers);

			const size_t nSample = size_t(samplesRead) / m_sampleSize;
			for (size_t sample = 0; sample < nSample; ++sample)
			{
				std::vector<float> buffer(m_nUsedChannels);
				for (size_t ch = 0; ch < m_nUsedChannels; ++ch) { buffer[ch] = buffers[sample][ch]; }	// access only with available indices 
				m_sendBuffers.push_back(buffer);
			}

			const int readToSend = int(m_sendBuffers.size()) - int(m_nSamplePerSentBlock); // must check buffer size in that way !!!
			if (readToSend > 0)
			{
				// for debug only: m_driverCtx.getLogManager() << LogLevel_Info << "[info] Buffer size = " << m_sendBuffers.size() << "\n";
				m_samples.clear();
				m_samples.resize(nLocChannel * m_nSamplePerSentBlock);

				for (size_t ch = 0, i = 0; ch < m_header.getChannelCount(); ++ch)
				{
					for (size_t sample = 0; sample < m_nSamplePerSentBlock; ++sample) { m_samples[i++] = m_sendBuffers[sample][ch]; }
				}

				// receive events from hardware
				// and put them the correct way in a CStimulationSet object
				//m_callback->setStimulationSet(stimSet);
				for (size_t sample = 0; sample < m_nSamplePerSentBlock; ++sample)
				{	// check triggers:
					for (size_t t = 0; t < m_triggerIndices.size(); ++t)
					{
						const uint16_t trigg = uint16_t(m_sendBuffers[sample][m_triggerIndices[t]]);
						if (trigg != m_lastTriggerStates[t])
						{
							const uint64_t time = CTime(m_header.getSamplingFrequency(), sample).time();
							m_stimSet.appendStimulation(OVTK_StimulationId_Label(trigg), time, 0); // send the same time as the 'sample'
							m_lastTriggerStates[t] = trigg;
						}
					}
				}

				// send acquired data...
				m_callback->setSamples(&m_samples[0], m_nSamplePerSentBlock);
				m_callback->setStimulationSet(m_stimSet);

				m_samples.clear();
				m_stimSet.clear();
				// When your sample buffer is fully loaded, 
				// it is advised to ask the acquisition server 
				// to correct any drift in the acquisition automatically.
				m_driverCtx.correctDriftSampleCount(m_driverCtx.getSuggestedDriftCorrectionSampleCount());

				// delete sent samples
				m_sendBuffers.erase(m_sendBuffers.begin(), m_sendBuffers.begin() + m_nSamplePerSentBlock);
				break;
			}
			if (m_bSimulationMode) { Sleep(100); }
		}
	}
	else if (m_driverCtx.isImpedanceCheckRequested()) //impedance measurement 
	{
		if (m_iRecordingMode != RM_IMPEDANCE) { return true; }	// go out of the loop

		std::vector<std::vector<float>> buffers(1, std::vector<float>(1));
		const uint32_t samplesRead = ampGetImpedanceData(m_handle, m_sampleBuffer, m_bufferSize);
		if (samplesRead < 1) { return true; }

		liveAmpExtractImpedanceData(samplesRead, buffers);

		const size_t nMeasuredChannels = (m_impedanceChannels - 2) /
										 2; // GND, REF and pairs of ch+ and ch-, countOfMeasuredChannles can be different than  m_header.getChannelCount()

		const uint32_t nSample = samplesRead / m_sampleSize;
		for (uint32_t sample = 0; sample < nSample; ++sample)
		{
			// must extract impedance values for each cEEG channel, and or Bipolar channel
			for (uint32_t ch = 0; ch < nMeasuredChannels; ++ch)
			{
				int type;
				int res = ampGetProperty(m_handle, PG_CHANNEL, ch, CPROP_I32_Type, &type, sizeof(type));
				if (res != AMP_OK)
				{
					m_driverCtx.getLogManager() << LogLevel_Error << "[ImpMeas] GetProperty type error: " << res << "\n";
					break;
				}

				if (type == CT_EEG) { m_driverCtx.updateImpedance(ch, buffers[sample][2 * (ch + 1)]); }
				else if (type == CT_BIP) { m_driverCtx.updateImpedance(ch, buffers[sample][2 * (ch + 1)] - buffers[sample][2 * (ch + 1) + 1]); }
			}
		}
	}

	return true;
}

bool CDriverBrainProductsLiveAmp::stop()
{
	if (!m_driverCtx.isConnected() || !m_driverCtx.isStarted()) { return false; }

	// ...
	// request the hardware to stop
	// sending data
	// ...
	if (m_handle != nullptr)
	{
		const int res = ampStopAcquisition(m_handle);
		m_driverCtx.getLogManager() << LogLevel_Trace << "'stop' called. stopping the acquisition:" << res << "\n";
	}

	return true;
}

bool CDriverBrainProductsLiveAmp::uninitialize()
{
	if (!m_driverCtx.isConnected() || m_driverCtx.isStarted()) { return false; }

	// ...
	// uninitialize hardware here
	// ...
	if (m_handle != nullptr)
	{
		int res = ampStopAcquisition(m_handle);
		if (res != AMP_OK) { m_driverCtx.getLogManager() << LogLevel_Trace << "Uninitialized called. stopping acquisition:" << res << "\n"; }

		res = ampCloseDevice(m_handle);
		m_driverCtx.getLogManager() << LogLevel_Trace << "Uninitialized called. Closing the device:" << res << "\n";
	}


	delete [] m_sampleBuffer;
	m_sampleBuffer = nullptr;
	m_callback     = nullptr;
	m_samples.clear();

	return true;
}

//___________________________________________________________________//
//                                                                   //
bool CDriverBrainProductsLiveAmp::isConfigurable()
{
	return true; // change to false if your device is not configurable
}

bool CDriverBrainProductsLiveAmp::configure()
{
	// get sampling rate index:
	uint32_t sampRateIndex = -1;
	for (uint32_t i = 0; i < m_samplings.size(); ++i) { if (m_physicalSampling == m_samplings[i]) { sampRateIndex = i; } }


	// Change this line if you need to specify some references to your driver attribute that need configuration, e.g. the connection ID.
	CConfigurationBrainProductsLiveAmp config(*this, Directories::getDataDir() + "/applications/acquisition-server/interface-BrainProductsLiveAmp.ui",
											  sampRateIndex, m_nEEG, m_nBipolar, m_nAux, m_nACC, m_useAccChannels,
											  m_goodImpedanceLimit, m_badImpedanceLimit, m_sSerialNumber, m_useBipolarChannels);

	if (!config.configure(m_header)) { return false; }

	// update sampling rate
	if (sampRateIndex >= 0 && sampRateIndex < m_samplings.size()) { m_physicalSampling = m_samplings[sampRateIndex]; }

	m_header.setSamplingFrequency(m_physicalSampling);
	m_header.setChannelCount(m_nEEG + m_nBipolar + m_nAux + m_nACC);

	m_settings.save();

	return true;
}


bool CDriverBrainProductsLiveAmp::initializeLiveAmp()
{
	HANDLE hlocDevice = nullptr;
	char hwi[20];
	strcpy_s(hwi, "ANY");  // use word SIM to simulate the LiveAmp
	std::string serialN(m_sSerialNumber);

	if (strcmp(hwi, "SIM") == 0)
	{
		m_bSimulationMode = true;
		serialN           = "054201-0001";   // 32 channels
		//serialN = "054201-0010"; // 64 channels
	}

	if (m_handle != nullptr)
	{
		ampCloseDevice(m_handle);
		m_handle = nullptr;
	}

	// dialog window doesn't show all meassage, due to threading problems. The information will be seen in Dialog title.
	GtkWindow* window = GTK_WINDOW(gtk_window_new (GTK_WINDOW_TOPLEVEL));
	GtkWidget* dialog = gtk_message_dialog_new(window, GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_NONE,
											   "  Connecting to LiveAmp, wait some seconds please ...    ");
	gtk_window_set_title(GTK_WINDOW(dialog), " Connecting to LiveAmp, please wait...");

	gtk_window_set_position(GTK_WINDOW(dialog), GTK_WIN_POS_CENTER);
	gtk_widget_show_all(dialog);


	int res = ampEnumerateDevices(hwi, sizeof(hwi), "LiveAmp", 0);
	if (res == 0)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[InitializeLiveAmp] No LiveAmp connected! \n";
		gtk_widget_destroy(dialog);
		return false;
	}
	if (res < 0)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[InitializeLiveAmp] Error by LiveAmp initialization; error code= " << res << "\n";
		gtk_widget_destroy(dialog);
		return false;
	}
	const int numDevices = res;
	for (int i = 0; i < numDevices; ++i)
	{
		hlocDevice = nullptr;
		res        = ampOpenDevice(i, &hlocDevice);
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "[InitializeLiveAmp] Cannot open device # " << i << "; error code = " << res << "\n";
			gtk_widget_destroy(dialog);
			return false;
		}

		char sVar[20];
		res = ampGetProperty(hlocDevice, PG_DEVICE, i, DPROP_CHR_SerialNumber, sVar, sizeof(sVar)); // get serial number
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "[InitializeLiveAmp] Cannot read Serial number from device # " << i << ";  error code = "
					<< res << "\n";
			gtk_widget_destroy(dialog);
			return false;
		}

		m_driverCtx.getLogManager() << LogLevel_Info << "[InitializeLiveAmp] Serial number = " << sVar << "\n";

		const int check = strcmp(sVar, serialN.c_str());
		if (check == 0)
		{
			m_driverCtx.getLogManager() << LogLevel_Info << "[InitializeLiveAmp] Serial numbers match: " << serialN << "\n";
			m_handle = hlocDevice; // save device handler
			gtk_widget_destroy(dialog);
			return true;
		}
		res = ampCloseDevice(hlocDevice);
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "[InitializeLiveAmp] Cannot close the device # " << i << "; error code = " << res << "\n";
			gtk_widget_destroy(dialog);
			return false;
		}
	}

	gtk_widget_destroy(dialog);

	if (m_handle == nullptr)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[InitializeLiveAmp] There is no LiveAmp with serial number: " << serialN << " detected!";
		return false;
	}

	return false;
}

bool CDriverBrainProductsLiveAmp::configureLiveAmp()
{
	// amplifier configuration
	float var = float(m_header.getSamplingFrequency());
	int res   = ampSetProperty(m_handle, PG_DEVICE, 0, DPROP_F32_BaseSampleRate, &var, sizeof(var));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[Config]  Error setting sampling rate, error code:  " << res << "\n";
		return false;
	}

	m_driverCtx.getLogManager() << LogLevel_Info << "[Config]  Set sampling frequency = " << m_header.getSamplingFrequency() << "\n";


	m_iRecordingMode = RM_NORMAL;  // initialize acquisition mode: standard/normal
	res              = ampSetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_RecordingMode, &m_iRecordingMode, sizeof(m_iRecordingMode));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[Config] Error setting acquisition mode, error code:  " << res << "\n";
		return false;
	}

	// set good and bad impedance level
	res = ampSetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_GoodImpedanceLevel, &m_goodImpedanceLimit, sizeof(m_goodImpedanceLimit));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[Config] Error setting DPROP_I32_GoodImpedanceLevel, error code:  " << res << "\n";
		return false;
	}

	res = ampSetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_BadImpedanceLevel, &m_badImpedanceLimit, sizeof(m_badImpedanceLimit));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[Config] Error setting DPROP_I32_BadImpedanceLevel, error code:  " << res << "\n";
		return false;
	}

	return true;
}

bool CDriverBrainProductsLiveAmp::checkAvailableChannels()
{
	// check the "LiveAmp_Channel" version: LiveAmp8, LiveAmp16, LiveAmp32 or LiveAmp64
	uint32_t moduleChannels; // check number of channels that are allowed!
	int res = ampGetProperty(m_handle, PG_MODULE, 0, MPROP_I32_UseableChannels, &moduleChannels, sizeof(moduleChannels));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "#1 MPROP_I32_UseableChannels, error code= " << res << "\n";
		return false;
	}

	// checks available channels, gets the count of each channel type
	uint32_t availableEEG  = 0;
	uint32_t availableAUX  = 0;
	uint32_t availableACC  = 0;
	uint32_t availableTrig = 0;

	int avlbchannels;
	res = ampGetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_AvailableChannels, &avlbchannels, sizeof(avlbchannels));

	for (int c = 0; c < avlbchannels; ++c)
	{
		int type;
		res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_I32_Type, &type, sizeof(type));
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "[Check] GetProperty type error: " << res << "\n";
			return false;
		}

		if (type == CT_AUX)
		{
			char value[20];
			res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_CHR_Function, &value, sizeof(value));
			if (res != AMP_OK)
			{
				m_driverCtx.getLogManager() << LogLevel_Error << "[Check] GetProperty CPROP_CHR_Function #1 error: " << res << "\n";
				return false;
			}

			if (value[0] == 'X' || value[0] == 'Y' || value[0] == 'Z' || value[0] == 'x' || value[0] == 'y' || value[0] == 'z') { availableACC++; }
			else { availableAUX++; }
		}
		else if (type == CT_EEG || type == CT_BIP) { availableEEG++; }
		else if (type == CT_TRG || type == CT_DIG)
		{
			char cValue[20];
			res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_CHR_Function, &cValue, sizeof(cValue));
			if (res != AMP_OK)
			{
				m_driverCtx.getLogManager() << LogLevel_Error << "[Check] GetProperty CPROP_CHR_Function #2 error: " << res << "\n";
				return false;
			}
			if (strcmp("Trigger Input", cValue) == 0) { availableTrig += 1 + 8; }	// One LiveAmp trigger input + 8 digital inputs from AUX box		 
		}
	}


	//*********************************************************************************
	// very important check !!! EEG + Bipolar must match configuration limitations
	//*********************************************************************************
	if (moduleChannels == 32)
	{
		// if there is any Bipolar channel, it means that last 8 physical channels must be Bipolar
		if (m_nBipolar > 0 && m_nEEG > (32 - 8))
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "[Check] Number of EEG channels:" << m_nEEG << " and Bipolar channels: " <<
					m_nBipolar << " don't match the LiveAmp configuration !!!\n";
			return false;
		}
	}


	// Used EEG channels:
	if (m_nEEG + m_nBipolar > moduleChannels)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[Check] Number of used EEG and Bip. channels '" << (m_nEEG + m_nBipolar) <<
				"' don't match with number of channels from LiveAmp Channel configuration '" << moduleChannels << "\n";
		return false;
	}
	if (m_nEEG + m_nBipolar > availableEEG)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[Check] Number of available EEG channels '" << availableEEG <<
				"' don't match with number of channels from Device configuration '" << m_nEEG << "\n";
		return false;
	}
	if (m_header.getSamplingFrequency() >= 1000 && moduleChannels >= 32 && (m_nEEG + m_nBipolar) > 24)
	{
		m_driverCtx.getLogManager() << LogLevel_Error <<
				"If the sampling rate is 1000Hz, there should be 24 EEG (or 21 EEG and 3 AUX)  channels used, to avoid sample loss due to Bluetooth connection.\n";
		return false;
	}


	if (m_nAux > availableAUX)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Number of input AUX channeles (" << m_nAux <<
				") don't match available number of AUX channels (" << availableAUX << ") \n";
		return false;
	}

	if (m_header.getSamplingFrequency() >= 1000 && moduleChannels >= 32 && (m_nEEG + m_nBipolar + m_nAux + m_nACC) > 24)
	{
		m_driverCtx.getLogManager() << LogLevel_Error <<
				"If the sampling rate is 1000Hz, there should be 24 EEG (or 21 EEG and 3 AUX)  channels used, to avoid sample loss due to Bluetooth connection. \n ";
		return false;
	}

	return true;
}

bool CDriverBrainProductsLiveAmp::disableAllAvailableChannels()
{
	// disables all channle first. It is better to do so, than to enable only the channels that will be used, according to the driver settings.
	int avlbchannels;
	int res = ampGetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_AvailableChannels, &avlbchannels, sizeof(avlbchannels));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << " Get available channels, error code= " << res << "\n";
		return false;
	}


	// now disable all channel first,	
	for (int c = 0; c < avlbchannels; ++c)
	{
		BOOL disabled = false;
		int type      = 0;

		res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_I32_Type, &type, sizeof(type));
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << " Error: ampGetProperty for channel type, channel= " << c << "; error code= " << res <<
					"\n";
			return false;
		}

		// can not disable trigger and digital channels.
		if (type == CT_DIG || type == CT_TRG) { continue; }

		res = ampSetProperty(m_handle, PG_CHANNEL, c, CPROP_B32_RecordingEnabled, &disabled, sizeof(disabled));
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << " Error: ampGetProperty for channel type, channel= " << c << "; error code= " << res << "\n";
			return false;
		}
	}

	return true;
}


bool CDriverBrainProductsLiveAmp::getChannelIndices()
{
	int enable         = 1;
	m_nEnabledChannels = 0;
	m_triggerIndices.clear();

	std::vector<int> accessIndices;  // indexes of physical channel 

	int avlbchannels;
	int res = ampGetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_AvailableChannels, &avlbchannels, sizeof(avlbchannels));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "#3 Get available channels, error code= " << res << "\n";
		return false;
	}


	// check the "LiveAmp_Channel" version: LiveAmp8, LiveAmp16, LiveAmp32 or LiveAmp64
	uint32_t moduleChannels;
	res = ampGetProperty(m_handle, PG_MODULE, 0, MPROP_I32_UseableChannels, &moduleChannels, sizeof(moduleChannels));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "#2 MPROP_I32_UseableChannels, error code= " << res << "\n";
		return false;
	}

	// enable channels and get indexes of channels to be used!
	// The order of physical channels by LiveAmp: EEGs, BIPs, AUXs, ACCs, TRIGs 
	uint32_t nEEG = 0;
	uint32_t nBip = 0;
	uint32_t nAux = 0;
	uint32_t nAcc = 0;

	for (int c = 0; c < avlbchannels; ++c)
	{
		int type;
		res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_I32_Type, &type, sizeof(type));
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "[Check] GetProperty type error: " << res << "\n";
			return false;
		}

		// type of channel is first EEG. After one of the channle is re-typed as Bipolar, that the rest of 8 channels in the group of 32 will be Bipolar as well
		if (type == CT_EEG || type == CT_BIP)
		{
			if (nEEG < m_nEEG)
			{
				res = ampSetProperty(m_handle, PG_CHANNEL, c, CPROP_B32_RecordingEnabled, &enable, sizeof(enable));
				if (res != AMP_OK)
				{
					m_driverCtx.getLogManager() << LogLevel_Error << " Cannot enable channel: " << c << "; error code= " << res << "\n";
					return false;
				}

				accessIndices.push_back(m_nEnabledChannels);
				m_nEnabledChannels++;
				nEEG++;
			}
			else if (nBip < m_nBipolar)
			{
				//*********************************************************************************
				// If Bipolar channel will be used, set the type to Bipolar here!!!
				// Still it works for LiveAmp8, LiveAmp16 and LiveAmp32.
				// Bipolar channels can be only phisical channels from index 24 - 31 !!!
				//*********************************************************************************
				if ((c > 23 && c < 32) || (moduleChannels > 32 && c > 55 && c < 64))  // last 8 channels of 32ch module can be bipolar channels 
				{
					res = ampSetProperty(m_handle, PG_CHANNEL, c, CPROP_B32_RecordingEnabled, &enable, sizeof(enable));
					if (res != AMP_OK)
					{
						m_driverCtx.getLogManager() << LogLevel_Error << " Cannot enable channel: " << c << "; error code= " << res << "\n";
						return false;
					}

					int tp = CT_BIP;
					res    = ampSetProperty(m_handle, PG_CHANNEL, c, CPROP_I32_Type, &tp, sizeof(tp));
					if (res != AMP_OK)
					{
						m_driverCtx.getLogManager() << LogLevel_Error << "[Check] SetProperty type CT_BIP error: " << res << "\n";
						return false;
					}

					accessIndices.push_back(m_nEnabledChannels);
					m_nEnabledChannels++;
					nBip++;
				}
			}
		}
		else if (type == CT_AUX)
		{
			char cValue[20];
			res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_CHR_Function, &cValue, sizeof(cValue));
			if (res != AMP_OK)
			{
				m_driverCtx.getLogManager() << LogLevel_Error << "[Check] GetProperty CPROP_CHR_Function #1 error: " << res << "\n";
				return false;
			}

			// detect ACC channels
			if (cValue[0] == 'X' || cValue[0] == 'Y' || cValue[0] == 'Z' || cValue[0] == 'x' || cValue[0] == 'y' || cValue[0] == 'z')
			{
				if (nAcc < m_nACC)
				{
					res = ampSetProperty(m_handle, PG_CHANNEL, c, CPROP_B32_RecordingEnabled, &enable, sizeof(enable));
					if (res != AMP_OK)
					{
						m_driverCtx.getLogManager() << LogLevel_Error << " Cannot enable channel: " << c << "; error code= " << res << "\n";
						return false;
					}
					accessIndices.push_back(m_nEnabledChannels);
					m_nEnabledChannels++;
					nAcc++;
				}
			}
			else
			{
				if (nAux < m_nAux)
				{
					res = ampSetProperty(m_handle, PG_CHANNEL, c, CPROP_B32_RecordingEnabled, &enable, sizeof(enable));
					if (res != AMP_OK)
					{
						m_driverCtx.getLogManager() << LogLevel_Error << " Cannot enable channel: " << c << "; error code= " << res << "\n";
						return false;
					}
					accessIndices.push_back(m_nEnabledChannels);
					m_nEnabledChannels++;
					nAux++;
				}
			}
		}

		else if (type == CT_TRG || type == CT_DIG)  // those channels are always enabled!
		{
			char cValue[20];
			res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_CHR_Function, &cValue, sizeof(cValue));
			if (res != AMP_OK)
			{
				m_driverCtx.getLogManager() << LogLevel_Error << " GetProperty CPROP_CHR_Function error by Trigger, error code= " << res << "\n";
				return false;
			}

			if (strcmp("Trigger Input", cValue) == 0) { m_triggerIndices.push_back(m_nEnabledChannels); }

			m_nEnabledChannels++; // Trigger and digital channels are always enabled!
		}
	}

	// initialize trigger states
	for (uint32_t i = 0; i < m_triggerIndices.size(); ++i) { m_lastTriggerStates.push_back(0); }

	// double check
	int avlbchannels2;
	res = ampGetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_AvailableChannels, &avlbchannels2, sizeof(avlbchannels2));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "#3 Get available channels, error code= " << res << "\n";
		return false;
	}

	return true;
}


bool CDriverBrainProductsLiveAmp::configureImpedanceMessure()
{
	if (!m_driverCtx.isImpedanceCheckRequested()) { return true; }

	m_iRecordingMode = RM_IMPEDANCE;
	int res          = ampSetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_RecordingMode, &m_iRecordingMode, sizeof(m_iRecordingMode));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[ImpedanceMessureInit] Error setting impedance mode, error code:  " << res << "\n";
		return false;
	}

	m_impedanceChannels = 2; // GND + REF

	// for impedance measurements use only EEG and Bip channels

	int avlbchannels;
	res = ampGetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_AvailableChannels, &avlbchannels, sizeof(avlbchannels));
	if (res != AMP_OK)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "[ImpedanceMessureInit] Get available channels, error code= " << res << "\n";
		return false;
	}

	// enable channels and get indexes of channels to be used!
	// The order of physical channels by LiveAmp: EEGs, AUXs, ACCs, TRIGs 
	int nEEG = 0;
	for (int c = 0; c < avlbchannels; ++c)
	{
		int type;
		res = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_I32_Type, &type, sizeof(type));
		if (res != AMP_OK)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "[ImpedanceMessureInit] GetProperty type error: " << res << "\n";
			return false;
		}

		if (type == CT_EEG || type == CT_BIP)
		{
			BOOL enabled = false;
			res          = ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_B32_RecordingEnabled, &enabled, sizeof(enabled));
			if (res != AMP_OK)
			{
				m_driverCtx.getLogManager() << LogLevel_Error << " [ImpedanceMessureInit] Cannot read enable channel: " << c << "; error code= " << res << "\n";
				return false;
			}

			if (enabled) { nEEG += 2; } // for each channel CH+ and CH-
		}
	}

	m_impedanceChannels += nEEG;

	return true;
}


uint32_t CDriverBrainProductsLiveAmp::getLiveAmpSampleSize()
{
	int channels, dataType;
	float resolution;
	int byteSize = 0;

	m_dataTypes.clear();
	m_nUsedChannels = 0;

	// iterate through all enabled channels
	ampGetProperty(m_handle, PG_DEVICE, 0, DPROP_I32_AvailableChannels, &channels, sizeof(channels));
	for (int c = 0; c < channels; ++c)
	{
		int enabled;
		ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_B32_RecordingEnabled, &enabled, sizeof(enabled));
		if (enabled)
		{
			// get the type of channel
			ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_I32_DataType, &dataType, sizeof(dataType));
			m_dataTypes.push_back(dataType);
			ampGetProperty(m_handle, PG_CHANNEL, c, CPROP_F32_Resolution, &resolution, sizeof(resolution));
			m_resolutions.push_back(resolution);
			m_nUsedChannels++;

			switch (dataType)
			{
				case DT_INT16:
				case DT_UINT16: { byteSize += 2; }
				break;
				case DT_INT32:
				case DT_UINT32:
				case DT_FLOAT32: { byteSize += 4; }
				break;
				case DT_INT64:
				case DT_UINT64:
				case DT_FLOAT64: { byteSize += 8; }
				break;
				default:
					break;
			}
		}
	}

	byteSize += 8; // add the sample counter size

	return byteSize;
}

void CDriverBrainProductsLiveAmp::liveAmpExtractData(const int samplesRead, std::vector<std::vector<float>>& extractData)
{
	const int nSamples = samplesRead / m_sampleSize;
	float sample       = 0;

	extractData.clear();
	extractData.resize(nSamples);

	for (int s = 0; s < nSamples; ++s)
	{
		int offset = 0;
		//uint64_t sampCnt = *(uint64_t*)&m_sampleBuffer[s * m_sampleSize + offset];
		offset += 8; // sample counter offset

		extractData[s].resize(m_nUsedChannels);

		for (uint32_t i = 0; i < m_nUsedChannels; ++i)
		{
			switch (m_dataTypes[i])
			{
				case DT_INT16:
				{
					const int16_t tmp = reinterpret_cast<int16_t&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample            = float(tmp) * m_resolutions[i];
					offset += 2;
					break;
				}
				case DT_UINT16:
				{
					const uint16_t tmp = reinterpret_cast<uint16_t&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample             = float(tmp) * m_resolutions[i];
					offset += 2;
					break;
				}
				case DT_INT32:
				{
					const int tmp = reinterpret_cast<int&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample        = float(tmp) * m_resolutions[i];
					offset += 4;
					break;
				}
				case DT_UINT32:
				{
					const uint32_t tmp = reinterpret_cast<uint32_t&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample             = float(tmp) * m_resolutions[i];
					offset += 4;
					break;
				}
				case DT_FLOAT32:
				{
					const float tmp = reinterpret_cast<float&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample          = tmp * m_resolutions[i];
					offset += 4;
					break;
				}
				case DT_INT64:
				{
					const int64_t tmp = reinterpret_cast<int64_t&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample            = float(tmp) * m_resolutions[i];
					offset += 8;
					break;
				}
				case DT_UINT64:
				{
					const uint64_t tmp = reinterpret_cast<uint64_t&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample             = float(tmp) * m_resolutions[i];
					offset += 8;
					break;
				}
				case DT_FLOAT64:
				{
					const double tmp = reinterpret_cast<double&>(m_sampleBuffer[s * m_sampleSize + offset]);
					sample           = float(tmp) * m_resolutions[i];
					offset += 8;
					break;
				}
				default:
					break;
			}

			extractData[s][i] = sample;
		}
	}
}

void CDriverBrainProductsLiveAmp::liveAmpExtractImpedanceData(const int samplesRead, std::vector<std::vector<float>>& extractData)
{
	// extracts samples of each channel. In this case, since it is a impedance measurements it extracts EEG-channel data.
	const size_t nSamples   = size_t(samplesRead) / m_sampleSize;
	size_t offset           = 0;
	const size_t offsetStep = sizeof(float);

	extractData.clear();
	extractData.resize(nSamples);

	for (size_t s = 0; s < nSamples; ++s)
	{
		extractData[s].resize(m_impedanceChannels);

		for (size_t i = 0; i < m_impedanceChannels; ++i)
		{
			const float sample = *reinterpret_cast<float*>(&m_sampleBuffer[s * m_sampleSize + offset]);
			extractData[s][i]  = sample;
			offset += offsetStep; // sample counter offset
		}
	}
}


#endif //  TARGET_HAS_ThirdPartyLiveAmpAPI
