#include "ovasCConfigurationGenericTimeSignal.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/AcquisitionServer;
using namespace std;

CConfigurationGenericTimeSignal::CConfigurationGenericTimeSignal(IDriverContext& ctx, const char* gtkBuilderFilename)
	: CConfigurationBuilder(gtkBuilderFilename), m_driverCtx(ctx) {}

bool CConfigurationGenericTimeSignal::preConfigure() { return CConfigurationBuilder::preConfigure(); }

// normal header is filled (Subject ID, Age, Gender, channels, sampling frequency)
bool CConfigurationGenericTimeSignal::postConfigure() { return CConfigurationBuilder::postConfigure(); }
