/*
 * ovasCDriverBrainProductsActiCHamp.h
 *
 * Copyright (c) 2012, Mensia Technologies SA. All rights reserved.
 * -- Rights transferred to Inria, contract signed 21.11.2014
 *
 */

#pragma once

#if defined TARGET_HAS_ThirdPartyActiCHampAPI

#include "ovasIDriver.h"
#include "../ovasCHeader.h"

#include <deque>

namespace OpenViBE {
namespace AcquisitionServer {
/**
 * \class CDriverBrainProductsActiCHamp
 * \author Mensia Technologies
 */
class CDriverBrainProductsActiCHamp final : public IDriver
{
public:

	explicit CDriverBrainProductsActiCHamp(IDriverContext& ctx);
	void release() { delete this; }
	const char* getName() override { return "Brain Products actiCHamp"; }

	bool initialize(const uint32_t nSamplePerSentBlock, IDriverCallback& callback) override;
	bool uninitialize() override;

	bool start() override;
	bool stop() override;
	bool loop() override;

	bool isConfigurable() override { return true; }
	bool configure() override;
	const IHeader* getHeader() override { return &m_header; }

	CString getError(int* code = nullptr) const;

protected:

	void* m_handle = nullptr;

	IDriverCallback* m_callback = nullptr;
	CHeader m_header;

	std::vector<uint32_t> m_impedances;
	std::vector<float> m_samples;
	std::vector<float> m_resolutions;
	//std::vector < std::vector < float > > m_sampleCaches;
	std::deque<std::vector<float>> m_sampleCaches;
	uint32_t m_sampleCacheIdx = 0;
	std::vector<double> m_filters;

	CStimulationSet m_stimSet;

	uint32_t m_deviceID          = 0;
	uint32_t m_mode              = 0;
	uint32_t m_physicalSampling  = 0;
	uint32_t m_adcDataFilter     = 0;
	uint32_t m_adcDataDecimation = 0;
	uint32_t m_activeShieldGain  = 0;
	uint32_t m_moduleEnabled     = 0;
	bool m_useAuxChannels        = false;

	uint32_t m_physicalSamplingHz = 0;

	uint32_t m_nChannel    = 0;
	uint32_t m_nEEG        = 0;
	uint32_t m_nAux        = 0;
	uint32_t m_nTriggersIn = 0;

	uint64_t m_counter           = 0;
	uint64_t m_counterStep       = 0;
	uint64_t m_nSample           = 0;
	int64_t m_nDriftOffsetSample = 0;

	uint32_t m_goodImpedanceLimit = 5000;
	uint32_t m_badImpedanceLimit  = 10000;

	signed int* m_pAux   = nullptr;
	signed int* m_eeg    = nullptr;
	uint32_t* m_triggers = nullptr;
	uint32_t m_size      = 0;

	uint32_t m_uiLastTriggers = 0;
	uint64_t m_lastSampleDate = 0;

	bool m_bMyButtonstate = false;
};
}  // namespace AcquisitionServer
}  // namespace OpenViBE

#endif // TARGET_HAS_ThirdPartyActiCHampAPI
