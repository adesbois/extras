/*
 * ovasCDriverBrainProductsActiCHamp.cpp
 *
 * Copyright (c) 2012, Mensia Technologies SA. All rights reserved.
 * -- Rights transferred to Inria, contract signed 21.11.2014
 *
 */

#if defined TARGET_HAS_ThirdPartyActiCHampAPI

#include "ovasCDriverBrainProductsActiCHamp.h"
#include "ovasCConfigurationBrainProductsActiCHamp.h"

#include <toolkit/ovtk_all.h>
#include <system/ovCTime.h>

#include <ActiChamp.h>

using namespace OpenViBE;
using namespace /*OpenViBE::*/AcquisitionServer;
using namespace /*OpenViBE::*/Kernel;


//___________________________________________________________________//
//                                                                   //

namespace {
typedef union
{
	t_champDataModelAux ModelAux;
	t_champDataModel32 Model32;
	t_champDataModel64 Model64;
	t_champDataModel96 Model96;
	t_champDataModel128 Model128;
	t_champDataModel160 Model160;
} UBuffer;

const uint32_t NB_MAX_BUFFER_IN_CACHE = 1000;
UBuffer Buffer[NB_MAX_BUFFER_IN_CACHE];

// Low pass FIR filters before downsampling
//
// Computed with python and scipy
// References :
// http://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.tofile.html
// http://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.firwin.html
//
// The following filters have roughly 50ms delay as described in "2.1.4 What is the
// delay of a linear-phase FIR?" at http://www.dspguru.com/book/export/html/3 :
// n =   500, Fs =   5000, delay = (n-1)/(2*Fs) = 0.04990
// n =  1000, Fs =  10000, delay = (n-1)/(2*Fs) = 0.04995
// n =  2500, Fs =  25000, delay = (n-1)/(2*Fs) = 0.04998
// n =  5000, Fs =  50000, delay = (n-1)/(2*Fs) = 0.04999
// n = 10000, Fs = 100000, delay = (n-1)/(2*Fs) = 0.049995
//
// In order to correct this delay, filtering should be done as a two steps process with a forward filter
// followed by a backward filter. However, this leads to an n square complexity where a linear complexity is
// sufficient in forward only filtering (using 100kHz input signal, n=10000 taps !)
//
// To avoid such complexity, it is chosen to antedate acquired samples by 50ms cheating the drift correction
// process. Indeed, the acquisition server application monitors the drifting of the acquisition process and
// corrects this drift upon demand. It is up to the driver to require this correction and it can be chosen not
// to fit the 0 drift, but to fit an arbitrary fixed drift instead.
//
// The offset for this correction is stored in the m_nDriftOffsetSample variable

/* ---------------------------------------------------------------------------------------------------------------
	from scipy import signal
	N=500
	signal.firwin(N, cutoff=2048./(0.5*5000.), window='hamming').tofile("c:/openvibe/f64_5k_4096.bin")
	signal.firwin(N, cutoff=1024./(0.5*5000.), window='hamming').tofile("c:/openvibe/f64_5k_2048.bin")
	signal.firwin(N, cutoff= 512./(0.5*5000.), window='hamming').tofile("c:/openvibe/f64_5k_1024.bin")
	signal.firwin(N, cutoff= 256./(0.5*5000.), window='hamming').tofile("c:/openvibe/f64_5k_512.bin")
	signal.firwin(N, cutoff= 128./(0.5*5000.), window='hamming').tofile("c:/openvibe/f64_5k_256.bin")
	signal.firwin(N, cutoff=  64./(0.5*5000.), window='hamming').tofile("c:/openvibe/f64_5k_128.bin")

	N=1000
	signal.firwin(N, cutoff=2048./(0.5*10000.), window='hamming').tofile("c:/openvibe/f64_10k_4096.bin")
	signal.firwin(N, cutoff=1024./(0.5*10000.), window='hamming').tofile("c:/openvibe/f64_10k_2048.bin")
	signal.firwin(N, cutoff= 512./(0.5*10000.), window='hamming').tofile("c:/openvibe/f64_10k_1024.bin")
	signal.firwin(N, cutoff= 256./(0.5*10000.), window='hamming').tofile("c:/openvibe/f64_10k_512.bin")
	signal.firwin(N, cutoff= 128./(0.5*10000.), window='hamming').tofile("c:/openvibe/f64_10k_256.bin")
	signal.firwin(N, cutoff=  64./(0.5*10000.), window='hamming').tofile("c:/openvibe/f64_10k_128.bin")

	N=2500
	signal.firwin(N, cutoff=2048./(0.5*25000.), window='hamming').tofile("c:/openvibe/f64_25k_4096.bin")
	signal.firwin(N, cutoff=1024./(0.5*25000.), window='hamming').tofile("c:/openvibe/f64_25k_2048.bin")
	signal.firwin(N, cutoff= 512./(0.5*25000.), window='hamming').tofile("c:/openvibe/f64_25k_1024.bin")
	signal.firwin(N, cutoff= 256./(0.5*25000.), window='hamming').tofile("c:/openvibe/f64_25k_512.bin")
	signal.firwin(N, cutoff= 128./(0.5*25000.), window='hamming').tofile("c:/openvibe/f64_25k_256.bin")
	signal.firwin(N, cutoff=  64./(0.5*25000.), window='hamming').tofile("c:/openvibe/f64_25k_128.bin")

	N=5000
	signal.firwin(N, cutoff=2048./(0.5*50000.), window='hamming').tofile("c:/openvibe/f64_50k_4096.bin")
	signal.firwin(N, cutoff=1024./(0.5*50000.), window='hamming').tofile("c:/openvibe/f64_50k_2048.bin")
	signal.firwin(N, cutoff= 512./(0.5*50000.), window='hamming').tofile("c:/openvibe/f64_50k_1024.bin")
	signal.firwin(N, cutoff= 256./(0.5*50000.), window='hamming').tofile("c:/openvibe/f64_50k_512.bin")
	signal.firwin(N, cutoff= 128./(0.5*50000.), window='hamming').tofile("c:/openvibe/f64_50k_256.bin")
	signal.firwin(N, cutoff=  64./(0.5*50000.), window='hamming').tofile("c:/openvibe/f64_50k_128.bin")

	N=10000
	signal.firwin(N, cutoff=2048./(0.5*100000.), window='hamming').tofile("c:/openvibe/f64_100k_4096.bin")
	signal.firwin(N, cutoff=1024./(0.5*100000.), window='hamming').tofile("c:/openvibe/f64_100k_2048.bin")
	signal.firwin(N, cutoff= 512./(0.5*100000.), window='hamming').tofile("c:/openvibe/f64_100k_1024.bin")
	signal.firwin(N, cutoff= 256./(0.5*100000.), window='hamming').tofile("c:/openvibe/f64_100k_512.bin")
	signal.firwin(N, cutoff= 128./(0.5*100000.), window='hamming').tofile("c:/openvibe/f64_100k_256.bin")
	signal.firwin(N, cutoff=  64./(0.5*100000.), window='hamming').tofile("c:/openvibe/f64_100k_128.bin")
--------------------------------------------------------------------------------------------------------------- */

bool loadFilter(const char* filename, std::vector<double>& vFilter)
{
	FILE* file = fopen(filename, "rb");
	if (!file)
	{
		vFilter.clear();
		vFilter.push_back(1);
		return false;
	}
	fseek(file, 0, SEEK_END);
	const size_t len = ftell(file);
	fseek(file, 0, SEEK_SET);
	vFilter.resize(len / sizeof(double));
	fread(&vFilter[0], len, 1, file);
	fclose(file);
	return true;
}
}  // namespace

CDriverBrainProductsActiCHamp::CDriverBrainProductsActiCHamp(IDriverContext& ctx)
	: IDriver(ctx), m_mode(CHAMP_MODE_ACTIVE_SHIELD), m_adcDataDecimation(CHAMP_DECIMATION_2)
	  , m_activeShieldGain(5) // default value from pyCorder = 5/100
	  , m_moduleEnabled(0xffffffff)
{
	t_champVersion version;
	champGetVersion(nullptr, &version);

	m_driverCtx.getLogManager() << LogLevel_Trace << "Got actiCHamp version (Dll:" << size_t(version.Dll) << ")\n";
	m_driverCtx.getLogManager() << LogLevel_Trace << "Number of attached devices : " << size_t(champGetCount()) << "\n";

	m_header.setSamplingFrequency(512);
	m_header.setChannelCount(32);
}

//___________________________________________________________________//
//                                                                   //

bool CDriverBrainProductsActiCHamp::initialize(const uint32_t /*nSamplePerSentBlock*/, IDriverCallback& callback)
{
	if (m_driverCtx.isConnected()) { return false; }

	// Opens device
	// We change the working directory to be sure to have ActiChamp.bit in it
	if (!SetCurrentDirectory(Directories::getBinDir()))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Can not change current working directory!" << m_deviceID << "\n";
		return false;
	}

	int count = 0;
	while (count++ < 5)
	{
		m_handle = champOpen(m_deviceID);
		if (m_handle == nullptr)
		{
			m_driverCtx.getLogManager() << LogLevel_Trace << "Failed to open the device, retrying (" << count << ")" << "\n";
			System::Time::sleep(500);
		}
		else { break; }
	}

	if (m_handle == nullptr)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Can not open actiCHamp device id " << m_deviceID << "\n";
		return false;
	}
	// set back to previous working directory
	if (!SetCurrentDirectory(Directories::getDistRootDir()))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Can not change current working directory!" << m_deviceID << "\n";
		return false;
	}

	// Gets versions
	t_champVersion version;
	champGetVersion(m_handle, &version);
	m_driverCtx.getLogManager() << LogLevel_Trace << "Got actiCHamp version (Dll:" << size_t(version.Dll) << ", Driver:" <<
			size_t(version.Driver) << ", Cypress:" << size_t(version.Cypress) << ", Fpga:" << size_t(version.Fpga) << ", Msp430:" <<
			size_t(version.Msp430) << ")\n";

	// Gets battery voltages
	t_champVoltages voltage;
	if (champGetVoltages(m_handle, &voltage))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Could not get voltages - Got error [" << getError() << "]\n";
	}
	else
	{
		int state = 0;
		if (voltage.VDC < 5.5) { state = 1; }
		if (voltage.VDC < 5.2) { state = 2; }
		switch (state)
		{
			case 0: m_driverCtx.getLogManager() << LogLevel_Trace << "Battery level is fine (" << float(voltage.VDC) << ")\n";
				break;
			case 1: m_driverCtx.getLogManager() << LogLevel_Warning << "Battery level is critical (" << float(voltage.VDC) << ")\n";
				break;
			case 2: m_driverCtx.getLogManager() << LogLevel_ImportantWarning << "Battery level is bad (" << float(voltage.VDC) << ")\n";
				break;
			default: break;
		}
	}

	// Gets and sets modules status
	t_champModules modules;
	champGetModules(m_handle, &modules);
	modules.Enabled = m_moduleEnabled;
	champSetModules(m_handle, &modules);
	champGetModules(m_handle, &modules);
	m_driverCtx.getLogManager() << LogLevel_Trace << "Got modules status : Present=" << modules.Present << " / Enabled=" << modules.Enabled << "\n";

	// Active shield
	if (champSetActiveShieldGain(m_handle, m_activeShieldGain))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Could not set active shield gain - Got error [" << getError() << "]\n";
		champClose(m_handle);
		m_handle = nullptr;
		return false;
	}
	m_driverCtx.getLogManager() << LogLevel_Trace << "Active Shield Gain set to [" << m_activeShieldGain << "]\n";

	// Sets settings to acquisition state
	t_champSettingsEx oSettings;
	oSettings.Mode       = t_champMode(m_mode);
	oSettings.Rate       = t_champRate(m_physicalSampling);
	oSettings.AdcFilter  = t_champAdcFilter(m_adcDataFilter);
	oSettings.Decimation = t_champDecimation(m_adcDataDecimation);
	if (champSetSettingsEx(m_handle, &oSettings))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Could not set settings - Got error [" << getError() << "]\n";
		return false;
	}
	m_driverCtx.getLogManager() << LogLevel_Trace << "Acquisition Settings set to [Mode: " << m_mode << "] [PhysicalSamplingRate: " <<
			m_physicalSampling << "] [ADCDataFilter: " << m_adcDataFilter << "] [ADCDataDecimation: " << m_adcDataDecimation << "]\n";

	// Gets properties
	t_champProperty properties;
	if (champGetProperty(m_handle, &properties))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Could not get properties - Got error [" << getError() << "]\n";
		champClose(m_handle);
		m_handle = nullptr;
		return false;
	}

	// Sets channel count
	// as the modules were set before according to the configuration, the property structure
	// contains the right number of channels.
	m_nEEG     = properties.CountEeg;
	m_nAux     = (m_useAuxChannels ? properties.CountAux : 0);
	m_nChannel = m_nEEG + m_nAux;
	if (m_nChannel == 0)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "No channels were selected [EEG: " << m_nEEG << "] [AUX: " << m_nAux <<
				"]. Close device.\n";
		champClose(m_handle);
		m_handle = nullptr;
		return false;
	}
	m_driverCtx.getLogManager() << LogLevel_Trace << "Channel counts are  [EEG: " << m_nEEG << "] [AUX: " << m_nAux << "]\n";

	m_header.setChannelCount(m_nChannel);

	m_impedances.resize(m_nEEG + 1);
	m_samples.resize(m_nChannel);
	m_resolutions.resize(m_nChannel);

	m_physicalSamplingHz = uint32_t(properties.Rate);
	m_counterStep        = (uint64_t(m_header.getSamplingFrequency()) << 32) / uint64_t(m_physicalSamplingHz);

	m_driverCtx.getLogManager() << LogLevel_Trace << "Sampling rate in Hz  [Physical: " << m_physicalSamplingHz << "] [Driver: " << m_header.
			getSamplingFrequency() << "]\n";

	size_t j = 0;
	for (size_t i = 0; i < m_nEEG; i++, j++) { m_resolutions[j] = properties.ResolutionEeg * 1E6f; }	// converts to µV
	for (size_t i = 0; i < m_nAux; i++, j++) { m_resolutions[j] = properties.ResolutionAux * 1E6f; }	// converts to µV

	//@TODO ue of i & j are normals ?
	j = 0;
	for (size_t i = 0; i < m_nEEG; i++, j++) { m_header.setChannelUnits(i, OVTK_UNIT_Volts, OVTK_FACTOR_Micro); }
	for (size_t i = 0; i < m_nAux; i++, j++) { m_header.setChannelUnits(i, OVTK_UNIT_Volts, OVTK_FACTOR_Micro); }	// converts to µV

	// Sets data pointers
	// the amplifier model is depending on the number of channels, always including AUX
	switch ((m_useAuxChannels ? m_nChannel : m_nChannel + 8))
	{
		case 8: m_driverCtx.getLogManager() << LogLevel_Trace << "Data Model is [AUX]\n";
			m_pAux     = Buffer[0].ModelAux.Aux;
			m_eeg      = nullptr; // Buffer[0].ModelAux.Main;
			m_triggers = &Buffer[0].ModelAux.Triggers;
			m_size     = sizeof(Buffer[0].ModelAux);
			break;

		case 32 + 8: m_driverCtx.getLogManager() << LogLevel_Trace << "Data Model is [Model32]\n";
			m_pAux     = Buffer[0].Model32.Aux;
			m_eeg      = Buffer[0].Model32.Main;
			m_triggers = &Buffer[0].Model32.Triggers;
			m_size     = sizeof(Buffer[0].Model32);
			break;

		case 64 + 8: m_driverCtx.getLogManager() << LogLevel_Trace << "Data Model is [Model64]\n";
			m_pAux     = Buffer[0].Model64.Aux;
			m_eeg      = Buffer[0].Model64.Main;
			m_triggers = &Buffer[0].Model64.Triggers;
			m_size     = sizeof(Buffer[0].Model64);
			break;

		case 96 + 8: m_driverCtx.getLogManager() << LogLevel_Trace << "Data Model is [Model96]\n";
			m_pAux     = Buffer[0].Model96.Aux;
			m_eeg      = Buffer[0].Model96.Main;
			m_triggers = &Buffer[0].Model96.Triggers;
			m_size     = sizeof(Buffer[0].Model96);
			break;

		case 128 + 8: m_driverCtx.getLogManager() << LogLevel_Trace << "Data Model is [Model128]\n";
			m_pAux     = Buffer[0].Model128.Aux;
			m_eeg      = Buffer[0].Model128.Main;
			m_triggers = &Buffer[0].Model128.Triggers;
			m_size     = sizeof(Buffer[0].Model128);
			break;

		case 160 + 8: m_driverCtx.getLogManager() << LogLevel_Trace << "Data Model is [Model160]\n";
			m_pAux     = Buffer[0].Model160.Aux;
			m_eeg      = Buffer[0].Model160.Main;
			m_triggers = &Buffer[0].Model160.Triggers;
			m_size     = sizeof(Buffer[0].Model160);
			break;

		default: m_driverCtx.getLogManager() << LogLevel_Trace << "Data Model unknown.\n";
			m_pAux     = nullptr;
			m_eeg      = nullptr;
			m_triggers = nullptr;
			m_size     = 0;
			break;
	}

	if (m_size == 0)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Unknown device model " << m_nEEG << " EEG channels / " << m_nAux <<
				" Auxiliary channels\n";
		champClose(m_handle);
		m_handle = nullptr;
		return false;
	}
	m_driverCtx.getLogManager() << LogLevel_Trace << "Device model " << m_nEEG << " EEG channels / " << m_nAux << " Auxiliary channels\n";

	// Prepares low pass filter

#define __set_filter__(f,f_decim) \
	if(m_adcDataDecimation == CHAMP_DECIMATION_0) \
	{ \
		if(!loadFilter(Directories::getDataDir() + "/applications/acquisition-server/filters/" f ".bin", m_filters)) { m_driverCtx.getLogManager() << LogLevel_Error << "Failed to load filter !\n"; } \
	} \
	else \
	{ \
		if(!loadFilter(Directories::getDataDir() + "/applications/acquisition-server/filters/" f_decim ".bin", m_filters)) { m_driverCtx.getLogManager() << LogLevel_Error << "Failed to load filter !\n"; } \
	}\
	valid = true;

	m_driverCtx.getLogManager() << LogLevel_Trace << "Setting up the FIR filter for signal decimation (physical rate > driver rate).\n";

	bool valid = false;
	switch (m_header.getSamplingFrequency())
	{
		case 128: switch (m_physicalSampling)
			{
				case CHAMP_RATE_10KHZ: __set_filter__("f64_10k_128", "f64_5k_128");
					break;
				case CHAMP_RATE_50KHZ: __set_filter__("f64_50k_128", "f64_25k_128");
					break;
				case CHAMP_RATE_100KHZ: __set_filter__("f64_100k_128", "f64_50k_128");
					break;
				default: break;
			}
			break;

		case 256: switch (m_physicalSampling)
			{
				case CHAMP_RATE_10KHZ: __set_filter__("f64_10k_256", "f64_5k_256");
					break;
				case CHAMP_RATE_50KHZ: __set_filter__("f64_50k_256", "f64_25k_256");
					break;
				case CHAMP_RATE_100KHZ: __set_filter__("f64_100k_256", "f64_50k_256");
					break;
				default: break;
			}
			break;

		case 512: switch (m_physicalSampling)
			{
				case CHAMP_RATE_10KHZ: __set_filter__("f64_10k_512", "f64_5k_512");
					break;
				case CHAMP_RATE_50KHZ: __set_filter__("f64_50k_512", "f64_25k_512");
					break;
				case CHAMP_RATE_100KHZ: __set_filter__("f64_100k_512", "f64_50k_512");
					break;
				default: break;
			}
			break;

		case 1024: switch (m_physicalSampling)
			{
				case CHAMP_RATE_10KHZ: __set_filter__("f64_10k_1024", "f64_5k_1024");
					break;
				case CHAMP_RATE_50KHZ: __set_filter__("f64_50k_1024", "f64_25k_1024");
					break;
				case CHAMP_RATE_100KHZ: __set_filter__("f64_100k_1024", "f64_50k_1024");
					break;
				default: break;
			}
			break;

		case 2048: switch (m_physicalSampling)
			{
				case CHAMP_RATE_10KHZ: __set_filter__("f64_10k_2048", "f64_5k_2048");
					break;
				case CHAMP_RATE_50KHZ: __set_filter__("f64_50k_2048", "f64_25k_2048");
					break;
				case CHAMP_RATE_100KHZ: __set_filter__("f64_100k_2048", "f64_50k_2048");
					break;
				default: break;
			}
			break;

		case 4096: switch (m_physicalSampling)
			{
				case CHAMP_RATE_10KHZ: __set_filter__("f64_10k_4096", "f64_5k_4096");
					break;
				case CHAMP_RATE_50KHZ: __set_filter__("f64_50k_4096", "f64_25k_4096");
					break;
				case CHAMP_RATE_100KHZ: __set_filter__("f64_100k_4096", "f64_50k_4096");
					break;
				default: break;
			}
			break;

		default: break;
	}

#if 0
	__set_filter__("f64_none");
	__set_filter__("f64_10k_12");
	__set_filter__("f64_10k_256");
#endif

#undef __set_filter__

	if (!valid)
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Unhandled soft sampling frequency / physical sampling frequency configuration [" << m_header.
				getSamplingFrequency() << ", " << m_physicalSampling << "]\n";
		champClose(m_handle);
		m_handle = nullptr;
		return false;
	}

	// Prepares cache for filtering
	m_sampleCaches.clear();
	for (size_t i = 0; i < m_filters.size(); ++i) { m_sampleCaches.push_back(m_samples); }

	m_callback = &callback;
	m_counter  = 0;

	// Setting the inner latency as described in the beginning of the file
	m_nDriftOffsetSample = int64_t(m_header.getSamplingFrequency() * 50) / 1000;
	m_driverCtx.setInnerLatencySampleCount(-m_nDriftOffsetSample);
	m_driverCtx.getLogManager() << LogLevel_Trace << "Driver inner latency set to 50ms to compensate FIR filtering.\n";

	if (m_driverCtx.isImpedanceCheckRequested())
	{
		// Sets settings manually for impedance check
		t_champSettingsEx settings;
		settings.Mode       = CHAMP_MODE_IMPEDANCE;
		settings.Rate       = CHAMP_RATE_10KHZ;
		settings.AdcFilter  = CHAMP_ADC_NATIVE;
		settings.Decimation = CHAMP_DECIMATION_0;
		if (champSetSettingsEx(m_handle, &settings))
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "Could not set settings - Got error [" << getError() << "]\n";
			champClose(m_handle);
			m_handle = nullptr;
			return false;
		}
		m_driverCtx.getLogManager() << LogLevel_Trace << "Impedance check is requested. Setting the acquisition to [CHAMP_MODE_IMPEDANCE].\n";

		// Sets impedance setup manually
		t_champImpedanceSetup setup;
		setup.Good        = m_goodImpedanceLimit;// 5000; // 5kOhm
		setup.Bad         = m_badImpedanceLimit; //10000; // 10kOhm
		setup.LedsDisable = 0;
		setup.TimeOut     = 5;
		if (champImpedanceSetSetup(m_handle, &setup))
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "Could not set impedance setup - Got error [" << getError() << "]\n";
			champClose(m_handle);
			m_handle = nullptr;
			return false;
		}
		m_driverCtx.getLogManager() << LogLevel_Trace << "Impedance limits are set to [GREEN < " << m_goodImpedanceLimit << "Ohm < YELLOW < " <<
				m_badImpedanceLimit << "Ohm < RED].\n";

		// Gets/Sets impedance mode
		t_champImpedanceMode mode;
		if (champImpedanceGetMode(m_handle, &mode))
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "Could not get impedance mode - Got error [" << getError() << "]\n";
			champClose(m_handle);
			m_handle = nullptr;
			return false;
		}

		mode.Splitter = mode.Splitters;
		if (champImpedanceSetMode(m_handle, &mode))
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "Could not set impedance mode - Got error [" << getError() << "]\n";
			champClose(m_handle);
			m_handle = nullptr;
			return false;
		}
		m_driverCtx.getLogManager() << LogLevel_Trace << "Impedance splitters set to [" << size_t(mode.Splitters) << "]\n";

		// Starts impedance check
		if (champStart(m_handle))
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "Can not start device id " << m_deviceID << " - Got error [" << getError() << "]\n";
			champClose(m_handle);
			m_handle = nullptr;
			return false;
		}
		m_driverCtx.getLogManager() << LogLevel_Trace << "Impedance check started...\n";
	}

	return true;
}

bool CDriverBrainProductsActiCHamp::start()
{
	if (!m_driverCtx.isConnected() || m_driverCtx.isStarted()) { return false; }

	if (m_driverCtx.isImpedanceCheckRequested()) { champStop(m_handle); }

	// Sets settings to acquisition state
	t_champSettingsEx settings;
	settings.Mode       = t_champMode(m_mode);
	settings.Rate       = t_champRate(m_physicalSampling);
	settings.AdcFilter  = t_champAdcFilter(m_adcDataFilter);
	settings.Decimation = t_champDecimation(m_adcDataDecimation);
	if (champSetSettingsEx(m_handle, &settings))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Could not set settings - Got error [" << getError() << "]\n";
		return false;
	}
	m_driverCtx.getLogManager() << LogLevel_Trace << "Acquisition Settings set to [Mode: " << m_mode << "] [PhysicalSamplingRate: " <<
			m_physicalSampling << "] [ADCDataFilter: " << m_adcDataFilter << "] [ADCDataDecimation: " << m_adcDataDecimation << "]\n";

	// Starts acquisition
	if (champStart(m_handle))
	{
		m_driverCtx.getLogManager() << LogLevel_Error << "Can not start device id " << m_deviceID << " - Got error [" << getError() << "]\n";
		return false;
	}

	m_driverCtx.getLogManager() << LogLevel_Trace << "Acquisition started...\n";

	return true;
}

bool CDriverBrainProductsActiCHamp::loop()
{
	if (!m_driverCtx.isConnected()) { return false; }

	if (m_driverCtx.isStarted())
	{
		t_champDataStatus dataStatus;
		champGetDataStatus(m_handle, &dataStatus);
		//m_driverCtx.getLogManager() << LogLevel_Trace << "Status : Samples:" << size_t(l_oDataStatus.Samples) << " Errors:" << size_t(l_oDataStatus.Errors) << " Rate:" << float(l_oDataStatus.Rate) << " Speed:" << float(l_oDataStatus.Speed) << "\n";

		// Reads all the data.
		// Buffers are aligned : with one call to champGetData we get as much buffers as possible
		int code;
		while ((code = champGetData(m_handle, &Buffer[0], m_size * NB_MAX_BUFFER_IN_CACHE)) > 0)
		{
			m_driverCtx.getLogManager() << LogLevel_Debug << "Received " << float(code) / m_size << " new buffers with champGetData.\n";

			for (int buf = 0; buf < int(code / m_size); ++buf)
			{
				uint32_t i, j = 0;

				// we find the good pointers (m_eeg and m_pAux being on the first buffer)
				// Size of an union in memory is the size of the biggest possible type
				signed int* aux = m_pAux + buf * (m_size / sizeof(signed int));
				signed int* eeg = m_eeg + buf * (m_size / sizeof(signed int));
				// m_driverCtx.getLogManager() << LogLevel_Debug << "AUX ptr = " << (uint32_t)l_pAux << " EEG ptr = "<< (uint32_t)l_pEEG <<"\n";

				// Scales data according to respective resolution
				for (i = 0; i < m_nEEG; i++, j++) { m_samples[j] = m_resolutions[j] * float(eeg[i]); }
				if (m_useAuxChannels) { for (i = 0; i < m_nAux; i++, j++) { m_samples[j] = m_resolutions[j] * float(aux[i]); } }
				//m_driverCtx.getLogManager() << LogLevel_Debug << "Samples saved in cache.\n";

				// Updates cache
				//m_sampleCaches.erase(m_sampleCaches.begin());
				m_sampleCaches.pop_front();
				m_sampleCaches.push_back(m_samples);
				m_nSample++;

				// Downsamples and sends samples only when relevant
				m_counter += m_counterStep;
				if (m_counter >= (1LL << 32))
				{
					//m_driverCtx.getLogManager() << LogLevel_Trace << "Enough samples received to build a new buffer. Applying FIR filter.\n";
					m_counter -= (1LL << 32);

					// Filters last samples
					for (i = 0; i < m_samples.size(); ++i)
					{
						m_samples[i] = 0;
						auto it      = m_sampleCaches.begin();
						for (j = 0; j < m_filters.size(); ++j)
						{
							//m_samples[i]+=m_filters[j]*m_sampleCaches[j][i];
							m_samples[i] += float(m_filters[j] * (*it)[i]);
							++it;
						}
					}

					//m_driverCtx.getLogManager() << LogLevel_Trace << "Samples ready for sending.\n";
					m_callback->setSamples(&m_samples[0], 1);
					m_callback->setStimulationSet(m_stimSet);
					m_stimSet.clear();

					// Saves the date of this sample for further use with the triggers
					m_lastSampleDate += uint64_t((1LL << 32) / m_header.getSamplingFrequency());
					m_nSample = 0;
					//m_driverCtx.getLogManager() << LogLevel_Debug << "Sending one sample.\n";
				}

				//m_driverCtx.getLogManager() << LogLevel_Debug << "Getting trigger...\n";

				// we save the MyButton state and detect if it switched
				bool bSwitch      = m_bMyButtonstate;
				uint32_t* trigger = m_triggers + buf * (m_size / sizeof(uint32_t));
				m_bMyButtonstate  = ((*trigger & 0x80000000) >> 31) == 1;
				bSwitch           = (bSwitch ? !m_bMyButtonstate : m_bMyButtonstate);

				// Decomposes the trigger bytes:
				// Digital inputs (bits 0 - 7) + output (bits 8 - 15) state + 15 MSB reserved bits + MyButton bit (bit 31)
				// apply a mask to silence any modification on the output and reserved parts
				(*trigger) &= 0x800000ff;

				// Now we can test for modification on the interesting part (input triggers and MyButton)
				if (*trigger != m_uiLastTriggers)
				{
					m_driverCtx.getLogManager() << LogLevel_Trace << "Current input triggers state [";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000001)) << " ";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000002) >> 1) << " ";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000004) >> 2) << " ";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000008) >> 3) << " ";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000010) >> 4) << " ";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000020) >> 5) << " ";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000040) >> 6) << " ";
					m_driverCtx.getLogManager() << ((*trigger & 0x00000080) >> 7) << "] MyButton [";
					m_driverCtx.getLogManager() << m_bMyButtonstate << "]\n";

					// Every time that a trigger has changed, we send a stimulation.
					// code of stimulation = OVTK_StimulationId_LabelStart + value of the trigger bytes.
					// This supposes that the user knows the structure of the triggers bit by bit and can process it in a Lua box for example.
					// The value received is in [0-255]
					// The MyButton bit is set to 0 to ensure this range.

					// The date is relative to the last buffer start time (cf the setSamples before setStimulationSet)
					const uint64_t date = m_nSample * (1LL << 32) / m_physicalSamplingHz;
					m_stimSet.appendStimulation(OVTK_StimulationId_Label((*trigger)&0x000000ff), date, 0);

					// special case : MyButton
					// The MyButton trigger bit is too far in the trigger bytes to have a valid value
					// We use 2 dedicated stims OVTK_StimulationId_Button1_Pressed and OVTK_StimulationId_Button1_Released
					if (bSwitch)
					{
						m_stimSet.appendStimulation((m_bMyButtonstate ? OVTK_StimulationId_Button1_Pressed : OVTK_StimulationId_Button1_Released), date, 0);
					}
				}
				m_uiLastTriggers = *trigger;
			}
		}
		if (code < 0)
		{
			m_driverCtx.getLogManager() << LogLevel_Error << "An error occured with last sample request - Got error [" << code << "]\n";
			return false;
		}

		// As described in at the beginning of this file, the drift
		// correction process is altered to consider the ~ 50ms delay
		// of the single way filtering process
		// Inner latency was set accordingly.
		m_driverCtx.correctDriftSampleCount(m_driverCtx.getSuggestedDriftCorrectionSampleCount());
	}
	else
	{
		if (m_driverCtx.isImpedanceCheckRequested())
		{
			// Reads impedances
			if (champImpedanceGetData(m_handle, &m_impedances[0], m_impedances.size() * sizeof(uint32_t)))
			{
				m_driverCtx.getLogManager() << LogLevel_Warning << "Can not read impedances on device id " << m_deviceID << " - Got error [" <<
						getError() << "]\n";
			}
			else
			{
				// Updates impedances
				m_driverCtx.getLogManager() << LogLevel_Debug << "Impedances are [ ";
				for (size_t j = 0; j < m_nEEG; ++j)
				{
					m_driverCtx.updateImpedance(j, m_impedances[j]);
					m_driverCtx.getLogManager() << m_impedances[j] << " ";
				}
				m_driverCtx.getLogManager() << "\n";

				// Drops data
				while ((champGetData(m_handle, &Buffer, m_size * NB_MAX_BUFFER_IN_CACHE)) > 0) { }
			}
		}
	}

	return true;
}

bool CDriverBrainProductsActiCHamp::stop()
{
	if (!m_driverCtx.isConnected() || !m_driverCtx.isStarted()) { return false; }

	if (!m_driverCtx.isImpedanceCheckRequested()) { champStop(m_handle); }

	return true;
}

bool CDriverBrainProductsActiCHamp::uninitialize()
{
	if (!m_driverCtx.isConnected() || m_driverCtx.isStarted()) { return false; }

	if (m_driverCtx.isImpedanceCheckRequested()) { champStop(m_handle); }

	champClose(m_handle);
	m_handle = nullptr;

	return true;
}

//___________________________________________________________________//
//                                                                   //

bool CDriverBrainProductsActiCHamp::configure()
{
	CConfigurationBrainProductsActiCHamp config(Directories::getDataDir() + "/applications/acquisition-server/interface-BrainProducts-ActiCHamp.ui", m_deviceID,
												m_mode, m_physicalSampling, m_adcDataFilter, m_adcDataDecimation, m_activeShieldGain, m_moduleEnabled,
												m_useAuxChannels, m_goodImpedanceLimit, m_badImpedanceLimit);
	return config.configure(m_header);
}

//___________________________________________________________________//
//                                                                   //

CString CDriverBrainProductsActiCHamp::getError(int* code) const
{
	int iCode  = 0;
	int* pCode = (code ? code : &iCode);
	if (!m_handle) { return ""; }
	char buffer[1024];
	buffer[0] = '\0';
	champGetError(m_handle, pCode, buffer, sizeof(buffer));
	return buffer;
}

#endif // TARGET_HAS_ThirdPartyActiCHampAPI
