/*
 * ovasCConfigurationBrainProductsActiCHamp.cpp
 *
 * Copyright (c) 2012, Mensia Technologies SA. All rights reserved.
 * -- Rights transferred to Inria, contract signed 21.11.2014
 *
 */

#if defined TARGET_HAS_ThirdPartyActiCHampAPI

#include "ovasCConfigurationBrainProductsActiCHamp.h"

#include "ovasIHeader.h"

#include <actichamp.h>
#include <system/ovCTime.h>

#include <iostream>
#include <sstream>
#include <shellapi.h>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/AcquisitionServer;


static void DeviceChangedCB(GtkComboBox* /*comboBox*/, gpointer data)
{
	reinterpret_cast<CConfigurationBrainProductsActiCHamp*>(data)->comboBoxDeviceChangedCB();
}

static void ModuleToggledCB(GtkToggleButton* button, gpointer data)
{
	reinterpret_cast<CConfigurationBrainProductsActiCHamp*>(data)->buttonModuleToggledCB(gtk_toggle_button_get_active(button) != 0);
}

static void AUXChannelsToggledCB(GtkToggleButton* button, gpointer data)
{
	reinterpret_cast<CConfigurationBrainProductsActiCHamp*>(data)->buttonAuxChannelsToggledCB(gtk_toggle_button_get_active(button) != 0);
}

CConfigurationBrainProductsActiCHamp::CConfigurationBrainProductsActiCHamp(
	const char* gtkBuilderFilename, uint32_t& deviceID, uint32_t& mode, uint32_t& physicalSampling, uint32_t& adcDataFilter,
	uint32_t& adcDataDecimation, uint32_t& activeShieldGain, uint32_t& moduleEnabled, bool& useAuxChannels, uint32_t& goodImpedanceLimit,
	uint32_t& badImpedanceLimit)
	: CConfigurationBuilder(gtkBuilderFilename), m_deviceID(deviceID), m_mode(mode), m_physicalSampling(physicalSampling),
	  m_adcDataFilter(adcDataFilter)
	  , m_adcDataDecimation(adcDataDecimation), m_activeShieldGain(activeShieldGain), m_moduleEnabled(moduleEnabled), m_useAuxChannels(useAuxChannels)
	  , m_goodImpedanceLimit(goodImpedanceLimit), m_badImpedanceLimit(badImpedanceLimit) {}

namespace {
void open_url_brainproducts_download_actichamp_fw_cb(GtkWidget* /*widget*/, gpointer /*data*/)
{
	ShellExecute(nullptr, nullptr, "http://www.brainproducts.com/files/public/downloads/actiCHamp_rev03_FW-Update.zip", nullptr, nullptr, SW_SHOW);
}
}

bool CConfigurationBrainProductsActiCHamp::preConfigure()
{
	if (!CConfigurationBuilder::preConfigure()) { return false; }

	m_imageErrorIcon             = GTK_WIDGET(::gtk_builder_get_object(m_builder, "image_error_icon"));
	m_labelErrorMessage          = GTK_LABEL(::gtk_builder_get_object(m_builder, "label_error_message"));
	m_buttonErrorDLLLink         = GTK_BUTTON(::gtk_builder_get_object(m_builder, "button_bp_actichamp_download_firmware"));
	m_comboBoxDeviceId           = GTK_COMBO_BOX(::gtk_builder_get_object(m_builder, "combobox_device_id"));
	m_comboBoxMode               = GTK_COMBO_BOX(::gtk_builder_get_object(m_builder, "combobox_mode"));
	m_comboBoxPhysicalSampleRate = GTK_COMBO_BOX(::gtk_builder_get_object(m_builder, "combobox_physical_sample_rate"));
	m_comboBoxADCDataFilter      = GTK_COMBO_BOX(::gtk_builder_get_object(m_builder, "combobox_adc_data_filter"));
	m_comboBoxADCDataDecimation  = GTK_COMBO_BOX(::gtk_builder_get_object(m_builder, "combobox_adc_data_decimation"));
	m_buttonActiveShieldGain     = GTK_SPIN_BUTTON(::gtk_builder_get_object(m_builder, "spinbutton_active_shield_gain"));
	m_buttonUseAuxChannels       = GTK_TOGGLE_BUTTON(::gtk_builder_get_object(m_builder, "checkbutton_aux_channels"));
	m_buttonGoodImpedanceLimit   = GTK_SPIN_BUTTON(::gtk_builder_get_object(m_builder, "spinbutton_good_imp"));
	m_buttonBadImpedanceLimit    = GTK_SPIN_BUTTON(::gtk_builder_get_object(m_builder, "spinbutton_bad_imp"));

	m_module1 = GTK_TOGGLE_BUTTON(::gtk_builder_get_object(m_builder, "togglebutton_module_1"));
	m_module2 = GTK_TOGGLE_BUTTON(::gtk_builder_get_object(m_builder, "togglebutton_module_2"));
	m_module3 = GTK_TOGGLE_BUTTON(::gtk_builder_get_object(m_builder, "togglebutton_module_3"));
	m_module4 = GTK_TOGGLE_BUTTON(::gtk_builder_get_object(m_builder, "togglebutton_module_4"));
	m_module5 = GTK_TOGGLE_BUTTON(::gtk_builder_get_object(m_builder, "togglebutton_module_5"));

	g_signal_connect(G_OBJECT(m_module1), "toggled", G_CALLBACK(ModuleToggledCB), this);
	g_signal_connect(G_OBJECT(m_module2), "toggled", G_CALLBACK(ModuleToggledCB), this);
	g_signal_connect(G_OBJECT(m_module3), "toggled", G_CALLBACK(ModuleToggledCB), this);
	g_signal_connect(G_OBJECT(m_module4), "toggled", G_CALLBACK(ModuleToggledCB), this);
	g_signal_connect(G_OBJECT(m_module5), "toggled", G_CALLBACK(ModuleToggledCB), this);

	g_signal_connect(G_OBJECT(m_buttonUseAuxChannels), "toggled", G_CALLBACK(AUXChannelsToggledCB), this);
	g_signal_connect(G_OBJECT(m_buttonErrorDLLLink), "clicked", G_CALLBACK(open_url_brainproducts_download_actichamp_fw_cb), nullptr);

	g_signal_connect(G_OBJECT(m_comboBoxDeviceId), "changed", G_CALLBACK(DeviceChangedCB), this);

	const uint32_t nDevice = uint32_t(champGetCount());
	bool selected          = false;

	// autodetection of the connected device(s)
	for (uint32_t i = 0; i < nDevice; ++i)
	{
		gtk_combo_box_append_text(m_comboBoxDeviceId, ("Device " + std::to_string(i)).c_str());
		{
			if (m_deviceID == i)
			{
				gtk_combo_box_set_active(m_comboBoxDeviceId, i);
				selected = true;
			}
		}
	}

	if (nDevice == 0) { gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), 0); }

	if (!selected && nDevice != 0) { gtk_combo_box_set_active(m_comboBoxDeviceId, 0); }

	gtk_combo_box_set_active(m_comboBoxMode, m_mode);
	gtk_combo_box_set_active(m_comboBoxPhysicalSampleRate, m_physicalSampling);
	gtk_combo_box_set_active(m_comboBoxADCDataFilter, m_adcDataFilter);
	gtk_combo_box_set_active(m_comboBoxADCDataDecimation, (m_adcDataDecimation == CHAMP_DECIMATION_2 ? 1 : 0));
	gtk_spin_button_set_value(m_buttonActiveShieldGain, m_activeShieldGain);
	gtk_spin_button_set_value(m_buttonGoodImpedanceLimit, m_goodImpedanceLimit);
	gtk_spin_button_set_value(m_buttonBadImpedanceLimit, m_badImpedanceLimit);

	return true;
}

bool CConfigurationBrainProductsActiCHamp::postConfigure()
{
	GTK_COMBO_BOX(gtk_builder_get_object(m_builder, "combobox_device"));

	if (m_applyConfig)
	{
		gtk_spin_button_update(GTK_SPIN_BUTTON(m_buttonActiveShieldGain));
		gtk_spin_button_update(GTK_SPIN_BUTTON(m_buttonGoodImpedanceLimit));
		gtk_spin_button_update(GTK_SPIN_BUTTON(m_buttonBadImpedanceLimit));

		m_deviceID          = gtk_combo_box_get_active(m_comboBoxDeviceId);
		m_mode              = gtk_combo_box_get_active(m_comboBoxMode);
		m_physicalSampling  = gtk_combo_box_get_active(m_comboBoxPhysicalSampleRate);
		m_adcDataFilter     = gtk_combo_box_get_active((m_comboBoxADCDataFilter));
		m_adcDataDecimation = ((gtk_combo_box_get_active(m_comboBoxADCDataDecimation) == 1) ? CHAMP_DECIMATION_2 : CHAMP_DECIMATION_0
		); // The decimation enum gives FACTOR_2 = 2 and not 1.
		m_activeShieldGain   = uint32_t(gtk_spin_button_get_value(m_buttonActiveShieldGain));
		m_goodImpedanceLimit = uint32_t(gtk_spin_button_get_value(m_buttonGoodImpedanceLimit));
		m_badImpedanceLimit  = uint32_t(gtk_spin_button_get_value(m_buttonBadImpedanceLimit));
		m_useAuxChannels     = gtk_toggle_button_get_active(m_buttonUseAuxChannels) != 0;
		m_moduleEnabled      = 0x01
							   | (gtk_toggle_button_get_active(m_module1) ? 0x02 : 0x00)
							   | (gtk_toggle_button_get_active(m_module2) ? 0x04 : 0x00)
							   | (gtk_toggle_button_get_active(m_module3) ? 0x08 : 0x00)
							   | (gtk_toggle_button_get_active(m_module4) ? 0x10 : 0x00)
							   | (gtk_toggle_button_get_active(m_module5) ? 0x20 : 0x00);
	}

	if (!CConfigurationBuilder::postConfigure()) { return false; }
	return true;
}

void CConfigurationBrainProductsActiCHamp::comboBoxDeviceChangedCB()
{
	void* handle            = nullptr;
	const uint32_t deviceID = gtk_combo_box_get_active(m_comboBoxDeviceId);

	// retry opening the device several times, as sometimes the champOpen fails for no reason

	int retriesCount = 0;
	while (retriesCount++ < 1)
	{
		handle = champOpen(deviceID);
		if (handle == nullptr) { System::Time::sleep(500); }
		else { break; }
	}
	// Opens device
	if (handle != nullptr)
	{
		// Clear error messages
		gtk_widget_hide(m_imageErrorIcon);
		gtk_widget_hide(GTK_WIDGET(m_labelErrorMessage));
		gtk_widget_hide(GTK_WIDGET(m_buttonErrorDLLLink));

		// Gets properties
		t_champProperty properties;
		champGetProperty(handle, &properties);

		// Sets channel count
		//m_header->setSamplingFrequency(uint32_t(l_oProperties.Rate));
		//m_header->setChannelCount(l_oProperties.CountEeg + properties.CountAux);
		//gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), properties.CountEeg + properties.CountAux);

		// The channel count is updated with the toggled button callbacks.
		m_header->setChannelCount(0);

		// Gets modules
		t_champModules modules;
		champGetModules(handle, &modules);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module1), (modules.Present & 0x02) ? TRUE : FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module2), (modules.Present & 0x04) ? TRUE : FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module3), (modules.Present & 0x08) ? TRUE : FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module4), (modules.Present & 0x10) ? TRUE : FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module5), (modules.Present & 0x20) ? TRUE : FALSE);


		// Since the toggle_button method fires the event we have to clean up all boutons
		// and the channel count before actually activating the modules
		gtk_toggle_button_set_active(m_module1, FALSE);
		gtk_toggle_button_set_active(m_module2, FALSE);
		gtk_toggle_button_set_active(m_module3, FALSE);
		gtk_toggle_button_set_active(m_module4, FALSE);
		gtk_toggle_button_set_active(m_module5, FALSE);

		gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), 0);

		gtk_toggle_button_set_active(m_module1, (m_moduleEnabled & 0x02 && modules.Present & 0x02) ? TRUE : FALSE); // calls the moduleToggledCB.
		gtk_toggle_button_set_active(m_module2, (m_moduleEnabled & 0x04 && modules.Present & 0x04) ? TRUE : FALSE); // calls the moduleToggledCB.
		gtk_toggle_button_set_active(m_module3, (m_moduleEnabled & 0x08 && modules.Present & 0x08) ? TRUE : FALSE); // calls the moduleToggledCB.
		gtk_toggle_button_set_active(m_module4, (m_moduleEnabled & 0x10 && modules.Present & 0x10) ? TRUE : FALSE); // calls the moduleToggledCB.
		gtk_toggle_button_set_active(m_module5, (m_moduleEnabled & 0x20 && modules.Present & 0x20) ? TRUE : FALSE); // calls the moduleToggledCB.

		//Aux channels
		gtk_toggle_button_set_active(m_buttonUseAuxChannels, m_useAuxChannels);

		// TODO: champClose function returns an error code which is never ever checked,
		// closing a device can and does fail in some circumstances, but we can not really
		// do much about it.
		const int error = champClose(handle);
		if (error) { std::cerr << "Closing ActiCHamp device failed" << std::endl; }
	}
	else
	{
		// The device failed to open, we disable all of the controls and display an error message
		gtk_widget_show(m_imageErrorIcon);
		gtk_widget_show(GTK_WIDGET(m_labelErrorMessage));
		gtk_widget_show(GTK_WIDGET(m_buttonErrorDLLLink));

		m_header->setChannelCount(0);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), 0);

		gtk_widget_set_sensitive(GTK_WIDGET(m_module1), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module2), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module3), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module4), FALSE);
		gtk_widget_set_sensitive(GTK_WIDGET(m_module5), FALSE);
	}
}

void CConfigurationBrainProductsActiCHamp::buttonModuleToggledCB(const bool state)
{
	const uint32_t nChannel = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(m_nChannels));
	// there is minimum 1 channel.
	if (state) { gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), nChannel + (nChannel == 1 ? 31 : 32)); }
	else { gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), nChannel - (nChannel == 1 ? 31 : 32)); }
}

void CConfigurationBrainProductsActiCHamp::buttonAuxChannelsToggledCB(const bool state)
{
	const uint32_t nChannel = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(m_nChannels));

	if (state) { gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), nChannel + (nChannel == 1 ? 7 : 8)); }
	else { gtk_spin_button_set_value(GTK_SPIN_BUTTON(m_nChannels), nChannel - (nChannel == 1 ? 7 : 8)); }
}
#endif // TARGET_OS_Windows
