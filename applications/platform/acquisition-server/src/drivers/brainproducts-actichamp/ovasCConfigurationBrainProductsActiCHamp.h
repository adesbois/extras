/*
 * ovasCConfigurationBrainProductsActiCHamp.h
 *
 * Copyright (c) 2012, Mensia Technologies SA. All rights reserved.
 * -- Rights transferred to Inria, contract signed 21.11.2014
 *
 */

#pragma once

#if defined TARGET_HAS_ThirdPartyActiCHampAPI

#include "../ovasCConfigurationBuilder.h"

#include <gtk/gtk.h>

namespace OpenViBE {
namespace AcquisitionServer {
class CConfigurationBrainProductsActiCHamp final : public CConfigurationBuilder
{
public:

	CConfigurationBrainProductsActiCHamp(const char* gtkBuilderFilename, uint32_t& deviceID, uint32_t& mode, uint32_t& physicalSampling,
										 uint32_t& adcDataFilter, uint32_t& adcDataDecimation, uint32_t& activeShieldGain, uint32_t& moduleEnabled,
										 bool& useAuxChannels, uint32_t& goodImpedanceLimit, uint32_t& badImpedanceLimit);

	bool preConfigure() override;
	bool postConfigure() override;

	void comboBoxDeviceChangedCB();
	void buttonModuleToggledCB(bool state);
	void buttonAuxChannelsToggledCB(bool state);

protected:

	uint32_t& m_deviceID;
	uint32_t& m_mode;
	uint32_t& m_physicalSampling;
	uint32_t& m_adcDataFilter;
	uint32_t& m_adcDataDecimation;
	uint32_t& m_activeShieldGain;
	uint32_t& m_moduleEnabled;
	bool& m_useAuxChannels;
	uint32_t& m_goodImpedanceLimit;
	uint32_t& m_badImpedanceLimit;

	GtkWidget* m_imageErrorIcon               = nullptr;
	GtkLabel* m_labelErrorMessage             = nullptr;
	GtkButton* m_buttonErrorDLLLink           = nullptr;
	GtkComboBox* m_comboBoxDeviceId           = nullptr;
	GtkComboBox* m_comboBoxMode               = nullptr;
	GtkComboBox* m_comboBoxPhysicalSampleRate = nullptr;
	GtkComboBox* m_comboBoxADCDataFilter      = nullptr;
	GtkComboBox* m_comboBoxADCDataDecimation  = nullptr;
	GtkSpinButton* m_buttonActiveShieldGain   = nullptr;
	GtkToggleButton* m_buttonUseAuxChannels   = nullptr;
	GtkSpinButton* m_buttonGoodImpedanceLimit = nullptr;
	GtkSpinButton* m_buttonBadImpedanceLimit  = nullptr;

	GtkToggleButton* m_module1 = nullptr;
	GtkToggleButton* m_module2 = nullptr;
	GtkToggleButton* m_module3 = nullptr;
	GtkToggleButton* m_module4 = nullptr;
	GtkToggleButton* m_module5 = nullptr;
};
}  // namespace AcquisitionServer
}  // namespace OpenViBE

#endif // TARGET_HAS_ThirdPartyActiCHampAPI
