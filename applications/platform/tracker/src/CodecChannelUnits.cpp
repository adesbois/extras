#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "TypeChannelUnits.h"

#include "Decoder.h"
#include "Encoder.h"

namespace OpenViBE {
namespace Tracker {
template <>
bool DecoderAdapter<TypeChannelUnits, Toolkit::TChannelUnitsDecoder<BoxAlgorithmProxy>>::getHeaderImpl(TypeChannelUnits::Header& h)
{
	h.m_Dynamic = m_impl.getOutputDynamic();

	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(h.m_Header, *decoded);

	return true;
}

template <>
bool DecoderAdapter<TypeChannelUnits, Toolkit::TChannelUnitsDecoder<BoxAlgorithmProxy>>::getBufferImpl(TypeChannelUnits::Buffer& b)
{
	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(b.m_buffer, *decoded);

	return true;
}

template <>
bool EncoderAdapter<TypeChannelUnits, Toolkit::TChannelUnitsEncoder<BoxAlgorithmProxy>>::encodeHeaderImpl(const TypeChannelUnits::Header& hdr)
{
	m_impl.getInputDynamic() = hdr.m_Dynamic;

	IMatrix* header = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*header, hdr.m_Header);

	return m_impl.encodeHeader();
}

template <>
bool EncoderAdapter<TypeChannelUnits, Toolkit::TChannelUnitsEncoder<BoxAlgorithmProxy>>::encodeBufferImpl(const TypeChannelUnits::Buffer& buf)
{
	IMatrix* buffer = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*buffer, buf.m_buffer);

	return m_impl.encodeBuffer();
}
}  // namespace Tracker
}  // namespace OpenViBE
