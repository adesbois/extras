#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "TypeChannelLocalization.h"

#include "Decoder.h"
#include "Encoder.h"

namespace OpenViBE {
namespace Tracker {
template <>
bool DecoderAdapter<TypeChannelLocalization, Toolkit::TChannelLocalisationDecoder<BoxAlgorithmProxy>>::getHeaderImpl(
	TypeChannelLocalization::Header& h)
{
	h.m_Dynamic = m_impl.getOutputDynamic();

	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(h.m_Header, *decoded);

	return true;
}

template <>
bool DecoderAdapter<TypeChannelLocalization, Toolkit::TChannelLocalisationDecoder<BoxAlgorithmProxy>>::getBufferImpl(
	TypeChannelLocalization::Buffer& b)
{
	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(b.m_buffer, *decoded);

	return true;
}

template <>
bool EncoderAdapter<TypeChannelLocalization, Toolkit::TChannelLocalisationEncoder<BoxAlgorithmProxy>>::encodeHeaderImpl(
	const TypeChannelLocalization::Header& hdr)
{
	m_impl.getInputDynamic() = hdr.m_Dynamic;

	IMatrix* header = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*header, hdr.m_Header);

	return m_impl.encodeHeader();
}

template <>
bool EncoderAdapter<TypeChannelLocalization, Toolkit::TChannelLocalisationEncoder<BoxAlgorithmProxy>>::encodeBufferImpl(
	const TypeChannelLocalization::Buffer& buf)
{
	IMatrix* buffer = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*buffer, buf.m_buffer);

	return m_impl.encodeBuffer();
}
}  // namespace Tracker
}  // namespace OpenViBE
