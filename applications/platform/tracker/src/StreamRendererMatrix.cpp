//
// OpenViBE Tracker
//
// @author J.T. Lindgren
// 
// @todo add horizontal scaling support
// @todo add event handlers
// @todo add ruler, stimulations, channel names, a million of other things

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <iostream>

#include <mensia/advanced-visualization.hpp>
#include <m_GtkGL.hpp>

#include <system/ovCTime.h>

#include "StreamRendererMatrix.h"


#include "ruler/mTRulerAutoType.hpp"
#include "ruler/mTRulerPair.hpp"
#include "ruler/mTRulerConditionalPair.hpp"
#include "ruler/mCRulerConditionIsTimeLocked.hpp"

#include "ruler/mCRulerProgressV.hpp"

#include "ruler/mCRulerBottomCount.hpp"
#include "ruler/mCRulerBottomTime.hpp"

#include "ruler/mCRulerLeftChannelNames.hpp"

#include "ruler/mCRulerRightCount.hpp"
#include "ruler/mCRulerRightScale.hpp"
#include "ruler/mCRulerRightLabels.hpp"
#include "ruler/mCRulerRightFrequency.hpp"

#include "ruler/mCRulerRightTexture.hpp"


using namespace Mensia;
using namespace AdvancedVisualization;

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Tracker;

bool StreamRendererMatrix::initialize()
{
	const TypeMatrix::Buffer* firstChunk = m_stream->getChunk(0);
	if (firstChunk)
	{
		m_chunkDuration = firstChunk->m_EndTime - firstChunk->m_StartTime;

		if (firstChunk->m_buffer.getDimensionCount() == 1)
		{
			// Degenerate case like a feature vector
			m_nRows = firstChunk->m_buffer.getDimensionSize(0);
			m_nCols = 0;
		}
		else if (firstChunk->m_buffer.getDimensionCount() == 2)
		{
			m_nRows = firstChunk->m_buffer.getDimensionSize(0);
			m_nCols = firstChunk->m_buffer.getDimensionSize(1);
		}
		else if (firstChunk->m_buffer.getDimensionCount() > 2)
		{
			std::cout << "Warning: The matrix renderer does not work correctly if dims>2 (for tensors)\n";
			m_nRows = firstChunk->m_buffer.getDimensionSize(0);
			m_nCols = firstChunk->m_buffer.getDimensionSize(1);
		}
		else { log() << LogLevel_Error << "Error: Dimension count " << firstChunk->m_buffer.getDimensionCount() << " not supported\n"; }
	}
	else
	{
		std::cout << "Stream is empty\n";
		return false;
	}


	m_rendererCtx = new CRendererContext();
	m_rendererCtx->clear();
	m_rendererCtx->setTimeScale(1);
	m_rendererCtx->setScaleVisibility(m_isScaleVisible);
	m_rendererCtx->setCheckBoardVisibility(true);
	m_rendererCtx->setTimeLocked(true);
	m_rendererCtx->setPositiveOnly(false);
	m_rendererCtx->setDataType(CRendererContext::EDataType::Matrix);
	m_rendererCtx->setSampleDuration(m_chunkDuration.time());
	m_rendererCtx->setParentRendererContext(&getContext());

	m_rendererCtx->setAxisDisplay(true);

	auto& hdr = m_stream->getHeader();

	for (uint32_t j = 0; j < m_nRows; ++j) { m_rendererCtx->addChannel(std::string(hdr.m_Header.getDimensionLabel(0, j))); }

	m_swap.resize(m_nRows);

	m_renderers.push_back(IRenderer::create(ERendererType::Bitmap, false));

	m_ruler = new TRulerPair<CRulerProgressV, TRulerPair<
								 TRulerAutoType<IRuler, TRulerConditionalPair<CRulerBottomTime, CRulerBottomCount, CRulerConditionIsTimeLocked>, IRuler>,
								 TRulerPair<CRulerLeftChannelNames, CRulerRightTexture>>>;

	//	m_pRuler = new TRulerPair < TRulerConditionalPair < CRulerBottomTime, CRulerBottomCount, CRulerConditionIsTimeLocked >, TRulerPair < TRulerAutoType < IRuler, IRuler, CRulerRightFrequency >, TRulerPair < CRulerLeftChannelNames, CRulerProgressV > > >;
	m_ruler->setRendererContext(m_rendererCtx);
	m_ruler->setRenderer(m_renderers[0]);

	if (!StreamRendererBase::initialize()) { return false; }

	m_gtkGLWidget.initialize(*this, m_viewport, m_left, m_right, m_bottom);
	m_gtkGLWidget.setPointSmoothingActive(false);

	return true;
}


bool StreamRendererMatrix::reset(const CTime startTime, const CTime endTime)
{
	m_startTime = startTime;
	m_endTime   = endTime;

	const uint32_t numBuffers = ((m_endTime - m_startTime).ceil().time() / m_chunkDuration.time());

	m_rendererCtx->setElementCount(numBuffers);

	m_renderers[0]->clear(0);
	m_renderers[0]->setSampleCount(numBuffers); // $$$
	m_renderers[0]->setChannelCount(m_nRows);

	//  @FIXME  The offset is needed to have correct numbers on the ruler; remove ifdef once the feature is in
#ifdef RENDERER_SUPPORTS_OFFSET
	m_renderers[0]->setTimeOffset(m_startTime.time());
#endif

	return true;
}


bool StreamRendererMatrix::push(const TypeMatrix::Buffer& chunk, bool /*zeroInput*/)
{
#if 0
	static uint32_t pushed = 0;
	std::cout << "Push spec chk " << pushed << " " << chunk.m_buffer.getDimensionSize(0)
		<< " " << chunk.m_buffer.getDimensionSize(1) << " "
		<< CTime(chunk.m_startTime).toSeconds() << "," 
		<< CTime(chunk.m_endTime).toSeconds()
		<< "\n";
	std::cout << pushed << " first bytes "
		<< chunk.m_buffer.getBuffer()[0]
		<< chunk.m_buffer.getBuffer()[1]
		<< chunk.m_buffer.getBuffer()[2]
		<< chunk.m_buffer.getBuffer()[3]
		<< "\n";
	pushed++;
#endif


	m_rendererCtx->setSpectrumFrequencyRange(uint32_t((uint64_t(m_nRows) << 32) / m_chunkDuration.time()));

	// Handle the degenerate case NumCols=0
	const size_t actualCols = std::max<size_t>(m_nCols, 1);

	// Feed renderer with actual samples
	for (uint32_t j = 0; j < actualCols; ++j)
	{
		for (uint32_t k = 0; k < m_nRows; ++k) { m_swap[k] = float(chunk.m_buffer.getBuffer()[k * actualCols + j]); }
		m_renderers[0]->feed(&m_swap[0]);
	}

	return true;
}


bool StreamRendererMatrix::draw()
{
	StreamRendererMatrix::preDraw();

	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glColor4f(m_color.r, m_color.g, m_color.b, m_rendererCtx->getTranslucency());
	m_renderers[0]->render(*m_rendererCtx);
	glPopAttrib();

	StreamRendererMatrix::postDraw();

	return true;
}

bool StreamRendererMatrix::preDraw()
{
	this->updateRulerVisibility();

	//	auto m_sColorGradient=CString("0:0,0,0; 100:100,100,100");
	const std::string gradient = "0:0, 0, 50; 25:0, 100, 100; 50:0, 50, 0; 75:100, 100, 0; 100:75, 0, 0";
	//	auto m_sColorGradient = CString("0:100, 100, 100; 12:50, 100, 100; 25:0, 50, 100; 38:0, 0, 50; 50:0, 0, 0; 62:50, 0, 0; 75:100, 50, 0; 88:100, 100, 50; 100:100, 100, 100");

	if (!m_textureID) { m_textureID = m_gtkGLWidget.createTexture(gradient); }
	glBindTexture(GL_TEXTURE_1D, m_textureID);

	m_rendererCtx->setAspect(m_viewport->allocation.width * 1.0F / m_viewport->allocation.height);

	return true;
}

bool StreamRendererMatrix::finalize()
{
	m_renderers[0]->rebuild(*m_rendererCtx);
	m_renderers[0]->refresh(*m_rendererCtx);

	redraw(true);

	return true;
}

CString StreamRendererMatrix::renderAsText(const size_t indent) const
{
	std::stringstream ss;
	ss << std::string(indent, ' ') << "Rows: " << m_nRows << std::endl;
	ss << std::string(indent, ' ') << "Cols: " << m_nCols << std::endl;

	//	ss << std::string(indent, ' ') << "Channels: " << m_Header.m_header.getDimensionSize(0) << std::endl;
	//	ss << std::string(indent, ' ') << "Samples per chunk: " << m_Header.m_header.getDimensionSize(1) << std::endl;
	return ss.str().c_str();
}

bool StreamRendererMatrix::mouseButton(const int x, const int y, const int button, const int status)
{
	//if (button == 3 && status == 1) { showChunkList(); }
	return StreamRendererBase::mouseButton(x, y, button, status);
}

bool StreamRendererMatrix::showChunkList() { return showMatrixList<TypeMatrix>(m_stream, &m_streamListWindow, "List of chunks for Matrix stream"); }
