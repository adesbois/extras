#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "TypeStimulation.h"

#include "Decoder.h"
#include "Encoder.h"

namespace OpenViBE {
namespace Tracker {

template <>
bool DecoderAdapter<TypeStimulation, Toolkit::TStimulationDecoder<BoxAlgorithmProxy>>::getHeaderImpl(TypeStimulation::Header& /*target*/) { return true; }

template <>
bool DecoderAdapter<TypeStimulation, Toolkit::TStimulationDecoder<BoxAlgorithmProxy>>::getBufferImpl(TypeStimulation::Buffer& b)
{
	IStimulationSet* decoded = m_impl.getOutputStimulationSet();

	b.m_buffer.clear();
	for (size_t i = 0; i < decoded->getStimulationCount(); ++i)
	{
		b.m_buffer.appendStimulation(decoded->getStimulationIdentifier(i), decoded->getStimulationDate(i), decoded->getStimulationDuration(i));
	}
	return true;
}

template <>
bool EncoderAdapter<TypeStimulation, Toolkit::TStimulationEncoder<BoxAlgorithmProxy>>::encodeHeaderImpl(const TypeStimulation::Header& /*hdr*/)
{
	return m_impl.encodeHeader();
}

template <>
bool EncoderAdapter<TypeStimulation, Toolkit::TStimulationEncoder<BoxAlgorithmProxy>>::encodeBufferImpl(const TypeStimulation::Buffer& buf)
{
	IStimulationSet* inputSet = m_impl.getInputStimulationSet();

	inputSet->clear();
	for (size_t i = 0; i < buf.m_buffer.getStimulationCount(); ++i)
	{
		inputSet->appendStimulation(buf.m_buffer.getStimulationIdentifier(i), buf.m_buffer.getStimulationDate(i) + m_offset.time(),
									buf.m_buffer.getStimulationDuration(i));
	}

	return m_impl.encodeBuffer();
}

}  // namespace Tracker
}  // namespace OpenViBE
