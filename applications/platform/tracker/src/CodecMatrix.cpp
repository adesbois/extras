#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "TypeMatrix.h"

#include "Decoder.h"
#include "Encoder.h"

namespace OpenViBE {
namespace Tracker {
template <>
bool DecoderAdapter<TypeMatrix, Toolkit::TStreamedMatrixDecoder<BoxAlgorithmProxy>>::getHeaderImpl(TypeMatrix::Header& h)
{
	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(h.m_Header, *decoded);

	return true;
}


template <>
bool DecoderAdapter<TypeMatrix, Toolkit::TStreamedMatrixDecoder<BoxAlgorithmProxy>>::getBufferImpl(TypeMatrix::Buffer& b)
{
	const IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(b.m_buffer, *decoded);

	return true;
}

template <>
bool EncoderAdapter<TypeMatrix, Toolkit::TStreamedMatrixEncoder<BoxAlgorithmProxy>>::encodeHeaderImpl(const TypeMatrix::Header& hdr)
{
	IMatrix* header = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*header, hdr.m_Header);

	return m_impl.encodeHeader();
}

template <>
bool EncoderAdapter<TypeMatrix, Toolkit::TStreamedMatrixEncoder<BoxAlgorithmProxy>>::encodeBufferImpl(const TypeMatrix::Buffer& buf)
{
	IMatrix* buffer = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*buffer, buf.m_buffer);

	return m_impl.encodeBuffer();
}
}  // namespace Tracker
}  // namespace OpenViBE
