//
// OpenViBE Tracker
//
// @author J.T. Lindgren
// 

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <iostream>

#include <system/ovCTime.h>

#include "StreamRendererExperimentInfo.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Tracker;
using namespace std;

CString StreamRendererExperimentInfo::renderAsText(const size_t indent) const
{
	const TypeExperimentInfo::Header& hdr = m_stream->getHeader();

	stringstream ss;

	ss << string(indent, ' ') << "Experiment id: " << hdr.m_ExperimentID << endl;
	ss << string(indent, ' ') << "Experiment date: " << hdr.m_ExperimentDate << endl;

	ss << string(indent, ' ') << "Subject id: " << hdr.m_SubjectID << endl;
	ss << string(indent, ' ') << "Subject name: " << hdr.m_SubjectName << endl;
	ss << string(indent, ' ') << "Subject age: " << hdr.m_SubjectAge << endl;
	ss << string(indent, ' ') << "Subject gender: " << hdr.m_SubjectGender << endl;

	ss << string(indent, ' ') << "Laboratory id: " << hdr.m_LaboratoryID << endl;
	ss << string(indent, ' ') << "Laboratory name: " << hdr.m_LaboratoryName << endl;
	ss << string(indent, ' ') << "Technician id: " << hdr.m_TechnicianID << endl;
	ss << string(indent, ' ') << "Technician name: " << hdr.m_TechnicianName << endl;

	//	ss << string(indent, ' ') << "Channels: " << m_Header.m_header.getDimensionSize(0) << endl;
	//	ss << string(indent, ' ') << "Samples per chunk: " << m_Header.m_header.getDimensionSize(1) << endl;
	return ss.str().c_str();
}

bool StreamRendererExperimentInfo::showChunkList() { return StreamRendererLabel::showChunkList("Experiment information stream details"); }
