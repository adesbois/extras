#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "TypeSignal.h"

#include "Decoder.h"
#include "Encoder.h"

namespace OpenViBE {
namespace Tracker {
template <>
bool DecoderAdapter<TypeSignal, Toolkit::TSignalDecoder<BoxAlgorithmProxy>>::getHeaderImpl(TypeSignal::Header& h)
{
	h.m_Sampling = m_impl.getOutputSamplingRate();

	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(h.m_Header, *decoded);

	return true;
}

template <>
bool DecoderAdapter<TypeSignal, Toolkit::TSignalDecoder<BoxAlgorithmProxy>>::getBufferImpl(TypeSignal::Buffer& b)
{
	const IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(b.m_buffer, *decoded);

	return true;
}

template <>
bool EncoderAdapter<TypeSignal, Toolkit::TSignalEncoder<BoxAlgorithmProxy>>::encodeHeaderImpl(const TypeSignal::Header& hdr)
{
	m_impl.getInputSamplingRate() = hdr.m_Sampling;

	IMatrix* header = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*header, hdr.m_Header);

	return m_impl.encodeHeader();
}

template <>
bool EncoderAdapter<TypeSignal, Toolkit::TSignalEncoder<BoxAlgorithmProxy>>::encodeBufferImpl(const TypeSignal::Buffer& buf)
{
	IMatrix* buffer = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*buffer, buf.m_buffer);

	return m_impl.encodeBuffer();
}
}  // namespace Tracker
}  // namespace OpenViBE
