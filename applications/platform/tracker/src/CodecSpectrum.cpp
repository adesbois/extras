#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "TypeSpectrum.h"

#include "Decoder.h"
#include "Encoder.h"

namespace OpenViBE {
namespace Tracker {
template <>
bool DecoderAdapter<TypeSpectrum, Toolkit::TSpectrumDecoder<BoxAlgorithmProxy>>::getHeaderImpl(TypeSpectrum::Header& h)
{
	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(h.m_Header, *decoded);

	h.m_Sampling = m_impl.getOutputSamplingRate();

	IMatrix* abscissas = m_impl.getOutputFrequencyAbscissa();
	Toolkit::Matrix::copy(h.m_Abscissas, *abscissas);

	return true;
}

template <>
bool DecoderAdapter<TypeSpectrum, Toolkit::TSpectrumDecoder<BoxAlgorithmProxy>>::getBufferImpl(TypeSpectrum::Buffer& b)
{
	const IMatrix* decoded = m_impl.getOutputMatrix();

	Toolkit::Matrix::copy(b.m_buffer, *decoded);

	return true;
}

template <>
bool EncoderAdapter<TypeSpectrum, Toolkit::TSpectrumEncoder<BoxAlgorithmProxy>>::encodeHeaderImpl(const TypeSpectrum::Header& hdr)
{
	m_impl.getInputSamplingRate() = hdr.m_Sampling;

	IMatrix* header = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*header, hdr.m_Header);

	IMatrix* abscissas = m_impl.getInputFrequencyAbscissa();
	Toolkit::Matrix::copy(*abscissas, hdr.m_Abscissas);

	return m_impl.encodeHeader();
}

template <>
bool EncoderAdapter<TypeSpectrum, Toolkit::TSpectrumEncoder<BoxAlgorithmProxy>>::encodeBufferImpl(const TypeSpectrum::Buffer& buf)
{
	IMatrix* buffer = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*buffer, buf.m_buffer);

	return m_impl.encodeBuffer();
}
}  // namespace Tracker
}  // namespace OpenViBE
