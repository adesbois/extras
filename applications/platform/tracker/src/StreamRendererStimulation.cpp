//
// OpenViBE Tracker
//
// @author J.T. Lindgren
// 
// @note This renderer is intended to be used when there is only 
// a stimulation track. More often, the Tracker users might want to
// see the stimulations overlayed on the signal track.
//
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <iostream>

#include <mensia/advanced-visualization.hpp>
#include <mTGtkGLWidget.hpp>
#include <m_GtkGL.hpp>

#include "StreamRendererStimulation.h"
#include "TypeStimulation.h"

using namespace Mensia;

#include "ruler/mTRulerAutoType.hpp"
#include "ruler/mTRulerPair.hpp"
#include "ruler/mTRulerConditionalPair.hpp"
#include "ruler/mCRulerConditionIsTimeLocked.hpp"

#include "ruler/mCRulerProgressV.hpp"

#include "ruler/mCRulerBottomCount.hpp"
#include "ruler/mCRulerBottomTime.hpp"

#include "ruler/mCRulerLeftChannelNames.hpp"

#include "ruler/mCRulerRightCount.hpp"
#include "ruler/mCRulerRightScale.hpp"
#include "ruler/mCRulerRightLabels.hpp"

using namespace AdvancedVisualization;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Tracker;

bool StreamRendererStimulation::initialize()
{
	m_nChannel        = 1;
	m_sampling        = 512;
	m_samplesPerChunk = 32;
	m_chunkDuration   = CTime(m_sampling, m_samplesPerChunk);

	// TRendererStimulation < false, CRendererLine >:new CRendererLine
	IRenderer* renderer = IRenderer::create(ERendererType::Line, true);
	if (renderer == nullptr) { return false; }

	// Creates renderer context
	m_rendererCtx = new CRendererContext();
	m_rendererCtx->clear();
	m_rendererCtx->setTimeScale(1);
	m_rendererCtx->setCheckBoardVisibility(true);
	m_rendererCtx->setScaleVisibility(m_isScaleVisible);
	m_rendererCtx->setDataType(CRendererContext::EDataType::Signal);
	m_rendererCtx->addChannel("Stims");
	m_rendererCtx->setSampleDuration(CTime(m_sampling, 1).time());

	m_ruler = new TRulerPair<CRulerProgressV, TRulerPair<
								 TRulerAutoType<IRuler, TRulerConditionalPair<CRulerBottomTime, CRulerBottomCount, CRulerConditionIsTimeLocked>, IRuler>,
								 TRulerPair<CRulerLeftChannelNames, CRulerRightScale>>>;

	m_ruler->setRendererContext(m_rendererCtx);
	m_ruler->setRenderer(renderer);

	if (!StreamRendererBase::initialize()) { return false; }

	m_gtkGLWidget.initialize(*this, m_viewport, m_left, m_right, m_bottom);
	m_gtkGLWidget.setPointSmoothingActive(false);

	m_renderers.push_back(renderer);

	gtk_widget_set_size_request(m_main, 640, 100);

	return true;
}

bool StreamRendererStimulation::reset(const CTime startTime, const CTime endTime)
{
	m_startTime = startTime;
	m_endTime   = endTime;

	//	std::cout << "Overridden stimulation renderer reset\n";

	m_renderers[0]->clear(0);

	const uint64_t chunkCount = (m_endTime - m_startTime).time() / m_chunkDuration.time();
	const uint32_t numSamples = uint32_t(m_samplesPerChunk * chunkCount);

	m_renderers[0]->setChannelCount(m_nChannel);
	m_renderers[0]->setSampleCount(numSamples);

	//  @FIXME The offset is needed to have correct numbers on the ruler; remove ifdef once the feature is in
#ifdef RENDERER_SUPPORTS_OFFSET
	m_renderers[0]->setTimeOffset(m_startTime.time());
#endif

	// Stick in empty chunks to get a background
	std::vector<float> empty;
	empty.resize(m_samplesPerChunk * 1, 0);  // 1 channel
	for (uint64_t i = 0; i < chunkCount; ++i) { m_renderers[0]->feed(&empty[0], m_samplesPerChunk); }

	m_renderers[0]->rebuild(*m_rendererCtx);

	return true;
}

bool StreamRendererStimulation::push(const TypeStimulation::Buffer& chunk, bool /*zeroInput*/)
{
	for (size_t i = 0; i < chunk.m_buffer.getStimulationCount(); ++i)
	{
		m_renderers[0]->feed(chunk.m_buffer.getStimulationDate(i) - m_startTime.time(), chunk.m_buffer.getStimulationIdentifier(i));
	}

	return true;
}

CString StreamRendererStimulation::renderAsText(const size_t /*indent*/) const
{
	// No specific details for stimulation streams
	return "";
}

bool StreamRendererStimulation::showChunkList()
{
	if (m_stimulationListWindow)
	{
		gtk_window_present(GTK_WINDOW(m_stimulationListWindow));
		return true;
	}

	GtkBuilder* pBuilder   = gtk_builder_new();
	const CString filename = Directories::getDataDir() + "/applications/tracker/tracker.ui";
	if (!gtk_builder_add_from_file(pBuilder, filename, nullptr))
	{
		std::cout << "Problem loading [" << filename << "]\n";
		return false;
	}

	m_stimulationListWindow      = GTK_WIDGET(::gtk_builder_get_object(pBuilder, "tracker-stimulation_list"));
	GtkTreeView* channelTreeView = GTK_TREE_VIEW(::gtk_builder_get_object(pBuilder, "tracker-stimulation_list-treeview"));
	// GtkListStore* channelListStore = GTK_LIST_STORE(::gtk_builder_get_object(pBuilder, "liststore_select"));
	GtkTreeStore* channelListStore = gtk_tree_store_new(7, G_TYPE_UINT, G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_UINT64, G_TYPE_STRING,
														G_TYPE_DOUBLE);

	gtk_window_set_title(GTK_WINDOW(m_stimulationListWindow), "List of Stimulations in the stream");

	add_column(channelTreeView, "Chunk#", 0, 20);
	add_column(channelTreeView, "ChunkStart (s)", 1, 10);
	add_column(channelTreeView, "ChunkEnd (s)", 2, 10);
	add_column(channelTreeView, "Stim Time (s)", 3, 20);
	add_column(channelTreeView, "Stim Id", 4, 5);
	add_column(channelTreeView, "Stim Name", 5, 40);
	add_column(channelTreeView, "Stim Duration", 6, 10);

	gtk_tree_view_set_model(channelTreeView, GTK_TREE_MODEL(channelListStore));

	GtkTreeIter it;
	gtk_tree_store_clear(channelListStore);

	// ::gtk_tree_view_set_model(m_pChannelTreeView, nullptr);
	for (size_t i = 0; i < m_stream->getChunkCount(); ++i)
	{
		const TypeStimulation::Buffer* ptr = m_stream->getChunk(i);
		if (!ptr) { break; }

		for (uint32_t s = 0; s < ptr->m_buffer.getStimulationCount(); ++s)
		{
			const CString stimName = m_kernelCtx.getTypeManager().getEnumerationEntryNameFromValue(
				OV_TypeId_Stimulation, ptr->m_buffer.getStimulationIdentifier(s));

			gtk_tree_store_append(channelListStore, &it, nullptr);
			gtk_tree_store_set(channelListStore, &it,
							   0, i,
							   1, ptr->m_StartTime.toSeconds(),
							   2, ptr->m_EndTime.toSeconds(),
							   3, CTime(ptr->m_buffer.getStimulationDate(s)).toSeconds(),
							   4, ptr->m_buffer.getStimulationIdentifier(s),
							   5, stimName.toASCIIString(),
							   6, CTime(ptr->m_buffer.getStimulationDuration(s)).toSeconds(), -1);
		}
	}

	//	GList* cols = gtk_tree_view_get_columns(m_pChannelTreeView);

	// Hide instead of destroy on closing the window
	g_signal_connect(m_stimulationListWindow, "delete_event", G_CALLBACK(gtk_widget_hide_on_delete), nullptr);
	gtk_widget_show_all(GTK_WIDGET(m_stimulationListWindow));
	g_object_unref(pBuilder);

	return true;
}

bool StreamRendererStimulation::mouseButton(const int x, const int y, const int button, const int status)
{
	// if (button == 3 && status == 1) { showStimulationList(); }
	return StreamRendererBase::mouseButton(x, y, button, status);
}
