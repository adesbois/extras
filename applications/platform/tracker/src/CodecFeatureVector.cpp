// @todo this is identical to CodecFeatureMatrix. Refactor?

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "TypeFeatureVector.h"

#include "Decoder.h"
#include "Encoder.h"

namespace OpenViBE {
namespace Tracker {
template <>
bool DecoderAdapter<TypeFeatureVector, Toolkit::TFeatureVectorDecoder<BoxAlgorithmProxy>>::getHeaderImpl(TypeFeatureVector::Header& h)
{
	IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(h.m_Header, *decoded);

	return true;
}

template <>
bool DecoderAdapter<TypeFeatureVector, Toolkit::TFeatureVectorDecoder<BoxAlgorithmProxy>>::getBufferImpl(TypeFeatureVector::Buffer& b)
{
	const IMatrix* decoded = m_impl.getOutputMatrix();
	Toolkit::Matrix::copy(b.m_buffer, *decoded);

	return true;
}

template <>
bool EncoderAdapter<TypeFeatureVector, Toolkit::TFeatureVectorEncoder<BoxAlgorithmProxy>>::encodeHeaderImpl(
	const TypeFeatureVector::Header& hdr)
{
	IMatrix* buffer = m_impl.getInputMatrix();
	Toolkit::Matrix::copy(*buffer, hdr.m_Header);

	return m_impl.encodeHeader();
}

template <>
bool EncoderAdapter<TypeFeatureVector, Toolkit::TFeatureVectorEncoder<BoxAlgorithmProxy>>::encodeBufferImpl(
	const TypeFeatureVector::Buffer& buf)
{
	IMatrix* buffer = m_impl.getInputMatrix();

	Toolkit::Matrix::copy(*buffer, buf.m_buffer);

	return m_impl.encodeBuffer();
}
}  // namespace Tracker
}  // namespace OpenViBE
