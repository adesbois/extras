//
// OpenViBE Tracker
//
// @author J.T. Lindgren
// 

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <iostream>

#include <mensia/advanced-visualization.hpp>
#include <mTGtkGLWidget.hpp>
#include <m_GtkGL.hpp>

#include "StreamRendererSignal.h"

#include "ruler/mTRulerAutoType.hpp"
#include "ruler/mTRulerPair.hpp"
#include "ruler/mTRulerConditionalPair.hpp"
#include "ruler/mCRulerConditionIsTimeLocked.hpp"

#include "ruler/mCRulerProgressV.hpp"

#include "ruler/mCRulerBottomCount.hpp"
#include "ruler/mCRulerBottomTime.hpp"

#include "ruler/mCRulerLeftChannelNames.hpp"

#include "ruler/mCRulerRightCount.hpp"
#include "ruler/mCRulerRightScale.hpp"
#include "ruler/mCRulerRightLabels.hpp"

using namespace Mensia;
using namespace AdvancedVisualization;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Tracker;

bool StreamRendererSignal::initialize()
{
	// TRendererStimulation < false, CRendererLine >:new CRendererLine
	IRenderer* pRenderer = IRenderer::create(ERendererType::Line, true);
	if (pRenderer == nullptr) { return false; }

	const TypeSignal::Buffer* firstChunk = m_stream->getChunk(0);
	if (!firstChunk) { return false; }

	m_nChannel        = firstChunk->m_buffer.getDimensionSize(0);
	m_samplesPerChunk = firstChunk->m_buffer.getDimensionSize(1);
	m_chunkDuration   = firstChunk->m_EndTime - firstChunk->m_StartTime;

	// Creates renderer context
	m_rendererCtx = new CRendererContext();
	m_rendererCtx->clear();
	m_rendererCtx->setScale(1.0);
	m_rendererCtx->setTimeScale(1);
	m_rendererCtx->setCheckBoardVisibility(true);
	m_rendererCtx->setScaleVisibility(m_isScaleVisible);
	m_rendererCtx->setDataType(CRendererContext::EDataType::Signal);
	const CTime sampleDuration = CTime(m_chunkDuration.time() / m_samplesPerChunk);
	m_rendererCtx->setSampleDuration(sampleDuration.time());

	const TypeSignal::Header& hdr = m_stream->getHeader();
	for (uint32_t i = 0; i < m_nChannel; ++i)
	{
		const char* label = hdr.m_Header.getDimensionLabel(0, i);
		m_rendererCtx->addChannel(std::string(label));
	}

	m_ruler = new TRulerPair<CRulerProgressV, TRulerPair<
								 TRulerAutoType<IRuler, TRulerConditionalPair<CRulerBottomTime, CRulerBottomCount, CRulerConditionIsTimeLocked>, IRuler>,
								 TRulerPair<CRulerLeftChannelNames, CRulerRightScale>>>;

	m_ruler->setRendererContext(m_rendererCtx);
	m_ruler->setRenderer(pRenderer);

	if (!StreamRendererBase::initialize()) { return false; }

	m_gtkGLWidget.initialize(*this, m_viewport, m_left, m_right, m_bottom);
	m_gtkGLWidget.setPointSmoothingActive(false);

	m_renderers.push_back(pRenderer);

	return true;
}

bool StreamRendererSignal::reset(const CTime startTime, const CTime endTime)
{
	m_startTime = startTime;
	m_endTime   = endTime;

	// 	std::cout << "Overridden signal renderer reset\n";

	m_renderers[0]->clear(0);

	//	std::cout << "Start time is " << CTime(m_StartTime).toSeconds()
	//		<< " end is " << CTime(m_EndTime).toSeconds() << "\n";

	const uint64_t chunkCount = (m_endTime - m_startTime).ceil().time() / m_chunkDuration.time();
	const uint32_t numSamples = uint32_t(m_samplesPerChunk * chunkCount);

	m_renderers[0]->setSampleCount(numSamples);
	m_renderers[0]->setChannelCount(m_nChannel);

	// @FIXME The offset is needed to have correct numbers on the ruler; remove ifdef once the feature is in
#ifdef RENDERER_SUPPORTS_OFFSET
	m_renderers[0]->setTimeOffset(m_startTime.time());
#endif

	// m_renderers[0]->setHistoryDrawIndex(samplesBeforeStart);
	m_renderers[0]->rebuild(*m_rendererCtx);

	return true;
}

bool StreamRendererSignal::push(const TypeSignal::Buffer& chunk, const bool zeroInput /* = false */)
{
	std::vector<float> tmp;
	if (!zeroInput)
	{
		tmp.resize(chunk.m_buffer.getBufferElementCount());
		for (size_t i = 0; i < chunk.m_buffer.getBufferElementCount(); ++i) { tmp[i] = float(chunk.m_buffer.getBuffer()[i]); }
	}
	else { tmp.resize(chunk.m_buffer.getBufferElementCount(), 0); }

	m_renderers[0]->feed(&tmp[0], chunk.m_buffer.getDimensionSize(1));

	return true;
}

CString StreamRendererSignal::renderAsText(const size_t indent) const
{
	auto& hdr = m_stream->getHeader();

	std::stringstream ss;
	ss << std::string(indent, ' ') << "Sampling rate: " << hdr.m_Sampling << "hz" << std::endl;
	ss << std::string(indent, ' ') << "Channels: " << hdr.m_Header.getDimensionSize(0) << std::endl;
	ss << std::string(indent, ' ') << "Samples per chunk: " << hdr.m_Header.getDimensionSize(1) << std::endl;

	return ss.str().c_str();
}

bool StreamRendererSignal::mouseButton(const int x, const int y, const int button, const int status)
{
	//if (button == 3 && status == 1) { showChunkList(); }
	return StreamRendererBase::mouseButton(x, y, button, status);
}

bool StreamRendererSignal::showChunkList() { return showMatrixList<TypeSignal>(m_stream, &m_streamListWindow, "List of chunks for Signal stream"); }
