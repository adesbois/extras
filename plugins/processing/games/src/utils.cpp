#include <iostream>
#ifdef TARGET_HAS_ThirdPartyLSL

#include "utils.hpp"

//-------------------------------------------------------------------------------------------------
lsl::stream_info createSignalStreamInfo(const std::string& name, const std::string& id, const OpenViBE::IMatrix* matrix, const size_t frequency)
{
	const size_t nChannel = matrix->getDimensionSize(0);

	// Open a signal stream 
	lsl::stream_info res(name, "signal", nChannel, double(frequency), lsl::cf_float32, id);

	lsl::xml_element channels = res.desc().append_child("channels");
	for (size_t c = 0; c < nChannel; ++c)
	{
		const std::string tmp = matrix->getDimensionLabel(0, c);
		channels.append_child("channel").append_child_value("label", tmp).append_child_value("unit", "unknown").append_child_value("type", "signal");
	}

	return res;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
lsl::stream_info createStimulationStreamInfo(const std::string& name, const std::string& id)
{
	lsl::stream_info res(name, "Markers", 1, lsl::IRREGULAR_RATE, lsl::cf_int32, id);
	res.desc().append_child("channels").append_child("channel").append_child_value("label", "Stimulations").append_child_value("type", "marker");
	return res;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void sendSignal(lsl::stream_outlet* outlet, const OpenViBE::IMatrix* matrix, const uint64_t startTime, const uint64_t endTime)
{
	if (outlet->have_consumers())
	{
		const size_t nChannel = matrix->getDimensionSize(0);
		const size_t nSamples = matrix->getDimensionSize(1);
		const double* iBuffer = matrix->getBuffer();
		std::vector<float> buffer(nChannel);

		// note: the step computed below should be exactly the same as could be obtained from the sampling rate
		const double start = OpenViBE::CTime(startTime).toSeconds();
		const double step  = OpenViBE::CTime(endTime - startTime).toSeconds() / double(nSamples);

		for (size_t s = 0; s < nSamples; ++s)
		{
			for (size_t c = 0; c < nChannel; ++c) { buffer[c] = float(iBuffer[c * nSamples + s]); }
			outlet->push_sample(buffer, start + s * step);
		}
	}
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void sendStimulation(lsl::stream_outlet* outlet, const OpenViBE::IStimulationSet* stimSet)
{
	if (outlet->have_consumers())
	{
		for (size_t s = 0; s < stimSet->getStimulationCount(); ++s)
		{
			const int code    = int(stimSet->getStimulationIdentifier(s));
			const double date = OpenViBE::CTime(stimSet->getStimulationDate(s)).toSeconds();

			outlet->push_sample(&code, date);
		}
	}
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
lsl::stream_info findStreamInfo(const std::string& name, const std::string& id, const int timeout)
{
	// Find the signal stream
	const std::vector<lsl::stream_info> infos = lsl::resolve_stream("name", name, 1, timeout);
	if (infos.empty())
	{
		std::cerr << "Failed to find stream with name [" << name << "]\n";
		return lsl::stream_info();
	}

	for (const auto& i : infos)
	{
		if (i.source_id() == id) { return i; }	// This is the best one
	}
	return infos[0];
}
//-------------------------------------------------------------------------------------------------
#endif // TARGET_HAS_ThirdPartyLSL
