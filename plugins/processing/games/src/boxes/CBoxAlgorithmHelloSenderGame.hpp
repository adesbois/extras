///-------------------------------------------------------------------------------------------------
/// 
/// \file CBoxAlgorithmHelloSenderGame.hpp
/// \brief Class of the box that communicate with Hello Sender Game.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#ifdef TARGET_HAS_ThirdPartyLSL

#include "defines.hpp"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <lsl_cpp.h>

#include <ctime>
#include <iostream>

namespace OpenViBE {
namespace Plugins {
namespace Games {

/// <summary> The class CBoxAlgorithmHelloSenderGame describes the box that send value in LSL. </summary>
class CBoxAlgorithmHelloSenderGame final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	uint64_t getClockFrequency() override { return 64LL << 32; }
	bool initialize() override;
	bool uninitialize() override;

	bool processClock(Kernel::CMessageClock& msg) override;

	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, GAME_BOX_HELLO_SENDER)

protected:

	// Decoders
	Toolkit::TStimulationEncoder<CBoxAlgorithmHelloSenderGame> m_stimEncoder;
	Toolkit::TSignalEncoder<CBoxAlgorithmHelloSenderGame> m_signalEncoder;
	IMatrix* m_oMatrix          = nullptr;
	IStimulationSet* m_oStimSet = nullptr;
	float* m_buffer             = nullptr;
	double m_headerSent         = false;
	uint64_t m_lastMatrixTime   = 0;
	uint64_t m_lastStimTime     = 0;

	lsl::stream_inlet *m_signalInlet = nullptr, *m_stimInlet    = nullptr;
	const std::string m_signalName   = "ovSignal", m_markerName = "ovMarker";
};


/// <summary> Descriptor of the box Hello Sender Game. </summary>
class CBoxAlgorithmHelloSenderGameDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return "Hello Sender Game"; }
	CString getAuthorName() const override { return "Thibaut Monseigne"; }
	CString getAuthorCompanyName() const override { return "Inria"; }
	CString getShortDescription() const override { return "Receive stream via LabStreamingLayer (LSL) from the unity game Hello Sender."; }
	CString getDetailedDescription() const override
	{
		return "The Unity Game is in the OpenViBE Unity Game Set of Repository (https://gitlab.inria.fr/openvibe/unity-games/hello-sender).\n";
	}
	CString getCategory() const override { return "Games"; }
	CString getVersion() const override { return "0.1"; }
	CString getStockItemName() const override { return "gtk-connect"; }

	CIdentifier getCreatedClass() const override { return GAME_BOX_HELLO_SENDER; }
	IPluginObject* create() override { return new CBoxAlgorithmHelloSenderGame; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addOutput("Output matrix", OV_TypeId_StreamedMatrix);
		prototype.addOutput("Output stimulations", OV_TypeId_Stimulations);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, GAME_BOX_HELLO_SENDER_DESC)
};

} /// namespace Games
} /// namespace Plugins
} /// namespace OpenViBE


#endif // TARGET_HAS_ThirdPartyLSL
