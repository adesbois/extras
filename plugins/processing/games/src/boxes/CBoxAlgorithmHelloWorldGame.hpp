///-------------------------------------------------------------------------------------------------
/// 
/// \file CBoxAlgorithmHelloWorldGame.hpp
/// \brief Class of the box that communicate with Hello World Game.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#ifdef TARGET_HAS_ThirdPartyLSL

#include "defines.hpp"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <lsl_cpp.h>

#include <ctime>
#include <iostream>

namespace OpenViBE {
namespace Plugins {
namespace Games {

/// <summary> The class CBoxAlgorithmHelloWorldGame describes the box that send value in LSL. </summary>
class CBoxAlgorithmHelloWorldGame final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;

	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, GAME_BOX_HELLO_WORLD)

protected:

	// Decoders
	Toolkit::TStimulationDecoder<CBoxAlgorithmHelloWorldGame> m_stimDecoder;
	Toolkit::TSignalDecoder<CBoxAlgorithmHelloWorldGame> m_signalDecoder;
	IMatrix* m_iMatrix          = nullptr;
	IStimulationSet* m_iStimSet = nullptr;

	lsl::stream_outlet *m_signalOutlet = nullptr, *m_stimOutlet   = nullptr;
	const std::string m_signalName     = "ovSignal", m_markerName = "ovMarkers";
	std::string m_signalID, m_markerID;
};


/// <summary> Descriptor of the box Hello World Game. </summary>
class CBoxAlgorithmHelloWorldGameDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return "Hello World Game"; }
	CString getAuthorName() const override { return "Thibaut Monseigne"; }
	CString getAuthorCompanyName() const override { return "Inria"; }
	CString getShortDescription() const override { return "Send input stream via LabStreamingLayer (LSL) to the unity game Hello World."; }
	CString getDetailedDescription() const override
	{
		return "The Unity Game is in the OpenViBE Unity Game Set of Repository (https://gitlab.inria.fr/openvibe/unity-games/hello-world).\n";
	}
	CString getCategory() const override { return "Games"; }
	CString getVersion() const override { return "0.1"; }
	CString getStockItemName() const override { return "gtk-connect"; }

	CIdentifier getCreatedClass() const override { return GAME_BOX_HELLO_WORLD; }
	IPluginObject* create() override { return new CBoxAlgorithmHelloWorldGame; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input signal", OV_TypeId_Signal);
		prototype.addInput("Input stimulations", OV_TypeId_Stimulations);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, GAME_BOX_HELLO_WORLD_DESC)
};

} /// namespace Games
} /// namespace Plugins
} /// namespace OpenViBE

#endif // TARGET_HAS_ThirdPartyLSL
