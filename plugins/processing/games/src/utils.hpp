///-------------------------------------------------------------------------------------------------
/// 
/// \file utils.hpp
/// \brief Defines some function to facilitate Box Management.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#ifdef TARGET_HAS_ThirdPartyLSL

#include <openvibe/ov_all.h>
#include <lsl_cpp.h>

//-------------------------------------------------------------------------------------------------
// Sending
//-------------------------------------------------------------------------------------------------

/// <summary> Create the stream information for signals.</summary>
/// <param name="name"> The name of the stream. </param>
/// <param name="id"> The identifier of the stream. </param>
/// <param name="matrix"> The matrix to get infos (channel number and label).</param>
/// <param name="frequency"> The signal frequency.</param>
/// <returns> the stream info. </returns>
lsl::stream_info createSignalStreamInfo(const std::string& name, const std::string& id, const OpenViBE::IMatrix* matrix, const size_t frequency);

/// <summary> Create the stream information for stimulations.</summary>
/// <param name="name"> The name of the stream. </param>
/// <param name="id"> The identifier of the stream. </param>
/// <returns> the stream info. </returns>
lsl::stream_info createStimulationStreamInfo(const std::string& name, const std::string& id);

/// <summary> Send Signal in LSL outlet. </summary>
/// <param name="outlet"> The LSL outlet. </param>
/// <param name="matrix"> The matrix with the signal to send. </param>
/// <param name="startTime"> The start time of the signal. </param>
/// <param name="endTime"> The end time of the signal. </param>
void sendSignal(lsl::stream_outlet* outlet, const OpenViBE::IMatrix* matrix, const uint64_t startTime, const uint64_t endTime);

/// <summary> Send the stimulation in LSL outlet. </summary>
/// <param name="outlet"> The LSL outlet. </param>
/// <param name="stimSet">The stimulation set to send. </param>
void sendStimulation(lsl::stream_outlet* outlet, const OpenViBE::IStimulationSet* stimSet);


//-------------------------------------------------------------------------------------------------
// Receiving
//-------------------------------------------------------------------------------------------------

/// <summary> Finds the stream information with the function <c>lsl::resolve_stream</c>. </summary>
/// <param name="name"> The expected name of the stream. </param>
/// <param name="id"> Optionally The expected ID of the stream in addition of the name. </param>
/// <param name="timeout"> Optionally a timeout of the operation, in seconds (default: no timeout).
/// If the timeout expires, a default stream info is returned. </param>
/// <returns></returns>
lsl::stream_info findStreamInfo(const std::string& name, const std::string& id = "", const int timeout = LSL_FOREVER);

#endif // TARGET_HAS_ThirdPartyLSL
