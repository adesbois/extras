#include <openvibe/ov_all.h>
//#include "defines.hpp"

// Boxes Includes
#include "boxes/CBoxAlgorithmHelloSenderGame.hpp"
#include "boxes/CBoxAlgorithmHelloWorldGame.hpp"

namespace OpenViBE {
namespace Plugins {

OVP_Declare_Begin()
#ifdef TARGET_HAS_ThirdPartyLSL
	// Register boxes
	OVP_Declare_New(Games::CBoxAlgorithmHelloWorldGameDesc);
	OVP_Declare_New(Games::CBoxAlgorithmHelloSenderGameDesc);
#endif // TARGET_HAS_ThirdPartyLSL
OVP_Declare_End()

}
}
