///-------------------------------------------------------------------------------------------------
/// 
/// \file defines.hpp
/// \brief Defines list for Setting, Shortcut Macro and const.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/03/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define GAME_BOX_HELLO_WORLD						OpenViBE::CIdentifier(0x46705bc3, 0x3058a939)
#define GAME_BOX_HELLO_WORLD_DESC					OpenViBE::CIdentifier(0x3f921b93, 0xadb2b2ec)
#define GAME_BOX_HELLO_SENDER						OpenViBE::CIdentifier(0x4cbf4237, 0x24f54a7c)
#define GAME_BOX_HELLO_SENDER_DESC					OpenViBE::CIdentifier(0x5730a8f1, 0x96a608cc)
