#pragma once

#if defined(TARGET_HAS_ThirdPartyGTK)

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gtk/gtk.h>
#include <vector>

namespace OpenViBE {
namespace Plugins {
namespace Evaluation {
typedef std::pair<double, double> CCoordinate;

//The aim of the class is to handle the graphical part of a RocCurve
class CROCCurveDraw final
{
public:
	CROCCurveDraw(GtkNotebook* notebook, size_t classIndex, CString& className);
	~CROCCurveDraw() { }
	std::vector<CCoordinate>& getCoordinateVector() { return m_coordinateList; }

	void generateCurve();

	//Callbak functions, should not be called
	void resizeEvent(GdkRectangle* rectangle);
	void exposeEnvent();

	//This function is called when the cruve should be redraw for an external reason
	void forceRedraw() { redraw(); }

private:
	size_t m_margin   = 0;
	size_t m_classIdx = 0;
	std::vector<GdkPoint> m_pointList;
	std::vector<CCoordinate> m_coordinateList;
	size_t m_pixelsPerLeftRulerLabel = 0;

	GtkWidget* m_drawableArea = nullptr;
	bool m_hasBeenInit        = false;

	//For a mytical reason, gtk says that the DrawableArea is not a DrawableArea unless it's been exposed at least once...
	// So we need to if the DrawableArea as been exposed
	bool m_hasBeenExposed = false;

	void redraw();
	void drawLeftMark(size_t w, size_t h, const char* label) const;
	void drawBottomMark(size_t w, size_t h, const char* label) const;
};
}  // namespace Evaluation
}  // namespace Plugins
}  // namespace OpenViBE


#endif // TARGET_HAS_ThirdPartyGTK
