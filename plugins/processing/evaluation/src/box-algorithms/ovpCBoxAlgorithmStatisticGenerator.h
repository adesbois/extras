#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <map>

namespace OpenViBE {
namespace Plugins {
namespace Evaluation {
typedef struct
{
	CString name;
	double min;
	double max;
	double sum;
	size_t nSample;
} signal_info_t;

/**
 * \class CBoxAlgorithmStatisticGenerator
 * \author Serrière Guillaume (Inria)
 * \date Thu Apr 30 15:24:39 2015
 * \brief The class CBoxAlgorithmStatisticGenerator describes the box Statistic generator.
 *
 */
class CBoxAlgorithmStatisticGenerator final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_StatisticGenerator)

private:
	static XML::IXMLNode* getDoubleNode(const char* nodeName, double value);

	// Input decoder:
	Toolkit::TSignalDecoder<CBoxAlgorithmStatisticGenerator> m_signalDecoder;
	Toolkit::TStimulationDecoder<CBoxAlgorithmStatisticGenerator> m_stimDecoder;

	CString m_filename;
	std::map<CIdentifier, size_t> m_stimulations;
	std::vector<signal_info_t> m_signalInfos;

	bool m_hasBeenStreamed = false;
};


/**
 * \class CBoxAlgorithmStatisticGeneratorDesc
 * \author Serrière Guillaume (Inria)
 * \date Thu Apr 30 15:24:39 2015
 * \brief Descriptor of the box Statistic generator.
 *
 */
class CBoxAlgorithmStatisticGeneratorDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("General statistics generator"); }
	CString getAuthorName() const override { return CString("Serrière Guillaume"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Generate statistics on signal."); }

	CString getDetailedDescription() const override { return CString("Generate some general purpose statistics on signal and store them in a file."); }

	CString getCategory() const override { return CString("Evaluation"); }
	CString getVersion() const override { return CString("0.1"); }
	CString getStockItemName() const override { return CString("gtk-yes"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_StatisticGenerator; }
	IPluginObject* create() override { return new CBoxAlgorithmStatisticGenerator; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Signal",OV_TypeId_Signal);
		prototype.addInput("Stimulations",OV_TypeId_Stimulations);

		prototype.addSetting("Filename for saving",OV_TypeId_Filename, "${Path_UserData}/statistics-dump.xml");

		prototype.addFlag(Kernel::BoxFlag_CanModifySetting);

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_StatisticGeneratorDesc)
};
}  // namespace Evaluation
}  // namespace Plugins
}  // namespace OpenViBE
