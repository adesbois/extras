#pragma once

#if defined(TARGET_HAS_ThirdPartyGTK)

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <gtk/gtk.h>
#include <map>
#include <vector>

#include <visualization-toolkit/ovviz_all.h>

namespace OpenViBE {
namespace Plugins {
namespace Evaluation {
class CBoxAlgorithmClassifierAccuracyMeasure final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasure)

protected:

	//codecs
	// for the TARGET
	Toolkit::TStimulationDecoder<CBoxAlgorithmClassifierAccuracyMeasure> m_targetStimDecoder;
	// For the CLASSIFIERS
	std::vector<Toolkit::TStimulationDecoder<CBoxAlgorithmClassifierAccuracyMeasure>*> m_classifierStimDecoders;


	// deduced timeline:
	std::map<uint64_t, uint64_t> m_targetsTimeLines;
	uint64_t m_currentProcessingTimeLimit = 0;


	// Outputs: visualization in a gtk window
	GtkBuilder* m_mainWidgetInterface    = nullptr;
	GtkBuilder* m_toolbarWidgetInterface = nullptr;
	GtkWidget* m_mainWidget              = nullptr;
	GtkWidget* m_toolbarWidget           = nullptr;

public:
	typedef struct
	{
		GtkLabel* labelClassifier;
		GtkProgressBar* progressBar;
		size_t score;
		size_t nStimulation;
	} progress_bar_t;

	std::vector<progress_bar_t> m_ProgressBar;
	bool m_ShowPercentages = false;
	bool m_ShowScores      = false;

private:
	VisualizationToolkit::IVisualizationContext* m_visualizationCtx = nullptr;
};

class CBoxAlgorithmClassifierAccuracyMeasureListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:
	bool onInputNameChanged(Kernel::IBox& box, const size_t index) override
	{
		if (index == 0) { box.setInputName(0, "Targets"); }	// forced
		return true;
	}

	bool onInputAdded(Kernel::IBox& box, const size_t index) override
	{
		box.setInputType(index, OV_TypeId_Stimulations); // all inputs must be stimulations
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CBoxAlgorithmClassifierAccuracyMeasureDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Classifier Accuracy Measure"); }
	CString getAuthorName() const override { return CString("Laurent Bonnet"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }

	CString getShortDescription() const override { return CString("Displays real-time classifier accuracies as vertical progress bars"); }

	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Evaluation/Classification"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-sort-ascending"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasure; }
	IPluginObject* create() override { return new CBoxAlgorithmClassifierAccuracyMeasure; }

	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmClassifierAccuracyMeasureListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool hasFunctionality(const EPluginFunctionality functionality) const override { return functionality == EPluginFunctionality::Visualization; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Targets", OV_TypeId_Stimulations);
		prototype.addInput("Classifier 1", OV_TypeId_Stimulations);

		prototype.addFlag(Kernel::BoxFlag_CanAddInput);
		prototype.addFlag(Kernel::BoxFlag_CanModifyInput);

		prototype.addInputSupport(OV_TypeId_Stimulations);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ClassifierAccuracyMeasureDesc)
};
}  // namespace Evaluation
}  // namespace Plugins
}  // namespace OpenViBE

#endif // TARGET_HAS_ThirdPartyGTK
