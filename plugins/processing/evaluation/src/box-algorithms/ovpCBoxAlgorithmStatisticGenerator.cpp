#include "ovpCBoxAlgorithmStatisticGenerator.h"

#include <sstream>

#include <xml/IXMLHandler.h>
#include <xml/IXMLNode.h>
#include <limits>
#include <iomanip>

namespace {
const char* const STATISTIC_ROOT_NODE_NAME   = "Statistic";
const char* const STIMULATION_LIST_NODE_NAME = "Stimulations-list";
const char* const STIMULATION_NODE_NAME      = "Stimulation";
const char* const IDENTIFIER_CODE_NODE_NAME  = "Identifier";
const char* const IDENTIFIER_LABEL_NODE_NAME = "Label";
const char* const AMOUNT_NODE_NAME           = "Count";

const char* const CHANNEL_LIST_NODE_NAME  = "Channel-list";
const char* const CHANNEL_NODE_NAME       = "Channel";
const char* const CHANNEL_LABEL_NODE_NAME = "Name";
const char* const CHANNEL_MIN_NODE_NAME   = "Minimum";
const char* const CHANNEL_MAX_NODE_NAME   = "Maximum";
const char* const CHANNEL_MEAN_NODE_NAME  = "Mean";
}  // namespace


using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Evaluation;

bool CBoxAlgorithmStatisticGenerator::initialize()
{
	m_signalDecoder.initialize(*this, 0);
	m_stimDecoder.initialize(*this, 1);

	m_stimulations.clear();
	m_hasBeenStreamed = false;
	m_filename        = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);

	if (m_filename == CString(""))
	{
		this->getLogManager() << LogLevel_Error << "The filename is empty\n";
		return false;
	}
	return true;
}

bool CBoxAlgorithmStatisticGenerator::uninitialize()
{
	bool res = true;

	m_signalDecoder.uninitialize();
	m_stimDecoder.uninitialize();

	if (m_hasBeenStreamed)
	{
		XML::IXMLNode* rootNode = XML::createNode(STATISTIC_ROOT_NODE_NAME);
		XML::IXMLNode* stimNode = XML::createNode(STIMULATION_LIST_NODE_NAME);
		for (const auto& s : m_stimulations)
		{
			XML::IXMLNode* node       = XML::createNode(STIMULATION_NODE_NAME);
			XML::IXMLNode* idNode     = XML::createNode(IDENTIFIER_CODE_NODE_NAME);
			XML::IXMLNode* labelNode  = XML::createNode(IDENTIFIER_LABEL_NODE_NAME);
			XML::IXMLNode* amountNode = XML::createNode(AMOUNT_NODE_NAME);

			CIdentifier id = s.first;
			std::stringstream ss;
			ss << std::fixed << std::setprecision(10) << m_stimulations[id];

			idNode->setPCData(id.toString());
			labelNode->setPCData(this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, id.toUInteger()).toASCIIString());
			amountNode->setPCData(ss.str().c_str());

			node->addChild(idNode);
			node->addChild(labelNode);
			node->addChild(amountNode);
			stimNode->addChild(node);
		}
		rootNode->addChild(stimNode);


		XML::IXMLNode* channelsNode = XML::createNode(CHANNEL_LIST_NODE_NAME);
		for (size_t i = 0; i < m_signalInfos.size(); ++i)
		{
			signal_info_t& signalInfo = m_signalInfos[i];
			XML::IXMLNode* node       = XML::createNode(CHANNEL_NODE_NAME);
			XML::IXMLNode* nodeName   = XML::createNode(CHANNEL_LABEL_NODE_NAME);

			nodeName->setPCData(signalInfo.name.toASCIIString());
			node->addChild(nodeName);
			node->addChild(getDoubleNode(CHANNEL_MAX_NODE_NAME, signalInfo.max));
			node->addChild(getDoubleNode(CHANNEL_MIN_NODE_NAME, signalInfo.min));
			node->addChild(getDoubleNode(CHANNEL_MEAN_NODE_NAME, signalInfo.sum / signalInfo.nSample));

			channelsNode->addChild(node);
		}
		rootNode->addChild(channelsNode);

		XML::IXMLHandler* handler = XML::createXMLHandler();
		if (!handler->writeXMLInFile(*rootNode, m_filename.toASCIIString())) { res = false; }

		handler->release();
		rootNode->release();
	}
	return res;
}


bool CBoxAlgorithmStatisticGenerator::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}


bool CBoxAlgorithmStatisticGenerator::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_signalDecoder.decode(i);
		if (m_signalDecoder.isHeaderReceived())
		{
			const size_t mountChannel = m_signalDecoder.getOutputMatrix()->getDimensionSize(0);
			m_hasBeenStreamed         = true;
			for (size_t j = 0; j < mountChannel; ++j)
			{
				signal_info_t info = {
					m_signalDecoder.getOutputMatrix()->getDimensionLabel(0, j), std::numeric_limits<double>::max(), -std::numeric_limits<double>::max(), 0, 0
				};
				m_signalInfos.push_back(info);
			}
		}
		if (m_signalDecoder.isBufferReceived())
		{
			const size_t nSample = m_signalDecoder.getOutputMatrix()->getDimensionSize(1);
			double* buffer       = m_signalDecoder.getOutputMatrix()->getBuffer();
			for (size_t j = 0; j < m_signalInfos.size(); ++j)
			{
				signal_info_t& info = m_signalInfos[j];
				for (size_t k = 0; k < nSample; ++k)
				{
					const double sample = buffer[j * nSample + k];
					info.sum += sample;

					if (sample < info.min) { info.min = sample; }
					if (sample > info.max) { info.max = sample; }
				}
				info.nSample += nSample;
			}
		}
	}

	for (size_t i = 0; i < boxContext.getInputChunkCount(1); ++i)
	{
		m_stimDecoder.decode(i);
		if (m_stimDecoder.isHeaderReceived()) { m_hasBeenStreamed = true; }
		if (m_stimDecoder.isBufferReceived())
		{
			IStimulationSet& stimSet = *(m_stimDecoder.getOutputStimulationSet());
			for (size_t j = 0; j < stimSet.getStimulationCount(); ++j) { m_stimulations[stimSet.getStimulationIdentifier(j)]++; }
		}
	}

	return true;
}

XML::IXMLNode* CBoxAlgorithmStatisticGenerator::getDoubleNode(const char* const nodeName, const double value)
{
	XML::IXMLNode* tmp = XML::createNode(nodeName);
	std::stringstream ss;
	ss << std::fixed << std::setprecision(10) << value;
	tmp->setPCData(ss.str().c_str());
	return tmp;
}
