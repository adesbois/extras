#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <map>

namespace OpenViBE {
namespace Plugins {
namespace Evaluation {
class CAlgorithmConfusionMatrix final : virtual public Toolkit::TAlgorithm<IAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TAlgorithm<IAlgorithm>, OVP_ClassId_Algorithm_ConfusionMatrix)

protected:

	Kernel::TParameterHandler<bool> ip_usePercentages;
	Kernel::TParameterHandler<bool> ip_useSums;

	// input TARGET
	Kernel::TParameterHandler<IStimulationSet*> ip_targetStimSet;
	// deduced timeline:
	std::map<uint64_t, uint64_t> m_targetsTimeLines;

	// input CLASSIFIER
	Kernel::TParameterHandler<IStimulationSet*> ip_classifierStimSet;

	//CONFUSION MATRIX computing
	Kernel::TParameterHandler<IStimulationSet*> ip_classesCodes;
	Kernel::TParameterHandler<IMatrix*> op_confusionMatrix;

	CMatrix m_confusionMatrix; // the values, not percentage
	std::map<uint64_t, size_t> m_nClassificationAttemptPerClass;

private:

	bool isClass(const uint64_t id) const;
	size_t getClassIndex(const uint64_t id) const;
};

class CAlgorithmConfusionMatrixDesc final : virtual public IAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Confusion Matrix Algorithm"); }
	CString getAuthorName() const override { return CString("Laurent Bonnet"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }

	CString getShortDescription() const override { return CString("Make a confusion matrix out of classification results coming from one classifier."); }

	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Classification"); }
	CString getVersion() const override { return CString("1.0"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_ConfusionMatrix; }
	IPluginObject* create() override { return new CAlgorithmConfusionMatrix; }

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		prototype.addInputParameter(OVP_Algorithm_ConfusionMatrixAlgorithm_InputParameterId_TargetStimulationSet, "Targets",
									Kernel::ParameterType_StimulationSet);
		prototype.addInputParameter(OVP_Algorithm_ConfusionMatrixAlgorithm_InputParameterId_ClassifierStimulationSet, "Classification results",
									Kernel::ParameterType_StimulationSet);
		prototype.addInputParameter(OVP_Algorithm_ConfusionMatrixAlgorithm_InputParameterId_ClassCodes, "Class codes", Kernel::ParameterType_StimulationSet);
		prototype.addInputParameter(OVP_Algorithm_ConfusionMatrixAlgorithm_InputParameterId_Percentage, "Percentage", Kernel::ParameterType_Boolean);
		prototype.addInputParameter(OVP_Algorithm_ConfusionMatrixAlgorithm_InputParameterId_Sums, "Sums", Kernel::ParameterType_Boolean);

		prototype.addOutputParameter(OVP_Algorithm_ConfusionMatrixAlgorithm_OutputParameterId_ConfusionMatrix, "Confusion matrix",
									 Kernel::ParameterType_Matrix);

		prototype.addInputTrigger(OVP_Algorithm_ConfusionMatrixAlgorithm_InputTriggerId_ResetTarget, "Reset Target");
		prototype.addInputTrigger(OVP_Algorithm_ConfusionMatrixAlgorithm_InputTriggerId_ResetClassifier, "Reset Classifier");
		prototype.addInputTrigger(OVP_Algorithm_ConfusionMatrixAlgorithm_InputTriggerId_FeedTarget, "Feed Target");
		prototype.addInputTrigger(OVP_Algorithm_ConfusionMatrixAlgorithm_InputTriggerId_FeedClassifier, "Feed Classifier");

		prototype.addOutputTrigger(OVP_Algorithm_ConfusionMatrixAlgorithm_OutputTriggerId_ConfusionPerformed, "Confusion computing performed");

		return true;
	}

	_IsDerivedFromClass_Final_(IAlgorithmDesc, OVP_ClassId_Algorithm_ConfusionMatrixDesc)
};
}  // namespace Evaluation
}  // namespace Plugins
}  // namespace OpenViBE
