#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		cout << "Usage: test_evaluation <filename>\n";
		return 3;
	}

	ifstream file(argv[1], ios::in);

	if (file.good() && !file.bad() && file.is_open()) // ...
	{
		string line;
		while (getline(file, line))
		{
			if (line.find("Final value of Kappa") != string::npos)
			{
				cout << "Found kappa line " << line << endl;

				const size_t pos     = line.rfind(' ');
				const string cutline = line.substr(pos);

				stringstream kappa(cutline);

				double coefficient;
				kappa >> coefficient;

				if (coefficient != 0.840677)
				{
					cout << "Wrong Kappa coefficient. Found " << coefficient << " instead of 0.840677" << endl;
					return 1;
				}
				cout << "Test ok" << endl;
				return 0;
			}
		}
	}
	cout << "Error: Problem opening [" << argv[1] << "]\n";

	return 2;
}
