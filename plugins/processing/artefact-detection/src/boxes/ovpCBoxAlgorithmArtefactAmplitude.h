///-------------------------------------------------------------------------------------------------
/// 
/// \file ovpCBoxAlgorithmArtefactAmplitude.h
/// \brief Class of the box computing the covariance matrix
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/08/2019.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------
#pragma once

#include "ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace Artefact {
/// <summary>	The class CBoxAlgorithmArtefactAmplitude describes the box Artefact Amplitude. </summary>
class CBoxAlgorithmArtefactAmplitude final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_ArtefactAmplitude)

protected:

	Toolkit::TSignalDecoder<CBoxAlgorithmArtefactAmplitude> m_decoder;			// Input decoder
	IMatrix* m_iMatrix = nullptr;												// Input Matrix pointer

	double m_max      = 0;
	size_t m_nSamples = 0, m_nArtefact = 0;
};

/// <summary>	Descriptor of the box Artefact Detector. </summary>
class CBoxAlgorithmArtefactAmplitudeDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Artefact Amplitude"); }
	CString getAuthorName() const override { return CString("Thibaut Monseigne"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Simple Artefact Detection"); }

	CString getDetailedDescription() const override
	{
		return CString("Check if one element is higher than Max setting.\nThe signal is returned if no element exceeds the defined value.");
	}

	CString getCategory() const override { return CString("Artefact"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-no"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ArtefactAmplitude; }
	IPluginObject* create() override { return new CBoxAlgorithmArtefactAmplitude; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Signal",OV_TypeId_Signal);
		prototype.addOutput("Non-artefact signal",OV_TypeId_Signal);
		prototype.addSetting("Max (mV)",OV_TypeId_Float, "100");
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ArtefactAmplitudeDesc)
};
}  // namespace Artefact
}  // namespace Plugins
}  // namespace OpenViBE
