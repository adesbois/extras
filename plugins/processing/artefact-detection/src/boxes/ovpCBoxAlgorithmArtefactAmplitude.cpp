#include "ovpCBoxAlgorithmArtefactAmplitude.h"
#include <cmath>	// Floor
#include <sstream>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Artefact;
using namespace std;

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmArtefactAmplitude::initialize()
{
	//***** Codecs *****
	m_decoder.initialize(*this, 0);
	m_iMatrix = m_decoder.getOutputMatrix();

	//***** Settings *****
	m_max = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);

	//***** Assert *****
	OV_ERROR_UNLESS_KRF(m_max > 0, "Invalid Maximum [" << m_max << "] (expected value > 0)\n", ErrorType::BadSetting);

	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmArtefactAmplitude::uninitialize()
{
	m_decoder.uninitialize();
	stringstream ss;
	ss << m_nArtefact << " artefacts detected in " << m_nSamples << " samples (";
	ss.precision(2);
	ss << fixed << 100.0 * double(m_nArtefact) / double(m_nSamples) << "%)" << endl;
	this->getLogManager() << LogLevel_Info << ss.str();
	return true;
}
//---------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmArtefactAmplitude::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool CBoxAlgorithmArtefactAmplitude::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();
	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		bool artefact = false;
		m_decoder.decode(i);																		// Decode chunk
		OV_ERROR_UNLESS_KRF(m_iMatrix->getDimensionCount() == 2, "Invalid Input Signal", ErrorType::BadInput);
		m_nSamples++;
		//if (m_decoder.isHeaderReceived()) {}														// Header
		if (m_decoder.isBufferReceived())															// Buffer
		{
			const size_t size     = m_iMatrix->getDimensionSize(0) * m_iMatrix->getDimensionSize(1);	// get buffer size
			const double* iBuffer = m_iMatrix->getBuffer();												// input buffer
			for (size_t idx = 0; idx < size; ++idx)
			{
				if (abs(iBuffer[idx]) >= m_max)															// Amplitude comparison
				{
					this->getLogManager() << LogLevel_Trace << "Artefact detected in channel (" << floor(idx / m_iMatrix->getDimensionSize(1)) << ")\n";
					artefact = true;
					m_nArtefact++;
					break;
				}
			}
		}
		//if (m_decoder.isEndReceived()) {}														// End
		// We don't need output codec we copy just the inpu to the output if there is no amplitude artefact
		if (!artefact)
		{
			uint64_t tStart       = 0, tEnd = 0;
			size_t size           = 0;
			const uint8_t* buffer = nullptr;
			boxContext.getInputChunk(0, i, tStart, tEnd, size, buffer);
			boxContext.appendOutputChunkData(0, buffer, size);
			boxContext.markOutputAsReadyToSend(0, tStart, tEnd);
			boxContext.markInputAsDeprecated(0, i);
		}
	}
	return true;
}
//---------------------------------------------------------------------------------------------------
