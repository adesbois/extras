///-------------------------------------------------------------------------------------------------
/// 
/// \file ovp_defines.h
/// \brief Defines list for Setting, Shortcut Macro and const.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/08/2019.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_ArtefactAmplitude							OpenViBE::CIdentifier(0x4aefaf18, 0xb68095e4)
#define OVP_ClassId_BoxAlgorithm_ArtefactAmplitudeDesc						OpenViBE::CIdentifier(0xa65f2a61, 0x83596875)
