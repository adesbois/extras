#include <openvibe/ov_all.h>
#include "ovp_defines.h"

// Boxes Includes
#include "boxes/ovpCBoxAlgorithmArtefactAmplitude.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
	// Register boxes
	OVP_Declare_New(Artefact::CBoxAlgorithmArtefactAmplitudeDesc);

OVP_Declare_End()
