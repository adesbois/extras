#pragma once

//You may have to change this path to match your folder organisation
#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

//#include <pair>
#include <vector>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/containers/string.hpp>

// The unique identifiers for the box and its descriptor.
// Identifier are randomly chosen by the skeleton-generator.

namespace OpenViBE {
namespace Plugins {
namespace FileReadingAndWriting {
/**
 * \class CBoxAlgorithmSharedMemoryWriter
 * \author Dieter Devlaminck (INRIA)
 * \date Thu Jan 17 13:34:58 2013
 * \brief The class CBoxAlgorithmSharedMemoryWriter describes the box SharedMemoryWriter.
 *
 */

struct SMatrix
{
	size_t rowDim;
	size_t colDim;
	boost::interprocess::offset_ptr<double> data;
};

typedef boost::interprocess::allocator<char, boost::interprocess::managed_shared_memory::segment_manager> CharAllocator;
typedef boost::interprocess::basic_string<char, std::char_traits<char>, CharAllocator> ShmString;
typedef boost::interprocess::allocator<ShmString, boost::interprocess::managed_shared_memory::segment_manager> StringAllocator;
//typedef vector<ShmString, StringAllocator> MyShmStringVector;		
typedef boost::interprocess::allocator<std::pair<const ShmString, CIdentifier>, boost::interprocess::managed_shared_memory::segment_manager>
ShmemAllocatorMetaInfo;
typedef boost::interprocess::map<ShmString, CIdentifier, std::less<ShmString>, ShmemAllocatorMetaInfo> MyVectorMetaInfo;

typedef boost::interprocess::allocator<size_t, boost::interprocess::managed_shared_memory::segment_manager> ShmemAllocatorStimulation;
typedef boost::interprocess::allocator<boost::interprocess::offset_ptr<SMatrix>, boost::interprocess::managed_shared_memory::segment_manager>
ShmemAllocatorMatrix;
typedef boost::interprocess::vector<size_t, ShmemAllocatorStimulation> MyVectorStimulation;
typedef boost::interprocess::vector<boost::interprocess::offset_ptr<SMatrix>, ShmemAllocatorMatrix> MyVectorStreamedMatrix;

class CBoxAlgorithmSharedMemoryWriter final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;

	bool process() override;

	// As we do with any class in openvibe, we use the macro below 
	// to associate this box to an unique identifier. 
	// The inheritance information is also made available, 
	// as we provide the superclass Toolkit::TBoxAlgorithm < IBoxAlgorithm >
	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_SharedMemoryWriter)

protected:
	std::vector<Toolkit::TDecoder<CBoxAlgorithmSharedMemoryWriter>*> m_decoders;
	CString m_sharedMemoryName;
	boost::interprocess::managed_shared_memory m_sharedMemoryArray;

private:
	CIdentifier m_typeID = OV_UndefinedIdentifier;
	CString m_mutexName;
	boost::interprocess::named_mutex* m_mutex = nullptr;
	std::vector<MyVectorStimulation*> m_stimSets;
	std::vector<MyVectorStreamedMatrix*> m_streamedMatrices;
};


// The box listener can be used to call specific callbacks whenever the box structure changes : input added, name changed, etc.
// Please uncomment below the callbacks you want to use.
class CBoxAlgorithmSharedMemoryWriterListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	bool onInputAdded(Kernel::IBox& /*box*/, const size_t /*index*/) override { return true; }
	bool onInputRemoved(Kernel::IBox& /*box*/, const size_t /*index*/) override { return true; }
	bool onInputTypeChanged(Kernel::IBox& /*box*/, const size_t /*index*/) override { return true; }

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};


/**
 * \class CBoxAlgorithmSharedMemoryWriterDesc
 * \author Dieter Devlaminck (INRIA)
 * \date Thu Jan 17 13:34:58 2013
 * \brief Descriptor of the box SharedMemoryWriter.
 *
 */
class CBoxAlgorithmSharedMemoryWriterDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("SharedMemoryWriter"); }
	CString getAuthorName() const override { return CString("Dieter Devlaminck"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Stream input to shared memory"); }

	CString getDetailedDescription() const override
	{
		return CString(
			"The box writes input to shared memory so that it can be read by another process. Stimuli and streamed matrices are supported, and transformed into a format that can be written into shared memory. Based on the input types, a metainfo variable will be created in shared memory that will specify which variables have which type. This way the client can know what it will be reading.");
	}

	CString getCategory() const override { return CString("File reading and writing"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString(""); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_SharedMemoryWriter; }
	IPluginObject* create() override { return new CBoxAlgorithmSharedMemoryWriter; }


	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmSharedMemoryWriterListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("prediction1",OV_TypeId_StreamedMatrix);
		//prototype.addInput("prediction2",OV_TypeId_Stimulations);


		prototype.addFlag(Kernel::BoxFlag_CanModifyInput);
		prototype.addFlag(Kernel::BoxFlag_CanAddInput);
		//prototype.addFlag(Kernel::BoxFlag_CanRemoveInput);

		prototype.addSetting("SharedMemoryName",OV_TypeId_String, "SharedMemory_P300Stimulator");

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		prototype.addInputSupport(OV_TypeId_StreamedMatrix);
		prototype.addInputSupport(OV_TypeId_Stimulations);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_SharedMemoryWriterDesc)
};
}  // namespace FileReadingAndWriting
}  // namespace Plugins
}  // namespace OpenViBE
