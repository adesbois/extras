#pragma once

#ifdef TARGET_HAS_Boost

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <ctime>
#include <boost/asio.hpp>

enum { TCPWRITER_RAW, TCPWRITER_HEX, TCPWRITER_STRING }; // stimulation output types

namespace OpenViBE {
namespace Plugins {
namespace NetworkIO {
/**
 * \class CBoxAlgorithmTCPWriter
 * \author Jussi T. Lindgren (Inria)
 * \date Wed Sep 11 12:55:22 2013
 * \brief The class CBoxAlgorithmTCPWriter describes the box TCP Writer.
 *
 */
class CBoxAlgorithmTCPWriter final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	//Here is the different process callbacks possible
	// - On clock ticks :
	//virtual bool processClock(Kernel::CMessageClock& msg);
	// - On new input received (the most common behaviour for signal processing) :
	bool processInput(const size_t index) override;

	// If you want to use processClock, you must provide the clock frequency.
	//virtual uint64_t getClockFrequency();

	bool process() override;

	// As we do with any class in openvibe, we use the macro below 
	// to associate this box to an unique identifier. 
	// The inheritance information is also made available, 
	// as we provide the superclass Toolkit::TBoxAlgorithm < IBoxAlgorithm >
	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_TCPWriter)

protected:

	bool sendToClients(const void* buffer, size_t size);

	// Stream decoder
	Toolkit::TStimulationDecoder<CBoxAlgorithmTCPWriter> m_stimulationDecoder;
	Toolkit::TStreamedMatrixDecoder<CBoxAlgorithmTCPWriter> m_matrixDecoder;
	Toolkit::TSignalDecoder<CBoxAlgorithmTCPWriter> m_signalDecoder;
	Toolkit::TDecoder<CBoxAlgorithmTCPWriter>* m_activeDecoder = nullptr;

	boost::asio::io_service m_ioService;
	boost::asio::ip::tcp::acceptor* m_acceptor = nullptr;
	std::vector<boost::asio::ip::tcp::socket*> m_sockets;

	uint64_t m_outputStyle = 0;

	CIdentifier m_inputType = OV_UndefinedIdentifier;

	// Data written as global output header, 8*4 = 32 bytes. Padding allows dumb readers to step with double (==8 bytes).
	size_t m_rawVersion       = 0;					// in network byte order, version of the raw stream
	size_t m_endianness       = 0;					// in network byte order, 0==unknown, 1==little, 2==big, 3==pdp
	size_t m_frequency        = 0;					// this and the rest are in host byte order
	size_t m_nChannels        = 0;
	size_t m_nSamplesPerChunk = 0;
	size_t m_reserved0        = 0;
	size_t m_reserved1        = 0;
	size_t m_reserved2        = 0;

	void startAccept();
	void handleAccept(const boost::system::error_code& ec, boost::asio::ip::tcp::socket* pSocket);
};

class CBoxAlgorithmTCPWriterListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:
	CBoxAlgorithmTCPWriterListener(): m_lastType(OV_UndefinedIdentifier) { }

	bool onInputTypeChanged(Kernel::IBox& box, const size_t index) override
	{
		CIdentifier newType = OV_UndefinedIdentifier;
		box.getInputType(index, newType);
		// Set the right enumeration according to the type if we actualy change it
		// TODO find a way to init m_lastType with the right value
		if (m_lastType != newType)
		{
			if (newType != OV_TypeId_Stimulations) { box.setSettingType(1, OVP_TypeID_TCPWriter_RawOutputStyle); }
			else { box.setSettingType(1, OVP_TypeID_TCPWriter_OutputStyle); }
			box.setSettingValue(1, "Raw");
			m_lastType = newType;
		}
		return true;
	}

private:
	CIdentifier m_lastType = OV_UndefinedIdentifier;

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

/**
 * \class CBoxAlgorithmTCPWriterDesc
 * \author Jussi T. Lindgren (Inria)
 * \date Wed Sep 11 12:55:22 2013
 * \brief Descriptor of the box TCP Writer.
 *
 */
class CBoxAlgorithmTCPWriterDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("TCP Writer"); }
	CString getAuthorName() const override { return CString("Jussi T. Lindgren"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Send input stream out via a TCP socket"); }
	CString getDetailedDescription() const override { return CString("\n"); }
	CString getCategory() const override { return CString("Acquisition and network IO"); }
	CString getVersion() const override { return CString("0.2"); }
	CString getStockItemName() const override { return CString("gtk-connect"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_TCPWriter; }
	IPluginObject* create() override { return new CBoxAlgorithmTCPWriter; }

	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmTCPWriterListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input 1",OV_TypeId_StreamedMatrix);

		prototype.addSetting("Port",OV_TypeId_Integer, "5678");
		prototype.addSetting("Output format", OVP_TypeID_TCPWriter_RawOutputStyle, "Raw");

		prototype.addFlag(Kernel::BoxFlag_CanModifyInput);

		prototype.addInputSupport(OV_TypeId_StreamedMatrix);
		prototype.addInputSupport(OV_TypeId_Signal);
		prototype.addInputSupport(OV_TypeId_Stimulations);

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_TCPWriterDesc)
};
}  // namespace NetworkIO
}  // namespace Plugins
}  // namespace OpenViBE


#endif // TARGET_HAS_Boost
