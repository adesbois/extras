#ifdef TARGET_HAS_ThirdPartyLSL

/*
 * Notes: This code should be kept compatible with changes to LSL Input Driver and Output Plugin in OpenViBE Acquisition Server.
 *
 */
#include "ovpCBoxAlgorithmLSLExport.h"

#include <ctime>
#include <iostream>


using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace NetworkIO;

bool CBoxAlgorithmLSLExport::initialize()
{
	m_signalOutlet   = nullptr;
	m_stimulusOutlet = nullptr;

	m_signalDecoder.initialize(*this, 0);
	m_stimDecoder.initialize(*this, 1);

	m_signalName = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_markerName = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	// These are supposed to be unique, so we don't have them in the box config
	m_signalID = CIdentifier::random().toString();
	m_markerID = CIdentifier::random().toString();

	while (m_markerID == m_signalID) { m_markerID = CIdentifier::random().toString(); } // very unlikely

	this->getLogManager() << LogLevel_Trace << "Will create streams [" << m_signalName << ", id " << m_signalID
			<< "] and [" << m_markerName << ", id " << m_markerID << "]\n";

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmLSLExport::uninitialize()
{
	m_signalDecoder.uninitialize();
	m_stimDecoder.uninitialize();

	if (m_signalOutlet)
	{
		delete m_signalOutlet;
		m_signalOutlet = nullptr;
	}
	if (m_stimulusOutlet)
	{
		delete m_stimulusOutlet;
		m_stimulusOutlet = nullptr;
	}

	m_buffer.clear();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmLSLExport::processInput(const size_t /*index*/)
{
	// ready to process !
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmLSLExport::process()
{
	// the dynamic box context describes the current state of the box inputs and outputs (i.e. the chunks)
	IBoxIO& boxContext = this->getDynamicBoxContext();

	// Process signals
	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_signalDecoder.decode(i);
		if (m_signalDecoder.isHeaderReceived() && !m_signalOutlet)
		{
			const size_t nChannel        = m_signalDecoder.getOutputMatrix()->getDimensionSize(0);
			const size_t samplesPerBlock = m_signalDecoder.getOutputMatrix()->getDimensionSize(1);
			const size_t frequency       = m_signalDecoder.getOutputSamplingRate();

			m_buffer.resize(nChannel);

			// Open a signal stream 
			lsl::stream_info signalInfo(m_signalName.toASCIIString(), "signal", nChannel, frequency, lsl::cf_float32, m_signalID.toASCIIString());

			lsl::xml_element channels = signalInfo.desc().append_child("channels");
			//m_signalDecoder.getOutputMatrix()->getDimensionLabel(0, 1);

			for (size_t c = 0; c < nChannel; ++c)
			{
				const char* name = m_signalDecoder.getOutputMatrix()->getDimensionLabel(0, c);
				channels.append_child("channel").append_child_value("label", name).append_child_value("unit", "unknown").append_child_value("type", "signal");
			}

#ifdef DEBUG
			lsl::xml_element debug = signalInfo.desc().child("channels");
			if(debug.child("channel").child_value("label")) { std::cout << "channel label " << debug.child("channel").child_value("label") << "\n"; }
#endif

			// make a new outlet
			try { m_signalOutlet = new lsl::stream_outlet(signalInfo, samplesPerBlock); }
			catch (...)
			{
				this->getLogManager() << "Unable to create signal outlet\n";
				return false;
			}
		}
		if (m_signalDecoder.isBufferReceived())
		{
			if (m_signalOutlet->have_consumers())
			{
				const IMatrix* matrix        = m_signalDecoder.getOutputMatrix();
				const size_t nChannel        = matrix->getDimensionSize(0);
				const size_t samplesPerBlock = matrix->getDimensionSize(1);
				const double* iBuffer        = matrix->getBuffer();

				const uint64_t startTime = boxContext.getInputChunkStartTime(0, i);
				const uint64_t endTime   = boxContext.getInputChunkEndTime(0, i);

				// note: the step computed below should be exactly the same as could be obtained from the sampling rate
				const double start = CTime(startTime).toSeconds();
				const double step  = CTime(endTime - startTime).toSeconds() / double(samplesPerBlock);

				for (size_t s = 0; s < samplesPerBlock; ++s)
				{
					for (size_t c = 0; c < nChannel; ++c) { m_buffer[c] = float(iBuffer[c * samplesPerBlock + s]); }
					m_signalOutlet->push_sample(m_buffer, start + s * step);
				}
			}
		}
		if (m_signalDecoder.isEndReceived())
		{
			if (m_signalOutlet)
			{
				delete m_signalOutlet;
				m_signalOutlet = nullptr;
			}
		}
	}

	// Process stimuli -> LSL markers. 
	// Note that stimuli with identifiers not fitting to int will be mangled by a static cast.
	for (size_t i = 0; i < boxContext.getInputChunkCount(1); ++i)
	{
		m_stimDecoder.decode(i);
		if (m_stimDecoder.isHeaderReceived() && !m_stimulusOutlet)
		{
			// Open a stimulus stream
			lsl::stream_info info(m_markerName.toASCIIString(), "Markers", 1, lsl::IRREGULAR_RATE, lsl::cf_int32, m_markerID.toASCIIString());

			info.desc().append_child("channels").append_child("channel").append_child_value("label", "Stimulations").append_child_value("type", "marker");

			try { m_stimulusOutlet = new lsl::stream_outlet(info); }
			catch (...)
			{
				this->getLogManager() << "Unable to create marker outlet\n";
				return false;
			}
		}
		if (m_stimDecoder.isBufferReceived())
		{
			// Output stimuli
			if (m_stimulusOutlet->have_consumers())
			{
				const IStimulationSet* stimSet = m_stimDecoder.getOutputStimulationSet();

				for (size_t s = 0; s < stimSet->getStimulationCount(); ++s)
				{
					const int code    = int(stimSet->getStimulationIdentifier(s));
					const double date = CTime(stimSet->getStimulationDate(s)).toSeconds();

					m_stimulusOutlet->push_sample(&code, date);
				}
			}
		}
		if (m_stimDecoder.isEndReceived())
		{
			if (m_stimulusOutlet)
			{
				delete m_stimulusOutlet;
				m_stimulusOutlet = nullptr;
			}
		}
	}
	return true;
}

#endif
