#pragma once

#ifdef TARGET_HAS_ThirdPartyLSL

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <lsl_cpp.h>


#include <ctime>
#include <iostream>

namespace OpenViBE {
namespace Plugins {
namespace NetworkIO {
/**
 * \class CBoxAlgorithmLSLExport
 * \author Jussi T. Lindgren (Inria)
 * \date Fri Jan 30 09:55:22 2015
 * \brief The class CBoxAlgorithmLSLExport describes the box LSL Export.
 *
 */
class CBoxAlgorithmLSLExport final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;

	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_LSLExport)

protected:

	// Decoders
	Toolkit::TStimulationDecoder<CBoxAlgorithmLSLExport> m_stimDecoder;
	Toolkit::TSignalDecoder<CBoxAlgorithmLSLExport> m_signalDecoder;

	lsl::stream_outlet* m_signalOutlet   = nullptr;
	lsl::stream_outlet* m_stimulusOutlet = nullptr;

	std::vector<float> m_buffer;

	CString m_signalName;
	CString m_signalID;
	CString m_markerName;
	CString m_markerID;
};

class CBoxAlgorithmLSLExportListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

/**
 * \class CBoxAlgorithmLSLExportDesc
 * \author Jussi T. Lindgren (Inria)
 * \date Fri Jan 30 09:55:22 2015
 * \brief Descriptor of the box LSL Export.
 *
 */
class CBoxAlgorithmLSLExportDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("LSL Export"); }
	CString getAuthorName() const override { return CString("Jussi T. Lindgren"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Send input stream out via LabStreamingLayer (LSL)"); }
	CString getDetailedDescription() const override { return CString("\n"); }
	CString getCategory() const override { return CString("Acquisition and network IO"); }
	CString getVersion() const override { return CString("0.1"); }
	CString getStockItemName() const override { return CString("gtk-connect"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_LSLExport; }
	IPluginObject* create() override { return new CBoxAlgorithmLSLExport; }

	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmLSLExportListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input signal", OV_TypeId_Signal);
		prototype.addInput("Input stimulations", OV_TypeId_Stimulations);

		prototype.addSetting("Signal stream", OV_TypeId_String, "openvibeSignal");
		prototype.addSetting("Marker stream", OV_TypeId_String, "openvibeMarkers");

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_LSLExportDesc)
};
}  // namespace NetworkIO
}  // namespace Plugins
}  // namespace OpenViBE


#endif // TARGET_HAS_Boost
