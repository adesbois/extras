#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_SharedMemoryWriter			OpenViBE::CIdentifier(0xACC272DD, 0xC1BDC1B1)
#define OVP_ClassId_BoxAlgorithm_SharedMemoryWriterDesc 	OpenViBE::CIdentifier(0xACC727DD, 0xCACDC1B1)
#define OVP_ClassId_BoxAlgorithm_LSLExport					OpenViBE::CIdentifier(0x6F3467FF, 0x52794DA6)
#define OVP_ClassId_BoxAlgorithm_LSLExportDesc				OpenViBE::CIdentifier(0x40C03C3F, 0x034A19C2)
#define OVP_ClassId_BoxAlgorithm_TCPWriter					OpenViBE::CIdentifier(0x02F24947, 0x17FA0477)
#define OVP_ClassId_BoxAlgorithm_TCPWriterDesc				OpenViBE::CIdentifier(0x3C32489D, 0x46F565D3)
#define OVP_TypeID_TCPWriter_OutputStyle					OpenViBE::CIdentifier(0x6D7E53DD, 0x6A0A4753)
#define OVP_TypeID_TCPWriter_RawOutputStyle					OpenViBE::CIdentifier(0x77D3E238, 0xB954EC48)


// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines


#define OV_AttributeId_Box_FlagIsUnstable					OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)
