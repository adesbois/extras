#include "ovp_defines.h"

#include "box-algorithms/ovpCBoxAlgorithmLSLExport.h"
#include "box-algorithms/ovpCBoxAlgorithmTCPWriter.h"
#include "box-algorithms/ovpCBoxAlgorithmSharedMemoryWriter.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
#ifdef TARGET_HAS_ThirdPartyLSL
	OVP_Declare_New(NetworkIO::CBoxAlgorithmLSLExportDesc);
#endif

#ifdef TARGET_HAS_Boost
	OVP_Declare_New(FileReadingAndWriting::CBoxAlgorithmSharedMemoryWriterDesc);
	OVP_Declare_New(NetworkIO::CBoxAlgorithmTCPWriterDesc);

	context.getTypeManager().registerEnumerationEntry(OV_TypeId_BoxAlgorithmFlag, OV_AttributeId_Box_FlagIsUnstable.toString(),
													  OV_AttributeId_Box_FlagIsUnstable.toUInteger());

	context.getTypeManager().registerEnumerationType(OVP_TypeID_TCPWriter_OutputStyle, "Stimulus output");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeID_TCPWriter_OutputStyle, "Raw", TCPWRITER_RAW);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeID_TCPWriter_OutputStyle, "Hex", TCPWRITER_HEX);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeID_TCPWriter_OutputStyle, "String", TCPWRITER_STRING);

	context.getTypeManager().registerEnumerationType(OVP_TypeID_TCPWriter_RawOutputStyle, "Raw output");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeID_TCPWriter_RawOutputStyle, "Raw", TCPWRITER_RAW);
#endif

OVP_Declare_End()
