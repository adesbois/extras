#include "ovp_defines.h"

#include "algorithms/ovpCAlgorithmAddition.h"
#include "box-algorithms/ovpCBoxAlgorithmAdditionTest.h"

#include "box-algorithms/ovpCCrashingBox.h"

#include "box-algorithms/ovpCTestCodecToolkit.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
	// OVP_Declare_New(Samples::CAlgorithmAdditionDesc);
	// OVP_Declare_New(Samples::CBoxAlgorithmAdditionTestDesc);
	// OVP_Declare_New(Samples::CCrashingBoxDesc);	

	OVP_Declare_New(Tests::CTestCodecToolkitDesc);

OVP_Declare_End()
