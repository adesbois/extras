#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_AlgorithmAddition					OpenViBE::CIdentifier(0x75FCE50E, 0x8302FA91)
#define OVP_ClassId_AlgorithmAdditionDesc				OpenViBE::CIdentifier(0x842E0B85, 0xA59FABC1)
#define OVP_ClassId_BoxAlgorithmAdditionTest			OpenViBE::CIdentifier(0x534EB140, 0x15F41496)
#define OVP_ClassId_BoxAlgorithmAdditionTestDesc		OpenViBE::CIdentifier(0xB33EC315, 0xF63BC0C5)
#define OVP_ClassId_CrashingBox							OpenViBE::CIdentifier(0x00DAFD60, 0x39A58819)
#define OVP_ClassId_CrashingBoxDesc						OpenViBE::CIdentifier(0x009F54B9, 0x2B6A4922)
#define OVP_ClassId_TestCodecToolkit					OpenViBE::CIdentifier(0x330E3A87, 0x31565BA6)
#define OVP_ClassId_TestCodecToolkitDesc				OpenViBE::CIdentifier(0x376A4712, 0x1AA65567)

// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
