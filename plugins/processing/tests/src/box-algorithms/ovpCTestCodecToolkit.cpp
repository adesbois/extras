#include "ovpCTestCodecToolkit.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Tests;

using namespace /*OpenViBE::*/Toolkit;

bool CTestCodecToolkit::initialize()
{
	// You can also manipulate pointers to Codec object. Creation and destruction must be done like that :	
	TStreamedMatrixDecoder<CTestCodecToolkit>* matrixDecoder = new TStreamedMatrixDecoder<CTestCodecToolkit>(*this, 0);
	delete matrixDecoder;
	TStreamedMatrixEncoder<CTestCodecToolkit>* matrixEncoder = new TStreamedMatrixEncoder<CTestCodecToolkit>(*this, 0);
	delete matrixEncoder;

	TChannelLocalisationDecoder<CTestCodecToolkit>* channelLocalisationDecoder = new TChannelLocalisationDecoder<CTestCodecToolkit>(*this, 1);
	delete channelLocalisationDecoder;
	TChannelLocalisationEncoder<CTestCodecToolkit>* channelLocalisationEncoder = new TChannelLocalisationEncoder<CTestCodecToolkit>(*this, 1);
	delete channelLocalisationEncoder;

	TFeatureVectorDecoder<CTestCodecToolkit>* vectorDecoder = new TFeatureVectorDecoder<CTestCodecToolkit>(*this, 2);
	delete vectorDecoder;
	TFeatureVectorEncoder<CTestCodecToolkit>* vectorEncoder = new TFeatureVectorEncoder<CTestCodecToolkit>(*this, 2);
	delete vectorEncoder;

	TSpectrumDecoder<CTestCodecToolkit>* spectrumDecoder = new TSpectrumDecoder<CTestCodecToolkit>(*this, 3);
	delete spectrumDecoder;
	TSpectrumEncoder<CTestCodecToolkit>* spectrumEncoder = new TSpectrumEncoder<CTestCodecToolkit>(*this, 3);
	delete spectrumEncoder;

	TSignalDecoder<CTestCodecToolkit>* signalDecoder = new TSignalDecoder<CTestCodecToolkit>(*this, 4);
	delete signalDecoder;
	TSignalEncoder<CTestCodecToolkit>* signalEncoder = new TSignalEncoder<CTestCodecToolkit>(*this, 4);
	delete signalEncoder;

	TStimulationDecoder<CTestCodecToolkit>* stimulationDecoder = new TStimulationDecoder<CTestCodecToolkit>(*this, 5);
	delete stimulationDecoder;
	TStimulationEncoder<CTestCodecToolkit>* stimulationEncoder = new TStimulationEncoder<CTestCodecToolkit>(*this, 5);
	delete stimulationEncoder;

	TExperimentInfoDecoder<CTestCodecToolkit>* experimentInfoDecoder = new TExperimentInfoDecoder<CTestCodecToolkit>(*this, 6);
	delete experimentInfoDecoder;
	TExperimentInfoEncoder<CTestCodecToolkit>* experimentInfoEncoder = new TExperimentInfoEncoder<CTestCodecToolkit>(*this, 6);
	delete experimentInfoEncoder;

	//-----------------------------------------------------------------------------------------

	m_matrixDecoder.initialize(*this, 0);
	m_streamedMatrixEncoder.initialize(*this, 0);
	m_streamedMatrixEncoder.getInputMatrix().setReferenceTarget(m_matrixDecoder.getOutputMatrix());
	m_decoders.push_back(&m_matrixDecoder);
	m_encoders.push_back(&m_streamedMatrixEncoder);

	m_channelLocalisationDecoder.initialize(*this, 1);
	m_channelLocalisationEncoder.initialize(*this, 1);
	m_channelLocalisationEncoder.getInputMatrix().setReferenceTarget(m_channelLocalisationDecoder.getOutputMatrix());
	m_channelLocalisationEncoder.getInputDynamic().setReferenceTarget(m_channelLocalisationDecoder.getOutputDynamic());
	m_decoders.push_back(&m_channelLocalisationDecoder);
	m_encoders.push_back(&m_channelLocalisationEncoder);

	m_featureVectorDecoder.initialize(*this, 2);
	m_featureVectorEncoder.initialize(*this, 2);
	m_featureVectorEncoder.getInputMatrix().setReferenceTarget(m_featureVectorDecoder.getOutputMatrix());
	m_decoders.push_back(&m_featureVectorDecoder);
	m_encoders.push_back(&m_featureVectorEncoder);

	m_spectrumDecoder.initialize(*this, 3);
	m_spectrumEncoder.initialize(*this, 3);
	m_spectrumEncoder.getInputMatrix().setReferenceTarget(m_spectrumDecoder.getOutputMatrix());
	m_spectrumEncoder.getInputFrequencyAbscissa().setReferenceTarget(m_spectrumDecoder.getOutputFrequencyAbscissa());
	m_spectrumEncoder.getInputSamplingRate().setReferenceTarget(m_spectrumDecoder.getOutputSamplingRate());
	m_decoders.push_back(&m_spectrumDecoder);
	m_encoders.push_back(&m_spectrumEncoder);

	m_signalDecoder.initialize(*this, 4);
	m_signalEncoder.initialize(*this, 4);
	m_signalEncoder.getInputMatrix().setReferenceTarget(m_signalDecoder.getOutputMatrix());
	m_signalEncoder.getInputSamplingRate().setReferenceTarget(m_signalDecoder.getOutputSamplingRate());
	m_decoders.push_back(&m_signalDecoder);
	m_encoders.push_back(&m_signalEncoder);

	m_stimDecoder.initialize(*this, 5);
	m_stimEncoder.initialize(*this, 5);
	m_stimEncoder.getInputStimulationSet().setReferenceTarget(m_stimDecoder.getOutputStimulationSet());
	m_decoders.push_back(&m_stimDecoder);
	m_encoders.push_back(&m_stimEncoder);

	m_experimentInfoDecoder.initialize(*this, 6);
	m_experimentInfoEncoder.initialize(*this, 6);
	m_experimentInfoEncoder.getInputExperimentID().setReferenceTarget(m_experimentInfoDecoder.getOutputExperimentID());
	m_experimentInfoEncoder.getInputExperimentDate().setReferenceTarget(m_experimentInfoDecoder.getOutputExperimentDate());
	m_experimentInfoEncoder.getInputSubjectID().setReferenceTarget(m_experimentInfoDecoder.getOutputSubjectID());
	m_experimentInfoEncoder.getInputSubjectName().setReferenceTarget(m_experimentInfoDecoder.getOutputSubjectName());
	m_experimentInfoEncoder.getInputSubjectAge().setReferenceTarget(m_experimentInfoDecoder.getOutputSubjectAge());
	m_experimentInfoEncoder.getInputSubjectGender().setReferenceTarget(m_experimentInfoDecoder.getOutputSubjectGender());
	m_experimentInfoEncoder.getInputLaboratoryID().setReferenceTarget(m_experimentInfoDecoder.getOutputLaboratoryID());
	m_experimentInfoEncoder.getInputLaboratoryName().setReferenceTarget(m_experimentInfoDecoder.getOutputLaboratoryName());
	m_experimentInfoEncoder.getInputTechnicianID().setReferenceTarget(m_experimentInfoDecoder.getOutputTechnicianID());
	m_experimentInfoEncoder.getInputTechnicianName().setReferenceTarget(m_experimentInfoDecoder.getOutputTechnicianName());
	m_decoders.push_back(&m_experimentInfoDecoder);
	m_encoders.push_back(&m_experimentInfoEncoder);

	return true;
}

bool CTestCodecToolkit::uninitialize()
{
	for (size_t i = 0; i < m_decoders.size(); ++i) { m_decoders[i]->uninitialize(); }
	for (size_t i = 0; i < m_encoders.size(); ++i) { m_encoders[i]->uninitialize(); }

	return true;
}

bool CTestCodecToolkit::processInput(const size_t /*index*/)
{
	this->getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

bool CTestCodecToolkit::process()
{
	IBoxIO& boxContext           = this->getDynamicBoxContext();
	const IBox& staticBoxContext = this->getStaticBoxContext();

	for (size_t i = 0; i < staticBoxContext.getInputCount(); ++i)
	{
		for (size_t j = 0; j < boxContext.getInputChunkCount(i); ++j)
		{
			// we can manipulate decoders and encoders without knowing their types
			m_decoders[i]->decode(j);

			if (m_decoders[i]->isHeaderReceived()) { m_encoders[i]->encodeHeader(); }
			if (m_decoders[i]->isBufferReceived())
			{
				//let's check what is inside the buffer
				CIdentifier iType;
				staticBoxContext.getInputType(i, iType);
				if (iType == OV_TypeId_StreamedMatrix)
				{
					IMatrix* matrix = m_matrixDecoder.getOutputMatrix();
					this->getLogManager() << LogLevel_Info << "Streamed Matrix buffer received (" << matrix->getBufferElementCount() <<
							" elements in buffer).\n";
				}
				else if (iType == OV_TypeId_ChannelLocalisation)
				{
					IMatrix* matrix = m_channelLocalisationDecoder.getOutputMatrix();
					this->getLogManager() << LogLevel_Info << "Channel localisation buffer received (" << matrix->getBufferElementCount() <<
							" elements in buffer).\n";
				}
				else if (iType == OV_TypeId_FeatureVector)
				{
					IMatrix* matrix = m_featureVectorDecoder.getOutputMatrix();
					this->getLogManager() << LogLevel_Info << "Feature Vector buffer received (" << matrix->getBufferElementCount() <<
							" features in vector).\n";
				}
				else if (iType == OV_TypeId_Spectrum)
				{
					IMatrix* matrix = m_spectrumDecoder.getOutputFrequencyAbscissa();
					this->getLogManager() << LogLevel_Info << "Spectrum frequencies abscissas received (" << matrix->getBufferElementCount() <<
							" elements in matrix).\n";
				}
				else if (iType == OV_TypeId_Signal)
				{
					IMatrix* matrix   = m_signalDecoder.getOutputMatrix();
					uint64_t sampling = m_signalDecoder.getOutputSamplingRate();
					this->getLogManager() << LogLevel_Info << "Signal buffer received (" << matrix->getBufferElementCount() <<
							" elements in buffer) with sampling frequency " << sampling << "Hz.\n";
				}
				else if (iType == OV_TypeId_Stimulations)
				{
					IStimulationSet* stimSet = m_stimDecoder.getOutputStimulationSet();
					// as we constantly receive stimulations on the stream, we will check if the incoming set is empty or not
					if (stimSet->getStimulationCount() != 0)
					{
						this->getLogManager() << LogLevel_Info << "Stimulation Set buffer received (1st stim is ["
								<< stimSet->getStimulationIdentifier(0) << ":"
								<< this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, stimSet->getStimulationIdentifier(0))
								<< "]).\n";
						m_stimEncoder.getInputStimulationSet()->clear();
						m_stimEncoder.getInputStimulationSet()->appendStimulation(stimSet->getStimulationIdentifier(0) + 1, stimSet->getStimulationDate(0), 0);
					}
				}
				else if (iType == OV_TypeId_ExperimentInfo)
				{
					uint64_t xPid = m_experimentInfoDecoder.getOutputExperimentID();
					this->getLogManager() << LogLevel_Info << "Experiment information buffer received (xp ID: " << xPid << ").\n";
				}
				else
				{
					this->getLogManager() << LogLevel_Error << "Undefined input type.\n";
					return true;
				}
				m_encoders[i]->encodeBuffer();
			}
			if (m_decoders[i]->isEndReceived()) { m_encoders[i]->encodeEnd(); }
			boxContext.markOutputAsReadyToSend(i, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
		}
	}

	return true;
}
