#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <iostream>

namespace OpenViBE {
namespace Plugins {
namespace Tests {
class CCrashingBox final : public IBoxAlgorithm
{
public:

	void release() override { delete this; }

	bool initialize(Kernel::IBoxAlgorithmContext& context) override;
	bool uninitialize(Kernel::IBoxAlgorithmContext& context) override;

	bool processInput(Kernel::IBoxAlgorithmContext& context, const size_t index) override;
	bool process(Kernel::IBoxAlgorithmContext& context) override;

	_IsDerivedFromClass_Final_(IBoxAlgorithm, OVP_ClassId_CrashingBox)
};

class CCrashingBoxDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }
	CString getName() const override { return CString("Crashing box"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString("A box which code launches exceptions"); }

	CString getDetailedDescription() const override { return CString("This box illustrates the behavior of the platform given a crashing plugin code"); }

	CString getCategory() const override { return CString("Tests"); }
	CString getVersion() const override { return CString("1.0"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_CrashingBox; }
	IPluginObject* create() override { return new CCrashingBox(); }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("an input", OV_UndefinedIdentifier);
		return true;
	}

	CString getStockItemName() const override { return CString("gtk-cancel"); }

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_CrashingBoxDesc)
};
}  // namespace Tests
}  // namespace Plugins
}  // namespace OpenViBE
