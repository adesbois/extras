#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace Tests {
class CBoxAlgorithmAdditionTest final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	CBoxAlgorithmAdditionTest() {}

	void release() override { delete this; }

	uint64_t getClockFrequency() override { return uint64_t(1LL) << 36; }
	bool initialize() override;
	bool uninitialize() override;
	bool processClock(Kernel::CMessageClock& msg) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithmAdditionTest)

protected:

	Kernel::ELogLevel m_logLevel = Kernel::LogLevel_None;

	int64_t m_i1 = 0;
	int64_t m_i2 = 0;
	int64_t m_i3 = 0;
	int64_t m_i4 = 0;

	Kernel::IAlgorithmProxy* m_proxy1 = nullptr;
	Kernel::IAlgorithmProxy* m_proxy2 = nullptr;
	Kernel::IAlgorithmProxy* m_proxy3 = nullptr;
};

class CBoxAlgorithmAdditionTestDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Addition Test"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString("This box illustrates how an algorithm can be used in a box"); }

	CString getDetailedDescription() const override
	{
		return CString("This specific sample computes 4 random numbers and uses 3 sum operator algorithms in order to get the total");
	}

	CString getCategory() const override { return CString("Tests"); }
	CString getVersion() const override { return CString("1.0"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithmAdditionTest; }
	IPluginObject* create() override { return new CBoxAlgorithmAdditionTest(); }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addSetting("Log level to use", OV_TypeId_LogLevel, "Information");

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithmAdditionTestDesc)
};
}  // namespace Tests
}  // namespace Plugins
}  // namespace OpenViBE
