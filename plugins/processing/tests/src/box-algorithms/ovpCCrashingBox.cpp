#include "ovpCCrashingBox.h"
#include <cmath>	// For unix system

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Tests;
using namespace std;

bool CCrashingBox::initialize(IBoxAlgorithmContext& /*context*/) { throw 0; }

bool CCrashingBox::uninitialize(IBoxAlgorithmContext& /*context*/)
{
	const int one  = int(1.0);
	const int zero = int(sin(0.0));
	const int div  = one / zero;
	return div ? true : false;
}

bool CCrashingBox::processInput(IBoxAlgorithmContext& context, const size_t /*index*/)
{
	context.markAlgorithmAsReadyToProcess();
	return true;
}

bool CCrashingBox::process(IBoxAlgorithmContext& /*context*/)
{
	*static_cast<int*>(nullptr) = 0;
	return true;
}
