#include "ovpCAlgorithmAddition.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Tests;

bool CAlgorithmAddition::initialize()
{
	m_parameter1.initialize(getInputParameter(CIdentifier(0, 1)));
	m_parameter2.initialize(getInputParameter(CIdentifier(0, 2)));
	m_parameter3.initialize(getOutputParameter(CIdentifier(0, 3)));

	return true;
}

bool CAlgorithmAddition::uninitialize()
{
	m_parameter3.uninitialize();
	m_parameter2.uninitialize();
	m_parameter1.uninitialize();

	return true;
}

bool CAlgorithmAddition::process()
{
	m_parameter3 = m_parameter1 + m_parameter2;

	return true;
}
