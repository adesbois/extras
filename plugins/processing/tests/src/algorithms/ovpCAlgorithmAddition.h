#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>

#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace Tests {
class CAlgorithmAddition final : public Toolkit::TAlgorithm<IAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TAlgorithm<IAlgorithm>, OVP_ClassId_AlgorithmAddition)

protected:

	Kernel::TParameterHandler<int64_t> m_parameter1;
	Kernel::TParameterHandler<int64_t> m_parameter2;
	Kernel::TParameterHandler<int64_t> m_parameter3;
};

class CAlgorithmAdditionDesc final : public IAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Addition"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString("Computes and outputs the sum of two inputs"); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Tests"); }
	CString getVersion() const override { return CString("1.0"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_AlgorithmAddition; }
	IPluginObject* create() override { return new CAlgorithmAddition(); }

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		prototype.addInputParameter(CIdentifier(0, 1), "First addition operand", Kernel::ParameterType_Integer);
		prototype.addInputParameter(CIdentifier(0, 2), "Second addition operand", Kernel::ParameterType_Integer);
		prototype.addOutputParameter(CIdentifier(0, 3), "Addition result", Kernel::ParameterType_Integer);

		return true;
	}

	_IsDerivedFromClass_Final_(IAlgorithmDesc, OVP_ClassId_AlgorithmAdditionDesc)
};
}  // namespace Tests
}  // namespace Plugins
}  // namespace OpenViBE
