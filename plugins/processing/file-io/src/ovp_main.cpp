#include "ovp_defines.h"
#include "box-algorithms/ovpCGDFFileReader.h"
#include "box-algorithms/ovpCGDFFileWriter.h"
#include "box-algorithms/ovpCBCICompetitionIIIbReader.h"

#include "algorithms/brainamp/ovpCAlgorithmBrainampFileReader.h"

#include "box-algorithms/brainamp/ovpCBoxAlgorithmBrainampFileReader.h"
#include "box-algorithms/brainamp/ovpCBoxAlgorithmBrainampFileWriter.h"

#include "box-algorithms/bci2000reader/ovpCBoxAlgorithmBCI2000Reader.h"

#include "box-algorithms/ovpCBoxAlgorithmSignalConcatenation.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
	context.getTypeManager().registerEnumerationEntry(OV_TypeId_BoxAlgorithmFlag, OV_AttributeId_Box_FlagIsUnstable.toString(),
													  OV_AttributeId_Box_FlagIsUnstable.toUInteger());

	OVP_Declare_New(FileIO::CGDFFileReaderDesc)
	OVP_Declare_New(FileIO::CGDFFileWriterDesc)
	OVP_Declare_New(FileIO::CBCICompetitionIIIbReaderDesc)
	OVP_Declare_New(FileIO::CAlgorithmBrainampFileReaderDesc)
	OVP_Declare_New(FileIO::CBoxAlgorithmBrainampFileReaderDesc)
	OVP_Declare_New(FileIO::CBoxAlgorithmBrainampFileWriterDesc)
	OVP_Declare_New(FileIO::CBoxAlgorithmBCI2000ReaderDesc)
	OVP_Declare_New(FileIO::CBoxAlgorithmSignalConcatenationDesc)

OVP_Declare_End()
