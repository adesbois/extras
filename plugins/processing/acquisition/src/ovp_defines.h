#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_AcquisitionClient			OpenViBE::CIdentifier(0x35D225CB, 0x3E6E3A5F)
#define OVP_ClassId_BoxAlgorithm_AcquisitionClientDesc		OpenViBE::CIdentifier(0x7D3061B9, 0x43565E8C)

// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
