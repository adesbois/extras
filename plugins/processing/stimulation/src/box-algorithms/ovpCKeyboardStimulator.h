#pragma once

#if defined(TARGET_HAS_ThirdPartyGTK)

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <vector>
#include <map>

#include <visualization-toolkit/ovviz_all.h>

namespace TCPTagging {
class IStimulusSender; // fwd declare
}

namespace OpenViBE {
namespace Plugins {
namespace Stimulation {
class CKeyboardStimulator final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	CKeyboardStimulator() { }

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	uint64_t getClockFrequency() override { return (32LL << 32); }

	bool processClock(Kernel::CMessageClock& msg) override;
	bool process() override { return true; }

	bool parseConfigurationFile(const char* filename);
	void processKey(const guint key, const bool state);

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_KeyboardStimulator)

protected:
	Toolkit::TStimulationEncoder<CKeyboardStimulator> m_encoder;

	GtkWidget* m_widget = nullptr;

	typedef struct
	{
		uint64_t press;
		uint64_t release;
		bool status;
	} key_t;

	//! Stores keyvalue/stimulation couples
	std::map<guint, key_t> m_keyToStimulation;

	//! Vector of the stimulations to send when possible
	std::vector<uint64_t> m_stimulationToSend;

	//! Plugin's previous activation date
	uint64_t m_previousActivationTime = 0;

	bool m_error = false;

	// TCP Tagging
	TCPTagging::IStimulusSender* m_stimulusSender = nullptr;

	bool m_unknownKeyPressed = false;
	size_t m_unknownKeyCode  = 0;

	VisualizationToolkit::IVisualizationContext* m_visualizationCtx = nullptr;
};

/**
* Plugin's description
*/
class CKeyboardStimulatorDesc final : public IBoxAlgorithmDesc
{
public:
	CString getName() const override { return CString("Keyboard stimulator"); }
	CString getAuthorName() const override { return CString("Bruno Renier"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString("Stimulation generator"); }
	CString getDetailedDescription() const override { return CString("Sends stimulations according to key presses"); }
	CString getCategory() const override { return CString("Stimulation"); }
	CString getVersion() const override { return CString("0.2"); }
	void release() override { }
	CIdentifier getCreatedClass() const override { return OVP_ClassId_KeyboardStimulator; }
	IPluginObject* create() override { return new CKeyboardStimulator(); }

	bool hasFunctionality(const EPluginFunctionality functionality) const override { return functionality == EPluginFunctionality::Visualization; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addOutput("Outgoing Stimulations", OV_TypeId_Stimulations);

		prototype.addSetting("Filename", OV_TypeId_Filename, "${Path_Data}/plugins/stimulation/simple-keyboard-to-stimulations.txt");
		// @note we don't want to expose these to the user as there is no latency correction in tcp tagging; its best to use localhost
		// prototype.addSetting("TCP Tagging Host address", OV_TypeId_String, "");
		// prototype.addSetting("TCP Tagging Host port",    OV_TypeId_Integer, "15361");

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_KeyboardStimulatorDesc)
};
}  // namespace Stimulation
}  // namespace Plugins
}  // namespace OpenViBE

#endif
