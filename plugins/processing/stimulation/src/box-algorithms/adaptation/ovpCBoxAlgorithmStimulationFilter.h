#pragma once

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <vector>

namespace OpenViBE {
namespace Plugins {
namespace Stimulation {
class CBoxAlgorithmStimulationFilter final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_StimulationFilter)

protected:

	Kernel::IAlgorithmProxy* m_decoder = nullptr;
	Kernel::IAlgorithmProxy* m_encoder = nullptr;
	Kernel::TParameterHandler<const IMemoryBuffer*> ip_buffer;
	Kernel::TParameterHandler<IStimulationSet*> op_stimSet;
	Kernel::TParameterHandler<IStimulationSet*> ip_stimSet;
	Kernel::TParameterHandler<IMemoryBuffer*> op_buffer;

	typedef struct
	{
		uint64_t action;
		uint64_t startStimID;
		uint64_t EndStimID;
	} rule_t;

	uint64_t m_defaultAction = 0;
	uint64_t m_startTime     = 0;
	uint64_t m_endTime       = 0;
	std::vector<rule_t> m_rules;
};

class CBoxAlgorithmStimulationFilterListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:
	bool onSettingAdded(Kernel::IBox& box, const size_t index) override
	{
		box.removeSetting(index);
		//we had a whole rule (3 settings)
		box.addSetting("Action to perform", OVP_TypeId_StimulationFilterAction, "Select");
		box.addSetting("Stimulation range begin", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
		box.addSetting("Stimulation range end", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_0F");

		return true;
	}

	bool onSettingRemoved(Kernel::IBox& box, const size_t index) override
	{
		//we must remove the 2 other settings corresponding to the rule
		const size_t settingGroupIdx = (index - 3) / 3;
		box.removeSetting(settingGroupIdx * 3 + 3);
		box.removeSetting(settingGroupIdx * 3 + 3);
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CBoxAlgorithmStimulationFilterDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Stimulation Filter"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }

	CString getShortDescription() const override { return CString("Filters incoming stimulations selecting or rejecting specific ranges of stimulations"); }

	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Stimulation/Adaptation"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-missing-image"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_StimulationFilter; }
	IPluginObject* create() override { return new CBoxAlgorithmStimulationFilter; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Stimulations", OV_TypeId_Stimulations);
		prototype.addOutput("Modified Stimulations", OV_TypeId_Stimulations);

		prototype.addSetting("Default action", OVP_TypeId_StimulationFilterAction, "Reject");
		prototype.addSetting("Time range begin", OV_TypeId_Float, "0");
		prototype.addSetting("Time range end", OV_TypeId_Float, "0");
		prototype.addSetting("Action to perform", OVP_TypeId_StimulationFilterAction, "Select");
		prototype.addSetting("Stimulation range begin", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
		prototype.addSetting("Stimulation range end", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_0F");

		prototype.addFlag(Kernel::BoxFlag_CanAddSetting);
		// prototype.addFlag   (OV_AttributeId_Box_FlagIsUnstable);
		return true;
	}

	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmStimulationFilterListener; }
	virtual void releaseBoxListener(IBoxListener* listener) { delete listener; }

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_StimulationFilterDesc)
};
}  // namespace Stimulation
}  // namespace Plugins
}  // namespace OpenViBE
