#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <cstdio>
#include <vector>
#include <map>
#include <iomanip>

namespace OpenViBE {
namespace Plugins {
namespace Stimulation {
class CBoxAlgorithmRunCommand final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_RunCommand)

protected:

	Kernel::IAlgorithmProxy* m_decoder = nullptr;
	Kernel::TParameterHandler<const IMemoryBuffer*> ip_buffer;
	Kernel::TParameterHandler<IStimulationSet*> op_stimSet;
	std::map<uint64_t, std::vector<CString>> m_commands;
};

class CBoxAlgorithmRunCommandListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	bool check(Kernel::IBox& box) const
	{
		for (size_t i = 0; i < box.getSettingCount(); i += 2)
		{
			box.setSettingName(i, ("Stimulation " + std::to_string(i / 2 + 1)).c_str());
			box.setSettingType(i, OV_TypeId_Stimulation);
			box.setSettingName(i + 1, ("Command " + std::to_string(i / 2 + 1)).c_str());
			box.setSettingType(i + 1, OV_TypeId_String);
		}
		return true;
	}

	bool onSettingAdded(Kernel::IBox& box, const size_t index) override
	{
		std::stringstream ss;
		ss.fill('0');
		ss << std::setw(2) << size_t(index / 2 + 1);
		const CString name(ss.str().c_str());
		box.setSettingDefaultValue(index, name);
		box.setSettingValue(index, name);
		box.addSetting("", OV_TypeId_String, "");
		this->check(box);
		return true;
	}

	bool onSettingRemoved(Kernel::IBox& box, const size_t index) override
	{
		box.removeSetting((index / 2) * 2);
		this->check(box);
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CBoxAlgorithmRunCommandDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Run Command"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }

	CString getShortDescription() const override { return CString("Runs some command using system call depending on provided stimulations"); }

	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Stimulation"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-execute"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_RunCommand; }
	IPluginObject* create() override { return new CBoxAlgorithmRunCommand; }
	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmRunCommandListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Stimulations", OV_TypeId_Stimulations);
		prototype.addSetting("Stimulation 1", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
		prototype.addSetting("Command 1", OV_TypeId_String, "");
		prototype.addFlag(Kernel::BoxFlag_CanAddSetting);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_RunCommandDesc)
};
}  // namespace Stimulation
}  // namespace Plugins
}  // namespace OpenViBE
