#pragma once

#if defined TARGET_HAS_ThirdPartyLua

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <boost/thread.hpp>
#include <boost/thread/condition.hpp>
#include <boost/noncopyable.hpp>

#include <map>
#include <vector>
#include <cstdio>

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

namespace OpenViBE {
namespace Plugins {
namespace Stimulation {
class CBoxAlgorithmLuaStimulator final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>, public boost::noncopyable
{
public:

	CBoxAlgorithmLuaStimulator();
	~CBoxAlgorithmLuaStimulator() override = default;

	void release() override { delete this; }

	uint64_t getClockFrequency() override;
	bool initialize() override;
	bool uninitialize() override;
	bool processClock(Kernel::CMessageClock& msg) override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_LuaStimulator)

	bool waitForStimulation(size_t inputIdx, size_t stimIdx);

	bool getInputCountCB(size_t& count);
	bool getOutputCountCB(size_t& count);
	bool getSettingCountCB(size_t& count);
	bool getSettingCB(size_t index, CString& setting);
	bool getConfigCB(const CString& str, CString& config);

	bool getCurrentTimeCB(uint64_t& time);
	bool sleepCB();
	bool getStimulationCountCB(size_t index, size_t& count);
	bool getStimulationCB(size_t inputIdx, size_t stimIdx, uint64_t& id, uint64_t& time, uint64_t& duration);
	bool removeStimulationCB(size_t inputIdx, size_t stimIdx);
	bool sendStimulationCB(size_t outputIdx, uint64_t id, uint64_t time, uint64_t duration);
	bool log(Kernel::ELogLevel level, const CString& text);

	void doThread();

	enum class EStates
	{
		Unstarted,
		Processing,
		Please_Quit,
		Finished,
	};

	static std::string toString(const EStates state);

	EStates m_State          = EStates::Unstarted;
	bool m_LuaThreadHadError = false;		// Set to true by the lua thread if there were issues
	bool m_FilterMode        = false;            // Output chunk generation driven by inputs (true) or by clock (false)

protected:

	lua_State* m_luaState = nullptr;

	uint64_t m_lastTime = 0;
	std::vector<std::multimap<uint64_t, std::pair<uint64_t, uint64_t>>> m_oStimulations;
	std::vector<std::multimap<uint64_t, std::pair<uint64_t, uint64_t>>> m_iStimulations;

	boost::thread* m_luaThread = nullptr;
	boost::mutex m_mutex;
	boost::mutex::scoped_lock m_innerLock;
	boost::mutex::scoped_lock m_outerLock;
	boost::mutex::scoped_lock m_exitLock;
	boost::condition m_condition;
	boost::condition m_exitCondition;

	std::vector<Toolkit::TStimulationDecoder<CBoxAlgorithmLuaStimulator>*> m_decoders;
	std::vector<Toolkit::TStimulationEncoder<CBoxAlgorithmLuaStimulator>*> m_encoders;

private:

	CBoxAlgorithmLuaStimulator(const CBoxAlgorithmLuaStimulator&);

	bool runLuaThread();
	bool sendStimulations(uint64_t startTime, uint64_t endTime);
};

class CBoxAlgorithmLuaStimulatorListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	bool onInputAdded(Kernel::IBox& box, const size_t index) override
	{
		box.setInputType(index, OV_TypeId_Stimulations);
		return true;
	}

	bool onOutputAdded(Kernel::IBox& box, const size_t index) override
	{
		box.setOutputType(index, OV_TypeId_Stimulations);
		return true;
	}

	bool onSettingAdded(Kernel::IBox& box, const size_t index) override
	{
		box.setSettingType(index, OV_TypeId_String);
		box.setSettingName(index, ("Setting " + std::to_string(index)).c_str());
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CBoxAlgorithmLuaStimulatorDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Lua Stimulator"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Generates some stimulations according to an Lua script"); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Scripting"); }
	CString getVersion() const override { return CString("1.1"); }
	CString getStockItemName() const override { return CString("gtk-missing-image"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_LuaStimulator; }
	IPluginObject* create() override { return new CBoxAlgorithmLuaStimulator; }
	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmLuaStimulatorListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addOutput("Stimulations", OV_TypeId_Stimulations);

		prototype.addSetting("Lua Script", OV_TypeId_Script, "");

		prototype.addFlag(Kernel::BoxFlag_CanAddOutput);
		prototype.addFlag(Kernel::BoxFlag_CanAddInput);
		prototype.addFlag(Kernel::BoxFlag_CanAddSetting);
		prototype.addFlag(Kernel::BoxFlag_CanModifySetting);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_LuaStimulatorDesc)
};
}  // namespace Stimulation
}  // namespace Plugins
}  // namespace OpenViBE

#endif // TARGET_HAS_ThirdPartyLua
