#include "CBoxAlgorithmStimulationValidator.hpp"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::Plugins::*/Stimulation;

///-------------------------------------------------------------------------------------------------
bool CBoxAlgorithmStimulationValidator::initialize()
{
	m_decoder.initialize(*this, 0);
	m_iStimulation = m_decoder.getOutputStimulationSet();
	m_encoder.initialize(*this, 0);
	m_oStimulation = m_encoder.getInputStimulationSet();

	m_stim  = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0));
	m_limit = size_t(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1)));

	return true;
}
///-------------------------------------------------------------------------------------------------

///-------------------------------------------------------------------------------------------------
bool CBoxAlgorithmStimulationValidator::uninitialize()
{
	m_decoder.uninitialize();
	m_encoder.uninitialize();
	return true;
}
///-------------------------------------------------------------------------------------------------


///-------------------------------------------------------------------------------------------------
bool CBoxAlgorithmStimulationValidator::processInput(const size_t index)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
///-------------------------------------------------------------------------------------------------


///-------------------------------------------------------------------------------------------------
bool CBoxAlgorithmStimulationValidator::process()
{
	IBoxIO& boxCtx = this->getDynamicBoxContext();

	const uint64_t currentTime = getPlayerContext().getCurrentTime();
	m_oStimulation->setStimulationCount(0);								// reset stimulation output
	//***** Stimulations *****
	for (size_t i = 0; i < boxCtx.getInputChunkCount(0); ++i)
	{
		m_decoder.decode(i);								// Decode the chunk
		if (m_decoder.isBufferReceived())					// Buffer Received
		{
			for (size_t j = 0; j < m_iStimulation->getStimulationCount(); ++j)
			{
				if (m_iStimulation->getStimulationIdentifier(j) == m_stim)
				{
					m_count++;
					if (m_count >= m_limit)
					{
						m_count = 0;
						m_oStimulation->appendStimulation(m_stim.toUInteger(), m_iStimulation->getStimulationDate(j), 0);
					}
				}
			}
		}
	}
	if (m_lastEndTime == 0) { m_encoder.encodeHeader(); }			// First time
	else { m_encoder.encodeBuffer(); }								// Others
	boxCtx.markOutputAsReadyToSend(0, m_lastEndTime, currentTime);	// preserve continuous
	m_lastEndTime = currentTime;									// preserve continuous
	return true;
}
///-------------------------------------------------------------------------------------------------
