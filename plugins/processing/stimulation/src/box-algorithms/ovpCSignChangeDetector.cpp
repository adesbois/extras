#include "ovpCSignChangeDetector.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Stimulation;

bool CSignChangeDetector::initialize()
{
	// we read the settings:
	// The stimulations names:
	const CString on  = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	const CString off = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	m_onStimId  = getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_Stimulation, on);
	m_offStimId = getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_Stimulation, off);

	m_lastSample  = 0;
	m_firstSample = true;

	m_decoder.initialize(*this, 0);
	m_encoder.initialize(*this, 0);

	m_channelIdx = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	if (m_channelIdx == 0)
	{
		this->getLogManager() << LogLevel_Info << "Channel Index is 0. The channel indexing convention starts from 1.\n";
		return false;
	}
	m_channelIdx--; // Convert from [1,n] indexing to [0,n-1] indexing used later

	return true;
}

bool CSignChangeDetector::uninitialize()
{
	m_encoder.uninitialize();
	m_decoder.uninitialize();

	return true;
}

bool CSignChangeDetector::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CSignChangeDetector::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	// Get a few convenience handles
	const IMatrix* matrix    = m_decoder.getOutputMatrix();
	IStimulationSet* stimSet = m_encoder.getInputStimulationSet();

	// We decode the stream matrix
	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		const uint64_t start = boxContext.getInputChunkStartTime(0, i);
		const uint64_t end   = boxContext.getInputChunkEndTime(0, i);

		m_decoder.decode(i);

		// if  we received the header
		if (m_decoder.isHeaderReceived())
		{
			//we analyse the header (meaning the input matrix size)
			if (matrix->getDimensionCount() != 2)
			{
				this->getLogManager() << LogLevel_ImportantWarning << "Streamed matrix must have exactly 2 dimensions\n";
				return false;
			}
			if (m_channelIdx >= matrix->getDimensionSize(0))
			{
				this->getLogManager() << LogLevel_Info << "Channel Index out of bounds. Incoming matrix has fewer channels than specified index.\n";
				return false;
			}

			m_samplesPerChannel = matrix->getDimensionSize(1);

			// we send a header on the stimulation output:
			m_encoder.encodeHeader();
			boxContext.markOutputAsReadyToSend(0, start, end);
		}


		// if we received a buffer
		if (m_decoder.isBufferReceived())
		{
			stimSet->clear();
			const double* data = matrix->getBuffer();
			// for each data sample of the buffer we look for sign change

			for (size_t j = 0; j < matrix->getDimensionSize(1); ++j)
			{
				const double currentSample = data[(m_channelIdx * m_samplesPerChannel) + j];
				if (m_firstSample)
				{
					m_lastSample  = currentSample;
					m_firstSample = false;
				}

				// Change from positive to negative
				if (m_lastSample >= 0 && currentSample < 0)
				{
					const uint64_t time = start + (end - start) * j / m_samplesPerChannel;
					stimSet->appendStimulation(m_offStimId, time, 0);
				}

				// Change from negative to positive
				if (m_lastSample < 0 && currentSample >= 0)
				{
					const uint64_t time = start + (end - start) * j / m_samplesPerChannel;
					stimSet->appendStimulation(m_onStimId, time, 0);
				}

				m_lastSample = currentSample;
			}

			m_encoder.encodeBuffer();
			boxContext.markOutputAsReadyToSend(0, start, end);
		}

		// if we received the End
		if (m_decoder.isEndReceived())
		{
			// we send the End signal to the stimulation output:
			m_encoder.encodeEnd();
			boxContext.markOutputAsReadyToSend(0, start, end);
		}

		// The stream matrix chunk i has been processed
		boxContext.markInputAsDeprecated(0, i);
	}

	return true;
}
