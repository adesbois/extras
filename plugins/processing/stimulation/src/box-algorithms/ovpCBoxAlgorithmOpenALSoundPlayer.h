#pragma once

#if defined TARGET_HAS_ThirdPartyOpenAL

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <AL/alut.h>
#include <vorbis/vorbisfile.h>
#include <iostream>
#include <vector>

namespace TCPTagging {
class IStimulusSender; // fwd declare
}

namespace OpenViBE {
namespace Plugins {
namespace Stimulation {
class CBoxAlgorithmOpenALSoundPlayer final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	struct SOggVorbisStream
	{
		OggVorbis_File Stream;
		FILE* File;
		ALenum Format;
		ALsizei SampleRate;
	};

	enum ESupportedFileFormat
	{
		FILE_FORMAT_WAV = 0,
		FILE_FORMAT_OGG,
		FILE_FORMAT_UNSUPPORTED
	};

	CBoxAlgorithmOpenALSoundPlayer() { }

	void release() override { delete this; }

	uint64_t getClockFrequency() override { return (128LL << 32); }
	bool initialize() override;
	bool uninitialize() override;
	bool processClock(Kernel::CMessageClock& msg) override;
	bool processInput(const size_t index) override;
	bool process() override;

	bool openSoundFile();
	bool playSound();
	bool stopSound(bool forwardStim);

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_OpenALSoundPlayer)

protected:

	Toolkit::TStimulationDecoder<CBoxAlgorithmOpenALSoundPlayer> m_decoder;
	Toolkit::TStimulationEncoder<CBoxAlgorithmOpenALSoundPlayer> m_encoder;

	uint64_t m_lastOutputChunkDate = 0;
	bool m_startOfSoundSent        = false;
	bool m_endOfSoundSent          = false;
	bool m_loop                    = false;
	uint64_t m_playTrigger         = 0;
	uint64_t m_stopTrigger         = 0;
	CString m_filename;

	std::vector<ALuint> m_sources;
	ESupportedFileFormat m_fileFormat = FILE_FORMAT_UNSUPPORTED;

	//The handles
	ALuint m_soundBufferHandle;
	ALuint m_sourceHandle;
	//OGG
	SOggVorbisStream m_oggVorbisStream;
	std::vector<char> m_rawOggBufferFromFile;

	// For sending stimulations to the TCP Tagging
	TCPTagging::IStimulusSender* m_stimulusSender = nullptr;
};

class CBoxAlgorithmOpenALSoundPlayerDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Sound Player"); }
	CString getAuthorName() const override { return CString("Laurent Bonnet"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Play/Stop a sound, with or without loop."); }

	CString getDetailedDescription() const override
	{
		return CString("Available format : WAV / OGG. Play and stop with input stimulations. Box based on OpenAL.");
	}

	CString getCategory() const override { return CString("Stimulation"); }
	CString getVersion() const override { return CString("1.1"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_OpenALSoundPlayer; }
	IPluginObject* create() override { return new CBoxAlgorithmOpenALSoundPlayer; }
	CString getStockItemName() const override { return CString("gtk-media-play"); }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input triggers", OV_TypeId_Stimulations);
		prototype.addOutput("Resync triggers (deprecated)", OV_TypeId_Stimulations);
		prototype.addSetting("PLAY trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
		prototype.addSetting("STOP trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
		prototype.addSetting("File to play", OV_TypeId_Filename, "${Path_Data}/plugins/stimulation/ov_beep.wav");
		prototype.addSetting("Loop", OV_TypeId_Boolean, "false");

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_OpenALSoundPlayerDesc)
};
}  // namespace Stimulation
}  // namespace Plugins
}  // namespace OpenViBE

#endif //TARGET_HAS_ThirdPartyOpenAL
