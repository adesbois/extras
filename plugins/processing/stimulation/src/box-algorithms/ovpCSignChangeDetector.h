#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace Stimulation {
class CSignChangeDetector final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_SignChangeDetector)

protected:

	Toolkit::TStreamedMatrixDecoder<CSignChangeDetector> m_decoder;
	Toolkit::TStimulationEncoder<CSignChangeDetector> m_encoder;

	uint64_t m_onStimId          = 0;
	uint64_t m_offStimId         = 0;
	uint64_t m_channelIdx        = 0;
	uint64_t m_samplesPerChannel = 0;
	double m_lastSample          = 0;
	bool m_firstSample           = false;
};

class CSignChangeDetectorDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Sign Change Detector"); }
	CString getAuthorName() const override { return CString("Joan Fruitet and Jozef Legeny"); }
	CString getAuthorCompanyName() const override { return CString("Inria Sophia"); }
	CString getShortDescription() const override { return CString("Detects the change of input's sign"); }

	CString getDetailedDescription() const override
	{
		return CString("Triggers a stimulation when one of the input's sign changes (input gets positive or negative");
	}

	CString getCategory() const override { return CString("Stimulation"); }
	CString getVersion() const override { return CString("1.2"); }
	CString getStockItemName() const override { return CString("gtk-missing-image"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_SignChangeDetector; }
	IPluginObject* create() override { return new CSignChangeDetector; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Signal", OV_TypeId_StreamedMatrix);
		prototype.addOutput("Generated stimulations", OV_TypeId_Stimulations);
		prototype.addSetting("Sign switch to positive stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
		prototype.addSetting("Sign switch to negative stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
		prototype.addSetting("Channel Index", OV_TypeId_Integer, "1");
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_SignChangeDetectorDesc)
};
}  // namespace Stimulation
}  // namespace Plugins
}  // namespace OpenViBE
