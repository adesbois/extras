#include "ovpCBoxAlgorithmLuaStimulator.h"


#if defined TARGET_HAS_ThirdPartyLua

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Stimulation;
using namespace /*OpenViBE::*/Toolkit;

#define __CB_Assert__(condition) \
	if(!(condition)) \
	{ \
		lua_pushstring(state, "Assertion failed : ["#condition"]"); \
		lua_error(state); \
		return 0; \
	}

namespace {
class CLuaThread
{
public:
	CLuaThread(CBoxAlgorithmLuaStimulator* pBox) : m_box(pBox) { }
	CLuaThread(const CLuaThread& thread) : m_box(thread.m_box) { }
	void operator()() const { m_box->doThread(); }

protected:
	CBoxAlgorithmLuaStimulator* m_box;
};
}  // namespace

static void lua_setcallback(lua_State* state, const char* name, int (*callback)(lua_State*), void* data)
{
	lua_getglobal(state, "__openvibe_box_context");
	lua_pushlightuserdata(state, data);
	lua_pushcclosure(state, callback, 1);
	lua_setfield(state, -2, name);
}

// Returns true on success, i.e. if code is 0 (no failure).
static bool lua_report(ILogManager& logManager, lua_State* state, const int code)
{
	if (code)
	{
		logManager << LogLevel_ImportantWarning << "Lua error: " << lua_tostring(state, -1) << "\n";
		lua_pop(state, 1);
	}
	return (code == 0);
}

static bool lua_check_argument_count(lua_State* state, const char* name, const int count1, const int count2 = -1)
{
	if (count2 < 0)
	{
		if (lua_gettop(state) != count1 + 1)
		{
			const std::string msg = "[" + std::string(name) + "] takes " + std::to_string(count1) + " parameter" + (count1 >= 2 ? "s" : "");
			lua_pushstring(state, msg.c_str());
			lua_error(state);
			return false;
		}
	}
	else
	{
		if (lua_gettop(state) != count1 + 1 && lua_gettop(state) != count2 + 1)
		{
			const std::string msg = "[" + std::string(name) + "] takes either " + std::to_string(count1) + " or " + std::to_string(count2) + " parameter"
									+ (count2 >= 2 ? "s" : "");
			lua_pushstring(state, msg.c_str());
			lua_error(state);
			return false;
		}
	}
	return true;
}

static bool lua_check_box_status(lua_State* state, const char* name, const CBoxAlgorithmLuaStimulator::EStates curState)
{
	if (curState == CBoxAlgorithmLuaStimulator::EStates::Please_Quit)
	{
		const std::string msg = "[" + std::string(name) + "] This thread has been requested to quit...";
		lua_pushstring(state, msg.c_str());
		lua_error(state);
		return false;
	}
	if (curState != CBoxAlgorithmLuaStimulator::EStates::Processing)
	{
		const std::string msg = "[" + std::string(name) + "] should only be called in the [process] callback";
		lua_pushstring(state, msg.c_str());
		lua_error(state);
		return false;
	}
	return true;
}

static int lua_get_input_count_cb(lua_State* state)
{
	size_t nInput = 0;
	auto pThis    = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "get_input_count", 0)) { return 0; }

	__CB_Assert__(pThis->getInputCountCB(nInput));
	lua_pushinteger(state, nInput);
	return 1;
}

static int lua_get_output_count_cb(lua_State* state)
{
	size_t nOutput = 0;
	auto pThis     = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "get_output_count", 0)) { return 0; }

	__CB_Assert__(pThis->getOutputCountCB(nOutput));
	lua_pushinteger(state, nOutput);
	return 1;
}

static int lua_get_setting_count_cb(lua_State* state)
{
	size_t nSetting = 0;
	auto pThis      = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "get_setting_count", 0)) { return 0; }

	__CB_Assert__(pThis->getSettingCountCB(nSetting));
	lua_pushinteger(state, nSetting);
	return 1;
}

static int lua_get_setting_cb(lua_State* state)
{
	CString setting;
	auto pThis = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "get_setting", 1)) { return 0; }

	__CB_Assert__(pThis->getSettingCB(size_t(lua_tointeger(state, 2)-1), setting));
	lua_pushstring(state, setting.toASCIIString());
	return 1;
}

static int lua_get_config_cb(lua_State* state)
{
	CString config;
	auto pThis = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "get_config", 1)) { return 0; }

	__CB_Assert__(pThis->getConfigCB(lua_tostring(state, 2), config));
	lua_pushstring(state, config.toASCIIString());
	return 1;
}

static int lua_get_current_time_cb(lua_State* state)
{
	uint64_t time = 0;
	auto pThis    = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "get_current_time", 0)) { return 0; }

	__CB_Assert__(pThis->getCurrentTimeCB(time));
	lua_pushnumber(state, CTime(time).toSeconds());
	return 1;
}

static int lua_sleep_cb(lua_State* state)
{
	auto pThis = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_box_status(state, "sleep", pThis->m_State)) { return 0; }
	if (!lua_check_argument_count(state, "sleep", 0)) { return 0; }

	__CB_Assert__(pThis->sleepCB());
	return 0;
}

static int lua_get_stimulation_count_cb(lua_State* state)
{
	size_t count = 0;
	auto pThis   = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_box_status(state, "get_stimulation_count", pThis->m_State)) { return 0; }
	if (!lua_check_argument_count(state, "get_stimulation_count", 1)) { return 0; }

	__CB_Assert__(pThis->getStimulationCountCB(size_t(lua_tointeger(state, 2)-1), count));
	lua_pushinteger(state, count);
	return 1;
}

static int lua_get_stimulation_cb(lua_State* state)
{
	uint64_t id       = uint64_t(-1);
	uint64_t time     = uint64_t(-1);
	uint64_t duration = uint64_t(-1);
	auto pThis        = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_box_status(state, "get_stimulation", pThis->m_State)) { return 0; }
	if (!lua_check_argument_count(state, "get_stimulation", 2)) { return 0; }

	__CB_Assert__(pThis->getStimulationCB(size_t(lua_tointeger(state, 2)-1), size_t(lua_tointeger(state, 3)-1), id, time, duration));
	lua_pushinteger(state, lua_Integer(id));
	lua_pushnumber(state, CTime(time).toSeconds());
	lua_pushnumber(state, CTime(duration).toSeconds());
	return 3;
}

static int lua_remove_stimulation_cb(lua_State* state)
{
	auto pThis = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_box_status(state, "remove_stimulation", pThis->m_State)) { return 0; }
	if (!lua_check_argument_count(state, "remove_stimulation", 2)) { return 0; }

	__CB_Assert__(pThis->removeStimulationCB(size_t(lua_tointeger(state, 2)-1), size_t(lua_tointeger(state, 3)-1)));
	return 0;
}

static int lua_send_stimulation_cb(lua_State* state)
{
	const int iArguments = lua_gettop(state);
	auto pThis           = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_box_status(state, "send_stimulation", pThis->m_State)) { return 0; }
	if (!lua_check_argument_count(state, "send_stimulation", 3, 4)) { return 0; }

	__CB_Assert__(pThis->sendStimulationCB(size_t(lua_tointeger(state, 2)-1), size_t(lua_tointeger(state, 3)),
		CTime(double(lua_tonumber(state, 4))).time(),
		(iArguments == 5 ? CTime(double(lua_tonumber(state, 5))).time() : 0)));
	return 0;
}

// Has the side effect of setting m_bLuaThreadHadError to true if called with "Error" or "Fatal" loglevels.
static int lua_log_cb(lua_State* state)
{
	auto pThis = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "log", 2)) { return 0; }

	ELogLevel level = LogLevel_Debug;
	const std::string str(lua_tostring(state, 2));
	if (str == "Trace") { level = LogLevel_Trace; }
	else if (str == "Info") { level = LogLevel_Info; }
	else if (str == "Warning") { level = LogLevel_Warning; }
	else if (str == "ImportantWarning") { level = LogLevel_ImportantWarning; }
	else if (str == "Error")
	{
		level                      = LogLevel_Error;
		pThis->m_LuaThreadHadError = true;
	}
	else if (str == "Fatal")
	{
		level                      = LogLevel_Fatal;
		pThis->m_LuaThreadHadError = true;
	}
	__CB_Assert__(pThis->log(level, lua_tostring(state, 3)));

	return 0;
}

static int lua_keep_processing_cb(lua_State* state)
{
	const auto pThis = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);
	if (pThis->m_State == CBoxAlgorithmLuaStimulator::EStates::Processing) { return 1; }
	return 0;
}


static int lua_set_filter_mode_cb(lua_State* state)
{
	auto pThis = static_cast<CBoxAlgorithmLuaStimulator*>(lua_touserdata(state, lua_upvalueindex(1)));
	__CB_Assert__(pThis != nullptr);

	if (!lua_check_argument_count(state, "set_filter_mode", 1)) { return 0; }

	pThis->m_FilterMode = (lua_tointeger(state, 2) == 0 ? false : true);

	return 0;
}

#if BOOST_VERSION >= 103500
CBoxAlgorithmLuaStimulator::CBoxAlgorithmLuaStimulator()
	: m_innerLock(m_mutex, boost::defer_lock), m_outerLock(m_mutex, boost::defer_lock), m_exitLock(m_mutex, boost::defer_lock) { }
#else
CBoxAlgorithmLuaStimulator::CBoxAlgorithmLuaStimulator()
	: m_innerLock(m_mutex, false), m_outerLock(m_mutex, false), m_exitLock(m_mutex, false) { }
#endif

uint64_t CBoxAlgorithmLuaStimulator::getClockFrequency()
{
	if (m_FilterMode) { return 0; }		// Only when input received
	return 128LL << 32;					// the box clock frequency
}

bool CBoxAlgorithmLuaStimulator::processClock(Kernel::CMessageClock& /*msg*/)
{
	if (!m_FilterMode) { this->getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess(); }
	return true;
}

bool CBoxAlgorithmLuaStimulator::processInput(const size_t /*index*/)
{
	if (m_FilterMode) { getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess(); }
	return true;
}

bool CBoxAlgorithmLuaStimulator::initialize()
{
	size_t i;
	const IBox& boxContext = this->getStaticBoxContext();

	CString filename = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	filename         = this->getConfigurationManager().expand(filename);

	m_FilterMode = false;

	m_State    = EStates::Unstarted;
	m_luaState = luaL_newstate();

	luaL_openlibs(m_luaState);

	lua_newtable(m_luaState);
	lua_setglobal(m_luaState, "__openvibe_box_context");

	lua_setcallback(m_luaState, "get_input_count", lua_get_input_count_cb, this);
	lua_setcallback(m_luaState, "get_output_count", lua_get_output_count_cb, this);
	lua_setcallback(m_luaState, "get_setting_count", lua_get_setting_count_cb, this);
	lua_setcallback(m_luaState, "get_setting", lua_get_setting_cb, this);
	lua_setcallback(m_luaState, "get_config", lua_get_config_cb, this);
	lua_setcallback(m_luaState, "get_current_time", lua_get_current_time_cb, this);
	lua_setcallback(m_luaState, "sleep", lua_sleep_cb, this);
	lua_setcallback(m_luaState, "get_stimulation_count", lua_get_stimulation_count_cb, this);
	lua_setcallback(m_luaState, "get_stimulation", lua_get_stimulation_cb, this);
	lua_setcallback(m_luaState, "remove_stimulation", lua_remove_stimulation_cb, this);
	lua_setcallback(m_luaState, "send_stimulation", lua_send_stimulation_cb, this);
	lua_setcallback(m_luaState, "log", lua_log_cb, this);
	lua_setcallback(m_luaState, "keep_processing", lua_keep_processing_cb, this);
	lua_setcallback(m_luaState, "set_filter_mode", lua_set_filter_mode_cb, this);

	if (!lua_report(this->getLogManager(), m_luaState, luaL_dostring(m_luaState, "function initialize(box) end"))) { return false; }
	if (!lua_report(this->getLogManager(), m_luaState, luaL_dostring(m_luaState, "function uninitialize(box) end"))) { return false; }
	if (!lua_report(this->getLogManager(), m_luaState, luaL_dostring(m_luaState, "function process(box) end"))) { return false; }
	if (!lua_report(this->getLogManager(), m_luaState, luaL_dofile(m_luaState, filename.toASCIIString()))) { return false; }
	if (!lua_report(this->getLogManager(), m_luaState, luaL_dostring(m_luaState, "initialize(__openvibe_box_context)"))) { return false; }

	m_iStimulations.resize(boxContext.getInputCount());
	m_oStimulations.resize(boxContext.getOutputCount());

	for (i = 0; i < boxContext.getInputCount(); ++i)
	{
		TStimulationDecoder<CBoxAlgorithmLuaStimulator>* decoder = new TStimulationDecoder<CBoxAlgorithmLuaStimulator>;
		decoder->initialize(*this, i);

		m_decoders.push_back(decoder);
	}

	for (i = 0; i < boxContext.getOutputCount(); ++i)
	{
		TStimulationEncoder<CBoxAlgorithmLuaStimulator>* encoder = new TStimulationEncoder<CBoxAlgorithmLuaStimulator>;
		encoder->initialize(*this, i);

		m_encoders.push_back(encoder);
	}

	m_lastTime          = 0;
	m_luaThread         = nullptr;
	m_LuaThreadHadError = false;

	return true;
}

bool CBoxAlgorithmLuaStimulator::uninitialize()
{
	if (m_luaThread)
	{
		m_outerLock.lock();

		// If Lua thread is still running, ask it to stop.
		if (m_State == EStates::Processing || m_State == EStates::Please_Quit)
		{
			this->getLogManager() << LogLevel_Debug << "Requesting thread to quit, waiting max 5 secs ...\n";

			m_State = EStates::Please_Quit;
			m_condition.notify_one();

			m_outerLock.unlock();
			m_exitLock.lock();

			bool gotLock = false;
			for (int i = 0; i < 5; ++i)
			{
				// Wait for the thread to stop (in that case it notifies the lock)
				boost::system_time timeout = boost::get_system_time() + boost::posix_time::milliseconds(1000);
				if (m_exitCondition.timed_wait(m_exitLock, timeout))
				{
					gotLock = true;
					break;
				}
				this->getLogManager() << LogLevel_Info << "Waiting for thread to exit, " << i + 1 << "/5 ...\n";
			}
			if (gotLock) { this->getLogManager() << LogLevel_Debug << "Ok, thread notified the exit lock\n"; }
			else { this->getLogManager() << LogLevel_Debug << "Bad, thread did not notify the exit lock in 5s\n"; }

			m_exitLock.unlock();
			m_outerLock.lock();
		}

		if (m_State == EStates::Finished)
		{
			this->getLogManager() << LogLevel_Debug << "Ok, thread reached Finished as expected ...\n";
			m_luaThread->join();
			delete m_luaThread;
			m_luaThread = nullptr;
		}
		else { this->getLogManager() << LogLevel_Warning << "Bad, thread still in state " << toString(m_State) << ", cannot delete. Memory leak.\n"; }

		m_outerLock.unlock();
	}

	if (m_luaState)
	{
		if (!lua_report(this->getLogManager(), m_luaState, luaL_dostring(m_luaState, "uninitialize(__openvibe_box_context)")))
		{
			this->getLogManager() << LogLevel_Warning << "Lua script uninitialize failed\n";
			// Don't return here on false, still want to free the resources below
		}
		lua_close(m_luaState);
		m_luaState = nullptr;
	}
	/* TODO: Remove next comment*/
	/*IBox& staticBoxContext=*/
	this->getStaticBoxContext();

	for (size_t i = 0; i < m_decoders.size(); ++i)
	{
		m_decoders[i]->uninitialize();
		delete m_decoders[i];
	}
	m_decoders.clear();

	for (size_t i = 0; i < m_encoders.size(); ++i)
	{
		m_encoders[i]->uninitialize();
		delete m_encoders[i];
	}
	m_encoders.clear();


	return true;
}

bool CBoxAlgorithmLuaStimulator::process()
{
	IBoxIO& boxContext  = this->getDynamicBoxContext();
	const size_t nInput = this->getStaticBoxContext().getInputCount();
	const uint64_t time = this->getPlayerContext().getCurrentTime();

	// Default time range for the generated stimulation chunk
	uint64_t start = (m_FilterMode ? std::numeric_limits<uint64_t>::max() : m_lastTime);
	uint64_t end   = (m_FilterMode ? 0 : time);

	if (time == 0)
	{
		// Send all headers
		for (size_t i = 0; i < m_encoders.size(); ++i)
		{
			m_encoders[i]->encodeHeader();
			boxContext.markOutputAsReadyToSend(size_t(i), 0, 0);
		}
	}

	for (size_t i = 0; i < nInput; ++i)
	{
		for (size_t j = 0; j < boxContext.getInputChunkCount(i); ++j)
		{
			m_decoders[i]->decode(j);
			if (m_decoders[i]->isHeaderReceived()) { }
			if (m_decoders[i]->isBufferReceived())
			{
				const IStimulationSet* stimSet = m_decoders[i]->getOutputStimulationSet();

				for (size_t k = 0; k < stimSet->getStimulationCount(); ++k)
				{
					m_iStimulations[i].insert(std::make_pair(stimSet->getStimulationDate(k),
															 std::make_pair(stimSet->getStimulationIdentifier(k),
																			stimSet->getStimulationDuration(k))));
				}

				if (m_FilterMode)
				{
					// In this mode, the output chunk time range contains all the current input chunk time ranges
					start = std::min<uint64_t>(start, boxContext.getInputChunkStartTime(i, j));
					end   = std::max<uint64_t>(end, boxContext.getInputChunkEndTime(i, j));
					if (start < m_lastTime)
					{
						this->getLogManager() << LogLevel_Warning << "Earliest current input chunk start time "
								<< CTime(start) << " is older than the last sent block end time "
								<< CTime(m_lastTime) << "\n";
					}
				}
			}
			if (m_decoders[i]->isEndReceived()) { }
			boxContext.markInputAsDeprecated(i, j);
		}
	}

	runLuaThread();

	// Send unless endtime=0 (then either we're at header time, or no chunks were received in filter mode)
	if (end > 0) { sendStimulations(start, end); }

	if (!m_luaThread && m_LuaThreadHadError)
	{
		// Lua thread has exit, so it was ok to check m_bLuaThreadHadError without a lock
		return false;
	}

	return true;
}

bool CBoxAlgorithmLuaStimulator::runLuaThread()
{
	m_outerLock.lock();

	// Executes one step of the thread
	switch (m_State)
	{
		case EStates::Unstarted:
			m_State = EStates::Processing;
			m_luaThread = new boost::thread(CLuaThread(this));
			m_condition.wait(m_outerLock);
			break;

		case EStates::Processing:
			m_condition.notify_one();
			m_condition.wait(m_outerLock);
			break;

		case EStates::Please_Quit:
			// Shouldn't happen as only uninitialize() sets this
			break;

		case EStates::Finished:
			break;

		default:
			break;
	}

	// Executes one step of the box once the thread has finished
	if (m_luaThread)
	{
		switch (m_State)
		{
			case EStates::Unstarted:
				break; // This should never happen

			case EStates::Processing:
				break;

			case EStates::Please_Quit:
				// Shouldn't happen as only uninitialize() sets this
				break;

			case EStates::Finished:
				if (m_luaThread)
				{
					m_luaThread->join();
					delete m_luaThread;
					m_luaThread = nullptr;
					this->getLogManager() << LogLevel_Info << "Lua script terminated\n";
				}
				break;

			default:
				break;
		}
	}

	m_outerLock.unlock();

	return true;
}

bool CBoxAlgorithmLuaStimulator::sendStimulations(const uint64_t startTime, const uint64_t endTime)
{
	IBoxIO& boxContext   = this->getDynamicBoxContext();
	const size_t nOutput = this->getStaticBoxContext().getOutputCount();

	for (size_t i = 0; i < nOutput; ++i)
	{
		IStimulationSet* stimSet = m_encoders[i]->getInputStimulationSet();
		stimSet->clear();

		auto it = m_oStimulations[i].begin();
		while (it != m_oStimulations[i].end() && it->first < endTime)
		{
			const auto itStim = it;
			++it;

			stimSet->appendStimulation(itStim->second.first, itStim->first, itStim->second.second);
			this->getLogManager() << LogLevel_Debug << "On output " << i << " - should send stimulation " << itStim->second.first << " at date "
					<< CTime(itStim->first) << " with duration " << itStim->second.second << "\n";

			m_oStimulations[i].erase(itStim);
		}

		m_encoders[i]->encodeBuffer();

		boxContext.markOutputAsReadyToSend(i, startTime, endTime);
	}

	m_lastTime = endTime;

	return true;
}

bool CBoxAlgorithmLuaStimulator::waitForStimulation(const size_t inputIdx, const size_t stimIdx)
{
	if (inputIdx >= m_iStimulations.size())
	{
		this->getLogManager() << LogLevel_ImportantWarning << "Input " << (inputIdx + 1) << " does not exist\n";
		return false;
	}

	while (m_iStimulations[inputIdx].size() <= stimIdx)
	{
		switch (m_State)
		{
			case EStates::Unstarted: return false; // this should never happen
			case EStates::Finished: return false;
			case EStates::Please_Quit: return false;
			case EStates::Processing: this->sleepCB();
				break;
			default: break;
		}
	}

	return true;
}

bool CBoxAlgorithmLuaStimulator::getInputCountCB(size_t& count)
{
	count = this->getStaticBoxContext().getInputCount();
	return true;
}

bool CBoxAlgorithmLuaStimulator::getOutputCountCB(size_t& count)
{
	count = this->getStaticBoxContext().getOutputCount();
	return true;
}

bool CBoxAlgorithmLuaStimulator::getSettingCountCB(size_t& count)
{
	count = this->getStaticBoxContext().getSettingCount();
	return true;
}

bool CBoxAlgorithmLuaStimulator::getSettingCB(const size_t index, CString& setting)
{
	if (index >= this->getStaticBoxContext().getSettingCount())
	{
		this->getLogManager() << LogLevel_Warning << "Setting " << (index + 1) << " does not exist\n";
		setting = CString("");
		return true;
	}

	setting = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), index);

	return true;
}

bool CBoxAlgorithmLuaStimulator::getConfigCB(const CString& str, CString& config)
{
	config = this->getConfigurationManager().expand(str);
	return true;
}

bool CBoxAlgorithmLuaStimulator::getCurrentTimeCB(uint64_t& time)
{
	time = this->getPlayerContext().getCurrentTime();
	return true;
}

bool CBoxAlgorithmLuaStimulator::sleepCB()
{
	m_condition.notify_one();
	m_condition.wait(m_innerLock);
	return true;
}

bool CBoxAlgorithmLuaStimulator::getStimulationCountCB(const size_t index, size_t& count)
{
	if (index >= m_iStimulations.size())
	{
		this->getLogManager() << LogLevel_Warning << "Input " << (index + 1) << " does not exist\n";
		count = 0;
		return true;
	}

	count = size_t(m_iStimulations[index].size());
	return true;
}

bool CBoxAlgorithmLuaStimulator::getStimulationCB(const size_t inputIdx, const size_t stimIdx, uint64_t& id, uint64_t& time, uint64_t& duration)
{
	if (!this->waitForStimulation(inputIdx, stimIdx))
	{
		id       = uint64_t(-1);
		time     = uint64_t(-1);
		duration = uint64_t(-1);
		return true;
	}

	auto it = m_iStimulations[inputIdx].begin();

	for (size_t i = 0; i < stimIdx; i++, ++it) {}

	id       = it->second.first;
	time     = it->first;
	duration = it->second.second;

	this->getLogManager() << LogLevel_Debug << "getStimulationCB " << (inputIdx + 1) << " " << (stimIdx + 1) << " " << id << " " << time << " " << duration
			<< "\n";

	return true;
}

bool CBoxAlgorithmLuaStimulator::removeStimulationCB(const size_t inputIdx, const size_t stimIdx)
{
	if (!this->waitForStimulation(inputIdx, stimIdx)) { return true; }

	auto it = m_iStimulations[inputIdx].begin();

	for (size_t i = 0; i < stimIdx; i++, ++it) {}

	m_iStimulations[inputIdx].erase(it);

	this->getLogManager() << LogLevel_Debug << "removeStimulationCB " << (inputIdx + 1) << " " << (stimIdx + 1) << "\n";

	return true;
}

bool CBoxAlgorithmLuaStimulator::sendStimulationCB(const size_t outputIdx, uint64_t id, uint64_t time, uint64_t duration)
{
	if (outputIdx < m_oStimulations.size())
	{
		if (time >= m_lastTime)
		{
			this->getLogManager() << LogLevel_Debug << "sendStimulationCB " << (outputIdx + 1) << " " << id << " " << time << " " << duration << "\n";
			m_oStimulations[outputIdx].insert(std::make_pair(time, std::make_pair(id, duration)));
		}
		else
		{
			this->getLogManager() << LogLevel_ImportantWarning << "Ignored outdated stimulation " << id << " " << CTime(time) << " " << CTime(duration)
					<< " sent on output " << (outputIdx + 1) << " - older than last chunk end time " << CTime(m_lastTime) << "\n";
		}
	}
	else
	{
		this->getLogManager() << LogLevel_ImportantWarning << "Ignored stimulation " << id << " " << CTime(time) << " " << CTime(duration)
				<< " sent on unexistant output " << (outputIdx + 1) << "\n";
	}
	return true;
}

bool CBoxAlgorithmLuaStimulator::log(const ELogLevel level, const CString& text)
{
	this->getLogManager() << level << "<" << LogColor_PushStateBit << LogColor_ForegroundGreen
			<< "In Script" << LogColor_PopStateBit << "> " << text.toASCIIString() << "\n";
	return true;
}

void CBoxAlgorithmLuaStimulator::doThread()
{
	m_innerLock.lock();
	if (!lua_report(this->getLogManager(), m_luaState, luaL_dostring(m_luaState, "process(__openvibe_box_context)"))) { m_LuaThreadHadError = true; }
	m_State = EStates::Finished;
	m_innerLock.unlock();

	m_condition.notify_one();
	m_exitCondition.notify_one();
}

std::string CBoxAlgorithmLuaStimulator::toString(const EStates state)
{
	switch (state)
	{
		case EStates::Unstarted: return "Unstarted";
		case EStates::Processing: return "Processing";
		case EStates::Please_Quit: return "Please_Quit";
		case EStates::Finished: return "Finished";
		default: return "Invalid";
	}
}

#endif // TARGET_HAS_ThirdPartyLua
