///-------------------------------------------------------------------------------------------------
/// 
/// \file CBoxAlgorithmStimulationValidator.hpp
/// \brief Classes of the Box Stimulation Validator.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date Mon Feb 17 10:42:46 2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------
#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE
{
	namespace Plugins
	{
		namespace Stimulation
		{
			/// <summary> The class CBoxAlgorithmStimulationValidator describes the box Stimulation Validator. </summary>
			class CBoxAlgorithmStimulationValidator final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
			{
			public:
				void release() override { delete this; }

				bool initialize() override;
				bool uninitialize() override;

				//Here is the different process callbacks possible
				// - On clock ticks :
				//bool processClock(Kernel::CMessageClock& msg) override;		
				// - On new input received (the most common behaviour for signal processing) :
				bool processInput(const size_t index) override;

				// If you want to use processClock, you must provide the clock frequency.
				//uint64_t getClockFrequency() override;

				bool process() override;

				// As we do with any class in openvibe, we use the macro below to associate this box to an unique identifier. 
				// The inheritance information is also made available, as we provide the superclass Toolkit::TBoxAlgorithm < IBoxAlgorithm >
				_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_StimulationValidator)

			protected:
				//***** Codecs *****
				Toolkit::TStimulationDecoder<CBoxAlgorithmStimulationValidator> m_decoder;	// Stimulation Decoder
				Toolkit::TStimulationEncoder<CBoxAlgorithmStimulationValidator> m_encoder;	// Stimulation Encoder
				IStimulationSet *m_iStimulation = nullptr, *m_oStimulation = nullptr;		// Stimulation Receiver

				//***** Settings *****
				CIdentifier m_stim = OVTK_StimulationId_Label_00;
				size_t m_count = 0, m_limit = 0;
				uint64_t m_lastEndTime = 0;
			};



			/// <summary> Descriptor of the box Stimulation Validator. </summary>
			class CBoxAlgorithmStimulationValidatorDesc final : virtual public IBoxAlgorithmDesc
			{
			public:

				void release() override { }

				CString getName() const override { return CString("Stimulation Validator"); }
				CString getAuthorName() const override { return CString("Thibaut Monseigne"); }
				CString getAuthorCompanyName() const override { return CString("Inria"); }
				CString getShortDescription() const override { return CString("Count a stimulation and send this after N occurence."); }
				CString getDetailedDescription() const override { return CString(""); }
				CString getCategory() const override { return CString("Stimulation"); }
				CString getVersion() const override { return CString("1.0"); }
				CString getStockItemName() const override { return CString("gtk-add"); }

				CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_StimulationValidator; }
				IPluginObject* create() override { return new CBoxAlgorithmStimulationValidator; }

				/*
				IBoxListener* createBoxListener() const override { return new CBoxAlgorithmStimulationValidatorListener; }
				void releaseBoxListener(IBoxListener* listener) const override { delete listener; }
				*/
				bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
				{
					prototype.addInput("Input",OV_TypeId_Stimulations);
					prototype.addOutput("Output",OV_TypeId_Stimulations);
					prototype.addSetting("Stimulation to count",OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
					prototype.addSetting("Number for validation",OV_TypeId_Integer, "2");
					prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
					return true;
				}

				_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_StimulationValidatorDesc)
			};
		}  // namespace Stimulation
	}  // namespace Plugins
}  // namespace OpenViBE
