#include "ovp_defines.h"

#include "box-algorithms/ovpCKeyboardStimulator.h"
#include "box-algorithms/ovpCSignChangeDetector.h"

#include "box-algorithms/ovpCBoxAlgorithmRunCommand.h"
#if defined TARGET_HAS_ThirdPartyLua
#include "box-algorithms/ovpCBoxAlgorithmLuaStimulator.h"
#endif // TARGET_HAS_ThirdPartyLua
#if defined TARGET_HAS_ThirdPartyOpenAL
#include "box-algorithms/ovpCBoxAlgorithmOpenALSoundPlayer.h"
#endif // TARGET_HAS_ThirdPartyOpenAL

#include "box-algorithms/adaptation/ovpCBoxAlgorithmStimulationFilter.h"

#include "box-algorithms/ovpCBoxAlgorithmP300SpellerStimulator.h"
#include "box-algorithms/ovpCBoxAlgorithmP300IdentifierStimulator.h"
#include "box-algorithms/CBoxAlgorithmStimulationValidator.hpp"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::Plugins::*/Stimulation;

OVP_Declare_Begin()
	context.getTypeManager().registerEnumerationEntry(OV_TypeId_BoxAlgorithmFlag, OV_AttributeId_Box_FlagIsUnstable.toString(),
													  OV_AttributeId_Box_FlagIsUnstable.toUInteger());
	context.getTypeManager().registerEnumerationType(OVP_TypeId_StimulationFilterAction, "Stimulation Filter Action");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_StimulationFilterAction, "Select", OVP_TypeId_StimulationFilterAction_Select.toUInteger());
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_StimulationFilterAction, "Reject", OVP_TypeId_StimulationFilterAction_Reject.toUInteger());

#if defined(TARGET_HAS_ThirdPartyGTK)
	OVP_Declare_New(CKeyboardStimulatorDesc);
#endif

	OVP_Declare_New(CSignChangeDetectorDesc);

	OVP_Declare_New(CBoxAlgorithmRunCommandDesc);
#if defined TARGET_HAS_ThirdPartyLua
	OVP_Declare_New(CBoxAlgorithmLuaStimulatorDesc);
#endif // TARGET_HAS_ThirdPartyLua

#if defined TARGET_HAS_ThirdPartyOpenAL
	OVP_Declare_New(CBoxAlgorithmOpenALSoundPlayerDesc);
#endif // TARGET_HAS_ThirdPartyOpenAL

	OVP_Declare_New(CBoxAlgorithmStimulationFilterDesc);

	OVP_Declare_New(CBoxAlgorithmP300SpellerStimulatorDesc);
	OVP_Declare_New(CBoxAlgorithmP300IdentifierStimulatorDesc);

	OVP_Declare_New(CBoxAlgorithmStimulationValidatorDesc);

	context.getTypeManager().registerEnumerationType(OVP_TypeId_Voting_ClearVotes, "Clear votes");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_Voting_ClearVotes, "When expires", OVP_TypeId_Voting_ClearVotes_WhenExpires.toUInteger());
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_Voting_ClearVotes, "After output", OVP_TypeId_Voting_ClearVotes_AfterOutput.toUInteger());
	context.getTypeManager().registerEnumerationType(OVP_TypeId_Voting_OutputTime, "Output time");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_Voting_OutputTime, "Time of voting", OVP_TypeId_Voting_OutputTime_Vote.toUInteger());
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_Voting_OutputTime, "Time of last winning stimulus",
													  OVP_TypeId_Voting_OutputTime_Winner.toUInteger());
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_Voting_OutputTime, "Time of last voting stimulus",
													  OVP_TypeId_Voting_OutputTime_Last.toUInteger());
	context.getTypeManager().registerEnumerationType(OVP_TypeId_Voting_RejectClass_CanWin, "Reject can win");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_Voting_RejectClass_CanWin, "Yes", OVP_TypeId_Voting_RejectClass_CanWin_Yes.toUInteger());
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_Voting_RejectClass_CanWin, "No", OVP_TypeId_Voting_RejectClass_CanWin_No.toUInteger());

OVP_Declare_End()
