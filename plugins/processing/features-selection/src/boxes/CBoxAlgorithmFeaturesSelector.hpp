///-------------------------------------------------------------------------------------------------
/// 
/// \file CBoxAlgorithmFeaturesSelector.hpp
/// \brief Classes of the Box Features Selector.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/02/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------
#pragma once

#include "ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include "algorithm/CMRMR.hpp"
#include <vector>

namespace OpenViBE {
namespace Plugins {
namespace FeaturesSelection {
/// <summary>	The class CBoxAlgorithmFeaturesSelector describes the box Features Selector. </summary>
class CBoxAlgorithmFeaturesSelector final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_FeaturesSelector)

protected:
	//***** Codecs *****
	Toolkit::TFeatureVectorDecoder<CBoxAlgorithmFeaturesSelector> m_decoder;
	Toolkit::TFeatureVectorEncoder<CBoxAlgorithmFeaturesSelector> m_encoder;
	IMatrix *m_iMatrix = nullptr, *m_oMatrix = nullptr;

	//***** Settings *****
	std::vector<size_t> m_lookup;

	/// <summary> Parse the setting. </summary>
	/// <param name="setting"> Setting to parse. </param>
	/// <returns> True if Setting is correctly parsed.</returns>
	bool parseSetting(const std::string& setting);
};

/// <summary> Descriptor of the box Features Selector. </summary>
class CBoxAlgorithmFeaturesSelectorDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Features Selector"); }
	CString getAuthorName() const override { return CString("Thibaut Monseigne"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Select a subset of features vector."); }
	CString getDetailedDescription() const override { return CString("Select Features with index starting from 0."); }
	CString getCategory() const override { return CString("Features Selection"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-sort-ascending"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_FeaturesSelector; }
	IPluginObject* create() override { return new CBoxAlgorithmFeaturesSelector; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input", OV_TypeId_FeatureVector);
		prototype.addOutput("Output", OV_TypeId_FeatureVector);
		prototype.addSetting("Features List", OV_TypeId_String, ":");
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_FeaturesSelectorDesc)
};
}  // namespace FeaturesSelection
}  // namespace Plugins
}  // namespace OpenViBE
