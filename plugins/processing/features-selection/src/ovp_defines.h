///-------------------------------------------------------------------------------------------------
/// 
/// \file ovp_defines.h
/// \brief Defines list for Setting, Shortcut Macro and const.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 12/02/2020.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once
#include <string>

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_FeaturesSelection			OpenViBE::CIdentifier(0xee36249f, 0x22a32e6e)
#define OVP_ClassId_BoxAlgorithm_FeaturesSelectionDesc		OpenViBE::CIdentifier(0xee36249f, 0x22a32e6f)
#define OVP_ClassId_BoxAlgorithm_FeaturesSelector			OpenViBE::CIdentifier(0xee36249f, 0x22a32e7e)
#define OVP_ClassId_BoxAlgorithm_FeaturesSelectorDesc		OpenViBE::CIdentifier(0xee36249f, 0x22a32e7f)

// Types Lists
//---------------------------------------------------------------------------------------------------
#define OVP_TypeId_Features_Selection_Method				OpenViBE::CIdentifier(0x5261636B, 0x46534D45)
#define OVP_TypeId_mRMR_Method								OpenViBE::CIdentifier(0x5261636B, 0x6D524D52)

enum class EFeatureSelection { MRMR };

//inline std::string toString(const EFeatureSelection& e) { switch (e) { default: return "mRMR (minimum Redundancy Maximum Relevance)"; } }
inline std::string toString(const EFeatureSelection& /*e*/) { return "mRMR (minimum Redundancy Maximum Relevance)"; }
