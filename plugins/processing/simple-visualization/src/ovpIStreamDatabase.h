#pragma once

#include <openvibe/ov_all.h>

namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
class IStreamDisplayDrawable;

/**
 * \brief Abstract class of objects than can be updated by an IStreamDatabase object
 */
class IStreamDisplayDrawable
{
public:
	virtual bool init() = 0;
	virtual bool redraw() = 0;
	virtual ~IStreamDisplayDrawable() = default;
};

class IStreamDatabase
{
public:
	virtual ~IStreamDatabase() = default;

	/**
	 * \brief Initialize the database, including creating decoder
	 * \return True if initialization succeeded
	 */
	virtual bool initialize() = 0;

	/**
	 * \brief Set drawable object to update.
	 * \param drawable drawable object to update.
	 */
	virtual void setDrawable(IStreamDisplayDrawable* drawable) = 0;

	/**
	 * \brief Set flag stating whether or not to redraw upon new data reception
	 * \param redrawOnNewData Redraw flag
	 */
	virtual void setRedrawOnNewData(const bool redrawOnNewData) = 0;

	/**
	 * \brief Determine whether first buffer has been received yet
	 * \return True if first buffer has been received already, false otherwise
	 */
	virtual bool hasFirstBuffer() = 0;

	/**
	 * \brief Set max buffer count
	 * Set max buffer count directly (as opposed to computing it from time scale)
	 * \remarks This method sets m_ignoreTimeScale to true
	 */
	virtual bool setMaxBufferCount(const size_t count) = 0;

	/**
	 * \brief Set time scale
	 * Computes the maximum number of buffers that can be displayed simultaneously
	 * \remarks This method sets m_ignoreTimeScale to false
	 * \param timeScale Time window's width in seconds.
	 * \return True if buffer count changed, false otherwise
	 */
	virtual bool setTimeScale(const double timeScale) = 0;

	/**
	 * \brief Decode a memory buffer using proxy
	 * \param buffer Memory buffer to decode
	 * \param startTime Start time of memory buffer
	 * \param endTime End time of memory buffer
	 * \return True if memory buffer could be properly decoded, false otherwise
	 */
	virtual bool decodeMemoryBuffer(const IMemoryBuffer* buffer, const uint64_t startTime, const uint64_t endTime) = 0;

	/**
	 * \brief Get number of buffers necessary to cover time scale
	 * \remarks Can't be computed before 2 buffers have been received, because
	 * the time step between the start of 2 consecutive buffers must be known
	 * \return Maximum number of buffers stored in this object
	 */
	virtual size_t getMaxBufferCount() = 0;

	/**
	 * \brief Get current buffer count
	 * \return Current buffer count
	 */
	virtual size_t getCurrentBufferCount() = 0;

	/**
	 * \brief Get pointer on a given buffer
	 * \param index Index of buffer to retrieve
	 * \return Buffer pointer if buffer exists, nullptr otherwise
	 */
	virtual const double* getBuffer(const size_t index) = 0;

	/**
	 * \brief Get start time of a given buffer
	 * \param index Index of buffer whose start time is to be retrieved
	 * \return Start time if buffer exists, 0 otherwise
	 */
	virtual uint64_t getStartTime(const size_t index) = 0;

	/**
	 * \brief Get end time of a given buffer
	 * \param index Index of buffer whose end time is to be retrieved
	 * \return End time if buffer exists, 0 otherwise
	 */
	virtual uint64_t getEndTime(const size_t index) = 0;

	/**
	 * \brief Get number of elements contained in a buffer
	 * \return Buffer element count or 0 if no buffer has been received yet
	 */
	virtual size_t getBufferElementCount() = 0;

	/**
	 * \brief Get time span covered by a buffer
	 * \return Buffer time span
	 */
	virtual uint64_t getBufferDuration() = 0;

	/**
	 * \brief Determine whether buffer time step has been computed yet
	 * \return True if buffer time step has been computed
	 */
	virtual bool isBufferTimeStepComputed() = 0;

	/**
	 * \brief Get time step between the start of 2 consecutive buffers
	 * \remarks This value can't be computed before the first 2 buffers are received
	 * \return Buffer time step
	 */
	virtual uint64_t getBufferTimeStep() = 0;

	/**
	 * \brief Get number of samples per buffer
	 * \return Number of samples per buffer
	 */
	virtual size_t getSampleCountPerBuffer() = 0;

	/**
	 * \brief Get number of channels
	 * \return Number of channels
	 */
	virtual size_t getChannelCount() = 0;

	/**
	 * \brief Get channel label
	 * \param[in] index index of channel
	 * \param[out] label channel label
	 * \return true if channel label could be retrieved, false otherwise
	 */
	virtual bool getChannelLabel(const size_t index, CString& label) = 0;

	/** \name Min/max values retrieval */
	//@{

	/**
	 * \brief Compute min/max values currently displayed for a given channel
	 * \param [in] index Index of channel
	 * \param [out] min Minimum displayed value for channel of interest
	 * \param [out] max Maximum displayed value for channel of interest
	 * \return True if values could be computed, false otherwise
	 */
	virtual bool getChannelMinMaxValues(const size_t index, double& min, double& max) = 0;

	/**
	 * \brief Compute min/max values currently displayed, taking all channels into account
	 * \param [out] min Minimum displayed value
	 * \param [out] max Maximum displayed value
	 * \return True if values could be computed, false otherwise
	 */
	virtual bool getGlobalMinMaxValues(double& min, double& max) = 0;

	/**
	 * \brief Compute min/max values in last buffer for a given channel
	 * \param [in] index Index of channel
	 * \param [out] min Minimum value for channel of interest
	 * \param [out] max Maximum value for channel of interest
	 * \return True if values could be computed, false otherwise
	 */
	virtual bool getLastBufferChannelMinMaxValues(const size_t index, double& min, double& max) = 0;

	/**
	 * \brief Compute min/max values in last buffer, taking all channels into account
	 * \param [out] min Minimum value
	 * \param [out] max Maximum value
	 * \return True if values could be computed, false otherwise
	 */
	virtual bool getLastBufferGlobalMinMaxValues(double& min, double& max) = 0;

	//@}
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
