#pragma once

#include "ovpCBufferDatabase.h"
#include <gtk/gtk.h>
#include <openvibe/ov_all.h>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
class CBufferDatabase;

/**
 * Used to display an horizontal temporal ruler.
 * Uses information fetched from a signal database object.
 * \param database Object from which to fetch time data
 * \param width Width to be requested by widget
 * \param height Height to be requested by widget
 */
class CBottomTimeRuler
{
public:
	CBottomTimeRuler(CBufferDatabase& database, int width, int height);
	~CBottomTimeRuler() = default;

	//! returns the widget, so it can be added to the main interface
	GtkWidget* getWidget() const { return m_bottomRuler; }

	//! draws the ruler
	void draw();

	/**
	 * \brief Resize this ruler when the widget passed in parameter is resized
	 * \param widget Widget whose width is matched by this widget
	 */
	void linkWidthToWidget(GtkWidget* widget);

	//! in scan mode, leftmost displayed time is not always the one of the oldest buffer
	void setLeftmostDisplayedTime(const uint64_t time) { m_leftmostDisplayedTime = time; }

	/**
	 * \brief Callback notified upon resize events
	 * \param width New window width
	 * \param height New window height
	 */
	void onResizeEventCB(gint width, gint height) const;

private:
	//! Draw ruler
	void drawRuler(int64_t baseX, int rulerWidth, double startTime, double endTime, double length, double baseValue, double valueStep, int clipLeft,
				   int clipRight);

	//! Gtk widget
	GtkWidget* m_bottomRuler = nullptr;
	//! Signal database from which time information is retrieved
	CBufferDatabase* m_database = nullptr;
	//! Height request
	int m_height = 0;
	//! Space allocated per label
	uint64_t m_pixelsPerLabel = 20;
	//! When in scan mode, current leftmost displayed time
	uint64_t m_leftmostDisplayedTime = 0;
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
