#pragma once

#include "ovpIStreamDatabase.h"
#include <openvibe/ov_all.h>
#include <gtk/gtk.h>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
/**
 * Displays a time ruler.
 * Uses information fetched from a stream database object.
 */
class CTimeRuler
{
public:
	/**
	 * \brief Constructor
	 * \param streamDatabase Object from which data is retrieved
	 * \param width Width to be requested by widget
	 * \param height Height to be requested by widget
	 */
	CTimeRuler(IStreamDatabase& streamDatabase, int width, int height);

	/**
	 * \brief Destructor
	 */
	~CTimeRuler() = default;

	/**
	 * \brief Get widget handled by this object
	 * \return Gtk widget
	 */
	GtkWidget* getWidget() const { return m_widget; }

	/**
	 * \brief Toggle ruler on/off
	 * \param active Activation flag
	 */
	void toggle(const bool active) const { active ? gtk_widget_show(m_widget) : gtk_widget_hide(m_widget); }

	/**
	 * \brief Draw ruler
	 */
	void draw();

	/**
	 * \brief Link ruler width to another widget's
	 * \param widget Widget whose width must be matched by this object
	 */
	void linkWidthToWidget(GtkWidget* widget);

	/**
	 * \brief Callback notified upon resize events
	 * \param width New window width
	 * \remarks height is the m_heightRequest
	 */
	void onResizeEventCB(const gint width, gint /*height*/) const { gtk_widget_set_size_request(m_widget, width, m_height); }

private:
	//! Ruler widget
	GtkWidget* m_widget = nullptr;
	//! Database from which stream information is retrieved
	IStreamDatabase& m_stream;
	//! Height request
	int m_height = 0;
	//! Size available per label along the ruler
	uint64_t m_pixelsPerLabel = 20;
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
