#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_SignalDisplay										OpenViBE::CIdentifier(0x0055BE5F, 0x087BDD12)
#define OVP_ClassId_SignalDisplayDesc									OpenViBE::CIdentifier(0x00C4F2D5, 0x58810276)
#define OVP_ClassId_DisplayCueImage										OpenViBE::CIdentifier(0x005789A4, 0x3AB78A36)
#define OVP_ClassId_DisplayCueImageDesc									OpenViBE::CIdentifier(0x086185A4, 0x796A854C)
#define OVP_ClassId_GrazVisualization									OpenViBE::CIdentifier(0x00DD290D, 0x5F142820)
#define OVP_ClassId_GrazVisualizationDesc								OpenViBE::CIdentifier(0x00F1955D, 0x38813A6A)
#define OVP_ClassId_GeneralizedGrazVisualization						OpenViBE::CIdentifier(0xf0d1b4b9, 0xb420c213)
#define OVP_ClassId_GeneralizedGrazVisualizationDesc					OpenViBE::CIdentifier(0x0d414c51, 0xb0ed32f2)
#define OVP_ClassId_PowerSpectrumDisplay								OpenViBE::CIdentifier(0x004C0EA4, 0x713EC6D9)
#define OVP_ClassId_PowerSpectrumDisplayDesc							OpenViBE::CIdentifier(0x00116B40, 0x69E1B00D)
#define OVP_ClassId_TopographicMap2DDisplay								OpenViBE::CIdentifier(0x0B104632, 0x451C265F)
#define OVP_ClassId_TopographicMap2DDisplayDesc							OpenViBE::CIdentifier(0x7154037A, 0x4BC52A9F)
#define OVP_ClassId_Simple3DDisplay										OpenViBE::CIdentifier(0x31A00483, 0x35924E6B)
#define OVP_ClassId_Simple3DDisplayDesc									OpenViBE::CIdentifier(0x443E145F, 0x77205DA0)
#define OVP_ClassId_TopographicMap3DDisplay								OpenViBE::CIdentifier(0x36F95BE4, 0x0EF06290)
#define OVP_ClassId_TopographicMap3DDisplayDesc							OpenViBE::CIdentifier(0x6AD52C48, 0x6E1C1746)
#define OVP_ClassId_VoxelDisplay										OpenViBE::CIdentifier(0x76E42EA2, 0x66FB5265)
#define OVP_ClassId_VoxelDisplayDesc									OpenViBE::CIdentifier(0x79321659, 0x642D3D0C)
#define OVP_ClassId_TimeFrequencyMapDisplay								OpenViBE::CIdentifier(0x3AE63330, 0x76532117)
#define OVP_ClassId_TimeFrequencyMapDisplayDesc							OpenViBE::CIdentifier(0x1BAE74F3, 0x20FB7C89)
#define OVP_ClassId_BoxAlgorithm_P300SpellerVisualization				OpenViBE::CIdentifier(0x195E41D6, 0x6E684D47)
#define OVP_ClassId_BoxAlgorithm_P300SpellerVisualizationDesc			OpenViBE::CIdentifier(0x31DE2B0D, 0x028202E7)
#define OVP_ClassId_BoxAlgorithm_P300IdentifierCardVisualization		OpenViBE::CIdentifier(0x3AF7FF20, 0xA68745DB)
#define OVP_ClassId_BoxAlgorithm_P300IdentifierCardVisualizationDesc	OpenViBE::CIdentifier(0x84F146EF, 0x4AA712A4)
#define OVP_ClassId_BoxAlgorithm_P300MagicCardVisualization				OpenViBE::CIdentifier(0x841F46EF, 0x471AA2A4)
#define OVP_ClassId_BoxAlgorithm_P300MagicCardVisualizationDesc			OpenViBE::CIdentifier(0x37FAFF20, 0xA74685DB)
#define OVP_ClassId_BoxAlgorithm_ErpPlot								OpenViBE::CIdentifier(0x10DC6917, 0x2B29B2A0)
#define OVP_ClassId_BoxAlgorithm_ErpPlotDesc							OpenViBE::CIdentifier(0x10DC6917, 0x2B29B2A0)
#define OVP_ClassId_BoxAlgorithm_LevelMeasure							OpenViBE::CIdentifier(0x657138E4, 0x46D6586F)
#define OVP_ClassId_BoxAlgorithm_LevelMeasureDesc						OpenViBE::CIdentifier(0x4D061428, 0x11B02233)
#define OVP_ClassId_Algorithm_LevelMeasure								OpenViBE::CIdentifier(0x63C71764, 0x34A9717F)
#define OVP_ClassId_Algorithm_LevelMeasureDesc							OpenViBE::CIdentifier(0x3EB6754F, 0x22FB1722)

// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines

#define OV_AttributeId_Box_FlagIsUnstable							OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)

#define OVP_TypeId_SphericalLinearInterpolationType					OpenViBE::CIdentifier(0x44B76D9E, 0x618229BC)
#define OVP_TypeId_SignalDisplayMode								OpenViBE::CIdentifier(0x5DE046A6, 0x086340AA)
#define OVP_TypeId_SignalDisplayScaling								OpenViBE::CIdentifier(0x33A30739, 0x00D5299B)
#define OVP_TypeId_FeedbackMode										OpenViBE::CIdentifier(0x5261636B, 0x464d4f44)

enum EInterpolationType { Spline = 1, Laplacian = 2 };

enum ESignalDisplayMode { Scroll, Scan };

enum ESignalDisplayScaling { PerChannel, Global, None };	// Note: the code relies on the following indexing starting from 0

enum EFeedbackMode { Positive, Best, All, No };

#define OVP_Algorithm_LevelMeasure_InputParameterId_Matrix			OpenViBE::CIdentifier(0x59430053, 0x67C23A83)
#define OVP_Algorithm_LevelMeasure_OutputParameterId_MainWidget		OpenViBE::CIdentifier(0x101C4641, 0x466C71E3)
#define OVP_Algorithm_LevelMeasure_OutputParameterId_ToolbarWidget	OpenViBE::CIdentifier(0x14905FFC, 0x6FE425B2)
#define OVP_Algorithm_LevelMeasure_InputTriggerId_Reset				OpenViBE::CIdentifier(0x3EAF36C5, 0x74490C56)
#define OVP_Algorithm_LevelMeasure_InputTriggerId_Refresh			OpenViBE::CIdentifier(0x71356FE4, 0x3E8F62DC)
#define OVP_Algorithm_LevelMeasure_OutputTriggerId_Refreshed		OpenViBE::CIdentifier(0x3C3C1B06, 0x360305D9)

#define OVP_ClassId_Algorithm_SphericalSplineInterpolation			OpenViBE::CIdentifier(0x4F112803, 0x661D4029)
#define OVP_ClassId_Algorithm_SphericalSplineInterpolationDesc		OpenViBE::CIdentifier(0x00D67A20, 0x3D3D4729)

#define OVP_Algorithm_SphericalSplineInterpolation_InputParameterId_SplineOrder					OpenViBE::CIdentifier(0x3B8200F6, 0x205162C7)
#define OVP_Algorithm_SphericalSplineInterpolation_InputParameterId_ControlPointsCount			OpenViBE::CIdentifier(0x2ABF11FC, 0x174A2CFE)
#define OVP_Algorithm_SphericalSplineInterpolation_InputParameterId_ControlPointsCoordinates	OpenViBE::CIdentifier(0x36F743FE, 0x37897AB9)
#define OVP_Algorithm_SphericalSplineInterpolation_InputParameterId_ControlPointsValues			OpenViBE::CIdentifier(0x4EA55599, 0x670274A7)
#define OVP_Algorithm_SphericalSplineInterpolation_InputParameterId_SamplePointsCoordinates		OpenViBE::CIdentifier(0x280A531D, 0x339C18AA)
#define OVP_Algorithm_SphericalSplineInterpolation_OutputParameterId_SamplePointsValues			OpenViBE::CIdentifier(0x12D0319C, 0x51ED4D8B)
#define OVP_Algorithm_SphericalSplineInterpolation_OutputParameterId_MinSamplePointValue		OpenViBE::CIdentifier(0x0CEE2041, 0x79455EED)
#define OVP_Algorithm_SphericalSplineInterpolation_OutputParameterId_MaxSamplePointValue		OpenViBE::CIdentifier(0x1ECB03E3, 0x40EF757F)
#define OVP_Algorithm_SphericalSplineInterpolation_InputTriggerId_PrecomputeTables				OpenViBE::CIdentifier(0x42A650DA, 0x62B35F76)
#define OVP_Algorithm_SphericalSplineInterpolation_InputTriggerId_ComputeSplineCoefs			OpenViBE::CIdentifier(0x5B353712, 0x069F3D3B)
#define OVP_Algorithm_SphericalSplineInterpolation_InputTriggerId_ComputeLaplacianCoefs			OpenViBE::CIdentifier(0x7D8C545E, 0x7C086660)
#define OVP_Algorithm_SphericalSplineInterpolation_InputTriggerId_InterpolateSpline				OpenViBE::CIdentifier(0x1241610E, 0x03CB1AD9)
#define OVP_Algorithm_SphericalSplineInterpolation_InputTriggerId_InterpolateLaplacian			OpenViBE::CIdentifier(0x11CE0AC3, 0x0FD85469)
#define OVP_Algorithm_SphericalSplineInterpolation_OutputTriggerId_Error						OpenViBE::CIdentifier(0x08CB0679, 0x3A6F3C3A)


// Some enumerations
//---------------------------------------------------------------------------------------------------
enum EDisplayMode { ZoomIn, ZoomOut, GlobalBestFit };

namespace OpenViBE {
namespace Kernel {
/**
* \brief Standard 3D objects
*/
enum EStandard3DObject
{
	Standard3DObject_Invalid = -1,
	Standard3DObject_Sphere,
	Standard3DObject_Cone,
	Standard3DObject_Cube,
	Standard3DObject_Quad
};
}	// namespace Kernel
}	// namespace OpenViBE
