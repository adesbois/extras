#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <gtk/gtk.h>
#include <map>
#include "../utils.h"


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
class CBoxAlgorithmP300IdentifierCardVisualization final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_P300IdentifierCardVisualization)

private:

	using widget_style_t = struct
	{
		int index;
		GdkColor bgColor;
		GtkWidget* parent;
		GtkWidget* widget;
		GtkWidget* image;
	};

	typedef void (CBoxAlgorithmP300IdentifierCardVisualization::*cache_callback)(widget_style_t& style, void* data);

	void cacheBuildFromTable(GtkTable* table);
	void cacheForEach(cache_callback callback, void* data);
	void cacheForEachIf(int card, cache_callback ifCB, cache_callback elseCB, void* ifUserData, void* elseUserData);
	void cacheChangeNullCB(widget_style_t& style, void* data);
	void cacheChangeImageCB(widget_style_t& style, void* data);
	void cacheChangeBackgroundCB(widget_style_t& style, void* data);

protected:

	CString m_interfaceFilename;
	uint64_t m_cardStimulationBase = 0;

private:

	Kernel::IAlgorithmProxy* m_sequenceStimulationDecoder       = nullptr;
	Kernel::IAlgorithmProxy* m_targetStimulationDecoder         = nullptr;
	Kernel::IAlgorithmProxy* m_targetFlaggingStimulationEncoder = nullptr;
	Kernel::IAlgorithmProxy* m_cardSelectionStimulationDecoder  = nullptr;
	Kernel::TParameterHandler<const IMemoryBuffer*> m_sequenceMemoryBuffer;
	Kernel::TParameterHandler<const IMemoryBuffer*> m_targetMemoryBuffer;
	Kernel::TParameterHandler<const IStimulationSet*> m_targetFlaggingStimulationSet;
	Kernel::TParameterHandler<IStimulationSet*> m_sequenceStimulationSet;
	Kernel::TParameterHandler<IStimulationSet*> m_targetStimulationSet;
	Kernel::TParameterHandler<IMemoryBuffer*> m_targetFlaggingMemoryBuffer;
	uint64_t m_lastTime = 0;

	GtkBuilder* m_mainWidgetInterface = nullptr;
	//GtkBuilder* m_toolbarWidgetInterface = nullptr;
	GtkWidget* m_mainWindow = nullptr;
	//GtkWidget* m_toolbarWidget = nullptr;
	GtkTable* m_table  = nullptr;
	GtkLabel* m_result = nullptr;
	//GtkLabel* m_target = nullptr;
	GdkColor m_bgColor         = InitGDKColor(0, 0, 0, 0);
	GdkColor m_targetBgColor   = InitGDKColor(0, 6554, 26214, 6554);
	GdkColor m_selectedBgColor = InitGDKColor(0, 45875, 13107, 13107);
	GtkLabel* m_targetLabel    = nullptr;
	GtkLabel* m_selectedLabel  = nullptr;
	uint64_t m_nCard           = 0;

	int m_targetCard = 0;

	std::vector<GtkWidget*> m_fgImageTargets;
	std::vector<GtkWidget*> m_fgImageWorks;
	std::vector<GtkWidget*> m_fgImageResults;
	GtkWidget* m_bgImageTarget = nullptr;
	GtkWidget* m_bgImageWork   = nullptr;
	GtkWidget* m_bgImageResult = nullptr;

	bool m_tableInitialized = false;

	std::vector<widget_style_t> m_caches;

	VisualizationToolkit::IVisualizationContext* m_visualizationCtx = nullptr;
};

class CBoxAlgorithmP300IdentifierCardVisualizationDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("P300 Identifier Card Visualization"); }
	CString getAuthorName() const override { return CString("Baptiste Payan"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }

	CString getShortDescription() const override
	{
		return CString("Displays images to the user based on received stimulations with some additional P300 related functionality");
	}

	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Visualization/Presentation"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-select-font"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_P300IdentifierCardVisualization; }
	IPluginObject* create() override { return new CBoxAlgorithmP300IdentifierCardVisualization; }

	bool hasFunctionality(const EPluginFunctionality functionality) const override { return functionality == EPluginFunctionality::Visualization; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Sequence stimulations", OV_TypeId_Stimulations);
		prototype.addInput("Target stimulations", OV_TypeId_Stimulations);
		prototype.addInput("Card selection stimulations", OV_TypeId_Stimulations);

		prototype.addOutput("Target / Non target flagging", OV_TypeId_Stimulations);

		prototype.addSetting("Interface filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-identifier-card.ui");
		prototype.addSetting("Background color", OV_TypeId_Color, "0,0,0");
		prototype.addSetting("Target background color", OV_TypeId_Color, "10,40,10");
		prototype.addSetting("Selected background color", OV_TypeId_Color, "70,20,20");
		prototype.addSetting("Card stimulation base", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_01");
		prototype.addSetting("Background Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/openvibe-logo.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/01.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/02.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/03.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/04.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/05.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/06.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/07.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/08.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/09.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/10.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/11.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "${Path_Data}/plugins/simple-visualization/p300-magic-card/12.png");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "");
		prototype.addSetting("Card filename", OV_TypeId_Filename, "");

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_P300IdentifierCardVisualizationDesc)
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
