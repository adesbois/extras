/*
 * Note: The signal display and its subclasses (SignalChannelDisplay, SignalDisplayView)
 * were rehauled to give a better user experience for different types of signal. However,
 * the code could likely use significant refactoring for clarity and maintainability.
 * If this is done, care should be taken that the code does not break.
 */

#include "ovpCSignalDisplay.h"

#include <iostream>

#include <sstream>

#include <system/ovCTime.h>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SimpleVisualization;

using namespace /*OpenViBE::*/Toolkit;

using namespace std;

bool CSignalDisplay::initialize()
{
	this->getStaticBoxContext().getInputType(0, m_InputTypeID);

	if (m_InputTypeID == OV_TypeId_StreamedMatrix) { m_StreamDecoder = new TStreamedMatrixDecoder<CSignalDisplay>(*this, 0); }
	else if (m_InputTypeID == OV_TypeId_Signal) { m_StreamDecoder = new TSignalDecoder<CSignalDisplay>(*this, 0); }
	else
	{
		this->getLogManager() << LogLevel_Error << "Unknown input stream type at stream 0\n";
		return false;
	}

	m_StimulationDecoder.initialize(*this, 1);
	m_UnitDecoder.initialize(*this, 2);

	m_BufferDatabase = new CBufferDatabase(*this);

	//retrieve settings
	const CIdentifier displayMode = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0));
	const CIdentifier scalingMode = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1));
	m_refreshInterval             = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	const double verticalScale    = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);
	const double verticalOffset   = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 4);
	const double timeScale        = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 5);
	const bool horizontalRuler    = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 6);
	const bool verticalRuler      = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 7);
	const bool multiview          = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 8);

	if (m_refreshInterval < 0)
	{
		this->getLogManager() << LogLevel_Error << "Refresh interval must be >= 0\n";
		return false;
	}
	if (verticalScale <= 0)
	{
		this->getLogManager() << LogLevel_Error << "Vertical scale must be > 0\n";
		return false;
	}
	if (timeScale <= 0)
	{
		this->getLogManager() << LogLevel_Error << "Time scale must be > 0\n";
		return false;
	}

	this->getLogManager() << LogLevel_Debug << "l_sVerticalScale=" << verticalScale << ", offset " << verticalOffset << "\n";
	this->getLogManager() << LogLevel_Trace << "l_sScalingMode=" << scalingMode << "\n";

	//create GUI
	m_SignalDisplayView = new CSignalDisplayView(*m_BufferDatabase, displayMode, scalingMode, verticalScale, verticalOffset, timeScale, horizontalRuler,
												 verticalRuler, multiview);

	m_BufferDatabase->setDrawable(m_SignalDisplayView);

	//parent visualization box in visualization tree
	GtkWidget* widget  = nullptr;
	GtkWidget* toolbar = nullptr;
	dynamic_cast<CSignalDisplayView*>(m_SignalDisplayView)->getWidgets(widget, toolbar);
	m_visualizationCtx = dynamic_cast<VisualizationToolkit::IVisualizationContext*>(this->createPluginObject(
		OVP_ClassId_Plugin_VisualizationCtx));
	m_visualizationCtx->setWidget(*this, widget);
	if (toolbar != nullptr) { m_visualizationCtx->setToolbar(*this, toolbar); }

	m_lastScaleRefreshTime = 0;

	return true;
}

bool CSignalDisplay::uninitialize()
{
	m_UnitDecoder.uninitialize();
	m_StimulationDecoder.uninitialize();
	if (m_StreamDecoder)
	{
		m_StreamDecoder->uninitialize();
		delete m_StreamDecoder;
	}

	delete m_SignalDisplayView;
	delete m_BufferDatabase;
	m_SignalDisplayView = nullptr;
	m_BufferDatabase    = nullptr;

	if (m_visualizationCtx)
	{
		this->releasePluginObject(m_visualizationCtx);
		m_visualizationCtx = nullptr;
	}

	return true;
}

bool CSignalDisplay::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CSignalDisplay::process()
{
	IDynamicBoxContext* boxContext = getBoxAlgorithmContext()->getDynamicBoxContext();

	if (m_BufferDatabase->getErrorStatus())
	{
		this->getLogManager() << LogLevel_Error <<
				"Buffer database reports an error. Its possible that the inputs given to the Signal Display are not supported by it.\n";
		return false;
	}

	// Subcomponents may generate errors while running e.g. in gtk callbacks, where its not safe/possible to call logmanager
	if (!dynamic_cast<CSignalDisplayView*>(m_SignalDisplayView)->m_ErrorState.empty())
	{
		for (const auto& e : dynamic_cast<CSignalDisplayView*>(m_SignalDisplayView)->m_ErrorState) { this->getLogManager() << LogLevel_Error << e; }
		return false;
	}

#ifdef DEBUG
	uint64_t in = System::Time::zgetTime();
#endif

	// Channel units on input 2 
	for (size_t c = 0; c < boxContext->getInputChunkCount(2); ++c)
	{
		m_UnitDecoder.decode(c);
		if (m_UnitDecoder.isBufferReceived())
		{
			std::vector<std::pair<CString, CString>> channelUnits;
			channelUnits.resize(m_UnitDecoder.getOutputMatrix()->getDimensionSize(0));
			const double* buffer = m_UnitDecoder.getOutputMatrix()->getBuffer();
			for (size_t i = 0; i < channelUnits.size(); ++i)
			{
				CString unit   = this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_MeasurementUnit, uint64_t(buffer[i * 2 + 0]));
				CString factor = this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Factor, uint64_t(buffer[i * 2 + 1]));

				channelUnits[i] = std::pair<CString, CString>(unit, factor);
			}

			if (!dynamic_cast<CSignalDisplayView*>(m_SignalDisplayView)->setChannelUnits(channelUnits))
			{
				this->getLogManager() << LogLevel_Warning << "Unable to set channel units properly\n";
			}
		}
	}

	// Stimulations in input 1
	for (size_t c = 0; c < boxContext->getInputChunkCount(1); ++c)
	{
		m_StimulationDecoder.decode(c);
		if (m_StimulationDecoder.isBufferReceived())
		{
			const IStimulationSet* stimSet = m_StimulationDecoder.getOutputStimulationSet();
			const size_t count             = stimSet->getStimulationCount();

			m_BufferDatabase->setStimulationCount(count);

			for (size_t s = 0; s < count; ++s)
			{
				const uint64_t id   = stimSet->getStimulationIdentifier(s);
				const uint64_t date = stimSet->getStimulationDate(s);
				CString name        = getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, id);

				if (name == CString("")) { name = CString(("Id " + std::to_string(id)).c_str()); }
				dynamic_cast<CSignalDisplayView*>(m_SignalDisplayView)->onStimulationReceivedCB(id, name);
				m_BufferDatabase->setStimulation(s, id, date);
			}
		}
	}

	// Streamed matrix in input 0
	for (size_t c = 0; c < boxContext->getInputChunkCount(0); ++c)
	{
		m_StreamDecoder->decode(c);
		if (m_StreamDecoder->isHeaderReceived())
		{
			const IMatrix* matrix = static_cast<TStreamedMatrixDecoder<CSignalDisplay>*>(m_StreamDecoder)->getOutputMatrix();

			if (m_InputTypeID == OV_TypeId_Signal)
			{
				const uint64_t rate = static_cast<TSignalDecoder<CSignalDisplay>*>(m_StreamDecoder)->getOutputSamplingRate();
				m_BufferDatabase->setSampling(size_t(rate));
			}

			m_BufferDatabase->setMatrixDimensionCount(matrix->getDimensionCount());
			for (size_t i = 0; i < matrix->getDimensionCount(); ++i)
			{
				m_BufferDatabase->setMatrixDimensionSize(i, matrix->getDimensionSize(i));
				for (size_t j = 0; j < matrix->getDimensionSize(i); ++j) { m_BufferDatabase->setMatrixDimensionLabel(i, j, matrix->getDimensionLabel(i, j)); }
			}
		}

		if (m_StreamDecoder->isBufferReceived())
		{
			const IMatrix* matrix = static_cast<TStreamedMatrixDecoder<CSignalDisplay>*>(m_StreamDecoder)->getOutputMatrix();

#ifdef DEBUG
			static int count = 0;
			std::cout << "Push chunk " << (count++) << " at " << boxContext->getInputChunkStartTime(0, c) << "\n";

			if (!Toolkit::Matrix::isContentValid(*matrix, true, true))
			{
				this->getLogManager() << LogLevel_Warning << "Chunk at ["
					<< boxContext->getInputChunkStartTime(0, c) << ", "
					<< boxContext->getInputChunkEndTime(0, c) << "] "
					<< "contains invalid entries\n";
			}
#endif

			const bool returnValue = m_BufferDatabase->setMatrixBuffer(matrix->getBuffer(), boxContext->getInputChunkStartTime(0, c),
																	   boxContext->getInputChunkEndTime(0, c));
			if (!returnValue) { return false; }
		}
	}

	const uint64_t timeNow = getPlayerContext().getCurrentTime();
	if (m_lastScaleRefreshTime == 0 || timeNow - m_lastScaleRefreshTime > CTime(m_refreshInterval).time())
	{
		dynamic_cast<CSignalDisplayView*>(m_SignalDisplayView)->refreshScale();
		m_lastScaleRefreshTime = timeNow;
	}

#ifdef DEBUG
	out = System::Time::zgetTime();
	std::cout << "Elapsed1 " << out - in << "\n";
#endif

	return true;
}
