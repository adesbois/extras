#include "ovpCBoxAlgorithmLevelMeasure.h"

#include "../algorithms/ovpCAlgorithmLevelMeasure.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SimpleVisualization;

bool CBoxAlgorithmLevelMeasure::initialize()
{
	m_matrix = new CMatrix();

	m_matrixDecoder = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StreamedMatrixDecoder));
	m_levelMeasure  = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_ClassId_Algorithm_LevelMeasure));

	m_matrixDecoder->initialize();
	m_levelMeasure->initialize();

	m_matrixBuffer.initialize(m_matrixDecoder->getInputParameter(OVP_GD_Algorithm_StreamedMatrixDecoder_InputParameterId_MemoryBufferToDecode));
	m_matrixHandler.initialize(m_matrixDecoder->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputParameterId_Matrix));

	m_levelMeasureMatrix.initialize(m_levelMeasure->getInputParameter(OVP_Algorithm_LevelMeasure_InputParameterId_Matrix));
	m_levelMeasureMainWidget.initialize(m_levelMeasure->getOutputParameter(OVP_Algorithm_LevelMeasure_OutputParameterId_MainWidget));
	m_levelMeasureToolbarWidget.initialize(m_levelMeasure->getOutputParameter(OVP_Algorithm_LevelMeasure_OutputParameterId_ToolbarWidget));

	m_matrixHandler.setReferenceTarget(m_matrix);
	m_levelMeasureMatrix.setReferenceTarget(m_matrix);

	return true;
}

bool CBoxAlgorithmLevelMeasure::uninitialize()
{
	m_levelMeasureToolbarWidget.uninitialize();
	m_levelMeasureMainWidget.uninitialize();
	m_levelMeasureMatrix.uninitialize();

	m_matrixHandler.uninitialize();
	m_matrixBuffer.uninitialize();

	m_levelMeasure->uninitialize();
	m_matrixDecoder->uninitialize();

	getAlgorithmManager().releaseAlgorithm(*m_levelMeasure);
	getAlgorithmManager().releaseAlgorithm(*m_matrixDecoder);

	delete m_matrix;
	m_matrix = nullptr;

	if (m_visualizationCtx)
	{
		this->releasePluginObject(m_visualizationCtx);
		m_visualizationCtx = nullptr;
	}

	return true;
}

bool CBoxAlgorithmLevelMeasure::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmLevelMeasure::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_matrixBuffer = boxContext.getInputChunk(0, i);
		m_matrixDecoder->process();
		if (m_matrixDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputTriggerId_ReceivedHeader))
		{
			m_levelMeasure->process(OVP_Algorithm_LevelMeasure_InputTriggerId_Reset);
			m_visualizationCtx = dynamic_cast<VisualizationToolkit::IVisualizationContext*>(this->createPluginObject(
				OVP_ClassId_Plugin_VisualizationCtx));
			m_visualizationCtx->setWidget(*this, m_levelMeasureMainWidget);
			m_visualizationCtx->setToolbar(*this, m_levelMeasureToolbarWidget);
		}

		if (m_matrixDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputTriggerId_ReceivedBuffer))
		{
			// ----- >8 ------------------------------------------------------------------------------------------------------------------------------------------------------
			// should be done in a processing box !

			double sum = 0;

			{
				double* buffer = m_matrix->getBuffer();
				size_t n       = m_matrix->getBufferElementCount();
				while (n--)
				{
					sum += *buffer;
					buffer++;
				}
			}

			{
				const double factor = (sum != 0 ? 1. / sum : 0.5);
				double* buffer      = m_matrix->getBuffer();
				size_t n            = m_matrix->getBufferElementCount();
				while (n--)
				{
					*buffer *= factor;
					buffer++;
				}
			}

			// ----- >8 ------------------------------------------------------------------------------------------------------------------------------------------------------

			m_levelMeasure->process(OVP_Algorithm_LevelMeasure_InputTriggerId_Refresh);
		}
		if (m_matrixDecoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputTriggerId_ReceivedEnd)) { }
		boxContext.markInputAsDeprecated(0, i);
	}

	return true;
}
