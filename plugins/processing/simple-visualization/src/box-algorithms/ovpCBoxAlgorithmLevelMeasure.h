#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
class CBoxAlgorithmLevelMeasure final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_LevelMeasure)

protected:

	Kernel::IAlgorithmProxy* m_matrixDecoder = nullptr;
	Kernel::IAlgorithmProxy* m_levelMeasure  = nullptr;

	Kernel::TParameterHandler<const IMemoryBuffer*> m_matrixBuffer;
	Kernel::TParameterHandler<IMatrix*> m_matrixHandler;

	Kernel::TParameterHandler<IMatrix*> m_levelMeasureMatrix;
	Kernel::TParameterHandler<GtkWidget*> m_levelMeasureMainWidget;
	Kernel::TParameterHandler<GtkWidget*> m_levelMeasureToolbarWidget;

	IMatrix* m_matrix = nullptr;

private:
	VisualizationToolkit::IVisualizationContext* m_visualizationCtx = nullptr;
};

class CBoxAlgorithmLevelMeasureDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Level measure"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString(""); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Visualization/Basic"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-go-up"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_LevelMeasure; }
	IPluginObject* create() override { return new CBoxAlgorithmLevelMeasure; }

	bool hasFunctionality(const EPluginFunctionality functionality) const override { return functionality == EPluginFunctionality::Visualization; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input matrix to display", OV_TypeId_StreamedMatrix);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_LevelMeasureDesc)
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
