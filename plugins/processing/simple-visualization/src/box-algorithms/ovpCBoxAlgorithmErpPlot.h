#pragma once

//You may have to change this path to match your folder organisation
#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include <cairo.h>

#include <cfloat>

#include <gdk/gdk.h>

#include <gtk/gtk.h>

#include <iostream>
#include <list>
#include <utility>
#include <vector>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
class Graph
{
public:
	//should be a list of colors;
	Graph(std::vector<GdkColor>& lineColor, std::vector<CString>& lineText, const int rowIndex, const int colIndex, const int curveSize)
		: m_LineColor(lineColor), m_LineText(lineText)
	{
		m_Curves.resize(lineColor.size());
		m_Variance.resize(lineColor.size());
		for (size_t i = 0; i < m_Curves.size(); ++i)
		{
			m_Curves[i].resize(curveSize, 0);
			m_Variance[i].resize(curveSize, 0);
		}
		this->m_RowIdx    = rowIndex;
		this->m_ColIdx    = colIndex;
		this->m_CurveSize = curveSize;
		m_PointCounter    = new int[lineColor.size()];
		m_Maximum         = -FLT_MAX;
		m_Minimum         = FLT_MAX;
		for (size_t i = 0; i < lineColor.size(); ++i) { m_PointCounter[i] = 0; }
	}

	~Graph()
	{
		delete [] m_PointCounter;
		m_Curves.clear();
		m_Variance.clear();
	}

	void resizeAxis(gint width, gint height, size_t nrOfGraphs);
	void draw(GtkWidget* widget);
	void drawAxis(cairo_t* ctx);
	void drawLine(cairo_t* ctx, double* xo, double* yo, double* xe, double* ye) const;
	void drawAxisLabels(cairo_t* ctx);
	void drawCurves(cairo_t* ctx);
	void drawLegend(cairo_t* ctx) const;
	void drawVar(cairo_t* ctx);
	void updateCurves(const double* curve, size_t howMany, size_t curveIndex);
	void snapCoords(cairo_t* ctx, double* x, double* y) const;
	double adjustValueToScale(double value);

	std::vector<std::vector<double>> m_Curves; //private
	std::vector<std::vector<double>> m_Variance;

	std::vector<GdkColor>& m_LineColor; //private
	std::vector<CString>& m_LineText; //private
	double m_Maximum = 1;
	double m_Minimum = -1;
	std::pair<int, int> m_ArgMaximum;
	std::pair<int, int> m_ArgMinimum;
	double m_GraphWidth   = 0;
	double m_GraphHeight  = 0;
	double m_GraphOriginX = 0;
	double m_GraphOriginY = 0;
	int m_RowIdx          = 0; //private
	int m_ColIdx          = 0; //private 
	int m_CurveSize       = 0; //private
	int* m_PointCounter   = nullptr;
	uint64_t m_StartTime  = 0;
	uint64_t m_EndTime    = 0;
	double m_FontSize     = 1.0;
};

/**
 * \class CBoxAlgorithmErpPlot
 * \author Dieter Devlaminck (INRIA)
 * \date Fri Nov 16 10:50:43 2012
 * \brief The class CBoxAlgorithmErpPlot describes the box ERP plot.
 *
 */
class CBoxAlgorithmErpPlot final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;
	bool save();
	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_ErpPlot)

protected:
	CString m_figureFileName;
	std::vector<GdkColor> m_legendColors;
	std::vector<CString> m_legend;
	GtkWidget* m_drawWindow        = nullptr;
	std::list<Graph*>* m_graphList = nullptr;
	bool m_firstHeaderReceived     = false;
	std::vector<Toolkit::TStreamedMatrixDecoder<CBoxAlgorithmErpPlot>*> m_decoders;
	std::vector<Toolkit::TStreamedMatrixDecoder<CBoxAlgorithmErpPlot>*> m_varianceDecoders;
	Toolkit::TStimulationDecoder<CBoxAlgorithmErpPlot>* m_stimulationDecoder = nullptr;
	uint64_t m_triggerToSave                                                 = 0;
	bool m_xStartsAt0                                                        = false;

private:
	VisualizationToolkit::IVisualizationContext* m_visualizationCtx = nullptr;
};

// The box listener can be used to call specific callbacks whenever the box structure changes : input added, name changed, etc.
// Please uncomment below the callbacks you want to use.
class CBoxAlgorithmErpPlotListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	bool onInputAdded(Kernel::IBox& box, const size_t index) override
	{
		box.setInputType(index, OV_TypeId_StreamedMatrix);
		const size_t c               = index / 2 + 1;
		const std::string idx        = std::to_string(c), iLabel = "ERP ",
						  colorLabel = "Line color ", textLabel  = "Line label ", varianceLabel = "Variance ";

		box.setInputName(index, (iLabel + idx).c_str());
		box.addSetting((colorLabel + idx).c_str(),OV_TypeId_Color, "0,0,0");
		box.addSetting((textLabel + idx).c_str(),OV_TypeId_String, "curve");
		//add the corresponding variance input
		box.addInput((varianceLabel + idx).c_str(), OV_TypeId_StreamedMatrix);

		return true;
	}

	bool onInputRemoved(Kernel::IBox& box, const size_t index) override
	{
		box.removeSetting(index * 2 + 2);
		box.removeSetting(index * 2 + 1);
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};


/**
 * \class CBoxAlgorithmErpPlotDesc
 * \author Dieter Devlaminck (INRIA)
 * \date Fri Nov 16 10:50:43 2012
 * \brief Descriptor of the box ERP plot.
 *
 */
class CBoxAlgorithmErpPlotDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("ERP plot"); }
	CString getAuthorName() const override { return CString("Dieter Devlaminck"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Plots event-related potentials"); }
	CString getDetailedDescription() const override { return CString("plots target ERP versus non-target ERP"); }
	CString getCategory() const override { return CString("Visualization/Presentation"); }
	CString getVersion() const override { return CString("1.1"); }
	CString getStockItemName() const override { return CString(""); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ErpPlot; }
	IPluginObject* create() override { return new CBoxAlgorithmErpPlot; }

	bool hasFunctionality(const EPluginFunctionality functionality) const override { return functionality == EPluginFunctionality::Visualization; }

	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmErpPlotListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Trigger",OV_TypeId_Stimulations);
		prototype.addInput("ERP1",OV_TypeId_StreamedMatrix);
		prototype.addInput("Variance1",OV_TypeId_StreamedMatrix);

		prototype.addFlag(Kernel::BoxFlag_CanAddInput);

		prototype.addSetting("Filename final figure",OV_TypeId_Filename, "");
		prototype.addSetting("Trigger to save figure",OV_TypeId_Stimulation, "OVTK_StimulationId_ExperimentStop");
		prototype.addSetting("X starts at 0",OV_TypeId_Boolean, "true");
		prototype.addSetting("Line color 1",OV_TypeId_Color, "0,0,0");
		prototype.addSetting("Line label 1",OV_TypeId_String, "curve 1");

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ErpPlotDesc)
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
