#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <visualization-toolkit/ovviz_all.h>

#include "../ovpCBufferDatabase.h"
#include "ovpCSignalDisplay/ovpCSignalDisplayView.h"

namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
/**
* This plugin opens a new GTK window and displays the incoming signals. The user may change the zoom level,
* the width of the time window displayed, ...
*/
class CSignalDisplay final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	CSignalDisplay() = default;

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(IBoxAlgorithm, OVP_ClassId_SignalDisplay)

	Toolkit::TDecoder<CSignalDisplay>* m_StreamDecoder = nullptr;
	Toolkit::TStimulationDecoder<CSignalDisplay> m_StimulationDecoder;
	Toolkit::TChannelUnitsDecoder<CSignalDisplay> m_UnitDecoder;

	//The main object used for the display (contains all the GUI code)
	CSignalDisplayDrawable* m_SignalDisplayView = nullptr;

	//Contains all the data about the incoming signal
	CBufferDatabase* m_BufferDatabase = nullptr;

	CIdentifier m_InputTypeID = OV_UndefinedIdentifier;

protected:
	uint64_t m_lastScaleRefreshTime = 0;
	double m_refreshInterval        = 0;

private:
	VisualizationToolkit::IVisualizationContext* m_visualizationCtx = nullptr;
};


class CSignalDisplayListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	bool onInputTypeChanged(Kernel::IBox& box, const size_t index) override
	{
		if (index == 1)
		{
			CIdentifier settingType = OV_UndefinedIdentifier;
			box.getSettingType(index, settingType);
			if (settingType != OV_TypeId_Stimulations)
			{
				this->getLogManager() << Kernel::LogLevel_Error << "Error: Only stimulation type supported for input 2\n";
				box.setInputType(index, OV_TypeId_Stimulations);
			}
		}
		else if (index == 2)
		{
			CIdentifier settingType = OV_UndefinedIdentifier;
			box.getSettingType(index, settingType);
			if (settingType != OV_TypeId_ChannelUnits)
			{
				this->getLogManager() << Kernel::LogLevel_Error << "Error: Only measurement unit type supported for input 3\n";
				box.setInputType(index, OV_TypeId_ChannelUnits);
			}
		}

		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

/**
 * Signal Display plugin descriptor
 */
class CSignalDisplayDesc final : public IBoxAlgorithmDesc
{
public:
	CString getName() const override { return CString("Signal display"); }
	CString getAuthorName() const override { return CString("Bruno Renier, Yann Renard, Alison Cellard, Jussi T. Lindgren"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString("Displays the incoming stream"); }

	CString getDetailedDescription() const override { return CString("This box can be used to visualize signal and matrix streams"); }

	CString getCategory() const override { return CString("Visualization/Basic"); }
	CString getVersion() const override { return CString("0.3"); }
	void release() override { }
	CIdentifier getCreatedClass() const override { return OVP_ClassId_SignalDisplay; }
	CString getStockItemName() const override { return CString("gtk-zoom-fit"); }
	IPluginObject* create() override { return new CSignalDisplay(); }
	IBoxListener* createBoxListener() const override { return new CSignalDisplayListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool hasFunctionality(const EPluginFunctionality functionality) const override { return functionality == EPluginFunctionality::Visualization; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addSetting("Display Mode", OVP_TypeId_SignalDisplayMode, "Scan");
		prototype.addSetting("Auto vertical scale", OVP_TypeId_SignalDisplayScaling, CSignalDisplayView::SCALING_MODES[0].c_str());
		prototype.addSetting("Scale refresh interval (secs)", OV_TypeId_Float, "5");
		prototype.addSetting("Vertical Scale",OV_TypeId_Float, "100");
		prototype.addSetting("Vertical Offset",OV_TypeId_Float, "0");
		prototype.addSetting("Time Scale", OV_TypeId_Float, "10");
		prototype.addSetting("Bottom ruler", OV_TypeId_Boolean, "true");
		prototype.addSetting("Left ruler", OV_TypeId_Boolean, "false");
		prototype.addSetting("Multiview", OV_TypeId_Boolean, "false");

		prototype.addInput("Data", OV_TypeId_Signal);
		prototype.addInput("Stimulations", OV_TypeId_Stimulations);
		prototype.addInput("Channel Units", OV_TypeId_ChannelUnits);

		prototype.addInputSupport(OV_TypeId_Signal);
		prototype.addInputSupport(OV_TypeId_StreamedMatrix);
		prototype.addInputSupport(OV_TypeId_ChannelUnits);

		prototype.addFlag(Kernel::BoxFlag_CanModifyInput);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_SignalDisplayDesc)
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
