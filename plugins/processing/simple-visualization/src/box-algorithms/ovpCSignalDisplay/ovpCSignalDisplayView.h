#pragma once

#include <openvibe/ov_all.h>
#include "ovpCSignalChannelDisplay.h"
#include "../../ovpCBufferDatabase.h"
#include "../../ovpCBottomTimeRuler.h"
#include <gtk/gtk.h>
#include <map>
#include <string>
#include <vector>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
/**
* This class contains everything necessary to setup a GTK window and display
* a signal thanks to a CBufferDatabase's information.
*/
class CSignalDisplayView : public CSignalDisplayDrawable
{
public:


	/**
	 * \brief Constructor
	 * \param [in] buffer Signal database
	 * \param [in] timeScale Initial time scale value
	 * \param [in] displayMode Initial signal display mode
	 * \param [in] scalingMode Initial signal scaling mode
	 * \param [in] verticalScale Initial vertical scale value
	 * \param [in] verticalOffset Initial vertical offset value
	 * \param [in] timeScale Initial time scale value
	 * \param [in] horizontalRuler Initial horizontal ruler activation
	 * \param [in] verticalRuler Initial vertical ruler activation
	 * \param [in] multiview Initial multiview ruler activation
	 */
	CSignalDisplayView(CBufferDatabase& buffer, const CIdentifier& displayMode, const CIdentifier& scalingMode,
					   double verticalScale, double verticalOffset, double timeScale, bool horizontalRuler, bool verticalRuler, bool multiview);

	/**
	 * \brief Base constructor
	 * \param [in] buffer Signal database
	 * \param [in] timeScale Initial time scale value
	 * \param [in] displayMode Initial signal display mode
	 */
	void construct(CBufferDatabase& buffer, double timeScale, const CIdentifier& displayMode);

	/**
	 * \brief Destructor
	 */
	~CSignalDisplayView() override;
	/**
	 * \brief Get pointers to plugin main widget and (optional) toolbar widget
	 * \param [out] widget Pointer to main widget
	 * \param [out] toolbar Pointer to (optional) toolbar widget
	 */
	void getWidgets(GtkWidget*& widget, GtkWidget*& toolbar) const;
	/**
	 * Initializes the window.
	 */
	void init() override;
	/**
	 * Invalidates the window's content and tells it to redraw itself.
	 */
	void redraw() override;
	/**
	* Toggle left rulers on/off
	* \param active Show rulers if true.
	*/
	void toggleLeftRulers(bool active);
	/**
	 * Toggle time ruler on/off
	 * \param active Show the ruler if true.
	 */
	void toggleBottomRuler(bool active);
	/**
	 * Toggle a channel on/off
	 * \param index The index of the channel to toggle.
	 * \param active Show the channel if true.
	 */
	void toggleChannel(size_t index, bool active);

	void toggleChannelMultiView(bool active);

	void changeMultiView();

	void removeOldWidgets();
	void recreateWidgets(size_t nChannel);
	void updateMainTableStatus();

	void updateDisplayTableSize() const;

	void activateToolbarButtons(bool active) const;

	/**
	 * Callback called when display mode changes
	 * \param mode New display mode
	 * \return True
	 */
	bool onDisplayModeToggledCB(const CIdentifier& mode);
	bool onUnitsToggledCB(bool active);

	/**
	 * Callback called when vertical scale mode changes
	 * \param button Radio button toggled
	 * \return True
	 */
	bool onVerticalScaleModeToggledCB(GtkToggleButton* button);

	/**
	 * Callback called when custom vertical scale is changed
	 * \param button Custom vertical scale widget
	 * \return True if custom vertical scale value could be retrieved, false otherwise
	 */
	bool onCustomVerticalScaleChangedCB(GtkSpinButton* button);
	bool onCustomVerticalOffsetChangedCB(GtkSpinButton* button);

	/**
	 * \brief Get a channel display object
	 * \param index Index of channel display
	 * \return Channel display object
	 */
	CSignalChannelDisplay* getChannelDisplay(size_t index);

	bool isChannelDisplayVisible(size_t index);

	void onStimulationReceivedCB(uint64_t id, const CString& name);

	bool setChannelUnits(const std::vector<std::pair<CString, CString>>& channelUnits);

	/**
	 * \brief Get a color from a stimulation code
	 * \remarks Only the lower 32 bits of the stimulation code are currently used to compute the color.
	 * \param[in] id Stimulation code
	 * \param[out] color Color computed from stimulation code
	 */
	void getStimulationColor(uint64_t id, GdkColor& color);

	/**
	 * \brief Get a color from a signal
	 * \param[in] index channel index
	 * \param[out] color Color computed from stimulation code
	 */
	void getMultiViewColor(size_t index, GdkColor& color);

	void refreshScale();

private:
	/**
	 * \brief Update stimulations color dialog with a new (stimulation, color) pair
	 * \param[in] stimulation Stimulation label
	 * \param[in] color Stimulation color
	 */
	void updateStimulationColorsDialog(const CString& stimulation, const GdkColor& color) const;

public:

	//! The Builder handler used to create the interface
	GtkBuilder* m_Builder = nullptr;

	//! The table containing the CSignalChannelDisplays
	GtkWidget* m_SignalDisplayTable = nullptr;
	GtkWidget* m_Separator          = nullptr;

	//! Array of the channel's labels
	std::vector<GtkWidget*> m_ChannelLabel;

	//! Array of CSignalChannelDisplays (one per channel, displays the corresponding channel)
	std::vector<CSignalChannelDisplay*> m_ChannelDisplay;
	std::map<size_t, GtkWidget*> m_Separators;

	//! Show left rulers when true
	bool m_ShowLeftRulers = false;

	//!Show bottom time ruler when true
	bool m_ShowBottomRuler = false;

	//! Time of displayed signals at the left of channel displays
	uint64_t m_LeftmostDisplayedTime = 0;

	//! Largest displayed value range, to be matched by all channels in global best fit mode
	double m_LargestDisplayedValueRange = 0;
	double m_LargestDisplayedValue      = 0;
	double m_SmallestDisplayedValue     = 0;

	//! Current value range margin, used to avoid redrawing signals every time the largest value range changes
	double m_ValueRangeMargin = 0;
	// double m_ValueMaxMargin;
	/*! Margins added to largest and subtracted from smallest displayed values are computed as :
	m_MarginFactor * m_LargestDisplayedValueRange. If m_MarginFactor = 0, there's no margin at all.
	If factor is 0.1, largest displayed value range is extended by 10% above and below its extremums at the time
	when margins are computed. */
	double m_MarginFactor = 0.4;

	//! Normal/zooming cursors
	std::array<GdkCursor*, 2> m_Cursor;

	/** \name Vertical scale */
	//@{
	//! Flag set to true when you'd like the display to *check* if the scale needs to change and possibly update
	bool m_VerticalScaleRefresh = true;
	//! Flag set to true when you'd like the display to update in any case
	bool m_VerticalScaleForceUpdate = false;
	//! Value of custom vertical scale
	double m_CustomVerticalScaleValue = 0;
	//! Value of custom vertical offset
	double m_CustomVerticalOffset = 0;
	//@}

	//! The database that contains the information to use to draw the signals
	CBufferDatabase* m_Buffer = nullptr;

	//! Vector of gdk points. Used to draw the signals.
	std::vector<GdkPoint> m_Points;

	//! Vector of raw points. Stores the points' coordinates before cropping.
	std::vector<std::pair<double, double>> m_RawPoints;

	std::vector<CString> m_ChannelName;

	//! Vector of indexes of the channels to display
	std::map<size_t, bool> m_SelectedChannels;

	size_t m_NSelectedChannel = 0;

	std::map<size_t, std::pair<CString, CString>> m_ChannelUnits;

	//! Flag set to true once multi view configuration dialog is initialized
	bool m_MultiViewEnabled = false;

	//! Vector of indices of selected channels
	std::map<size_t, bool> m_MultiViewSelectedChannels;

	//Map of stimulation codes received so far, and their corresponding name and color
	std::map<uint64_t, std::pair<CString, GdkColor>> m_Stimulations;

	//Map of signal indices received so far, and their corresponding name and color
	std::map<uint64_t, std::pair<CString, GdkColor>> m_Signals;

	//! Bottom box containing bottom ruler
	GtkBox* m_BottomBox = nullptr;

	//! Bottom time ruler
	CBottomTimeRuler* m_BottomRuler = nullptr;

	//! Widgets for left rulers
	std::vector<GtkWidget*> m_LeftRulers;

	CIdentifier m_ScalingMode = OV_UndefinedIdentifier;

	static const std::vector<std::string> SCALING_MODES;

	std::vector<CString> m_ErrorState;

	bool m_StimulationColorsShown = false;
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
