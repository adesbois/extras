#pragma once

#include <openvibe/ov_all.h>
#include <gtk/gtk.h>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
/**
 * Used to display a vertical "amplitude" ruler.
 * */
class CSignalDisplayLeftRuler
{
public:
	/**
	 * \brief Constructor
	 * \param width Width to be requested by widget
	 * \param height Height to be requested by widget
	 */
	CSignalDisplayLeftRuler(int width, int height);
	//	CSignalChannelDisplay* pParentChannelDisplay);

	/**
	 * \brief Destructor
	 */
	~CSignalDisplayLeftRuler() = default;

	/*
	 * \brief Update ruler with latest min/max values and request a redraw
	 * \param min Minimum value to be displayed
	 * \param max Maximum value to be displayed
	 */
	void update(double min, double max);

	//! returns the widget, so it can be added to the main interface
	GtkWidget* getWidget() const { return m_Ruler; }

	/**
	 * \brief Draws the ruler by using the information from the database.
	 */
	void draw() const;

	GtkWidget* m_Ruler         = nullptr;
	int m_Width                = 0;
	double m_MaxDisplayedValue = -DBL_MAX;
	double m_MinDisplayedValue = DBL_MAX;
	uint64_t m_PixelsPerLabel  = 10;
	// CSignalChannelDisplay* m_pParentChannelDisplay = nullptr;
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
