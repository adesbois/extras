#pragma once

#include "../../ovp_defines.h"
#include "ovpCSignalDisplayLeftRuler.h"

#include <glib.h>
#include <gtk/gtk.h>
#include <openvibe/ov_all.h>

#include <map>
#include <vector>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
class CSignalDisplayView;
class CBufferDatabase;

class CSignalChannelDisplay
{
public:
	/**
	 * \brief Constructor
	 * \param displayView Parent view
	 * \param channelDisplayW Width to be requested by widget
	 * \param channelDisplayH Height to be requested by widget
	 * \param leftRulerW Width to be requested by left ruler
	 * \param leftRulerH Height to be requested by left ruler
	 */
	CSignalChannelDisplay(CSignalDisplayView* displayView, int channelDisplayW, int channelDisplayH, int leftRulerW, int leftRulerH);

	/**
	 * \brief Destructor
	 */
	~CSignalChannelDisplay();

	/**
	 * \brief Get ruler widget
	 * \return Pointer to ruler widget
	 */
	GtkWidget* getRulerWidget(size_t index) const;

	/**
	 * \brief Get signal display widget
	 * \return Pointer to signal display widget
	 */
	GtkWidget* getSignalDisplayWidget() const { return m_DrawingArea; }

	/**
	 * \brief Callback notified upon resize events
	 * \param width New window width
	 * \param height New window height
	 */
	void onResizeEventCB(gint width, gint height);

	/**
	 * \brief Updates scale following a resize event or a time scale change
	 */
	void updateScale();

	// Updates some drawing limits, i.e. to limit drawing to [chn_i,...,chn_j]
	void updateLimits();

	/**
	 * \brief Reset list of channels displayed by this object
	 */
	void resetChannelList();

	/**
	 * \brief Add a channel to the list of channels to be displayed
	 * \param channel Index of channel to be displayed
	 */
	void addChannel(size_t channel);

	void addChannelList(size_t channel);

	/**
	 * \brief Get rectangle to clear and redraw based on latest signal data received
	 * \param[out] rect Rectangle holding part of drawing area to clear and update
	 */
	void getUpdateRectangle(GdkRectangle& rect) const;

	/**
	 * \brief Flag widget so that its whole window is redrawn at next refresh
   */
	void redrawAllAtNextRefresh(bool redraw);

	/**
	 * \brief Check whether the whole window must be redrawn
	 * \return True if the whole window must be redrawn, false otherwise
	 */
	bool mustRedrawAll() const { return m_RedrawAll; }

	/**
	 * \brief Draws the signal on the signal's drawing area.
	 * \param area Exposed area that needs to be redrawn
	 */
	void draw(const GdkRectangle& area);

	/**
	 * \brief Clip signals to drawing area
	 * Computes the list of points used to draw the lines (m_ParentDisplayView->m_Points) using the raw points list
	 * (m_ParentDisplayView->m_RawPoints) and by cropping the lines when they go out of the window.
	 * \param pointCount Number of points to clip
	 * \return The number of points to display.
	 */
	uint64_t cropCurve(uint64_t pointCount) const;

	/**
	 * \brief Computes the parameters necessary for the signal to be zoomed at the selected coordinates.
	 * \param zoomIn If true, the operation is a zoom In, if false it's a zoom Out.
	 * \param x The X-coordinate of the center of the area we want to zoom in.
	 * \param y The Y-coordinate of the center of the area we want to zoom in.
	 */
	void computeZoom(bool zoomIn, double x, double y);

	/**
	 * \brief Returns empiric y min and maxes of the currently shown signal chunks for all subchannels.
	 * Note that the actually used display limits may be different. This function can be used
	 * to get the data extremal values and then use these to configure the display appropriately.
	 */
	void getDisplayedValueRange(std::vector<double>& min, std::vector<double>& max) const;

	/*
	 * \brief Sets scale for all subchannels. 
	 */
	void setGlobalScaleParameters(double min, double max, double margin);

	/*
	 * \brief Sets scale for a single subchannel.
	 */
	void setLocalScaleParameters(size_t subChannelIdx, double min, double max, double margin);

	/**
	 * \brief Updates signal scale and translation based on latest global range and margin
	 */
	void updateDisplayParameters();

private:
	/**
	 * \brief Get first buffer to display index and position and first sample to display index
	 * \param[out] bufferIdx Index of first buffer to display
	 * \param[out] sampleIdx Index of first sample to display
	 * \param[out] bufferPos Position of first buffer to display (0-based, from left edge)
	 */
	void getFirstBufferToDisplayInformation(size_t& bufferIdx, size_t& sampleIdx, size_t& bufferPos) const;

	/**
	 * \brief Get start X coord of a buffer
	 * \param pos Position of buffer on screen (0-based, from left edge)
	 * \return Floored X coordinate of buffer
	 */
	int getBufferStartX(size_t pos) const;

	/**
	 * \brief Get X coordinate of a sample
	 * \param bufferPos Position of buffer on screen (0-based, from left edge)
	 * \param sampleIdx Index of sample in buffer
	 * \param offset X offset from which to start drawing. Used in scroll mode only.
	 * \return X coordinate of sample
	 */
	double getSampleXCoordinate(size_t bufferPos, size_t sampleIdx, double offset) const;

	/**
	 * \brief Get Y coordinate of a sample
	 * \param value Sample value and index of channel
	 * \param channelIdx Index of channel
	 * \return Y coordinate of sample
	 */
	double getSampleYCoordinate(double value, size_t channelIdx);

	/**
	 * \brief Get Y coordinate of a sample in Multiview mode
	 * \param value Sample value and index of channel
	 * \return Y coordinate of sample
	 */
	double getSampleYMultiViewCoordinate(double value);

	/**
	 * \brief Draw signals (and stimulations, if any) displayed by this channel
	 * \param firstBuffer Index of first buffer to display
	 * \param lastBuffer Index of last buffer to display
	 * \param firstSample Index of first sample to display in first buffer (subsequent buffers will start at sample 0)
	 * \param startX Start X Coordinate
	 * \param firstChannel Index of first channel to display
	 * \param lastChannel Index of last channel to display
	 * \return True if all went ok, false otherwise
	 */
	bool drawSignals(size_t firstBuffer, size_t lastBuffer, size_t firstSample, double startX, size_t firstChannel, size_t lastChannel);

	/**
	 * \brief Draw vertical line highlighting where data was last drawn
	 */
	void drawProgressLine(size_t firstBufferIdx, size_t firstBufferPos) const;

	/**
	 * \brief Draw Y=0 line
	 */
	void drawZeroLine();

public:
	//! Vector of Left rulers displaying signal scale. Indexed by channel id. @note This is a map as the active number of channels 
	// may change by the toolbar whereas this total set of rulers doesn't...
	std::map<size_t, CSignalDisplayLeftRuler*> m_LeftRuler;
	//! The drawing area where the signal is to be drawn
	GtkWidget* m_DrawingArea = nullptr;
	//! Drawing area dimensions, in pixels
	size_t m_Width = 0, m_Height = 0;
	//! Available width per buffer, in pixels
	double m_WidthPerBuffer = 0;
	//! Available width per sample point, in pixels
	double m_PointStep = 0;
	//! The index list of the channels to display
	std::vector<size_t> m_ChannelList;
	//! The "parent" view (which uses this widget)
	CSignalDisplayView* m_ParentDisplayView = nullptr;
	//! The database from which the information are to be read
	CBufferDatabase* m_Database = nullptr;

	/** \ name Extrema of displayed values for all channel in this display */
	//@{
	std::vector<double> m_LocalMaximum, m_LocalMinimum;
	//@}

	/** \name Auto scaling parameters */
	//@{
	//		double m_ScaleX, m_TranslateX;

	std::vector<double> m_ScaleY, m_TranslateY;
	//@}

	/** \name Zooming parameters (user controlled) */
	//@{
	double m_ZoomTranslateX = 0, m_ZoomTranslateY = 0, m_ZoomScaleX = 1, m_ZoomScaleY = 1;
	//! The zoom factor step
	const double m_ZoomFactor = 1.5;
	//@}

	/** \name Scale margin parameters */
	//@{
	std::vector<double> m_OuterTopMargin, m_InnerTopMargin, m_InnerBottomMargin, m_OuterBottomMargin;
	//@}

	size_t m_LeftRulerW = 0, m_LeftRulerH = 0;

	//! Current signal display mode
	EDisplayMode m_CurrentSignalMode = GlobalBestFit;
	//! Time of latest displayed data
	uint64_t m_LatestDisplayedTime = 0;

	//! Should the whole window be redrawn at next redraw?
	bool m_RedrawAll = false;

	//! Is it a multiview display ?
	bool m_MultiView = false;

	// These parameters control that we don't unnecessarily draw parts of the signal which are not in view

	// Currently visible y segment in the drawing area
	size_t m_StartY = 0, m_StopY = 0;

	// First and last channel to draw
	size_t m_FirstChannelToDisplay = 0, m_LastChannelToDisplay = 0;
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
