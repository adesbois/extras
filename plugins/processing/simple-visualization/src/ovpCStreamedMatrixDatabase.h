#pragma once
#include "ovpIStreamDatabase.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <deque>
#include <vector>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
/**
* This class is used to store information about the incoming matrix stream. It can request a IStreamDisplayDrawable
* object to redraw itself upon changes in its data.
*/
class CStreamedMatrixDatabase : public IStreamDatabase
{
public:
	/**
	 * \brief Constructor
	 * \param
	 */
	explicit CStreamedMatrixDatabase(Toolkit::TBoxAlgorithm<IBoxAlgorithm>& parent) : m_parentPlugin(parent) {}

	/**
	 * \brief Destructor
	 */
	~CStreamedMatrixDatabase() override;

	bool initialize() override;

	void setDrawable(IStreamDisplayDrawable* drawable) override { m_drawable = drawable; }
	void setRedrawOnNewData(const bool redrawOnNewData) override { m_redrawOnNewData = redrawOnNewData; }

	bool hasFirstBuffer() override { return m_hasFirstBuffer; }
	bool setMaxBufferCount(const size_t count) override;
	bool setTimeScale(const double timeScale) override;
	bool decodeMemoryBuffer(const IMemoryBuffer* buffer, const uint64_t startTime, const uint64_t endTime) override;

	size_t getMaxBufferCount() override { return m_nMaxBuffer; }
	size_t getCurrentBufferCount() override { return m_matrices.size(); }

	const double* getBuffer(const size_t index) override { return (index >= m_matrices.size()) ? nullptr : m_matrices[index]->getBuffer(); }

	uint64_t getStartTime(const size_t index) override { return (index >= m_startTimes.size()) ? 0 : m_startTimes[index]; }
	uint64_t getEndTime(const size_t index) override { return (index >= m_endTimes.size()) ? 0 : m_endTimes[index]; }

	size_t getBufferElementCount() override { return (m_matrices.empty()) ? 0 : m_matrices[0]->getBufferElementCount(); }
	uint64_t getBufferDuration() override { return (m_startTimes.empty() || m_endTimes.empty()) ? 0 : m_endTimes[0] - m_startTimes[0]; }
	bool isBufferTimeStepComputed() override { return m_bufferTimeStepComputed; }
	uint64_t getBufferTimeStep() override { return m_bufferTimeStepComputed ? m_bufferTimeStep : 0; }

	size_t getSampleCountPerBuffer() override { return m_matrixHeader.getDimensionCount() == 0 ? 0 : m_matrixHeader.getDimensionSize(1); }
	size_t getChannelCount() override { return (m_matrixHeader.getDimensionCount() == 0) ? 0 : m_matrixHeader.getDimensionSize(0); }

	bool getChannelLabel(const size_t index, CString& label) override;
	bool getChannelMinMaxValues(const size_t index, double& min, double& max) override;
	bool getGlobalMinMaxValues(double& min, double& max) override;
	bool getLastBufferChannelMinMaxValues(const size_t index, double& min, double& max) override;
	bool getLastBufferGlobalMinMaxValues(double& min, double& max) override;

protected:
	bool onBufferCountChanged();

	virtual bool decodeHeader();
	virtual bool decodeBuffer(const uint64_t startTime, const uint64_t endTime);

	// parent plugin
	Toolkit::TBoxAlgorithm<IBoxAlgorithm>& m_parentPlugin;
	//decoder algorithm
	Kernel::IAlgorithmProxy* m_decoder = nullptr;
	//drawable object to update (if needed)
	IStreamDisplayDrawable* m_drawable = nullptr;
	//flag stating whether to redraw the IStreamDisplayDrawable upon new data reception if true (default)
	bool m_redrawOnNewData = true;
	//flag stating whether first samples buffer has been received
	bool m_hasFirstBuffer = false;
	//flag stating whether buffer time step was computed
	bool m_bufferTimeStepComputed = false;
	//time difference between start times of two consecutive buffers
	uint64_t m_bufferTimeStep = 0;
	//maximum number of buffers stored in database
	size_t m_nMaxBuffer = 2;
	//flag stating whether time scale should be ignored (max buffer count externally set)
	bool m_ignoreTimeScale = false;
	//maximum duration of displayed buffers (in seconds)
	double m_timeScale = 10;
	//double-linked list of start times of stored buffers
	std::deque<uint64_t> m_startTimes;
	//double-linked list of end times of stored buffers
	std::deque<uint64_t> m_endTimes;
	//streamed matrix header
	CMatrix m_matrixHeader;
	//streamed matrix	history
	std::deque<CMatrix*> m_matrices;
	//min/max values for each channel
	std::vector<std::deque<std::pair<double, double>>> m_channelMinMaxValues;
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
