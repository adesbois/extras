#pragma once

#include "ovpCStreamedMatrixDatabase.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
/**
* This class is used to store information about the incoming spectrum stream. It can request a IStreamDisplayDrawable
* object to redraw itself upon changes in its data.
*/
class CSpectrumDatabase final : public CStreamedMatrixDatabase
{
public:
	explicit CSpectrumDatabase(Toolkit::TBoxAlgorithm<IBoxAlgorithm>& parent) : CStreamedMatrixDatabase(parent) { }
	~CSpectrumDatabase() override = default;

	bool initialize() override;

	/**
	 * \brief Get number of frequency bands
	 * \return Number of frequency bands
	 */
	size_t getFrequencyAbscissaCount() const { return m_frequencyAbscissa.size(); }

protected:
	bool decodeHeader() override;

	/**
	 * \brief Set displayed frequency range
	 * \param minimumDisplayedFrequency Minimum frequency to display
	 * \param maximumDisplayedFrequency Maximum frequency to display
	 */
	//TODO (if min/max computation should be restricted to this range)
	/*
	void setDisplayedFrequencyRange(double minimumDisplayedFrequency, double maximumDisplayedFrequency);*/

	/** \name Frequency bands management */
	//@{


	/**
	 * \brief Get width of a frequency band (in Hz)
	 * \return Frequency band width
	 */
	// double getFrequencyBandWidth();

	/**
	 * \brief Get frequency band start frequency
	 * \param index Index of frequency band
	 * \return Frequency band start if it could be retrieved, 0 otherwise
	 */
	// double getFrequencyBandStart(const size_t index);

	/**
	 * \brief Get frequency band stop frequency
	 * \param index Index of frequency band
	 * \return Frequency band stop if it could be retrieved, 0 otherwise
	 */
	// double getFrequencyBandStop(const size_t index);

	//@}

private:
	std::vector<double> m_frequencyAbscissa;
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
