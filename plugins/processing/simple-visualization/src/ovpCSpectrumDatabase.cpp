#include "ovpCSpectrumDatabase.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::*/Kernel;

using namespace /*OpenViBE::*/Plugins;
using namespace SimpleVisualization;

using namespace /*OpenViBE::*/Toolkit;

using namespace std;

bool CSpectrumDatabase::initialize()
{
	if (m_decoder != nullptr) { return false; }

	m_decoder = &m_parentPlugin.getAlgorithmManager().getAlgorithm(
		m_parentPlugin.getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SpectrumDecoder));

	m_decoder->initialize();

	return true;
}

//double CSpectrumDatabase::getFrequencyBandWidth()
//{
//	if(m_frequencyAbscissa.size() == 0) { return 0; }
//	else { return m_oFrequencyBands[0].second - m_oFrequencyBands[0].first; }
//}

//double CSpectrumDatabase::getFrequencyBandStart(const size_t index)
//{
//	if(m_oFrequencyBands.size() == 0) { return 0; }
//	else { return m_oFrequencyBands[index].first; }
//}

//double CSpectrumDatabase::getFrequencyBandStop(const size_t index)
//{
//	if(index >= m_oFrequencyBands.size()) { return 0; }
//	else { return m_oFrequencyBands[index].second; }
//}

bool CSpectrumDatabase::decodeHeader()
{
	//retrieve spectrum header
	TParameterHandler<IMatrix*> frequencyAbscissaMatrix;
	frequencyAbscissaMatrix.initialize(m_decoder->getOutputParameter(OVP_GD_Algorithm_SpectrumDecoder_OutputParameterId_FrequencyAbscissa));

	//store frequency bands
	for (size_t i = 0; i < frequencyAbscissaMatrix->getDimensionSize(0); ++i) { m_frequencyAbscissa.push_back(frequencyAbscissaMatrix->getBuffer()[i]); }

	CStreamedMatrixDatabase::decodeHeader();

	return true;
}
