#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <gtk/gtk.h>
#include <map>
#include <vector>


namespace OpenViBE {
namespace Plugins {
namespace SimpleVisualization {
class CAlgorithmLevelMeasure final : public Toolkit::TAlgorithm<IAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TAlgorithm<IAlgorithm>, OVP_ClassId_Algorithm_LevelMeasure)

protected:

	Kernel::TParameterHandler<IMatrix*> m_ipMatrix;
	Kernel::TParameterHandler<GtkWidget*> m_opMainWidget;
	Kernel::TParameterHandler<GtkWidget*> m_opToolbarWidget;

	GtkBuilder* m_mainWidgetInterface    = nullptr;
	GtkBuilder* m_toolbarWidgetInterface = nullptr;
	GtkWidget* m_mainWindow              = nullptr;
	GtkWidget* m_toolbarWidget           = nullptr;

public:

	using progress_bar_t = struct
	{
		GtkProgressBar* bar;
		size_t score;
		bool lastWasOverThreshold;
	};

	std::vector<progress_bar_t> m_ProgressBar;
	bool m_ShowPercentages = false;
	double m_Threshold     = 0.1;
};

class CAlgorithmLevelMeasureDesc final : public IAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Level measure"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString("Displays sample chunk of each channel as a row of progress bars"); }
	CString getDetailedDescription() const override { return CString("Another way to look at it: Each displayed row is a histogram normalized to sum to 1"); }
	CString getCategory() const override { return CString("Simple visualization"); }
	CString getVersion() const override { return CString("1.0"); }
	virtual CString getStockItemName() const { return CString("gtk-go-up"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_LevelMeasure; }
	IPluginObject* create() override { return new CAlgorithmLevelMeasure; }

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		prototype.addInputParameter(OVP_Algorithm_LevelMeasure_InputParameterId_Matrix, "Matrix", Kernel::ParameterType_Matrix);
		prototype.addOutputParameter(OVP_Algorithm_LevelMeasure_OutputParameterId_MainWidget, "Main widget", Kernel::ParameterType_Pointer);
		prototype.addOutputParameter(OVP_Algorithm_LevelMeasure_OutputParameterId_ToolbarWidget, "Toolbar widget", Kernel::ParameterType_Pointer);
		prototype.addInputTrigger(OVP_Algorithm_LevelMeasure_InputTriggerId_Reset, "Reset");
		prototype.addInputTrigger(OVP_Algorithm_LevelMeasure_InputTriggerId_Refresh, "Refresh");
		prototype.addOutputTrigger(OVP_Algorithm_LevelMeasure_OutputTriggerId_Refreshed, "Refreshed");

		return true;
	}

	_IsDerivedFromClass_Final_(IAlgorithmDesc, OVP_ClassId_Algorithm_LevelMeasureDesc)
};
}  // namespace SimpleVisualization
}  // namespace Plugins
}  // namespace OpenViBE
