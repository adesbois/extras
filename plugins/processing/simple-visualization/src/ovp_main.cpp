#include "ovp_defines.h"
#include "algorithms/ovpCAlgorithmLevelMeasure.h"

//Presentation
#include "box-algorithms/ovpCBoxAlgorithmP300IdentifierCardVisualization.h"
#include "box-algorithms/ovpCBoxAlgorithmP300MagicCardVisualization.h"
#include "box-algorithms/ovpCBoxAlgorithmP300SpellerVisualization.h"
#include "box-algorithms/ovpCDisplayCueImage.h"
#include "box-algorithms/ovpCGrazMultiVisualization.h"
#include "box-algorithms/ovpCGrazVisualization.h"

//2D plugins
#include "box-algorithms/ovpCBoxAlgorithmErpPlot.h"
#include "box-algorithms/ovpCBoxAlgorithmLevelMeasure.h"
#include "box-algorithms/ovpCSignalDisplay.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
	context.getTypeManager().registerEnumerationEntry(OV_TypeId_BoxAlgorithmFlag, OV_AttributeId_Box_FlagIsUnstable.toString(),
													  OV_AttributeId_Box_FlagIsUnstable.toUInteger());

	context.getTypeManager().registerEnumerationType(OVP_TypeId_SphericalLinearInterpolationType, "Spherical linear interpolation type");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SphericalLinearInterpolationType, "Spline (potentials)", Spline);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SphericalLinearInterpolationType, "Spline laplacian (currents)", Laplacian);

	context.getTypeManager().registerEnumerationType(OVP_TypeId_SignalDisplayMode, "Signal display mode");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SignalDisplayMode, "Scroll", Scroll);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SignalDisplayMode, "Scan", Scan);

	context.getTypeManager().registerEnumerationType(OVP_TypeId_SignalDisplayScaling, "Signal display scaling");
	context.getTypeManager().registerEnumerationEntry(
		OVP_TypeId_SignalDisplayScaling, SimpleVisualization::CSignalDisplayView::SCALING_MODES[PerChannel].c_str(), PerChannel);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SignalDisplayScaling, SimpleVisualization::CSignalDisplayView::SCALING_MODES[Global].c_str(),
													  Global);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SignalDisplayScaling, SimpleVisualization::CSignalDisplayView::SCALING_MODES[None].c_str(),
													  None);

	context.getTypeManager().registerEnumerationType(OVP_TypeId_FeedbackMode, "Feedback display mode");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_FeedbackMode, "Positive Only", Positive);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_FeedbackMode, "Best Only", Best);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_FeedbackMode, "All", All);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_FeedbackMode, "None", No);


	OVP_Declare_New(SimpleVisualization::CDisplayCueImageDesc)
	OVP_Declare_New(SimpleVisualization::CSignalDisplayDesc)
	OVP_Declare_New(SimpleVisualization::CAlgorithmLevelMeasureDesc)

	OVP_Declare_New(SimpleVisualization::CGrazVisualizationDesc)
	OVP_Declare_New(SimpleVisualization::CGrazMultiVisualizationDesc)

	OVP_Declare_New(SimpleVisualization::CBoxAlgorithmP300SpellerVisualizationDesc)
	OVP_Declare_New(SimpleVisualization::CBoxAlgorithmP300IdentifierCardVisualizationDesc)
	OVP_Declare_New(SimpleVisualization::CBoxAlgorithmP300MagicCardVisualizationDesc)

	OVP_Declare_New(SimpleVisualization::CBoxAlgorithmLevelMeasureDesc)
	OVP_Declare_New(SimpleVisualization::CBoxAlgorithmErpPlotDesc)

OVP_Declare_End()
