#pragma once

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
class CAlgorithmStimulationBasedEpoching final : public Toolkit::TAlgorithm<IAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TAlgorithm<IAlgorithm>, OVP_ClassId_Algorithm_StimulationBasedEpoching)

protected:

	Kernel::TParameterHandler<IMatrix*> m_iSignal;
	Kernel::TParameterHandler<uint64_t> m_nOffsetSample;
	Kernel::TParameterHandler<IMatrix*> m_oSignal;
	Kernel::TParameterHandler<uint64_t> m_endTimeChunkToProcess;

	uint64_t m_receivedSamples        = 0;
	uint64_t m_samplesToSkip          = 0;
	uint64_t m_timeLastProcessedChunk = 0;
};

class CAlgorithmStimulationBasedEpochingDesc final : public IAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Stimulation based epoching"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString(""); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Signal processing/Epoching"); }
	CString getVersion() const override { return CString("1.0"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_StimulationBasedEpoching; }
	IPluginObject* create() override { return new CAlgorithmStimulationBasedEpoching; }

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		prototype.addInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_InputSignal, "Input signal", Kernel::ParameterType_Matrix);
		prototype.addInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_OffsetSampleCount, "Offset sample count",
									Kernel::ParameterType_Integer);
		prototype.addInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_EndTimeChunkToProcess, "End time of the chunk one wants to process",
									Kernel::ParameterType_Integer);

		prototype.addOutputParameter(OVP_Algorithm_StimulationBasedEpoching_OutputParameterId_OutputSignal, "Output signal", Kernel::ParameterType_Matrix);

		prototype.addInputTrigger(OVP_Algorithm_StimulationBasedEpoching_InputTriggerId_Reset, "Reset");
		prototype.addInputTrigger(OVP_Algorithm_StimulationBasedEpoching_InputTriggerId_PerformEpoching, "Perform epoching");

		prototype.addOutputTrigger(OVP_Algorithm_StimulationBasedEpoching_OutputTriggerId_EpochingDone, "Epoching done");

		return true;
	}

	_IsDerivedFromClass_Final_(IAlgorithmDesc, OVP_ClassId_Algorithm_StimulationBasedEpochingDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE
