#include "ovpCAlgorithmStimulationBasedEpoching.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;

bool CAlgorithmStimulationBasedEpoching::initialize()
{
	m_iSignal.initialize(getInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_InputSignal));
	m_nOffsetSample.initialize(getInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_OffsetSampleCount));
	m_oSignal.initialize(getOutputParameter(OVP_Algorithm_StimulationBasedEpoching_OutputParameterId_OutputSignal));
	m_endTimeChunkToProcess.initialize(getInputParameter(OVP_Algorithm_StimulationBasedEpoching_InputParameterId_EndTimeChunkToProcess));

	return true;
}

bool CAlgorithmStimulationBasedEpoching::uninitialize()
{
	m_oSignal.uninitialize();
	m_nOffsetSample.uninitialize();
	m_iSignal.uninitialize();
	m_endTimeChunkToProcess.uninitialize();

	return true;
}

bool CAlgorithmStimulationBasedEpoching::process()
{
	if (isInputTriggerActive(OVP_Algorithm_StimulationBasedEpoching_InputTriggerId_Reset))
	{
		m_receivedSamples        = 0;
		m_timeLastProcessedChunk = m_endTimeChunkToProcess - 1;
		m_samplesToSkip          = m_nOffsetSample;
	}

	if (isInputTriggerActive(OVP_Algorithm_StimulationBasedEpoching_InputTriggerId_PerformEpoching) &&
		(m_timeLastProcessedChunk < m_endTimeChunkToProcess))
	{
		m_timeLastProcessedChunk = m_endTimeChunkToProcess;
		const size_t niSample    = m_iSignal->getDimensionSize(1);
		const size_t noSample    = m_oSignal->getDimensionSize(1);

		if (m_samplesToSkip != 0)
		{
			if (m_samplesToSkip >= niSample)
			{
				m_samplesToSkip -= niSample;
				return true;
			}
		}

		size_t toCopy = size_t(niSample - m_samplesToSkip);
		if (toCopy >= noSample - m_receivedSamples)
		{
			toCopy = size_t(noSample - m_receivedSamples);
			this->activateOutputTrigger(OVP_Algorithm_StimulationBasedEpoching_OutputTriggerId_EpochingDone, true);
		}

		if (toCopy)
		{
			for (size_t i = 0; i < m_iSignal->getDimensionSize(0); ++i)
			{
				memcpy(m_oSignal->getBuffer() + i * noSample + m_receivedSamples, m_iSignal->getBuffer() + i * niSample + m_samplesToSkip,
					   toCopy * sizeof(double));
			}
		}

		m_receivedSamples += toCopy;
		m_samplesToSkip = 0;
	}

	return true;
}
