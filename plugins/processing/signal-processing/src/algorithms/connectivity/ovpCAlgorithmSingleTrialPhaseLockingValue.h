#pragma once

#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include "ovpCConnectivityAlgorithm.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/FFT>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
class CAlgorithmSingleTrialPhaseLockingValue final : public CConnectivityAlgorithm
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool process() override;


	_IsDerivedFromClass_Final_(CConnectivityAlgorithm, OVP_ClassId_Algorithm_SingleTrialPhaseLockingValue)

protected:

	Kernel::IAlgorithmProxy* m_hilbertTransform = nullptr;

	Kernel::TParameterHandler<IMatrix*> ip_hilbertInput;
	Kernel::TParameterHandler<IMatrix*> op_instantaneousPhase;
};

class CAlgorithmSingleTrialPhaseLockingValueDesc final : public CConnectivityAlgorithmDesc
{
public:
	void release() override { }

	CString getName() const override { return CString("Phase Locking algorithm"); }
	CString getAuthorName() const override { return CString("Alison Cellard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Computes the Phase-Locking Value algorithm"); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Signal processing/Connectivity"); }
	CString getVersion() const override { return CString("0.1"); }
	virtual CString getStockItemName() const { return CString("gtk-execute"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_SingleTrialPhaseLockingValue; }
	IPluginObject* create() override { return new CAlgorithmSingleTrialPhaseLockingValue; }

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1, "Signal 1", Kernel::ParameterType_Matrix);
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2, "Signal 2", Kernel::ParameterType_Matrix);
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix, "Pairs of channel", Kernel::ParameterType_Matrix);

		prototype.addOutputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix, "S-PLV signal", Kernel::ParameterType_Matrix);

		prototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Initialize, "Initialize");
		prototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Process, "Process");
		prototype.addOutputTrigger(OVP_Algorithm_Connectivity_OutputTriggerId_ProcessDone, "Process done");

		return true;
	}

	_IsDerivedFromClass_Final_(CConnectivityAlgorithmDesc, OVP_ClassId_Algorithm_SingleTrialPhaseLockingValueDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE

#endif //TARGET_HAS_ThirdPartyEIGEN
