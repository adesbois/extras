#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "ovpCAlgorithmSingleTrialPhaseLockingValue.h"
#include <cmath>
#include <complex>
#include <Eigen/Dense>


using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;

using namespace Eigen;
using namespace std;


bool CAlgorithmSingleTrialPhaseLockingValue::initialize()
{
	ip_signal1.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1));
	ip_signal2.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2));

	ip_channelPairs.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix));
	op_matrix.initialize(this->getOutputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix));

	// Create algorithm instance of Hilbert transform
	m_hilbertTransform = &this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_ClassId_Algorithm_HilbertTransform));
	m_hilbertTransform->initialize();

	ip_hilbertInput.initialize(m_hilbertTransform->getInputParameter(OVP_Algorithm_HilbertTransform_InputParameterId_Matrix));
	op_instantaneousPhase.initialize(m_hilbertTransform->getOutputParameter(OVP_Algorithm_HilbertTransform_OutputParameterId_PhaseMatrix));

	return true;
}

bool CAlgorithmSingleTrialPhaseLockingValue::uninitialize()
{
	ip_signal1.uninitialize();
	ip_signal2.uninitialize();

	ip_hilbertInput.uninitialize();
	op_instantaneousPhase.uninitialize();

	ip_channelPairs.uninitialize();
	op_matrix.uninitialize();

	m_hilbertTransform->uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_hilbertTransform);

	return true;
}


bool CAlgorithmSingleTrialPhaseLockingValue::process()
{
	const std::complex<double> iComplex(0.0, 1.0);

	IMatrix* iMatrix1          = ip_signal1;
	IMatrix* iMatrix2          = ip_signal2;
	IMatrix* channelPairs      = ip_channelPairs;
	IMatrix* oMatrix           = op_matrix;
	const size_t channelCount1 = iMatrix1->getDimensionSize(0);
	size_t samplesPerChannel1  = iMatrix1->getDimensionSize(1);
	size_t samplesPerChannel2  = iMatrix2->getDimensionSize(1);
	const size_t nPairs        = ip_channelPairs->getDimensionSize(0) / 2;
	MatrixXd channelToCompare  = MatrixXd::Zero(ip_channelPairs->getDimensionSize(0), samplesPerChannel1);
	double* iMatrixBuffer1     = iMatrix1->getBuffer();
	double* iMatrixBuffer2     = iMatrix2->getBuffer();
	double* oMatrixBuffer      = oMatrix->getBuffer();
	double* hilberInputBuffer  = ip_hilbertInput->getBuffer();


	if (this->isInputTriggerActive(OVP_Algorithm_Connectivity_InputTriggerId_Initialize))
	{
		if (samplesPerChannel1 != samplesPerChannel2)
		{
			this->getLogManager() << LogLevel_Error << "Can't compute S-PLV on two signals with different lengths";
			return false;
		}

		if (samplesPerChannel1 < 2 || samplesPerChannel2 < 2)
		{
			const size_t size = samplesPerChannel1 <= samplesPerChannel2 ? samplesPerChannel1 : samplesPerChannel2;
			this->getLogManager() << LogLevel_Error << "Can't compute S-PLV, input signal size = " << size << "\n";
			return false;
		}

		// Setting size of output
		oMatrix->setDimensionCount(2); // the output matrix will have 2 dimensions
		oMatrix->setDimensionSize(0, nPairs); //
		oMatrix->setDimensionSize(1, 1);//

		for (size_t i = 0; i < nPairs; ++i)
		{
			const size_t idx = 2 * i;
			CString name1    = iMatrix1->getDimensionLabel(0, size_t(channelPairs->getBuffer()[idx]));
			CString name2    = iMatrix2->getDimensionLabel(0, size_t(channelPairs->getBuffer()[idx + 1]));
			CString name     = name1 + name2;
			oMatrix->setDimensionLabel(0, i, name);
		}

		//
		ip_hilbertInput->setDimensionCount(2);
		ip_hilbertInput->setDimensionSize(0, 1);
		ip_hilbertInput->setDimensionSize(1, samplesPerChannel1);
	}

	if (this->isInputTriggerActive(OVP_Algorithm_Connectivity_InputTriggerId_Process))
	{
		std::complex<double> sum(0.0, 0.0);

		//_______________________________________________________________________________________
		//
		// Compute S-PLV for each pairs
		//_______________________________________________________________________________________
		//

		for (size_t channel = 0; channel < nPairs; ++channel)
		{
			VectorXd channelToCompare1 = VectorXd::Zero(samplesPerChannel1);
			VectorXd channelToCompare2 = VectorXd::Zero(samplesPerChannel2);
			VectorXd phase1            = VectorXd::Zero(samplesPerChannel1);
			VectorXd phase2            = VectorXd::Zero(samplesPerChannel2);
			sum                        = 0.0;		// imaginary part is set to zero by definition

			const size_t channelIdx = 2 * channel; //Index on single channel

			//_______________________________________________________________________________________
			//
			// Form pairs with the lookup matrix given
			//_______________________________________________________________________________________
			//

			for (size_t sample = 0; sample < samplesPerChannel1; ++sample)
			{
				if (channelPairs->getBuffer()[sample] < channelCount1)
				{
					channelToCompare(channelIdx, sample)     = iMatrixBuffer1[sample + size_t(channelPairs->getBuffer()[channelIdx]) * samplesPerChannel1];
					channelToCompare(channelIdx + 1, sample) = iMatrixBuffer2[sample + size_t(channelPairs->getBuffer()[channelIdx + 1]) * samplesPerChannel2];
				}
			}


			// Retrieve the 2 channel to compare
			channelToCompare1 = channelToCompare.row(channelIdx);
			channelToCompare2 = channelToCompare.row(channelIdx + 1);

			// Apply Hilbert transform to each channel to compute instantaneous phase
			// Channel 1
			for (size_t sample = 0; sample < samplesPerChannel1; ++sample)
			{
				hilberInputBuffer[sample] = channelToCompare1(sample); // Pass channel 1 as input for the Hilbert transform algorithm
			}


			if (m_hilbertTransform->process(OVP_Algorithm_HilbertTransform_InputTriggerId_Initialize)
			) // Check initialization before doing the process on channel 1
			{
				m_hilbertTransform->process(OVP_Algorithm_HilbertTransform_InputTriggerId_Process);

				// Channel 2
				for (size_t i = 0; i < samplesPerChannel1; ++i)
				{
					phase1(i)            = op_instantaneousPhase->getBuffer()[i]; // Store instantaneous phase given by Hilbert algorithm
					hilberInputBuffer[i] = channelToCompare2(i); // Pass channel 2 as input for the Hilbert transform algorithm
				}

				// Check initialization before doing the process on channel 2
				if (m_hilbertTransform->process(OVP_Algorithm_HilbertTransform_InputTriggerId_Initialize))
				{
					m_hilbertTransform->process(OVP_Algorithm_HilbertTransform_InputTriggerId_Process);

					// Compute S-PLV and store it in opMatrixBuffer for each pair
					for (size_t i = 0; i < samplesPerChannel1; ++i)
					{
						phase2(i) = op_instantaneousPhase->getBuffer()[i]; // Store instantaneous phase given by Hilbert algorithm
						sum += exp(iComplex * (phase1(i) - phase2(i)));
					}

					oMatrixBuffer[channel] = abs(sum) / samplesPerChannel1;
				}
			}

			else // Display error if initialization of Hilbert transform was unsuccessful
			{
				this->getLogManager() << LogLevel_Error << "Hilbert transform initialization returned bad status\n";
			}
		}

		this->activateOutputTrigger(OVP_Algorithm_Connectivity_OutputTriggerId_ProcessDone, true);
	}

	return true;
}


#endif //TARGET_HAS_ThirdPartyEIGEN
