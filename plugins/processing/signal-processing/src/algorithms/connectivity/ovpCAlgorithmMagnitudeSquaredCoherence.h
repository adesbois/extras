#pragma once

#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include "ovpCConnectivityAlgorithm.h"
#include <Eigen/Dense>
#include <unsupported/Eigen/FFT>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
class CAlgorithmMagnitudeSquaredCoherence : public CConnectivityAlgorithm
{
public:

	void release() override { delete this; }

	bool computePeriodogram(const Eigen::VectorXd& input, Eigen::MatrixXcd& periodograms, const Eigen::VectorXd& window, const size_t& nSegments,
							const size_t& lSegments, const size_t& nOverlap);
	bool powerSpectralDensity(const Eigen::VectorXd& input, Eigen::VectorXd& output, const Eigen::VectorXd& window, const size_t& nSegments,
							  const size_t& lSegments, const size_t& nOverlap);
	bool crossSpectralDensity(const Eigen::VectorXd& input1, const Eigen::VectorXd& input2, Eigen::VectorXcd& output, const Eigen::VectorXd& window,
							  const size_t& nSegments, const size_t& lSegments, const size_t& nOverlap);

	bool initialize() override;
	bool uninitialize() override;
	bool process() override;


	_IsDerivedFromClass_Final_(CConnectivityAlgorithm, OVP_ClassId_Algorithm_MagnitudeSquaredCoherence)

protected:


	Kernel::TParameterHandler<IMatrix*> ip_signal1;
	Kernel::TParameterHandler<IMatrix*> ip_signal2;

	Kernel::TParameterHandler<uint64_t> ip_sampling;

	Kernel::TParameterHandler<IMatrix*> ip_channelPairs;
	Kernel::TParameterHandler<IMatrix*> op_matrixMean;
	Kernel::TParameterHandler<IMatrix*> op_matrixSpectrum;

	Kernel::TParameterHandler<uint64_t> ip_segmentLength;
	Kernel::TParameterHandler<uint64_t> ip_overlap;
	Kernel::TParameterHandler<uint64_t> ip_windowType;

	Kernel::TParameterHandler<IMatrix*> op_freqAbscissaVector;

private:
	// why members?
	Eigen::VectorXd m_powerSpectrum1;
	Eigen::VectorXd m_powerSpectrum2;
	Eigen::VectorXcd m_crossSpectrum;

	Eigen::VectorXd m_window;  // Window weight vector
	double m_u = 0;       // Window's normalization constant

	Eigen::FFT<double, Eigen::internal::kissfft_impl<double>> m_fft; // Instance of the fft transform
};

class CAlgorithmMagnitudeSquaredCoherenceDesc final : public CConnectivityAlgorithmDesc
{
public:
	void release() override { }

	CString getName() const override { return CString("Magnitude Squared Coherence"); }
	CString getAuthorName() const override { return CString("Alison Cellard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Compute Coherence"); }

	CString getDetailedDescription() const override { return CString("Computes the Magnitude Squared Coherence algorithm between two signals"); }

	CString getCategory() const override { return CString("Signal processing/Connectivity"); }
	CString getVersion() const override { return CString("0.1"); }
	virtual CString getStockItemName() const { return CString("gtk-execute"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_MagnitudeSquaredCoherence; }
	IPluginObject* create() override { return new CAlgorithmMagnitudeSquaredCoherence; }

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		CConnectivityAlgorithmDesc::getAlgorithmPrototype(prototype);

		prototype.addInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_Window, "Window method",
									Kernel::ParameterType_Enumeration, OVP_TypeId_WindowType);
		prototype.addInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_SegLength, "Length of segments",
									Kernel::ParameterType_Integer, 32);
		prototype.addInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_Overlap, "Overlap (percentage)", Kernel::ParameterType_Integer);

		prototype.addOutputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix, "Mean coherence signal", Kernel::ParameterType_Matrix);
		prototype.addOutputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_OutputParameterId_OutputMatrixSpectrum, "Coherence spectrum",
									 Kernel::ParameterType_Matrix);
		prototype.addOutputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_OutputParameterId_FreqVector, "Frequency vector", Kernel::ParameterType_Matrix);

		prototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Initialize, "Initialize");
		prototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Process, "Process");
		prototype.addOutputTrigger(OVP_Algorithm_Connectivity_OutputTriggerId_ProcessDone, "Process done");

		return true;
	}

	_IsDerivedFromClass_Final_(CConnectivityAlgorithmDesc, OVP_ClassId_Algorithm_MagnitudeSquaredCoherenceDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE

#endif //TARGET_HAS_ThirdPartyEIGEN
