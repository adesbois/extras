#include "ovpCConnectivityAlgorithm.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;

using namespace /*OpenViBE::*/Toolkit;
using namespace /*OpenViBE::*/Plugins;

bool CConnectivityAlgorithm::initialize()
{
	ip_signal1.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1));
	ip_signal2.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2));
	ip_sampling1.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_Sampling1));
	ip_sampling2.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_Sampling2));
	ip_channelPairs.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix));
	op_matrix.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix));

	return true;
}


bool CConnectivityAlgorithm::uninitialize()
{
	ip_signal1.uninitialize();
	ip_signal2.uninitialize();
	ip_sampling1.uninitialize();
	ip_sampling2.uninitialize();
	ip_channelPairs.uninitialize();
	op_matrix.uninitialize();

	return true;
}

bool CConnectivityAlgorithm::process()
{
	if (this->isInputTriggerActive(OVP_Algorithm_Connectivity_InputTriggerId_Initialize))
	{
		IMatrix* channelPairs = ip_channelPairs;
		if (!channelPairs)
		{
			this->getLogManager() << LogLevel_ImportantWarning << "Channel lookup matrix is NULL\n Channel pairs selection failed\n";
			return false;
		}
		return true;
	}

	return true;
}
