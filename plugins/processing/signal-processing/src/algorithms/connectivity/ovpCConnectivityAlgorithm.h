#pragma once

#include "../../ovp_defines.h"
#include <toolkit/algorithms/ovtkTAlgorithm.h>

namespace OpenViBE {
namespace Plugins {
class CConnectivityAlgorithm : public Toolkit::TAlgorithm<IAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool process() override;

	_IsDerivedFromClass_(Toolkit::TAlgorithm < IAlgorithm >, OVP_ClassId_ConnectivityAlgorithm)

protected:

	Kernel::TParameterHandler<IMatrix*> ip_signal1;
	Kernel::TParameterHandler<IMatrix*> ip_signal2;
	Kernel::TParameterHandler<uint64_t> ip_sampling1;
	Kernel::TParameterHandler<uint64_t> ip_sampling2;
	Kernel::TParameterHandler<IMatrix*> ip_channelPairs;
	Kernel::TParameterHandler<IMatrix*> op_matrix;
};

class CConnectivityAlgorithmDesc : public IAlgorithmDesc
{
public:

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1, "Signal 1", Kernel::ParameterType_Matrix);
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2, "Signal 2", Kernel::ParameterType_Matrix);
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix, "Pairs of channel", Kernel::ParameterType_Matrix);
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_Sampling1, "Sampling Rate of signal 1", Kernel::ParameterType_UInteger);
		prototype.addInputParameter(OVP_Algorithm_Connectivity_InputParameterId_Sampling2, "Sampling Rate of signal 2", Kernel::ParameterType_UInteger);

		// prototype.addOutputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix,    "Matrix", Kernel::ParameterType_Matrix);

		prototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Initialize, "Initialize");
		prototype.addInputTrigger(OVP_Algorithm_Connectivity_InputTriggerId_Process, "Process");

		prototype.addOutputTrigger(OVP_Algorithm_Connectivity_OutputTriggerId_ProcessDone, "Process done");

		return true;
	}

	_IsDerivedFromClass_(IAlgorithmDesc, OVP_ClassId_ConnectivityAlgorithmDesc)
};
}  // namespace Plugins
}  // namespace OpenViBE
