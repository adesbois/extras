#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "ovpCAlgorithmMagnitudeSquaredCoherence.h"
#include <cmath>
#include <complex>
#include <Eigen/Dense>
#include <unsupported/Eigen/FFT>
#include <iostream>
#include "../basic/ovpCWindowFunctions.h"


using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;

using namespace Eigen;
using namespace std;

// Let s=w.*x(k) be the windowed input signal x at segment k. The column k of the output matrix will be fft(s).
bool CAlgorithmMagnitudeSquaredCoherence::computePeriodogram(const VectorXd& input, MatrixXcd& periodograms, const VectorXd& window,
															 const size_t& nSegments, const size_t& lSegments, const size_t& nOverlap)
{
	periodograms = MatrixXcd::Zero(lSegments, nSegments);

	// Segment input vector and apply window to segment each segment
	for (size_t k = 0; k < nSegments; ++k)
	{
		VectorXd segments(lSegments);

		for (size_t i = 0; i < lSegments; ++i) { segments(i) = input(i + k * nOverlap) * window(i); }

		// Remove the DC component of each channel. Without this we get really high coherences spread all over the 
		// spectrum when using e.g. the motor imagery dataset bundled with OpenViBE. @todo before or after windowing? here after.
		// windowedSegments = windowedSegments - VectorXd::Ones(lSegments) * windowedSegments.mean();

		VectorXcd fourier = VectorXcd::Zero(lSegments);

		// FFT of windowed segments
		m_fft.fwd(fourier, segments);
		periodograms.col(k) = fourier;
	}
	return true;
}


bool CAlgorithmMagnitudeSquaredCoherence::powerSpectralDensity(const VectorXd& input, VectorXd& output, const VectorXd& window,
															   const size_t& nSegments, const size_t& lSegments, const size_t& nOverlap)
{
	// Compute periodograms
	MatrixXcd signalFourier;
	computePeriodogram(input, signalFourier, window, nSegments, lSegments, nOverlap);

	// output(i) will be the power for the band i across segments (time) as summed from the periodogram
	output = VectorXd::Zero(lSegments);
	for (size_t k = 0; k < nSegments; ++k) { output += (signalFourier.col(k).cwiseProduct(signalFourier.col(k).conjugate())).real() / m_u; }
	output /= double(nSegments);
	return true;
}


bool CAlgorithmMagnitudeSquaredCoherence::crossSpectralDensity(const VectorXd& input1, const VectorXd& input2, VectorXcd& output,
															   const VectorXd& window, const size_t& nSegments, const size_t& lSegments,
															   const size_t& nOverlap)
{
	MatrixXcd signalFourier1, signalFourier2;

	//Compute periodograms for input 1 and 2
	computePeriodogram(input1, signalFourier1, window, nSegments, lSegments, nOverlap);
	computePeriodogram(input2, signalFourier2, window, nSegments, lSegments, nOverlap);

	output = VectorXcd::Zero(lSegments);

	for (size_t k = 0; k < nSegments; ++k) { output += signalFourier2.col(k).cwiseProduct(signalFourier1.col(k).conjugate()) / m_u; }

	output /= double(nSegments);

	// cout << "vxX " << output.transpose() << "\n";

	return true;
}

bool CAlgorithmMagnitudeSquaredCoherence::initialize()
{
	ip_signal1.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1));
	ip_signal2.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2));
	ip_sampling.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_Sampling1));

	ip_channelPairs.initialize(this->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix));
	op_matrixMean.initialize(this->getOutputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix));
	op_matrixSpectrum.initialize(this->getOutputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_OutputParameterId_OutputMatrixSpectrum));
	op_freqAbscissaVector.initialize(this->getOutputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_OutputParameterId_FreqVector));

	ip_windowType.initialize(this->getInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_Window));
	ip_segmentLength.initialize(this->getInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_SegLength));
	ip_overlap.initialize(this->getInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_Overlap));

	// Set default values
	ip_windowType    = uint64_t(EWindowType::Welch);
	ip_segmentLength = 32;
	ip_overlap       = 50;

	return true;
}

bool CAlgorithmMagnitudeSquaredCoherence::uninitialize()
{
	ip_signal1.uninitialize();
	ip_signal2.uninitialize();
	ip_sampling.uninitialize();

	ip_channelPairs.uninitialize();
	op_matrixMean.uninitialize();
	op_matrixSpectrum.uninitialize();
	op_freqAbscissaVector.uninitialize();

	ip_windowType.uninitialize();
	ip_segmentLength.uninitialize();
	ip_overlap.uninitialize();

	return true;
}


bool CAlgorithmMagnitudeSquaredCoherence::process()
{
	// Inputs
	const IMatrix* iMatrix1     = ip_signal1;
	const IMatrix* iMatrix2     = ip_signal2;
	const IMatrix* channelPairs = ip_channelPairs;
	// dumpMatrix("s1", *iMatrix1);
	// dumpMatrix("s2", *iMatrix2);

	const size_t samplesPerChannel1 = iMatrix1->getDimensionSize(1);
	const size_t samplesPerChannel2 = iMatrix2->getDimensionSize(1);
	const size_t nPair              = ip_channelPairs->getDimensionSize(0) / 2;

	const double* matrixBuffer1 = iMatrix1->getBuffer();
	const double* matrixBuffer2 = iMatrix2->getBuffer();
	const double* channelBuffer = channelPairs->getBuffer();

	const size_t segmentsLength  = size_t(ip_segmentLength);
	const EWindowType windowType = EWindowType(uint64_t(ip_windowType));
	const size_t overlapPercent  = size_t(ip_overlap);
	// cout << "OP : " << overlapPercent << "\n";

	// Outputs
	IMatrix* oMatrixMeanCoherence     = op_matrixMean;
	IMatrix* oMatrixCoherenceSpectrum = op_matrixSpectrum;

	if (this->isInputTriggerActive(OVP_Algorithm_Connectivity_InputTriggerId_Initialize))
	{
		OV_ERROR_UNLESS_KRF(samplesPerChannel1 == samplesPerChannel2, "Can't compute MSCoherence on two signals with different lengths",
							ErrorType::BadProcessing);
		OV_ERROR_UNLESS_KRF(samplesPerChannel1 !=0 && samplesPerChannel2 != 0, "Can't compute MSCoherence, input signal size = 0", ErrorType::BadProcessing);
		OV_ERROR_UNLESS_KRF(overlapPercent >= 0 && overlapPercent <= 100, "Overlap must be a value between 0 and 100", ErrorType::BadProcessing);
		OV_ERROR_UNLESS_KRF(segmentsLength > 0, "Segments must have a strictly positive length (>0)", ErrorType::BadProcessing);

		// Setting window vector. We do this only once after we know the real segment length.
		m_window = VectorXd::Zero(segmentsLength);

		// Getting window vector
		if (windowType == EWindowType::Bartlett) { WindowFunctions::bartlett(m_window, segmentsLength); }
		else if (windowType == EWindowType::Hamming) { WindowFunctions::hamming(m_window, segmentsLength); }
		else if (windowType == EWindowType::Hann) { WindowFunctions::hann(m_window, segmentsLength); }
		else if (windowType == EWindowType::Parzen) { WindowFunctions::parzen(m_window, segmentsLength); }
		else if (windowType == EWindowType::Welch) { WindowFunctions::welch(m_window, segmentsLength); }
		else // else rectangular window
		{
			m_window = VectorXd::Ones(segmentsLength);
		}
		// cout<<"window = "<<m_vecXdWindow.transpose()<<endl;

		// Calculate window normalization constant m_U. A 1/LSegments factor has been removed since it will be canceled
		m_u = 0;
		for (size_t i = 0; i < segmentsLength; ++i) { m_u += pow(m_window(i), 2); }
		// cout << "norm " << m_U << "\n";

		// Setting size of outputs
		oMatrixMeanCoherence->setDimensionCount(2); // the output matrix will have 2 dimensions
		oMatrixMeanCoherence->setDimensionSize(0, nPair);
		oMatrixMeanCoherence->setDimensionSize(1, 1); // Compute the mean so only one value

		oMatrixCoherenceSpectrum->setDimensionCount(2); // the output matrix will have 2 dimensions
		oMatrixCoherenceSpectrum->setDimensionSize(0, nPair);
		oMatrixCoherenceSpectrum->setDimensionSize(1, segmentsLength);

		op_freqAbscissaVector->setDimensionCount(1);
		op_freqAbscissaVector->setDimensionSize(0, segmentsLength);

		// Setting name of output channels for visualization
		for (size_t i = 0; i < nPair; ++i)
		{
			const size_t idx = 2 * i;
			CString name1    = iMatrix1->getDimensionLabel(0, size_t(channelBuffer[idx + 0]));
			CString name2    = iMatrix2->getDimensionLabel(0, size_t(channelBuffer[idx + 1]));
			CString name     = name1 + " " + name2;

			oMatrixMeanCoherence->setDimensionLabel(0, i, name);
			oMatrixCoherenceSpectrum->setDimensionLabel(0, i, name);
		}

		// Create frequency vector for the spectrum encoder
		double* buffer = op_freqAbscissaVector->getBuffer();
		for (size_t i = 0; i < segmentsLength; ++i) { buffer[i] = i * (double(ip_sampling) / segmentsLength); }
	}

	if (this->isInputTriggerActive(OVP_Algorithm_Connectivity_InputTriggerId_Process))
	{
		const size_t nOverlap = overlapPercent * segmentsLength / 100; // Convert percentage in samples

		// Calculate number of segment on data set giving segment's length and overlap
		const size_t nSegments = nOverlap != 0 ? (samplesPerChannel1 - segmentsLength) / nOverlap + 1 : samplesPerChannel1 / segmentsLength;

		//_______________________________________________________________________________________
		//
		// Compute MSC for each pair
		//_______________________________________________________________________________________
		//

		for (size_t channel = 0; channel < nPair; ++channel)
		{
			VectorXd channelToCompare1(samplesPerChannel1);
			VectorXd channelToCompare2(samplesPerChannel2);

			//_______________________________________________________________________________________
			//
			// Form pairs with the lookup matrix given
			//_______________________________________________________________________________________
			//

			const size_t channelIdx = 2 * channel; //Index of a single channel
			const size_t channel1   = size_t(channelBuffer[channelIdx + 0]);
			const size_t channel2   = size_t(channelBuffer[channelIdx + 1]);

			for (size_t s = 0; s < samplesPerChannel1; ++s) { channelToCompare1(s) = matrixBuffer1[s + channel1 * samplesPerChannel1]; }
			for (size_t s = 0; s < samplesPerChannel2; ++s) { channelToCompare2(s) = matrixBuffer2[s + channel2 * samplesPerChannel2]; }

			// Remove the DC component of each channel. Without this we get really high coherences spread all over the 
			// spectrum when using e.g. the motor imagery dataset bundled with OpenViBE. @todo before or after windowing? here before.
			channelToCompare1 = channelToCompare1 - VectorXd::Ones(samplesPerChannel1) * channelToCompare1.mean();
			channelToCompare2 = channelToCompare2 - VectorXd::Ones(samplesPerChannel2) * channelToCompare2.mean();

			/* Compute MSC */
			powerSpectralDensity(channelToCompare1, m_powerSpectrum1, m_window, nSegments, segmentsLength, nOverlap);
			powerSpectralDensity(channelToCompare2, m_powerSpectrum2, m_window, nSegments, segmentsLength, nOverlap);
			crossSpectralDensity(channelToCompare1, channelToCompare2, m_crossSpectrum, m_window, nSegments, segmentsLength, nOverlap);

			const double epsilon = 10e-3;	// discard bands with autospectral power smaller than this

			const VectorXd coherenceNum = (m_crossSpectrum.real().cwiseProduct(m_crossSpectrum.conjugate())).real();
			const VectorXd coherenceDen = m_powerSpectrum1.cwiseProduct(m_powerSpectrum2);
			const VectorXd coherence    = coherenceNum.cwiseQuotient(coherenceDen);

			double meanCohere = 0;
			size_t nValids    = 0;
			for (int i = 0; i < coherence.size() / 2; ++i) // skip symmetric part
			{
				// Write coherence to output
				double val = coherence(i);
				// if( (m_vecXdPowerSpectrum1(i)<l_Epsilon || m_vecXdPowerSpectrum2(i)<l_Epsilon)
				if (coherenceDen(i) < epsilon || val != val)
				{
					// Set to zero if both channels have very low autospectral power or if the result is NaN 
					val = 0;
				}
				else { nValids++; }	// Only include valid results in the mean

				oMatrixCoherenceSpectrum->getBuffer()[i * 2 + 0 + channel * coherence.size()] = val;
				oMatrixCoherenceSpectrum->getBuffer()[i * 2 + 1 + channel * coherence.size()] = val;

				// Compute MSC mean over frequencies
				meanCohere += val;
			}

			// Write coherence mean over valid frequencies to output. 
			// The validity test might not matter much for real data that may have power on all bands, but its critical for artificial examples.
			if (nValids) { oMatrixMeanCoherence->getBuffer()[channel] = meanCohere / nValids; }
			else { oMatrixMeanCoherence->getBuffer()[channel] = 0; }
			// cout << "Coh " << meanCohere << "\n";
		}
		this->activateOutputTrigger(OVP_Algorithm_Connectivity_OutputTriggerId_ProcessDone, true);
	}

	return true;
}
#endif //TARGET_HAS_ThirdPartyEIGEN
