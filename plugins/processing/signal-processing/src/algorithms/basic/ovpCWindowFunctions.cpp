#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "ovpCWindowFunctions.h"
#include <iostream>
//#include <cmath>
//#include <complex>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

using namespace OpenViBE;
using namespace std;
using namespace Eigen;


bool WindowFunctions::bartlett(VectorXd& window, const size_t size)
{
	for (size_t i = 0; i < size; ++i)
	{
		if (i <= (size - 1) / 2) { window(i) = 2. * i / (size - 1); }
		else if (i < size) { window(i) = 2. * ((size - 1) - i) / (size - 1); }
	}
	return true;
}

bool WindowFunctions::hamming(VectorXd& window, const size_t size)
{
	for (size_t i = 0; i < size; ++i) { window(i) = 0.54 - 0.46 * cos(2. * M_PI * i / (size - 1)); }
	return true;
}

bool WindowFunctions::hann(VectorXd& window, const size_t size)
{
	for (size_t i = 0; i < size; ++i) { window(i) = 0.5 - 0.5 * cos(2. * M_PI * i / (size - 1)); }
	return true;
}


bool WindowFunctions::parzen(VectorXd& window, const size_t size)
{
	for (size_t i = 0; i < size; ++i) { window(i) = 1. - pow((i - (size - 1.) / 2.) / ((size + 1.) / 2.), 2); }
	return true;
}


bool WindowFunctions::welch(VectorXd& window, const size_t size)
{
	for (size_t i = 0; i < size; ++i) { window(i) = 1.0 - fabs((i - (size - 1.0) / 2.0) / ((size + 1.0) / 2.0)); }
	return true;
}


#endif //TARGET_HAS_ThirdPartyEIGEN
