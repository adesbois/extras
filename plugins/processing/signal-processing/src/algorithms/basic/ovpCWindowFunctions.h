#pragma once

#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include <openvibe/ov_all.h>
#include <Eigen/Dense>

namespace WindowFunctions {
bool bartlett(Eigen::VectorXd& window, const size_t size);
bool hamming(Eigen::VectorXd& window, const size_t size);
bool hann(Eigen::VectorXd& window, const size_t size);
bool parzen(Eigen::VectorXd& window, const size_t size);
bool welch(Eigen::VectorXd& window, const size_t size);
} //namespace WindowFunctions

#endif //TARGET_HAS_ThirdPartyEIGEN
