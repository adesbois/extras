#pragma once

#if defined(TARGET_HAS_ThirdPartyITPP)

#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <itpp/base/vec.h>
#include <deque>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
class CMatrixVariance final : public Toolkit::TAlgorithm<IAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TAlgorithm<IAlgorithm>, OVP_ClassId_Algorithm_MatrixVariance)

protected:

	Kernel::TParameterHandler<uint64_t> ip_averagingMethod;
	Kernel::TParameterHandler<uint64_t> ip_matrixCount;
	Kernel::TParameterHandler<double> ip_significanceLevel;
	Kernel::TParameterHandler<IMatrix*> ip_matrix;
	Kernel::TParameterHandler<IMatrix*> op_averagedMatrix;
	Kernel::TParameterHandler<IMatrix*> op_varianceMatrix;
	Kernel::TParameterHandler<IMatrix*> op_confidenceBound;

	std::deque<itpp::Vec<double>*> m_history;

	itpp::Vec<double> m_mean;
	itpp::Vec<double> m_m;
	itpp::Vec<double> m_variance;
	size_t m_inputCounter = 0;
};

class CMatrixVarianceDesc final : public IAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Matrix variance"); }
	CString getAuthorName() const override { return CString("Dieter Devlaminck"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString(""); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Signal processing/Basic"); }
	CString getVersion() const override { return CString("1.0"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_Algorithm_MatrixVariance; }
	IPluginObject* create() override { return new CMatrixVariance(); }

	bool getAlgorithmPrototype(Kernel::IAlgorithmProto& prototype) const override
	{
		prototype.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_Matrix, "Matrix", Kernel::ParameterType_Matrix);
		prototype.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_MatrixCount, "Matrix count", Kernel::ParameterType_UInteger);
		prototype.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_SignificanceLevel, "Significance Level", Kernel::ParameterType_UInteger);
		prototype.addInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_AveragingMethod, "Averaging Method", Kernel::ParameterType_UInteger);

		prototype.addOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_AveragedMatrix, "Averaged matrix", Kernel::ParameterType_Matrix);
		prototype.addOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_Variance, "Matrix variance", Kernel::ParameterType_Matrix);
		prototype.addOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_ConfidenceBound, "Confidence bound", Kernel::ParameterType_Matrix);

		prototype.addInputTrigger(OVP_Algorithm_MatrixVariance_InputTriggerId_Reset, "Reset");
		prototype.addInputTrigger(OVP_Algorithm_MatrixVariance_InputTriggerId_FeedMatrix, "Feed matrix");
		prototype.addInputTrigger(OVP_Algorithm_MatrixVariance_InputTriggerId_ForceAverage, "Force average");

		prototype.addOutputTrigger(OVP_Algorithm_MatrixVariance_OutputTriggerId_AveragePerformed, "Average performed");

		return true;
	}

	_IsDerivedFromClass_Final_(IAlgorithmDesc, OVP_ClassId_Algorithm_MatrixVarianceDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE

#endif
