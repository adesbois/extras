#if defined(TARGET_HAS_ThirdPartyITPP)

#include "ovpCMatrixVariance.h"

#include <cmath>

// the boost version used at the moment of writing this caused 4800 by internal call to "int _isnan" in a bool-returning function.
#if defined(WIN32)
#pragma warning (disable : 4800)
#endif

#include <boost/math/distributions/students_t.hpp>
#include <itpp/base/vec.h>
#include <itpp/base/math/elem_math.h>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;

using namespace boost::math;
using namespace itpp;

// ________________________________________________________________________________________________________________
//

bool CMatrixVariance::initialize()
{
	ip_averagingMethod.initialize(getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_AveragingMethod));
	ip_matrixCount.initialize(getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_MatrixCount));
	ip_significanceLevel.initialize(getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_SignificanceLevel));
	ip_matrix.initialize(getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_Matrix));
	op_averagedMatrix.initialize(getOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_AveragedMatrix));
	op_varianceMatrix.initialize(getOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_Variance));
	op_confidenceBound.initialize(getOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_ConfidenceBound));

	return true;
}

bool CMatrixVariance::uninitialize()
{
	for (auto it = m_history.begin(); it != m_history.end(); ++it) { delete *it; }
	m_history.clear();

	op_averagedMatrix.uninitialize();
	op_varianceMatrix.uninitialize();
	op_confidenceBound.uninitialize();
	ip_matrix.uninitialize();
	ip_matrixCount.uninitialize();
	ip_averagingMethod.uninitialize();
	ip_significanceLevel.uninitialize();

	return true;
}

// ________________________________________________________________________________________________________________
//

bool CMatrixVariance::process()
{
	IMatrix* iMatrix = ip_matrix;
	//IMatrix* oMatrix=op_pAveragedMatrix;

	bool shouldPerformAverage = false;

	if (this->isInputTriggerActive(OVP_Algorithm_MatrixVariance_InputTriggerId_Reset))
	{
		m_mean.set_size(int(ip_matrix->getBufferElementCount()));
		m_mean.zeros();
		m_m.set_size(int(ip_matrix->getBufferElementCount()));
		m_m.zeros();
		m_variance.set_size(int(ip_matrix->getBufferElementCount()));
		m_variance.zeros();
		m_inputCounter = 0;

		for (auto it = m_history.begin(); it != m_history.end(); ++it) { delete *it; }
		m_history.clear();

		Toolkit::Matrix::copyDescription(*op_averagedMatrix, *iMatrix);
		Toolkit::Matrix::copyDescription(*op_varianceMatrix, *iMatrix);
		Toolkit::Matrix::copyDescription(*op_confidenceBound, *iMatrix);
	}

	if (this->isInputTriggerActive(OVP_Algorithm_MatrixVariance_InputTriggerId_FeedMatrix))
	{
		const int nElement = int(iMatrix->getBufferElementCount());
		if (ip_averagingMethod == size_t(EEpochAverageMethod::Moving))
		{
			//CMatrix* swapMatrix= nullptr;

			if (m_history.size() >= ip_matrixCount)
			{
				delete m_history.front();
				m_history.pop_front();
			}
			/*else
			{
				swapMatrix=new CMatrix();
				Toolkit::Matrix::copyDescription(*lswapMatrix, *iMatrix);
			}*/
			//Toolkit::Matrix::copyContent(*swapMatrix, *iMatrix);

			Vec<double>* matrices = new Vec<double>(iMatrix->getBuffer(), nElement);
			m_history.push_back(matrices);
			shouldPerformAverage = (m_history.size() == ip_matrixCount);
		}
		else if (ip_averagingMethod == size_t(EEpochAverageMethod::MovingImmediate))
		{
			//CMatrix* swapMatrix= nullptr;

			if (m_history.size() >= ip_matrixCount)
			{
				delete m_history.front();
				m_history.pop_front();
			}
			/*else
			{
				swapMatrix=new CMatrix();
				Toolkit::Matrix::copyDescription(*swapMatrix, *iMatrix);
			}*/

			//Toolkit::Matrix::copyContent(*swapMatrix, *iMatrix);

			Vec<double>* matrices = new Vec<double>(iMatrix->getBuffer(), nElement);
			m_history.push_back(matrices);
			shouldPerformAverage = (!m_history.empty());
		}
		else if (ip_averagingMethod == size_t(EEpochAverageMethod::Block))
		{
			//CMatrix* swapMatrix=new CMatrix();

			if (m_history.size() >= ip_matrixCount)
			{
				for (auto it = m_history.begin(); it != m_history.end(); ++it) { delete *it; }
				m_history.clear();
			}

			//Toolkit::Matrix::copyDescription(*swapMatrix, *iMatrix);
			//Toolkit::Matrix::copyContent(*swapMatrix, *iMatrix);

			Vec<double>* matrices = new Vec<double>(iMatrix->getBuffer(), nElement);
			m_history.push_back(matrices);
			shouldPerformAverage = (m_history.size() == ip_matrixCount);
		}
		else if (ip_averagingMethod == size_t(EEpochAverageMethod::Cumulative))
		{
			if (!m_history.empty())
			{
				//std::cout << "size of history " << m_history.size() << "\n";
				delete m_history.front();
				m_history.pop_front();
			}
			/*else
				std::cout << "history empty \n";*/
			//CMatrix* swapMatrix=new CMatrix();

			//Toolkit::Matrix::copyDescription(*swapMatrix, *iMatrix);
			//Toolkit::Matrix::copyContent(*swapMatrix, *iMatrix);

			Vec<double>* matrices = new Vec<double>(iMatrix->getBuffer(), nElement);
			m_history.push_back(matrices);
			shouldPerformAverage = (!m_history.empty());
		}
		else { shouldPerformAverage = false; }
	}

	if (shouldPerformAverage)
	{
		if (!m_history.empty())
		{
			students_t_distribution<double> distrib(2);
			if (ip_averagingMethod == size_t(EEpochAverageMethod::Cumulative))
			{
				//incremental estimation of mean and variance
				for (auto it = m_history.begin(); it != m_history.end(); ++it)
				{
					m_inputCounter++;
					Vec<double> buffer = **it;
					Vec<double> delta  = buffer - m_mean;
					m_mean += delta / double(m_inputCounter);
					m_m += elem_mult(delta, (buffer - m_mean));
					if (m_inputCounter > 1) { m_variance = m_m / double(m_inputCounter - 1); }
				}
				distrib = students_t_distribution<double>(m_inputCounter <= 1 ? 1 : m_inputCounter - 1);
				//CMatrix swapMatrix();
				//getLogManager() << LogLevel_Info << "Variance first element " << m_Variance[0] << ", last element " << m_Variance[iMatrix->getBufferElementCount()-1] << "\n";

				memcpy(op_averagedMatrix->getBuffer(), m_mean._data(), iMatrix->getBufferElementCount() * sizeof(double));
				memcpy(op_varianceMatrix->getBuffer(), m_variance._data(), iMatrix->getBufferElementCount() * sizeof(double));
			}
			else
			{
				distrib = students_t_distribution<double>(double(ip_matrixCount) - 1);

				Toolkit::Matrix::clearContent(*op_varianceMatrix);
				Toolkit::Matrix::clearContent(*op_averagedMatrix);

				const size_t count = op_averagedMatrix->getBufferElementCount();
				const double scale = 1. / m_history.size();

				for (auto& h : m_history)
				{
					//batch computation of mean
					Vec<double> buffer    = *h;
					double* averageBuffer = op_averagedMatrix->getBuffer();
					for (int i = 0; i < int(count); ++i)
					{
						*averageBuffer += buffer[i] * scale;
						averageBuffer++;
					}
					//batch computation of variance
					averageBuffer                = op_averagedMatrix->getBuffer();
					double* matrixVarianceBuffer = op_varianceMatrix->getBuffer();
					for (int i = 0; i < int(count); ++i)
					{
						*matrixVarianceBuffer += (buffer[i] - *(averageBuffer + i)) * (buffer[i] - *(averageBuffer + i)) / (m_history.size() - 1.0F);
						matrixVarianceBuffer++;
					}
				}
				m_variance = Vec<double>(op_averagedMatrix->getBuffer(), int(count));
			}

			//computing confidence bounds
			const double q = double(quantile(complement(distrib, ip_significanceLevel / 2.0)));
			getLogManager() << LogLevel_Debug << "Quantile at " << ip_significanceLevel << " is " << q << "\n";
			Vec<double> bound;
			if (ip_averagingMethod == size_t(EEpochAverageMethod::Cumulative)) { bound = (q / sqrt(double(m_inputCounter))) * itpp::sqrt(m_variance); }
			else { bound = (q / double(ip_matrixCount)) * itpp::sqrt(m_variance); }
			memcpy(op_confidenceBound->getBuffer(), bound._data(), iMatrix->getBufferElementCount() * sizeof(double));
		}

		this->activateOutputTrigger(OVP_Algorithm_MatrixVariance_OutputTriggerId_AveragePerformed, true);
	}

	return true;
}

#endif
