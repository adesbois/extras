#if defined(TARGET_HAS_ThirdPartyFFTW3) // required by wavelet2s

#include "ovpCBoxAlgorithmInverse_DWT.h"

#include <vector>
#include <map>
#include <math.h>
#include <iostream>
#include <string>

#include "../../../contrib/packages/wavelet2d/wavelet2s.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;

bool CBoxAlgorithmInverse_DWT::initialize()
{
	const size_t nInput = this->getStaticBoxContext().getInputCount();
	m_algoInfoDecoder.initialize(*this, 0);

	m_waveletType        = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_decompositionLevel = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	m_algoXDecoder = new Toolkit::TSignalDecoder<CBoxAlgorithmInverse_DWT> [nInput];

	for (size_t o = 0; o < nInput - 1; ++o) { m_algoXDecoder[o].initialize(*this, o + 1); }

	m_encoder.initialize(*this, 0);
	return true;
}

bool CBoxAlgorithmInverse_DWT::uninitialize()
{
	const size_t nInput = this->getStaticBoxContext().getInputCount();

	m_algoInfoDecoder.uninitialize();

	for (size_t o = 0; o < nInput - 1; ++o) { m_algoXDecoder[o].uninitialize(); }

	if (m_algoXDecoder)
	{
		delete[] m_algoXDecoder;
		m_algoXDecoder = nullptr;
	}

	m_encoder.uninitialize();

	return true;
}


bool CBoxAlgorithmInverse_DWT::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmInverse_DWT::process()
{
	// the dynamic box context describes the current state of the box inputs and outputs (i.e. the chunks)
	IBoxIO& boxContext = this->getDynamicBoxContext();

	//    size_t J = std::atoi(m_decompositionLevel);
	const size_t nInput = this->getStaticBoxContext().getInputCount();
	const std::string nm(m_waveletType.toASCIIString());
	std::vector<std::vector<double>> dwtop;
	std::vector<std::vector<double>> idwt_output;
	std::vector<std::vector<double>> flag;
	std::vector<std::vector<size_t>> length;

	size_t flagReceveid = 0;

	std::vector<size_t> nChannels(nInput);
	std::vector<size_t> nSamples(nInput);

	//Check if all inputs have some information to decode
	if (boxContext.getInputChunkCount(nInput - 1) == 1)
	{
		//Decode the first input (Informations)
		for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
		{
			m_algoInfoDecoder.decode(i);

			nChannels[0] = m_algoInfoDecoder.getOutputMatrix()->getDimensionSize(0);
			nSamples[0]  = m_algoInfoDecoder.getOutputMatrix()->getDimensionSize(1);


			IMatrix* matrix = m_algoInfoDecoder.getOutputMatrix();
			double* buffer  = matrix->getBuffer();

			//this->getLogManager() << LogLevel_Warning << "buffer 0 " << (size_t)buffer0[0] << "\n";

			length.resize(nChannels[0]);
			flag.resize(nChannels[0]);

			for (size_t j = 0; j < nChannels[0]; ++j)
			{
				size_t f = 0;
				for (size_t l = 0; l < size_t(buffer[0]); ++l)
				{
					length[j].push_back(size_t(buffer[l + 1]));
					f = l;
				}

				for (size_t l = 0; l < size_t(buffer[f + 2]); ++l) { flag[j].push_back(size_t(buffer[f + 3 + l])); }
			}
			flagReceveid = 1;
		}

		//If Informations is decoded
		if (flagReceveid == 1)
		{
			//Decode each decomposition level
			for (size_t o = 0; o < nInput - 1; ++o)
			{
				//Decode data of channels
				for (size_t i = 0; i < boxContext.getInputChunkCount(o + 1); ++i)
				{
					m_algoXDecoder[o].decode(i);
					nChannels[o + 1] = m_algoXDecoder[o].getOutputMatrix()->getDimensionSize(0);
					nSamples[o + 1]  = m_algoXDecoder[o].getOutputMatrix()->getDimensionSize(1);
					IMatrix* matrix  = m_algoXDecoder[o].getOutputMatrix();
					double* buffer   = matrix->getBuffer();

					//dwtop is the dwt coefficients
					dwtop.resize(nChannels[0]);

					//Store input data (dwt coefficients) in just one vector (dwtop)
					for (size_t j = 0; j < nChannels[0]; ++j)
					{
						for (size_t k = 0; k < nSamples[o + 1]; ++k) { dwtop[j].push_back(buffer[k + j * nSamples[o + 1]]); }
					}

					//Check if received informations about dwt box are coherent with inverse dwt box settings
					if (!length[0].empty() && o == nInput - 2)
					{
						//Check if quantity of samples received are the same
						if (length[0].at(nInput - 1) == dwtop[0].size())
						{
							//Resize idwt vector
							idwt_output.resize(nChannels[0]);

							//Calculate idwt for each channel
							for (size_t j = 0; j < nChannels[0]; ++j) { idwt(dwtop[j], flag[j], nm, idwt_output[j], length[j]); }


							m_encoder.getInputSamplingRate() = 2 * m_algoXDecoder[o].getOutputSamplingRate();

							m_encoder.getInputMatrix()->setDimensionCount(2);

							m_encoder.getInputMatrix()->setDimensionSize(0, nChannels[0]);
							m_encoder.getInputMatrix()->setDimensionSize(1, length[0].at(nInput - 1));


							for (size_t j = 0; j < nChannels[0]; j++)
							{
								m_encoder.getInputMatrix()->setDimensionLabel(0, j, m_algoXDecoder[o].getOutputMatrix()->getDimensionLabel(0, j));
							}


							//Encode resultant signal to output
							for (size_t j = 0; j < nChannels[0]; ++j)
							{
								for (size_t k = 0; k < size_t(idwt_output[j].size()); ++k)
								{
									m_encoder.getInputMatrix()->getBuffer()[k + j * size_t(idwt_output[j].size())] = idwt_output[j][k];
								}
							}


							m_encoder.encodeHeader();
							boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
							m_encoder.encodeBuffer();
							boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
							m_encoder.encodeEnd();
							boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
						}
					}
				}
			}
		}
	}


	return true;
}

#endif
