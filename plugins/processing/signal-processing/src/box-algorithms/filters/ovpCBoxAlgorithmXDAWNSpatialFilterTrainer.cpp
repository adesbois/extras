#if defined TARGET_HAS_ThirdPartyITPP

#include "ovpCBoxAlgorithmXDAWNSpatialFilterTrainer.h"

#include <complex>
#include <sstream>
#include <cstdio>
#include <vector>
#include <map>

#include <itpp/base/algebra/inv.h>
#include <itpp/stat/misc_stat.h>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;

using namespace itpp;

// Taken from http://techlogbook.wordpress.com/2009/08/12/adding-generalized-eigenvalue-functions-to-it
//            http://techlogbook.wordpress.com/2009/08/12/calling-lapack-functions-from-c-codes
//            http://sourceforge.net/projects/itpp/forums/forum/115656/topic/3363490?message=7557038
//
// http://icl.cs.utk.edu/projectsfiles/f2j/javadoc/org/netlib/lapack/DSYGV.html
// http://www.lassp.cornell.edu/sethna/GeneDynamics/NetworkCodeDocumentation/lapack_8h.html#a17

namespace {
extern "C" {
// This symbol comes from LAPACK
/*
		void zggev_(char *jobvl, char *jobvr, int *n, std::complex<double> *a,
			int *lda, std::complex<double> *b, int *ldb, std::complex<double> *alpha,
			std::complex<double> *beta, std::complex<double> *vl,
			int *ldvl, std::complex<double> *vr, int *ldvr,
			std::complex<double> *work, int *lwork, double *rwork, int *info);
*/
int dsygv_(int* itype, char* jobz, char* uplo, int* n, double* a, int* lda, double* b, int* ldb, double* w, double* work, int* lwork, int* info);
}
}  // namespace

namespace itppext {
bool eig(const mat& A, const mat& B, vec& d, mat& V)
{
	it_assert_debug(A.rows() == A.cols(), "eig: Matrix A is not square");
	it_assert_debug(B.rows() == B.cols(), "eig: Matrix B is not square");
	it_assert_debug(A.rows() == B.cols(), "eig: Matrix A and B don't have the same size");

	const int worksize = 4 * A.rows(); // This may be chosen better!
	mat lA(A);
	mat lB(B);
	vec lW(A.rows());
	vec lWork(worksize);
	lW.zeros();
	lWork.zeros();

	int itype    = 1; // 1: Ax=lBx 2: ABx=lx 3: BAx=lx
	char jobz    = 'V', uplo = 'U';
	int n        = lA.rows();
	double* a    = lA._data();
	int lda      = n;
	double* b    = lB._data();
	int ldb      = n;
	double* w    = lW._data();
	int lwork    = worksize;
	double* work = lWork._data();
	int info     = 0;

	dsygv_(&itype, &jobz, &uplo, &n, a, &lda, b, &ldb, w, work, &lwork, &info);

	d = lW;
	V = lA;

	return (info == 0);
}

mat convert(const IMatrix& matrix)
{
	mat res(matrix.getDimensionSize(1), matrix.getDimensionSize(0));
	memcpy(res._data(), matrix.getBuffer(), matrix.getBufferElementCount() * sizeof(double));
	return res.transpose();
}
}  // namespace itppext

bool CBoxAlgorithmXDAWNSpatialFilterTrainer::initialize()
{
	m_stimulationDecoder.initialize(*this, 0);
	m_signalDecoder.initialize(*this, 1);
	m_evokedPotentialDecoder.initialize(*this, 2);

	m_encoder.initialize(*this, 0);

	m_stimID                             = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_spatialFilterConfigurationFilename = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_filterDim                          = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2);
	m_saveAsBoxConfig                    = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);

	return true;
}

bool CBoxAlgorithmXDAWNSpatialFilterTrainer::uninitialize()
{
	m_evokedPotentialDecoder.uninitialize();
	m_signalDecoder.uninitialize();
	m_stimulationDecoder.uninitialize();
	m_encoder.uninitialize();
	return true;
}

bool CBoxAlgorithmXDAWNSpatialFilterTrainer::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

namespace {
typedef struct
{
	uint64_t startTime;
	uint64_t endTime;
	CMatrix* matrix;
} chunk_t;
}

bool CBoxAlgorithmXDAWNSpatialFilterTrainer::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	bool shouldTrain = false;
	uint64_t date    = 0, chunkStartTime = 0, chunkEndTime = 0;

	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_stimulationDecoder.decode(i);
		if (m_stimulationDecoder.isHeaderReceived())
		{
			m_encoder.encodeHeader();
			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
		}
		if (m_stimulationDecoder.isBufferReceived())
		{
			IStimulationSet* stimSet = m_stimulationDecoder.getOutputStimulationSet();
			// See if there is a training stimulation. If several, accept the first one.
			for (size_t j = 0; j < stimSet->getStimulationCount(); ++j)
			{
				if (stimSet->getStimulationIdentifier(j) == m_stimID)
				{
					date           = stimSet->getStimulationDate(j);	// date of the last matching stimulus in the set
					chunkStartTime = boxContext.getInputChunkStartTime(0, i);
					chunkEndTime   = boxContext.getInputChunkEndTime(0, i);
					shouldTrain    = true;
					break;
				}
			}
		}
		if (m_stimulationDecoder.isEndReceived())
		{
			m_encoder.encodeEnd();
			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
		}
		boxContext.markInputAsDeprecated(0, i);
	}

	if (shouldTrain)
	{
		this->getLogManager() << LogLevel_Info << "Received train stimulation - be patient\n";
		this->getLogManager() << LogLevel_Trace << "Decoding signal chunks ...\n";

		bool isContinuous = true;
		uint64_t end      = 0;
		std::vector<chunk_t> chunks;
		for (size_t i = 0; i < boxContext.getInputChunkCount(1); ++i)
		{
			m_signalDecoder.decode(i);
			if (m_signalDecoder.isHeaderReceived())
			{
				// Don't care about the header
			}
			if (m_signalDecoder.isBufferReceived())
			{
				const IMatrix* matrix = m_signalDecoder.getOutputMatrix();
				chunk_t chunk;
				chunk.startTime = boxContext.getInputChunkStartTime(1, i);
				chunk.endTime   = boxContext.getInputChunkEndTime(1, i);
				chunk.matrix    = new CMatrix;
				Toolkit::Matrix::copy(*chunk.matrix, *matrix);
				chunks.push_back(chunk);

				if (chunk.startTime != end)
				{
					this->getLogManager() << LogLevel_Error << "Chunk " << i << " start time != last chunk end time [" << chunk.startTime << " vs " << end <<
							"]\n";
					isContinuous = false;
					break;
				}
				end = chunk.endTime;
			}
			if (m_signalDecoder.isEndReceived()) { }
			boxContext.markInputAsDeprecated(1, i);
		}

		if (!isContinuous)
		{
			// @fixme mem leak
			this->getLogManager() << LogLevel_Error << "Input signal is not continuous... Can't continue\n";
			return false;
		}

		this->getLogManager() << LogLevel_Trace << "Decoding evoked response potential chunks ...\n";

		std::vector<chunk_t> evokedPotential;
		for (size_t i = 0; i < boxContext.getInputChunkCount(2); ++i)
		{
			m_evokedPotentialDecoder.decode(i);
			if (m_evokedPotentialDecoder.isHeaderReceived())
			{
				// Don't care about the header
			}
			if (m_evokedPotentialDecoder.isBufferReceived())
			{
				const IMatrix* matrix = m_evokedPotentialDecoder.getOutputMatrix();
				chunk_t chunk;
				chunk.startTime = boxContext.getInputChunkStartTime(2, i);
				chunk.endTime   = boxContext.getInputChunkEndTime(2, i);
				chunk.matrix    = new CMatrix;
				Toolkit::Matrix::copy(*chunk.matrix, *matrix);
				evokedPotential.push_back(chunk);
			}
			if (m_evokedPotentialDecoder.isEndReceived()) { }
			boxContext.markInputAsDeprecated(2, i);
		}

		if (evokedPotential.empty())
		{
			// @fixme mem leak
			this->getLogManager() << LogLevel_Error << "No evoked potentials received... Can't continue\n";
			return false;
		}

		this->getLogManager() << LogLevel_Trace << "Averaging evoked response potential...\n";

		auto it = evokedPotential.begin();
		CMatrix averagedERPMatrixOV;
		Toolkit::Matrix::copy(averagedERPMatrixOV, *it->matrix);
		for (++it; it != evokedPotential.end(); ++it)
		{
			const double* potentialBuffer = it->matrix->getBuffer();
			double* buffer                = averagedERPMatrixOV.getBuffer();
			for (size_t j = 0; j < averagedERPMatrixOV.getBufferElementCount(); ++j) { *(buffer++) += *(potentialBuffer++); }
		}
		double* buffer = averagedERPMatrixOV.getBuffer();
		for (size_t j = 0; j < averagedERPMatrixOV.getBufferElementCount(); ++j) { (*buffer++) /= evokedPotential.size(); }

		// WARNING - OpenViBE matrices are transposed ITPP matrices !

		this->getLogManager() << LogLevel_Trace << "Converting OpenViBE matrices to IT++ matrices...\n";

		const size_t nChunk          = chunks.size();
		const size_t nChannel        = chunks.begin()->matrix->getDimensionSize(0);
		const size_t nSamplePerChunk = chunks.begin()->matrix->getDimensionSize(1);
		const size_t nSamplePerErp   = averagedERPMatrixOV.getDimensionSize(1);

		mat matrix(nChannel, nChunk * nSamplePerChunk);
		it = chunks.begin();
		for (size_t i = 0; it != chunks.end(); ++it, ++i)
		{
			const mat m = itppext::convert(*it->matrix);
			matrix.set_submatrix(0, int(i) * nSamplePerChunk, m);
		}

		mat averagedERPMatrix(nChannel, nSamplePerErp);
		averagedERPMatrix = itppext::convert(averagedERPMatrixOV);

		mat dMatrix(nChunk * nSamplePerChunk, nSamplePerErp);
		dMatrix.clear();
		for (it = evokedPotential.begin(); it != evokedPotential.end(); ++it)
		{
			// Compute index of the sample corresponding to the start of the ERP
			const uint64_t erpStartTime = it->startTime;
			const size_t erpStartIndex  = size_t(CTime(erpStartTime).toSampleCount(m_signalDecoder.getOutputSamplingRate()));

			for (size_t k = 0; k < nSamplePerErp; ++k) { dMatrix(erpStartIndex + k, k) = 1; }
		}

		const mat A = (averagedERPMatrix * inv(dMatrix.transpose() * dMatrix) * averagedERPMatrix.transpose()) * double(evokedPotential.size())
					  / double(nSamplePerChunk * nChunk);
		std::stringstream s4;
		s4 << "A :\n" << A << "\n";
		this->getLogManager() << LogLevel_Debug << s4.str() << "\n";

		const mat B = (matrix * matrix.transpose()) / double(nSamplePerChunk * nChunk);

		std::stringstream s5;
		s5 << "B :\n" << B << "\n";
		this->getLogManager() << LogLevel_Debug << s5.str() << "\n";

		// Free resources
		for (auto& c : chunks) { delete c.matrix; }
		chunks.clear();

		for (auto& ep : evokedPotential) { delete ep.matrix; }
		evokedPotential.clear();

		this->getLogManager() << LogLevel_Trace << "Computing generalized eigen vector decomposition...\n";

		mat eigenVector;
		vec eigenValue;

		if (itppext::eig(A, B, eigenValue, eigenVector))
		{
			std::map<double, vec> eigenVectors;
			for (size_t i = 0; i < nChannel; ++i)
			{
				vec v                       = eigenVector.get_col(i);
				eigenVectors[eigenValue[i]] = v / norm(v);
			}

			size_t cnt = 0;
			//We need to compute the size of the first dimension before setting the matrix
			const size_t dimension1Size = eigenVectors.size() < m_filterDim ? eigenVectors.size() : m_filterDim;

			CMatrix outputVectors;
			outputVectors.setDimensionCount(2);
			outputVectors.setDimensionSize(0, dimension1Size);
			outputVectors.setDimensionSize(1, nChannel);

			auto itR = eigenVectors.rbegin();
			for (size_t i = 0; i < dimension1Size; ++itR, i++) { for (size_t j = 0; j < nChannel; ++j) { outputVectors.getBuffer()[cnt++] = itR->second[j]; } }
			if (m_saveAsBoxConfig)
			{
				FILE* file = fopen(m_spatialFilterConfigurationFilename.toASCIIString(), "wb");
				if (!file)
				{
					this->getLogManager() << LogLevel_Error << "The file [" << m_spatialFilterConfigurationFilename << "] could not be opened for writing...";
					return false;
				}

				fprintf(file, "<OpenViBE-SettingsOverride>\n");
				fprintf(file, "\t<SettingValue>");
				for (size_t i = 0; i < outputVectors.getBufferElementCount(); ++i) { fprintf(file, "%e ", outputVectors.getBuffer()[i]); }
				fprintf(file, "</SettingValue>\n");
				fprintf(file, "\t<SettingValue>%zu</SettingValue>\n", m_filterDim);
				fprintf(file, "\t<SettingValue>%zu</SettingValue>\n", nChannel);
				fprintf(file, "\t<SettingValue></SettingValue>\n");
				fprintf(file, "</OpenViBE-SettingsOverride>\n");
				fclose(file);
			}
			else
			{
				if (!Toolkit::Matrix::saveToTextFile(outputVectors, m_spatialFilterConfigurationFilename))
				{
					this->getLogManager() << LogLevel_Error << "Unable to save to [" << m_spatialFilterConfigurationFilename << "\n";
					return false;
				}
			}

			this->getLogManager() << LogLevel_Info << "Training finished... Eigen values are ";
			itR = eigenVectors.rbegin();
			for (size_t i = 0; itR != eigenVectors.rend() && i < m_filterDim; ++itR, i++) { this->getLogManager() << " | " << double(itR->first); }
			this->getLogManager() << "\n";
		}
		else
		{
			this->getLogManager() << LogLevel_Error << "Generalized eigen vector decomposition failed...\n";
			return false;
		}

		this->getLogManager() << LogLevel_Info << "xDAWN Spatial filter trained successfully.\n";

		m_encoder.getInputStimulationSet()->clear();
		m_encoder.getInputStimulationSet()->appendStimulation(OVTK_StimulationId_TrainCompleted, date, 0);
		m_encoder.encodeBuffer();

		boxContext.markOutputAsReadyToSend(0, chunkStartTime, chunkEndTime);
	}

	return true;
}

#endif // TARGET_HAS_ThirdPartyITPP
