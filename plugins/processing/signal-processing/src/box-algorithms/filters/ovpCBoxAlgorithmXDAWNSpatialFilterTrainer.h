#pragma once

#if defined TARGET_HAS_ThirdPartyITPP

#include "../../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
class CBoxAlgorithmXDAWNSpatialFilterTrainer final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainer)

protected:

	Toolkit::TStimulationDecoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_stimulationDecoder;
	Toolkit::TSignalDecoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_signalDecoder;
	Toolkit::TSignalDecoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_evokedPotentialDecoder;

	Toolkit::TStimulationEncoder<CBoxAlgorithmXDAWNSpatialFilterTrainer> m_encoder;

	uint64_t m_stimID = 0;
	CString m_spatialFilterConfigurationFilename;
	size_t m_filterDim     = 0;
	bool m_saveAsBoxConfig = false;
};

class CBoxAlgorithmXDAWNSpatialFilterTrainerDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("xDAWN Trainer (Deprecated)"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }

	CString getShortDescription() const override
	{
		return CString(
			"Computes spatial filter coeffcients in order to get better evoked potential classification (typically used for P300 detection)");
	}

	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Signal processing/Filtering"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-missing-image"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainer; }
	IPluginObject* create() override { return new CBoxAlgorithmXDAWNSpatialFilterTrainer; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Stimulations", OV_TypeId_Stimulations);
		prototype.addInput("Session signal", OV_TypeId_Signal);
		prototype.addInput("Evoked potential epochs", OV_TypeId_Signal);
		prototype.addOutput("Train-completed Flag", OV_TypeId_Stimulations);

		prototype.addSetting("Train stimulation", OV_TypeId_Stimulation, "OVTK_StimulationId_Train");
		prototype.addSetting("Spatial filter configuration", OV_TypeId_Filename, "");
		prototype.addSetting("Filter dimension", OV_TypeId_Integer, "4");
		prototype.addSetting("Save as box config", OV_TypeId_Boolean, "true");
		// prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);
		prototype.addFlag(Kernel::BoxFlag_IsDeprecated);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_XDAWNSpatialFilterTrainerDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE

#endif // TARGET_HAS_ThirdPartyITPP
