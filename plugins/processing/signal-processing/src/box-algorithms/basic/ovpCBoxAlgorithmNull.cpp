#include "ovpCBoxAlgorithmNull.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::Plugins::*/SignalProcessing;

bool CBoxAlgorithmNull::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmNull::process()
{
	IBoxIO* boxContext  = getBoxAlgorithmContext()->getDynamicBoxContext();
	const size_t nInput = getBoxAlgorithmContext()->getStaticBoxContext()->getInputCount();
	for (size_t i = 0; i < nInput; ++i) { for (size_t j = 0; j < boxContext->getInputChunkCount(i); ++j) { boxContext->markInputAsDeprecated(i, j); } }

	return true;
}
