#pragma once

#if defined TARGET_HAS_ThirdPartyITPP

//You may have to change this path to match your folder organisation
#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <complex>

#include <itpp/itbase.h>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessingBasic {
/**
 * \class CBoxAlgorithmIFFTbox
 * \author Guillermo Andrade B. (INRIA)
 * \date Fri Jan 20 15:35:05 2012
 * \brief The class CBoxAlgorithmIFFTbox describes the box IFFT box.
 *
 */
class CBoxAlgorithmIFFTbox final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_IFFTbox)

protected:
	// Codec algorithms specified in the skeleton-generator:
	Toolkit::TSpectrumDecoder<CBoxAlgorithmIFFTbox> m_decoder[2];
	Toolkit::TSignalEncoder<CBoxAlgorithmIFFTbox> m_encoder;
private:
	itpp::Vec<std::complex<double>> m_frequencyBuffer;
	itpp::Vec<double> m_signalBuffer;
	size_t m_nSample        = 0;
	size_t m_channelsNumber = 0;
	bool m_headerSent       = false;
};

/**
 * \class CBoxAlgorithmIFFTboxDesc
 * \author Guillermo Andrade B. (INRIA)
 * \date Fri Jan 20 15:35:05 2012
 * \brief Descriptor of the box IFFT box.
 *
 */
class CBoxAlgorithmIFFTboxDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("IFFT"); }
	CString getAuthorName() const override { return CString("Guillermo Andrade B."); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Compute Inverse Fast Fourier Transformation"); }

	CString getDetailedDescription() const override { return CString("Compute Inverse Fast Fourier Transformation (depends on ITPP/fftw)"); }

	CString getCategory() const override { return CString("Signal processing/Spectral Analysis"); }
	CString getVersion() const override { return CString("0.2"); }
	CString getStockItemName() const override { return CString("gtk-execute"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_IFFTbox; }
	IPluginObject* create() override { return new CBoxAlgorithmIFFTbox; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("real part",OV_TypeId_Spectrum);
		prototype.addInput("imaginary part",OV_TypeId_Spectrum);

		prototype.addOutput("Signal output",OV_TypeId_Signal);

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_IFFTboxDesc)
};
}  // namespace SignalProcessingBasic
}  // namespace Plugins
}  // namespace OpenViBE
#endif //TARGET_HAS_ThirdPartyITPP
