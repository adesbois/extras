#include "ovpCBoxAlgorithmEpochVariance.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;

bool CEpochVariance::initialize()
{
	CIdentifier inputTypeID;
	getStaticBoxContext().getInputType(0, inputTypeID);
	if (inputTypeID == OV_TypeId_StreamedMatrix)
	{
		m_decoder = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StreamedMatrixDecoder));
		m_encoder = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StreamedMatrixEncoder));
		m_encoderForVariance = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StreamedMatrixEncoder));
		m_encoderForConfidenceBound = &getAlgorithmManager().
				getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StreamedMatrixEncoder));
	}
	else if (inputTypeID == OV_TypeId_FeatureVector)
	{
		m_decoder                   = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_FeatureVectorDecoder));
		m_encoder                   = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_FeatureVectorEncoder));
		m_encoderForVariance        = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_FeatureVectorEncoder));
		m_encoderForConfidenceBound = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_FeatureVectorEncoder));
	}
	else if (inputTypeID == OV_TypeId_Signal)
	{
		m_decoder                   = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalDecoder));
		m_encoder                   = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalEncoder));
		m_encoderForVariance        = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalEncoder));
		m_encoderForConfidenceBound = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalEncoder));
	}
	else if (inputTypeID == OV_TypeId_Spectrum)
	{
		m_decoder                   = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SpectrumDecoder));
		m_encoder                   = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SpectrumEncoder));
		m_encoderForVariance        = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SpectrumDecoder));
		m_encoderForConfidenceBound = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SpectrumDecoder));
	}
	else { return false; }
	m_decoder->initialize();
	m_encoder->initialize();
	m_encoderForVariance->initialize();
	m_encoderForConfidenceBound->initialize();

	m_matrixVariance = &getAlgorithmManager().getAlgorithm(getAlgorithmManager().createAlgorithm(OVP_ClassId_Algorithm_MatrixVariance));
	m_matrixVariance->initialize();

	if (inputTypeID == OV_TypeId_StreamedMatrix) { }
	else if (inputTypeID == OV_TypeId_FeatureVector) { }
	else if (inputTypeID == OV_TypeId_Signal)
	{
		m_encoder->getInputParameter(OVP_GD_Algorithm_SignalEncoder_InputParameterId_Sampling)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Sampling));
		m_encoderForVariance->getInputParameter(OVP_GD_Algorithm_SignalEncoder_InputParameterId_Sampling)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Sampling));
		m_encoderForConfidenceBound->getInputParameter(OVP_GD_Algorithm_SignalEncoder_InputParameterId_Sampling)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SignalDecoder_OutputParameterId_Sampling));
	}
	else if (inputTypeID == OV_TypeId_Spectrum)
	{
		m_encoder->getInputParameter(OVP_GD_Algorithm_SpectrumEncoder_InputParameterId_FrequencyAbscissa)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SpectrumDecoder_OutputParameterId_FrequencyAbscissa));
		m_encoderForVariance->getInputParameter(OVP_GD_Algorithm_SpectrumEncoder_InputParameterId_FrequencyAbscissa)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SpectrumDecoder_OutputParameterId_FrequencyAbscissa));
		m_encoderForConfidenceBound->getInputParameter(OVP_GD_Algorithm_SpectrumEncoder_InputParameterId_FrequencyAbscissa)->setReferenceTarget(
			m_decoder->
			getOutputParameter(OVP_GD_Algorithm_SpectrumDecoder_OutputParameterId_FrequencyAbscissa));

		m_encoder->getInputParameter(OVP_GD_Algorithm_SpectrumEncoder_InputParameterId_Sampling)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SpectrumDecoder_OutputParameterId_Sampling));
		m_encoderForVariance->getInputParameter(OVP_GD_Algorithm_SpectrumEncoder_InputParameterId_Sampling)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SpectrumDecoder_OutputParameterId_Sampling));
		m_encoderForConfidenceBound->getInputParameter(OVP_GD_Algorithm_SpectrumEncoder_InputParameterId_Sampling)->setReferenceTarget(
			m_decoder->getOutputParameter(OVP_GD_Algorithm_SpectrumDecoder_OutputParameterId_Sampling));
	}

	ip_averagingMethod.initialize(m_matrixVariance->getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_AveragingMethod));
	ip_matrixCount.initialize(m_matrixVariance->getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_MatrixCount));
	ip_SignificanceLevel.initialize(m_matrixVariance->getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_SignificanceLevel));

	ip_averagingMethod   = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0));
	ip_matrixCount       = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1));
	ip_SignificanceLevel = double(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2));

	m_matrixVariance->getInputParameter(OVP_Algorithm_MatrixVariance_InputParameterId_Matrix)->setReferenceTarget(
		m_decoder->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputParameterId_Matrix));
	m_encoder->getInputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_InputParameterId_Matrix)->setReferenceTarget(
		m_matrixVariance->getOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_AveragedMatrix));
	m_encoderForVariance->getInputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_InputParameterId_Matrix)->setReferenceTarget(
		m_matrixVariance->getOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_Variance));
	m_encoderForConfidenceBound->getInputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_InputParameterId_Matrix)->setReferenceTarget(
		m_matrixVariance->getOutputParameter(OVP_Algorithm_MatrixVariance_OutputParameterId_ConfidenceBound));


	if (ip_matrixCount <= 0)
	{
		getLogManager() << LogLevel_Error << "You should provide a positive number of epochs better than " << ip_matrixCount << "\n";
		return false;
	}

	return true;
}

bool CEpochVariance::uninitialize()
{
	ip_averagingMethod.uninitialize();
	ip_matrixCount.uninitialize();

	m_matrixVariance->uninitialize();
	m_encoder->uninitialize();
	m_encoderForVariance->uninitialize();
	m_encoderForConfidenceBound->uninitialize();
	m_decoder->uninitialize();

	getAlgorithmManager().releaseAlgorithm(*m_matrixVariance);
	getAlgorithmManager().releaseAlgorithm(*m_encoder);
	getAlgorithmManager().releaseAlgorithm(*m_encoderForVariance);
	getAlgorithmManager().releaseAlgorithm(*m_encoderForConfidenceBound);
	getAlgorithmManager().releaseAlgorithm(*m_decoder);

	return true;
}

bool CEpochVariance::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CEpochVariance::process()
{
	IBoxIO& boxContext  = getDynamicBoxContext();
	const size_t nInput = getStaticBoxContext().getInputCount();

	for (size_t i = 0; i < nInput; ++i)
	{
		for (size_t j = 0; j < boxContext.getInputChunkCount(i); ++j)
		{
			TParameterHandler<const IMemoryBuffer*> bufferHandle(
				m_decoder->getInputParameter(OVP_GD_Algorithm_StreamedMatrixDecoder_InputParameterId_MemoryBufferToDecode));
			TParameterHandler<IMemoryBuffer*> obufferHandle(
				m_encoder->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_OutputParameterId_EncodedMemoryBuffer));
			TParameterHandler<IMemoryBuffer*> obufferHandleForVariance(
				m_encoderForVariance->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_OutputParameterId_EncodedMemoryBuffer));
			TParameterHandler<IMemoryBuffer*> bufferHandleForConfidenceBound(
				m_encoderForConfidenceBound->getOutputParameter(OVP_GD_Algorithm_StreamedMatrixEncoder_OutputParameterId_EncodedMemoryBuffer));
			bufferHandle                   = boxContext.getInputChunk(i, j);
			obufferHandle                  = boxContext.getOutputChunk(0);
			obufferHandleForVariance       = boxContext.getOutputChunk(1);
			bufferHandleForConfidenceBound = boxContext.getOutputChunk(2);

			m_decoder->process();
			if (m_decoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputTriggerId_ReceivedHeader))
			{
				m_matrixVariance->process(OVP_Algorithm_MatrixVariance_InputTriggerId_Reset);
				m_encoder->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeHeader);
				m_encoderForVariance->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeHeader);
				m_encoderForConfidenceBound->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeHeader);
				boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
				boxContext.markOutputAsReadyToSend(1, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
				boxContext.markOutputAsReadyToSend(2, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
			}
			if (m_decoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputTriggerId_ReceivedBuffer))
			{
				m_matrixVariance->process(OVP_Algorithm_MatrixVariance_InputTriggerId_FeedMatrix);
				if (m_matrixVariance->isOutputTriggerActive(OVP_Algorithm_MatrixVariance_OutputTriggerId_AveragePerformed))
				{
					m_encoder->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeBuffer);
					m_encoderForVariance->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeBuffer);
					m_encoderForConfidenceBound->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeBuffer);
					boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
					boxContext.markOutputAsReadyToSend(1, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
					boxContext.markOutputAsReadyToSend(2, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
				}
			}
			if (m_decoder->isOutputTriggerActive(OVP_GD_Algorithm_StreamedMatrixDecoder_OutputTriggerId_ReceivedEnd))
			{
				m_encoder->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeEnd);
				m_encoderForVariance->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeEnd);
				m_encoderForConfidenceBound->process(OVP_GD_Algorithm_StreamedMatrixEncoder_InputTriggerId_EncodeEnd);
				boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
				boxContext.markOutputAsReadyToSend(1, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
				boxContext.markOutputAsReadyToSend(2, boxContext.getInputChunkStartTime(i, j), boxContext.getInputChunkEndTime(i, j));
			}

			boxContext.markInputAsDeprecated(i, j);
		}
	}

	return true;
}
