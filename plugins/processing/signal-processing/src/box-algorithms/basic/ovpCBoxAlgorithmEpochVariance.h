#pragma once

#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
class CEpochVariance final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_EpochVariance)

protected:

	Kernel::IAlgorithmProxy* m_decoder                   = nullptr;
	Kernel::IAlgorithmProxy* m_encoder                   = nullptr;
	Kernel::IAlgorithmProxy* m_encoderForVariance        = nullptr;
	Kernel::IAlgorithmProxy* m_encoderForConfidenceBound = nullptr;
	Kernel::IAlgorithmProxy* m_matrixVariance            = nullptr;

	Kernel::TParameterHandler<uint64_t> ip_matrixCount;
	Kernel::TParameterHandler<uint64_t> ip_averagingMethod;
	Kernel::TParameterHandler<double> ip_SignificanceLevel;
};

class CEpochVarianceListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	bool onInputTypeChanged(Kernel::IBox& box, const size_t index) override
	{
		CIdentifier typeID = OV_UndefinedIdentifier;
		box.getInputType(index, typeID);
		for (size_t i = 0; i < box.getOutputCount(); ++i) { box.setOutputType(i, typeID); }
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CEpochVarianceDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }
	CString getName() const override { return CString("Epoch variance"); }
	CString getAuthorName() const override { return CString("Dieter Devlaminck"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Computes variance of each sample over several epochs"); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Signal processing/Basic"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-missing-image"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_EpochVariance; }
	IPluginObject* create() override { return new CEpochVariance(); }
	IBoxListener* createBoxListener() const override { return new CEpochVarianceListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input epochs", OV_TypeId_StreamedMatrix);
		prototype.addOutput("Averaged epochs", OV_TypeId_StreamedMatrix);
		prototype.addOutput("Variance of epochs", OV_TypeId_StreamedMatrix);
		prototype.addOutput("Confidence bounds", OV_TypeId_StreamedMatrix);
		prototype.addSetting("Averaging type", OVP_TypeId_EpochAverageMethod, "Moving epoch average");
		prototype.addSetting("Epoch count", OV_TypeId_Integer, "4");
		prototype.addSetting("Significance level", OV_TypeId_Float, "0.01");
		prototype.addFlag(Kernel::BoxFlag_CanModifyInput);

		prototype.addInputSupport(OV_TypeId_StreamedMatrix);
		prototype.addInputSupport(OV_TypeId_FeatureVector);
		prototype.addInputSupport(OV_TypeId_Signal);
		prototype.addInputSupport(OV_TypeId_Spectrum);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_EpochVarianceDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE
