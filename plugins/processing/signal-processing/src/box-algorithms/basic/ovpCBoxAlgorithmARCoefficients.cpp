#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "ovpCBoxAlgorithmARCoefficients.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessing;
using namespace std;

bool CBoxAlgorithmARCoefficients::initialize()
{
	// Signal stream decoder
	m_decoder.initialize(*this, 0);

	m_method = &this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_ClassId_Algorithm_ARBurgMethod));
	m_method->initialize();

	ip_matrix.initialize(m_method->getInputParameter(OVP_Algorithm_ARBurgMethod_InputParameterId_Matrix));
	op_matrix.initialize(m_method->getOutputParameter(OVP_Algorithm_ARBurgMethod_OutputParameterId_Matrix));

	ip_order.initialize(m_method->getInputParameter(OVP_Algorithm_ARBurgMethod_InputParameterId_UInteger));
	ip_order = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0));

	// Feature vector stream encoder
	m_encoder.initialize(*this, 0);

	// The AR Burg's Method algorithm will take the matrix coming from the signal decoder:
	ip_matrix.setReferenceTarget(m_decoder.getOutputMatrix());

	// The feature vector encoder will take the matrix from the AR Burg's Method algorithm:
	m_encoder.getInputMatrix().setReferenceTarget(op_matrix);


	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmARCoefficients::uninitialize()
{
	m_decoder.uninitialize();


	ip_matrix.uninitialize();
	ip_order.uninitialize();
	op_matrix.uninitialize();

	m_method->uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_method);

	m_encoder.uninitialize();

	return true;
}

/*******************************************************************************/


bool CBoxAlgorithmARCoefficients::processInput(const size_t /*index*/)
{
	// ready to process !
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmARCoefficients::process()
{
	// the dynamic box context describes the current state of the box inputs and outputs (i.e. the chunks)
	IBoxIO& boxContext = this->getDynamicBoxContext();

	// we decode the input signal chunks
	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_decoder.decode(i);

		if (m_decoder.isHeaderReceived())
		{
			// Header received
			m_method->process(OVP_Algorithm_ARBurgMethod_InputTriggerId_Initialize);

			// Make sure the algo initialization was successful			
			if (!m_method->process(OVP_Algorithm_ARBurgMethod_InputTriggerId_Initialize))
			{
				this->getLogManager() << LogLevel_Error << "Initialization was unsuccessful\n";
				return false;
			}

			// Pass the header to the next boxes, by encoding a header on the output 0:	
			m_encoder.encodeHeader();

			// send the output chunk containing the header. The dates are the same as the input chunk:
			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
		}


		if (m_decoder.isBufferReceived())
		{
			// we process the signal matrix with our algorithm
			m_method->process(OVP_Algorithm_ARBurgMethod_InputTriggerId_Process);

			// If the process is done successfully, we can encode the buffer
			if (m_method->isOutputTriggerActive(OVP_Algorithm_ARBurgMethod_OutputTriggerId_ProcessDone))
			{
				// Encode the output buffer :
				m_encoder.encodeBuffer();

				// and send it to the next boxes :
				boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
			}
		}
		if (m_decoder.isEndReceived())
		{
			// End of stream received. This happens only once when pressing "stop". Just pass it to the next boxes so they receive the message :
			m_encoder.encodeEnd();
			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, i), boxContext.getInputChunkEndTime(0, i));
		}
	}

	return true;
}

#endif // #if defined(TARGET_HAS_ThirdPartyEIGEN)
