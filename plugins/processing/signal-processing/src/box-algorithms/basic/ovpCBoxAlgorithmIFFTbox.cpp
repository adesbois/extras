#if defined TARGET_HAS_ThirdPartyITPP

//#define __OpenViBEPlugins_BoxAlgorithm_IFFTbox_CPP__
// to get ifft:
#include <itpp/itsignal.h>
#include "ovpCBoxAlgorithmIFFTbox.h"


using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace SignalProcessingBasic;


bool CBoxAlgorithmIFFTbox::initialize()
{
	m_decoder[0].initialize(*this, 0);	// Spectrum stream real part decoder
	m_decoder[1].initialize(*this, 1);	// Spectrum stream imaginary part decoder
	m_encoder.initialize(*this, 0);		// Signal stream encoder

	m_nSample    = 0;
	m_headerSent = false;

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmIFFTbox::uninitialize()
{
	m_decoder[0].uninitialize();
	m_decoder[1].uninitialize();
	m_encoder.uninitialize();

	return true;
}

bool CBoxAlgorithmIFFTbox::processInput(const size_t /*index*/)
{
	IDynamicBoxContext& boxContext = this->getDynamicBoxContext();
	const size_t nInput            = this->getStaticBoxContext().getInputCount();

	if (boxContext.getInputChunkCount(0) == 0) { return true; }
	const uint64_t start = boxContext.getInputChunkStartTime(0, 0);
	const uint64_t end   = boxContext.getInputChunkEndTime(0, 0);
	for (size_t i = 1; i < nInput; ++i)
	{
		if (boxContext.getInputChunkCount(i) == 0) { return true; }

		if (start != boxContext.getInputChunkStartTime(i, 0) || end != boxContext.getInputChunkEndTime(i, 0))
		{
			OV_WARNING_K("Chunk dates mismatch, check stream structure or parameters");
			return false;
		}
	}

	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmIFFTbox::process()
{
	// the dynamic box context describes the current state of the box inputs and outputs (i.e. the chunks)
	IBoxIO& boxContext  = this->getDynamicBoxContext();
	const size_t nInput = this->getStaticBoxContext().getInputCount();

	size_t nHeader = 0;
	size_t nBuffer = 0;
	size_t nEnd    = 0;

	for (size_t i = 0; i < nInput; ++i)
	{
		m_decoder[i].decode(0);
		if (m_decoder[i].isHeaderReceived())
		{
			//detect if header of other input is already received 
			if (0 == nHeader)
			{
				// Header received. This happens only once when pressing "play". For example with a StreamedMatrix input, you now know the dimension count, sizes, and labels of the matrix
				// ... maybe do some process ...
				m_channelsNumber = m_decoder[i].getOutputMatrix()->getDimensionSize(0);
				m_nSample        = m_decoder[i].getOutputMatrix()->getDimensionSize(1);
				OV_ERROR_UNLESS_KRF(m_channelsNumber > 0 && m_nSample > 0, "Both dims of the input matrix must have positive size",
									ErrorType::BadProcessing);

				m_nSample = (m_nSample - 1) * 2;
				if (m_nSample == 0) { m_nSample = 1; }
			}
			else
			{
				OV_ERROR_UNLESS_KRF(
					Toolkit::Matrix::isDescriptionSimilar(*m_decoder[0].getOutputMatrix(), *m_decoder[i].getOutputMatrix(), false),
					"The matrix components of the two streams have different properties, check stream structures or parameters",
					ErrorType::BadProcessing);

				OV_ERROR_UNLESS_KRF(
					Toolkit::Matrix::isDescriptionSimilar(*m_decoder[0].getOutputFrequencyAbscissa(), *m_decoder[i].getOutputFrequencyAbscissa(),
						false), "The frequencies abscissas descriptors of the two streams have different properties, check stream structures or parameters",
					ErrorType::BadProcessing);

				OV_ERROR_UNLESS_KRF(m_decoder[0].getOutputMatrix()->getDimensionSize(1) == m_decoder[i].getOutputFrequencyAbscissa()->getDimensionSize(0),
									"Frequencies abscissas count " << m_decoder[i].getOutputFrequencyAbscissa()->getDimensionSize(0) <<
									" does not match the corresponding matrix chunk size " << m_decoder[0].getOutputMatrix()->getDimensionSize(1) <<
									", check stream structures or parameters", ErrorType::BadProcessing);

				OV_ERROR_UNLESS_KRF(m_decoder[0].getOutputSamplingRate(), "Sampling rate must be positive, check stream structures or parameters",
									ErrorType::BadProcessing);

				OV_ERROR_UNLESS_KRF(m_decoder[0].getOutputSamplingRate() == m_decoder[i].getOutputSamplingRate(),
									"Sampling rates don't match (" << m_decoder[0].getOutputSamplingRate() << " != " << m_decoder[i].getOutputSamplingRate() <<
									"), please check stream structures or parameters", ErrorType::BadProcessing);
			}

			nHeader++;
		}
		if (m_decoder[i].isBufferReceived()) { nBuffer++; }
		if (m_decoder[i].isEndReceived()) { nEnd++; }
	}

	if ((nHeader && nHeader != nInput) || (nBuffer && nBuffer != nInput) || (nEnd && nEnd != nInput))
	{
		OV_WARNING_K("Stream structure mismatch");
		return false;
	}

	if (nBuffer)
	{
		OV_ERROR_UNLESS_KRF(m_nSample, "Received buffer before header, shouldn't happen\n", ErrorType::BadProcessing);

		if (!m_headerSent)
		{
			m_signalBuffer.set_size(int(m_nSample));
			m_frequencyBuffer.set_size(int(m_nSample));

			m_encoder.getInputSamplingRate() = m_decoder[0].getOutputSamplingRate();
			m_encoder.getInputMatrix()->setDimensionCount(2);
			m_encoder.getInputMatrix()->setDimensionSize(0, m_channelsNumber);
			m_encoder.getInputMatrix()->setDimensionSize(1, m_nSample);
			for (size_t channel = 0; channel < m_channelsNumber; ++channel)
			{
				m_encoder.getInputMatrix()->setDimensionLabel(
					0, channel, m_decoder[0].getOutputMatrix()->getDimensionLabel(0, channel));
			}

			// Pass the header to the next boxes, by encoding a header on the output 0:
			m_encoder.encodeHeader();
			// send the output chunk containing the header. The dates are the same as the input chunk:
			boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, 0), boxContext.getInputChunkEndTime(0, 0));

			m_headerSent = true;
		}

		const double* bufferInput0 = m_decoder[0].getOutputMatrix()->getBuffer();
		const double* bufferInput1 = m_decoder[1].getOutputMatrix()->getBuffer();

		for (size_t channel = 0; channel < m_channelsNumber; ++channel)
		{
			for (size_t j = 0; j < m_nSample; ++j)
			{
				m_frequencyBuffer[j].real(bufferInput0[int(channel * m_nSample + j)]);
				m_frequencyBuffer[j].imag(bufferInput1[int(channel * m_nSample + j)]);
			}

			m_signalBuffer = ifft_real(m_frequencyBuffer);

			double* bufferOutput = m_encoder.getInputMatrix()->getBuffer();
			for (size_t j = 0; j < m_nSample; ++j) { bufferOutput[int(channel * m_nSample + j)] = m_signalBuffer[j]; }
		}
		m_encoder.encodeBuffer();
		boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, 0), boxContext.getInputChunkEndTime(0, 0));
	}
	if (nEnd)
	{
		// End of stream received. This happens only once when pressing "stop". Just pass it to the next boxes so they receive the message :
		m_encoder.encodeEnd();
		boxContext.markOutputAsReadyToSend(0, boxContext.getInputChunkStartTime(0, 0), boxContext.getInputChunkEndTime(0, 0));
	}

	return true;
}

#endif //TARGET_HAS_ThirdPartyITPP
