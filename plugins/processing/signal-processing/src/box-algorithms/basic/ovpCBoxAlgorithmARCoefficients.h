#pragma once

#if defined(TARGET_HAS_ThirdPartyEIGEN)

//You may have to change this path to match your folder organisation
#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
/**
 * \class CBoxAlgorithmARCoefficients
 * \author Alison Cellard (Inria)
 * \date Wed Nov 28 10:40:52 2012
 * \brief The class CBoxAlgorithmARCoefficients describes the box AR Features.
 *
 */
class CBoxAlgorithmARCoefficients final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	// Process callbacks on new input received
	bool processInput(const size_t index) override;


	bool process() override;

	// As we do with any class in openvibe, we use the macro below 
	// to associate this box to an unique identifier. 
	// The inheritance information is also made available, 
	// as we provide the superclass Toolkit::TBoxAlgorithm < IBoxAlgorithm >
	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_ARCoefficients)

protected:

	// Signal stream decoder
	Toolkit::TSignalDecoder<CBoxAlgorithmARCoefficients> m_decoder;
	// Feature vector stream encoder
	Toolkit::TStreamedMatrixEncoder<CBoxAlgorithmARCoefficients> m_encoder;

	Kernel::IAlgorithmProxy* m_method = nullptr;
	Kernel::TParameterHandler<IMatrix*> ip_matrix;
	Kernel::TParameterHandler<IMatrix*> op_matrix;
	Kernel::TParameterHandler<uint64_t> ip_order;
};


/**
 * \class CBoxAlgorithmARCoefficientsDesc
 * \author Alison Cellard (Inria)
 * \date Wed Nov 28 10:40:52 2012
 * \brief Descriptor of the box AR Features.
 *
 */
class CBoxAlgorithmARCoefficientsDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("AutoRegressive Coefficients"); }
	CString getAuthorName() const override { return CString("Alison Cellard"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }

	CString getShortDescription() const override { return CString("Estimates autoregressive (AR) coefficients from a set of signals"); }

	CString getDetailedDescription() const override { return CString("Estimates autoregressive (AR) linear model coefficients using Burg's method"); }

	CString getCategory() const override { return CString("Signal processing/Basic"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-convert"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ARCoefficients; }
	IPluginObject* create() override { return new CBoxAlgorithmARCoefficients; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("EEG Signal",OV_TypeId_Signal);
		prototype.addOutput("AR Features",OV_TypeId_StreamedMatrix);
		prototype.addSetting("Order",OV_TypeId_Integer, "1");

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ARCoefficientsDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE

#endif  // TARGET_HAS_ThirdPartyEIGEN
