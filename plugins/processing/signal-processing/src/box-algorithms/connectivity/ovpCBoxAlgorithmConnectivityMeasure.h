#pragma once

#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "../../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <cstdio>
#include <cstring>
#include <vector>
#include <iostream>
#include <Eigen/Dense>

#include "../../algorithms/connectivity/ovpCConnectivityAlgorithm.h"

namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {
static const size_t N_COMMON_SETTINGS = 3;
/**
 * \class CBoxAlgorithmConnectivityMeasure
 * \author Alison Cellard (Inria)
 * \date Fri Apr 19 11:21:04 2013
 * \brief The class CBoxAlgorithmConnectivityMeasure describes the box Connectivity Measure.
 *
 */
class CBoxAlgorithmConnectivityMeasure final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;


	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_ConnectivityMeasure)

protected:

	// Signal stream decoder and encoder
	Toolkit::TSignalDecoder<CBoxAlgorithmConnectivityMeasure> m_decoder0;	///< Decoder for input 1
	Toolkit::TSignalDecoder<CBoxAlgorithmConnectivityMeasure> m_decoder1;	///< Decoder for input 2 if needed
	Toolkit::TSignalEncoder<CBoxAlgorithmConnectivityMeasure> m_encoder0;	///< Encoder for output 1
	Toolkit::TSpectrumEncoder<CBoxAlgorithmConnectivityMeasure> m_encoder1;	///< Encoder for output 2 if needed

	Kernel::IAlgorithmProxy* m_method = nullptr;
	Kernel::TParameterHandler<IMatrix*> ip_matrix1;
	Kernel::TParameterHandler<IMatrix*> ip_matrix2;
	Kernel::TParameterHandler<uint64_t> ip_sampling1;
	Kernel::TParameterHandler<uint64_t> ip_sampling2;
	Kernel::TParameterHandler<IMatrix*> op_matrix1;// Output matrix, will store the connectivity measure
	Kernel::TParameterHandler<IMatrix*> op_matrix2; // In case of second input

	Kernel::TParameterHandler<IMatrix*> ip_channelTable;
	std::vector<size_t> m_channelTable; // Matrix storing the index of the channels required

	Kernel::TParameterHandler<uint64_t> ip_windowMethod;
	Kernel::TParameterHandler<uint64_t> ip_segmentsLength;
	Kernel::TParameterHandler<uint64_t> ip_overlap;
	// Kernel::TParameterHandler <uint64_t> ip_FFTSize;

	Kernel::TParameterHandler<IMatrix*> op_frequencies;

private:
	size_t m_nInput  = 0; // Number of inputs (1 or 2)
	size_t m_nOutput = 0;
	bool m_range1    = false;
	bool m_range2    = false;
};

class CBoxAlgorithmConnectivityMeasureListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	explicit CBoxAlgorithmConnectivityMeasureListener(const size_t customSettingBase) : m_customSettingBase(customSettingBase) { }

	bool initialize() override
	{
		m_algorithmClassID = OV_UndefinedIdentifier;
		m_method           = nullptr;
		return true;
	}

	bool uninitialize() override
	{
		if (m_method)
		{
			m_method->uninitialize();
			this->getAlgorithmManager().releaseAlgorithm(*m_method);
			m_method = nullptr;
		}
		return true;
	}


	bool onInputAdded(Kernel::IBox& box, const size_t index) override
	{
		box.setInputType(index, OV_TypeId_Signal);
		return true;
	}

	bool onInitialized(Kernel::IBox& box) override { return this->onAlgorithmClassIdentifierChanged(box); }

	bool onSettingValueChanged(Kernel::IBox& box, const size_t index) override { return index == 0 ? this->onAlgorithmClassIdentifierChanged(box) : true; }

	bool onAlgorithmClassIdentifierChanged(Kernel::IBox& box) override
	{
		CString methodName;
		const CIdentifier prevMethodID = m_algorithmClassID;
		CIdentifier id                 = OV_UndefinedIdentifier;


		box.getSettingValue(0, methodName);

		const CIdentifier methodID = this->getTypeManager().getEnumerationEntryValueFromName(
			OVP_ClassId_ConnectivityAlgorithm, methodName);
		if (methodID != m_algorithmClassID)
		{
			if (m_method)
			{
				m_method->uninitialize();
				this->getAlgorithmManager().releaseAlgorithm(*m_method);
				m_method           = nullptr;
				m_algorithmClassID = OV_UndefinedIdentifier;
			}
			if (methodID != OV_UndefinedIdentifier)
			{
				m_method = &this->getAlgorithmManager().getAlgorithm(
					this->getAlgorithmManager().createAlgorithm(methodID));
				m_method->initialize();
				m_algorithmClassID = methodID;
			}

			if (prevMethodID != OV_UndefinedIdentifier)
			{
				while (box.getSettingCount() > m_customSettingBase) { box.removeSetting(m_customSettingBase); }
				while (box.getOutputCount() > 1) { box.removeOutput(1); }
			}
		}

		if (m_method)
		{
			size_t j = 1;
			box.setOutputName(0, m_method->getOutputParameterName(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix));
			while ((id = m_method->getNextOutputParameterIdentifier(id)) != OV_UndefinedIdentifier)
			{
				if (id != OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix
					&& id != OVP_Algorithm_MagnitudeSquaredCoherence_OutputParameterId_FreqVector)
				{
					CString outputName = m_method->getOutputParameterName(id);
					box.addOutput(outputName, OVTK_TypeId_Spectrum);
					j++;
				}
			}
			while (j < box.getOutputCount()) { box.removeOutput(j); }


			size_t i = m_customSettingBase;
			while ((id = m_method->getNextInputParameterIdentifier(id)) != OV_UndefinedIdentifier)
			{
				if ((id != OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1)
					&& (id != OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2)
					&& (id != OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix)
					&& (id != OVP_Algorithm_Connectivity_InputParameterId_Sampling1)
					&& (id != OVP_Algorithm_Connectivity_InputParameterId_Sampling2))
				{
					CIdentifier typeID            = OV_UndefinedIdentifier;
					CString name                  = m_method->getInputParameterName(id);
					Kernel::IParameter* parameter = m_method->getInputParameter(id);
					Kernel::TParameterHandler<uint64_t> ip_uiParameter(parameter);
					Kernel::TParameterHandler<double> ip_dParameter(parameter);
					Kernel::TParameterHandler<bool> ip_bParameter(parameter);
					Kernel::TParameterHandler<CString*> ip_sParameter(parameter);
					char buffer[1024];
					bool valid = true;
					switch (parameter->getType())
					{
						case Kernel::ParameterType_Enumeration:
							strcpy(buffer,
								   this->getTypeManager().getEnumerationEntryNameFromValue(parameter->getSubTypeIdentifier(), ip_uiParameter).
										 toASCIIString());
							typeID = parameter->getSubTypeIdentifier();
							break;

						case Kernel::ParameterType_Integer:
						case Kernel::ParameterType_UInteger:
							strcpy(buffer, std::to_string(uint64_t(ip_uiParameter)).c_str());
							typeID = OV_TypeId_Integer;
							break;

						case Kernel::ParameterType_Boolean:
							strcpy(buffer, bool(ip_bParameter) ? "true" : "false");
							typeID = OV_TypeId_Boolean;
							break;

						case Kernel::ParameterType_Float:
							strcpy(buffer, std::to_string(double(ip_dParameter)).c_str());
							typeID = OV_TypeId_Float;
							break;
						case Kernel::ParameterType_String:
							strcpy(buffer, static_cast<CString*>(ip_sParameter)->toASCIIString());
							typeID = OV_TypeId_String;
							break;

						default:
							valid = false;
							break;
					}

					if (valid)
					{
						if (i >= box.getSettingCount()) { box.addSetting(name, typeID, buffer); }
						else
						{
							CIdentifier oldTypeID = OV_UndefinedIdentifier;
							box.getSettingType(i, oldTypeID);
							if (oldTypeID != typeID)
							{
								box.setSettingType(i, typeID);
								box.setSettingValue(i, buffer);
							}
							box.setSettingName(i, name);
						}

						i++;
					}
				}
			}

			while (i < box.getSettingCount()) { box.removeSetting(i); }
		}
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)

protected:

	CIdentifier m_algorithmClassID    = OV_UndefinedIdentifier;
	Kernel::IAlgorithmProxy* m_method = nullptr;
	const size_t m_customSettingBase;
};


/**
 * \class CBoxAlgorithmConnectivityMeasureDesc
 * \author Alison Cellard (Inria)
 * \date Fri Apr 19 11:21:04 2013
 * \brief Descriptor of the box Connectivity Measure.
 *
 */
class CBoxAlgorithmConnectivityMeasureDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Connectivity Measure"); }
	CString getAuthorName() const override { return CString("Alison Cellard"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Measure connectivity between pairs of channel"); }

	CString getDetailedDescription() const override
	{
		return CString("Measure connectivity between pairs of channel using the kind of measure chosen (PLV, MSC, etc.)");
	}

	CString getCategory() const override { return CString("Signal processing/Connectivity"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-new"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_ConnectivityMeasure; }
	IPluginObject* create() override { return new CBoxAlgorithmConnectivityMeasure; }

	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmConnectivityMeasureListener(N_COMMON_SETTINGS); }

	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }


	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input Signal", OV_TypeId_Signal);
		prototype.addFlag(Kernel::BoxFlag_CanAddInput);
		// prototype.addFlag(Kernel::BoxFlag_CanModifyInput);

		prototype.addOutput("Connectivity measure", OV_TypeId_Signal);

		prototype.addSetting("Method",OVP_ClassId_ConnectivityAlgorithm, OVP_ClassId_Algorithm_SingleTrialPhaseLockingValue.toString());
		prototype.addSetting("Pairs of channels",OV_TypeId_String, "1-2");
		prototype.addSetting("Channel Matching Method", OVP_TypeId_MatchMethod, "Smart");

		//               prototype.addFlag(Kernel::BoxFlag_CanAddSetting);

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}


	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_ConnectivityMeasureDesc)
};
}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE

#endif //TARGET_HAS_ThirdPartyEIGEN
