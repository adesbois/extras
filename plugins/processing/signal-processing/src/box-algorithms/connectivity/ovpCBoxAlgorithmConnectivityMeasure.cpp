#if defined(TARGET_HAS_ThirdPartyEIGEN)

#include "ovpCBoxAlgorithmConnectivityMeasure.h"

#include <cstdio>
#include <iostream>

/**********************************************************************************************************************
 * FindChannel and the method to split the channel list was taken from the channel selector created by Yann Renard *
 **********************************************************************************************************************/
namespace OpenViBE {
namespace Plugins {
namespace SignalProcessing {

static size_t FindChannel(const IMatrix& matrix, const std::string& channel, const EMatchMethod matchMethod, const size_t start = 0)
{
	size_t res            = std::numeric_limits<size_t>::max();
	const size_t nChannel = matrix.getDimensionSize(0);

	if (matchMethod == EMatchMethod::Name)
	{
		for (size_t i = start; i < matrix.getDimensionSize(0); ++i)
		{
			if (Toolkit::String::isAlmostEqual(matrix.getDimensionLabel(0, i), channel.c_str(), false)) { res = i; }
		}
	}
	else if (matchMethod == EMatchMethod::Index)
	{
		try
		{
			const int value = std::stoi(channel);

			// The rules for pairs selection do not allow negative indexing.
			if (value > 0)
			{
				const size_t index = size_t(value - 1); // => makes it 0-indexed !
				if (index < nChannel && start <= index) { res = index; }
			}
		}
		catch (const std::exception&) { }	// catch block intentionnaly left blank
	}
	else if (matchMethod == EMatchMethod::Smart)
	{
		if (res == std::numeric_limits<size_t>::max()) { res = FindChannel(matrix, channel, EMatchMethod::Name, start); }
		if (res == std::numeric_limits<size_t>::max()) { res = FindChannel(matrix, channel, EMatchMethod::Index, start); }
	}

	return res;
}

bool CBoxAlgorithmConnectivityMeasure::initialize()
{
	const Kernel::IBox& boxCtx = this->getStaticBoxContext();
	m_nInput                   = boxCtx.getInputCount();
	m_nOutput                  = boxCtx.getOutputCount();

	const CString sAlgorithmClassID    = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	const CIdentifier algorithmClassID = this->getTypeManager().getEnumerationEntryValueFromName(OVP_ClassId_ConnectivityAlgorithm, sAlgorithmClassID);

	// Display an error message if the algorithm is not recognized
	OV_ERROR_UNLESS_KRF(algorithmClassID != OV_UndefinedIdentifier, "Unknown connectivity algorithm [" << sAlgorithmClassID << "]",
						Kernel::ErrorType::BadSetting);

	// Create algorithm instance of type ConnectivityAlgorithm
	m_method = &this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(algorithmClassID));
	m_method->initialize();

	// Initialize
	ip_matrix1.initialize(m_method->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix1));
	ip_matrix2.initialize(m_method->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_InputMatrix2));
	ip_channelTable.initialize(m_method->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_LookupMatrix));
	op_matrix1.initialize(m_method->getOutputParameter(OVP_Algorithm_Connectivity_OutputParameterId_OutputMatrix));

	// Signal stream decoder and encoder initialization
	m_decoder0.initialize(*this, 0);
	m_encoder0.initialize(*this, 0);

	// if an input was added, creation of the corresponding decoder
	if (m_nInput == 2) { m_decoder1.initialize(*this, 1); }
		// The box can't process more than two input
	else if (m_nInput > 2)
	{
		this->getLogManager() << Kernel::LogLevel_ImportantWarning << "Incorrect number of inputs\n";
		return false;
	}

	// if an output was added, creation of the corresponding encoder
	if (m_nOutput > 1) { m_encoder1.initialize(*this, 1); }

	// if MScoherence is computed, manage corresponding settings and output
	if (algorithmClassID == OVP_ClassId_Algorithm_MagnitudeSquaredCoherence)
	{
		op_matrix2.initialize(m_method->getOutputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_OutputParameterId_OutputMatrixSpectrum));
		ip_sampling1.initialize(m_method->getInputParameter(OVP_Algorithm_Connectivity_InputParameterId_Sampling1));

		const CString sWindowMethodID    = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3);
		const CIdentifier windowMethodID = this->getTypeManager().getEnumerationEntryValueFromName(OVP_TypeId_WindowType, sWindowMethodID);

		// Initialize other settings
		ip_windowMethod.initialize(m_method->getInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_Window));
		ip_segmentsLength.initialize(m_method->getInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_SegLength));
		ip_overlap.initialize(m_method->getInputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_InputParameterId_Overlap));

		ip_windowMethod   = windowMethodID.toUInteger();
		ip_segmentsLength = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 5);
		ip_overlap        = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 4);

		m_encoder1.getInputMatrix().setReferenceTarget(op_matrix2);
		m_encoder1.getInputFrequencyAbscissa().setReferenceTarget(
			m_method->getOutputParameter(OVP_Algorithm_MagnitudeSquaredCoherence_OutputParameterId_FreqVector));
		m_encoder1.getInputSamplingRate().setReferenceTarget(m_decoder0.getOutputSamplingRate());
	}


	ip_channelTable->setDimensionCount(2);
	ip_channelTable->setDimensionSize(1, 1);

	// Set reference target
	ip_matrix1.setReferenceTarget(m_decoder0.getOutputMatrix());
	ip_sampling1.setReferenceTarget(m_decoder0.getOutputSamplingRate());

	if (m_nInput == 2) { ip_matrix2.setReferenceTarget(m_decoder1.getOutputMatrix()); }
	else { ip_matrix2.setReferenceTarget(m_decoder0.getOutputMatrix()); }

	m_encoder0.getInputMatrix().setReferenceTarget(op_matrix1);
	m_encoder0.getInputSamplingRate().setReferenceTarget(m_decoder0.getOutputSamplingRate());

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmConnectivityMeasure::uninitialize()
{
	m_method->uninitialize();
	this->getAlgorithmManager().releaseAlgorithm(*m_method);

	m_decoder0.uninitialize();
	m_encoder0.uninitialize();

	// if a second decoder algorithm was created
	if (m_nInput == 2) { m_decoder1.uninitialize(); }
	if (m_nOutput > 1) { m_encoder1.uninitialize(); }

	return true;
}


bool CBoxAlgorithmConnectivityMeasure::processInput(const size_t /*index*/)
{
	Kernel::IBoxIO& boxCtx = this->getDynamicBoxContext();
	const size_t nInput    = this->getStaticBoxContext().getInputCount();

	if (boxCtx.getInputChunkCount(0) == 0) { return true; }

	const uint64_t start = boxCtx.getInputChunkStartTime(0, 0);
	const uint64_t end   = boxCtx.getInputChunkEndTime(0, 0);

	for (size_t i = 1; i < nInput; ++i)
	{
		if (boxCtx.getInputChunkCount(i) == 0) { return true; }
		bool valid = true;
		if (start != boxCtx.getInputChunkStartTime(i, 0)) { valid = false; }
		if (end != boxCtx.getInputChunkEndTime(i, 0)) { valid = false; }
		if (!valid)
		{
			OV_WARNING_K("Chunk dates mismatch, check stream structure or parameters");
			return valid;
		}
	}

	// ready to process !
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmConnectivityMeasure::process()
{
	Kernel::IBoxIO& boxCtx = this->getDynamicBoxContext();

	bool headerReceived = false;
	bool bufferReceived = false;
	bool endReceived    = false;

	// we decode the input signal chunks
	for (size_t i = 0; i < boxCtx.getInputChunkCount(0); ++i)
	{
		m_decoder0.decode(i);

		if (m_nInput == 2)
		{
			m_decoder1.decode(i);

			if (m_decoder0.isHeaderReceived() && m_decoder1.isHeaderReceived()) { headerReceived = true; }
			if (m_decoder0.isBufferReceived() && m_decoder1.isBufferReceived()) { bufferReceived = true; }
			if (m_decoder0.isEndReceived() && m_decoder1.isEndReceived()) { endReceived = true; }
		}
		else
		{
			if (m_decoder0.isHeaderReceived()) { headerReceived = true; }
			if (m_decoder0.isBufferReceived()) { bufferReceived = true; }
			if (m_decoder0.isEndReceived()) { endReceived = true; }
		}

		// If header is received
		if (headerReceived)
		{
			//______________________________________________________________________________________________________________________________________
			//
			// Splits the channel list in order to identify channel pairs to process ***** Code adapted from channel selector *****
			//_______________________________________________________________________________________________________________________________________
			//

			// Retrieve string setting giving channel pairs
			CString channelList       = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
			const EMatchMethod method = EMatchMethod(uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2)));

			m_channelTable.clear();

			std::vector<CString> pairs;
			const size_t nPairs = split(channelList, Toolkit::String::TSplitCallback<std::vector<CString>>(pairs), OV_Value_EnumeratedStringSeparator);

			for (size_t j = 0; j < nPairs; ++j)
			{
				m_range1 = false;
				m_range2 = false;
				std::vector<CString> channel;
				const size_t nChannel = split(pairs[j], Toolkit::String::TSplitCallback<std::vector<CString>>(channel), OVP_Value_CoupledStringSeparator);

				for (size_t c = 0; c < nChannel; c = c + 2)
				{
					std::vector<CString> subChannel1;
					std::vector<CString> subChannel2;

					const size_t range1 = split(channel[c], Toolkit::String::TSplitCallback<std::vector<CString>>(subChannel1), OV_Value_RangeStringSeparator);
					const size_t range2 = split(channel[c + 1], Toolkit::String::TSplitCallback<std::vector<CString>>(subChannel2),
												OV_Value_RangeStringSeparator);

					size_t rangeStartIndex, rangeEndIndex, rangeStartIndex2, rangeEndIndex2;

					if (Toolkit::String::isAlmostEqual(subChannel1[0], "*", false)) // If all channels selected
					{
						// Select all the channels
						rangeStartIndex = 0;
						rangeEndIndex   = ip_matrix1->getDimensionSize(0) - 1;
					}
					else // Else find channels selected
					{
						rangeStartIndex = FindChannel(*ip_matrix1, subChannel1[0].toASCIIString(), method);
						rangeEndIndex   = FindChannel(*ip_matrix1, subChannel1[range1 - 1].toASCIIString(), method);
					}


					if (Toolkit::String::isAlmostEqual(subChannel2[0], "*", false))
					{
						//Select all the channels
						rangeStartIndex2 = 0;
						rangeEndIndex2   = ip_matrix1->getDimensionSize(0) - 1;
					}
					else
					{
						rangeStartIndex2 = FindChannel(*ip_matrix1, subChannel2[0].toASCIIString(), method);
						rangeEndIndex2   = FindChannel(*ip_matrix1, subChannel2[range2 - 1].toASCIIString(), method);
					}

					// When first or second part is not found but associated token is empty, don't consider this as an error
					if (rangeStartIndex == size_t(-1) && subChannel1[0] == CString("")) { rangeStartIndex = 0; }
					if (rangeEndIndex == size_t(-1) && subChannel1[range1 - 1] == CString("")) { rangeEndIndex = ip_matrix1->getDimensionSize(0) - 1; }

					if (rangeStartIndex2 == size_t(-1) && subChannel2[0] == CString("")) { rangeStartIndex2 = 0; }
					if (rangeEndIndex2 == size_t(-1) && subChannel2[range2 - 1] == CString("")) { rangeEndIndex2 = ip_matrix1->getDimensionSize(0) - 1; }

					// After these corrections, if either first or second token were not found, or if start index is greater than start index, consider this an error and invalid range
					if (rangeStartIndex == size_t(-1) || rangeEndIndex == size_t(-1) || rangeStartIndex > rangeEndIndex)
					{
						OV_WARNING_K("Invalid channel or range [" << channel[c] << "]");
					}
					else if (rangeStartIndex2 == size_t(-1) || rangeEndIndex2 == size_t(-1) || rangeStartIndex2 > rangeEndIndex2)
					{
						OV_WARNING_K("Invalid channel or range [" << channel[c+1] << "]");
					}
					else
					{
						// The ranges are valid so selects all the channels in those range
						this->getLogManager() << Kernel::LogLevel_Trace << "For range [" << channel[c] << "] :\n";
						for (size_t k = rangeStartIndex; k <= rangeEndIndex; ++k)
						{
							for (size_t l = rangeStartIndex2; l <= rangeEndIndex2; ++l)
							{
								m_channelTable.push_back(k);
								m_channelTable.push_back(l);

								this->getLogManager() << Kernel::LogLevel_Trace << "  Selected channels [" << k + 1 << "," << l + 1 << "]\n";
							}
						}
					}
				}
			}

			ip_channelTable->setDimensionSize(0, m_channelTable.size());


			// Copy the look up vector into the parameterHandler in order to pass it to the algorithm
			for (size_t j = 0; j < m_channelTable.size(); ++j) { ip_channelTable->getBuffer()[j] = double(m_channelTable[j]); }

			// Make sure the algo initialization was successful
			if (!m_method->process(OVP_Algorithm_Connectivity_InputTriggerId_Initialize))
			{
				OV_WARNING_K("Initialization was unsuccessful");
				return false;
			}

			// Pass the header to the next boxes, by encoding a header on the output 0:
			m_encoder0.encodeHeader();

			// send the output chunk containing the header. The dates are the same as the input chunk:
			boxCtx.markOutputAsReadyToSend(0, boxCtx.getInputChunkStartTime(0, i), boxCtx.getInputChunkEndTime(0, i));

			if (m_nOutput > 1)
			{
				m_encoder1.encodeHeader();
				boxCtx.markOutputAsReadyToSend(1, boxCtx.getInputChunkStartTime(0, i), boxCtx.getInputChunkEndTime(0, i));
			}
		}
		if (bufferReceived)
		{
			m_method->process(OVP_Algorithm_Connectivity_InputTriggerId_Process);

			if (m_method->isOutputTriggerActive(OVP_Algorithm_Connectivity_OutputTriggerId_ProcessDone))
			{
				// Encode the output buffer :
				m_encoder0.encodeBuffer();
				// and send it to the next boxes :
				boxCtx.markOutputAsReadyToSend(0, boxCtx.getInputChunkStartTime(0, i), boxCtx.getInputChunkEndTime(0, i));
				if (m_nOutput > 1)
				{
					m_encoder1.encodeBuffer();
					boxCtx.markOutputAsReadyToSend(1, boxCtx.getInputChunkStartTime(0, i), boxCtx.getInputChunkEndTime(0, i));
				}
			}
		}

		if (endReceived)
		{
			// End of stream received. This happens only once when pressing "stop". Just pass it to the next boxes so they receive the message :
			m_encoder0.encodeEnd();

			boxCtx.markOutputAsReadyToSend(0, boxCtx.getInputChunkStartTime(0, i), boxCtx.getInputChunkEndTime(0, i));

			if (m_nOutput > 1)
			{
				m_encoder1.encodeEnd();
				boxCtx.markOutputAsReadyToSend(1, boxCtx.getInputChunkStartTime(0, i), boxCtx.getInputChunkEndTime(0, i));
			}
		}

		// The current input chunk has been processed, and automatically discarded
	}

	return true;
}

}  // namespace SignalProcessing
}  // namespace Plugins
}  // namespace OpenViBE

#endif //#TARGET_HAS_ThirdPartyEIGEN
