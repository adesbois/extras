#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <string>
#include <queue>

#if defined(TARGET_OS_Windows)
#include <windows.h>
#endif

namespace OpenViBE {
namespace Plugins {
namespace DLLBridge {
class CDLLBridge final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;

	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_DLLBridge)

private:

	Toolkit::TDecoder<CDLLBridge>* m_decoder = nullptr;
	Toolkit::TEncoder<CDLLBridge>* m_encoder = nullptr;

	CIdentifier m_inputTypeID = OV_UndefinedIdentifier;
	CString m_dllFile;
	CString m_parameters;

	// These functions are expected from the DLL library
	// @note the inputs are non-const on purpose to ensure maximum compatibility with non-C++ dlls
	typedef void (* INITFUNC)(int* paramsLength, const char* params, int* errorCode);
	typedef void (* UNINITFUNC)(int* errorCode);
	typedef void (* PROCESSHEADERFUNC)(int* rowsIn, int* colsIn, int* samplingRateIn, int* rowsOut, int* colsOut, int* samplingRateOut,
									   int* errorCode);
	typedef void (* PROCESSFUNC)(double* matIn, double* matOut, int* errorCode);

	INITFUNC m_initialize             = nullptr;
	UNINITFUNC m_uninitialize         = nullptr;
	PROCESSHEADERFUNC m_processHeader = nullptr;
	PROCESSFUNC m_process             = nullptr;

#if defined(TARGET_OS_Windows)
	HINSTANCE m_library = nullptr;
#elif defined(TARGET_OS_Linux)
	void* m_library = nullptr;
#endif
};

class CDLLBridgeListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:
	bool onInputTypeChanged(Kernel::IBox& box, const size_t index) override
	{
		CIdentifier typeID = OV_UndefinedIdentifier;
		box.getInputType(index, typeID);
		box.setOutputType(0, typeID);
		return true;
	}

	bool onOutputTypeChanged(Kernel::IBox& box, const size_t index) override
	{
		CIdentifier typeID = OV_UndefinedIdentifier;
		box.getOutputType(index, typeID);
		box.setInputType(0, typeID);
		return true;
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

/**
* Plugin's description
*/
class CDLLBridgeDesc final : public IBoxAlgorithmDesc
{
public:
	CString getName() const override { return CString("DLL Bridge"); }
	CString getAuthorName() const override { return CString("Jussi T. Lindgren"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Process a signal or matrix stream with a DLL"); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Scripting"); }
	CString getVersion() const override { return CString("0.2"); }
	CString getStockItemName() const override { return CString("gtk-convert"); }
	void release() override { }
	CIdentifier getCreatedClass() const override { return OVP_ClassId_DLLBridge; }
	IPluginObject* create() override { return new CDLLBridge(); }
	IBoxListener* createBoxListener() const override { return new CDLLBridgeListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addSetting("DLL file", OV_TypeId_Filename, "");
		prototype.addSetting("Parameters", OV_TypeId_String, "");

		prototype.addInput("Input",OV_TypeId_Signal);
		prototype.addOutput("Output",OV_TypeId_Signal);

		prototype.addInputSupport(OV_TypeId_StreamedMatrix);
		prototype.addInputSupport(OV_TypeId_Signal);
		prototype.addOutputSupport(OV_TypeId_StreamedMatrix);
		prototype.addOutputSupport(OV_TypeId_Signal);

		prototype.addFlag(Kernel::BoxFlag_CanModifyInput);
		prototype.addFlag(Kernel::BoxFlag_CanModifyOutput);

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_DLLBridgeDesc)
};
}  // namespace DLLBridge
}  // namespace Plugins
}  // namespace OpenViBE
