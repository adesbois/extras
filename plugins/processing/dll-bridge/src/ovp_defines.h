#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_DLLBridge						OpenViBE::CIdentifier(0x6F837408, 0x35BA0312)
#define OVP_ClassId_DLLBridgeDesc					OpenViBE::CIdentifier(0x2BB7368F, 0x207D6485)

// Global defines
//---------------------------------------------------------------------------------------------------
#define OV_AttributeId_Box_FlagIsUnstable			OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)
