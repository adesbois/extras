#include <openvibe/ov_all.h>

#include "ovp_defines.h"

#include "box-algorithms/ovpCBoxAlgorithmMatlabScripting.h"

#include <vector>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
#if defined TARGET_HAS_ThirdPartyMatlab

	OVP_Declare_New(Matlab::CBoxAlgorithmMatlabScriptingDesc);
	context.getTypeManager().registerEnumerationEntry(OV_TypeId_BoxAlgorithmFlag, OV_AttributeId_Box_FlagIsUnstable.toString(),
													  OV_AttributeId_Box_FlagIsUnstable.toUInteger());

#endif // TARGET_HAS_ThirdPartyMatlab

OVP_Declare_End()
