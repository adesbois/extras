#include <vector>

#include "ovp_defines.h"
#include "toolkit/algorithms/classification/ovtkCAlgorithmPairingStrategy.h" //For comparision mecanism

#include "algorithms/ovpCAlgorithmClassifierSVM.h"

#include "box-algorithms/ovpCBoxAlgorithmOutlierRemoval.h"

#if defined TARGET_HAS_ThirdPartyEIGEN
#include "algorithms/ovpCAlgorithmClassifierMLP.h"
#endif // TARGET_HAS_ThirdPartyEIGEN

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

const char* const PAIRWISE_STRATEGY_ENUMERATION_NAME = "Pairwise Decision Strategy";

OVP_Declare_Begin()
	// SVM related
	context.getTypeManager().registerEnumerationEntry(OVTK_TypeId_ClassificationAlgorithm, "Support Vector Machine (SVM)",
													  OVP_ClassId_Algorithm_ClassifierSVM.toUInteger());
	Toolkit::registerClassificationComparisonFunction(OVP_ClassId_Algorithm_ClassifierSVM, Classification::SVMClassificationCompare);
	OVP_Declare_New(Classification::CAlgorithmClassifierSVMDesc);

	context.getTypeManager().registerEnumerationType(OVP_TypeId_SVMType, "SVM Type");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SVMType, "C-SVC", C_SVC);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SVMType, "Nu-SVC", NU_SVC);

	context.getTypeManager().registerEnumerationType(OVP_TypeId_SVMKernelType, "SVM Kernel Type");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SVMKernelType, "Linear", LINEAR);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SVMKernelType, "Polinomial", POLY);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SVMKernelType, "Radial basis function", RBF);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_SVMKernelType, "Sigmoid", SIGMOID);


	context.getTypeManager().registerEnumerationType(OVP_TypeId_ClassificationPairwiseStrategy, PAIRWISE_STRATEGY_ENUMERATION_NAME);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_ClassificationPairwiseStrategy, "Support Vector Machine (SVM)",
													  OVP_ClassId_Algorithm_ClassifierSVM.toUInteger());

	context.getTypeManager().registerEnumerationType(OVP_TypeId_OneVsOne_DecisionAlgorithms, "One vs One Decision Algorithms");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_OneVsOne_DecisionAlgorithms, "SVM Kernel Type", OVP_TypeId_SVMType.toUInteger());

#if defined TARGET_HAS_ThirdPartyEIGEN
	//MLP section
	OVP_Declare_New(Classification::CAlgorithmClassifierMLPDesc);
	context.getTypeManager().registerEnumerationEntry(OVTK_TypeId_ClassificationAlgorithm, "Multi-layer Perceptron",
													  OVP_ClassId_Algorithm_ClassifierMLP.toUInteger());
	Toolkit::registerClassificationComparisonFunction(OVP_ClassId_Algorithm_ClassifierMLP, Classification::MLPClassificationCompare);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_ClassificationPairwiseStrategy, "Multi-layer Perceptron",
													  OVP_ClassId_Algorithm_ClassifierMLP.toUInteger());

	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_OneVsOne_DecisionAlgorithms, "Multi-layer Perceptron",
													  OVP_ClassId_Algorithm_ClassifierMLP.toUInteger());

#endif // TARGET_HAS_ThirdPartyEIGEN

	// Register boxes
	OVP_Declare_New(Classification::CBoxAlgorithmOutlierRemovalDesc);

OVP_Declare_End()

#include<cmath>

bool OVFloatEqual(const double first, const double second)
{
	const double epsilon = 0.000001;
	return epsilon > fabs(first - second);
}
