#if defined TARGET_HAS_ThirdPartyEIGEN

#include "ovpCAlgorithmClassifierMLP.h"
#include "../ovp_defines.h"

#include <map>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cmath>

#include <Eigen/Dense>
#include <Eigen/Core>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::Plugins::*/Classification;
using namespace /*OpenViBE::*/Toolkit;

using namespace Eigen;

//Need to be reachable from outside
const char* const MLP_EVALUATION_FUNCTION_NAME = "Evaluation function";

static const char* const MLP_TYPE_NODE_NAME                = "MLP";
static const char* const MLP_NEURON_CONFIG_NODE_NAME       = "Neuron-configuration";
static const char* const MLP_INPUT_NEURON_COUNT_NODE_NAME  = "Input-neuron-count";
static const char* const MLP_HIDDEN_NEURON_COUNT_NODE_NAME = "Hidden-neuron-count";
static const char* const MLP_MAX_NODE_NAME                 = "Maximum";
static const char* const MLP_MIN_NODE_NAME                 = "Minimum";
static const char* const MLP_INPUT_BIAS_NODE_NAME          = "Input-bias";
static const char* const MLP_INPUT_WEIGHT_NODE_NAME        = "Input-weight";
static const char* const MLP_HIDDEN_BIAS_NODE_NAME         = "Hidden-bias";
static const char* const MLP_HIDDEN_WEIGHT_NODE_NAME       = "Hidden-weight";
static const char* const MLP_CLASS_LABEL_NODE_NAME         = "Class-label";

int Classification::MLPClassificationCompare(IMatrix& first, IMatrix& second)
{
	//We first need to find the best classification of each.
	double* buffer        = first.getBuffer();
	const double maxFirst = *(std::max_element(buffer, buffer + first.getBufferElementCount()));

	buffer                 = second.getBuffer();
	const double maxSecond = *(std::max_element(buffer, buffer + second.getBufferElementCount()));

	//Then we just compared them
	if (OVFloatEqual(maxFirst, maxSecond)) { return 0; }
	if (maxFirst > maxSecond) { return -1; }
	return 1;
}

#define MLP_DEBUG 0
#if MLP_DEBUG
void dumpMatrix(ILogManager& rMgr, const MatrixXd& mat, const CString& desc)
{
	rMgr << LogLevel_Info << desc << "\n";
	for (int i = 0; i < mat.rows(); ++i) {
		rMgr << LogLevel_Info << "Row " << i << ": ";
		for (int j = 0; j < mat.cols(); ++j) {
			rMgr << mat(i, j) << " ";
		}
		rMgr << "\n";
	}
}
#else
void dumpMatrix(ILogManager& /*rMgr*/, const MatrixXd& /*mat*/, const CString& /*desc*/) { }
#endif


bool CAlgorithmClassifierMLP::initialize()
{
	TParameterHandler<int64_t> iHidden(this->getInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_HiddenNeuronCount));
	iHidden = 3;

	TParameterHandler<XML::IXMLNode*> config(this->getOutputParameter(OVTK_Algorithm_Classifier_OutputParameterId_Config));
	config = nullptr;

	TParameterHandler<double> iAlpha(this->getInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Alpha));
	iAlpha = 0.01;
	TParameterHandler<double> iEpsilon(this->getInputParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Epsilon));
	iEpsilon = 0.000001;
	return true;
}

bool CAlgorithmClassifierMLP::uninitialize() { return true; }

bool CAlgorithmClassifierMLP::train(const IFeatureVectorSet& dataset)
{
	m_labels.clear();

	this->initializeExtraParameterMechanism();
	size_t hiddenNeuronCount = size_t(this->getInt64Parameter(OVP_Algorithm_ClassifierMLP_InputParameterId_HiddenNeuronCount));
	double alpha             = this->getDoubleParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Alpha);
	double epsilon           = this->getDoubleParameter(OVP_Algorithm_ClassifierMLP_InputParameterId_Epsilon);
	this->uninitializeExtraParameterMechanism();

	if (hiddenNeuronCount < 1)
	{
		this->getLogManager() << LogLevel_Error << "Invalid amount of neuron in the hidden layer. Fallback to default value (3)\n";
		hiddenNeuronCount = 3;
	}
	if (alpha <= 0)
	{
		this->getLogManager() << LogLevel_Error << "Invalid value for learning coefficient (" << alpha << "). Fallback to default value (0.01)\n";
		alpha = 0.01;
	}
	if (epsilon <= 0)
	{
		this->getLogManager() << LogLevel_Error << "Invalid value for stop learning condition (" << epsilon << "). Fallback to default value (0.000001)\n";
		epsilon = 0.000001;
	}

	std::map<double, size_t> classCount;
	std::map<double, VectorXd> targetList;
	//We need to compute the min and the max of data in order to normalize and center them
	for (size_t i = 0; i < dataset.getFeatureVectorCount(); ++i) { classCount[dataset[i].getLabel()]++; }
	size_t validationElementCount = 0;

	//We generate the list of class
	for (auto iter = classCount.begin(); iter != classCount.end(); ++iter)
	{
		//We keep 20% percent of the training set for the validation for each class
		validationElementCount += size_t(iter->second * 0.2);
		m_labels.push_back(iter->first);
		iter->second = size_t(iter->second * 0.2);
	}

	const size_t nbClass  = m_labels.size();
	const size_t nFeature = dataset.getFeatureVector(0).getSize();

	//Generate the target vector for each class. To save time and memory, we compute only one vector per class
	//Vector tagret looks like following [0 0 1 0] for class 3 (if 4 classes)
	for (size_t i = 0; i < nbClass; ++i)
	{
		VectorXd oTarget = VectorXd::Zero(nbClass);
		//class 1 is at index 0
		oTarget[size_t(m_labels[i])] = 1.;
		targetList[m_labels[i]]      = oTarget;
	}

	//We store each normalize vector we get for training. This not optimal in memory but avoid a lot of computation later
	//List of the class of the feature vectors store in the same order are they are in validation/training set(to be able to get the target)
	std::vector<double> oTrainingSet;
	std::vector<double> oValidationSet;
	MatrixXd oTrainingDataMatrix(nFeature, dataset.getFeatureVectorCount() - validationElementCount);
	MatrixXd oValidationDataMatrix(nFeature, validationElementCount);

	//We don't need to make a shuffle it has already be made by the trainer box
	//We store 20% of the feature vectors for validation
	int validationIndex = 0, trainingIndex = 0;
	for (size_t i = 0; i < dataset.getFeatureVectorCount(); ++i)
	{
		const Map<VectorXd> oFeatureVec(const_cast<double*>(dataset.getFeatureVector(i).getBuffer()), nFeature);
		VectorXd oData = oFeatureVec;
		if (classCount[dataset.getFeatureVector(i).getLabel()] > 0)
		{
			oValidationDataMatrix.col(validationIndex++) = oData;
			oValidationSet.push_back(dataset.getFeatureVector(i).getLabel());
			--classCount[dataset.getFeatureVector(i).getLabel()];
		}
		else
		{
			oTrainingDataMatrix.col(trainingIndex++) = oData;
			oTrainingSet.push_back(dataset.getFeatureVector(i).getLabel());
		}
	}

	//We now get the min and the max of the training set for normalization
	m_max = oTrainingDataMatrix.maxCoeff();
	m_min = oTrainingDataMatrix.minCoeff();
	//Normalization of the data. We need to do it to avoid saturation of tanh.
	for (size_t i = 0; i < size_t(oTrainingDataMatrix.cols()); ++i)
	{
		for (size_t j = 0; j < size_t(oTrainingDataMatrix.rows()); ++j)
		{
			oTrainingDataMatrix(j, i) = 2 * (oTrainingDataMatrix(j, i) - m_min) / (m_max - m_min) - 1;
		}
	}
	for (size_t i = 0; i < size_t(oValidationDataMatrix.cols()); ++i)
	{
		for (size_t j = 0; j < size_t(oValidationDataMatrix.rows()); ++j)
		{
			oValidationDataMatrix(j, i) = 2 * (oValidationDataMatrix(j, i) - m_min) / (m_max - m_min) - 1;
		}
	}

	const double featureCount = double(oTrainingSet.size());
	const double boundValue   = 1. / (nFeature + 1);
	double previousError      = std::numeric_limits<double>::max();
	double cumulativeError    = 0;

	//Let's generate randomly weights and biases
	//We restrain the weight between -1/(fan-in) and 1/(fan-in) to avoid saturation in the worst case
	m_inputWeight = MatrixXd::Random(hiddenNeuronCount, nFeature) * boundValue;
	m_inputBias   = VectorXd::Random(hiddenNeuronCount) * boundValue;

	m_hiddenWeight = MatrixXd::Random(nbClass, hiddenNeuronCount) * boundValue;
	m_hiddenBias   = VectorXd::Random(nbClass) * boundValue;

	MatrixXd oDeltaInputWeight  = MatrixXd::Zero(hiddenNeuronCount, nFeature);
	VectorXd oDeltaInputBias    = VectorXd::Zero(hiddenNeuronCount);
	MatrixXd oDeltaHiddenWeight = MatrixXd::Zero(nbClass, hiddenNeuronCount);
	VectorXd oDeltaHiddenBias   = VectorXd::Zero(nbClass);

	MatrixXd oY1, oA2;
	//A1 is the value compute in hidden neuron before applying tanh
	//Y1 is the output vector of hidden layer
	//A2 is the value compute by output neuron before applying transfer function
	//Y2 is the value of output after the transfer function (softmax)
	while (true)
	{
		oDeltaInputWeight.setZero();
		oDeltaInputBias.setZero();
		oDeltaHiddenWeight.setZero();
		oDeltaHiddenBias.setZero();
		//The first cast of tanh has to been explicit for windows compilation
		oY1.noalias() = ((m_inputWeight * oTrainingDataMatrix).colwise() + m_inputBias).unaryExpr(
			std::ptr_fun<double, double>(static_cast<double(*)(double)>(tanh)));
		oA2.noalias() = (m_hiddenWeight * oY1).colwise() + m_hiddenBias;
		for (size_t i = 0; i < featureCount; ++i)
		{
			const VectorXd& oTarget = targetList[oTrainingSet[i]];
			const VectorXd& oData   = oTrainingDataMatrix.col(i);

			//Now we compute all deltas of output layer
			VectorXd oOutputDelta = oA2.col(i) - oTarget;
			for (size_t j = 0; j < nbClass; ++j)
			{
				for (size_t k = 0; k < hiddenNeuronCount; ++k) { oDeltaHiddenWeight(j, k) -= oOutputDelta[j] * oY1.col(i)[k]; }
			}
			oDeltaHiddenBias.noalias() -= oOutputDelta;

			//Now we take care of the hidden layer
			VectorXd oHiddenDelta = VectorXd::Zero(hiddenNeuronCount);
			for (size_t j = 0; j < hiddenNeuronCount; ++j)
			{
				for (size_t k = 0; k < nbClass; ++k) { oHiddenDelta[j] += oOutputDelta[k] * m_hiddenWeight(k, j); }
				oHiddenDelta[j] *= (1 - pow(oY1.col(i)[j], 2));
			}

			for (size_t j = 0; j < hiddenNeuronCount; ++j) { for (size_t k = 0; k < nFeature; ++k) { oDeltaInputWeight(j, k) -= oHiddenDelta[j] * oData[k]; } }
			oDeltaInputBias.noalias() -= oHiddenDelta;
		}
		//We finish the loop, let's apply deltas
		m_hiddenWeight.noalias() += oDeltaHiddenWeight / featureCount * alpha;
		m_hiddenBias.noalias() += oDeltaHiddenBias / featureCount * alpha;
		m_inputWeight.noalias() += oDeltaInputWeight / featureCount * alpha;
		m_inputBias.noalias() += oDeltaInputBias / featureCount * alpha;

		dumpMatrix(this->getLogManager(), m_hiddenWeight, "m_hiddenWeight");
		dumpMatrix(this->getLogManager(), m_hiddenBias, "m_hiddenBias");
		dumpMatrix(this->getLogManager(), m_inputWeight, "m_inputWeight");
		dumpMatrix(this->getLogManager(), m_inputBias, "m_inputBias");

		//Now we compute the cumulative error in the validation set
		cumulativeError = 0;
		//We don't compute Y2 because we train on the identity
		oA2.noalias() = (m_hiddenWeight * ((m_inputWeight * oValidationDataMatrix).colwise() + m_inputBias).unaryExpr(std::ptr_fun<double, double>(tanh))).
						colwise() + m_hiddenBias;
		for (size_t i = 0; i < oValidationSet.size(); ++i)
		{
			const VectorXd& oTarget         = targetList[oValidationSet[i]];
			const VectorXd& oIdentityResult = oA2.col(i);

			//Now we need to compute the error
			for (size_t j = 0; j < nbClass; ++j) { cumulativeError += 0.5 * pow(oIdentityResult[j] - oTarget[j], 2); }
		}
		cumulativeError /= oValidationSet.size();
		//If the delta of error is under Epsilon we consider that the training is over
		if (previousError - cumulativeError < epsilon) { break; }
		previousError = cumulativeError;
	}
	dumpMatrix(this->getLogManager(), m_hiddenWeight, "oHiddenWeight");
	dumpMatrix(this->getLogManager(), m_hiddenBias, "oHiddenBias");
	dumpMatrix(this->getLogManager(), m_inputWeight, "oInputWeight");
	dumpMatrix(this->getLogManager(), m_inputBias, "oInputBias");
	return true;
}

bool CAlgorithmClassifierMLP::classify(const IFeatureVector& sample, double& classLabel, IVector& distance, IVector& probability)
{
	if (sample.getSize() != size_t(m_inputWeight.cols()))
	{
		this->getLogManager() << LogLevel_Error << "Classifier expected " << size_t(m_inputWeight.cols()) << " features, got " << sample.getSize() << "\n";
		return false;
	}

	const Map<VectorXd> oFeatureVec(const_cast<double*>(sample.getBuffer()), sample.getSize());
	VectorXd oData = oFeatureVec;
	//we normalize and center data on 0 to avoid saturation
	for (size_t j = 0; j < sample.getSize(); ++j) { oData[j] = 2 * (oData[j] - m_min) / (m_max - m_min) - 1; }

	const size_t classCount = m_labels.size();

	VectorXd oA2 = m_hiddenBias + (m_hiddenWeight * (m_inputBias + (m_inputWeight * oData)).unaryExpr(std::ptr_fun<double, double>(tanh)));

	//The final transfer function is the softmax
	VectorXd oY2 = oA2.unaryExpr(std::ptr_fun<double, double>(exp));
	oY2 /= oY2.sum();

	distance.setSize(classCount);
	probability.setSize(classCount);

	//We use A2 as the classification values output, and the Y2 as the probability
	double max        = oY2[0];
	size_t classFound = 0;
	distance[0]       = oA2[0];
	probability[0]    = oY2[0];
	for (size_t i = 1; i < classCount; ++i)
	{
		if (oY2[i] > max)
		{
			max        = oY2[i];
			classFound = i;
		}
		distance[i]    = oA2[i];
		probability[i] = oY2[i];
	}

	classLabel = m_labels[classFound];

	return true;
}

XML::IXMLNode* CAlgorithmClassifierMLP::saveConfig()
{
	XML::IXMLNode* rootNode = XML::createNode(MLP_TYPE_NODE_NAME);

	std::stringstream classes;
	for (int i = 0; i < m_hiddenBias.size(); ++i) { classes << m_labels[i] << " "; }
	XML::IXMLNode* classLabelNode = XML::createNode(MLP_CLASS_LABEL_NODE_NAME);
	classLabelNode->setPCData(classes.str().c_str());
	rootNode->addChild(classLabelNode);

	XML::IXMLNode* configuration = XML::createNode(MLP_NEURON_CONFIG_NODE_NAME);

	//The input and output neuron count are not mandatory but they facilitate a lot the loading process
	XML::IXMLNode* tempNode = XML::createNode(MLP_INPUT_NEURON_COUNT_NODE_NAME);
	dumpData(tempNode, int64_t(m_inputWeight.cols()));
	configuration->addChild(tempNode);

	tempNode = XML::createNode(MLP_HIDDEN_NEURON_COUNT_NODE_NAME);
	dumpData(tempNode, int64_t(m_inputWeight.rows()));
	configuration->addChild(tempNode);
	rootNode->addChild(configuration);

	tempNode = XML::createNode(MLP_MIN_NODE_NAME);
	dumpData(tempNode, m_min);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(MLP_MAX_NODE_NAME);
	dumpData(tempNode, m_max);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(MLP_INPUT_WEIGHT_NODE_NAME);
	dumpData(tempNode, m_inputWeight);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(MLP_INPUT_BIAS_NODE_NAME);
	dumpData(tempNode, m_inputBias);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(MLP_HIDDEN_BIAS_NODE_NAME);
	dumpData(tempNode, m_hiddenBias);
	rootNode->addChild(tempNode);

	tempNode = XML::createNode(MLP_HIDDEN_WEIGHT_NODE_NAME);
	dumpData(tempNode, m_hiddenWeight);
	rootNode->addChild(tempNode);

	return rootNode;
}

bool CAlgorithmClassifierMLP::loadConfig(XML::IXMLNode* configNode)
{
	m_labels.clear();
	std::stringstream data(configNode->getChildByName(MLP_CLASS_LABEL_NODE_NAME)->getPCData());
	double temp;
	while (data >> temp) { m_labels.push_back(temp); }

	int64_t featureSize, hiddenNeuronCount;
	XML::IXMLNode* neuronConfigNode = configNode->getChildByName(MLP_NEURON_CONFIG_NODE_NAME);

	loadData(neuronConfigNode->getChildByName(MLP_HIDDEN_NEURON_COUNT_NODE_NAME), hiddenNeuronCount);
	loadData(neuronConfigNode->getChildByName(MLP_INPUT_NEURON_COUNT_NODE_NAME), featureSize);

	loadData(configNode->getChildByName(MLP_MAX_NODE_NAME), m_max);
	loadData(configNode->getChildByName(MLP_MIN_NODE_NAME), m_min);

	loadData(configNode->getChildByName(MLP_INPUT_WEIGHT_NODE_NAME), m_inputWeight, hiddenNeuronCount, featureSize);
	loadData(configNode->getChildByName(MLP_INPUT_BIAS_NODE_NAME), m_inputBias);
	loadData(configNode->getChildByName(MLP_HIDDEN_WEIGHT_NODE_NAME), m_hiddenWeight, m_labels.size(), hiddenNeuronCount);
	loadData(configNode->getChildByName(MLP_HIDDEN_BIAS_NODE_NAME), m_hiddenBias);

	return true;
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, MatrixXd& matrix)
{
	std::stringstream data;

	data << std::scientific;
	for (size_t i = 0; i < size_t(matrix.rows()); ++i) { for (size_t j = 0; j < size_t(matrix.cols()); ++j) { data << " " << matrix(i, j); } }

	node->setPCData(data.str().c_str());
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, VectorXd& vector)
{
	std::stringstream data;

	data << std::scientific;
	for (size_t i = 0; i < size_t(vector.size()); ++i) { data << " " << vector[i]; }

	node->setPCData(data.str().c_str());
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, const int64_t value)
{
	std::stringstream data;
	data << value;
	node->setPCData(data.str().c_str());
}

void CAlgorithmClassifierMLP::dumpData(XML::IXMLNode* node, const double value)
{
	std::stringstream data;
	data << std::scientific;
	data << value;
	node->setPCData(data.str().c_str());
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, MatrixXd& matrix, const size_t nRow, const size_t nCol)
{
	matrix = MatrixXd(nRow, nCol);
	std::stringstream data(node->getPCData());

	std::vector<double> coefs;
	double value;
	while (data >> value) { coefs.push_back(value); }

	size_t index = 0;
	for (size_t i = 0; i < nRow; ++i)
	{
		for (size_t j = 0; j < nCol; ++j)
		{
			matrix(int(i), int(j)) = coefs[index];
			++index;
		}
	}
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, VectorXd& vector)
{
	std::stringstream data(node->getPCData());
	std::vector<double> coefs;
	double value;
	while (data >> value) { coefs.push_back(value); }
	vector = VectorXd(coefs.size());

	for (size_t i = 0; i < coefs.size(); ++i) { vector[i] = coefs[i]; }
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, int64_t& value)
{
	std::stringstream data(node->getPCData());
	data >> value;
}

void CAlgorithmClassifierMLP::loadData(XML::IXMLNode* node, double& value)
{
	std::stringstream data(node->getPCData());
	data >> value;
}

#endif
