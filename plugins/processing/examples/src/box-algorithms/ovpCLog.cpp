#include "ovpCLog.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Examples;
using namespace /*OpenViBE::*/Toolkit;

bool CLog::initialize()
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "initialize\n";
	return true;
}

bool CLog::uninitialize()
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "uninitialize\n";
	return true;
}

bool CLog::processClock(Kernel::CMessageClock& /*msg*/)
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "processClock\n";
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CLog::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "processInput\n";
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CLog::process()
{
	getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Info << "process\n";

	IBoxIO* boxContext  = getBoxAlgorithmContext()->getDynamicBoxContext();
	const size_t nInput = getBoxAlgorithmContext()->getStaticBoxContext()->getInputCount();

	for (size_t i = 0; i < nInput; ++i) { for (size_t j = 0; j < boxContext->getInputChunkCount(i); ++j) { boxContext->markInputAsDeprecated(i, j); } }

	return true;
}
