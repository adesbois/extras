#include "ovp_defines.h"

#include "box-algorithms/ovpCBoxAlgorithmStreamedMatrixSwitch.h"

#include <vector>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
	context.getTypeManager().registerEnumerationEntry(OV_TypeId_BoxAlgorithmFlag, OV_AttributeId_Box_FlagIsUnstable.toString(),
													  OV_AttributeId_Box_FlagIsUnstable.toUInteger());
	OVP_Declare_New(Streaming::CBoxAlgorithmStreamedMatrixSwitchDesc);
OVP_Declare_End()
