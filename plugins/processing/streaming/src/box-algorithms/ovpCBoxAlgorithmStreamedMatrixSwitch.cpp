#include "ovpCBoxAlgorithmStreamedMatrixSwitch.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::*/Toolkit;
using namespace Streaming;

using namespace std;

bool CBoxAlgorithmStreamedMatrixSwitch::initialize()
{
	// Getting the settings to build the map Stim code / output index
	for (size_t i = 1; i < this->getStaticBoxContext().getSettingCount(); ++i)
	{
		const uint64_t stimCode = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), i);
		const size_t idx        = i - 1;
		if (!m_stimOutputIndexes.insert(make_pair(stimCode, idx)).second)
		{
			this->getLogManager() << LogLevel_Warning << "The stimulation code ["
					<< this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, stimCode) << "] for the output ["
					<< idx << "] is already used by a previous output.\n";
		}
		else
		{
			this->getLogManager() << LogLevel_Trace << "The stimulation code ["
					<< this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, stimCode) << "] is registered for the output ["
					<< idx << "]\n";
		}
	}

	const bool defaultToFirstOutput = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	if (defaultToFirstOutput) { m_activeOutputIdx = 0; }
	else { m_activeOutputIdx = size_t(-1); }	// At start, no output is active.

	// Stimulation stream decoder
	m_stimDecoder.initialize(*this, 0);
	m_lastStimInputChunkEndTime = 0;

	//initializing the decoder depending on the input type.
	CIdentifier typeID;
	this->getStaticBoxContext().getInputType(1, typeID);

	m_streamDecoder = nullptr;

	if (typeID == OV_TypeId_StreamedMatrix) { m_streamDecoder = new TStreamedMatrixDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else if (typeID == OV_TypeId_Signal) { m_streamDecoder = new TSignalDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else if (typeID == OV_TypeId_Spectrum) { m_streamDecoder = new TSpectrumDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else if (typeID == OV_TypeId_FeatureVector) { m_streamDecoder = new TFeatureVectorDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else if (typeID == OV_TypeId_ChannelLocalisation) { m_streamDecoder = new TChannelLocalisationDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else if (typeID == OV_TypeId_Stimulations) { m_streamDecoder = new TStimulationDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else if (typeID == OV_TypeId_ExperimentInfo) { m_streamDecoder = new TExperimentInfoDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else if (typeID == OV_TypeId_ChannelUnits) { m_streamDecoder = new TChannelUnitsDecoder<CBoxAlgorithmStreamedMatrixSwitch>(*this, 1); }
	else
	{
		this->getLogManager() << LogLevel_Error << "Unsupported stream type " << this->getTypeManager().getTypeName(typeID) << " (" << typeID.str() << ")\n";
		return false;
	}

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmStreamedMatrixSwitch::uninitialize()
{
	m_stimDecoder.uninitialize();
	if (m_streamDecoder)
	{
		m_streamDecoder->uninitialize();
		delete m_streamDecoder;
	}

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmStreamedMatrixSwitch::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmStreamedMatrixSwitch::process()
{
	// the dynamic box context describes the current state of the box inputs and outputs (i.e. the chunks)
	IBoxIO& boxContext = this->getDynamicBoxContext();

	const size_t nOutput  = this->getStaticBoxContext().getOutputCount();
	uint64_t start        = 0;
	uint64_t end          = 0;
	size_t chunkSize      = 0;
	bool gotStimulation   = false;
	const uint8_t* buffer = nullptr;

	//iterate over all chunk on input 0 (Stimulation)
	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		m_stimDecoder.decode(i);

		if (m_stimDecoder.isHeaderReceived() || m_stimDecoder.isEndReceived())
		{
			// nothing
		}
		if (m_stimDecoder.isBufferReceived())
		{
			// we update the active output index and time if needed
			IStimulationSet* stimSet = m_stimDecoder.getOutputStimulationSet();
			for (size_t j = 0; j < stimSet->getStimulationCount(); j++)
			{
				if (m_stimOutputIndexes.find(stimSet->getStimulationIdentifier(j)) != m_stimOutputIndexes.end())
				{
					m_activeOutputIdx = m_stimOutputIndexes[stimSet->getStimulationIdentifier(j)];
					this->getLogManager() << LogLevel_Trace << "Switching with ["
							<< this->getTypeManager().getEnumerationEntryNameFromValue(OV_TypeId_Stimulation, stimSet->getStimulationIdentifier(j))
							<< "] to output [" << m_activeOutputIdx << "].\n";
				}
			}
			gotStimulation              = true;
			m_lastStimInputChunkEndTime = boxContext.getInputChunkEndTime(0, i);
		}
	}

	for (size_t i = 0; i < boxContext.getInputChunkCount(1); ++i)
	{
		//We decode the chunk but we don't automatically mark it as deprecated, as we may need to keep it.
		m_streamDecoder->decode(i, false);
		{
			boxContext.getInputChunk(1, i, start, end, chunkSize, buffer);
			if (m_streamDecoder->isHeaderReceived() || m_streamDecoder->isEndReceived())
			{
				for (size_t j = 0; j < nOutput; ++j)
				{
					boxContext.appendOutputChunkData(j, buffer, chunkSize);
					boxContext.markOutputAsReadyToSend(j, start, end);
				}
				boxContext.markInputAsDeprecated(1, i);
			}
			if (m_streamDecoder->isBufferReceived())
			{
				if (m_activeOutputIdx == size_t(-1))
				{
					// we drop every chunk when no output is activated
					boxContext.markInputAsDeprecated(1, i);
				}
				else
				{
					if (!gotStimulation || (start < m_lastStimInputChunkEndTime))
					{
						// the input chunk is in the good time range (we are sure that no stim has been received to change the active output)
						boxContext.appendOutputChunkData(m_activeOutputIdx, buffer, chunkSize);
						boxContext.markOutputAsReadyToSend(m_activeOutputIdx, start, end);
						boxContext.markInputAsDeprecated(1, i);
					}
					// else : we keep the input chunk, no mark as deprecated !
				}
			}
		}
	}

	return true;
}
