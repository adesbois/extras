#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_BoxAlgorithm_StreamedMatrixSwitch		OpenViBE::CIdentifier(0x556A2C32, 0x61DF49FC)
#define OVP_ClassId_BoxAlgorithm_StreamedMatrixSwitchDesc	OpenViBE::CIdentifier(0x556A2C32, 0x61DF49FC)

// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines

#define OV_AttributeId_Box_FlagIsUnstable					OpenViBE::CIdentifier(0x666FFFFF, 0x666FFFFF)
