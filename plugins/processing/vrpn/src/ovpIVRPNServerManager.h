#pragma once

#if defined TARGET_HAS_ThirdPartyVRPN

#include <openvibe/ov_all.h>

namespace OpenViBE {
namespace Plugins {
namespace VRPN {
class IVRPNServerManager
{
public:

	static IVRPNServerManager& getInstance();

	virtual ~IVRPNServerManager() { }

	virtual bool initialize() = 0;
	virtual bool uninitialize() = 0;

	virtual bool process() = 0;
	virtual bool reportAnalog(const CIdentifier& identifier) = 0;
	virtual bool reportButton(const CIdentifier& identifier) = 0;

	virtual bool addServer(const CString& name, CIdentifier& identifier) = 0;
	virtual bool isServer(const CIdentifier& identifier) const = 0;
	virtual bool isServer(const CString& name) const = 0;
	virtual bool getServerIdentifier(const CString& name, CIdentifier& identifier) const = 0;
	virtual bool getServerName(const CIdentifier& identifier, CString& name) const = 0;
	virtual bool removeServer(const CIdentifier& identifier) = 0;

	virtual bool setButtonCount(const CIdentifier& identifier, size_t nButton) = 0;
	virtual bool setButtonState(const CIdentifier& identifier, size_t buttonIndex, bool status) = 0;
	virtual bool getButtonState(const CIdentifier& identifier, size_t buttonIndex) const = 0;

	virtual bool setAnalogCount(const CIdentifier& serverID, size_t nAnalog) = 0;
	virtual bool setAnalogState(const CIdentifier& serverID, size_t analogIndex, double status) = 0;
	virtual double getAnalogState(const CIdentifier& serverID, size_t analogIndex) const = 0;
};
}  // namespace VRPN
}  // namespace Plugins
}  // namespace OpenViBE

#endif // OVP_HAS_Vrpn
