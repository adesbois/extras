#if defined TARGET_HAS_ThirdPartyVRPN

#include "ovpIVRPNServerManager.h"

#include <vrpn_Connection.h>
#include <vrpn_Analog.h>
#include <vrpn_Button.h>

#include <vector>
#include <map>
#include <iostream>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace VRPN;
using namespace std;


namespace OpenViBE {
namespace Plugins {
namespace VRPN {
namespace {
class CVRPNServerManager final : public IVRPNServerManager
{
public:

	CVRPNServerManager() { }
	~CVRPNServerManager() override { }

	bool initialize() override;
	bool uninitialize() override;

	bool process() override;
	bool reportAnalog(const CIdentifier& serverID) override;
	bool reportButton(const CIdentifier& serverID) override;

	bool addServer(const CString& name, CIdentifier& identifier) override;
	bool isServer(const CIdentifier& identifier) const override;
	bool isServer(const CString& name) const override;
	bool getServerIdentifier(const CString& name, CIdentifier& identifier) const override;
	bool getServerName(const CIdentifier& serverID, CString& name) const override;
	bool removeServer(const CIdentifier& serverID) override;

	bool setButtonCount(const CIdentifier& serverID, size_t nButton) override;
	bool setButtonState(const CIdentifier& serverID, size_t buttonIndex, bool status) override;
	bool getButtonState(const CIdentifier& serverID, size_t buttonIndex) const override;

	bool setAnalogCount(const CIdentifier& serverID, size_t nAnalog) override;
	bool setAnalogState(const CIdentifier& serverID, size_t analogIndex, double status) override;
	double getAnalogState(const CIdentifier& serverID, size_t analogIndex) const override;

protected:

	vrpn_Connection* m_connection = nullptr;

	map<CIdentifier, CString> m_serverNames;
	map<CIdentifier, vrpn_Button_Server*> m_buttonServers;
	map<CIdentifier, vrpn_Analog_Server*> m_analogServers;
	map<CIdentifier, vector<bool>> m_buttonCaches;

	size_t m_nInitialize = 0;
};
}  // namespace

IVRPNServerManager& IVRPNServerManager::getInstance()
{
	static CVRPNServerManager gVRPNServerManager;
	return gVRPNServerManager;
}
}  // namespace VRPN
}  // namespace Plugins
}  // namespace OpenViBE

bool CVRPNServerManager::initialize()
{
	if (!m_nInitialize)
	{
		//m_Connection=new vrpn_Connection;
		m_connection = vrpn_create_server_connection();
	}
	m_nInitialize++;
	return true;
}

bool CVRPNServerManager::uninitialize()
{
	m_nInitialize--;
	if (!m_nInitialize)
	{
		for (auto it = m_analogServers.begin(); it != m_analogServers.end(); ++it)
		{
			if (it->second)
			{
				delete it->second;
				it->second = nullptr;
			}
		}
		m_analogServers.clear();
		for (auto it = m_buttonServers.begin(); it != m_buttonServers.end(); ++it)
		{
			if (it->second)
			{
				delete it->second;
				it->second = nullptr;
			}
		}
		m_buttonServers.clear();

		// $$$ UGLY !
		// The following function should destroy correctly the connection, but does not.
		//vrpn_ConnectionManager::instance().deleteConnection(m_Connection);
		delete m_connection;
		m_connection = nullptr;
	}
	return true;
}

bool CVRPNServerManager::process()
{
	for (auto it = m_analogServers.begin(); it != m_analogServers.end(); ++it) { if (it->second) { it->second->mainloop(); } }
	for (auto it = m_buttonServers.begin(); it != m_buttonServers.end(); ++it) { if (it->second) { it->second->mainloop(); } }
	if (m_connection) { m_connection->mainloop(); }
	return true;
}

bool CVRPNServerManager::reportAnalog(const CIdentifier& serverID)
{
	auto it = m_analogServers.find(serverID);
	if (it != m_analogServers.end())
	{
		if (it->second)
		{
			// This is public function and mainloop won't call it for me
			// Thank you VRPN for this to be similar to button behavior ;o)
			it->second->report();
		}
	}
	return true;
}

bool CVRPNServerManager::reportButton(const CIdentifier& serverID)
{
	const auto it = m_buttonServers.find(serverID);
	if (it != m_buttonServers.end())
	{
		if (it->second)
		{
			// This is not public function, however, mainloop calls it for me
			// Thank you VRPN for this to be similar to analog behavior ;o)
			// itButtonServer->second->report_changes();
		}
	}
	return true;
}

bool CVRPNServerManager::addServer(const CString& name, CIdentifier& identifier)
{
	if (this->isServer(name)) { return this->getServerIdentifier(name, identifier); }

	identifier = CIdentifier::random();
	while (m_serverNames.find(identifier) != m_serverNames.end()) { ++identifier; }

	m_serverNames[identifier] = name;
	return true;
}

bool CVRPNServerManager::isServer(const CIdentifier& identifier) const { return m_serverNames.find(identifier) != m_serverNames.end(); }

bool CVRPNServerManager::isServer(const CString& name) const
{
	for (auto it = m_serverNames.begin(); it != m_serverNames.end(); ++it) { if (it->second == name) { return true; } }
	return false;
}

bool CVRPNServerManager::getServerIdentifier(const CString& name, CIdentifier& identifier) const
{
	for (auto it = m_serverNames.begin(); it != m_serverNames.end(); ++it)
	{
		if (it->second == name)
		{
			identifier = it->first;
			return true;
		}
	}
	return false;
}

bool CVRPNServerManager::getServerName(const CIdentifier& serverID, CString& name) const
{
	const auto it = m_serverNames.find(serverID);
	if (it == m_serverNames.end()) { return false; }
	name = it->second;
	return true;
}

bool CVRPNServerManager::removeServer(const CIdentifier& serverID)
{
	if (!this->isServer(serverID)) { return false; }
	// TODO
	return true;
}

bool CVRPNServerManager::setButtonCount(const CIdentifier& serverID, const size_t nButton)
{
	if (!this->isServer(serverID)) { return false; }
	delete m_buttonServers[serverID];
	m_buttonServers[serverID] = new vrpn_Button_Server(m_serverNames[serverID], m_connection, int(nButton));
	m_buttonCaches[serverID].clear();
	m_buttonCaches[serverID].resize(nButton);
	return true;
}

bool CVRPNServerManager::setButtonState(const CIdentifier& serverID, const size_t buttonIndex, const bool status)
{
	if (!this->isServer(serverID) || m_buttonServers.find(serverID) == m_buttonServers.end()) { return false; }
	m_buttonServers[serverID]->set_button(int(buttonIndex), status ? 1 : 0);
	m_buttonCaches[serverID][buttonIndex] = status;
	return true;
}

bool CVRPNServerManager::getButtonState(const CIdentifier& serverID, const size_t buttonIndex) const
{
	if (!this->isServer(serverID)) { return false; }
	const auto itButtonServer = m_buttonServers.find(serverID);
	if (itButtonServer == m_buttonServers.end()) { return false; }
	const auto itButtonCache = m_buttonCaches.find(serverID);
	return itButtonCache->second[buttonIndex];
}

bool CVRPNServerManager::setAnalogCount(const CIdentifier& serverID, const size_t nAnalog)
{
	if (!this->isServer(serverID)) { return false; }
	if (m_analogServers[serverID]) { delete m_analogServers[serverID]; }
	m_analogServers[serverID] = new vrpn_Analog_Server(m_serverNames[serverID], m_connection);
	if (size_t(m_analogServers[serverID]->setNumChannels(vrpn_uint32(nAnalog))) != nAnalog) { return false; }
	return true;
}

bool CVRPNServerManager::setAnalogState(const CIdentifier& serverID, const size_t analogIndex, const double status)
{
	if (!this->isServer(serverID) || m_analogServers.find(serverID) == m_analogServers.end()) { return false; }
	const size_t nChannel = m_analogServers[serverID]->getNumChannels();
	if (analogIndex >= nChannel) { return false; }

	m_analogServers[serverID]->channels()[analogIndex] = status;
	return true;
}

double CVRPNServerManager::getAnalogState(const CIdentifier& serverID, const size_t analogIndex) const
{
	if (!this->isServer(serverID)) { return 0; }
	auto it = m_analogServers.find(serverID);
	if (it == m_analogServers.end()) { return 0; }
	return it->second->channels()[analogIndex];
}

#endif // OVP_HAS_Vrpn
