#pragma once

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <list>
#include <vrpn_Button.h>

namespace OpenViBE {
namespace Plugins {
namespace VRPN {
class CBoxAlgorithmVRPNButtonClient final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	uint64_t getClockFrequency() override { return 128LL << 32; } // the box clock frequency
	bool initialize() override;
	bool uninitialize() override;
	bool processClock(Kernel::CMessageClock& msg) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_VRPNButtonClient)

	std::list<std::pair<size_t, bool>> m_Buttons;
	void setButton(const size_t index, const bool pressed);

protected:

	uint64_t m_lastChunkEndTime = 0;
	bool m_gotStimulation       = false;
	std::vector<Kernel::IAlgorithmProxy*> m_encoders;
	std::vector<IStimulationSet*> m_stimSets;
	std::vector<uint64_t> m_stimIDsOn;
	std::vector<uint64_t> m_stimIDsOff;
	vrpn_Button_Remote* m_vrpnButtonRemote = nullptr;
};

class CBoxAlgorithmVRPNButtonClientListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	static bool check(Kernel::IBox& box)
	{
		for (size_t i = 0; i < box.getOutputCount(); ++i)
		{
			const std::string idx = std::to_string(i + 1);
			box.setOutputName(i, ("Output " + idx).c_str());
			box.setSettingName(i * 2 + 1, ("Button " + idx + " ON").c_str());
			box.setSettingName(i * 2 + 2, ("Button " + idx + " OFF").c_str());
		}
		return true;
	}

	bool onOutputAdded(Kernel::IBox& box, const size_t index) override
	{
		box.setOutputType(index, OV_TypeId_Stimulations);
		box.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
		box.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");
		return check(box);
	}

	bool onOutputRemoved(Kernel::IBox& box, const size_t index) override
	{
		box.removeSetting(index * 2 + 2);
		box.removeSetting(index * 2 + 1);
		return check(box);
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CBoxAlgorithmVRPNButtonClientDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Button VRPN Client"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }

	CString getShortDescription() const override
	{
		return CString("Connects to an external VRPN device and translate a button information into OpenViBE stimulations");
	}

	CString getDetailedDescription() const override { return CString("-"); }
	CString getCategory() const override { return CString("Acquisition and network IO/VRPN"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-connect"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_VRPNButtonClient; }
	IPluginObject* create() override { return new CBoxAlgorithmVRPNButtonClient; }
	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmVRPNButtonClientListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		// prototype.addInput  ("input name", /* input type (OV_TypeId_Signal) */);
		prototype.addOutput("Output", OV_TypeId_Stimulations);
		prototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn@localhost");
		prototype.addSetting("Button 1 ON", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
		prototype.addSetting("Button 1 OFF", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");
		prototype.addFlag(Kernel::BoxFlag_CanAddOutput);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_VRPNButtonClientDesc)
};
}  // namespace VRPN
}  // namespace Plugins
}  // namespace OpenViBE
#endif // TARGET_HAS_ThirdPartyVRPN
