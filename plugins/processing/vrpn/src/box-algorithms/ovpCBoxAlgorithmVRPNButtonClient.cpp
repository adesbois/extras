#if defined TARGET_HAS_ThirdPartyVRPN

#include "ovpCBoxAlgorithmVRPNButtonClient.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace VRPN;

static void VRPN_CALLBACK vrpn_button_cb(void* data, const vrpn_BUTTONCB b)
{
	CBoxAlgorithmVRPNButtonClient* box = static_cast<CBoxAlgorithmVRPNButtonClient*>(data);

	(box->m_Buttons).push_back(std::pair<size_t, bool>(b.button, b.state ? true : false));
}

bool CBoxAlgorithmVRPNButtonClient::initialize()
{
	const size_t nOutput = this->getStaticBoxContext().getOutputCount();

	for (size_t i = 0; i < nOutput; ++i)
	{
		IAlgorithmProxy* encoder = &this->getAlgorithmManager().getAlgorithm(
			this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_StimulationEncoder));
		encoder->initialize();
		CStimulationSet* stimSet = new CStimulationSet();
		TParameterHandler<IStimulationSet*> ip_StimSet(encoder->getInputParameter(OVP_GD_Algorithm_StimulationEncoder_InputParameterId_StimulationSet));
		ip_StimSet = stimSet;
		m_encoders.push_back(encoder);
		m_stimSets.push_back(stimSet);

		m_stimIDsOn.push_back(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), i * 2 + 1));
		m_stimIDsOff.push_back(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), i * 2 + 2));
	}

	const CString name = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);

	m_vrpnButtonRemote = new vrpn_Button_Remote(name.toASCIIString());
	m_vrpnButtonRemote->register_change_handler(static_cast<void*>(this), vrpn_button_cb);

	m_lastChunkEndTime = uint64_t(-1);

	return true;
}

bool CBoxAlgorithmVRPNButtonClient::uninitialize()
{
	const size_t nOutput = this->getStaticBoxContext().getOutputCount();

	delete m_vrpnButtonRemote;
	m_vrpnButtonRemote = nullptr;

	for (size_t i = 0; i < nOutput; ++i)
	{
		delete m_stimSets[i];
		m_encoders[i]->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_encoders[i]);
	}
	m_stimSets.clear();
	m_encoders.clear();
	m_stimIDsOn.clear();
	m_stimIDsOff.clear();

	return true;
}

bool CBoxAlgorithmVRPNButtonClient::processClock(Kernel::CMessageClock& /*msg*/)
{
	this->getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}


bool CBoxAlgorithmVRPNButtonClient::process()
{
	IBoxIO& boxContext   = this->getDynamicBoxContext();
	const size_t nOutput = this->getStaticBoxContext().getOutputCount();
	size_t i;

	// Clears pending stimulations
	for (i = 0; i < nOutput; ++i) { m_stimSets[i]->clear(); }

	// Refreshes VRPN device
	m_gotStimulation = false;
	m_vrpnButtonRemote->mainloop();

	while (!m_Buttons.empty())
	{
		const std::pair<size_t, bool> state = m_Buttons.front();
		m_Buttons.pop_front();

		setButton(state.first, state.second);
	}

	// Encodes streams
	for (i = 0; i < nOutput; ++i)
	{
		TParameterHandler<IMemoryBuffer*> op_buffer(
			m_encoders[i]->getOutputParameter(OVP_GD_Algorithm_StimulationEncoder_OutputParameterId_EncodedMemoryBuffer));
		op_buffer = boxContext.getOutputChunk(i);
		if (m_lastChunkEndTime == uint64_t(-1))
		{
			m_encoders[i]->process(OVP_GD_Algorithm_StimulationEncoder_InputTriggerId_EncodeHeader);
			boxContext.markOutputAsReadyToSend(i, 0, 0);
		}
		else
		{
			if (m_gotStimulation) { m_encoders[i]->process(OVP_GD_Algorithm_StimulationEncoder_InputTriggerId_EncodeBuffer); }
			boxContext.markOutputAsReadyToSend(i, m_lastChunkEndTime, this->getPlayerContext().getCurrentTime());
		}
	}

	// Updates timings
	m_lastChunkEndTime = this->getPlayerContext().getCurrentTime();

	return true;
}

void CBoxAlgorithmVRPNButtonClient::setButton(const size_t index, const bool pressed)
{
	const IBox& boxContext = this->getStaticBoxContext();

	if (index >= boxContext.getOutputCount())
	{
		this->getLogManager() << LogLevel_Warning << "Ignored button " << index + 1 << " with state " << CString(pressed ? "pressed" : "released") << "...\n";
	}
	else
	{
		this->getLogManager() << LogLevel_Trace << "Changed button " << index + 1 << " with state " << CString(pressed ? "pressed" : "released") << "...\n";
		if (pressed) { m_stimSets[index]->appendStimulation(m_stimIDsOn[index], this->getPlayerContext().getCurrentTime(), 0); }
		else { m_stimSets[index]->appendStimulation(m_stimIDsOff[index], this->getPlayerContext().getCurrentTime(), 0); }
		m_gotStimulation = true;
	}
}
#endif // TARGET_HAS_ThirdPartyVRPN
