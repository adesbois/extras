#if defined TARGET_HAS_ThirdPartyVRPN

#include "ovpCBoxAlgorithmVRPNAnalogClient.h"


using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace VRPN;


static void VRPN_CALLBACK vrpn_analog_cb(void* data, const vrpn_ANALOGCB a)
{
	CBoxAlgorithmVRPNAnalogClient* client = static_cast<CBoxAlgorithmVRPNAnalogClient*>(data);
	client->setAnalog(a.num_channel, a.channel);
}

bool CBoxAlgorithmVRPNAnalogClient::initialize()
{
	m_vrpnAnalogRemote = nullptr;

	m_peripheralName      = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_sampling            = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);
	m_nChannel            = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2));
	m_nSamplePerSentBlock = uint64_t(FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 3));

	m_encoder = &this->getAlgorithmManager().getAlgorithm(this->getAlgorithmManager().createAlgorithm(OVP_GD_ClassId_Algorithm_SignalEncoder));
	m_encoder->initialize();

	ip_matrix.initialize(m_encoder->getInputParameter(OVP_GD_Algorithm_SignalEncoder_InputParameterId_Matrix));
	ip_sampling.initialize(m_encoder->getInputParameter(OVP_GD_Algorithm_SignalEncoder_InputParameterId_Sampling));
	op_buffer.initialize(m_encoder->getOutputParameter(OVP_GD_Algorithm_SignalEncoder_OutputParameterId_EncodedMemoryBuffer));

	m_firstStart       = true;
	m_lastChunkEndTime = 0;

	m_chunkDuration = CTime(m_sampling, m_nSamplePerSentBlock).time();

	m_lastSamples.resize(m_nChannel);
	m_sampleBuffer.clear();

	return true;
}

bool CBoxAlgorithmVRPNAnalogClient::uninitialize()
{
	if (m_vrpnAnalogRemote)
	{
		delete m_vrpnAnalogRemote;
		m_vrpnAnalogRemote = nullptr;
	}

	if (m_encoder)
	{
		m_encoder->uninitialize();
		this->getAlgorithmManager().releaseAlgorithm(*m_encoder);
		m_encoder = nullptr;
	}

	m_sampleBuffer.clear();
	m_lastSamples.clear();

	return true;
}

bool CBoxAlgorithmVRPNAnalogClient::processClock(Kernel::CMessageClock& /*msg*/)
{
	this->getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CBoxAlgorithmVRPNAnalogClient::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	if (m_firstStart)
	{
		ip_sampling = m_sampling;
		ip_matrix->setDimensionCount(2);
		ip_matrix->setDimensionSize(0, m_nChannel);
		ip_matrix->setDimensionSize(1, m_nSamplePerSentBlock);

		// @TODO do labels ?

		op_buffer = boxContext.getOutputChunk(0);
		m_encoder->process(OVP_GD_Algorithm_SignalEncoder_InputTriggerId_EncodeHeader);
		boxContext.markOutputAsReadyToSend(0, 0, 0);

		m_lastSamples.resize(m_nChannel);
		for (size_t i = 0; i < m_nChannel; ++i) { m_lastSamples[i] = 0; }

		m_firstStart = false;
	}

	if (!m_vrpnAnalogRemote)
	{
		m_vrpnAnalogRemote = new vrpn_Analog_Remote(m_peripheralName.toASCIIString());
		m_vrpnAnalogRemote->register_change_handler(this, &vrpn_analog_cb);
	}

	m_vrpnAnalogRemote->mainloop();

	const uint64_t time = this->getPlayerContext().getCurrentTime();

	if ((time - m_lastChunkEndTime) >= m_chunkDuration)
	{
		// Time to send a chunk. Copy our current sample buffer to the output matrix.
		size_t idx      = 0;
		double* oBuffer = ip_matrix->getBuffer();

		while (!m_sampleBuffer.empty())
		{
			const std::vector<double>& tmp = m_sampleBuffer.front();

			for (size_t j = 0; j < m_nChannel; ++j) { oBuffer[j * m_nSamplePerSentBlock + idx] = tmp[j]; }

			m_sampleBuffer.pop_front();
			idx++;
		}
		// If the buffer didn't have enough data from callbacks, pad with the last sample
		while (idx < m_nSamplePerSentBlock)
		{
			for (size_t j = 0; j < m_nChannel; ++j) { oBuffer[j * m_nSamplePerSentBlock + idx] = m_lastSamples[j]; }
			idx++;
		}

		op_buffer = boxContext.getOutputChunk(0);
		m_encoder->process(OVP_GD_Algorithm_SignalEncoder_InputTriggerId_EncodeBuffer);
		boxContext.markOutputAsReadyToSend(0, m_lastChunkEndTime, m_lastChunkEndTime + m_chunkDuration);
		m_lastChunkEndTime += m_chunkDuration;
#if defined _DEBUG
		const double diff = CTime(m_lastChunkEndTime).toSeconds() - CTime(time).toSeconds();
		if (std::abs(diff) > 1) { this->getLogManager() << LogLevel_Warning << "Time difference detected: " << diff << " secs.\n"; }
#endif
	}

	return true;
}

void CBoxAlgorithmVRPNAnalogClient::setAnalog(const size_t nAnalog, const double* analog)
{
	// Count how many samples are encoded in pAnalog
	const size_t nSample = nAnalog / m_nChannel;
	for (size_t i = 0; i < nSample; ++i)
	{
		// Append the sample to the buffer
		for (size_t j = 0; j < m_nChannel; ++j) { m_lastSamples[j] = analog[j * nSample + i]; }
		m_sampleBuffer.push_back(m_lastSamples);

		// Drop the oldest sample if our buffer got full. This means that VRPN is sending data faster than we consume.
		if (m_sampleBuffer.size() > m_nSamplePerSentBlock) { m_sampleBuffer.pop_front(); }
	}
}

#endif // TARGET_HAS_ThirdPartyVRPN
