#pragma once

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <map>
#include <cstdio>

namespace OpenViBE {
namespace Plugins {
namespace VRPN {
class CVRPNButtonServer final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	CVRPNButtonServer() {}
	void release() override { delete this; }
	uint64_t getClockFrequency() override { return 64LL << 32; } // 64 times per second
	bool initialize() override;
	bool uninitialize() override;
	bool processClock(Kernel::CMessageClock& msg) override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(IBoxAlgorithm, OVP_ClassId_VRPNButtonServer)

	void setStimulation(const size_t index, const uint64_t id, const uint64_t date);

protected:

	std::vector<Toolkit::TStimulationDecoder<CVRPNButtonServer>*> m_decoders;

	//Start and end time of the last buffer
	uint64_t m_startTime = 0;
	uint64_t m_endTime   = 0;

	size_t m_currInput = 0;

	CIdentifier m_serverID = OV_UndefinedIdentifier;

	//Pairs of start/stop stimulations id
	std::map<size_t, std::pair<uint64_t, uint64_t>> m_stimulationPairs;
};

class CVRPNButtonServerListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	bool check(Kernel::IBox& box) const
	{
		for (size_t i = 0; i < box.getInputCount(); ++i)
		{
			box.setInputName(i, ("Input " + std::to_string(i + 1)).c_str());
			box.setInputType(i, OV_TypeId_Stimulations);
		}

		for (size_t i = 0; i < box.getInputCount(); ++i)
		{
			box.setSettingName(i * 2 + 1, ("Button " + std::to_string(i + 1) + " ON").c_str());
			box.setSettingType(i * 2 + 1, OV_TypeId_Stimulation);

			box.setSettingName(i * 2 + 2, ("Button " + std::to_string(i + 1) + " OFF").c_str());
			box.setSettingType(i * 2 + 2, OV_TypeId_Stimulation);
		}

		return true;
	}

	bool onInputRemoved(Kernel::IBox& box, const size_t index) override
	{
		box.removeSetting(index * 2 + 1);
		box.removeSetting(index * 2 + 1);

		return this->check(box);
	}

	bool onInputAdded(Kernel::IBox& box, const size_t /*index*/) override
	{
		box.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
		box.addSetting("", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");

		return this->check(box);
	}

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CVRPNButtonServerDesc final : public IBoxAlgorithmDesc
{
public:

	CString getName() const override { return CString("Button VRPN Server"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Creates VRPN button servers (one per input)."); }

	CString getDetailedDescription() const override
	{
		return CString("Creates VRPN button servers to make data from the plugin's inputs available to VRPN client applications.");
	}

	CString getCategory() const override { return CString("Acquisition and network IO/VRPN"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-connect"); }
	CIdentifier getCreatedClass() const override { return OVP_ClassId_VRPNButtonServer; }
	IPluginObject* create() override { return new CVRPNButtonServer(); }
	IBoxListener* createBoxListener() const override { return new CVRPNButtonServerListener; }
	void release() override { }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input 1", OVTK_TypeId_Stimulations);
		prototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn");
		prototype.addSetting("Button 1 ON", OV_TypeId_Stimulation, "OVTK_GDF_Feedback_Continuous");
		prototype.addSetting("Button 1 OFF", OV_TypeId_Stimulation, "OVTK_GDF_End_Of_Trial");
		prototype.addFlag(Kernel::BoxFlag_CanAddInput);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_VRPNButtonServerDesc)
};
}  // namespace VRPN
}  // namespace Plugins
}  // namespace OpenViBE

#endif // OVP_HAS_Vrpn
