#pragma once

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>
#include <map>

namespace OpenViBE {
namespace Plugins {
namespace VRPN {
class CVRPNAnalogServer final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	CVRPNAnalogServer() {}
	void release() override { delete this; }
	uint64_t getClockFrequency() override { return 64LL << 32; } // 64hz
	bool initialize() override;
	bool uninitialize() override;
	bool processClock(Kernel::CMessageClock& msg) override;
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(IBoxAlgorithm, OVP_ClassId_VRPNAnalogServer)

protected:

	CIdentifier m_serverID = OV_UndefinedIdentifier;
	bool m_analogSet       = false;

	std::map<size_t, Kernel::IAlgorithmProxy*> m_decoders;
	std::map<size_t, size_t> m_nAnalogs;
};

class CVRPNAnalogServerListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:

	static bool check(Kernel::IBox& box)
	{
		for (size_t i = 0; i < box.getInputCount(); ++i)
		{
			box.setInputName(i, ("Input " + std::to_string(i + 1)).c_str());
			box.setInputType(i, OV_TypeId_StreamedMatrix);
		}

		return true;
	}

	bool onInputRemoved(Kernel::IBox& box, const size_t /*index*/) override { return check(box); }
	bool onInputAdded(Kernel::IBox& box, const size_t /*index*/) override { return check(box); }

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};

class CVRPNAnalogServerDesc final : public IBoxAlgorithmDesc
{
public:

	CString getName() const override { return CString("Analog VRPN Server"); }
	CString getAuthorName() const override { return CString("Bruno Renier/Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }
	CString getShortDescription() const override { return CString("Creates VRPN analog servers (one per input)."); }

	CString getDetailedDescription() const override
	{
		return CString("Creates VRPN analog servers to make data from the plugin's inputs available to VRPN client applications.");
	}

	CString getCategory() const override { return CString("Acquisition and network IO/VRPN"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-connect"); }
	CIdentifier getCreatedClass() const override { return OVP_ClassId_VRPNAnalogServer; }
	IPluginObject* create() override { return new CVRPNAnalogServer(); }
	IBoxListener* createBoxListener() const override { return new CVRPNAnalogServerListener; }
	void release() override { }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input 1", OV_TypeId_StreamedMatrix);
		prototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn");
		prototype.addFlag(Kernel::BoxFlag_CanAddInput);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_VRPNAnalogServerDesc)
};
}  // namespace VRPN
}  // namespace Plugins
}  // namespace OpenViBE

#endif // OVP_HAS_Vrpn
