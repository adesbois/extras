#pragma once

#if defined TARGET_HAS_ThirdPartyVRPN

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <vector>
#include <deque>

#include <vrpn_Analog.h>

namespace OpenViBE {
namespace Plugins {
namespace VRPN {
class CBoxAlgorithmVRPNAnalogClient final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	uint64_t getClockFrequency() override { return 128LL << 32; }	 // the box clock frequency
	bool initialize() override;
	bool uninitialize() override;
	bool processClock(Kernel::CMessageClock& msg) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_VRPNAnalogClient)

	void setAnalog(size_t nAnalog, const double* analog);

protected:

	uint64_t m_lastChunkEndTime  = 0;
	uint64_t m_chunkDuration     = 0;
	size_t m_sampling            = 0;
	size_t m_nChannel            = 0;
	size_t m_nSamplePerSentBlock = 0;

	bool m_firstStart = false;

	Kernel::IAlgorithmProxy* m_encoder = nullptr;
	Kernel::TParameterHandler<IMatrix*> ip_matrix;
	Kernel::TParameterHandler<uint64_t> ip_sampling;
	Kernel::TParameterHandler<IMemoryBuffer*> op_buffer;

	CString m_peripheralName;

	std::deque<std::vector<double>> m_sampleBuffer;	// Used as a limited-size buffer of sample vectors
	std::vector<double> m_lastSamples;				// The last sample received from VRPN

	vrpn_Analog_Remote* m_vrpnAnalogRemote = nullptr;
};

class CBoxAlgorithmVRPNAnalogClientDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Analog VRPN Client"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA"); }

	CString getShortDescription() const override
	{
		return CString("Connects to an external VRPN device and translate an analog information into OpenViBE signal");
	}

	CString getDetailedDescription() const override { return CString("-"); }
	CString getCategory() const override { return CString("Acquisition and network IO/VRPN"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-connect"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_VRPNAnalogClient; }
	IPluginObject* create() override { return new CBoxAlgorithmVRPNAnalogClient; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		// prototype.addInput  ("input name", /* input type (OV_TypeId_Signal) */);
		prototype.addOutput("Output", OV_TypeId_Signal);
		prototype.addSetting("Peripheral name", OV_TypeId_String, "openvibe-vrpn@localhost");
		prototype.addSetting("Sampling Rate", OV_TypeId_Integer, "512");
		prototype.addSetting("Number of Channels", OV_TypeId_Integer, "16");
		prototype.addSetting("Sample Count per Sent Block", OV_TypeId_Integer, "32");

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_VRPNAnalogClientDesc)
};
}  // namespace VRPN
}  // namespace Plugins
}  // namespace OpenViBE
#endif // TARGET_HAS_ThirdPartyVRPN
