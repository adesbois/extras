#if defined TARGET_HAS_ThirdPartyVRPN

#include "ovpCVRPNButtonServer.h"
#include "../ovpIVRPNServerManager.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace VRPN;
using namespace /*OpenViBE::*/Toolkit;
using namespace std;

bool CVRPNButtonServer::initialize()
{
	const IBox* box = getBoxAlgorithmContext()->getStaticBoxContext();

	//get server name, and creates a button server for this server
	const CString name = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);

	IVRPNServerManager::getInstance().initialize();
	IVRPNServerManager::getInstance().addServer(name, m_serverID);
	IVRPNServerManager::getInstance().setButtonCount(m_serverID, box->getInputCount());

	m_decoders.resize(box->getInputCount());

	//get stim id
	for (size_t i = 0; i < box->getInputCount(); ++i)
	{
		CString onStimulationID  = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1 + i * 2);
		CString offStimulationID = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 2 + i * 2);
		m_stimulationPairs[i]    = pair<uint64_t, uint64_t>(
			getBoxAlgorithmContext()->getPlayerContext()->getTypeManager().getEnumerationEntryValueFromName(OVTK_TypeId_Stimulation, onStimulationID),
			getBoxAlgorithmContext()->getPlayerContext()->getTypeManager().getEnumerationEntryValueFromName(OVTK_TypeId_Stimulation, offStimulationID));

		m_decoders[i] = new TStimulationDecoder<CVRPNButtonServer>;
		m_decoders[i]->initialize(*this, i);
	}

	return true;
}

bool CVRPNButtonServer::uninitialize()
{
	for (size_t i = 0; i < m_decoders.size(); ++i) { delete m_decoders[i]; }
	m_decoders.clear();

	IVRPNServerManager::getInstance().uninitialize();

	return true;
}

bool CVRPNButtonServer::processClock(Kernel::CMessageClock& /*msg*/)
{
	IVRPNServerManager::getInstance().process();
	return true;
}

bool CVRPNButtonServer::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}

bool CVRPNButtonServer::process()
{
	IBoxIO* boxIO = getBoxAlgorithmContext()->getDynamicBoxContext();

	for (size_t input = 0; input < getBoxAlgorithmContext()->getStaticBoxContext()->getInputCount(); ++input)
	{
		m_currInput = input;

		for (size_t chunk = 0; chunk < boxIO->getInputChunkCount(input); ++chunk)
		{
			m_decoders[input]->decode(chunk);

			if (m_decoders[input]->isBufferReceived())
			{
				const IStimulationSet* stimSet = m_decoders[input]->getOutputStimulationSet();
				for (size_t s = 0; s < stimSet->getStimulationCount(); ++s)
				{
					setStimulation(s, stimSet->getStimulationIdentifier(s), stimSet->getStimulationDate(s));
				}
			}
		}
	}

	return true;
}

void CVRPNButtonServer::setStimulation(const size_t /*index*/, const uint64_t id, const uint64_t /*date*/)
{
	const pair<uint64_t, uint64_t> stimulationPair = m_stimulationPairs[m_currInput];

	if (stimulationPair.first == stimulationPair.second)
	{
		if (stimulationPair.first == id)
		{
			getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Trace << "Received TOGGLE stimulation for button "
					<< m_currInput << " (" << id << ")\n";
			IVRPNServerManager::getInstance().setButtonState(m_serverID, m_currInput,
															 !IVRPNServerManager::getInstance().getButtonState(m_serverID, m_currInput));
			IVRPNServerManager::getInstance().reportButton(m_serverID);
		}
	}
	else
	{
		if (stimulationPair.first == id)
		{
			getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Trace << "Received ON stimulation for button "
					<< m_currInput << " (" << id << ")\n";
			IVRPNServerManager::getInstance().setButtonState(m_serverID, m_currInput, true);
			IVRPNServerManager::getInstance().reportButton(m_serverID);
		}
		if (stimulationPair.second == id)
		{
			getBoxAlgorithmContext()->getPlayerContext()->getLogManager() << LogLevel_Trace << "Received OFF stimulation for button "
					<< m_currInput << " (" << id << ")\n";
			IVRPNServerManager::getInstance().setButtonState(m_serverID, m_currInput, false);
			IVRPNServerManager::getInstance().reportButton(m_serverID);
		}
	}
}

#endif // OVP_HAS_Vrpn
