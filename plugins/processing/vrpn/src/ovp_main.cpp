#include <openvibe/ov_all.h>

#include "ovp_defines.h"
#include "box-algorithms/ovpCVRPNAnalogServer.h"
#include "box-algorithms/ovpCVRPNButtonServer.h"
#include "box-algorithms/ovpCBoxAlgorithmVRPNAnalogClient.h"
#include "box-algorithms/ovpCBoxAlgorithmVRPNButtonClient.h"

#include <vector>

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
#if defined TARGET_HAS_ThirdPartyVRPN
	OVP_Declare_New(VRPN::CVRPNAnalogServerDesc);
	OVP_Declare_New(VRPN::CVRPNButtonServerDesc);
	OVP_Declare_New(VRPN::CBoxAlgorithmVRPNAnalogClientDesc);
	OVP_Declare_New(VRPN::CBoxAlgorithmVRPNButtonClientDesc);
#endif

OVP_Declare_End()
