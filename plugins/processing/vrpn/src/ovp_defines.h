#pragma once

// Boxes
//---------------------------------------------------------------------------------------------------
#define OVP_ClassId_VRPNAnalogServer								OpenViBE::CIdentifier(0x0DDC3A7E, 0x6F6E6401)
#define OVP_ClassId_VRPNAnalogServerDesc							OpenViBE::CIdentifier(0xF54B8E03, 0xAAFF15C6)
#define OVP_ClassId_VRPNButtonServer								OpenViBE::CIdentifier(0x0E382E6F, 0x5BE1F00C)
#define OVP_ClassId_VRPNButtonServerDesc							OpenViBE::CIdentifier(0xBC86F256, 0x002495EF)
#define OVP_ClassId_BoxAlgorithm_VRPNAnalogClient					OpenViBE::CIdentifier(0x7CF4A95E, 0x7270D07B)
#define OVP_ClassId_BoxAlgorithm_VRPNAnalogClientDesc				OpenViBE::CIdentifier(0x77B2AE79, 0xFDC31871)
#define OVP_ClassId_BoxAlgorithm_VRPNButtonClient					OpenViBE::CIdentifier(0x40714327, 0x458877D2)
#define OVP_ClassId_BoxAlgorithm_VRPNButtonClientDesc				OpenViBE::CIdentifier(0x16FB6283, 0x45EC313F)


// Global defines
//---------------------------------------------------------------------------------------------------
#ifdef TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
#include "ovp_global_defines.h"
#endif // TARGET_HAS_ThirdPartyOpenViBEPluginsGlobalDefines
