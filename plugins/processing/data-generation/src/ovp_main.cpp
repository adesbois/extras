#include "ovp_defines.h"

#include "box-algorithms/ovpCBoxAlgorithmNoiseGenerator.h"
#include "box-algorithms/ovpCSinusSignalGenerator.h"
#include "box-algorithms/ovpCBoxAlgorithmChannelUnitsGenerator.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
	OVP_Declare_New(DataGeneration::CNoiseGeneratorDesc);
	context.getTypeManager().registerEnumerationType(OVP_TypeId_NoiseType, "Noise type");
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_NoiseType, "Uniform", OVP_TypeId_NoiseType_Uniform);
	context.getTypeManager().registerEnumerationEntry(OVP_TypeId_NoiseType, "Gaussian", OVP_TypeId_NoiseType_Gaussian);

	OVP_Declare_New(DataGeneration::CSinusSignalGeneratorDesc);
	OVP_Declare_New(DataGeneration::CChannelUnitsGeneratorDesc);


OVP_Declare_End()
