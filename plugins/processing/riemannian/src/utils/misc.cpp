#include "utils/misc.hpp"
#include <toolkit/tools/ovtkMatrix.h>

using namespace std;
using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;

using namespace Eigen;

//*****************************************************
//******************** CONVERSIONS ********************
//*****************************************************
//---------------------------------------------------------------------------------------------------
bool MatrixConvert(const IMatrix& in, MatrixXd& out)
{
	if (in.getDimensionCount() != 2) { return false; }
	out.resize(in.getDimensionSize(0), in.getDimensionSize(1));

	// double loop to avoid the problem of row major and column major storage
	size_t idx           = 0;
	const double* buffer = in.getBuffer();
	for (size_t i = 0, nR = out.rows(); i < nR; ++i) { for (size_t j = 0, nC = out.cols(); j < nC; ++j) { out(i, j) = buffer[idx++]; } }
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool MatrixConvert(const MatrixXd& in, IMatrix& out)
{
	if (in.rows() == 0 || in.cols() == 0) { return false; }
	const size_t nR = in.rows(), nC = in.cols();
	MatrixResize(out, nR, nC);

	// double loop to avoid the problem of row major and column major storage
	size_t idx     = 0;
	double* buffer = out.getBuffer();
	for (size_t i = 0; i < nR; ++i) { for (size_t j = 0; j < nC; ++j) { buffer[idx++] = in(i, j); } }
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool MatrixConvert(const RowVectorXd& in, IMatrix& out)
{
	if (in.size() == 0) { return false; }
	VectorResize(out, in.size());
	//one row system copy doesn't cause problem
	memcpy(out.getBuffer(), in.data(), out.getBufferElementCount() * sizeof(double));
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool MatrixConvert(const IMatrix& in, RowVectorXd& out)
{
	if (in.getDimensionCount() != 1) { return false; }
	out.resize(in.getDimensionSize(0));
	//one row system copy doesn't cause problem
	memcpy(out.data(), in.getBuffer(), in.getBufferElementCount() * sizeof(double));
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool MatrixConvert(const std::vector<double>& in, IMatrix& out)
{
	if (in.empty()) { return false; }
	VectorResize(out, in.size());
	//one row system copy doesn't cause problem
	memcpy(out.getBuffer(), in.data(), out.getBufferElementCount() * sizeof(double));
	return true;
}
//---------------------------------------------------------------------------------------------------

//***********************************************************
//******************** MATRIX MANAGEMENT ********************
//***********************************************************
//---------------------------------------------------------------------------------------------------
bool MatrixInit(IMatrix& m, const size_t rows, size_t columns)
{
	if (columns < 1) { columns = rows; }
	MatrixResize(m, rows, columns);
	Toolkit::Matrix::clearContent(m);	// Set to 0
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool MatrixResize(IMatrix& m, const size_t rows, size_t columns)
{
	if (columns < 1) { columns = rows; }
	if (m.getDimensionCount() != 2 || m.getDimensionSize(0) != rows || m.getDimensionSize(1) != columns)
	{
		m.setDimensionCount(2);
		m.setDimensionSize(0, rows);
		m.setDimensionSize(1, columns);

		// CHange label to have 1 to N label on row and column (Square Matrix Feature)
		for (size_t i = 0; i < rows; ++i) { m.setDimensionLabel(0, i, std::to_string(i + 1).c_str()); }
	}
	return true;
}
//---------------------------------------------------------------------------------------------------

//***********************************************************
//******************** VECTOR MANAGEMENT ********************
//***********************************************************
//---------------------------------------------------------------------------------------------------
bool VectorInit(IMatrix& m, const size_t n)
{
	VectorResize(m, n);
	Toolkit::Matrix::clearContent(m);	// Set to 0
	return true;
}
//---------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------
bool VectorResize(IMatrix& m, const size_t n)
{
	if (m.getDimensionCount() != 1 || m.getDimensionSize(0) != n)
	{
		m.setDimensionCount(1);
		m.setDimensionSize(0, n);
	}
	return true;
}
//---------------------------------------------------------------------------------------------------
