///-------------------------------------------------------------------------------------------------
/// 
/// \file misc.hpp
/// \brief All functions to Convert OpenViBE::IMatrix and Eigen::MatrixXd, links to Eigen function, manipulate OpenVibe::IMatrix and more.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 26/10/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#include <openvibe/ov_all.h>
#include <Eigen/Dense>

//*****************************************************
//******************** Conversions ********************
//*****************************************************
/// <summary>	Convert OpenViBE Matrix to Eigen Matrix. </summary>
/// <param name="in"> 	The Eigen Matrix. </param>
/// <param name="out">	The OpenVibe Matrix. </param>
bool MatrixConvert(const OpenViBE::IMatrix& in, Eigen::MatrixXd& out);

/// <summary>	Convert Eigen Matrix to OpenViBE Matrix (It doesn't use Memory::copy because of Eigne store in column major by default). </summary>
/// <param name="in"> 	The Eigen Matrix. </param>
/// <param name="out">	The OpenVibe Matrix. </param>
bool MatrixConvert(const Eigen::MatrixXd& in, OpenViBE::IMatrix& out);

/// <summary>	Convert Eigen Row Vector to OpenViBE Matrix with one dimension. </summary>
/// <param name="in"> 	The Eigen Row Vector. </param>
/// <param name="out">	The OpenVibe Matrix. </param>
bool MatrixConvert(const Eigen::RowVectorXd& in, OpenViBE::IMatrix& out);

/// <summary>	Convert OpenViBE Matrix with one dimension to Eigen Row Vector. </summary>
/// <param name="in">	The OpenVibe Matrix. </param>
/// <param name="out"> 	The Eigen Row Vector. </param>
bool MatrixConvert(const OpenViBE::IMatrix& in, Eigen::RowVectorXd& out);

/// <summary>	Convertvector double to OpenViBE Matrix with one dimension. </summary>
/// <param name="in"> 	The Vector of double. </param>
/// <param name="out">	The OpenVibe Matrix. </param>
bool MatrixConvert(const std::vector<double>& in, OpenViBE::IMatrix& out);

//***********************************************************
//******************** Matrix Management ********************
//***********************************************************
/// <summary>Initialize the matrix (do not create objects).</summary>
/// <param name="m">The matrix to initialize.</param>
/// <param name="rows">The number of rows.</param>
/// <param name="columns">The number of columns (if &lt; 1 Init to a Square Matrix) .</param>
bool MatrixInit(OpenViBE::IMatrix& m, size_t rows = 2, size_t columns = 0);

/// <summary>Resize the matrix (do not create objects).</summary>
/// <param name="m">The matrix to resize.</param>
/// <param name="rows">The number of rows.</param>
/// <param name="columns">The number of columns (if &lt; 1 resize to a Square Matrix) .</param>
bool MatrixResize(OpenViBE::IMatrix& m, size_t rows = 2, size_t columns = 0);

//***********************************************************
//******************** Vector Management ********************
//***********************************************************
/// <summary>Initialize the vector (matrix with one dimension) (do not create objects).</summary>
/// <param name="m">The vector to initialize.</param>
/// <param name="n">The number of elements.</param>
bool VectorInit(OpenViBE::IMatrix& m, size_t n = 2);

/// <summary>Resize the vector (matrix with one dimension) (do not create objects).</summary>
/// <param name="m">The vector to resize.</param>
/// <param name="n">The number of elements.</param>
bool VectorResize(OpenViBE::IMatrix& m, size_t n = 2);
