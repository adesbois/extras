///-------------------------------------------------------------------------------------------------
/// 
/// \file defines.hpp
/// \brief Defines list for Setting, Shortcut Macro and const.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 26/10/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// \remarks 
/// - List of Estimator inspired by the work of Alexandre Barachant : <a href="https://github.com/alexandrebarachant/pyRiemann">pyRiemann</a> (<a href="https://github.com/alexandrebarachant/pyRiemann/blob/master/LICENSE">License</a>).
/// - List of Metrics inspired by the work of Alexandre Barachant : <a href="https://github.com/alexandrebarachant/pyRiemann">pyRiemann</a> (<a href="https://github.com/alexandrebarachant/pyRiemann/blob/master/LICENSE">License</a>).
/// 
///-------------------------------------------------------------------------------------------------

#pragma once
namespace OpenViBE {
// Boxes
//---------------------------------------------------------------------------------------------------
#define ClassId_BoxAlgorithm_CovarianceMeanCalculator				CIdentifier(0x67955ea4, 0x7c643c0f)
#define ClassId_BoxAlgorithm_CovarianceMeanCalculatorDesc			CIdentifier(0x62e8f759, 0xd59d82a9)
#define ClassId_BoxAlgorithm_MatrixClassifierTrainer				CIdentifier(0xc0b79b42, 0x4150c837)
#define ClassId_BoxAlgorithm_MatrixClassifierTrainerDesc			CIdentifier(0x26f4fa93, 0x704d39dd)
#define ClassId_BoxAlgorithm_MatrixClassifierProcessor				CIdentifier(0x918f6952, 0xb22ddf0d)
#define ClassId_BoxAlgorithm_MatrixClassifierProcessorDesc			CIdentifier(0x8cf29eec, 0x223fbfc5)
#define ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVector		CIdentifier(0x7c265dba, 0x202c1f70)
#define ClassId_BoxAlgorithm_CovarianceMatrixToFeatureVectorDesc	CIdentifier(0xc0fb0445, 0x0d1cd546)
#define ClassId_BoxAlgorithm_FeatureVectorToCovarianceMatrix		CIdentifier(0x7c265dba, 0x202c1f71)
#define ClassId_BoxAlgorithm_FeatureVectorToCovarianceMatrixDesc	CIdentifier(0xc0fb0445, 0x0d1cd541)
#define ClassId_BoxAlgorithm_CovarianceMatrixCalculator				CIdentifier(0x9a93af80, 0x6449c826)
#define ClassId_BoxAlgorithm_CovarianceMatrixCalculatorDesc			CIdentifier(0x12fcd91f, 0xd1d8f678)
#define ClassId_BoxAlgorithm_MatrixAffineTransformation				CIdentifier(0x1BAA7180, 0x52CB19B8)
#define ClassId_BoxAlgorithm_MatrixAffineTransformationDesc			CIdentifier(0x0AF511E5, 0x27137BBA)

// M�thodes/Types Lists
//---------------------------------------------------------------------------------------------------
#define TypeId_Estimator											CIdentifier(0x5261636B, 0x45535449)
#define TypeId_Metric												CIdentifier(0x5261636B, 0x4D455452)
#define TypeId_Matrix_Classifier									CIdentifier(0x5261636B, 0x436C6173)
#define TypeId_Classifier_Adaptation								CIdentifier(0x5261636B, 0x41646170)
//---------------------------------------------------------------------------------------------------
}  // namespace OpenViBE
