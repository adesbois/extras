#include <openvibe/ov_all.h>
#include "defines.hpp"

// Boxes Includes
#include "boxes/CBoxAlgorithmCovarianceMatrixCalculator.hpp"
#include "boxes/CBoxAlgorithmCovarianceMatrixToFeatureVector.hpp"
#include "boxes/CBoxAlgorithmFeatureVectorToCovarianceMatrix.hpp"
#include "boxes/CBoxAlgorithmCovarianceMeanCalculator.hpp"
#include "boxes/CBoxAlgorithmMatrixClassifierTrainer.hpp"
#include "boxes/CBoxAlgorithmMatrixClassifierProcessor.hpp"
#include "boxes/CBoxAlgorithmMatrixAffineTransformation.hpp"

namespace OpenViBE {
namespace Plugins {

template <typename T>
static void setEnumeration(const Kernel::IPluginModuleContext& context, const CIdentifier& typeID, const std::string& name, const std::vector<T>& enumeration)
{
	context.getTypeManager().registerEnumerationType(typeID, name.c_str());
	for (const auto& e : enumeration) { context.getTypeManager().registerEnumerationEntry(typeID, toString(e).c_str(), size_t(e)); }
}


OVP_Declare_Begin()
	// Register boxes
	OVP_Declare_New(Riemannian::CBoxAlgorithmCovarianceMatrixCalculatorDesc);
	OVP_Declare_New(Riemannian::CBoxAlgorithmCovarianceMatrixToFeatureVectorDesc);
	OVP_Declare_New(Riemannian::CBoxAlgorithmFeatureVectorToCovarianceMatrixDesc);
	OVP_Declare_New(Riemannian::CBoxAlgorithmCovarianceMeanCalculatorDesc);
	OVP_Declare_New(Riemannian::CBoxAlgorithmMatrixClassifierTrainerDesc);
	OVP_Declare_New(Riemannian::CBoxAlgorithmMatrixClassifierProcessorDesc);
	OVP_Declare_New(Riemannian::CBoxAlgorithmMatrixAffineTransformationDesc);

	// Enumeration Estimator
	const std::vector<Geometry::EEstimator> estimators = {
		Geometry::EEstimator::COV, Geometry::EEstimator::COR, Geometry::EEstimator::LWF,
		Geometry::EEstimator::SCM, Geometry::EEstimator::OAS, Geometry::EEstimator::IDE
	};
	setEnumeration(context, TypeId_Estimator, "Estimator", estimators);

	// Enumeration Metric
	const std::vector<Geometry::EMetric> metrics = {
		Geometry::EMetric::Riemann, Geometry::EMetric::Euclidian, Geometry::EMetric::LogEuclidian, Geometry::EMetric::LogDet,
		Geometry::EMetric::Kullback, Geometry::EMetric::Harmonic, Geometry::EMetric::Identity
	};
	setEnumeration(context, TypeId_Metric, "Metric", metrics);

	// Enumeration Classifier
	const std::vector<Geometry::EMatrixClassifiers> classifiers = {
		Geometry::EMatrixClassifiers::MDM, Geometry::EMatrixClassifiers::MDM_Rebias,
		Geometry::EMatrixClassifiers::FgMDM_RT, Geometry::EMatrixClassifiers::FgMDM_RT_Rebias
	};
	setEnumeration(context, TypeId_Matrix_Classifier, "Matrix Classifier", classifiers);

	// Enumeration Classifier Adaptater
	const std::vector<Geometry::EAdaptations> adaptations = {
		Geometry::EAdaptations::None, Geometry::EAdaptations::Supervised, Geometry::EAdaptations::Unsupervised
	};
	setEnumeration(context, TypeId_Classifier_Adaptation, "Classifier Adaptation", adaptations);

OVP_Declare_End()

}  // namespace Plugins
}  // namespace OpenViBE
