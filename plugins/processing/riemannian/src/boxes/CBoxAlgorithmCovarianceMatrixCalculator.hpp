///-------------------------------------------------------------------------------------------------
/// 
/// \file CBoxAlgorithmCovarianceMatrixCalculator.hpp
/// \brief Class of the box computing the covariance matrix
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 16/10/2018.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------
# pragma once

#include "defines.hpp"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <geometry/Covariance.hpp>

namespace OpenViBE {
namespace Plugins {
namespace Riemannian {
/// <summary>	 The class CBoxAlgorithmCovarianceMatrixCalculator describes the box Covariance Matrix Calculator. </summary>
class CBoxAlgorithmCovarianceMatrixCalculator final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, ClassId_BoxAlgorithm_CovarianceMatrixCalculator)

protected:
	bool covarianceMatrix() const;

	//***** Codecs *****
	Toolkit::TSignalDecoder<CBoxAlgorithmCovarianceMatrixCalculator> m_i0SignalCodec;			// Input Signal Codec
	Toolkit::TStreamedMatrixEncoder<CBoxAlgorithmCovarianceMatrixCalculator> m_o0MatrixCodec;	// Output Matrix Codec

	//***** Matrices *****
	IMatrix* m_iMatrix = nullptr;								// Input Matrix pointer
	IMatrix* m_oMatrix = nullptr;								// Output Matrix pointer

	//***** Settings *****
	Geometry::EEstimator m_est   = Geometry::EEstimator::COV;	// Covariance Estimator
	bool m_center                = true;						// Center data
	Kernel::ELogLevel m_logLevel = Kernel::LogLevel_Info;		// Log Level
};

/// <summary>	 Descriptor of the box Covariance Matrix Calculator. </summary>
class CBoxAlgorithmCovarianceMatrixCalculatorDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return "Covariance Matrix Calculator"; }
	CString getAuthorName() const override { return "Thibaut Monseigne"; }
	CString getAuthorCompanyName() const override { return "Inria"; }
	CString getShortDescription() const override { return "Calculation of the covariance matrix of the input signal."; }

	CString getDetailedDescription() const override
	{
		return "Calculation of the covariance matrix of the input signal.\nReturns a covariance matrix of size NxN per each input chunk. Where N is the number of channels.";
	}

	CString getCategory() const override { return "Riemannian Geometry"; }
	CString getVersion() const override { return "0.1"; }
	CString getStockItemName() const override { return "gtk-execute"; }

	CIdentifier getCreatedClass() const override { return ClassId_BoxAlgorithm_CovarianceMatrixCalculator; }
	IPluginObject* create() override { return new CBoxAlgorithmCovarianceMatrixCalculator; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Input Signal", OV_TypeId_Signal);
		prototype.addOutput("Output Covariance Matrix", OV_TypeId_StreamedMatrix);

		prototype.addSetting("Estimator", TypeId_Estimator, toString(Geometry::EEstimator::COV).c_str());
		prototype.addSetting("Center Data", OV_TypeId_Boolean, "true");
		prototype.addSetting("Log Level", OV_TypeId_LogLevel, "Information");

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, ClassId_BoxAlgorithm_CovarianceMatrixCalculatorDesc)
};
}  // namespace Riemannian
}  // namespace Plugins
}  // namespace OpenViBE
