///-------------------------------------------------------------------------------------------------
/// 
/// \file CBoxAlgorithmMatrixAffineTransformation.hpp
/// \brief Class of the box Affine Transformation.
/// \author Thibaut Monseigne (Inria).
/// \version 1.0.
/// \date 28/08/2019.
/// \copyright <a href="https://choosealicense.com/licenses/agpl-3.0/">GNU Affero General Public License v3.0</a>.
/// 
///-------------------------------------------------------------------------------------------------

#pragma once

#include "defines.hpp"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <geometry/classifier/CBias.hpp>

namespace OpenViBE {
namespace Plugins {
namespace Riemannian {
/// <summary>	The class CBoxAlgorithmMatrixAffineTransformation describes the box Matrix Affine Transformation. </summary>
class CBoxAlgorithmMatrixAffineTransformation final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, ClassId_BoxAlgorithm_MatrixAffineTransformation)

protected:

	bool loadXML();
	bool saveXML() const;

	//***** Codecs *****
	Toolkit::TStreamedMatrixDecoder<CBoxAlgorithmMatrixAffineTransformation> m_iMatrixCodec;	// Input Signal Codec
	Toolkit::TStreamedMatrixEncoder<CBoxAlgorithmMatrixAffineTransformation> m_oMatrixCodec;	// Output Signal Codec

	IMatrix *m_iMatrix = nullptr, *m_oMatrix = nullptr;		// Input/Output Matrix pointer

	//***** Settings *****
	std::string m_ifilename, m_ofilename;					// Input/Output Filename
	bool m_continuous = false;

	//***** Variable *****
	Geometry::CBias m_bias;
	std::vector<Eigen::MatrixXd> m_samples;
};

/// <summary>	Descriptor of the box Matrix Classifier Trainer. </summary>
class CBoxAlgorithmMatrixAffineTransformationDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return "Matrix Affine Transformation"; }
	CString getAuthorName() const override { return "Thibaut Monseigne"; }
	CString getAuthorCompanyName() const override { return "Inria"; }
	CString getShortDescription() const override { return "Compute and Apply the Bias matrix for Affine Transformation on square matrix."; }

	CString getDetailedDescription() const override
	{
		return "Compute and Apply the Reference matrix for Affine Transformation on square matrix (isR * M * isR^(-1) = I).\nYou can load an existing matrix.\nContinuous update is to update Bias at each chunk or at the end.";
	}

	CString getCategory() const override { return "Riemannian Geometry"; }
	CString getVersion() const override { return "0.1"; }
	CString getStockItemName() const override { return "gtk-execute"; }

	CIdentifier getCreatedClass() const override { return ClassId_BoxAlgorithm_MatrixAffineTransformation; }
	IPluginObject* create() override { return new CBoxAlgorithmMatrixAffineTransformation; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Square Matrix",OV_TypeId_StreamedMatrix);
		prototype.addOutput("Transformed Square Matrix", OV_TypeId_StreamedMatrix);

		prototype.addSetting("Filename to load transformation", OV_TypeId_Filename, "${Player_ScenarioDirectory}/my-transformation-input.xml");
		prototype.addSetting("Filename to save transformation",OV_TypeId_Filename, "${Player_ScenarioDirectory}/my-transformation-output.xml");
		prototype.addSetting("Continuous Update", OV_TypeId_Boolean, "false");

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, ClassId_BoxAlgorithm_MatrixAffineTransformationDesc)
};
} // namespace Riemannian
}  // namespace Plugins
}  // namespace OpenViBE
