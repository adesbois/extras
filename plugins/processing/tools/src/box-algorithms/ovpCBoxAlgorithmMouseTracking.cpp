#if defined(TARGET_HAS_ThirdPartyGTK)

#include "ovpCBoxAlgorithmMouseTracking.h"


using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Tools;
using namespace /*OpenViBE::*/Toolkit;


void motion_event_handler(GtkWidget* widget, GdkEventMotion* event, gpointer data);

bool CBoxAlgorithmMouseTracking::initialize()
{
	// Feature vector stream encoder
	m_algo0SignalEncoder.initialize(*this, 0);
	// Feature vector stream encoder
	m_algo1SignalEncoder.initialize(*this, 1);

	m_algo0SignalEncoder.getInputMatrix().setReferenceTarget(m_absoluteCoordinateBuffer);
	m_algo1SignalEncoder.getInputMatrix().setReferenceTarget(m_relativeCoordinateBuffer);

	// Allocates coordinates Matrix
	m_absoluteCoordinateBuffer = new CMatrix();
	m_relativeCoordinateBuffer = new CMatrix();

	// Retrieves settings
	m_sampling              = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 0);
	m_nGeneratedEpochSample = FSettingValueAutoCast(*this->getBoxAlgorithmContext(), 1);

	m_nSentSample = 0;
	m_MouseX      = 0;
	m_MouseY      = 0;
	m_prevX       = 0;
	m_prevY       = 0;

	// Creates empty window to get mouse position
	m_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_add_events(m_window, GDK_POINTER_MOTION_MASK);
	gtk_window_maximize(GTK_WINDOW(m_window));
	gtk_widget_show_all(m_window);

	g_signal_connect(m_window, "motion-notify-event", G_CALLBACK(motion_event_handler), this);

	m_headerSent = false;

	m_algo0SignalEncoder.getInputSamplingRate() = m_sampling;
	m_algo1SignalEncoder.getInputSamplingRate() = m_sampling;

	return true;
}
/*******************************************************************************/

bool CBoxAlgorithmMouseTracking::uninitialize()
{
	m_algo0SignalEncoder.uninitialize();
	m_algo1SignalEncoder.uninitialize();

	delete m_absoluteCoordinateBuffer;
	m_absoluteCoordinateBuffer = nullptr;

	delete m_relativeCoordinateBuffer;
	m_relativeCoordinateBuffer = nullptr;

	gtk_widget_destroy(m_window);
	m_window = nullptr;

	return true;
}
/*******************************************************************************/


bool CBoxAlgorithmMouseTracking::processClock(Kernel::CMessageClock& /*msg*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();
	return true;
}
/*******************************************************************************/


uint64_t CBoxAlgorithmMouseTracking::getClockFrequency()
{
	// Intentional parameter swap to get the frequency
	m_clockFrequency = CTime(m_nGeneratedEpochSample, m_sampling).time();

	return m_clockFrequency;
}

/*******************************************************************************/

bool CBoxAlgorithmMouseTracking::process()
{
	IBoxIO& boxContext = this->getDynamicBoxContext();

	//Send header and initialize Matrix
	if (!m_headerSent)
	{
		m_absoluteCoordinateBuffer->setDimensionCount(2);
		m_absoluteCoordinateBuffer->setDimensionSize(0, 2);
		m_absoluteCoordinateBuffer->setDimensionSize(1, m_nGeneratedEpochSample);
		m_absoluteCoordinateBuffer->setDimensionLabel(0, 0, "x");
		m_absoluteCoordinateBuffer->setDimensionLabel(0, 1, "y");

		m_relativeCoordinateBuffer->setDimensionCount(2);
		m_relativeCoordinateBuffer->setDimensionSize(0, 2);
		m_relativeCoordinateBuffer->setDimensionSize(1, m_nGeneratedEpochSample);
		m_relativeCoordinateBuffer->setDimensionLabel(0, 0, "x");
		m_relativeCoordinateBuffer->setDimensionLabel(0, 1, "y");


		m_algo0SignalEncoder.encodeHeader();
		m_algo1SignalEncoder.encodeHeader();

		m_headerSent = true;

		boxContext.markOutputAsReadyToSend(0, 0, 0);
		boxContext.markOutputAsReadyToSend(1, 0, 0);
	}
	else //Do the process and send coordinates to output
	{
		const size_t nSentSample = m_nSentSample;


		for (size_t i = 0; i < m_nGeneratedEpochSample; ++i)
		{
			m_absoluteCoordinateBuffer->getBuffer()[0 * m_nGeneratedEpochSample + i] = m_MouseX;
			m_absoluteCoordinateBuffer->getBuffer()[1 * m_nGeneratedEpochSample + i] = m_MouseY;

			m_relativeCoordinateBuffer->getBuffer()[0 * m_nGeneratedEpochSample + i] = m_MouseX - m_prevX;
			m_relativeCoordinateBuffer->getBuffer()[1 * m_nGeneratedEpochSample + i] = m_MouseY - m_prevY;
		}


		m_prevX = m_MouseX;
		m_prevY = m_MouseY;

		m_nSentSample += m_nGeneratedEpochSample;

		const uint64_t tStart = CTime(m_sampling, nSentSample).time();
		const uint64_t tEnd   = CTime(m_sampling, m_nSentSample).time();

		m_algo0SignalEncoder.encodeBuffer();
		m_algo1SignalEncoder.encodeBuffer();

		boxContext.markOutputAsReadyToSend(0, tStart, tEnd);
		boxContext.markOutputAsReadyToSend(1, tStart, tEnd);
	}
	return true;
}

// CALLBACK
// Get mouse position
void motion_event_handler(GtkWidget* /*widget*/, GdkEventMotion* event, gpointer data)
{
	CBoxAlgorithmMouseTracking* box = static_cast<CBoxAlgorithmMouseTracking*>(data);
	box->m_MouseX                   = double(event->x);
	box->m_MouseY                   = double(event->y);
}


#endif
