#pragma once

#if defined(TARGET_HAS_ThirdPartyGTK)

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

#include <iostream>

#include <gtk/gtk.h>

namespace OpenViBE {
namespace Plugins {
namespace Tools {
/**
 * \class CBoxAlgorithmMouseTracking
 * \author Alison Cellard (Inria)
 * \date Mon Mar 10 15:07:21 2014
 * \brief The class CBoxAlgorithmMouseTracking describes the box Mouse tracking.
 *
 */
class CBoxAlgorithmMouseTracking final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	// CBoxAlgorithmMouseTracking();
	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processClock(Kernel::CMessageClock& msg) override;
	uint64_t getClockFrequency() override;

	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_MouseTracking)

protected:
	// Feature vector stream encoder
	Toolkit::TSignalEncoder<CBoxAlgorithmMouseTracking> m_algo0SignalEncoder;
	// Feature vector stream encoder
	Toolkit::TSignalEncoder<CBoxAlgorithmMouseTracking> m_algo1SignalEncoder;

	// To check if the header was sent or not
	bool m_headerSent = false;

	// Requested Sampling frequency
	size_t m_sampling = 0;
	// Process clock frequency
	uint64_t m_clockFrequency = 0;

	// Length of output chunks
	size_t m_nGeneratedEpochSample = 0;
	// Absolute coordinates of the mouse pointer, that is, relative to the window in fullscreen
	IMatrix* m_absoluteCoordinateBuffer = nullptr;
	// Relative coordinates of the mouse pointer, the coordinates is relative to the previous point
	IMatrix* m_relativeCoordinateBuffer = nullptr;

	size_t m_nSentSample = 0;

	// Gtk window to track mouse position
	GtkWidget* m_window = nullptr;

	// X coordinate from the previous position (in pixel, reference is upper left corner of window)
	double m_prevX = 0;
	// Y coordinate from the previous position (in pixel, reference is upper left corner of window)
	double m_prevY = 0;


public:
	// X coordinate of mouse current position
	double m_MouseX = 0;
	// Y coordinate of mouse current position
	double m_MouseY = 0;
};


class CBoxAlgorithmMouseTrackingListener final : public Toolkit::TBoxListener<IBoxListener>
{
public:
	bool onSettingValueChanged(Kernel::IBox& /*box*/, const size_t /*index*/) override { return true; }

	_IsDerivedFromClass_Final_(Toolkit::TBoxListener<IBoxListener>, OV_UndefinedIdentifier)
};


/**
 * \class CBoxAlgorithmMouseTrackingDesc
 * \author Alison Cellard (Inria)
 * \date Mon Mar 10 15:07:21 2014
 * \brief Descriptor of the box Mouse tracking.
 *
 */
class CBoxAlgorithmMouseTrackingDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Mouse Tracking"); }
	CString getAuthorName() const override { return CString("Alison Cellard"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Track mouse position within the screen"); }

	CString getDetailedDescription() const override { return CString("Return absolute and relative to the previous one mouse position"); }

	CString getCategory() const override { return CString("Tools"); }
	CString getVersion() const override { return CString("1"); }
	CString getStockItemName() const override { return CString("gtk-index"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_MouseTracking; }
	IPluginObject* create() override { return new CBoxAlgorithmMouseTracking; }


	IBoxListener* createBoxListener() const override { return new CBoxAlgorithmMouseTrackingListener; }
	void releaseBoxListener(IBoxListener* listener) const override { delete listener; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addOutput("Absolute coordinate",OV_TypeId_Signal);
		prototype.addOutput("Previous relative coordinate",OV_TypeId_Signal);

		prototype.addSetting("Sampling Frequency",OV_TypeId_Integer, "16");
		prototype.addSetting("Generated epoch sample count",OV_TypeId_Integer, "1");

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_MouseTrackingDesc)
};
}  // namespace Tools
}  // namespace Plugins
}  // namespace OpenViBE


#endif
