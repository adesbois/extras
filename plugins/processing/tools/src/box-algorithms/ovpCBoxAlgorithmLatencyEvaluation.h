#pragma once

#include "../ovp_defines.h"
#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace Tools {
class CBoxAlgorithmLatencyEvaluation final : public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override { return true; }
	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_LatencyEvaluation)

	uint64_t m_StartTime         = 0;
	Kernel::ELogLevel m_LogLevel = Kernel::ELogLevel::LogLevel_None;
};

class CBoxAlgorithmLatencyEvaluationDesc final : public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Latency evaluation"); }
	CString getAuthorName() const override { return CString("Yann Renard"); }
	CString getAuthorCompanyName() const override { return CString("INRIA/IRISA"); }
	CString getShortDescription() const override { return CString("Evaluates i/o jittering and outputs values to log manager"); }

	CString getDetailedDescription() const override
	{
		return CString("This box evaluates i/o jittering comparing input chunk' start and end time against current clock time");
	}

	CString getCategory() const override { return CString("Tools"); }
	CString getVersion() const override { return CString("1.0"); }
	CString getStockItemName() const override { return CString("gtk-info"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_LatencyEvaluation; }
	IPluginObject* create() override { return new CBoxAlgorithmLatencyEvaluation; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("input", OV_TypeId_EBMLStream);
		prototype.addSetting("Log level to use", OV_TypeId_LogLevel, "Trace");
		prototype.addFlag(Kernel::BoxFlag_CanModifyInput);
		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_LatencyEvaluationDesc)
};
}  // namespace Tools
}  // namespace Plugins
}  // namespace OpenViBE
