#pragma once

#include "../ovp_defines.h"

#include <openvibe/ov_all.h>
#include <toolkit/ovtk_all.h>

namespace OpenViBE {
namespace Plugins {
namespace Tools {
/**
 * \class CBoxAlgorithmKeypressEmulator
 * \author Jussi T. Lindgren / Inria
 * \date 29.Oct.2019
 * \brief Emulates keypresses on a keyboard based on input stimulations. Based on a request from Fabien Lotte / POTIOC.
 *
 */
class CBoxAlgorithmKeypressEmulator final : virtual public Toolkit::TBoxAlgorithm<IBoxAlgorithm>
{
public:

	void release() override { delete this; }

	bool initialize() override;
	bool uninitialize() override;

	bool processInput(const size_t index) override;
	bool process() override;

	_IsDerivedFromClass_Final_(Toolkit::TBoxAlgorithm<IBoxAlgorithm>, OVP_ClassId_BoxAlgorithm_KeypressEmulator)

	// Register enums to the kernel used by this box
	static void registerEnums(const Kernel::IPluginModuleContext& ctx);

protected:
	Toolkit::TStimulationDecoder<CBoxAlgorithmKeypressEmulator> m_decoder;

	// @todo for multiple triggers, use std::map<> 
	uint64_t m_triggerStimulation = 0;
	uint64_t m_keyToPress         = 0;
	uint64_t m_modifier           = 0;
};

class CBoxAlgorithmKeypressEmulatorDesc final : virtual public IBoxAlgorithmDesc
{
public:

	void release() override { }

	CString getName() const override { return CString("Keypress Emulator"); }
	CString getAuthorName() const override { return CString("Jussi T. Lindgren"); }
	CString getAuthorCompanyName() const override { return CString("Inria"); }
	CString getShortDescription() const override { return CString("Emulates pressing keyboard keys when receiving stimulations"); }
	CString getDetailedDescription() const override { return CString(""); }
	CString getCategory() const override { return CString("Tools"); }
	CString getVersion() const override { return CString("0.1"); }
	CString getStockItemName() const override { return CString("gtk-index"); }

	CIdentifier getCreatedClass() const override { return OVP_ClassId_BoxAlgorithm_KeypressEmulator; }
	IPluginObject* create() override { return new CBoxAlgorithmKeypressEmulator; }

	bool getBoxPrototype(Kernel::IBoxProto& prototype) const override
	{
		prototype.addInput("Stimulations",OV_TypeId_Stimulations);

		// @todo add support for multiple keys, e.g. look at VRPN boxes for howto
		prototype.addSetting("Trigger", OV_TypeId_Stimulation, "OVTK_StimulationId_Label_00");
		prototype.addSetting("Key to press", OVP_TypeId_Keypress_Key, "A");
		prototype.addSetting("Key modifier", OVP_TypeId_Keypress_Modifier, OVP_TypeId_Keypress_Modifier_None.toString());

		prototype.addFlag(OV_AttributeId_Box_FlagIsUnstable);

		return true;
	}

	_IsDerivedFromClass_Final_(IBoxAlgorithmDesc, OVP_ClassId_BoxAlgorithm_KeypressEmulatorDesc)
};
}  // namespace Tools
}  // namespace Plugins
}  // namespace OpenViBE
