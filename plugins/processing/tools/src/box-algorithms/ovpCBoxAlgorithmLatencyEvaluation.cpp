#include "ovpCBoxAlgorithmLatencyEvaluation.h"
#include <system/ovCTime.h>

// @note by just repeatedly printing to the console, this box introduces significant latency by itself.
// @fixme If it makes sense to enable this box at all, it should be reimplemented as printing to a small GUI widget instead.

using namespace OpenViBE;
using namespace /*OpenViBE::*/Kernel;
using namespace /*OpenViBE::*/Plugins;
using namespace Tools;

bool CBoxAlgorithmLatencyEvaluation::initialize()
{
	CString logLevel;
	getBoxAlgorithmContext()->getStaticBoxContext()->getSettingValue(0, logLevel);
	m_LogLevel = ELogLevel(getBoxAlgorithmContext()->getPlayerContext()->getTypeManager().getEnumerationEntryValueFromName(OV_TypeId_LogLevel, logLevel));

	m_StartTime = System::Time::zgetTime();

	return true;
}

bool CBoxAlgorithmLatencyEvaluation::processInput(const size_t /*index*/)
{
	getBoxAlgorithmContext()->markAlgorithmAsReadyToProcess();

	return true;
}

bool CBoxAlgorithmLatencyEvaluation::process()
{
	IBoxIO& boxContext  = this->getDynamicBoxContext();
	const uint64_t time = getPlayerContext().getCurrentTime();

	for (size_t i = 0; i < boxContext.getInputChunkCount(0); ++i)
	{
		const uint64_t start      = boxContext.getInputChunkStartTime(0, i);
		const uint64_t end        = boxContext.getInputChunkEndTime(0, i);
		const double startLatency = (double((int64_t(time - start) >> 22) * 1000) / 1024.0);
		const double endLatency   = (double((int64_t(time - end) >> 22) * 1000) / 1024.0);

		// getLogManager() << LogLevel_Debug << "Timing values [start:end:current]=[" << tStart << ":" << tEnd << ":" << time << "]\n";
		getLogManager() << m_LogLevel << "Current latency at chunk " << i << " [start:end]=[" << startLatency << ":" << endLatency << "] ms\n";

		boxContext.markInputAsDeprecated(0, i);
	}

	const uint64_t elapsed = System::Time::zgetTime() - m_StartTime;
	const double latency   = (double((int64_t(elapsed - time) >> 22) * 1000) / 1024.0);

	getLogManager() << m_LogLevel << "Inner latency : " << latency << " ms\n";

	return true;
}
