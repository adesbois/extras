#include "ovp_defines.h"

//// #include "box-algorithms/ovpCBoxAlgorithmLatencyEvaluation.h"
#include "box-algorithms/ovpCBoxAlgorithmMouseTracking.h"
#include "box-algorithms/ovpCBoxAlgorithmKeypressEmulator.h"

#include "openvibe/ovCIdentifier.h"

using namespace OpenViBE;
using namespace /*OpenViBE::*/Plugins;

OVP_Declare_Begin()
	context.getTypeManager().registerEnumerationEntry(OV_TypeId_BoxAlgorithmFlag, OV_AttributeId_Box_FlagIsUnstable.toString(),
													  OV_AttributeId_Box_FlagIsUnstable.toUInteger());
	// OVP_Declare_New(Tools::CBoxAlgorithmLatencyEvaluationDesc);
#if defined(TARGET_HAS_ThirdPartyGTK)
	OVP_Declare_New(Tools::CBoxAlgorithmMouseTrackingDesc);
#endif

	OVP_Declare_New(Tools::CBoxAlgorithmKeypressEmulatorDesc);
	// @note the following code is a bit long so we've implemented it inside the class
	Tools::CBoxAlgorithmKeypressEmulator::registerEnums(context);

OVP_Declare_End()
